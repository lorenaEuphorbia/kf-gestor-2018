-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 11-10-2017 a las 09:41:45
-- Versión del servidor: 5.5.54-0ubuntu0.12.04.1
-- Versión de PHP: 5.6.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gestor`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formularios`
--

CREATE TABLE `formularios` (
  `id` int(11) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `email` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `mensaje` text COLLATE utf8_spanish2_ci,
  `is_aviso` tinyint(1) NOT NULL DEFAULT '0',
  `is_correo` tinyint(1) DEFAULT '0',
  `creacion` timestamp NULL DEFAULT NULL,
  `modificacion` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `formularios`
--

INSERT INTO `formularios` (`id`, `users_id`, `email`, `telefono`, `nombre`, `mensaje`, `is_aviso`, `is_correo`, `creacion`, `modificacion`) VALUES
(1, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:23:29', '2017-07-04 10:23:29'),
(2, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:23:53', '2017-07-04 10:23:53'),
(3, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:42:44', '2017-07-04 10:42:44'),
(4, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:43:19', '2017-07-04 10:43:19'),
(5, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:43:26', '2017-07-04 10:43:26'),
(6, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:45:44', '2017-07-04 10:45:44'),
(7, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:48:59', '2017-07-04 10:48:59'),
(8, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:49:25', '2017-07-04 10:49:25'),
(9, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:49:49', '2017-07-04 10:49:49'),
(10, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:50:30', '2017-07-04 10:50:30'),
(11, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:54:09', '2017-07-04 10:54:09'),
(12, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 1, '2017-07-04 10:55:32', '2017-07-04 12:55:33'),
(13, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-05 06:12:48', '2017-07-05 06:12:48'),
(14, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 1, '2017-07-05 06:14:02', '2017-07-05 08:14:02'),
(15, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 1, '2017-07-05 06:15:21', '2017-07-05 08:15:21'),
(16, NULL, 'adsas@asdas.com', NULL, 'Cesar Vega Martin', 'asdasda das d s', 1, 1, '2017-07-05 07:11:51', '2017-07-05 09:11:52'),
(17, NULL, 'adsas@asdas.com', NULL, 'Cesar Vega Martin', 'asdasda das d s', 1, 1, '2017-07-05 07:12:04', '2017-07-05 09:12:04'),
(18, NULL, 'adsas@asdas.com', NULL, 'Cesar Vega Martin', 'asdasda das d s', 1, 1, '2017-07-05 07:19:01', '2017-07-05 09:19:02'),
(19, NULL, 'asdad@asdas.com', NULL, 'Cesar Vega Martin', 'asdas', 1, 1, '2017-07-05 07:25:04', '2017-07-05 09:25:05'),
(20, NULL, 'asdad@asdas.com', NULL, 'Cesar Vega Martin', 'asdas', 1, 1, '2017-07-05 07:27:01', '2017-07-05 09:27:05'),
(21, NULL, 'asdad@asdas.com', NULL, 'Cesar Vega Martin', 'asdas', 1, 1, '2017-07-05 09:08:14', '2017-07-05 11:08:15'),
(22, NULL, 'asdad@asdas.com', NULL, 'Cesar Vega Martin', 'asdas', 1, 1, '2017-07-05 09:08:27', '2017-07-05 11:08:34'),
(23, NULL, NULL, NULL, NULL, NULL, 0, 1, '2017-07-05 09:10:17', '2017-07-05 11:10:24'),
(24, NULL, 'cevm88@gmail.com', NULL, 'Cesar Vega Martin', 'asdas', 1, 1, '2017-07-05 09:10:40', '2017-07-05 11:10:41'),
(25, NULL, 'asdasda', NULL, 'Cesar Vega Martin', 'dsadasd', 1, 1, '2017-07-05 09:16:23', '2017-07-05 11:16:24'),
(26, NULL, 'asdasda', NULL, 'Cesar Vega Martin', 'dsadasd', 1, 1, '2017-07-05 09:20:53', '2017-07-05 11:20:53'),
(27, NULL, 'asdasda', NULL, 'Cesar Vega Martin', 'dsadasd', 1, 1, '2017-07-05 09:20:58', '2017-07-05 11:20:58'),
(28, NULL, 'adsas@asdas.com', NULL, 'Cesar Vega Martin', 'asdasd', 1, 1, '2017-07-05 09:22:27', '2017-07-05 11:22:28'),
(29, NULL, 'adsas@asdas.com', NULL, 'Cesar Vega Martin', 'asdasd', 1, 1, '2017-07-05 09:22:49', '2017-07-05 11:22:50'),
(30, NULL, 'cevm88@gmail.com', NULL, 'Cesar Vega Martin', 'asdasdad', 1, 1, '2017-07-05 09:23:01', '2017-07-05 11:23:02'),
(31, NULL, 'cevm88@gmail.com', NULL, 'Cesar Vega Martin', 'asdsadas', 1, 1, '2017-07-05 09:23:54', '2017-07-05 11:23:54'),
(32, NULL, 'cevm88@gmail.com', NULL, 'Cesar Vega Martin', 'asdsadas', 1, 1, '2017-07-05 09:25:33', '2017-07-05 11:25:34'),
(33, NULL, 'asdads', NULL, 'Cesar Vega Martin', 'asdas', 1, 1, '2017-07-05 09:31:27', '2017-07-05 11:31:28'),
(34, NULL, 'adsas@asdas.com', NULL, 'Cesar Vega Martin', 'asdasd', 1, 1, '2017-07-05 09:32:24', '2017-07-05 11:32:25'),
(35, NULL, 'asdad@asdas.com', NULL, 'Cesar Vega Martin', 'asd', 1, 1, '2017-07-05 09:33:17', '2017-07-05 11:33:18'),
(36, NULL, 'cevm88@gmail.com', NULL, 'Cesar Vega Martin', 'asdas', 1, 1, '2017-07-05 09:34:22', '2017-07-05 11:34:23'),
(37, NULL, 'cevm88@gmail.com', NULL, 'Cesar Vega Martin', 'ascd', 1, 1, '2017-07-05 09:34:59', '2017-07-05 11:35:00'),
(38, NULL, 'cevm88@gmail.com', NULL, 'Cesar Vega Martin', 'ceasdad', 1, 1, '2017-07-05 09:36:42', '2017-07-05 11:36:42'),
(39, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 1, 1, '2017-07-11 04:28:58', '2017-07-11 06:28:59'),
(40, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 1, 1, '2017-07-20 05:21:09', '2017-07-20 07:21:12'),
(41, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 1, 1, '2017-08-09 09:16:23', '2017-08-09 11:16:24'),
(42, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 1, 1, '2017-08-09 09:19:19', '2017-08-09 11:19:20'),
(43, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 1, 1, '2017-08-09 09:21:01', '2017-08-09 11:21:02'),
(44, NULL, 'cevm88@gmail.com', NULL, 'test', 'teasd', 1, 1, '2017-08-09 10:44:01', '2017-08-09 12:44:01'),
(45, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 1, 1, '2017-08-22 09:20:39', '2017-08-22 11:20:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idiomas`
--

CREATE TABLE `idiomas` (
  `id` int(11) NOT NULL,
  `codigo` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `region` varchar(5) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre_es` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `oculto` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `idiomas`
--

INSERT INTO `idiomas` (`id`, `codigo`, `region`, `nombre_es`, `nombre`, `activo`, `oculto`) VALUES
(1, 'en', 'en-us', 'Inglés', 'English - United States', 1, 0),
(2, 'es', 'es-es', 'Español', 'Spanish - Spain (Traditional)', 1, 0),
(3, 'zh', 'zh-sg', 'Chino', 'Chinese - Singapore', 0, 1),
(4, 'fr', 'fr-fr', 'Francés', 'French - France', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idiomas__claves`
--

CREATE TABLE `idiomas__claves` (
  `id` int(11) NOT NULL,
  `id_seccion` int(11) NOT NULL COMMENT 'sección',
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre identificativo según la sección',
  `type` varchar(20) COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'text'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `idiomas__claves`
--

INSERT INTO `idiomas__claves` (`id`, `id_seccion`, `nombre`, `type`) VALUES
(1, 1, 'texto', 'ckeditor'),
(2, 1, 'nombre', 'text'),
(3, 1, 'titulo', 'text'),
(4, 1, 'subtitulo', 'text'),
(5, 1, 'meta_titulo', 'text'),
(6, 1, 'meta_descripcion', 'text'),
(7, 2, 'texto', 'ckeditor'),
(8, 2, 'nombre', 'text'),
(9, 2, 'titulo', 'text'),
(11, 2, 'meta_titulo', 'text'),
(12, 2, 'meta_descripcion', 'text'),
(13, 5, '', 'ckeditor'),
(14, 5, 'nombre', 'text'),
(15, 5, 'titulo', 'text'),
(16, 5, 'subtitulo', 'text'),
(17, 5, 'meta_titulo', 'text'),
(18, 5, 'meta_descripcion', 'text'),
(19, 6, 'url', 'text'),
(20, 6, 'nombre', 'text'),
(21, 6, 'titulo', 'text'),
(22, 6, 'subtitulo', 'text'),
(23, 6, 'meta_titulo', 'text'),
(24, 6, 'meta_descripcion', 'text'),
(25, 9, 'url', 'text'),
(26, 9, 'nombre', 'text'),
(27, 9, 'titulo', 'text'),
(28, 9, 'subtitulo', 'text'),
(29, 9, 'meta_titulo', 'text'),
(30, 9, 'meta_descripcion', 'text'),
(32, 10, 'nombre', 'text'),
(33, 10, 'titulo', 'text'),
(34, 10, 'subtitulo', 'text'),
(35, 10, 'meta_titulo', 'text'),
(36, 10, 'meta_descripcion', 'text'),
(45, 9, 'bt-home', 'text'),
(46, 9, 'bt-objetivos', 'text'),
(47, 9, 'bt-organizacion', 'text'),
(48, 9, 'bt-historia', 'text'),
(49, 9, 'bt-contacto', 'text'),
(50, 9, 'bt-miembros', 'text'),
(51, 9, 'bt-actividades', 'text'),
(52, 9, 'bt-donacion', 'text'),
(53, 9, 'bt-noticias', 'text'),
(54, 9, 'bt-enlaces', 'text'),
(55, 9, 'bt-aportaciones', 'text'),
(56, 9, 'bt-aviso', 'text'),
(57, 9, 'bt-politica', 'text'),
(58, 9, 'bt-cookies', 'text'),
(59, 9, 'bt-asociacion', 'text'),
(60, 9, 'bt-asociacion-objetivos', 'text'),
(61, 9, 'bt-asociacion-organizacion', 'text'),
(62, 9, 'bt-asociacion-localizacion', 'text'),
(63, 9, 'bt-asociacion-estatutos', 'text'),
(64, 9, 'bt-asociarse', 'text'),
(66, 11, 'nombre-bt', 'text'),
(67, 11, 'objetivo', 'text'),
(68, 11, 'texto-intro', 'text'),
(69, 11, 'meta_titulo', 'text'),
(70, 11, 'meta_descripcion', 'text'),
(71, 9, 'objetivo-actual', 'text'),
(72, 9, 'quiero-donar-bt', 'text'),
(73, 1, 'numero-amigos', 'number'),
(74, 1, 'leyenda-ventajas', 'ckeditor'),
(75, 10, 'texto', 'ckeditor'),
(77, 9, 'noticiasmarcador', 'text'),
(78, 9, 'actividadesmarcador', 'text'),
(79, 2, 'subtitulo', 'text'),
(80, 12, 'texto', 'ckeditor'),
(81, 12, 'nombre', 'text'),
(82, 12, 'titulo', 'text'),
(83, 12, 'subtitulo', 'text'),
(84, 12, 'meta_titulo', 'text'),
(85, 12, 'meta_descripcion', 'text'),
(86, 9, 'ver_mas', 'text'),
(87, 13, 'texto', 'ckeditor'),
(88, 13, 'nombre', 'text'),
(89, 13, 'titulo', 'text'),
(90, 13, 'subtitulo', 'text'),
(91, 13, 'meta_titulo', 'text'),
(92, 13, 'meta_descripcion', 'text'),
(93, 14, 'url', 'text'),
(94, 14, 'nombre', 'text'),
(95, 14, 'titulo', 'text'),
(96, 14, 'subtitulo', 'text'),
(97, 14, 'meta_titulo', 'text'),
(98, 14, 'meta_descripcion', 'text'),
(100, 14, 'texto', 'ckeditor'),
(101, 15, 'url', 'text'),
(102, 15, 'nombre', 'text'),
(103, 15, 'titulo', 'text'),
(104, 15, 'subtitulo', 'text'),
(105, 15, 'meta_titulo', 'text'),
(106, 15, 'meta_descripcion', 'text'),
(107, 15, 'texto', 'ckeditor'),
(108, 16, 'url', 'text'),
(109, 16, 'nombre', 'text'),
(110, 16, 'titulo', 'text'),
(111, 16, 'subtitulo', 'text'),
(112, 16, 'meta_titulo', 'text'),
(113, 16, 'meta_descripcion', 'text'),
(114, 16, 'texto', 'ckeditor'),
(115, 17, 'url', 'text'),
(116, 17, 'nombre', 'text'),
(117, 17, 'titulo', 'text'),
(118, 17, 'subtitulo', 'text'),
(119, 17, 'meta_titulo', 'text'),
(120, 17, 'meta_descripcion', 'text'),
(121, 17, 'texto', 'ckeditor'),
(122, 5, 'texto', 'ckeditor'),
(123, 18, 'url', 'text'),
(124, 18, 'nombre', 'text'),
(125, 18, 'titulo', 'text'),
(126, 18, 'subtitulo', 'text'),
(127, 18, 'meta_titulo', 'text'),
(128, 18, 'meta_descripcion', 'text'),
(129, 18, 'texto', 'ckeditor'),
(142, 21, 'url', 'text'),
(143, 21, 'nombre', 'text'),
(144, 21, 'titulo', 'text'),
(145, 21, 'subtitulo', 'text'),
(146, 21, 'meta_titulo', 'text'),
(147, 21, 'meta_descripcion', 'text'),
(178, 21, 'texto', 'ckeditor'),
(179, 27, 'url', 'text'),
(180, 27, 'nombre', 'text'),
(181, 27, 'titulo', 'text'),
(182, 27, 'subtitulo', 'text'),
(183, 27, 'meta_titulo', 'text'),
(184, 27, 'meta_descripcion', 'text'),
(185, 27, 'texto', 'ckeditor'),
(186, 28, 'url', 'text'),
(187, 28, 'nombre', 'text'),
(188, 28, 'titulo', 'text'),
(189, 28, 'subtitulo', 'text'),
(190, 28, 'meta_titulo', 'text'),
(191, 28, 'meta_descripcion', 'text'),
(192, 28, 'texto', 'ckeditor'),
(193, 29, 'url', 'text'),
(194, 29, 'nombre', 'text'),
(195, 29, 'titulo', 'text'),
(196, 29, 'subtitulo', 'text'),
(197, 29, 'meta_titulo', 'text'),
(198, 29, 'meta_descripcion', 'text'),
(199, 29, 'texto', 'ckeditor'),
(200, 30, 'url', 'text'),
(201, 30, 'nombre', 'text'),
(202, 30, 'titulo', 'text'),
(203, 30, 'subtitulo', 'text'),
(204, 30, 'meta_titulo', 'text'),
(205, 30, 'meta_descripcion', 'text'),
(206, 30, 'texto', 'ckeditor'),
(207, 11, 'titulo', 'text'),
(208, 11, 'subtitulo', 'text'),
(209, 11, 'texto', 'ckeditor'),
(210, 11, 'titulo-formulario-donacion', 'text'),
(211, 11, 'texto-formulario-donacion', 'ckeditor'),
(212, 11, 'info-donacion', 'ckeditor'),
(213, 16, 'texto-aviso', 'ckeditor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idiomas__secciones`
--

CREATE TABLE `idiomas__secciones` (
  `id` int(11) NOT NULL,
  `slug` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(200) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `logo` varchar(200) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `position` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `oculto` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `idiomas__secciones`
--

INSERT INTO `idiomas__secciones` (`id`, `slug`, `nombre`, `logo`, `position`, `parent_id`, `oculto`) VALUES
(1, 'home', 'Home', '', -1, 0, 0),
(2, 'asociacion', 'Asociacion', '', -2, 0, 1),
(5, 'noticias', 'Noticias', '', -4, 0, 1),
(6, 'contacto', 'Contacto', '', -5, 0, 0),
(9, 'comunes', 'Comunes', '', -6, 0, 1),
(10, 'historia', 'Historia', 'star', -3, 0, 1),
(11, 'mecenazgo', 'Mecenazgo', '', 0, 0, 0),
(12, 'objetivos', 'Objetivos', '', 0, 0, 1),
(13, 'miembros', 'Miembros', '', 0, 0, 1),
(14, 'organizacion', 'Organizacion', '', 0, 0, 0),
(15, 'localizacion', 'Localizacion', '', 0, 0, 0),
(16, 'asociarse', 'Asociarse', '', 0, 0, 1),
(17, 'estatutos', 'Estatutos', '', 0, 0, 0),
(18, 'actividades', 'Actividades', '', 0, 0, 1),
(21, 'donacion-y-restauracion', 'DonacionYRestauracion', '', 0, 0, 0),
(27, 'enlaces', 'Enlaces', '', 0, 0, 0),
(28, 'aviso-legal', 'AvisoLegal', '', 0, 0, 1),
(29, 'politica-de-privacidad', 'PoliticaDePrivacidad', '', 0, 0, 1),
(30, 'cookies', 'Cookies', '', 0, 0, 1);

--
-- Disparadores `idiomas__secciones`
--
DELIMITER $$
CREATE TRIGGER `insertar_claves_comunes` AFTER INSERT ON `idiomas__secciones` FOR EACH ROW INSERT INTO idiomas__claves (`id_seccion`, `nombre`)
VALUES
		(NEW.id, 'url'),
		(NEW.id, 'nombre'),
		(NEW.id, 'titulo'),
		(NEW.id, 'subtitulo'),
		(NEW.id, 'meta_titulo'),
		(NEW.id, 'meta_descripcion')
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idiomas__textos`
--

CREATE TABLE `idiomas__textos` (
  `id` int(11) NOT NULL,
  `id_clave` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `valor` text COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `idiomas__textos`
--

INSERT INTO `idiomas__textos` (`id`, `id_clave`, `id_idioma`, `valor`) VALUES
(1, 1, 2, '<p>Desde la Asociación de Amigos del Museo Nacional de Escultura queremos darte la bienvenida a este espacio de intercambio cultural.</p>\r\n<h3><strong>Considerate en tu casa</strong></h3>\r\n<p>Esta web, junto a nuestra asociación, busca acercar el <a href=\"http://www.mecd.gob.es/mnescultura/inicio.html\" target=\"_blank\">Museo Nacional de Escultura</a> al visitante no especializado, hacérle partícipe de nuestras actividades, exposiciones y eventos y contribuir a ampliar la oferta cultural de la ciudad de Valladolid.</p>'),
(2, 2, 2, 'Incio'),
(3, 18, 1, 'Here meta tag'),
(4, 18, 2, 'Esta es la meta'),
(5, 18, 3, 'CHINO'),
(6, 18, 4, 'Çe est le meta'),
(7, 3, 1, 'Hey!'),
(8, 3, 2, '¡Bienvenido!'),
(9, 3, 3, 'CHINO'),
(10, 3, 4, 'Salut!'),
(11, 2, 1, 'Home'),
(12, 2, 3, 'CHINO'),
(13, 2, 4, 'SALUT'),
(14, 36, 1, 'Meta in inglés'),
(15, 36, 2, 'Meta en español'),
(16, 36, 3, 'CHINO'),
(17, 36, 4, 'Le Meta en Français'),
(18, 5, 1, 'iuhnuh'),
(19, 5, 2, 'trct'),
(28, 45, 1, 'Home'),
(29, 45, 2, 'Inicio'),
(30, 46, 1, 'Objetivos en'),
(31, 46, 2, 'Objetivos'),
(32, 47, 1, 'organización en'),
(33, 47, 2, 'Organización'),
(34, 48, 1, 'History'),
(35, 48, 2, 'Historia'),
(36, 49, 1, 'Contact'),
(37, 49, 2, 'Contacto'),
(38, 26, 1, ''),
(39, 26, 2, 'Hola Amigos del museo'),
(40, 50, 1, 'Members'),
(41, 50, 2, 'Miembros'),
(42, 51, 1, 'Activities'),
(43, 51, 2, 'Actividades'),
(44, 52, 1, 'Donacion en'),
(45, 52, 2, 'Donación y restauración'),
(46, 53, 1, 'News'),
(47, 53, 2, 'Noticias'),
(48, 54, 1, 'Links'),
(49, 54, 2, 'Enlaces'),
(50, 55, 1, 'Aportaciones en'),
(51, 55, 2, 'Aportaciones'),
(52, 56, 1, 'Legal advice'),
(53, 56, 2, 'Aviso legal'),
(54, 57, 1, 'Privace policy'),
(55, 57, 2, 'Política de privacidad'),
(56, 58, 1, 'Cookies'),
(57, 58, 2, 'Política de cookies'),
(58, 59, 1, 'Asociacion en'),
(59, 59, 2, 'Asociación'),
(60, 60, 1, 'Objetivos en'),
(61, 60, 2, 'Objetivos'),
(62, 61, 1, 'organización en'),
(63, 61, 2, 'Organización'),
(64, 62, 1, 'Location'),
(65, 62, 2, 'Localización'),
(66, 63, 1, 'Estatutos en'),
(67, 63, 2, 'Estatutos'),
(68, 64, 1, 'Asociarse en'),
(69, 64, 2, 'Asociarse'),
(70, 66, 1, 'Micromecenazgo en'),
(71, 66, 2, 'Micromecenazgo'),
(72, 68, 1, 'dsfsdfsdfsdfsdfegrdfgesfdged'),
(73, 68, 2, 'La Asociación Amigos del Museo de Escultura contempla un plan de micromecenazgo para que cualquier particular o empresa participe'),
(74, 67, 1, 'Restauration'),
(75, 67, 2, 'Restauración del Retablo de la Virgen lorem ipsum (1612)'),
(76, 71, 1, 'current target'),
(77, 71, 2, 'objetivo actual'),
(78, 72, 1, 'I want to donate'),
(79, 72, 2, 'quiero donar'),
(80, 73, 1, '200'),
(81, 73, 2, '348'),
(82, 74, 1, '<p>dfasdfqwdsfc</p>'),
(83, 74, 2, '<p>Ser Amigo del Museo Nacional de Escultura está lleno de <a href=\"#/miembros\" target=\"_self\">ventajas</a></p>'),
(84, 33, 1, 'History of the museum'),
(85, 33, 2, 'Historia del Museo'),
(86, 34, 1, 'Breve... en'),
(87, 34, 2, 'Breve historia de nuestro museo'),
(88, 75, 1, '<p>ds</p>'),
(89, 75, 2, '<h3>El mueso nacional colegio nacional de escultura</h3>\r\n<p>La creación del <a href=\"http://www.mecd.gob.es/mnescultura/inicio.html\" target=\"_blank\"><strong>Museo Nacional de Escultura</strong></a> se remonta al año 1933. En la primera sede elegida para su ubicación, el Colegio San Gregorio y la capilla funeraria de su fundador, Fray Alonso de Burgos, se logró reunir e instalar un importante conjunto de esculturas y pinturas que, procedentes del antiguo <strong>Museo Provincial de Bellas Artes</strong>, constituyeron su núcleo inicial.</p>\r\n<p>En las siete decadas transcurridas desde entonces, han sido mucho los cambios que la institución ha debido asumir. El importante crecimiento de sus colecciones, unas mayores exigencias en el cumplimiento de las funciones de conservación e investigación, un fuerte incremento de su actividad cultural, la necesidad de nuevos servicios de atención al público... han obligado a ampliar su superficie con edificios adyacentes (Palacio de Villena, Casa del Sol...) y a abordar un programa de renovación de infraestructuras y mejora de instalaciones museográficas que actualmente está siendo aplicado a su sede principal, el Colegio San Gregorio.</p>\r\n<p><img alt=\"\" src=\"http://imac.local:5757/files/ckeditor/3cfda62.jpg\" style=\"height:753px; width:500px\" /></p>\r\n<blockquote>\r\n<p>Imagen de la fachada</p></blockquote>\r\n<h3>La colección</h3>\r\n<p>La colección del Museo Nacional de Escultura está integrada por <strong>un extraordinario conjunto de escultura y pintura de los siglos XIII al XIX</strong> que permite apreciar la <strong>evolución formal de la plástica hispana a lo largo de los siglos Gótico, Renacimiento, Manierismo, Barroco y Rococó</strong>.</p>\r\n<p>Los ricos fondos que la componen son una muestra elocuente de la calidad que alcanzaron las formas artísticas en nuestro país, expresión viva de un complejo entramado de relaciones internacionales, de circulación de influencias y artistas geniales que dieron como resultado un magnífico conjunto patrimonial.</p>\r\n<p>El núcleo primitivo de esta colección procede de los conventos desamortizados de Valladolid y su entorno. La transcendencia histórica de la ciudad en épocas pasadas, está en el origen de la importancia de las creaciones artísticas atesoradas por esta vía, producto de una larga tradición de mecenazgo y protección de las artes. A estas aportaciones hay que añadir <strong>donaciones, depósitos </strong>y, sobre todo, <strong>adquisiciones del Estado</strong> orientadas a completar una visión global de la escultura española con todos sus matices, dentro de unos límites geográficos que exceden los peninsulares.</p>\r\n<p><img alt=\"\" src=\"http://imac.local:5757/files/ckeditor/4511583.jpg\" style=\"height:333px; width:500px\" /></p>\r\n<p>Retablos, sillerías, monumentos funerarios, pasos procesionales... son algunas de las tipologías a las que pertenecían en origen este importante conjunto de obras. Los variados materiales en los que fueron realizadas (barro, piedra, bronce y, sobre todo, madera policromada) se nos presentan hoy como privilegiados vehículos de las ideas, sentimientos y emociones de la sociedad en la que se gestaron.</p>\r\n<p>Las peculiaridades de la escultura en cuanto a tipologías (retablos, sillerías corales o escultura funeraria) y también en lo que se refiere a materiales (madera, bronce, piedra, marmol o marfil), alcanzan en España uno de sus puntos álgidos con el uso de la madera policromada, una auténtica síntesis de las artes, de volumen y color, de esfuerzo por lograr mayor verismo y transmitir auténticas sensaciones.</p>\r\n<p>La secuencia cronológica permite conocer lo que supuso en cada periodo, la evolución de los géneros artísticos como reflejo de la sociedad en la que se creó. Importación, comercio, talleres locales y corrientes estílisticas dieron lugar a formas singulares llenas de interés.</p>'),
(92, 77, 1, 'news'),
(93, 77, 2, 'noticias'),
(94, 78, 1, 'actividadaes'),
(95, 78, 2, 'actividades'),
(96, 9, 1, ''),
(97, 9, 2, 'Asociación de Amigos del Museo Nacional de Escultura'),
(98, 79, 1, ''),
(99, 79, 2, 'Acercando el museo a la gente'),
(100, 7, 1, ''),
(101, 7, 2, '<p><strong>El MUSEO NACIONAL DE ESCULTURA</strong> representa una de las principales instituciones culturales de nuestro país y, atendiendo a la importancia de sus colecciones, una de las más relevantes a nivel mundial.</p>\r\n<p>La Asociación tiene como finalidad principal el apoyo al Museo bien entendido que la palabra “apoyo” no presenta, en este caso, ningún signo de pasividad ya que lo que <strong>logramos</strong> con nuestra actividad es <strong>llevar la voz de la persona no especializada</strong>, de la sociedad <strong>y hacerla oír en los museos</strong>.</p>\r\n<p>A lo largo de los años, el trabajo esforzado y las ideas de todos ellos han producido en los propios museos una transformación, acercándolos a la gente y acercando la gente a los museos.</p>\r\n<p>La Asociación no persigue otra finalidad que <strong>favorecer a sus socios el conocimiento preferente de todas las muestras culturales</strong> que pueda desarrollar o albergar el Museo, así como <strong>llevar a cabo su propio programa cultural </strong>que estará en sintonía con el del propio Museo, ya que es lo que constituye nuestra razón de ser.</p>\r\n<h3><strong>Te necesitamos</strong></h3>\r\n<p>Un proyecto de estas características necesita, evidentemente, del apoyo de la Sociedad a la que sirve. Por eso, al igual que otras instituciones de este género en todo el mundo, nos parece necesario invitar a particulares y empresas a participar en su desarrollo.</p>\r\n<p>A través de los Amigos del Museo nos gustaría que, todos los amantes del arte y la cultura, pudieran vincularse a nuestra tarea, lanzando con esta línea, un llamamiento a la sociedad para seguir en este empeño que sólo será posible con la colaboración y participación entusiasta de todos.</p>'),
(102, 80, 1, ''),
(103, 80, 2, '<p>Desde su inicio, la Asociación ha venido desarrollando, gracias a la colaboración y esfuerzo de todos los Amigos, una importante labor de consolidación y afianzamiento en la sociedad consiguiendo hacerse un hueco en la actividad cultural de nuestra ciudad.</p>\r\n<p>Así, afianzados, la Asociación tiene previsto potenciar su actividad con la finalidad de contribuir a la difusión de las colecciones del <a href=\"http://www.mecd.gob.es/mnescultura\" target=\"_blank\">Museo Nacional de Escultura</a>.&nbsp;</p>\r\n<h3><strong>Nuestros objetivos se centran</strong></h3>\r\n<p>Sus objetivos se centran, por un lado, en consolidar las líneas de trabajo que se han desarrollado hasta la fecha y, por otro, en introducir nuevas propuestas que contribuyan a la consecución de los fines de la misma.</p>\r\n<p>En la actualidad, el número de socios ha aumentado considerablemente, lo cual nos llena de satisfacción ya que siempre es muy agradable encontrar una buena respuesta de implicación de la sociedad en la conservación y promoción de nuestro patrimonio del cual es una muestra incomparable el Museo Nacional de Escultura.</p>\r\n<p>La Asociación no persigue otra finalidad que acercar a sus socios el conocimiento preferente de todas las muestras culturales que pueda generar o albergar el Museo, así como desarrollar su propio programa cultural que siempre estará en sintonía con el del Museo ya que ello es lo que constituye nuestra razón de ser.</p>\r\n<h3><strong>¿Quieres formar parte de la asociación?</strong></h3>\r\n<p>Ser parte de Amigos del Museo Nacional de Escultura está lleno de <a href=\"/#miembros/\" target=\"_self\">ventajas</a>.</p>\r\n<p>&nbsp;</p>'),
(104, 82, 1, 'Objetivos de la Asociación EN'),
(105, 82, 2, 'Objetivos de la Asociación'),
(106, 83, 1, 'Acercando el museo a la gente'),
(107, 83, 2, 'Acercando el museo a la gente'),
(108, 86, 1, ''),
(109, 86, 2, 'ver más'),
(110, 1, 1, ''),
(111, 89, 1, ''),
(112, 89, 2, 'Ventajas de ser socio'),
(113, 90, 1, 'fdter'),
(114, 90, 2, 'Ventajas exclusivas para nuestros miembros'),
(115, 87, 1, ''),
(116, 87, 2, '<p>Existen diferentes membresías para nuestra asociación.</p>\r\n<h3>Amigos</h3>\r\n<p><strong>Aportaciones</strong>:</p>\r\n<ul>\r\n	<li>A partir de 40€/año</li>\r\n	<li>A partir de 18€/año para menores de 25 años</li>\r\n</ul>\r\n<p><strong>Beneficios:</strong></p>\r\n<ol>\r\n	<li>Entrada gratuita al Museo con un acompañante</li>\r\n	<li>Entrada gratuita a otros museos estatales</li>\r\n	<li>Invitación y visitas guuiadas exclusivas para los Amigos</li>\r\n	<li>Invitación a las conferencias del Museo</li>\r\n	<li>Información sobre actividades del Museo y de la asociación</li>\r\n	<li>Descuento del 25% en publicaciones editadas por la Asociación</li>\r\n	<li>Descuento del 10% en artículos de la tienda y otras publicaciones.</li>\r\n	<li>Descuento en actividades que requieran pago</li>\r\n	<li>Desgravación fiscal</li>\r\n</ol>\r\n<h3><strong>Amigos protectores</strong></h3>\r\n<p><strong>Aportaciones</strong></p>\r\n<p>A partir de 300€/año</p>\r\n<p><strong>Beneficios (extra sobre los amigos)</strong></p>\r\n<ol>\r\n	<li>Catálogos de la Asociación gratuitos</li>\r\n	<li>Seis carnés de iguales beneficios</li>\r\n</ol>\r\n<h3><strong>Protectores empresariales o institucionales</strong></h3>\r\n<p><strong>Aportaciones</strong></p>\r\n<p>A partir de 900€/año</p>\r\n<p><strong>Beneficios</strong>&nbsp;</p>\r\n<ol>\r\n	<li>Carné personalizado a la persona designada</li>\r\n	<li>Diez carnés de amigo protector con iguales beneficios</li>\r\n	<li>Reconocimiento de esta colaboración en la memoria y difusión publicitarioa anual de la Asociación.</li>\r\n	<li>Prioridad de uso de espacios del Museo para celebrar recepciones especiales o visitas privadas, que deberán contar con la oportuna autorización del Ministerio de Cultura</li>\r\n</ol>\r\n<p>&nbsp;</p>'),
(117, 94, 1, 'Organization'),
(118, 94, 2, 'Organización'),
(119, 95, 1, 'Organization'),
(120, 95, 2, 'Organización'),
(121, 96, 1, 'Organización cultural sin ánimo de lucro'),
(122, 96, 2, 'Organización cultural sin ánimo de lucro'),
(123, 100, 1, ''),
(124, 100, 2, '<p>La Asociación se constituye como una organización cultural sin ánimo de lucro, de ámbito nacional en torno al <a href=\"http://www.mecd.gob.es/mnescultura/inicio.html\" target=\"_blank\">Museo Nacional de Escultura</a> de Valladolid.</p>\r\n<h3><strong>Estatutos y órganos de gobierno</strong></h3>\r\n<p><strong>Asamblea general</strong>: Órgano supremo de la Asociación que está integrada por todas las personas asociadas.</p>\r\n<p><strong>Junta directiva</strong>, actualmente compuesta por:</p>\r\n<ol>\r\n	<li><strong>Presidentea</strong>: D. Eduardo de Mata Trapote</li>\r\n	<li><strong>Vicepresidenta</strong>: Dña Mar Álvarez Rueda</li>\r\n	<li><strong>Secretario</strong>: Dña Ruth Domínguez Fernández</li>\r\n	<li><strong>Tesorero</strong>: D. Juan Carlos de Margarida Sanz</li>\r\n	<li><strong>Vocales</strong>: Dña Dolores Benavides Agundez, Dña Leonor Rodríguez Rodríguez, D. José Manuel San Miguel Monteira</li>\r\n</ol>'),
(125, 103, 1, 'Location'),
(126, 103, 2, 'Localización'),
(127, 104, 1, ''),
(128, 104, 2, ''),
(129, 107, 1, ''),
(130, 107, 2, '<p>La Asociación de Amigos del Museo Nacional de Escultura tiene su sede en el propio <a href=\"http://www.mecd.gob.es/mnescultura/inicio.html\" target=\"_blank\">Museo Nacional de Escultura</a>, sito en <strong>Calle Cadenas de San Gregorio, 1.2</strong></p>\r\n<p><strong><img alt=\"\" src=\"http://imac.local:5757/files/ckeditor/5745c86.jpg\" /></strong></p>\r\n<h3><strong>Horarios de atención al público</strong></h3>\r\n<p><strong>De martes a sábado</strong>: de 10h a 14h y de 16h a 19:30h</p>\r\n<p><strong>Domingos y festivos</strong>: de 10h a 14h</p>\r\n<p>Cerrado todos los lunes del año y los festivos:</p>\r\n<ul>\r\n	<li>1 y 6 de enero</li>\r\n	<li>1 de mayo</li>\r\n	<li>8 de septiembre</li>\r\n	<li>24, 25 y 31 de diciembre</li>\r\n</ul>'),
(131, 110, 1, 'Asociarse'),
(132, 110, 2, 'Asociarse'),
(133, 114, 1, ''),
(134, 114, 2, '<p>Si después de haber leído <a href=\"/#estatutos/\">nuestros estatutos</a> desea asociarse, le proponemos dos posibles formas:</p>\r\n<h3><strong>Presencial</strong></h3>\r\n<p>En el propio Museo, en el horario indicado puede hacer efectiva su inscripción.</p>\r\n<h3><strong>Online</strong></h3>\r\n<p>Si lo desea, puede rellenar el siguiente formulario de inscripción con el que procederemos a domiciliar el pago de su cuota. &nbsp;En ambos casos, en el menor tiempo posible, recibirá su carnet de Amigo con el que podrá disfrutar de <a href=\"/#miembros/\">todas las ventanjas</a> durante un año.</p>'),
(135, 117, 1, ''),
(136, 117, 2, 'Nuestros estatutos'),
(137, 121, 1, ''),
(138, 121, 2, '<h3><strong>OBJETO DE LA ASOCIACIÓN </strong></h3>\r\n<p><strong>Artículo 1.- </strong>Con la denominación de ASOCIACIÓN AMIGOS DEL MUSEO NACIONAL DE ESCULTURA, se constituye una entidad sin ánimo de lucro, con capacidad jurídica y plena capacidad de obrar, que se propone agrupar a la sociedad civil a través de todos los amantes y simpatizantes de nuestro Museo y de toda clase de manifestaciones artísticas.</p>\r\n<p>El Régimen de la entidad está constituido por los presentes estatutos y los acuerdos válidamente adoptados por su Asamblea General y órganos directivos, dentro de la esfera de su respectiva competencia. En lo no previsto se regirá por la Ley Orgánica 1/2002, de 22 de marzo, reguladora del derecho de asociación, y normas complementarias,</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 2.- </strong>Los fines de esta Asociación son los siguientes:</p>\r\n<p>a) Apoyar la gestión y labor del Museo Nacional de Escultura y de las Secciones y Colecciones a él vinculadas.</p>\r\n<p>b) Ofrecer su concurso moral y material para el mejor desenvolvimiento de la actividad del Museo y su proyección en la sociedad.</p>\r\n<p>c) Ofrecer el concurso de sus socios para que, gratuitamente, puedan prestar su colaboración en cuantas actividades programe el Museo.</p>\r\n<p>d) Apoyar toda iniciativa que tienda a velar por la conservación de la riqueza artística de España y procurar la relación constante con todos aquellos organismos similares, nacionales y extranjeros.</p>\r\n<p>&nbsp;</p>\r\n<p>Para el cumplimiento de estos fines la Asociación podrá realizar cuantas actividades, incluidas, en su caso, las económicas, conduzcan directa o indirectamente a su consecución, sin que, en ningún caso, la realización de actividades mercantiles pueda constituir su actividad principal.</p>\r\n<p>A titulo enunciativo y no limitativo, pueden señalarse las siguientes actividades:</p>\r\n<p>a) Educativas, para dar a conocer la historia, los edificios y las colecciones del Museo.</p>\r\n<p>b) Expositivas, para dar mayor difusión a los fondos del Museo, en sí mismos, o en relación con otras obras cuya exposición conjunta permita conocer y valorar mejor las del Museo.</p>\r\n<p>c) Editoriales, mediante la publicación, en cualquier soporte, de obras que tengan por objetos el estudio, la divulgación o el apoyo al Museo, previa la oportuna autorización, si se requiere, por parte del Ministerio de Cultura.</p>\r\n<p>Si se obtuvieren beneficios económicos derivados de las actividades, deberán ser destinados exclusivamente al cumplimiento de los fines de la Asociación, sin que quepa el reparto de los mismos entre los asociados, familiares de los mismos o relacionados, como tampoco la cesión gratuita a personas físicas o jurídicas que tengan ánimo de lucro.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 3.- </strong>Mientras la Asamblea General convocada específicamente con tal objeto, no acuerde lo contrario, el domicilio de la Asociación radicará en el Museo Nacional de Escultura, calle Cadenas de San Gregorio n.º 1-2, en Valladolid, en locales cedidos para este solo fin por la dirección del centro.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 4.- </strong>El ámbito territorial de acción de la Asociación se extiende a todo el territorio nacional y, en su caso, al extranjero.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 5.- </strong>La duración de la Asociación será indefinida y sólo se disolverá por acuerdo de la Asamblea General Extraordinaria o por cualquiera de las causas previstas en las leyes.</p>\r\n<p>&nbsp;</p>\r\n<h3><strong>GOBIERNO DE LA ASOCIACIÓN: ÓRGANOS DIRECTIVOS Y FORMAS DE ADMINISTRACIÓN</strong></h3>\r\n<p><strong>Artículo 6.- </strong>La dirección y administración de la Asociación será ejercida por el Presidente, la Junta Directiva y la Asamblea General de Socios.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 7.- </strong>La Junta Directiva estará formada por un Presidente, un Vicepresidente, un Secretario, un Vicesecretario, un Tesorero, un Vicetesorero y dos vocales, cargos que deberán recaer en socios activos y elegidos por votación.</p>\r\n<p>La Junta Directiva celebrará sesiones a iniciativa del presidente o por petición de la mitad de sus miembros, quedando constituida cuando asista la mitad de los integrantes de la misma.</p>\r\n<p>A criterio de la Junta Directiva, se establece la posibilidad de proponer a la Asamblea General el nombramiento de una Presidencia honorífica.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 8.- </strong>Los cargos que componen la Junta Directiva, serán gratuitos y se elegirán por la Asamblea General. La composición de dicha Junta Directiva se renovará al menos cada cuatro años. Los componentes de la Junta saliente podrán presentarse a la reelección si así lo desean.</p>\r\n<p>Los miembros de la Junta Directiva que hubieran agotado el plazo para el cual fueron elegidos, continuarán ostentando sus cargos hasta el momento en que se produzca la aceptación de los que les sustituyan.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 9.- </strong>Facultades de la Junta Directiva:</p>\r\n<p>1.- Las facultades de la Junta Directiva se extenderán, con carácter general, a todos los actos propios de las finalidades de la Asociación, siempre que no requieran, según estos Estatutos, autorización expresa de la Asamblea General.</p>\r\n<p>&nbsp;</p>\r\n<p>2.- Son facultades particulares de la Junta Directiva:</p>\r\n<p>a) Dirigir las actividades sociales y culturales, así como llevar la gestión económica y administrativa de la Asociación.</p>\r\n<p>b) Ejecutar los acuerdos de la Asamblea General.</p>\r\n<p>c) Formular y someter a la aprobación de la Asamblea General los Balances y las Cuentas anuales.</p>\r\n<p>d) Resolver sobre la admisión de nuevos asociados.</p>\r\n<p>e) Nombrar delegados para alguna determinada actividad de la Asociación.</p>\r\n<p>f) Cualquier otra facultad que no sea de exclusiva competencia de la Asamblea General de socios.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 10.- </strong>El Presidente de la Asociación tendrá las siguientes facultades:</p>\r\n<p>a) Representar legalmente a la Asociación ante toda clase de organismos públicos o privados.</p>\r\n<p>b) Convocar, presidir y levantar las sesiones que celebre la Asamblea General y la Junta Directiva.</p>\r\n<p>c) Dirigir las deliberaciones de una y otra.</p>\r\n<p>d) Ordenar pagos y autorizar con su firma los documentos, actas y correspondencia.</p>\r\n<p>e) Adoptar cualquier medida urgente que la buena marcha de la Asociación aconseje o en el desarrollo de sus actividades resulte necesaria o conveniente, sin perjuicio de dar cuenta posteriormente a la Junta Directiva.</p>\r\n<p>El Vicepresidente sustituirá al Presidente en ausencia de éste, motivada por enfermedad, vacante o cualquier otra causa, y tendrá las mismas atribuciones que él; teniendo las facultades que el presidente le delegue o establezca para él la Asamblea General.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 11.- </strong>El Secretario tendrá a su cargo:</p>\r\n<p>a) La dirección de los trabajos puramente administrativos de la Asociación.</p>\r\n<p>b) Expedir certificaciones.</p>\r\n<p>c) Levantar actas de las Asambleas y de las reuniones de la Junta Directiva.</p>\r\n<p>d) Llevar los libros de la Asociación que sean legalmente establecidos y el fichero de asociados.</p>\r\n<p>e) Custodiar la documentación de la entidad.</p>\r\n<p>f) Hacer que se cursen a los Registros correspondientes las comunicaciones sobre designación de Juntas Directivas y demás acuerdos sociales inscribibles, así como que se presenten las cuentas anuales y que se cumplan las obligaciones documentales en los términos que legalmente correspondan.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 12.- </strong>Son funciones del Tesorero:</p>\r\n<p>a) Recaudar y custodiar los fondos pertenecientes a la Asociación.</p>\r\n<p>b) Dar cumplimiento a las órdenes de pago que expida el Presidente.</p>\r\n<p>c) Elaborar y presentar las cuentas anuales de la Asociación y cumplir con las obligaciones fiscales de la misma.</p>\r\n<p>d) Llevar inventario de los bienes, si los hubiera.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 13.- </strong>Los Vocales tendrán las obligaciones propias de su cargo como miembros de la Junta Directiva, así como las que nazcan de las delegaciones o comisiones de trabajo que la propia Junta las encomiende.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 14.- </strong>Las vacantes que se pudieran producir durante el mandato de cualquiera de los miembros de la Junta Directiva serán cubiertas provisionalmente entre dichos miembros hasta la elección definitiva por la Asamblea General Extraordinaria.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 15.- </strong>Para que los acuerdos de la Junta Directiva sean válidos deberán adoptarse por mayoría de votos de los asistentes, siendo el Secretario el encargado de levantar acta, que se transcribirá en el libro de Actas o Acuerdos, de conformidad con el art. 11.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 16.- </strong>La Asamblea General es el órgano supremo de gobierno de la Asociación y estará integrada por todos los asociados.</p>\r\n<p>Las reuniones de la Asamblea General serán ordinarias y extraordinarias. La ordinaria se celebrará una vez al año dentro de los cuatro meses siguientes al cierre del ejercicio; las extraordinarias se celebrarán cuando las circunstancias lo aconsejen, a juicio del Presidente, cuando la Directiva lo acuerde o cuando lo proponga por escrito una décima parte de los asociados.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 17.- </strong>Las convocatorias de las Asambleas Generales se realizarán por escrito que señale el lugar, día y hora de la reunión, así como el orden del día con expresión concreta de los asuntos a tratar. Entre la convocatoria y el día señalado para la celebración de la Asamblea en primera convocatoria habrán de mediar al menos quince días, pudiendo así mismo hacerse constar, si procediere, la fecha y hora en que se reunirá la Asamblea en segunda convocatoria, sin que entre una y otra pueda mediar un plazo inferior a una hora.</p>\r\n<p>Las Asambleas Generales, tanto ordinarias como extraordinarias, quedarán válidamente constituidas en primera convocatoria cuando concurran a ella un tercio de los asociados con derecho a voto, y en segunda convocatoria cualquiera que sea el número de asociados con derecho a voto.</p>\r\n<p>Los acuerdos se tomarán por mayoría simple de las personas presentes, o representadas mediante autorización escrita; es decir, cuando los votos afirmativos superen a los negativos, no siendo computables a estos efectos los votos en blanco ni las abstenciones.</p>\r\n<p>&nbsp;</p>\r\n<p>Será necesaria mayoría cualificada de las personas presentes o representadas, que resultará cuando los votos afirmativos superen la mitad de éstas, para:</p>\r\n<p>a) Nombramiento de las Juntas directivas y administradores.</p>\r\n<p>b) Acuerdo para constituir una Federación de Asociaciones o integrarse en ella.</p>\r\n<p>c) Disposición o enajenación de bienes integrantes del inmovilizado.</p>\r\n<p>d) Modificación de los estatutos.</p>\r\n<p>e) Disolución de la entidad.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 18.- </strong>Son facultades de la Asamblea General Ordinaria:</p>\r\n<p>a) Aprobar, en su caso, la gestión de la Junta Directiva.</p>\r\n<p>b) Examinar y aprobar las Cuentas anuales.</p>\r\n<p>c) Aprobar o rechazar las propuestas de la Junta Directiva en orden a las actividades de la Asociación.</p>\r\n<p>d) Fijar las cuotas ordinarias o extraordinarias y la cuantía y periodicidad de las mismas, así como acordar los gastos que deben atenderse con las cuotas extraordinarias.</p>\r\n<p>e) Cualquiera otra que no sea de la competencia exclusiva de la Asamblea Extraordinaria.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Corresponde a la Asamblea General Extraordinaria: </strong></p>\r\n<p>a) Nombramiento de los miembros de la Junta Directiva.</p>\r\n<p>b) Modificación de los Estatutos.</p>\r\n<p>c) Disolución de la Asociación.</p>\r\n<p>d) Expulsión de socios, a propuesta de la Junta Directiva.</p>\r\n<p>e) Constitución de Federaciones o integración en ellas.</p>\r\n<p>f) Solicitud de declaración de utilidad pública.</p>\r\n<p>&nbsp;</p>\r\n<h3><strong>SOCIOS.- DERECHOS Y DEBERES</strong></h3>\r\n<p><strong>Artículo 19.- </strong>Podrán pertenecer a la Asociación aquellas personas con capacidad de obrar que tengan interés en el desarrollo de los fines de la Asociación. Las solicitudes deberán realizarse por escrito en que conste la manifestación de voluntad de asociarse y de acatar los estatutos y las disposiciones que rijan en cada momento.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 20.- </strong>Las categorías de socios serán: Amigos, Protectores y Protectores Empresariales o Institucionales.</p>\r\n<p>Tendrá categoría de “Amigo” toda persona que solicite su ingreso y contribuya a los fines de la Asociación con la aportación que se establezca como mínima.</p>\r\n<p>Con el fin de fomentar el conocimiento del Museo por parte de los más jóvenes, se crea un apartado para “Amigos menores de 25 años” cuya aportación será inferior a la mínima establecida, aunque gozarán de los mismos derechos.</p>\r\n<p>Serán “Socios Protectores” quienes contribuyan al sostenimiento de la Asociación con una aportación superior a la que se estipule como mínima.</p>\r\n<p>“Protectores empresariales o Institucionales” son aquellas empresas o entidades (también puede existir la figura a nivel individual) que aporten a la Asociación una cuota más elevada o contribuyan con su apoyo en ciertas ayudas concretas que, para el beneficio del Museo o de la Asociación, puedan ofrecer.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 21.- </strong>Los socios causarán baja por alguna de las causas siguientes:</p>\r\n<p>a) Por renuncia voluntaria, comunicada por escrito a la Junta Directiva.</p>\r\n<p>b) Por incumplimiento de las obligaciones económicas y de socio.</p>\r\n<p>c) Por conducta contraria a la buena convivencia y a los fines de la Asociación.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 22.- </strong>Todos los socios tendrán derecho a:</p>\r\n<p>a) Participar en las actividades de la Asociación y de sus órganos de gobierno, ejerciendo el derecho de voto.</p>\r\n<p>b) Audiencia previa respecto de la adopción de medidas disciplinarias contra los mismos.</p>\r\n<p>c) Impugnar los acuerdos de los órganos de la Asociación.</p>\r\n<p>d) Acceso a toda la documentación de la Asociación, a través de los órganos de representación, en los términos previstos en la ley orgánica 15/1999, de protección de datos de carácter personal.</p>\r\n<p>e) Entrada gratuita al Museo para el titular del carné y un acompañante.</p>\r\n<p>f) Invitación a las inauguraciones o visitas de preinauguración a las exposiciones del Museo.</p>\r\n<p>g) Invitación a las conferencias del Museo.</p>\r\n<p>h) Información sobre las actividades organizadas por la Asociación.</p>\r\n<p>i) Descuento en el pago de las actividades que no sean gratuitas (cursos, concier-tos, viajes…).</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Los amigos Protectores gozarán, además de:</strong></p>\r\n<p>a) Catálogos de la Asociación, gratuitos.</p>\r\n<p>b) Seis invitaciones al año para que visiten el museo o las exposiciones las personas que designen.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Los Protectores empresariales o institucionales tendrán también las siguientes ventajas: </strong></p>\r\n<p>a) Diez carnés de amigo de la Asociación a nombre de la empresa o entidad.</p>\r\n<p>b) Reconocimiento de esta colaboración en las publicaciones, así como en la memoria y en la difusión publicitaria anual de los actos de la Asociación.</p>\r\n<p>c) Prioridad en la utilización de los espacios del Museo para celebrar recepciones especiales o visitas privadas, que deberán contar con la oportuna autorización del Ministerio de Cultura.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 23.- </strong>Serán obligaciones de todos los socios:</p>\r\n<p>a) Compartir las finalidades de la Asociación y colaborar en la consecución de las mismas.</p>\r\n<p>b) Pagar las cuotas, derramas y aportaciones que pudieren corresponderles.</p>\r\n<p>c) Acatar las prescripciones contenidas en estos Estatutos y los acuerdos válidos adoptados por la Asamblea General y por la Junta Directiva.</p>\r\n<p>d) Cumplir las obligaciones inherentes a su condición.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 24.- </strong>Régimen disciplinario:</p>\r\n<p>El asociado que incumpliere sus obligaciones para con la Asociación, o cuya conducta menoscabe los fines o prestigio de la misma, será objeto del correspondiente expediente disciplinario incoado por la Junta Directiva, quien, previa audiencia al interesado, resolverá lo que proceda.</p>\r\n<p>Las sanciones pueden comprender desde la suspensión temporal de derechos, hasta la expulsión.</p>\r\n<p>Si la Junta acordare la expulsión, propondrá la misma a la Asamblea General para su aprobación.</p>\r\n<p>&nbsp;</p>\r\n<h3><strong>RÉGIMEN ECONÓMICO </strong></h3>\r\n<p><strong>Artículo 25.- </strong>El patrimonio fundacional es inexistente y el presupuestos inicial anual no podrá exceder de ciento cincuenta mil euros.</p>\r\n<p>El ejercicio asociativo y económico será anual, y su cierre tendrá lugar el 31 de diciembre de cada año.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 26.- </strong>Los recursos económicos previstos para el desarrollo de las actividades de la Asociación serán los siguientes:</p>\r\n<p>a) Cuotas de socios.</p>\r\n<p>b) Donativos de entidades o particulares.</p>\r\n<p>c) Subvenciones del Estado, Comunidad Autónoma, Ayuntamiento o Diputación.</p>\r\n<p>d) Herencias o legados que pudiere recibir legalmente por asociados o terceras personas.</p>\r\n<p>e) Ingresos que se obtengan mediante actividades lícitas que acuerde la Junta Directiva.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 27.- </strong>Las cuotas ordinarias o extraordinarias se establecerán por la Asamblea General a propuesta de la Junta Directiva y no serán reintegrables.</p>\r\n<p>La Asamblea General podrá fijar una cuota de admisión de socios, como aportación inicial no reintegrable.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 28.- </strong>La Asociación dispondrá de una relación actualizada de sus asociados y llevará una contabilidad que permita obtener una imagen fiel del patrimonio, resultado y situación financiera de la misma, así como de de las actividades realizadas. Efectuará un inventario de bienes y recogerá en un libro de actas las reuniones de los órganos de gobierno y representación.</p>\r\n<p>La contabilidad será llevada conforme a las normas específicas de aplicación.</p>\r\n<p>&nbsp;</p>\r\n<h3><strong>DISOLUCIÓN DE LA ASOCIACIÓN </strong></h3>\r\n<p><strong>Artículo 29.- </strong>Se disolverá la Asociación:</p>\r\n<p>a) Voluntariamente cuando así lo acuerde la Asamblea General Extraordinaria, convocada al efecto, por mayoría absoluta de los asociados presentes. Esta Asamblea debe celebrarse con un quórum mínimo de 2/3 de los asociados.</p>\r\n<p>b) Por las causas que especifica el art. 39 del Código Civil.</p>\r\n<p>c) Por sentencia judicial firme.</p>\r\n<p>En caso de disolución, se nombrará una comisión liquidadora la cual si, una vez extinguidas las deudas, existiese sobrante líquido, lo destinará para fines que no desvirtúen su naturaleza no lucrativa (concretamente al Museo Nacional de Escultura).</p>\r\n<p>&nbsp;</p>\r\n<h3><strong>REFORMA DE LOS ESTATUTOS </strong></h3>\r\n<p><strong>Artículo 30.- </strong>Las modificaciones de los presentes estatutos serán competencia de la Asamblea General Extraordinaria, debiéndose adoptar el acuerdo por mayoría cualificada de los presentes y representados. Se deberán comunicar al registro las modificaciones que se realicen.</p>\r\n<p>&nbsp;</p>\r\n<h3><strong>DISPOSICIÓN ADICIONAL </strong></h3>\r\n<p>En todo cuanto no esté previsto en los presentes Estatutos se aplicará la vigente Ley Orgánica 1/2002, de 22 de marzo, reguladora del Derecho de Asociación, y las disposiciones complementarias.</p>'),
(139, 15, 1, 'Amigos del Museo de Escultura Asociation News'),
(140, 15, 2, 'Noticias de la asociación Amigos del Museo de Escultura'),
(141, 122, 1, ''),
(142, 122, 2, '<p>Desde la Asociación Amigos del Museo de Escultura, en nuestra labor de acercar el Museo a la gente, destinamos este espacio para dar a conocer todas las novedades, eventos y noticias de interés para asociados y amigos.</p>'),
(143, 125, 1, ''),
(144, 125, 2, 'Actividades Asociación Amigos del Museo de Escultura'),
(145, 129, 1, '<p>texto</p>'),
(146, 129, 2, '<p>La Asociación Amigos del Museo Nacional de Escultura organiza una serie de actividades con la finalidad de contribuir a la difusión de nuestro rico patrimonio.</p>\r\n<p><strong>Conferencias</strong>, <strong>publicaciones</strong>, <strong>adqusiciones</strong> y <strong>restauraciones</strong>, <strong>intercambios culturales</strong> y otras actividades como <strong>conciertos</strong>, <strong>visitas guiadas</strong> son algunas de las muchas actividades que proponemos desde nuestra asociación.</p>\r\n<h3>¡Conócelas!</h3>'),
(147, 144, 1, ''),
(148, 144, 2, 'Donaciones y Restauraciones'),
(149, 178, 1, ''),
(150, 178, 2, '<p>La Asociación de Amigos del Mueseo Nacional de Escultura, desde sus incios, ha apostado por contribuir en mejorar nuestro patrimonio cultural. Gracias a nuestros socios y <strong><a href=\"#/aportaciones/\" target=\"_self\">donantes</a></strong> hemos adquirido diferentes piezas así como colaborado en la restauración de muchas de ellas.</p>\r\n<h3><strong>1997</strong></h3>\r\n<p>La Asociación de Amigos donó al Museo las siguientes obras:</p>\r\n<p>De J. PÉREZ VILLAAMIL: “Patio del Colegio de San Gregorio” Tinta y acuarela sobre papel (40x 27,5)</p>\r\n<p>“Patio del Colegio de San Gregorio” Grabado(40x 27,5)</p>\r\n<p>Anónimo francés, 1868: “Patio de estudios del Colegio de San Gregorio” Lápiz sobre papel(18x 11, 5)</p>\r\n<p>&nbsp;</p>\r\n<h3><em><strong>1998</strong></em></h3>\r\n<p>La Junta Directiva de la Asociación, a propuesta de la Dirección del Museo, ha adquirido en el comercio del anticuario madrileño una intereseantísima escultura del siglo XVII. Se trata de una representación del <strong>Niño Jesús </strong>de plomo y policromada, que se puede adscribir al escultor ALONSO CANO (1601-1667)</p>\r\n<p>&nbsp;</p>\r\n<h3><em><strong>1999</strong></em></h3>\r\n<p>La Asociación ha financiado la restauración del <strong>órgano realejo</strong>, actualmente expuesto en la capilla del Museo.</p>\r\n<p>&nbsp;</p>\r\n<h3><em><strong>2</strong></em><em><strong>000</strong></em></h3>\r\n<p>A propuesta de la Dirección del Museo, la Asociación financió la restauración de la obra del pintor ANTONIO MORO “Calvario”, firmado y fechado en 1573.</p>\r\n<p>Gestionó con El Corte Inglés la restauración de un óleo sobre tabla titulado “La Virgen de la Rosa”, magnifica copia española del siglo XVI del original RAFAEL DE URBINO.</p>\r\n<p>&nbsp;</p>\r\n<h3><em><strong>2004</strong></em></h3>\r\n<p>Restauración en colaboración con Caja España, de las obras pictóricas de BOCANEGRA.</p>\r\n<p>&nbsp;</p>\r\n<h3><em><strong>2006</strong></em></h3>\r\n<p>Compra y donación al Museo de una pequeña escultura de un Cristo atado en la columna de principios del S. XVI del Círculo de Gregorio Fernández en cera.</p>\r\n<p>&nbsp;</p>\r\n<h3><em><strong>2014</strong></em></h3>\r\n<p>A finales del 2014, la Asociación adquirió para el Museo, unas litografías sobre</p>\r\n<p>papel de Bartolomé de las Casas, Bartolomé de Carranza, Luís de Granada y</p>\r\n<p>Melchor Cano, que ahora mismo están expuestas en las nuevas salas temáticas del Museo.</p>'),
(151, 145, 1, ''),
(152, 145, 2, 'Aumentando nuestro patrimonio cultural'),
(153, 181, 1, 'Links de interés'),
(154, 181, 2, 'Enlaces de interés'),
(155, 185, 1, ''),
(156, 185, 2, '<h3>Museos</h3>\r\n<table style=\"border:none; width:609px\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><a href=\"http://icom.museum/\" target=\"_blank\">The Internacional Council of Museums (ICOM)</a></p></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><a href=\"http://www.europeanmuseumforum.org/\" target=\"_blank\">European Museum Forum (Foro Europeo de los Museos)</a></p></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><a href=\"http://www.icom-ce.org/\" target=\"_blank\">Comité Español del ICO</a></p></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<h3>&nbsp;</h3>\r\n<h3>Cultura</h3>\r\n<table style=\"border:none; width:609px\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><a href=\"http://www.patrimonio-mundial.com/\" target=\"_blank\">Patrimonio Mundial</a></p></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><a href=\"http://www.amigosdemuseos.com/\" target=\"_blank\">Federación Española de Amigos de los Museos</a></p></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<h3>&nbsp;</h3>\r\n<h3>Instituciones</h3>\r\n<table style=\"border:none; width:609px\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><a href=\"https://www.valladolid.es\" target=\"_blank\">Ayuntamiento de Valladolid</a></p></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><a href=\"http://www.jcyl.es/\" target=\"_blank\">Junta de Castilla y León</a></p></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><a href=\"http://www.uva.es/export/sites/uva/\" target=\"_blank\">Universidad de Valladolid</a></p></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><a href=\"http://www.mcu.es/\" target=\"_blank\">Ministerio de Cultura</a></p></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<h3>&nbsp;</h3>\r\n<h3>Valladolid</h3>\r\n<table style=\"border:none; width:609px\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><a href=\"http://diariosalamanca.blogspot.com/\" target=\"_blank\">blogValladolid, diario de una ciudad</a></p></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>&nbsp;</p>'),
(157, 4, 1, ''),
(158, 4, 2, 'Asociación de Amigos del Museo Nacional de Escultura'),
(159, 188, 1, 'Legal advised'),
(160, 188, 2, 'Aviso Legal'),
(161, 192, 1, ''),
(162, 192, 2, '<p>La Web <a href=\"http://www.amigosmuseoescultura.es\">www.amigosmuseoescultura.es</a> tiene por objeto facilitar el conocimiento por el público en general de las actividades que realiza y de los servicios profesionales que presta.</p>\r\n<h3>1.Información general para dar cumplimiento a la Ley 34/200</h3>\r\n<p>Titular: Amigos Museo Nacional de Escultura</p>\r\n<p>Dirección: Colegio San Gregorio C/Cadenas de San Gregorio, s/n, 47011, Valladolid (España)</p>\r\n<p>Contacto: <a href=\"mailto:amigos.museoescultura@mcu.es\">amigos.museoescultura@mcu.es</a> o +34 983 250 375</p>\r\n<p>Datos relativos a la empresa: La asociación de Amigos del Museo Nacional Colegio de San Gregorio está inscrita en el registro Nacional de Asociaciones, de acuerdo con lo dispuesto por el artículo 28.1 apartado e) de la Ley Orgánica 1/2002, de 22 de marzo, reguladora del Derecho de Asociación, en el Grupo: 1/Número Nacional: 56479.</p>\r\n<h3>2.Tabla de precios de los productos y servicios</h3>\r\n<p>Consulta nuestras cuotas de <a href=\"#/miembros\" target=\"_self\">asociados</a></p>\r\n<h3>3.Propiedad intelectual</h3>\r\n<p>Los contenidos suministrados por <a href=\"http://www.amigosmuseoescultura.es\">www.amigosmuseoescultura.es</a> están sujetos a los derechos de propiedad intelectual e industrial y son titularidad exclusiva de Amigos Museo Nacional de Escultura. Mediante la adquisición de un producto o servicio, Amigos Museo Nacional de Escultura no confiere al adquirente ningún derecho de alteración, explotación, reproducción o distribución del mismo fuera de lo estrictamente contratado, reservándose amigos Museo Nacional de Escultura todos estos derechos. La cesión de los citados derechos precisará el previo consentimiento por escrito por parte de Amigos Museo Nacional de Escultura.</p>\r\n<p>La propiedad intelectual se extiende, además del contenido incluido en <a href=\"http://www.amigosmuseoescultura.es\">www.amigosmuseoescultura.es</a>, a sus gráficos, logotipos, diseño, imágenes y código fuente utilizado para su programación.</p>'),
(163, 195, 1, ''),
(164, 195, 2, 'Política de privacidad'),
(165, 196, 1, ''),
(166, 196, 2, 'Amigos del Museo Nacional de Escultura'),
(167, 199, 1, ''),
(168, 199, 2, '<p>A través de la página <a href=\"http://www.amigosmuseoescultura.es\">www.amigosmuseoescultura.es</a> no se recoge ningún dato personal sin su conocimiento, ni se ceden a terceros.</p>\r\n<p>Los datos personales proporcionados a <a href=\"http://www.amigosmuseoescultura.es\">www.amigosmuseoescultura.es</a>, a través de sus formularios de contacto o por los clientes que adquieran productos o servicios de <a href=\"http://www.amigosmuseoescultra.es\">www.amigosmuseoescultra.es</a>, van a ser almacenados en un fichero automatizado de datos, propiedad de Amigos Museo Nacional de Escultura, para la finalidad única de la gestión de la asociación.</p>\r\n<p>El destinatario de la información es en exclusiva Amigos Museo Nacional de Escultura, que se comprometa a <strong>NO ENVIAR COMUNICACIONES PUBLICITARIAS O COMERCIALES SOBRE SUS PRODUCTOS O SERVICIOS.</strong></p>\r\n<p>Amigos Museo Nacional de Escultura se compromete a tratar los datos de manera lícita y responsable, de acuerdo con la Ley Orgánica 15/1999, de Protección de Datos de Carácter Personal, garantizando el Derecho a la intimidad personal y familiar, el honor y el Derecho Fundamental a la protección de los Datos Personales de todas las personas que se encuentren en sus ficheros.</p>\r\n<p>Todo aquel que proporcione sus datos personales a Amigos Museo Nacional de Escultura a través de las web <a href=\"http://www.amigosmuseoescultura.es\">www.amigosmuseoescultura.es</a> tendrá la posibilidad de revocar su consentimiento en cualquier momento, o de ejercitar sus derechos de Acceso, Rectificación, Cancelación y Oposición por cualquier medio admitido en derecho que permita acreditar la personalidad. Para ello deberá dirigirse a Amigos Museo Nacional de Escultura, C/ Cadenas de San Gregorio s/n, 47011, Valladolid (España) o a través de correo electrónico a la dirección <a href=\"mailto:amigos.museoescultura@mcu.es\">amigos.museoescultura@mcu.es</a> bajo el asunto “Datos personales” o también a través del teléfono +34 983 250 375.</p>'),
(169, 202, 1, ''),
(170, 202, 2, 'Política de Cookies'),
(171, 203, 1, ''),
(172, 203, 2, 'Amigos del Museo Nacional de Escultura'),
(173, 206, 1, '<p>texto</p>');
INSERT INTO `idiomas__textos` (`id`, `id_clave`, `id_idioma`, `valor`) VALUES
(174, 206, 2, '<p>El prestador por su propia cuenta o la de un tercero contratado para la prestación de servicios de medición, pueden utilizar cookies cuando un usuario navega por el sitio web. Las cookies son ficheros enviados al navegador por medio de un servidor web con la finalidad de registrar las actividades del usuario durante su tiempo de navegación.</p>\r\n<p>Las cookies utilizadas por el sitio web se asocian únicamente con un usuario anónimo y su ordenador, y no proporcionan por sí mismas los datos personales del usuario.</p>\r\n<p>Mediante el uso de las cookies resulta posible que el servidor donde se encuentra la web, reconozca el navegador web utilizado por el usuario con la finalidad de que la navegación sea más sencilla, permitiendo, por ejemplo, el acceso a los usuarios que se hayan registrado previamente, acceder a las áreas, servicios, promociones o concursos reservados exclusivamente a ellos sin tener que registrarse en cada visita. Se utilizan también para medir la audiencia y parámetros del tráfico, controlar el progreso y número de entradas.</p>\r\n<p>El usuario tiene la posibilidad de configurar su navegador para ser avisado de la recepción de cookies y para impedir su instalación en su equipo. Por favor, consulte las instrucciones y manuales de su navegador para ampliar esta información.</p>\r\n<p>Para utilizar el sitio web, no resulta necesario que el usuario permita la instalación de las cookies enviadas por el sitio web, o el tercero que actúe en su nombre, sin perjuicio de que sea necesario que el usuario inicie una sesión como tal en cada uno de los servicios cuya prestación requiera el previo registro o \"login\".</p>\r\n<p>Las cookies utilizadas en este sitio web tienen, en todo caso, carácter temporal con la única finalidad de hacer más eficaz su transmisión ulterior. En ningún caso se utilizarán las cookies para recoger información de carácter personal.</p>\r\n<h3><strong>Tabla de cookies utilizadas:</strong></h3>\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th>Proveedor</th>\r\n			<th>Nombre</th>\r\n			<th>Propósito</th>\r\n			<th>Más información</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Amigos del Museo Nacional de Escultura</td>\r\n			<td>PHPSESSID</td>\r\n			<td>Permite al usuario visualizar la página e interactuar con ella. Es esencial para el funcionamiento correcto de la página.</td>\r\n			<td>&nbsp;\r\n			<p><a href=\"#/aviso-legal\">Aviso legal</a></p>\r\n			<p><a href=\"#/politica-de-privacidad\">Política de privacidad</a></p></td>\r\n		</tr>\r\n		<tr>\r\n			<td>Google Analytics</td>\r\n			<td>\r\n			<p>__utma<br />\r\n			__utmb</p>\r\n			<p>__utmc<br />\r\n			__utmz</p></td>\r\n			<td>Recopilan información anónima sobre la navegación de los usuarios por el sitio web con el fin de conocer el origen de las visitas y otros datos estadísticos similares.</td>\r\n			<td>&nbsp;\r\n			<p><a href=\"https://www.google.com/intl/es/policies/\" target=\"_blank\">Centro de privacidad de Google</a></p>\r\n			<p><a href=\"http://tools.google.com/dlpage/gaoptout?hl=es\" target=\"_blank\">Complemento de inhabilitación de Google Analytics</a></p></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>Para desactivar las cookies, el usuario podrá, en cualquier momento, elegir cuáles quiere que funcionen en este sitio web mediante:</p>\r\n<p><strong>Configuración del navegador</strong>; por ejemplo:</p>\r\n<ol>\r\n	<li>Firefox, desde&nbsp;<a href=\"http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we\" target=\"_blank\">http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we</a></li>\r\n	<li>Chrome, desde&nbsp;<a href=\"http://support.google.com/chrome/bin/answer.py?hl=es&amp;answer=95647\" target=\"_blank\">http://support.google.com/chrome/bin/answer.py?hl=es&amp;answer=95647</a></li>\r\n	<li>Explorer, desde&nbsp;<a href=\"http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9\" target=\"_blank\">http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9</a></li>\r\n	<li>Safari, desde&nbsp;<a href=\"http://support.apple.com/kb/ph5042\" target=\"_blank\">http://support.apple.com/kb/ph5042</a></li>\r\n</ol>\r\n<p><strong>Sistemas de opt-out </strong>específicos indicados en la tabla anterior respecto de la cookie de que se trate (estos sistemas pueden conllevar que se instale en su equipo una cookie \"de rechazo\" para que funcione su elección de desactivación)</p>\r\n<p><strong>Otras herramientas de terceros</strong>, disponibles on line, que permiten a los usuarios detectar las cookies en cada sitio web que visita y gestionar su desactivación (por ejemplo, Ghostery:&nbsp;</p>\r\n<ol>\r\n	<li><a href=\"http://www.ghostery.com/privacy-statement\" target=\"_blank\">http://www.ghostery.com/privacy-statement</a>,&nbsp;</li>\r\n	<li><a href=\"http://www.ghostery.com/faq\" target=\"_blank\">http://www.ghostery.com/faq</a>).</li>\r\n</ol>'),
(175, 207, 1, ''),
(176, 207, 2, 'Aportaciones - Micromecenazgo'),
(177, 208, 1, ''),
(178, 208, 2, 'Colaborar con el Museo Nacional de Escultura'),
(179, 209, 1, ''),
(180, 209, 2, '<p>Desde de hace décadas, la Asociación de Amigos del Museo Nacional de Escultura promueve una serie de donaciones y restaruaciones con el fín de mejorar nuestro patrimonio cultural. Gracias a tu apoyo y al de cientos de donantes hemos llevado a cabo <a href=\"#/donacion-y-restauracion\" target=\"_self\">diferentes acciones</a>.&nbsp;</p>'),
(181, 210, 1, ''),
(182, 210, 2, 'Quiero hacer una donación'),
(183, 211, 1, ''),
(184, 211, 2, '<p>Para hacer una aportación rellena el siguiente formulario indicando tus datos y el importe que quieres donar. Recibirás un comprobante con el que podrás desgravarte.</p>'),
(185, 212, 1, ''),
(186, 212, 2, '<h3><strong>El objeto de tu donativo</strong></h3>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse dictum orci nec quam blandit tempor. Sed commodo ut nibh consectetur viverra. Duis tincidunt mauris eget magna iaculis, ac scelerisque nunc aliquet. Integer vestibulum porta porttitor. Sed accumsan erat ex, ut congue justo mattis in. Pellentesque fermentum quam suscipit, eleifend metus sed, placerat erat. Vestibulum gravida, nulla et iaculis ultrices, tellus dolor euismod turpis, sed convallis sapien metus quis risus. Praesent quam orci, venenatis et rutrum sed, fringilla vel tortor. Vestibulum facilisis sagittis ipsum non vehicula. Donec porttitor est mi, non tincidunt est vehicula a. Quisque et leo eros. Morbi dignissim dictum vestibulum. Praesent at laoreet augue. Etiam rhoncus urna neque, sed dignissim odio euismod quis.</p>'),
(187, 213, 1, '<p>Los datos recogidos serán incorporados a una base de datos propiedad de Amigos del Museo Nacional de Escultura con el fin único de la gestión de nuestra asociación. Podrá en todo momento acceder para modificar o eliminar sus datos o para darse de baja, para ello, deberá dirigirse de manera presencial a nuestras oficinas, sitas en el propio Museo en horario de atención al público.</p>'),
(188, 213, 2, '<h5>Los datos recogidos serán incorporados a una base de datos propiedad de Amigos del Museo Nacional de Escultura con el fin único de la gestión de nuestra asociación. Podrá en todo momento acceder para modificar o eliminar sus datos o para darse de baja, para ello, deberá dirigirse de manera presencial a nuestras oficinas, sitas en el propio Museo en horario de atención al público.</h5>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__archivos`
--

CREATE TABLE `noticias__archivos` (
  `id` int(11) NOT NULL,
  `id_noticia` int(11) UNSIGNED NOT NULL,
  `creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `descripcion` varchar(1000) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `noticias__archivos`
--

INSERT INTO `noticias__archivos` (`id`, `id_noticia`, `creacion`, `descripcion`) VALUES
(37, 1, '2016-07-11 07:59:47', 'Adjunto 1'),
(38, 1, '2016-07-11 08:00:14', 'Adjunto 2'),
(39, 2, '2016-07-11 10:18:01', 'Cuaderno de campo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__categorias`
--

CREATE TABLE `noticias__categorias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `url` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `titulo` varchar(120) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `meta_titulo` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `meta_descripcion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `owner_id` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `noticias__categorias`
--

INSERT INTO `noticias__categorias` (`id`, `nombre`, `url`, `titulo`, `meta_titulo`, `meta_descripcion`, `position`, `owner_id`, `created_at`, `updated_at`) VALUES
(1, 'test', 'test', NULL, 'test', 'test', 0, 0, '2017-08-01 07:53:00', '2017-08-01 07:53:00'),
(2, '', '', NULL, NULL, NULL, 0, 0, '2017-07-31 23:15:06', '2017-07-31 23:15:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__categories`
--

CREATE TABLE `noticias__categories` (
  `id` int(11) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'url en español',
  `nombre` varchar(255) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre en español',
  `position` int(11) NOT NULL DEFAULT '0',
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `noticias__categories`
--

INSERT INTO `noticias__categories` (`id`, `url`, `nombre`, `position`, `owner_id`, `created_at`, `updated_at`) VALUES
(1, 'expo', 'Exposiciones', -2, 0, NULL, NULL),
(2, 'itinerante-en', 'Itinerante', -1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__comments`
--

CREATE TABLE `noticias__comments` (
  `id` int(11) UNSIGNED NOT NULL,
  `post_id` int(11) UNSIGNED NOT NULL,
  `parent_id` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `number` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `author` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `content` text COLLATE utf8_spanish2_ci NOT NULL,
  `date` datetime DEFAULT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `moderated` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__files`
--

CREATE TABLE `noticias__files` (
  `id` int(11) UNSIGNED NOT NULL,
  `owner_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'propietario (id)',
  `owner_type` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'propietario (mod)',
  `type` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'tipo de archivo',
  `title_1` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'título',
  `name` text COLLATE utf8_spanish2_ci COMMENT 'nombre original',
  `mime_type` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'tipo recogido',
  `size` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'tamaño',
  `height` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'alto (si imagen)',
  `width` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ancho (si imagen)',
  `creacion` timestamp NULL DEFAULT NULL,
  `modificacion` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `noticias__files`
--

INSERT INTO `noticias__files` (`id`, `owner_id`, `owner_type`, `type`, `title_1`, `name`, `mime_type`, `size`, `height`, `width`, `creacion`, `modificacion`) VALUES
(29, 1, 'galeria', 'galery-img', 'aceitunas_sarasa1.jpg', 'aceitunas_sarasa1.jpg', 'image/jpeg', 231148, 243, 556, '2017-07-31 11:05:39', NULL),
(32, 14, 'galeria', 'galery-img', 'distribuidor_leon.png', 'distribuidor_leon.png', 'image/png', 33431, 320, 320, '2017-07-31 11:40:35', NULL),
(33, 15, 'galeria', 'galery-img', 'bodegas_lopezcristobal.jpg', 'bodegas_lopezcristobal.jpg', 'image/jpeg', 75677, 1089, 1368, '2017-07-31 11:41:06', NULL),
(34, 16, 'galeria', 'galery-img', 'chocolates_subiza.jpg', 'chocolates_subiza.jpg', 'image/jpeg', 36893, 600, 600, '2017-07-31 11:41:34', NULL),
(39, 21, 'galeria', 'galery-img', 'distribuidor_palencia.png', 'distribuidor_palencia.png', 'image/png', 21135, 320, 320, '2017-07-31 11:44:30', NULL),
(40, 3, 'galeria', 'galery-img', 'bodegas_lopezcristobal.jpg', 'bodegas_lopezcristobal.jpg', 'image/jpeg', 75677, 1089, 1368, '2017-07-31 11:50:36', NULL),
(41, 12, 'galeria', 'galery-img', 'aceitunas_sarasa1.jpg', 'aceitunas_sarasa1.jpg', 'image/jpeg', 231148, 243, 556, '2017-07-31 11:50:36', NULL),
(42, 11, 'galeria', 'galery-img', 'aceitunas_sarasa2.jpg', 'aceitunas_sarasa2.jpg', 'image/jpeg', 128439, 305, 554, '2017-07-31 11:50:36', NULL),
(46, 4, 'galeria', 'galery-img', 'aceitunas_sarasa3.jpg', 'aceitunas_sarasa3.jpg', 'image/jpeg', 115149, 269, 554, '2017-07-31 11:52:28', NULL),
(59, 32, 'galeria', 'galery-img', 'aceitunas_sarasa3.jpg', 'aceitunas_sarasa3.jpg', 'image/jpeg', 115149, 269, 554, '2017-08-01 07:51:53', NULL),
(60, 33, 'galeria', 'galery-img', 'aceitunas_sarasa1.jpg', 'aceitunas_sarasa1.jpg', 'image/jpeg', 231148, 243, 556, '2017-08-01 07:51:53', NULL),
(61, 34, 'galeria', 'galery-img', 'aceitunas_sarasa2.jpg', 'aceitunas_sarasa2.jpg', 'image/jpeg', 128439, 305, 554, '2017-08-01 07:51:53', NULL),
(62, 3, 'posts', 'img', 'campanal.jpg', 'campanal.jpg', 'image/jpeg', 24243, 414, 508, '2017-08-01 09:43:05', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__galeria`
--

CREATE TABLE `noticias__galeria` (
  `id` int(11) NOT NULL,
  `id_noticia` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `alt` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `noticias__galeria`
--

INSERT INTO `noticias__galeria` (`id`, `id_noticia`, `nombre`, `alt`, `position`, `created_at`, `updated_at`) VALUES
(1, 1, '1', '1', 0, '2017-02-09 13:31:42', NULL),
(3, 2, '3', '3', -3, NULL, NULL),
(11, 2, '2', '2', -2, NULL, NULL),
(12, 2, '1', '1', -1, NULL, NULL),
(14, 1, '', NULL, -1, NULL, NULL),
(22, 1, '', NULL, 0, NULL, NULL),
(32, 3, '3', '', -3, NULL, NULL),
(33, 3, '1', '', -1, NULL, NULL),
(34, 3, '2', '', -2, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__posts`
--

CREATE TABLE `noticias__posts` (
  `id` int(11) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'url en español',
  `title` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'título en español',
  `meta_titulo` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `meta_descripcion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `intro` text COLLATE utf8_spanish2_ci COMMENT 'entradilla en español',
  `content` text COLLATE utf8_spanish2_ci COMMENT 'contenido en español',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'fecha de creación',
  `leido` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'veces visto',
  `publicado` tinyint(1) NOT NULL DEFAULT '1',
  `category_id` int(11) UNSIGNED DEFAULT NULL COMMENT 'id de categoría',
  `publishing_date` timestamp NULL DEFAULT NULL COMMENT 'fecha de publicación',
  `modification_date` timestamp NULL DEFAULT NULL COMMENT 'ultima modificación'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `noticias__posts`
--

INSERT INTO `noticias__posts` (`id`, `url`, `title`, `meta_titulo`, `meta_descripcion`, `intro`, `content`, `date`, `leido`, `publicado`, `category_id`, `publishing_date`, `modification_date`) VALUES
(1, 'la-copia-de-la-copia', 'La copia de la copia', NULL, NULL, 'Un taller de fotografía con J.C. Quindós\r\nEdificio Casa del Sol\r\nDel 17 de febrero al 16 de abril de 2017', '<p>Esta exposición muestra el resultado de tres talleres fotográficos impartidos por Juan Carlos Quindós en el Museo Nacional de Escultura bajo la idea principal de La copia de la copia: una puesta en valor de la copia artística como estrategia creativa, que se apoya en la colección de vaciados en yeso de la Casa del Sol. El resultado muestra cómo la fotografía es un instrumento privilegiado para documentar e interpretar del patrimonio artístico, sin prejuicios: la propia idea de autor se desdibuja en este experimento colectivo, un gran collage cargado de expresividad que establece una relación más directa e intuitiva con el objeto que se mira.</p>\r\n<p><img alt=\"\" src=\"http://imac.local:5757/files/ckeditor/4b4b972.jpg\" style=\"height:683px; width:1024px\" /></p>\r\n<p>Esta muestra quiere servir de agradecimiento al gran trabajo realizado por los alumnos (muchos de ellos niños) que, mediante sus \"copias de copias\", han generado visiones propias, originales y estimulantes, del fantástico patrimonio escultórico del Museo.</p>', '2017-07-31 09:45:59', 69, 1, 2, '2017-02-08 09:27:00', NULL),
(2, 'henry-moore', 'Henry Moore412 123123 12', NULL, NULL, 'Arte en la calle por Obra Social La Caixa', '<p>Henry Moore (1898-1986) es uno de los grandes maestros de la escultura moderna. En las décadas que siguieron a la Segunda Guerra Mundial alcanzó fama mundial por los bronces monumentales expuestos en espacios exteriores, cívicos y públicos de todo el mundo, de Escocia a orillas del Mar Rojo, de la escuela secundaria del pueblo donde nació (Castleford, en Inglaterra), a la sede de Naciones Unidas en Nueva York. La exposición&nbsp;<strong>Henry Moore</strong>&nbsp;forma parte del programa&nbsp;<strong>Arte en la calle</strong>, a través del cual la Obra Social ”la Caixa” quiere aproximar el arte a las personas fuera del marco habitual de museos y salas de exposiciones. Los seis bronces monumentales que forman la muestra, organizada en colaboración con el Ayuntamiento de Valladolid y la Henry Moore Foundation, fueron creados por el escultor en la cúspide de su carrera, entre los años 1960 y 1982. Estas seis esculturas son una muestra representativa de los motivos principales de la obra de Moore: la fascinación por la figura reclinada y los temas sobre \"madre e hijo\"; la exploración de la relación entre la figura humana y el paisaje, tanto urbano como rural; la tensión entre lo natural y lo abstracto; y la transformación de los objetos naturales en formas escultóricas.</p>\r\n<p>Organización y producción: Obra Social ”la Caixa”, en colaboración con la Henry Moore Foundation, el Ayuntamiento de Valladolid y el Museo Nacional de Escultura. Comisariado: Sebastiano Barassi, jefe de Colecciones de la Henry Moore Foundation. Fechas: del 1 de febrero al 2 de abril de 2017. Lugar: Calle Cadenas de San Gregorio</p>', '2017-07-31 06:51:01', 45, 1, 1, '2017-02-09 13:30:00', NULL),
(3, 'test', 'test', 'meta de título', 'meta de descripción', 'Buena entrada!', '', '2017-08-01 09:43:05', 0, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__rel_tags`
--

CREATE TABLE `noticias__rel_tags` (
  `id` int(11) NOT NULL,
  `post_id` int(11) UNSIGNED NOT NULL COMMENT 'id de post',
  `tag_id` int(11) UNSIGNED NOT NULL COMMENT 'id de tag'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__tags`
--

CREATE TABLE `noticias__tags` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_tipo` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'url en español',
  `text` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'nombre en español'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `noticias__tags`
--

INSERT INTO `noticias__tags` (`id`, `id_tipo`, `url`, `text`) VALUES
(1, NULL, 'art', 'arte'),
(2, NULL, 'baroque', 'barroco');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(128) COLLATE utf8_spanish2_ci NOT NULL,
  `codigo_cliente` int(11) UNSIGNED DEFAULT NULL,
  `id_token` int(11) UNSIGNED DEFAULT NULL,
  `nombre` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `apellidos` varchar(160) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nif` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `nif_dir_facturacion` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre_dir_facturacion` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `codigo_dir_facturacion` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `direccion_dir_facturacion` varchar(120) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `cp_dir_facturacion` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `localidad_dir_facturacion` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `provincia_dir_facturacion` int(11) DEFAULT NULL,
  `pais_dir_facturacion` int(11) DEFAULT NULL,
  `codigo_dir_envio` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre_dir_envio` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `direccion_dir_envio` varchar(120) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `cp_dir_envio` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `localidad_dir_envio` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `provincia_dir_envio` int(11) DEFAULT NULL,
  `pais_dir_envio` int(11) DEFAULT NULL,
  `envioEmpresa` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `envioImporte` float NOT NULL DEFAULT '0',
  `envioObservaciones` varchar(1000) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `formaDePago` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `is_factura` tinyint(1) NOT NULL DEFAULT '0',
  `is_envolver_regalo` tinyint(1) NOT NULL DEFAULT '0',
  `is_tarjeta_regalo` tinyint(1) NOT NULL DEFAULT '0',
  `str_tarjeta_regalo` varchar(1000) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `vale` int(11) DEFAULT '0',
  `pvp_total` float NOT NULL DEFAULT '0',
  `peso_total` int(11) NOT NULL DEFAULT '0',
  `pagando` tinyint(1) NOT NULL DEFAULT '0',
  `pagandoFecha` datetime DEFAULT NULL,
  `bancoRespPre` mediumtext COLLATE utf8_spanish2_ci,
  `bancoRespNot` mediumtext COLLATE utf8_spanish2_ci,
  `bancoRespConfirm` mediumtext COLLATE utf8_spanish2_ci,
  `bancoRespCancel` mediumtext COLLATE utf8_spanish2_ci,
  `validacion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `carrito` mediumtext COLLATE utf8_spanish2_ci,
  `request_id` int(11) DEFAULT NULL,
  `historico` tinyint(1) NOT NULL DEFAULT '0',
  `correo_resumen` tinyint(1) NOT NULL DEFAULT '0',
  `correo_envio` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `orders`
--

INSERT INTO `orders` (`id`, `code`, `codigo_cliente`, `id_token`, `nombre`, `apellidos`, `email`, `nif`, `telefono`, `fecha_nacimiento`, `nif_dir_facturacion`, `nombre_dir_facturacion`, `codigo_dir_facturacion`, `direccion_dir_facturacion`, `cp_dir_facturacion`, `localidad_dir_facturacion`, `provincia_dir_facturacion`, `pais_dir_facturacion`, `codigo_dir_envio`, `nombre_dir_envio`, `direccion_dir_envio`, `cp_dir_envio`, `localidad_dir_envio`, `provincia_dir_envio`, `pais_dir_envio`, `envioEmpresa`, `envioImporte`, `envioObservaciones`, `formaDePago`, `is_factura`, `is_envolver_regalo`, `is_tarjeta_regalo`, `str_tarjeta_regalo`, `vale`, `pvp_total`, `peso_total`, `pagando`, `pagandoFecha`, `bancoRespPre`, `bancoRespNot`, `bancoRespConfirm`, `bancoRespCancel`, `validacion`, `creation_date`, `modification_date`, `carrito`, `request_id`, `historico`, `correo_resumen`, `correo_envio`) VALUES
(304, 'H2017009KNX07AO08T70U487L1ZC19', NULL, 979, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-07 08:48:19', '2017-09-07 08:48:19', NULL, NULL, 0, 0, 0),
(305, 'D2017R09BP0070K0962AA35TH96Z17', NULL, 980, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-07 09:35:17', '2017-09-07 09:35:17', NULL, NULL, 0, 0, 0),
(306, '72017A09A4U118B06LVI415EZI8N27', NULL, 981, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 06:15:27', '2017-09-11 06:15:27', NULL, NULL, 0, 0, 0),
(307, '72017O09VNP11VQ07U3NJ05PCK0L01', 5, 983, 'adsasd', 'asdasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 07:05:01', '2017-09-11 07:05:01', NULL, NULL, 0, 0, 0),
(308, 'S2017X09SXL11C507BRJT11TM0YN42', NULL, 984, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 07:11:42', '2017-09-11 07:11:42', NULL, NULL, 0, 0, 0),
(309, 'T2017V09CN911KD07L7DJ143924M04', NULL, 985, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 07:14:04', '2017-09-11 07:14:04', NULL, NULL, 0, 0, 0),
(310, 'O2017O09980114X079Y3R15EG4F029', NULL, 986, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 07:15:29', '2017-09-11 07:15:29', NULL, NULL, 0, 0, 0),
(311, '62017H09IEF11QU07E73A16Y2C7K50', NULL, 987, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 07:16:50', '2017-09-11 07:16:50', NULL, NULL, 0, 0, 0),
(312, 'S2017009MLX11F507JG8H18V3P3M13', NULL, 988, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 07:18:13', '2017-09-11 07:18:13', NULL, NULL, 0, 0, 0),
(313, '82017A096OL114M07ENT818TQV3423', NULL, 989, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 07:18:23', '2017-09-11 07:18:23', NULL, NULL, 0, 0, 0),
(314, 'T20179099J211CE07XY7T21J3JSS13', NULL, 990, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 07:21:13', '2017-09-11 07:21:13', NULL, NULL, 0, 0, 0),
(315, 'D2017D093Z311JH078YZL416KFTA14', NULL, 995, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 07:41:14', '2017-09-11 07:41:14', NULL, NULL, 0, 0, 0),
(316, '32017U09RO611WI08SF8019MEJLL20', NULL, 996, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 08:19:20', '2017-09-11 08:19:20', NULL, NULL, 0, 0, 0),
(317, '32017E09E0W11T9088BHW27EK6A346', NULL, 997, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 08:27:46', '2017-09-11 08:27:46', NULL, NULL, 0, 0, 0),
(318, 'M2017S09U1011RC09BENT36U4YDS05', NULL, 999, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 09:36:05', '2017-09-11 09:36:05', NULL, NULL, 0, 0, 0),
(319, 'N2017109CFR11VW10BMRF31V4QKR33', NULL, 1004, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 10:31:33', '2017-09-11 10:31:33', NULL, NULL, 0, 0, 0),
(320, 'T2017009MDC11Z5105OBR33N92QP59', NULL, 1007, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 10:33:59', '2017-09-11 10:33:59', NULL, NULL, 0, 0, 0),
(321, 'R2017T09KHJ113D10AR9634LGWPP26', NULL, 1009, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 10:34:26', '2017-09-11 10:34:26', NULL, NULL, 0, 0, 0),
(322, 'N2017X09EJR123707BIBL21G72O640', NULL, 1011, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-12 07:21:40', '2017-09-12 07:21:40', NULL, NULL, 0, 0, 0),
(323, 'L2017N099AV13OZ09Q0QW074Q4R237', NULL, 1013, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-13 09:07:37', '2017-09-13 09:07:37', NULL, NULL, 0, 0, 0),
(324, 'X2017T092BN132W09LSTG10P26IM23', NULL, 1014, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-13 09:10:23', '2017-09-13 09:10:23', NULL, NULL, 0, 0, 0),
(325, 'H2017109RSM13X009B4TV14AZ75O10', NULL, 1015, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-13 09:14:10', '2017-09-13 09:14:10', NULL, NULL, 0, 0, 0),
(326, 'D2017X099OX13KZ10OTFB429JTMF09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-13 10:42:09', '2017-09-13 10:09:22', NULL, NULL, 0, 0, 0),
(327, 'A2017J09DHD13PC1112HI35GYTID52', NULL, 1017, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-13 11:35:52', '2017-09-13 11:35:52', NULL, NULL, 0, 0, 0),
(328, '02017109LTP13UG12PXBG21RM57A33', NULL, 1018, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-13 12:21:33', '2017-09-13 12:21:33', NULL, NULL, 0, 0, 0),
(329, 'U2017D098HF138112VFW526QS1PE17', NULL, 1019, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-13 12:26:17', '2017-09-13 12:26:17', NULL, NULL, 0, 0, 0),
(330, 'T2017Y096431315133H9X59B45SM38', NULL, 1021, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-13 13:59:38', '2017-09-13 13:59:38', NULL, NULL, 0, 0, 0),
(331, 'O2017M09T6T13ZS14KCTC03Y4X9K58', NULL, 1022, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-13 14:03:58', '2017-09-13 14:03:58', NULL, NULL, 0, 0, 0),
(332, 'E2017G09F7S13RT1452LV51C9PLE42', 5, 1023, 'adsasd', 'asdasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-13 14:51:42', '2017-09-13 14:51:42', NULL, NULL, 0, 0, 0),
(333, '12017I094PN14SU08P2ZD24PTBRC57', NULL, 1024, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-14 08:24:57', '2017-09-14 08:24:57', NULL, NULL, 0, 0, 0),
(334, 'E2017J09ESJ142X094NDX09IPTZI36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-14 09:09:36', '2017-09-14 09:09:37', NULL, NULL, 0, 0, 0),
(335, 'Q2017O09I0A14YG09EQXD37ZE13G14', NULL, 1026, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-14 09:37:14', '2017-09-14 09:37:14', NULL, NULL, 0, 0, 0),
(336, 'V2017G09R1R14Z209RHJX379UMDL51', NULL, 1027, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hllaa', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 9, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-14 09:37:51', '2017-09-14 10:40:31', NULL, NULL, 0, 0, 0),
(337, '02017C09G9K14I9113ZN133TUMQQ05', 5, 1028, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'qweqweqwe', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 115.5, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-14 11:33:05', '2017-09-14 13:02:35', NULL, NULL, 0, 0, 0),
(338, 'D20179099FM14ED125O0H030LDVU16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-14 12:03:16', '2017-09-14 12:09:18', NULL, NULL, 0, 0, 0),
(339, 'D2017W09GS71467135BI618WLPOW26', NULL, 1029, NULL, NULL, NULL, NULL, '344553445543', NULL, 'asdddddddddddddddddd', 'asdddddddddddddddddddddddddddddddddddddddddddddddddddddddddd', NULL, 'asddddddddddddddddddddddddddddddddddddddddddddddddddddddddddasdddddddddddddddddddddddddddddddddddddddddddddddddddddddddd', 'asdddddddd', 'asdddddddddddddddddddddddddddddddddddddddddddddddddddddddddd', 17, 1, NULL, 'asdddddddddddddddddddddddddddddddddddddddddddddddddddddddddd', 'asdddddddddddddddddddddddddddddddddddddddddddddddddddddddddd', 'asdddddddd', 'asdddddddddddddddddddddddddddddddddddddddddddddddddddddddddd', 49, 1, NULL, 0, 'asdasd asd as das', 'paypal', 1, 0, 0, NULL, 0, 17, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-14 13:18:26', '2017-09-14 14:07:15', NULL, NULL, 0, 0, 0),
(431, 'F2017S09A2814NJ080PJ51762A2839', 5, 1028, 'Cesar', 'Vega Martín', 'cevm88@gmail.com', '12345678N', '987654321', '2017-09-05', '12345678i', 'Euphorbia SL', '1', '1', '47002', 'Valladolid', 20, 1, '1', 'César Vega martín', 'Amadeo Arías 32 1ºA', '47014', 'Valladolid', 1, 1, 'Euphorbia SL', 5, 'Discrección', 'tarjeta', 1, 0, 0, 'Felicidades!', 0, 10, 1000, 0, '2017-09-27 00:00:00', NULL, NULL, NULL, NULL, NULL, '2017-09-14 08:17:39', '2017-09-14 08:18:02', NULL, 210000379, 0, 1, 1),
(432, '52017109G9414NB14KWPD59ZDDYR06', 5, 1033, 'adsasd', 'asdasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-14 14:59:06', '2017-09-14 14:59:06', NULL, NULL, 0, 0, 0),
(433, 'R2017609B2B15Y408R5U049DC8FG56', 5, 1034, 'adsasd', 'asdasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 08:49:56', '2017-09-15 08:49:56', NULL, NULL, 0, 0, 0),
(434, 'R2017R09XZR150V09KQ6E00BO55018', NULL, 1037, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 09:00:18', '2017-09-15 09:00:18', NULL, NULL, 0, 0, 0),
(435, 'K2017Z09Q6615IW090I2X02Q8E0N12', NULL, 1040, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 09:02:12', '2017-09-15 09:02:12', NULL, NULL, 0, 0, 0),
(436, 'S2017A09M2M15RE09KAIZ02LXIZL16', NULL, 1042, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 09:02:16', '2017-09-15 09:02:16', NULL, NULL, 0, 0, 0),
(437, 'L2017109MF4155B09A5YD31SJHM308', NULL, 1043, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 09:31:08', '2017-09-15 09:31:08', NULL, NULL, 0, 0, 0),
(438, 'N201760972W15UN09OW38529WOZM56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 09:52:56', '2017-09-15 09:09:29', NULL, NULL, 0, 0, 0),
(439, '02017A096GY15N00990OY558K27803', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 09:55:03', '2017-09-15 09:09:03', NULL, NULL, 0, 0, 0),
(440, 'I2017X09P6L15UW09XQSR58SVLRM05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 09:58:05', '2017-09-15 09:09:05', NULL, NULL, 0, 0, 0),
(441, 'M2017I09AJ215FT095F1N59M3G0F19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 09:59:19', '2017-09-15 09:09:19', NULL, NULL, 0, 0, 0),
(442, 'W2017Z09FU815BQ1030S709TRQ1E30', NULL, 1045, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 10:09:30', '2017-09-15 10:09:30', NULL, NULL, 0, 0, 0),
(443, 'L20170099RP152V101J2111Q7TCY16', 5, 1046, 'adsasd', 'asdasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 10:11:16', '2017-09-15 10:11:16', NULL, NULL, 0, 0, 0),
(444, 'S2017P09QD615Y5124R4B16P7DJL08', 5, 1048, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 12:16:08', '2017-09-15 12:51:40', NULL, NULL, 0, 0, 0),
(445, 'M2017Z096RZ15EB1308YT013DR5D27', 5, 1054, 'adsasd', 'asdasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 13:01:27', '2017-09-15 13:01:27', NULL, NULL, 0, 0, 0),
(446, 'T2017B09F5G156Z13ZSUK04WX2AK08', 5, 1057, 'adsasd', 'asdasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 13:04:08', '2017-09-15 13:04:08', NULL, NULL, 0, 0, 0),
(447, 'O2017Y097Z0159113QKE723QXDMP38', 5, 1059, 'adsasd', 'asdasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 13:23:38', '2017-09-15 13:23:38', NULL, NULL, 0, 0, 0),
(448, 'F2017A10TYB04K814UP2P2203S8907', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-04 14:22:07', '2017-10-04 02:10:25', NULL, NULL, 0, 0, 0),
(449, 'V2017O10SG4058112TWQE477VF8T03', 5, 1493, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-05 12:47:03', '2017-10-05 12:59:21', NULL, NULL, 0, 0, 0),
(450, 'H2017510ZSL05Z013FGIH192A2HA00', 5, 1494, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-05 13:19:00', '2017-10-05 13:19:15', NULL, NULL, 0, 0, 0),
(451, '92017Z10M2G050P13NR14205QU6520', 5, 1495, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-05 13:20:20', '2017-10-05 13:20:32', NULL, NULL, 0, 0, 0),
(452, 'T2017I10XS605EU13EF9851PZZFV41', 5, 1498, 'César', 'Vega', NULL, '12421477M', '601181340', NULL, '12421477M', NULL, NULL, 'c/ Amadeo Arías 32 1ºA', '47014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ Amadeo Arías 32 1ºA', '47014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-05 13:51:41', '2017-10-05 13:51:41', NULL, NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders__articulos`
--

CREATE TABLE `orders__articulos` (
  `id` int(11) NOT NULL,
  `id_order` int(10) UNSIGNED DEFAULT NULL,
  `articulo` int(11) DEFAULT NULL,
  `talla` varchar(3) CHARACTER SET utf8 DEFAULT NULL,
  `precio` float NOT NULL DEFAULT '0',
  `iva` float NOT NULL DEFAULT '0',
  `precio_iva` float NOT NULL DEFAULT '0',
  `unidades` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `orders__articulos`
--

INSERT INTO `orders__articulos` (`id`, `id_order`, `articulo`, `talla`, `precio`, `iva`, `precio_iva`, `unidades`, `created_at`, `updated_at`) VALUES
(386, 305, 5, NULL, 0, 0, 0, 1, '2017-09-07 11:14:33', '2017-09-07 11:14:33'),
(387, 305, 7, NULL, 0, 0, 0, 3, '2017-09-07 11:14:34', '2017-09-07 11:15:25'),
(388, 322, 1, NULL, 0, 0, 0, 5, '2017-09-12 05:21:40', '2017-09-12 05:21:58'),
(390, 322, 5, NULL, 0, 0, 0, 5, '2017-09-12 05:21:43', '2017-09-12 05:21:44'),
(391, 323, 2, NULL, 0, 0, 0, 1, '2017-09-13 07:07:42', '2017-09-13 07:07:42'),
(392, 324, 2, NULL, 0, 0, 0, 1, '2017-09-13 07:10:23', '2017-09-13 07:10:23'),
(393, 324, 1, NULL, 0, 0, 0, 4, '2017-09-13 07:10:30', '2017-09-13 07:10:35'),
(394, 324, 5, NULL, 0, 0, 0, 2, '2017-09-13 07:11:07', '2017-09-13 07:11:12'),
(395, 325, 6, NULL, 0, 0, 0, 3, '2017-09-13 07:14:10', '2017-09-13 07:15:50'),
(396, 325, 5, NULL, 0, 0, 0, 2, '2017-09-13 07:14:12', '2017-09-13 08:20:09'),
(397, 325, 7, NULL, 0, 0, 0, 2, '2017-09-13 07:14:35', '2017-09-13 07:52:27'),
(398, 327, 1, NULL, 0, 0, 0, 1, '2017-09-13 09:35:52', '2017-09-13 09:35:52'),
(399, 328, 2, NULL, 0, 0, 0, 1, '2017-09-13 10:21:37', '2017-09-13 10:21:37'),
(400, 329, 6, NULL, 0, 0, 0, 2, '2017-09-13 10:26:27', '2017-09-13 10:26:29'),
(401, 329, 2, NULL, 0, 0, 0, 1, '2017-09-13 11:25:03', '2017-09-13 11:25:03'),
(402, 329, 4, NULL, 0, 0, 0, 1, '2017-09-13 11:25:04', '2017-09-13 11:25:04'),
(403, 329, 7, NULL, 0, 0, 0, 2, '2017-09-13 11:25:05', '2017-09-13 11:25:07'),
(404, 329, 5, NULL, 0, 0, 0, 1, '2017-09-13 11:25:06', '2017-09-13 11:25:06'),
(405, 331, 4, NULL, 0, 0, 0, 1, '2017-09-13 12:04:04', '2017-09-13 12:04:04'),
(406, 332, 6, NULL, 0, 0, 0, 3, '2017-09-13 12:51:57', '2017-09-13 12:52:01'),
(407, 333, 2, NULL, 0, 0, 0, 1, '2017-09-14 06:25:01', '2017-09-14 06:25:01'),
(408, 335, 2, NULL, 9.5, 10, 0.863636, 1, '2017-09-14 07:37:20', '2017-09-14 07:37:20'),
(409, 336, 6, NULL, 4.5, 10, 0.409091, 2, '2017-09-14 07:38:00', '2017-09-14 08:40:31'),
(410, 337, 1, NULL, 7.5, 10, 0.681818, 5, '2017-09-14 09:33:09', '2017-09-14 11:02:35'),
(411, 337, 7, NULL, 39, 10, 3.54545, 2, '2017-09-14 10:20:27', '2017-09-14 11:02:35'),
(412, 339, 2, NULL, 9.5, 10, 0.863636, 1, '2017-09-14 11:18:31', '2017-09-14 12:07:15'),
(413, 339, 1, NULL, 7.5, 10, 0.681818, 1, '2017-09-14 11:18:32', '2017-09-14 12:07:15'),
(414, 431, 7, NULL, 0, 0, 0, 3, '2017-09-07 11:14:34', '2017-09-07 11:15:25'),
(415, 431, 5, NULL, 0, 0, 0, 5, '2017-09-12 05:21:43', '2017-09-12 05:21:44'),
(416, 431, 6, NULL, 0, 0, 0, 3, '2017-09-13 07:14:10', '2017-09-13 07:15:50'),
(418, 442, 2, NULL, 9.5, 10, 0.863636, 1, '2017-09-15 08:09:30', '2017-09-15 08:09:30'),
(419, 442, 1, NULL, 7.5, 10, 0.681818, 1, '2017-09-15 08:09:32', '2017-09-15 08:09:32'),
(420, 442, 3, NULL, 7.5, 10, 0.681818, 1, '2017-09-15 08:09:34', '2017-09-15 08:09:34'),
(421, 444, 6, NULL, 4.5, 10, 0.409091, 4, '2017-09-15 10:16:08', '2017-09-15 10:52:39'),
(423, 444, 7, NULL, 39, 10, 3.54545, 2, '2017-09-15 10:22:09', '2017-09-15 10:50:59'),
(424, 449, 2, NULL, 9.5, 10, 0.863636, 1, '2017-10-05 10:47:03', '2017-10-05 10:47:03'),
(425, 449, 3, NULL, 7.5, 10, 0.681818, 2, '2017-10-05 11:01:04', '2017-10-05 11:18:28'),
(426, 451, 2, NULL, 9.5, 10, 0.863636, 1, '2017-10-05 11:20:57', '2017-10-05 11:20:57'),
(427, 452, 2, NULL, 9.5, 10, 0.863636, 1, '2017-10-05 12:19:10', '2017-10-05 12:19:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders__refunds`
--

CREATE TABLE `orders__refunds` (
  `id` int(11) NOT NULL,
  `NPedidoWeb` int(11) NOT NULL,
  `RefNum` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `Cliente` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `direccion` varchar(40) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `localidad` varchar(40) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `codigo_postal` varchar(20) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `pais` int(4) NOT NULL DEFAULT '0',
  `zona` int(4) NOT NULL DEFAULT '0',
  `metodo_pago` varchar(20) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `transportista` varchar(5) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `comentario` text COLLATE utf8_spanish2_ci,
  `mail` tinyint(1) NOT NULL DEFAULT '0',
  `creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders__request`
--

CREATE TABLE `orders__request` (
  `id` int(11) NOT NULL,
  `response` text CHARACTER SET utf8,
  `modification` datetime DEFAULT NULL,
  `creacion` datetime NOT NULL,
  `id_order` int(10) UNSIGNED DEFAULT NULL,
  `historico` tinyint(1) NOT NULL DEFAULT '0',
  `tipo` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `articulos` int(11) NOT NULL DEFAULT '0',
  `pvp_articulos` float NOT NULL DEFAULT '0',
  `pvp_total` float NOT NULL DEFAULT '0',
  `pvp_envio` float NOT NULL DEFAULT '0',
  `pvp_iva` float NOT NULL DEFAULT '0',
  `pvp_descuento` float NOT NULL DEFAULT '0',
  `pvp_pago` float NOT NULL DEFAULT '0',
  `pvp_tarjeta` float NOT NULL DEFAULT '0',
  `pvp_envolver` float NOT NULL DEFAULT '0',
  `pvp_puntos` int(11) NOT NULL DEFAULT '0',
  `errors` text COLLATE utf8_spanish2_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `orders__request`
--

INSERT INTO `orders__request` (`id`, `response`, `modification`, `creacion`, `id_order`, `historico`, `tipo`, `articulos`, `pvp_articulos`, `pvp_total`, `pvp_envio`, `pvp_iva`, `pvp_descuento`, `pvp_pago`, `pvp_tarjeta`, `pvp_envolver`, `pvp_puntos`, `errors`) VALUES
(210000379, '{\"Ds_Date\":\"17\\/04\\/2017\",\"Ds_Hour\":\"12:47\",\"Ds_SecurePayment\":\"1\",\"Ds_Card_Country\":\"724\",\"Ds_Amount\":\"3000\",\"Ds_Currency\":\"978\",\"Ds_Order\":\"210000379\",\"Ds_MerchantCode\":\"336054614\",\"Ds_Terminal\":\"002\",\"Ds_Response\":\"0000\",\"Ds_MerchantData\":\"\",\"Ds_TransactionType\":\"0\",\"Ds_ConsumerLanguage\":\"1\",\"Ds_AuthorisationCode\":\"057678\"}', NULL, '2017-04-17 12:47:41', 431, 1, 'tarjeta', 1, 30, 30, 5, 6.3, 0, 0, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_requests`
--

CREATE TABLE `password_requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `token` varchar(128) COLLATE utf8_spanish2_ci NOT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portes__paises`
--

CREATE TABLE `portes__paises` (
  `id` int(11) NOT NULL,
  `locale` varchar(5) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `code` varchar(2) CHARACTER SET latin1 DEFAULT NULL,
  `nombre` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `nombre_idioma` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '0',
  `envio` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `portes__paises`
--

INSERT INTO `portes__paises` (`id`, `locale`, `code`, `nombre`, `nombre_idioma`, `activo`, `envio`, `created_at`, `updated_at`) VALUES
(1, 'es_ES', 'es', 'Español', 'Español', 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portes__provincias`
--

CREATE TABLE `portes__provincias` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_pais` int(11) NOT NULL,
  `nombre` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre_idioma` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '0',
  `envio` tinyint(1) NOT NULL DEFAULT '0',
  `code` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `portes__provincias`
--

INSERT INTO `portes__provincias` (`id`, `id_pais`, `nombre`, `nombre_idioma`, `activo`, `envio`, `code`, `created_at`, `updated_at`) VALUES
(1, 1, 'Álava', 'Álava', 1, 1, 'alava', NULL, NULL),
(2, 1, 'Albacete', 'Albacete', 1, 1, 'albacete', NULL, NULL),
(3, 1, 'Alicante', 'Alicante', 1, 1, 'alicante', NULL, NULL),
(4, 1, 'Almería', 'Almería', 1, 1, 'almeria', NULL, NULL),
(5, 1, 'Asturias', 'Asturias', 1, 1, 'asturias', NULL, NULL),
(6, 1, 'Ávila', 'Ávila', 1, 1, 'avila', NULL, NULL),
(7, 1, 'Badajoz', 'Badajoz', 1, 1, 'badajoz', NULL, NULL),
(8, 1, 'Barcelona', 'Barcelona', 1, 1, 'barcelona', NULL, NULL),
(9, 1, 'Burgos', 'Burgos', 1, 1, 'burgos', NULL, NULL),
(10, 1, 'Cáceres', 'Cáceres', 1, 1, 'caceres', NULL, NULL),
(11, 1, 'Cádiz', 'Cádiz', 1, 1, 'cadiz', NULL, NULL),
(12, 1, 'Cantabria', 'Cantabria', 1, 1, 'cantabria', NULL, NULL),
(13, 1, 'Castellón', 'Castellón', 1, 1, 'castellon', NULL, NULL),
(14, 1, 'Ceuta', 'Ceuta', 1, 1, 'ceuta', NULL, NULL),
(15, 1, 'Ciudad Real', 'Ciudad Real', 1, 1, 'ciudad_real', NULL, NULL),
(16, 1, 'Córdoba', 'Córdoba', 1, 1, 'cordoba', NULL, NULL),
(17, 1, 'Cuenca', 'Cuenca', 1, 1, 'cuenca', NULL, NULL),
(18, 1, 'Gerona', 'Gerona', 1, 1, 'gerona', NULL, NULL),
(19, 1, 'Granada', 'Granada', 1, 1, 'granada', NULL, NULL),
(20, 1, 'Guadalajara', 'Guadalajara', 1, 1, 'guadalajara', NULL, NULL),
(21, 1, 'Guipúzcoa', 'Guipúzcoa', 1, 1, 'guipuzcoa', NULL, NULL),
(22, 1, 'Huelva', 'Huelva', 1, 1, 'huelva', NULL, NULL),
(23, 1, 'Huesca', 'Huesca', 1, 1, 'huesca', NULL, NULL),
(24, 1, 'Islas Baleares', 'Islas Baleares', 1, 1, 'islas_baleares', NULL, NULL),
(25, 1, 'Jaén', 'Jaén', 1, 1, 'jaen', NULL, NULL),
(26, 1, 'La Coruña', 'La Coruña', 1, 1, 'la_coruña', NULL, NULL),
(27, 1, 'La Rioja', 'La Rioja', 1, 1, 'la_rioja', NULL, NULL),
(28, 1, 'Las Palmas', 'Las Palmas', 1, 1, 'las_palmas', NULL, NULL),
(29, 1, 'León', 'León', 1, 1, 'leon', NULL, NULL),
(30, 1, 'Lérida', 'Lérida', 1, 1, 'lleida', NULL, NULL),
(31, 1, 'Lugo', 'Lugo', 1, 1, 'lugo', NULL, NULL),
(32, 1, 'Madrid', 'Madrid', 1, 1, 'madrid', NULL, NULL),
(33, 1, 'Málaga', 'Málaga', 1, 1, 'malaga', NULL, NULL),
(34, 1, 'Melilla', 'Melilla', 1, 1, 'melilla', NULL, NULL),
(35, 1, 'Murcia', 'Murcia', 1, 1, 'murcia', NULL, NULL),
(36, 1, 'Navarra', 'Navarra', 1, 1, 'navarra', NULL, NULL),
(37, 1, 'Orense', 'Orense', 1, 1, 'orense', NULL, NULL),
(38, 1, 'Palencia', 'Palencia', 1, 1, 'palencia', NULL, NULL),
(39, 1, 'Pontevedra', 'Pontevedra', 1, 1, 'pontevedra', NULL, NULL),
(40, 1, 'Salamanca', 'Salamanca', 1, 1, 'salamanca', NULL, NULL),
(41, 1, 'Segovia', 'Segovia', 1, 1, 'segovia', NULL, NULL),
(42, 1, 'Sevilla', 'Sevilla', 1, 1, 'sevilla', NULL, NULL),
(43, 1, 'Soria', 'Soria', 1, 1, 'soria', NULL, NULL),
(44, 1, 'Tarragona', 'Tarragona', 1, 1, 'tarragona', NULL, NULL),
(45, 1, 'Tenerife', 'Tenerife', 1, 1, 'tenerife', NULL, NULL),
(46, 1, 'Teruel', 'Teruel', 1, 1, 'teruel', NULL, NULL),
(47, 1, 'Toledo', 'Toledo', 1, 1, 'toledo', NULL, NULL),
(48, 1, 'Valencia', 'Valencia', 1, 1, 'valencia', NULL, NULL),
(49, 1, 'Valladolid', 'Valladolid', 1, 1, 'valladolid', NULL, NULL),
(50, 1, 'Vizcaya', 'Vizcaya', 1, 1, 'vizcaya', NULL, NULL),
(51, 1, 'Zamora', 'Zamora', 1, 1, 'zamora', NULL, NULL),
(52, 1, 'Zaragoza', 'Zaragoza', 1, 1, 'zaragoza', NULL, NULL),
(53, 2, 'Aveiro', 'Aveiro', 0, 0, 'aveiro', NULL, NULL),
(54, 2, 'Beja', 'Beja', 0, 0, 'beja', NULL, NULL),
(55, 2, 'Braga', 'Braga', 0, 0, 'braga', NULL, NULL),
(56, 2, 'Bragança', 'Bragança', 0, 0, 'braganca', NULL, NULL),
(57, 2, 'Castelo Branco', 'Castelo Branco', 0, 0, 'castelo_branco', NULL, NULL),
(58, 2, 'Coimbra', 'Coimbra', 0, 0, 'coimbra', NULL, NULL),
(59, 2, 'Évora', 'Évora', 0, 0, 'evora', NULL, NULL),
(60, 2, 'Faro', 'Faro', 0, 0, 'faro', NULL, NULL),
(61, 2, 'Guarda', 'Guarda', 0, 0, 'guarda', NULL, NULL),
(62, 2, 'Leiria', 'Leiria', 0, 0, 'leiria', NULL, NULL),
(63, 2, 'Lisboa', 'Lisboa', 0, 0, 'lisboa', NULL, NULL),
(64, 2, 'Portalegre', 'Portalegre', 0, 0, 'portalegre', NULL, NULL),
(65, 2, 'Porto', 'Porto', 0, 0, 'porto', NULL, NULL),
(66, 2, 'Santarém', 'Santarém', 0, 0, 'santarem', NULL, NULL),
(67, 2, 'Setúbal', 'Setúbal', 0, 0, 'setubal', NULL, NULL),
(68, 2, 'Viana do Castelo', 'Viana do Castelo', 0, 0, 'viana_do_castelo', NULL, NULL),
(69, 2, 'Vila Real', 'Vila Real', 0, 0, 'vila_real', NULL, NULL),
(70, 2, 'Viseu', 'Viseu', 0, 0, 'viseu', NULL, NULL),
(71, 2, 'Açores', 'Açores', 0, 0, 'azores', NULL, NULL),
(72, 2, 'Madeira', 'Madeira', 0, 0, 'madeira', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portes__zonas`
--

CREATE TABLE `portes__zonas` (
  `id` int(11) NOT NULL,
  `id_pais` varchar(4) COLLATE utf8_spanish2_ci NOT NULL,
  `id_provincia` varchar(4) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `id_transportista` varchar(5) COLLATE utf8_spanish2_ci NOT NULL,
  `pvp` float NOT NULL,
  `pvp_rebaja` float NOT NULL,
  `pvp_devolver` float NOT NULL,
  `limite_rebaja` float NOT NULL,
  `is_rebaja` tinyint(1) NOT NULL DEFAULT '0',
  `dias_entrega` int(2) UNSIGNED NOT NULL DEFAULT '2',
  `activo` tinyint(1) NOT NULL DEFAULT '0',
  `envio` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preferences`
--

CREATE TABLE `preferences` (
  `id` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `text_1` text COLLATE utf8_spanish2_ci NOT NULL,
  `guarded` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `preferences`
--

INSERT INTO `preferences` (`id`, `text_1`, `guarded`, `created_at`, `updated_at`) VALUES
('analytics_google_analytics', '', 0, NULL, NULL),
('contacto_direccion', '<p>asdasd</p>', 0, NULL, NULL),
('contacto_email', 'cesar@euphorbia.es', 0, NULL, NULL),
('contacto_facebook', '', 0, NULL, NULL),
('contacto_instagram', 'asd', 0, NULL, NULL),
('contacto_nombre', 'Lorena Euphorbia', 0, NULL, NULL),
('contacto_twitter', 'asd', 0, NULL, NULL),
('contacto_youtube', 'asd', 0, NULL, NULL),
('forms_email', 'cesar@euphorbia.es', 1, NULL, NULL),
('forms_smtp_auth', '1', 1, NULL, NULL),
('forms_smtp_password', 'Cfrw2#39', 1, NULL, NULL),
('forms_smtp_port', '25', 1, NULL, NULL),
('forms_smtp_security', '', 1, NULL, NULL),
('forms_smtp_server', 'mail.en-pruebas.com', 1, NULL, NULL),
('forms_smtp_user', 'formularios@en-pruebas.com', 1, NULL, NULL),
('general_meta_description', 'Más de 20 años en distribución de todo tipo de productos alimenticios en Castilla y León y Cantabria.', 0, NULL, NULL),
('general_meta_title', 'Distribuidor de alimentos en Castilla y León', 0, NULL, NULL),
('general_titulo_web', 'Alfonso Peña :: Comercial de productos alimenticios', 0, NULL, NULL),
('legal_aviso_legal', '<p>La web www.comercialap.com tiene por objeto facilitar el conocimiento por el público en general de las actividades que realiza y de los servicios profesionales que presta.</p>\r\n<p><br />\r\n<strong>1. Información general para dar cumplimiento a la Ley 34/2002</strong><br />\r\n<br />\r\nTitular: Alfonso Peña S.L.<br />\r\nDirección: Calle Málaga, 29, 34004 Palencia (España)<br />\r\nContacto: info@comercialap.com o +34 979-165-827</p>\r\n<p><br />\r\nDatos relativos a la empresa: inscrita en el registro mercantil de Palencia, Folio 111, Tomo 72 General, Libro14, Sección 2ª de Sociedades, Hoja nº28 Inscripción 1ª<br />\r\nR.S.I. 4009851/P</p>\r\n<p>CIF de empresa: B34030072</p>\r\n<p><br />\r\n<strong>2. Propiedad intelectual</strong><br />\r\n<br />\r\nLos contenidos suministrados por www.comercialap.com están sujetos a los derechos de propiedad intelectual e industrial y son titularidad exclusiva de Alfonso Peña S.L.<br />\r\n<br />\r\nMediante la adquisición de un producto o servicio, Alfonso Peña S.L no confiere al adquirente ningún derecho de alteración, explotación, reproducción o distribución del mismo fuera de lo estrictamente contratado, reservándose Alfonso Peña S.L todos estos derechos.<br />\r\n<br />\r\nLa cesión de los citados derechos precisará el previo consentimiento por escrito por parte de Alfonso Peña S.L.<br />\r\n<br />\r\nLa propiedad intelectual se extiende, además del contenido incluido en www.comercialap.com, a sus gráficos, logotipos, diseño, imágenes y código fuente utilizado para su programación. Cualquier acto de transmisión, distribución, cesión, reproducción, almacenaje o comunicación pública total o parcial, debe contar con el consentimiento expreso de Alfonso Peña S.L.<br />\r\n<br />\r\n<strong>3. Cláusula de exención de responsabilidad</strong><br />\r\n<br />\r\nEsta página web puede contener enlaces a otras páginas que Alfonso Peña S.L no puede controlar, por lo tanto, Alfonso Peña S.L declina cualquier responsabilidad respecto a la información que pueda aparecer en páginas de terceros.</p>', 0, NULL, NULL),
('legal_politica_de_cookies', '<p>El prestador por su propia cuenta o la de un tercero contratado para la prestación de servicios de medición, pueden utilizar cookies cuando un usuario navega por el sitio web. Las cookies son ficheros enviados al navegador por medio de un servidor web con la finalidad de registrar las actividades del usuario durante su tiempo de navegación.</p>\r\n<p>Las cookies utilizadas por el sitio web se asocian únicamente con un usuario anónimo y su ordenador, y no proporcionan por sí mismas los datos personales del usuario.</p>\r\n<p>Mediante el uso de las cookies resulta posible que el servidor donde se encuentra la web, reconozca el navegador web utilizado por el usuario con la finalidad de que la navegación sea más sencilla, permitiendo, por ejemplo, el acceso a los usuarios que se hayan registrado previamente, acceder a las áreas, servicios, promociones o concursos reservados exclusivamente a ellos sin tener que registrarse en cada visita. Se utilizan también para medir la audiencia y parámetros del tráfico, controlar el progreso y número de entradas.</p>\r\n<p>El usuario tiene la posibilidad de configurar su navegador para ser avisado de la recepción de cookies y para impedir su instalación en su equipo. Por favor, consulte las instrucciones y manuales de su navegador para ampliar esta información.</p>\r\n<p>Para utilizar el sitio web, no resulta necesario que el usuario permita la instalación de las cookies enviadas por el sitio web, o el tercero que actúe en su nombre, sin perjuicio de que sea necesario que el usuario inicie una sesión como tal en cada uno de los servicios cuya prestación requiera el previo registro o \"login\".</p>\r\n<p>Las cookies utilizadas en este sitio web tienen, en todo caso, carácter temporal con la única finalidad de hacer más eficaz su transmisión ulterior. En ningún caso se utilizarán las cookies para recoger información de carácter personal.</p>\r\n<p>Tabla de cookies utilizadas:</p>\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th>Proveedor</th>\r\n			<th>Nombre</th>\r\n			<th>Propósito</th>\r\n			<th>Más información</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Comercial AP</td>\r\n			<td>PHPSESSID</td>\r\n			<td>Permite al usuario visualizar la página e interactuar con ella. Es esencial para el funcionamiento correcto de la página.</td>\r\n			<td>\r\n			<p><a href=\"/aviso-legal/\">Aviso legal</a></p>\r\n			<p><a href=\"/politica-de-privacidad/\">Política de privacidad</a></p></td>\r\n		</tr>\r\n		<tr>\r\n			<td>Google Analytics</td>\r\n			<td>__utma<br />\r\n			__utmb<br />\r\n			__utmc<br />\r\n			__utmz</td>\r\n			<td>Recopilan información anónima sobre la navegación de los usuarios por el sitio web con el fin de conocer el origen de las visitas y otros datos estadísticos similares.</td>\r\n			<td>\r\n			<p><a href=\"https://www.google.com/intl/es/policies/\" target=\"_blank\">Centro de privacidad de Google</a></p>\r\n			<p><a href=\"http://tools.google.com/dlpage/gaoptout?hl=es\" target=\"_blank\">Complemento de inhabilitación de Google Analytics</a></p></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>Para desactivar las cookies, el usuario podrá, en cualquier momento, elegir cuáles quiere que funcionen en este sitio web mediante:</p>\r\n<ul>\r\n	<li>la configuración del navegador; por ejemplo:\r\n	<ul>\r\n		<li>Firefox, desde <a href=\"http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we\" target=\"_blank\">http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we</a></li>\r\n		<li>Chrome, desde <a href=\"http://support.google.com/chrome/bin/answer.py?hl=es&amp;answer=95647\" target=\"_blank\">http://support.google.com/chrome/bin/answer.py?hl=es&amp;answer=95647</a></li>\r\n		<li>Explorer, desde <a href=\"http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9\" target=\"_blank\">http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9</a></li>\r\n		<li>Safari, desde <a href=\"http://support.apple.com/kb/ph5042\" target=\"_blank\">http://support.apple.com/kb/ph5042</a></li>\r\n	</ul></li>\r\n	<li>los sistemas de opt-out específicos indicados en la tabla anterior respecto de la cookie de que se trate (estos sistemas pueden conllevar que se instale en su equipo una cookie \"de rechazo\" para que funcione su elección de desactivación)</li>\r\n	<li>otras herramientas de terceros, disponibles on line, que permiten a los usuarios detectar las cookies en cada sitio web que visita y gestionar su desactivación (por ejemplo, Ghostery: <a href=\"http://www.ghostery.com/privacy-statement\" target=\"_blank\">http://www.ghostery.com/privacy-statement</a>, <a href=\"http://www.ghostery.com/faq\" target=\"_blank\">http://www.ghostery.com/faq</a>).</li>\r\n</ul>', 0, NULL, NULL),
('legal_politica_de_privacidad', '<p>A través de la página www.comerciaap.com no se recoge ningún dato personal sin su conocimiento, ni se ceden a terceros.<br />\r\n<br />\r\nLos datos personales proporcionados a www.comercialap.com, a través de sus formularios de contacto o por los clientes que adquieran productos o servicios de www.comercialap.com, van a ser almacenados en un fichero automatizado de datos, propiedad de Alfonso Peña S.L., para la finalidad única de la gestión de la empresa.<br />\r\n<br />\r\nAlfonso Peña S.L. se compromete a tratar los datos de manera lícita y responsable, de acuerdo con la Ley Orgánica 15/1999, de Protección de Datos de Carácter Personal, garantizando el Derecho a la Intimidad personal y familiar, el honor y el Derecho Fundamental a la protección de los Datos Personales de todas las personas que se encuentren en sus ficheros.<br />\r\n<br />\r\nTodo aquel que proporcione sus datos personales a Alfonso Peña S.L. a través de la web www.comercialap.com tendrá la posibilidad de revocar su consentimiento en cualquier momento, o de ejercitar sus derechos de Acceso, Rectificación, Cancelación y Oposición, por cualquier medio admitido en derecho que permita acreditar la personalidad. Para ello deberá dirigirse a Alfonso Peña S.L, Calle Málaga, 29, 34004 Palencia (España)o a través de correo electrónico a la dirección info@comercialap.com bajo el asunto \"Datos personales\" o también a través del teléfono +34 979-165-827.</p>', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preferences__files`
--

CREATE TABLE `preferences__files` (
  `id` int(10) UNSIGNED NOT NULL,
  `owner_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'propietario (id)',
  `owner_type` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'propietario (mod)',
  `type` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'tipo de archivo',
  `title_1` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'título',
  `name` text COLLATE utf8_spanish2_ci COMMENT 'nombre original',
  `mime_type` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'tipo recogido',
  `size` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'tamaño',
  `height` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'alto (si imagen)',
  `width` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ancho (si imagen)',
  `creacion` timestamp NULL DEFAULT NULL,
  `modificacion` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `preferences__files`
--

INSERT INTO `preferences__files` (`id`, `owner_id`, `owner_type`, `type`, `title_1`, `name`, `mime_type`, `size`, `height`, `width`, `creacion`, `modificacion`) VALUES
(3, 3, 'Galeria', 'galeria', 'Tanco.jpg', 'Tanco.jpg', 'image/jpeg', 230921, 500, 1600, '2017-01-29 20:18:31', NULL),
(4, 4, 'Galeria', 'galeria', 'Krone.jpg', 'Krone.jpg', 'image/jpeg', 280810, 500, 1600, '2017-01-29 20:19:00', NULL),
(5, 1, 'Johndeere', 'logo-john-deere', 'John_Deere.png', 'John_Deere.png', 'image/png', 4853, 60, 250, '2017-01-29 18:56:54', NULL),
(7, 5, 'Galeria', 'galeria', 'amazone.jpg', 'amazone.jpg', 'image/jpeg', 318592, 500, 1600, '2017-01-29 20:16:32', NULL),
(8, 6, 'Galeria', 'galeria', 'BvL.jpg', 'BvL.jpg', 'image/jpeg', 230945, 500, 1600, '2017-01-29 20:17:02', NULL),
(9, 7, 'Galeria', 'galeria', 'Kramer.jpg', 'Kramer.jpg', 'image/jpeg', 255744, 500, 1600, '2017-01-29 20:17:27', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preferences__galeria`
--

CREATE TABLE `preferences__galeria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `alt` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `link` varchar(250) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '/',
  `link_1` varchar(250) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '/',
  `nombre_1` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `alt_1` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `id_tipo` int(11) DEFAULT NULL,
  `id_preferencia` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `creacion` timestamp NULL DEFAULT NULL,
  `modificacion` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `preferences__galeria`
--

INSERT INTO `preferences__galeria` (`id`, `nombre`, `alt`, `link`, `link_1`, `nombre_1`, `alt_1`, `id_tipo`, `id_preferencia`, `owner_id`, `position`, `creacion`, `modificacion`) VALUES
(3, '', 'Tanco', '/', '/', '', 'Tanco', 1, '1', 0, 0, '2017-01-27 12:57:43', NULL),
(4, '', 'Krone', '/', '/', '', 'Krone', 1, '1', 0, 0, '2017-01-27 12:58:08', NULL),
(5, '', 'amazone', '/', '/', '', 'amazone', 1, '1', 0, 0, '2017-01-29 20:16:31', NULL),
(6, '', 'BvL', '/', '/', '', 'BvL', 1, '1', 0, 0, '2017-01-29 20:17:02', NULL),
(7, '', 'Kramer', '/', '/', '', 'Kramer', 1, '1', 0, 0, '2017-01-29 20:17:27', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `meta_titulo` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `meta_descripcion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8_spanish2_ci,
  `caracteristicas` text COLLATE utf8_spanish2_ci,
  `formato` text COLLATE utf8_spanish2_ci,
  `ingredientes` text COLLATE utf8_spanish2_ci,
  `id_categoria` int(11) DEFAULT '0',
  `precio` float NOT NULL DEFAULT '0',
  `precio_rebajado` float NOT NULL DEFAULT '0',
  `iva` float NOT NULL DEFAULT '4',
  `stock` int(11) NOT NULL DEFAULT '0',
  `peso` int(11) NOT NULL DEFAULT '0',
  `etiqueta` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `contenido` int(4) NOT NULL DEFAULT '100',
  `publicado` tinyint(1) NOT NULL DEFAULT '0',
  `es_novedad` tinyint(1) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `codigo`, `nombre`, `meta_titulo`, `meta_descripcion`, `url`, `descripcion`, `caracteristicas`, `formato`, `ingredientes`, `id_categoria`, `precio`, `precio_rebajado`, `iva`, `stock`, `peso`, `etiqueta`, `contenido`, `publicado`, `es_novedad`, `position`, `locked`, `created_at`, `updated_at`) VALUES
(1, '5099-K0001', 'Aceite de oliva Picual', 'Selectisimo :: Botella de Aceite de oliva Picual 750ml', 'Botella de Aceite de oliva Aceite de oliva Picual 750ml de DO..., Selectisimo tu tienda Gourmet,.. aceites de la mejor calidad de las distintas DO españolas,..', 'aceite-de-oliva-picual', 'El aceite de oliva es de color verde intenso. En la boca se aprecia su frescura, su amargor y el picante equilibrados, con un fuerte regusto a almendra. En mismo tiempo su aroma a frutas verdes,  hierba recién cortada, manzana, tomate e higuera es muy intenso. A parte el aceite de oliva picual Nº3 Elizondo está presentado en un bonito envase, que parece un caro perfume. Es una auténtica maravilla por lo tanto será muy buen detalle o regalo. En nuestra tienda online tenemos varios aceites Elizondo.', NULL, 'Botella de 750ml', 'aceite de oliva', 1, 8, 7.5, 10, 5, 0, '', 100, 1, 0, 0, 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(2, 'D5EA-C0002', 'Aceite de oliva Arbequina', 'Selectisimo :: Botella de Aceite de oliva Arbequina 1L', 'Botella de Aceite de oliva Arbequina 1L de DO..., Selectisimo tu tienda Gourmet,.. aceites de la mejor calidad de las distintas DO españolas,..', 'aceite-de-oliva-arbequina', 'El aceite de oliva es de color verde intenso. En la boca se aprecia su frescura, su amargor y el picante equilibrados, con un fuerte regusto a almendra. En mismo tiempo su aroma a frutas verdes,  hierba recién cortada, manzana, tomate e higuera es muy intenso. A parte el aceite de oliva picual Nº3 Elizondo está presentado en un bonito envase, que parece un caro perfume. Es una auténtica maravilla por lo tanto será muy buen detalle o regalo. En nuestra tienda online tenemos varios aceites Elizondo.', NULL, 'Botella de 1L', 'aceite de oliva', 1, 9.5, 9.5, 10, 0, 0, '', 100, 1, 1, 0, 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(3, '5B85-L0003', 'Jamón Ibérico de cebo 300gr', 'Selectisimo :: Jamón Ibérico de cebo de Salamanca', 'Jamón Ibérico de cebo de Salamanca..., Selectisimo tu tienda Gourmet,.. jamónería selecta con los mejores jamones de la zona de guijuelo..', 'jamon-iberico-de-cebo-300gr', 'Producto Certificado con una curación mínima de 16 meses, elaborada a partir de cerdos grasos.', NULL, 'Envase loncheado de 300gr. Listo para el consumo.', 'Jamón magro de cerdo, sal', 3, 8, 7.5, 10, 12, 0, '', 100, 1, 1, 0, 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(4, 'F2A7-H0004', 'Macaronias Enrique Rech 12 Unidades', 'Selectisimo :: Macaronias Enrique Rech 12 Unidades', '', 'macaronias-enrique-rech-12-unidades', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', NULL, 'Caja de 12 Unidades.', 'Lorem ipsum dolor sit amet,', 4, 8.3, 8.3, 10, 20, 0, '', 100, 1, 0, 0, 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(5, '191A-V0005', 'Mermelada la Artesana', 'Selectisimo :: Mermelada la Artesana', '', 'mermelada-la-artesana', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', NULL, 'Bote de 200ml.', 'Lorem ipsum dolor sit amet,', 4, 3.2, 2.9, 10, 2, 0, '', 100, 1, 1, 0, 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(6, '2790-Q0006', 'Las Morenitas de Puri. Almendras Garrapiñadas', 'Selectisimo :: Las Morenitas de Puri. Almendras Garrapiñadas', '', 'las-morenitas-de-pur-almendras-garrapinadas', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', NULL, 'Bote de cristal con 100gr de almendras.', 'Lorem ipsum dolor sit amet,', 4, 4.5, 4.5, 10, 3, 0, '', 100, 1, 1, 0, 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(7, '7988-S0007', 'Queso Ideazábal de Pastor Akelarre ED. Limitada 1K', 'Selectisimo :: Queso Ideazábal de Pastor Akelarre ED. Limitada 1K', '', 'queso-ideazabal-de-pastor-akelarre-ed-limitada-1k', 'Queso elaborado exclusivamente con leche cruda de oveja de raza Letxa de nuestra propia cabaña y con una maduración  mínima de sesenta días.', NULL, 'Forma: Cilíndrica, con caras sensiblemente planas./ /Altura: Entre 8 y 12 centímetros./ /Diámetro: De 13 a 16 centímetros./ /Peso: De 1.100 a 1.400 kilogramos./ /Corteza: Dura, de color amarillo pálido.', 'Leche cruda de oveja, sal', 4, 39, 39, 10, 4, 0, '', 100, 1, 0, 0, 0, '2017-09-01 06:47:23', '2017-09-01 06:47:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__categorias`
--

CREATE TABLE `productos__categorias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `url` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `titulo` varchar(120) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `meta_titulo` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `meta_descripcion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `owner_id` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos__categorias`
--

INSERT INTO `productos__categorias` (`id`, `nombre`, `url`, `titulo`, `meta_titulo`, `meta_descripcion`, `position`, `owner_id`, `created_at`, `updated_at`) VALUES
(1, 'Aceites con DO', 'aceites-con-do', 'Aceites con denominación de origen :: Selectisimo', 'Selectisimo :: Aceites selectos con denominación de origen', 'Selectisimo tu tienda Gourmet,.. Aceites selectos de las mejores DO españolas, carnes grasas certificadas,..', 0, 0, '2017-09-01 06:44:38', '2017-09-01 06:44:38'),
(2, 'Loncheados', 'loncheados', 'Selección Gourmet de Loncheados :: Selectisimo', 'Selectisimo :: Charcutería Gourmet los mejores Jamones y loncheados', 'Selectisimo tu tienda Gourmet,.. Loncheados selectos de las mejores DO españolas, jamones,... carnes grasas certificadas,..', 0, 0, '2017-09-01 06:44:38', '2017-09-01 06:44:38'),
(3, 'Jamones ibéricos', 'jamones', 'Jamones ibéricos con DO :: Selectisimo', 'Selectisimo :: Jamones Selectos con denominación de origen', 'Selectisimo tu tienda Gourmet,.. Jamones grasos,..', 0, 2, '2017-09-01 06:44:38', '2017-09-01 06:44:38'),
(4, 'Dulces Y Mermeladas', 'dulces-y-mermeladas', 'Dulces Y Mermeladas :: Selectisimo', 'Selectisimo :: Dulces Y Mermeladas', 'Selectisimo tu tienda Gourmet,.. Dulces Y Mermeladas,..', 0, 0, '2017-09-01 06:44:38', '2017-09-01 06:44:38'),
(5, 'Quesos & Patés', 'quesos-y-pates', 'Quesos & Patés :: Selectisimo', 'Selectisimo :: Dulces Y Mermeladas', 'Selectisimo tu tienda Gourmet,.. Quesos & Patés,..', 0, 0, '2017-09-01 06:44:38', '2017-09-01 06:44:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__comments`
--

CREATE TABLE `productos__comments` (
  `id` int(11) NOT NULL,
  `id_parent` int(11) NOT NULL DEFAULT '0',
  `id_usuario` int(11) NOT NULL DEFAULT '0',
  `nombre_usuario` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email_usuario` varchar(120) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `titulo` varchar(200) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `comentario` text COLLATE utf8_spanish2_ci,
  `puntuacion` int(11) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos__comments`
--

INSERT INTO `productos__comments` (`id`, `id_parent`, `id_usuario`, `nombre_usuario`, `email_usuario`, `titulo`, `comentario`, `puntuacion`, `position`, `owner_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 122, 5, 'test', 'test', '', '', 0, 0, 0, NULL, NULL, NULL),
(2, 56, 5, 'César', 'cevm88@gmail.com', 'Buenísimo', '<p>Lorem Ipsum Dolor</p>', 3, 0, 0, NULL, NULL, NULL),
(3, 56, 5, 'César Vega', 'cesar.euphorbia@gmail.com', 'Me encataron', '<p>test</p>', 5, 0, 0, NULL, NULL, NULL),
(4, 1, 2, 'Lorena F', 'cesar.euphorbia@gmail.com', 'Me encataron', 'prueba', 5, 0, 0, NULL, NULL, NULL),
(5, 1, 5, 'Cesar V.M.', 'cesar.euphorbia@gmail.com', 'Me encataron', 'prueba', 5, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__files`
--

CREATE TABLE `productos__files` (
  `id` int(10) UNSIGNED NOT NULL,
  `owner_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'propietario (id)',
  `owner_type` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'propietario (mod)',
  `type` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'tipo de archivo',
  `title_1` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'título',
  `name` text COLLATE utf8_spanish2_ci COMMENT 'nombre original',
  `mime_type` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'tipo recogido',
  `size` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'tamaño',
  `height` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'alto (si imagen)',
  `width` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ancho (si imagen)',
  `path` varchar(200) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `creacion` timestamp NULL DEFAULT NULL,
  `modificacion` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos__files`
--

INSERT INTO `productos__files` (`id`, `owner_id`, `owner_type`, `type`, `title_1`, `name`, `mime_type`, `size`, `height`, `width`, `path`, `creacion`, `modificacion`) VALUES
(35, 52, 'Galeria', '0000000001', 'campanal.jpg', 'hola', 'image/jpeg', 24243, 414, 508, NULL, '2017-07-13 10:48:40', '2017-07-13 10:48:40'),
(36, 52, 'Galeria', '0000000001', 'distribuidor_leon.png', 'f1231', 'image/png', 33431, 320, 320, NULL, '2017-07-13 11:03:03', '2017-07-13 11:03:03'),
(48, 55, 'Galeria', '0000000001', 'aceitunas_sarasa1.jpg', 'qewqeq', 'image/jpeg', 231148, 243, 556, NULL, '2017-07-26 05:36:36', '2017-07-26 05:36:36'),
(49, 55, 'Productos', 'img', 'aceites.jpg', 'aceites.jpg', 'image/jpeg', 33709, 414, 508, NULL, '2017-07-27 09:11:33', NULL),
(50, 552, 'Productos', 'img', 'aceites.jpg', 'aceites.jpg', 'image/jpeg', 33709, 414, 508, NULL, '2017-07-27 10:45:04', NULL),
(53, 58, 'Productos', 'img', 'distribuidor_burgos.png', 'distribuidor_burgos.png', 'image/png', 36416, 320, 320, NULL, '2017-07-27 11:08:44', NULL),
(55, 56, 'Productos', 'img', 'aceites.jpg', 'aceites.jpg', 'image/jpeg', 33709, 414, 508, NULL, '2017-07-28 06:53:34', NULL),
(76, 126, 'productos', 'img', 'descarga.jpg', 'descarga.jpg', 'image/jpeg', 7121, 225, 225, NULL, '2017-08-08 11:53:46', NULL),
(94, 8, 'productos', 'img', 'paleta-iberica-monte-nevado-sin-hueso-4.jpg', 'paleta-iberica-monte-nevado-sin-hueso-4.jpg', 'image/jpeg', 103687, 300, 400, NULL, '2017-08-16 11:51:15', NULL),
(95, 9, 'productos', 'img', 'jamonibericobellota-400x300.jpg', 'jamonibericobellota-400x300.jpg', 'image/jpeg', 33664, 300, 400, NULL, '2017-08-16 11:58:40', NULL),
(96, 10, 'productos', 'img', 'images.jpg', 'images.jpg', 'image/jpeg', 8675, 173, 292, NULL, '2017-08-16 12:03:16', NULL),
(99, 12, 'productos', 'img', 'salchichon-iberico-de-bellota-pata-negra-4.jpg', 'salchichon-iberico-de-bellota-pata-negra-4.jpg', 'image/jpeg', 90293, 300, 400, NULL, '2017-08-16 12:19:54', NULL),
(100, 12, 'productos', 'img', 'salchichon-iberico-de-bellota-pata-negra-4.jpg', 'salchichon-iberico-de-bellota-pata-negra-4.jpg', 'image/jpeg', 90293, 300, 400, NULL, '2017-08-16 12:20:03', NULL),
(101, 12, 'productos', 'img', 'salchichon-iberico-de-bellota-pata-negra-4.jpg', 'salchichon-iberico-de-bellota-pata-negra-4.jpg', 'image/jpeg', 90293, 300, 400, NULL, '2017-08-16 12:20:20', NULL),
(102, 12, 'productos', 'img', 'salchichon-iberico-de-bellota-pata-negra-4.jpg', 'salchichon-iberico-de-bellota-pata-negra-4.jpg', 'image/jpeg', 90293, 300, 400, NULL, '2017-08-16 12:20:32', NULL),
(103, 11, 'productos', 'img', 'foto llangonissa talls.jpg', 'foto llangonissa talls.jpg', 'image/jpeg', 32313, 300, 400, NULL, '2017-08-16 12:21:02', NULL),
(104, 13, 'productos', 'img', 'salchichon-iberico-dehesa-cordobesa-1.jpg', 'salchichon-iberico-dehesa-cordobesa-1.jpg', 'image/jpeg', 57813, 300, 400, NULL, '2017-08-16 12:44:04', NULL),
(105, 4, 'Galeria', 'F2A7-H0004', 'macarons.jpg', 'foto_frontal', 'image/jpeg', 276185, 745, 745, NULL, '2017-09-01 06:47:56', '2017-09-01 06:47:56'),
(106, 5, 'Galeria', '191A-V0005', 'crema-de-queso-con-miel.jpg', 'foto_lateral', 'image/jpeg', 47102, 500, 500, NULL, '2017-09-01 06:49:14', '2017-09-01 06:49:14'),
(107, 6, 'Galeria', '2790-Q0006', 'garrapinadas.jpg', 'foto_lateral', 'image/jpeg', 148052, 414, 650, NULL, '2017-09-01 06:49:38', '2017-09-01 06:49:38'),
(108, 7, 'Galeria', '7988-S0007', '00113301500826____5__640x640.jpg', 'foto_inicial', 'image/jpeg', 135926, 640, 640, NULL, '2017-09-01 06:50:24', '2017-09-01 06:50:24'),
(109, 7, 'Galeria', '7988-S0007', 'akelarre2.png', 'foto_1', 'image/png', 216089, 650, 540, NULL, '2017-09-01 06:50:24', '2017-09-01 06:50:24'),
(110, 7, 'Galeria', '7988-S0007', 'akelarre3.png', 'foto_2', 'image/png', 348859, 439, 644, NULL, '2017-09-01 06:50:24', '2017-09-01 06:50:24'),
(111, 1, 'Galeria', '5099-K0001', 'aceite-oliva-magnasur-picual-500-ml_m.jpg', 'foto_inicial', 'image/jpeg', 23795, 500, 500, NULL, '2017-09-01 06:52:05', '2017-09-01 06:52:05'),
(112, 2, 'Galeria', 'D5EA-C0002', 'CASAS-DE-HUALDO-ARBEQUINA1.png', 'foto_inicial', 'image/png', 32012, 400, 400, NULL, '2017-09-01 06:52:50', '2017-09-01 06:52:50'),
(113, 3, 'Galeria', '5B85-L0003', 'jamon-joselito-gran-reserva-bellota.jpg', 'foto_inicial', 'image/jpeg', 24768, 458, 458, NULL, '2017-09-01 06:53:08', '2017-09-01 06:53:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__galeria`
--

CREATE TABLE `productos__galeria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `alt` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `link` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__relaccionados`
--

CREATE TABLE `productos__relaccionados` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `id_producto_relaccionado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos__relaccionados`
--

INSERT INTO `productos__relaccionados` (`id`, `id_producto`, `id_producto_relaccionado`) VALUES
(1, 56, 56),
(2, 56, 58),
(3, 6, 4),
(4, 4, 4),
(5, 4, 6),
(6, 4, 7),
(7, 4, 8),
(8, 7, 4),
(9, 7, 6),
(10, 7, 7),
(11, 7, 8),
(12, 8, 3),
(13, 8, 6),
(14, 8, 8),
(15, 9, 4),
(16, 9, 6),
(17, 9, 7),
(18, 9, 8),
(19, 10, 4),
(20, 10, 6),
(21, 10, 7),
(22, 10, 8),
(23, 10, 9),
(24, 11, 7),
(25, 11, 12),
(26, 12, 11),
(27, 13, 11),
(28, 13, 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__rel_tags`
--

CREATE TABLE `productos__rel_tags` (
  `id` int(11) NOT NULL,
  `post_id` int(11) UNSIGNED NOT NULL COMMENT 'id de post',
  `tag_id` int(11) UNSIGNED NOT NULL COMMENT 'id de tag'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos__rel_tags`
--

INSERT INTO `productos__rel_tags` (`id`, `post_id`, `tag_id`) VALUES
(5, 2, 1),
(7, 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__tags`
--

CREATE TABLE `productos__tags` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_tipo` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'url en español',
  `url_2` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'url en inglés',
  `text` varchar(255) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre en español',
  `text_2` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'nombre en inglés'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos__tags`
--

INSERT INTO `productos__tags` (`id`, `id_tipo`, `url`, `url_2`, `text`, `text_2`) VALUES
(1, NULL, 'art', 'art', 'arte', 'art'),
(2, NULL, 'barrocoadsds-121-1', 'baroque', 'barrocoadsds 121 1 ', 'baroque');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__valores`
--

CREATE TABLE `productos__valores` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `clave` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `valor` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `position` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos__valores`
--

INSERT INTO `productos__valores` (`id`, `id_producto`, `clave`, `valor`, `position`, `created_at`, `updated_at`) VALUES
(305, 1, 'energia_kcal', '420', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(306, 1, 'energia_kj', '1700', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(307, 1, 'hidratos', '0', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(308, 1, 'azucares', '0', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(309, 1, 'proteinas', '0', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(310, 1, 'grasas_totales', '20.2', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(311, 1, 'grasas_monosaturadas', '0', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(312, 1, 'grasas_polisaturadas', '20', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(313, 3, 'energia_kcal', '380', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(314, 3, 'energia_kj', '1400', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(315, 3, 'hidratos', '20', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(316, 3, 'azucares', '10', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(317, 3, 'proteinas', '200', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(318, 3, 'grasas_totales', '5.2', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(319, 3, 'grasas_monosaturadas', '2', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(320, 3, 'grasas_polisaturadas', '3.2', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(321, 4, 'energia_kcal', '380', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(322, 4, 'energia_kj', '1400', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(323, 4, 'hidratos', '20', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(324, 4, 'azucares', '10', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(325, 4, 'proteinas', '200', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(326, 4, 'grasas_totales', '5.2', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(327, 4, 'grasas_monosaturadas', '2', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(328, 4, 'grasas_polisaturadas', '3.2', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(329, 5, 'energia_kcal', '380', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(330, 5, 'energia_kj', '1400', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(331, 5, 'hidratos', '20', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(332, 5, 'azucares', '10', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(333, 5, 'proteinas', '200', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(334, 5, 'grasas_totales', '5.2', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(335, 5, 'grasas_monosaturadas', '2', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(336, 5, 'grasas_polisaturadas', '3.2', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(337, 6, 'energia_kcal', '380', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(338, 6, 'energia_kj', '1400', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(339, 6, 'hidratos', '20', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(340, 6, 'azucares', '10', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(341, 6, 'proteinas', '200', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(342, 6, 'grasas_totales', '5.2', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(343, 6, 'grasas_monosaturadas', '2', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(344, 6, 'grasas_polisaturadas', '3.2', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(345, 7, 'energia_kcal', '420', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(346, 7, 'energia_kj', '1700', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(347, 7, 'hidratos', '20', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(348, 7, 'azucares', '10', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(349, 7, 'proteinas', '200', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(350, 7, 'grasas_totales', '5.2', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(351, 7, 'grasas_monosaturadas', '2', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23'),
(352, 7, 'grasas_polisaturadas', '3.2', 0, '2017-09-01 06:47:23', '2017-09-01 06:47:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `salt` varchar(128) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `creador` int(11) DEFAULT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_modificacion` timestamp NULL DEFAULT NULL,
  `baja` timestamp NULL DEFAULT NULL,
  `oculto` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `salt`, `creador`, `fecha_creacion`, `fecha_modificacion`, `baja`, `oculto`) VALUES
(2, 'lorena@euphorbia.es', '8d8774060e2f1046758f8ad72f0c3b1f2fd7f2260405d3526b28f20a71f0208fc262440820b42fb80b7f0126a88a8645a2e72130e64e024901a1900afecd724d', '364f105ee0b6b9f82954ca7e3b5740bad160bab2c777628dc0fb43b59d27fb9dbedbb3950b4d83aea2b4a816d058a69a093bf0efd4dcbbec682161f75b7b6c83', 0, '2017-01-19 12:05:51', NULL, NULL, 1),
(4, 'admin', '8d8774060e2f1046758f8ad72f0c3b1f2fd7f2260405d3526b28f20a71f0208fc262440820b42fb80b7f0126a88a8645a2e72130e64e024901a1900afecd724d', '364f105ee0b6b9f82954ca7e3b5740bad160bab2c777628dc0fb43b59d27fb9dbedbb3950b4d83aea2b4a816d058a69a093bf0efd4dcbbec682161f75b7b6c83', 0, '2017-05-04 07:31:01', NULL, NULL, 1),
(5, 'cesar@euphorbia.es', 'f3b7c32383464c9fb0044671d92b7a3b63fdf8b5b96d4f5834820a49857ac5e60685331c2f8ef4f34f184f20841590a2dad47b96c9b14e20278bf8110fb79cc9', '9085c96532da158f77900fd5f3a872c77eaa3c2c9e2b54177e76c0df042beb8d615e81eff9a594c19f858cf645fe363fce0803d418cbe33c8abd70c75fb90009', 0, '2017-08-22 07:02:04', '2017-08-22 05:02:04', NULL, 1),
(6, 'servicios@selectisimo.com', '2bd017b58df1866bd22495a1f661f2af4fab65156e0cfa5788c861f4277a716ebe8d136a1bd85ad3a4db84db5eb5d113f7b4a83d744e95617363a7bf74fb819c', 'd3301ce25f5ef0617c1bee565ab5346a93779b113fbd6837c396a2e89f6abfeb0a59f2cd642bb49164f63f27bd4bdf33e599f84a17ebc53415c222893a5b0b4c', NULL, '2017-07-14 11:52:47', NULL, NULL, 0),
(7, 'cevm88@gmail.com', 'b172cecb30459b7eddea99df97444fe8e5faf4eb4c03e3a140e35f651e47b06a5536fbbb0259572bcc066af9675fd91bf8022bbae8b8532c0452b972343100c9', 'cf68b8eab2187837890d3c144780df83f684ec1f454cd9fd221da3c4c599421ea535fe54a8887d5dd7afa3c557ab1d4c586bdb17e5d5c3f9368bb203c77f7009', 5, '2017-08-01 19:15:00', '2017-08-01 17:14:00', NULL, 0),
(9, 'gestor@euphorbia.es', NULL, NULL, 5, '2017-08-02 05:26:00', '2017-08-02 05:26:00', NULL, 0),
(10, 'cesar@selectisimo.com', '10b4c99beaf6e0c7a25d3a757a9d0482f4182db880f099f5b5f517ed448144ac3fd62ed60411f02d66ca435f84b9aa7a276f38ba44f3b9d586e5958793a76d32', 'a79b7f6fb9092a551928c99ec0eac18949de87857b3c1c94baa2afc20aed978bd71b45f3d40af4fd17920c361a1ac4fe414afe13fb2b6860e837cc3cbefe85eb', NULL, '2017-08-21 07:28:27', '2017-08-21 07:28:27', NULL, 0),
(11, 'usuario@test.com', '9dc24da62f22a027c40c22ae0be80566f1ae98596b64d2e58fdb1192544f752efeb4301d4bbdb1a4e071c008f1e13e0935cf1a89d837bce11091637e2b9311b4', 'd84fda215c163c79e3a75de68b5c6df141ef276fdbfce95357e7200741686aa6eeddbf48e0366fc1d91588f8e203e6f0450cedec27a6c90170f5b63a072873af', NULL, '2017-08-21 07:30:23', '2017-08-21 07:30:23', NULL, 0),
(12, 'test@test.com', '88b9aa1e33735db712b6534ee4c3caac138a68e1415ede2288cdca04b6246c9403e41b638609377bf46f28f1cdcac4627c1fecd500564595f52ad5d0251f89c8', '79067be1de38c055e7c3308dd9480d1c412f58339fdfa52cf3950ce1edbcedb2ae80cb2e76a1e0ffcaaf3a2d7b2bf35602ae3f693def383aa5d479b129457beb', NULL, '2017-08-21 07:40:43', '2017-08-21 07:40:43', NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users__data`
--

CREATE TABLE `users__data` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_user` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `apellidos` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `dni` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `razon` varchar(180) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `direccion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `poblacion` varchar(120) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `id_pais` int(11) DEFAULT NULL,
  `id_provincia` int(11) DEFAULT NULL,
  `codigo_postal` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `users__data`
--

INSERT INTO `users__data` (`id`, `id_user`, `nombre`, `apellidos`, `dni`, `razon`, `direccion`, `poblacion`, `id_pais`, `id_provincia`, `codigo_postal`, `email`, `telefono`, `fecha_nacimiento`, `created_at`, `updated_at`) VALUES
(3, 2, 'Lorena', 'Fernandez', '12345678P', 'Lorena Fernandez', 'Santuario 3 ', 'Valladolid', 1, 49, '47014', 'lorena@euphorbia.es', '987987987', NULL, NULL, NULL),
(11, 5, 'César', 'Vega', '12421477M', 'Pepe Perez', 'c/ Amadeo Arías 32 1ºA', 'Valladolid', 1, 49, '47014', NULL, '601181340', NULL, '2017-08-21 08:57:53', '2017-10-05 11:21:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users__files`
--

CREATE TABLE `users__files` (
  `id` int(10) UNSIGNED NOT NULL,
  `owner_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'propietario (id)',
  `owner_type` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'propietario (mod)',
  `type` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'tipo de archivo',
  `title_1` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'título',
  `name` text COLLATE utf8_spanish2_ci COMMENT 'nombre original',
  `mime_type` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'tipo recogido',
  `size` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'tamaño',
  `height` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'alto (si imagen)',
  `width` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ancho (si imagen)',
  `creacion` timestamp NULL DEFAULT NULL,
  `modificacion` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users__login_attempts`
--

CREATE TABLE `users__login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `time` datetime DEFAULT NULL,
  `ip` varchar(45) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `users__login_attempts`
--

INSERT INTO `users__login_attempts` (`id`, `user_id`, `time`, `ip`) VALUES
(1, 5, '2017-08-10 12:16:12', '127.0.0.1'),
(2, 5, '2017-10-04 14:22:16', '88.4.39.117');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users__rel_roles`
--

CREATE TABLE `users__rel_roles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `role_id` varchar(20) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `users__rel_roles`
--

INSERT INTO `users__rel_roles` (`id`, `user_id`, `role_id`) VALUES
(2, 2, 'administrator'),
(70, 5, 'administrator'),
(3, 5, 'master'),
(76, 5, 'publisher'),
(73, 5, 'root'),
(75, 6, 'publisher'),
(77, 7, 'administrator');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users__roles`
--

CREATE TABLE `users__roles` (
  `id` varchar(20) CHARACTER SET utf8 NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `users__roles`
--

INSERT INTO `users__roles` (`id`, `name`, `level`) VALUES
('administrator', 'Administrador', 1),
('master', 'Master', 2),
('publisher', 'Publisher', 3),
('root', 'Root', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users__tokens`
--

CREATE TABLE `users__tokens` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_usuario` int(11) UNSIGNED DEFAULT NULL,
  `codigo` varchar(32) COLLATE utf8_spanish2_ci NOT NULL,
  `creacion` timestamp NULL DEFAULT NULL,
  `modificacion` timestamp NULL DEFAULT NULL,
  `expiracion` timestamp NULL DEFAULT NULL,
  `valido` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `users__tokens`
--

INSERT INTO `users__tokens` (`id`, `id_usuario`, `codigo`, `creacion`, `modificacion`, `expiracion`, `valido`) VALUES
(979, NULL, '7hIXEnyyFkAdwgmgHstvE1', '2017-09-07 06:48:15', '2017-09-07 07:35:08', '2017-09-07 07:35:08', 0),
(980, NULL, 'mG3oBXRlh98XNAdB9k5iK', '2017-09-07 07:35:08', '2017-09-11 04:15:27', '2017-09-11 04:15:27', 0),
(981, NULL, '2oE9pFw316plHgRZQ8SaHV', '2017-09-11 04:15:19', '2017-09-11 04:57:25', '2017-09-11 04:57:25', 0),
(982, 5, 'w3FPKizrNDWNQvuRmh4Ah', '2017-09-11 04:57:26', '2017-09-11 05:04:05', '2017-09-11 05:04:05', 0),
(983, 5, '3Q7rKOkJo7RYu4i0zTNpKB', '2017-09-11 05:04:10', '2017-09-11 05:11:31', '2017-09-11 05:11:31', 0),
(984, 5, 'yVdYVRaYQ3BDKrtZjuqKy', '2017-09-11 05:11:38', '2017-09-11 05:13:55', '2017-09-11 05:13:55', 0),
(985, 5, '32PgvyItOAecq8jVQSBjbU', '2017-09-11 05:14:00', '2017-09-11 05:15:23', '2017-09-11 05:15:23', 0),
(986, 5, '1AZTecggK05cl5Q67hSNqH', '2017-09-11 05:15:27', '2017-09-11 05:16:47', '2017-09-11 05:16:47', 0),
(987, 5, '2FhKf2JmpiWSlmMVL27ZJb', '2017-09-11 05:16:49', '2017-09-11 05:18:08', '2017-09-11 05:18:08', 0),
(988, NULL, '5wfjWIMOXT6pxa90DctkG0', '2017-09-11 05:18:11', '2017-09-11 05:59:00', '2017-09-11 05:59:00', 0),
(989, 5, 'HG43bcpV2w5LqzRviYD4u', '2017-09-11 05:18:22', '2017-09-11 05:21:08', '2017-09-11 05:21:08', 0),
(990, 5, '2GsFrhvVIWrzuOE0Fr8Ev3', '2017-09-11 05:21:11', '2017-09-11 05:21:59', '2017-09-11 05:21:59', 0),
(991, NULL, '2fvFlPhvMA7AJt47gmICLP', '2017-09-11 05:22:21', '2017-09-11 05:59:00', '2017-09-11 05:59:00', 0),
(992, NULL, '45vSuTdoGLOmFso7y0OdWC', '2017-09-11 05:22:21', '2017-09-11 05:22:29', '2017-09-11 05:22:29', 0),
(993, NULL, '3r2TVODysPoEy9ieEyRIQm', '2017-09-11 05:22:32', '2017-09-11 05:59:00', '2017-09-11 05:59:00', 0),
(994, 5, '3oZt5ZdVnSsUoFDdLk8PWW', '2017-09-11 05:22:35', '2017-09-11 05:41:03', '2017-09-11 05:41:03', 0),
(995, NULL, 'h1cT4UIaHfHwrmT513LH6', '2017-09-11 05:41:08', '2017-09-11 05:59:00', '2017-09-11 05:59:00', 0),
(996, 5, '7l8pz4ZPwqdSiRGevE5jLL', '2017-09-11 05:59:04', '2017-09-11 06:27:29', '2017-09-11 06:27:29', 0),
(997, 5, '1gYQeTz8U0TCUisijJBnlh', '2017-09-11 06:27:38', '2017-09-11 06:46:29', '2017-09-11 06:46:29', 0),
(998, NULL, '70VouI029D14G76HzXo9qg', '2017-09-11 06:46:33', '2017-09-11 07:23:36', '2017-09-11 07:23:36', 0),
(999, NULL, '2axXCoNxBTSuGozs4pMBor', '2017-09-11 07:36:02', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1000, NULL, '3SXOiNsF39MHXj4c92bu27', '2017-09-11 08:05:58', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1001, NULL, '7imUP4PsunDixJEO9oSxJE', '2017-09-11 08:06:20', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1002, NULL, '1BVC6Ce8uyVImNJSF7AWUv', '2017-09-11 08:06:25', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1003, NULL, '2HkWTky6KPhNmTtcKjMTKm', '2017-09-11 08:07:15', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1004, NULL, '5tO7o30tZbaxyPSidkMhYN', '2017-09-11 08:07:15', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1005, NULL, '2l3Dd7Ii77mk2XWJDxFeLa', '2017-09-11 08:32:29', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1006, NULL, '256fvYSFrcqiaAgZt3DhZf', '2017-09-11 08:32:30', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1007, NULL, '6pw26Nmc8EsORRFLMlJuco', '2017-09-11 08:33:49', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1008, NULL, '2VOvnPqAXRYWMTm2R4Mdve', '2017-09-11 08:34:20', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1009, NULL, '5t9ToWCn4yPwsnhPwDO0EI', '2017-09-11 08:34:21', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1010, NULL, '4ZAXN1eFZtbak28kJDbOo8', '2017-09-11 10:29:31', '2017-09-12 05:21:35', '2017-09-12 05:21:35', 0),
(1011, NULL, '1yq9T2hOMAqE6MoF4N3i2B', '2017-09-12 05:21:35', '2017-09-13 04:12:34', '2017-09-13 04:12:34', 0),
(1012, NULL, '7dXxYq0uaGCAHMyaqm0Izt', '2017-09-13 04:12:34', '2017-09-13 07:07:32', '2017-09-13 07:07:32', 0),
(1013, NULL, '5V3PkIpPxMwWH1aqmra8Eu', '2017-09-13 07:07:35', '2017-09-13 07:39:49', '2017-09-13 07:39:49', 0),
(1014, 5, '2aQ30ryqcBdPDDZ8vNzsKD', '2017-09-13 07:10:18', '2017-09-13 07:14:23', '2017-09-13 07:14:23', 0),
(1015, 5, '7Ka58nYxCTzZ8yAQm73WYO', '2017-09-13 07:14:03', '2017-09-13 09:13:17', '2017-09-13 09:13:17', 0),
(1016, NULL, '9MALt88159BOxw8zyFiax', '2017-09-13 09:13:18', '2017-09-13 09:43:22', '2017-09-13 09:43:22', 0),
(1017, NULL, 'vE9RrFSZGB4ej2GArXRTv', '2017-09-13 09:35:49', '2017-09-13 10:07:27', '2017-09-13 10:07:27', 0),
(1018, NULL, '5Dk5hdQwGM4rEXd5FBt4Zu', '2017-09-13 10:17:09', '2017-09-13 11:12:02', '2017-09-13 11:12:02', 0),
(1019, 5, '7Xr9oB6W0b9JXfFw6bo6ZJ', '2017-09-13 10:26:11', '2017-09-13 11:55:49', '2017-09-13 11:55:49', 0),
(1020, NULL, '5NntzPQwL84bOdFB8qRmWd', '2017-09-13 11:23:20', '2017-09-13 11:53:21', '2017-09-13 11:53:21', 0),
(1021, NULL, '2kNyPwcuNKRKqrO5U0S0kN', '2017-09-13 11:54:54', '2017-09-13 12:30:10', '2017-09-13 12:30:10', 0),
(1022, NULL, '6MNKh6a53GGD5z5Z0vchXc', '2017-09-13 12:03:51', '2017-09-13 12:51:21', '2017-09-13 12:51:21', 0),
(1023, 5, '5ZNPf3Cu1SNHX4BbIH7Fy0', '2017-09-13 12:41:13', '2017-09-14 06:20:41', '2017-09-14 06:20:41', 0),
(1024, NULL, '5Hd3gLt22u4sfgHDTgiOJm', '2017-09-14 06:20:41', '2017-09-14 07:37:05', '2017-09-14 07:37:05', 0),
(1025, NULL, '12Jk5co0fVNHQAS66X6J9J', '2017-09-14 06:22:11', '2017-09-14 06:52:48', '2017-09-14 06:52:48', 0),
(1026, NULL, '7TBinB5z3sxZrBOaU8gVWv', '2017-09-14 07:37:12', '2017-09-14 08:07:33', '2017-09-14 08:07:33', 0),
(1027, 5, '7829WMmrNPfiNVKY6GV7Gr', '2017-09-14 07:37:46', '2017-09-14 09:32:37', '2017-09-14 09:32:37', 0),
(1028, 5, '1MEdjo6aYIrXHTfUgPWXK7', '2017-09-14 09:32:37', '2017-09-14 12:47:10', '2017-09-14 12:47:10', 0),
(1029, NULL, '5TiNsGJK8ppoI1kC6exQMy', '2017-09-14 11:15:52', '2017-09-14 12:47:10', '2017-09-14 12:47:10', 0),
(1030, NULL, '4DHIxz6N6SmHPhAeISTIl2', '2017-09-14 12:56:55', '2017-09-14 13:33:05', '2017-09-14 13:33:05', 0),
(1031, NULL, 'EPU71hdMOe2qj8gxbGjcN', '2017-09-14 12:56:56', '2017-09-14 13:33:05', '2017-09-14 13:33:05', 0),
(1032, NULL, '5XG2mpYcgMBdexcKmO0FB9', '2017-09-14 12:58:07', '2017-09-14 13:33:05', '2017-09-14 13:33:05', 0),
(1033, 5, '7bP6D94poF623Yq504hplJ', '2017-09-14 12:58:08', '2017-09-14 19:40:46', '2017-09-14 19:40:46', 0),
(1034, 5, '7O47r43cwcMnhumTB4Wa9B', '2017-09-15 06:46:33', '2017-09-15 07:30:53', '2017-09-15 07:30:53', 0),
(1035, NULL, '2r6ClgpTyRlZPVPvbRDRY', '2017-09-15 06:48:47', '2017-09-15 07:22:44', '2017-09-15 07:22:44', 0),
(1036, NULL, '48obInGEJjKaieVI1Bx8yf', '2017-09-15 07:00:04', '2017-09-15 07:30:53', '2017-09-15 07:30:53', 0),
(1037, NULL, '2ECy4H2tgDaT0pQniX8rhu', '2017-09-15 07:00:13', '2017-09-15 07:31:08', '2017-09-15 07:31:08', 0),
(1038, NULL, '60Z1ax1tWRYwjJ9Bbprjb0', '2017-09-15 07:01:08', '2017-09-15 07:32:27', '2017-09-15 07:32:27', 0),
(1039, NULL, '7gPIj5IsJpEa0fcrs5EAl4', '2017-09-15 07:01:08', '2017-09-15 07:32:27', '2017-09-15 07:32:27', 0),
(1040, NULL, 'k72A1AbCVUL9y1gg9edP6', '2017-09-15 07:02:12', '2017-09-15 07:32:27', '2017-09-15 07:32:27', 0),
(1041, NULL, '6my58qOS94A35ic2vE8GDF', '2017-09-15 07:02:15', '2017-09-15 07:32:27', '2017-09-15 07:32:27', 0),
(1042, NULL, 'X7jv8njfCgibZmcicDX0I', '2017-09-15 07:02:16', '2017-09-15 07:36:01', '2017-09-15 07:36:01', 0),
(1043, NULL, 'VDddmc18rCAx72FTtxiME', '2017-09-15 07:30:53', '2017-09-15 08:04:49', '2017-09-15 08:04:49', 0),
(1044, NULL, '4DfbYdTF4NdJiqWAdFLwXc', '2017-09-15 07:32:20', '2017-09-15 08:04:49', '2017-09-15 08:04:49', 0),
(1045, NULL, '7RBULkqC9YC79XNAoPlKxL', '2017-09-15 08:09:21', '2017-09-15 08:45:12', '2017-09-15 08:45:12', 0),
(1046, 5, '3YECnpcoqBsDmt01Fbl3g4', '2017-09-15 08:11:00', '2017-09-15 09:57:47', '2017-09-15 09:57:47', 0),
(1047, NULL, '5uiTzZMZQkvWGpo8TkhCPu', '2017-09-15 09:59:00', '2017-09-15 10:31:28', '2017-09-15 10:31:28', 0),
(1048, 5, '5q8UoP4XRCjBq91srvvMo0', '2017-09-15 10:09:20', '2017-09-15 10:56:24', '2017-09-15 10:56:24', 0),
(1049, NULL, '1HiAs46gD4pi8eQfgl7pKR', '2017-09-15 10:56:35', '2017-09-15 10:56:39', '2017-09-15 10:56:39', 0),
(1050, NULL, '567YyyfGBFE6XeYWPzVXCU', '2017-09-15 10:57:12', '2017-09-15 10:58:28', '2017-09-15 10:58:28', 0),
(1051, NULL, '12mKsVuS3lzQoMutchT6c4', '2017-09-15 11:00:24', '2017-09-15 11:42:29', '2017-09-15 11:42:29', 0),
(1052, NULL, '71HwFZoZnlVVuE5yKZ3CDs', '2017-09-15 11:01:05', '2017-09-15 11:42:29', '2017-09-15 11:42:29', 0),
(1053, NULL, 'IjvczPVyKzQgpxmlBPxgQ', '2017-09-15 11:01:06', '2017-09-15 11:42:29', '2017-09-15 11:42:29', 0),
(1054, 5, '51j9rf5yxWwOR3D2YwTIRv', '2017-09-15 11:01:19', '2017-09-15 11:01:37', '2017-09-15 11:01:37', 0),
(1055, NULL, 'Cs4x5n7cK7UM71Di8hmOD', '2017-09-15 11:02:13', '2017-09-15 11:42:29', '2017-09-15 11:42:29', 0),
(1056, 5, '2Uhqrt9PjvqTy0rV3LVOoV', '2017-09-15 11:02:41', '2017-09-15 11:04:08', '2017-09-15 11:04:08', 0),
(1057, 5, '1HrCXomj339dVhtS4MyyWu', '2017-09-15 11:03:34', '2017-09-15 11:04:17', '2017-09-15 11:04:17', 0),
(1058, NULL, '76Brnf7lpXDT51n20VIRfz', '2017-09-15 11:09:03', '2017-09-15 11:42:29', '2017-09-15 11:42:29', 0),
(1059, 5, '2X8wauQlRLgbbxAJS7MZrH', '2017-09-15 11:23:20', '2017-09-15 11:53:51', '2017-09-15 11:53:51', 0),
(1060, NULL, 'vpwngukgAM8ovj9YsY7Yv', '2017-09-15 11:53:53', '2017-09-15 12:24:37', '2017-09-15 12:24:37', 0),
(1061, NULL, '3rA6wfGpwu78jvvyrsjbRZ', '2017-09-15 11:53:53', '2017-09-15 12:24:37', '2017-09-15 12:24:37', 0),
(1062, NULL, '2yllyImTGjWYhWUQRbV0pd', '2017-09-15 12:24:40', '2017-09-15 13:19:27', '2017-09-15 13:19:27', 0),
(1063, NULL, 'Se7GqjivrDXM4PmeHCN4j', '2017-09-15 12:24:40', '2017-09-15 13:19:27', '2017-09-15 13:19:27', 0),
(1064, NULL, '6m9bS52ZWlytMwcDEUskRg', '2017-09-15 13:19:27', '2017-09-18 08:09:09', '2017-09-18 08:09:09', 0),
(1065, NULL, '4UxbNB9UzXmsbPdgqP3Ua0', '2017-09-15 13:19:27', '2017-09-18 08:09:09', '2017-09-18 08:09:09', 0),
(1066, NULL, '3CvaHfpU8DAaiXI85gksLF', '2017-09-18 08:22:42', '2017-09-18 08:55:07', '2017-09-18 08:55:07', 0),
(1067, NULL, '5EmyoReIKQhALXwsTHOiCx', '2017-09-18 08:22:42', '2017-09-18 08:55:07', '2017-09-18 08:55:07', 0),
(1068, NULL, '10CldkMksQTYl4xvIboIJw', '2017-09-18 08:55:06', '2017-09-18 09:26:09', '2017-09-18 09:26:09', 0),
(1069, NULL, 'wEDabE1MC3ieA6E0OvbTC', '2017-09-18 08:55:07', '2017-09-18 09:26:09', '2017-09-18 09:26:09', 0),
(1070, NULL, '1U6G0o3L3WlyMnSPSH4D9R', '2017-09-18 09:26:09', '2017-09-18 10:11:13', '2017-09-18 10:11:13', 0),
(1071, NULL, '385A23ZKkLq1kfwoeeO2UK', '2017-09-18 09:26:09', '2017-09-18 10:11:13', '2017-09-18 10:11:13', 0),
(1072, NULL, '17Awk0O43iVSOCx1NEQNU1', '2017-09-18 10:11:13', '2017-09-18 10:42:16', '2017-09-18 10:42:16', 0),
(1073, NULL, 'U18jp1GiNfzCEbbBY0dYC', '2017-09-18 10:11:13', '2017-09-18 10:42:16', '2017-09-18 10:42:16', 0),
(1074, NULL, '6fqFrXr12ca6lXF1d3kPo1', '2017-09-18 10:42:16', '2017-09-18 11:12:23', '2017-09-18 11:12:23', 0),
(1075, NULL, '3SGBsODOnkpXaEHJXcQ9qu', '2017-09-18 10:42:17', '2017-09-18 11:12:23', '2017-09-18 11:12:23', 0),
(1076, NULL, '6dl0S8WZZjKoGzfXtM1Ka5', '2017-09-18 11:12:22', '2017-09-18 12:05:06', '2017-09-18 12:05:06', 0),
(1077, NULL, '2IggeFYJi79xEInSa8gZe8', '2017-09-18 11:12:24', '2017-09-18 12:05:06', '2017-09-18 12:05:06', 0),
(1078, NULL, '2jhoatAynk0FNsJ0CYG71', '2017-09-18 12:05:06', '2017-09-18 12:36:53', '2017-09-18 12:36:53', 0),
(1079, NULL, '6DmAlZAlXScBGGlF2pKrLd', '2017-09-18 12:05:06', '2017-09-18 12:36:53', '2017-09-18 12:36:53', 0),
(1080, NULL, '7SXMfvMxkzWn3Pntgs9hLb', '2017-09-18 12:36:53', '2017-09-19 07:39:26', '2017-09-19 07:39:26', 0),
(1081, NULL, 'g2jEPSrio8carM9A2uyU9', '2017-09-18 12:36:54', '2017-09-19 07:39:26', '2017-09-19 07:39:26', 0),
(1082, NULL, '34kxKKvR7lAjWakUFZLaZl', '2017-09-19 08:33:36', '2017-09-19 09:12:17', '2017-09-19 09:12:17', 0),
(1083, NULL, '1C1I0n6ybiU7RvZGFzDwsa', '2017-09-19 08:33:36', '2017-09-19 09:12:17', '2017-09-19 09:12:17', 0),
(1084, NULL, '3yiVsusiMDu0HsqdtUGmGL', '2017-09-19 09:13:52', '2017-09-19 09:44:55', '2017-09-19 09:44:55', 0),
(1085, NULL, '7R3AXPuIck8R9Bkyg1ZqPo', '2017-09-19 09:13:53', '2017-09-19 09:44:55', '2017-09-19 09:44:55', 0),
(1086, NULL, '1sfuCwAONqJuZbaK9oD9j8', '2017-09-19 09:44:55', '2017-09-19 10:15:05', '2017-09-19 10:15:05', 0),
(1087, NULL, '1IDo49H5inOwqxwqJhDDuy', '2017-09-19 09:44:56', '2017-09-19 10:15:05', '2017-09-19 10:15:05', 0),
(1088, NULL, '4qnuN7dpN7IF6FfGfAGw9y', '2017-09-19 10:15:06', '2017-09-19 11:16:08', '2017-09-19 11:16:08', 0),
(1089, NULL, '2qgJXHurt1c3yLIxMc2bMd', '2017-09-19 10:15:07', '2017-09-19 11:16:08', '2017-09-19 11:16:08', 0),
(1090, NULL, '4kFyFMvSUhcb8LKKLV9KKr', '2017-09-19 11:16:06', '2017-09-19 11:49:45', '2017-09-19 11:49:45', 0),
(1091, NULL, '5GR6R1Y1VEQZOLRkIjyak3', '2017-09-19 11:16:07', '2017-09-19 11:49:45', '2017-09-19 11:49:45', 0),
(1092, NULL, '5LqFKSBURh4TLVkOOgfx1f', '2017-09-19 11:49:45', '2017-09-19 12:27:21', '2017-09-19 12:27:21', 0),
(1093, NULL, 'qFA4QFB6erAZFVbFk1GJY', '2017-09-19 11:49:46', '2017-09-19 12:27:21', '2017-09-19 12:27:21', 0),
(1094, NULL, '7SR5U7eGPvjEPBPwlOg5Ku', '2017-09-19 12:27:21', '2017-09-19 12:57:43', '2017-09-19 12:57:43', 0),
(1095, NULL, '4C00xLdJ8ZjChHrKRwDI8k', '2017-09-19 12:27:21', '2017-09-19 12:57:43', '2017-09-19 12:57:43', 0),
(1096, NULL, '3V9xqZXWttTQDGTPFdOWTd', '2017-09-19 12:57:43', '2017-09-20 07:24:08', '2017-09-20 07:24:08', 0),
(1097, NULL, '67rIcgsW2oGXSiH4aav3df', '2017-09-19 12:57:43', '2017-09-20 07:24:08', '2017-09-20 07:24:08', 0),
(1098, NULL, '6LOl0D3xnJG1tOQ98tmWkC', '2017-09-20 09:41:01', '2017-09-20 10:21:11', '2017-09-20 10:21:11', 0),
(1099, NULL, '6VqoaUNbRLFXNCldB9PIwx', '2017-09-20 09:41:01', '2017-09-20 10:21:11', '2017-09-20 10:21:11', 0),
(1100, NULL, '6JS6CF7VeX7TNhKs4nKD1w', '2017-09-20 10:21:13', '2017-09-20 10:51:39', '2017-09-20 10:51:39', 0),
(1101, NULL, '6hW49z8y0Kp22XIRLtmZNY', '2017-09-20 10:21:14', '2017-09-20 10:51:39', '2017-09-20 10:51:39', 0),
(1102, NULL, '7XLf9oviu1Hoc8YHG13Y5P', '2017-09-20 11:18:08', '2017-09-20 11:58:19', '2017-09-20 11:58:19', 0),
(1103, NULL, '5B1PxqPkKGq9zNBPtKe4aL', '2017-09-20 11:58:19', '2017-09-21 16:50:37', '2017-09-21 16:50:37', 0),
(1104, NULL, 'kqiy3esQDL3iOoEX1A4ja', '2017-09-20 11:58:19', '2017-09-21 16:50:37', '2017-09-21 16:50:37', 0),
(1105, NULL, '1lnqmgbxlv1ITJXAGoyH4Y', '2017-09-26 07:06:07', '2017-09-26 07:47:02', '2017-09-26 07:47:02', 0),
(1106, NULL, '73eksOR4YeANWPH7jtJSI4', '2017-09-26 08:33:09', '2017-09-26 09:59:12', '2017-09-26 09:59:12', 0),
(1107, NULL, '73tVnmiIcgYC8vz2h6cNvP', '2017-09-26 08:33:09', '2017-09-26 09:59:12', '2017-09-26 09:59:12', 0),
(1108, NULL, '1BAtX2yafeta5v9l2hvCeg', '2017-09-26 10:01:43', '2017-09-26 10:36:31', '2017-09-26 10:36:31', 0),
(1109, NULL, '6izUbC3GON0LrdimEYydkb', '2017-09-26 10:01:43', '2017-09-26 10:36:31', '2017-09-26 10:36:31', 0),
(1110, NULL, '5WVukSYMd095NylbUpi3Hg', '2017-09-26 10:36:31', '2017-09-26 11:07:40', '2017-09-26 11:07:40', 0),
(1111, NULL, '1nZAofSFICucMlGEWUH4yC', '2017-09-26 10:36:31', '2017-09-26 11:07:40', '2017-09-26 11:07:40', 0),
(1112, NULL, '3aF1QNSxeG22i21VMxkyzQ', '2017-09-26 11:07:57', '2017-09-26 11:38:00', '2017-09-26 11:38:00', 0),
(1113, NULL, '7U7y6vbh5axZCQkDlMsWiy', '2017-09-26 11:07:59', '2017-09-26 11:38:00', '2017-09-26 11:38:00', 0),
(1114, NULL, '3cpOCuLDABZjimGv5dc4Ik', '2017-09-26 11:37:59', '2017-09-26 12:08:00', '2017-09-26 12:08:00', 0),
(1115, NULL, 'lEYPnKcCdQi7l0fmk0sGX', '2017-09-26 11:37:59', '2017-09-26 12:08:00', '2017-09-26 12:08:00', 0),
(1116, NULL, '2hSKLBBt6yeqhtbsdgaEfQ', '2017-09-26 12:08:00', '2017-09-26 12:38:31', '2017-09-26 12:38:31', 0),
(1117, NULL, '41Plvz5C4YPXjhTlKvDLDm', '2017-09-26 12:08:01', '2017-09-26 12:38:31', '2017-09-26 12:38:31', 0),
(1118, NULL, '3ZNon8e7caFiNOPTgsh0NF', '2017-09-26 12:38:31', '2017-09-26 18:50:27', '2017-09-26 18:50:27', 0),
(1119, NULL, '55elUV9OInM6dh06CHB8G4', '2017-09-26 12:38:32', '2017-09-26 18:50:27', '2017-09-26 18:50:27', 0),
(1120, NULL, '7aSpnaJxG0UWUEdlcqa7R0', '2017-09-27 08:18:00', '2017-09-27 08:50:28', '2017-09-27 08:50:28', 0),
(1121, NULL, '76dc9hiT1SYUnhYCI8Imyo', '2017-09-27 08:18:01', '2017-09-27 08:50:28', '2017-09-27 08:50:28', 0),
(1122, NULL, '3JS4KsroieunbPfU4S1OmW', '2017-09-27 08:50:28', '2017-09-27 10:00:43', '2017-09-27 10:00:43', 0),
(1123, NULL, '5WSX0RrF9IoBZYYzKK2rrO', '2017-09-27 08:50:29', '2017-09-27 10:00:43', '2017-09-27 10:00:43', 0),
(1124, NULL, '1hP0FIwc87vQM4G6kwraPz', '2017-09-27 10:00:43', '2017-09-27 10:31:50', '2017-09-27 10:31:50', 0),
(1125, NULL, '7bWmThUPsFk16Of0g8c039', '2017-09-27 10:00:44', '2017-09-27 10:31:50', '2017-09-27 10:31:50', 0),
(1126, NULL, '44RnSR511T2IFe6fHr5GS', '2017-09-27 10:13:41', '2017-09-27 10:46:48', '2017-09-27 10:46:48', 0),
(1127, NULL, '4JhlQQT2LefHyMQKRSpv4v', '2017-09-27 10:15:38', '2017-09-27 10:46:48', '2017-09-27 10:46:48', 0),
(1128, NULL, '3IsfBlZ8XXpBXcnVnjUrhu', '2017-09-27 10:15:40', '2017-09-27 10:46:48', '2017-09-27 10:46:48', 0),
(1129, NULL, '2vEGc5RHaNyoGEDCFND07V', '2017-09-27 10:15:48', '2017-09-27 10:46:48', '2017-09-27 10:46:48', 0),
(1130, NULL, '1p400C87LhgX1MWnMTUqXB', '2017-09-27 10:23:10', '2017-09-27 10:53:28', '2017-09-27 10:53:28', 0),
(1131, NULL, '5FysEpaNTy9tUxpk0Y9eO9', '2017-09-27 10:23:11', '2017-09-27 10:53:28', '2017-09-27 10:53:28', 0),
(1132, NULL, '6ZVoQQdUUpG2KMRaCGEDkD', '2017-09-27 10:53:28', '2017-09-27 11:24:09', '2017-09-27 11:24:09', 0),
(1133, NULL, '5z3DllOmzpTuIexucNJToZ', '2017-09-27 10:53:30', '2017-09-27 11:24:09', '2017-09-27 11:24:09', 0),
(1134, NULL, '6RWs40jjoxcJySz6xwN2kw', '2017-09-27 11:24:09', '2017-09-27 11:54:12', '2017-09-27 11:54:12', 0),
(1135, NULL, '3bLQzrJMoSIrDZSfu7gNu6', '2017-09-27 11:24:59', '2017-09-27 11:55:30', '2017-09-27 11:55:30', 0),
(1136, NULL, '2gIb8bGsD1FAL7IGa00Vw8', '2017-09-27 11:54:11', '2017-09-27 12:24:38', '2017-09-27 12:24:38', 0),
(1137, NULL, '3m9px7uStPRQWPN46ndcWr', '2017-09-27 11:55:30', '2017-09-27 12:25:32', '2017-09-27 12:25:32', 0),
(1138, NULL, '75RkYAyU04byby0qIZDstv', '2017-09-27 12:24:38', '2017-09-28 07:10:50', '2017-09-28 07:10:50', 0),
(1139, NULL, '7j2YSbvWbqMJehUSY4TAdl', '2017-09-27 12:25:46', '2017-09-28 07:10:50', '2017-09-28 07:10:50', 0),
(1140, NULL, 'TZot7QWAUlcqnFMt9Jsip', '2017-09-28 08:36:06', '2017-09-28 09:11:45', '2017-09-28 09:11:45', 0),
(1141, NULL, '60GyXlSApu7FjxmGZjW5V2', '2017-09-28 08:36:08', '2017-09-28 09:11:45', '2017-09-28 09:11:45', 0),
(1142, NULL, '1DP3QHt6fPLnbbXwQF33He', '2017-09-28 08:36:11', '2017-09-28 09:11:45', '2017-09-28 09:11:45', 0),
(1143, NULL, '6s26hZ9NwVIKcBbmwQBCUX', '2017-09-28 08:38:17', '2017-09-28 09:11:45', '2017-09-28 09:11:45', 0),
(1144, NULL, '3Xcg4qGUu04JJfEvEfRDlC', '2017-09-28 08:38:20', '2017-09-28 09:11:45', '2017-09-28 09:11:45', 0),
(1145, NULL, '6YSZK543QsMqQMosVZw61B', '2017-09-28 08:38:22', '2017-09-28 09:11:45', '2017-09-28 09:11:45', 0),
(1146, NULL, '244aqD9kjOWBroxKVNE7VN', '2017-09-28 08:39:01', '2017-09-28 09:11:45', '2017-09-28 09:11:45', 0),
(1147, NULL, 'TTIzCDTSExXZfzgG8oTis', '2017-09-28 08:39:27', '2017-09-28 09:11:45', '2017-09-28 09:11:45', 0),
(1148, NULL, '2Vt79vQvaL1A3Cp3jIPIOA', '2017-09-28 08:40:23', '2017-09-28 09:11:45', '2017-09-28 09:11:45', 0),
(1149, NULL, '7kU5LCRj747XNDM4mFGXBv', '2017-09-28 08:40:40', '2017-09-28 09:11:45', '2017-09-28 09:11:45', 0),
(1150, NULL, '1iWwq3zJYDaU5fu1zyFUzL', '2017-09-28 08:40:43', '2017-09-28 09:11:45', '2017-09-28 09:11:45', 0),
(1151, NULL, '4eYepvkDBIeOQaTBhO0myf', '2017-09-28 08:40:45', '2017-09-28 09:11:45', '2017-09-28 09:11:45', 0),
(1152, NULL, 'EpJTIvVzzFfj3ckxZemrg', '2017-09-28 08:42:15', '2017-09-28 09:13:08', '2017-09-28 09:13:08', 0),
(1153, NULL, '6FxJkJYH0KubProouApibD', '2017-09-28 08:42:49', '2017-09-28 09:13:08', '2017-09-28 09:13:08', 0),
(1154, NULL, '1YC8X2HoKLKYuAMooanNXw', '2017-09-28 08:43:21', '2017-09-28 09:13:36', '2017-09-28 09:13:36', 0),
(1155, NULL, '2gVn7FqXM111KXu6BGGt46', '2017-09-28 08:44:07', '2017-09-28 09:15:51', '2017-09-28 09:15:51', 0),
(1156, NULL, 'YfcLS3ktukg9WL87y4F50', '2017-09-28 08:44:09', '2017-09-28 09:15:51', '2017-09-28 09:15:51', 0),
(1157, NULL, '5Dx6PzUWtDxQ5T1OsJawY', '2017-09-28 08:44:11', '2017-09-28 09:15:51', '2017-09-28 09:15:51', 0),
(1158, NULL, '5Fmmok0bkchPoIQTTGDFSe', '2017-09-28 08:47:28', '2017-09-28 09:19:51', '2017-09-28 09:19:51', 0),
(1159, NULL, '7YomjeRq6SYxBFyw4ezEvs', '2017-09-28 08:47:31', '2017-09-28 09:19:51', '2017-09-28 09:19:51', 0),
(1160, NULL, '53PJI5XYjVySvpDtAkPORX', '2017-09-28 08:47:32', '2017-09-28 09:19:51', '2017-09-28 09:19:51', 0),
(1161, NULL, '4WsKb8dgQlBpyuPIQTF029', '2017-09-28 08:50:34', '2017-09-28 09:20:42', '2017-09-28 09:20:42', 0),
(1162, NULL, '7Xd5Ui5mzoSb0285e1dyXO', '2017-09-28 08:50:38', '2017-09-28 09:20:42', '2017-09-28 09:20:42', 0),
(1163, NULL, '5yKg2iM1PjqLMTKq4EALBo', '2017-09-28 08:50:39', '2017-09-28 09:20:42', '2017-09-28 09:20:42', 0),
(1164, NULL, '45QrOj9cvHJcCMNIUhJA7L', '2017-09-28 08:57:16', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1165, NULL, '6l3Xg8heyRfXzzA7dLnX1V', '2017-09-28 08:57:20', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1166, NULL, '7aNTU5ZTjmN94qOtYBr9wW', '2017-09-28 08:57:20', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1167, NULL, '2X8H8ucVsobhbT9E0uujC3', '2017-09-28 08:57:56', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1168, NULL, '4rQMmhfM9ULTmJqDWBPPic', '2017-09-28 08:58:01', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1169, NULL, '5KyivIhTGZTtMiwe4pfIxx', '2017-09-28 08:58:01', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1170, NULL, '3Eqc65OIN29KWqH9RqWY3l', '2017-09-28 08:58:48', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1171, NULL, '11AsqSFapPiZIpYdf4lEn6', '2017-09-28 08:58:53', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1172, NULL, '7MrvIpBmdYXma61Nruaw7Z', '2017-09-28 08:58:53', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1173, NULL, '4JdGRaIl4jLcvlFch2wvsc', '2017-09-28 09:01:58', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1174, NULL, '4Nycwt9zy9E84usdcUxB2n', '2017-09-28 09:02:02', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1175, NULL, '5wD3nfAAsK3qwNMU95i1MI', '2017-09-28 09:02:03', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1176, NULL, '6xK1a40PoDaLPMZaLDHe6u', '2017-09-28 09:02:21', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1177, NULL, '3zWK2TlzdBS8YY8l6fnlLH', '2017-09-28 09:05:58', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1178, NULL, '5999ExAO7rx4T8dqAWcbj9', '2017-09-28 09:06:00', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1179, NULL, '6JIJByvpLmrpCI26dDTU5N', '2017-09-28 09:06:01', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1180, NULL, 'xIdNOYvoqgghvETrUbAEw', '2017-09-28 09:06:02', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1181, NULL, '3cFIgFeFI8RWI4xBVjvGOz', '2017-09-28 09:06:04', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1182, NULL, '3dwhugNPxgICMREYxDW4px', '2017-09-28 09:10:57', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1183, NULL, '6LXiYlZVtiH7IsQhUhE0Ze', '2017-09-28 09:10:58', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1184, NULL, '2XzajhgXMcsBwIouPHYEyh', '2017-09-28 09:11:00', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1185, NULL, '5vDuGUz7qPJolJus2uPl6T', '2017-09-28 09:11:45', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1186, NULL, '3F3QbWNnpXazwj1gASQNT2', '2017-09-28 09:11:46', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1187, NULL, '5ZZQpzRw9F1EGpmtCuRRxF', '2017-09-28 09:11:48', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1188, NULL, '2RHO8X4wSdaAcP9R4KvwR0', '2017-09-28 09:13:07', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1189, NULL, '2WP1S47bsFeaA7uBiIrbfi', '2017-09-28 09:13:09', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1190, NULL, '3eDTdK028UwmtauSaPgtcZ', '2017-09-28 09:13:11', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1191, NULL, '3TOwdE0z7ryxtCysOcroj8', '2017-09-28 09:13:36', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1192, NULL, '1XBSur8wvFfcFsQtL5kMT2', '2017-09-28 09:13:40', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1193, NULL, '26MqsBTtIVhE3fPJG21DJt', '2017-09-28 09:15:51', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1194, NULL, 'Soh27JgmM3JxvFvaL4fKd', '2017-09-28 09:15:52', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1195, NULL, '5V9PxXMWP2fX7cyGxFGeS1', '2017-09-28 09:15:55', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1196, NULL, 'wUCojn41hj1fZproNEwwG', '2017-09-28 09:16:04', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1197, NULL, '7WRXUFznEHGXUOUU1dIA5Y', '2017-09-28 09:16:22', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1198, NULL, '5iPypTso9u5BAgYw3929AA', '2017-09-28 09:16:45', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1199, NULL, '35VIF98P015lyG3xa4pj2K', '2017-09-28 09:19:51', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1200, NULL, '2L6tkSClThxO8BM2XrH60B', '2017-09-28 09:19:56', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1201, NULL, '1w9dizGSd0zcknA59UCOQw', '2017-09-28 09:19:56', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1202, NULL, '7C1m7LToaBBlOBqfKUTnrl', '2017-09-28 09:20:42', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1203, NULL, '2NRtvlCi9LSg29AThrUEaH', '2017-09-28 09:20:46', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1204, NULL, '2hwnvN3cGRLHa5RkG99Mmt', '2017-09-28 09:20:46', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1205, NULL, '5rNVd1F6g9JSGndGOQA0o9', '2017-09-28 09:21:00', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1206, NULL, '3eDxOSBNxG1ffylvMtLZXb', '2017-09-28 09:21:05', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1207, NULL, '6RQL1wcvkUPDTo5YSMPdst', '2017-09-28 09:21:05', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1208, NULL, '57hkbSaqWn3lo0ZicBIPyM', '2017-09-28 09:21:12', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1209, NULL, '5y18O33gPpmgMoi2B2PXKH', '2017-09-28 09:21:41', '2017-09-28 09:53:49', '2017-09-28 09:53:49', 0),
(1210, NULL, '1SapwXZx5hqR34wBwh7F5v', '2017-09-28 09:24:22', '2017-09-28 10:00:40', '2017-09-28 10:00:40', 0),
(1211, NULL, '7eVZZLOuGUykUxpH9xMydC', '2017-09-28 09:24:26', '2017-09-28 10:00:40', '2017-09-28 10:00:40', 0),
(1212, NULL, '5LLYBh7CR7Cs1YWTxFhFm2', '2017-09-28 09:24:27', '2017-09-28 10:00:40', '2017-09-28 10:00:40', 0),
(1213, NULL, 'Fz2dqlErgMF9gGIA5TBmY', '2017-09-28 09:53:49', '2017-09-28 10:29:03', '2017-09-28 10:29:03', 0),
(1214, NULL, '3yht6DxbG7cuf3PbzNEN2v', '2017-09-28 09:53:52', '2017-09-28 10:29:03', '2017-09-28 10:29:03', 0),
(1215, NULL, '5LZljpP80LVzYDWBGqX4dP', '2017-09-28 09:53:52', '2017-09-28 10:29:03', '2017-09-28 10:29:03', 0),
(1216, NULL, '6j2p7X53lyKu7vnDgXgY6w', '2017-09-28 09:59:46', '2017-09-28 10:29:48', '2017-09-28 10:29:48', 0),
(1217, NULL, '33FQnKm4xsyjEeQtk1n6wS', '2017-09-28 09:59:51', '2017-09-28 10:30:31', '2017-09-28 10:30:31', 0),
(1218, NULL, '3Z1z8yDcTqh6qQ6sNm6GxK', '2017-09-28 09:59:51', '2017-09-28 10:30:31', '2017-09-28 10:30:31', 0),
(1219, NULL, '55p9Lc0ZPz73U0mAMnD6QS', '2017-09-28 10:00:38', '2017-09-28 10:30:52', '2017-09-28 10:30:52', 0),
(1220, NULL, 'QiosQn5Rswask7t4aMS4c', '2017-09-28 10:00:39', '2017-09-28 10:30:52', '2017-09-28 10:30:52', 0),
(1221, NULL, '3ZBnx3t7X0mZrOrHXecIUf', '2017-09-28 10:00:41', '2017-09-28 10:30:52', '2017-09-28 10:30:52', 0),
(1222, NULL, '5J6zRc456iQCJsyTb0NXPw', '2017-09-28 10:04:22', '2017-09-28 10:34:31', '2017-09-28 10:34:31', 0),
(1223, NULL, 'EXeMl3BYHyUfdxBJRk9xy', '2017-09-28 10:04:23', '2017-09-28 10:34:31', '2017-09-28 10:34:31', 0),
(1224, NULL, '6cu8U6KG1pkkbBoQ9SV8aG', '2017-09-28 10:04:25', '2017-09-28 10:34:31', '2017-09-28 10:34:31', 0),
(1225, NULL, '6nvNp0paLRMF3y5xLormtZ', '2017-09-28 10:05:48', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1226, NULL, '2iyGCaJc9AhN4qzkrOFtYB', '2017-09-28 10:05:49', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1227, NULL, '61iZg8JQAJvnm4652xanTV', '2017-09-28 10:05:52', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1228, NULL, '4BXlwfvoZF0WeI2KaXMzwK', '2017-09-28 10:05:56', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1229, NULL, '6uIeTIv4A26YaIUIkRJAIJ', '2017-09-28 10:06:33', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1230, NULL, '5sVSo43xQSLJ0vwO82If8l', '2017-09-28 10:06:34', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1231, NULL, '1nhbF4n8ZoFBrP0F0C4vR3', '2017-09-28 10:06:36', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1232, NULL, '5B78MWO0oCzN3FBdttwCBA', '2017-09-28 10:07:18', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1233, NULL, '7lLPGYPgBT9QB8c45Dnmjq', '2017-09-28 10:07:20', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1234, NULL, '4OkO2t1w8aFaC9vNSeAXVi', '2017-09-28 10:07:22', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1235, NULL, '3nBKftoMGHysmXZwT9gfN3', '2017-09-28 10:08:06', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1236, NULL, 'M4rKkd7XXp0if2jAidA0W', '2017-09-28 10:08:08', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1237, NULL, '42sbR1xQFQ8wQCiB2wWpuE', '2017-09-28 10:08:10', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1238, NULL, '2sXfvAfQI6msjgpq1k1Iaw', '2017-09-28 10:08:35', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1239, NULL, 'Nk5UUQv2n8v6EYlDCk1K6', '2017-09-28 10:08:36', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1240, NULL, '5F61MMn8DQBytkzIqNltHk', '2017-09-28 10:08:38', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1241, NULL, '7FkGGNmGt3mCdW4InLSawu', '2017-09-28 10:09:26', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1242, NULL, '4n6sVn1REir9jIKdeT66RI', '2017-09-28 10:09:28', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1243, NULL, 'zGXMVKTuhwRHK9yragWRn', '2017-09-28 10:09:41', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1244, NULL, '7jFJnc930K4jdcRuctJL4N', '2017-09-28 10:13:04', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1245, NULL, '1XvFTaLza7My0TtORRN5Fb', '2017-09-28 10:13:05', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1246, NULL, '2955vmOUlN2G5EAW5L01Vo', '2017-09-28 10:13:07', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1247, NULL, '55mnwAzyaxJR7VrJ1FHPt', '2017-09-28 10:13:52', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1248, NULL, '65vHDqJl1fLdf2VXyHXjHj', '2017-09-28 10:13:54', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1249, NULL, '5xPWopzwIwoR15KjeIOtQC', '2017-09-28 10:13:56', '2017-09-28 10:44:00', '2017-09-28 10:44:00', 0),
(1250, NULL, '5KunrP1hed0wr2pGOo32HF', '2017-09-28 10:14:24', '2017-09-28 10:54:14', '2017-09-28 10:54:14', 0),
(1251, NULL, '38TARjYEGZWaFzoZeummwK', '2017-09-28 10:14:26', '2017-09-28 10:54:14', '2017-09-28 10:54:14', 0),
(1252, NULL, '9wEbU83g8D87LfnQLq091', '2017-09-28 10:14:29', '2017-09-28 10:54:14', '2017-09-28 10:54:14', 0),
(1253, NULL, '3yp0aCiQrwY1uayUVhD28Q', '2017-09-28 10:14:38', '2017-09-28 10:54:14', '2017-09-28 10:54:14', 0),
(1254, NULL, '1LEAdUFnwr7JV1ieWvljw6', '2017-09-28 10:16:22', '2017-09-28 10:54:14', '2017-09-28 10:54:14', 0),
(1255, NULL, '6olUVDyWUyZoQjGPM63pwh', '2017-09-28 10:16:25', '2017-09-28 10:54:14', '2017-09-28 10:54:14', 0),
(1256, NULL, '18Ykw6zupN4GibDHeqVecq', '2017-09-28 10:16:26', '2017-09-28 10:54:14', '2017-09-28 10:54:14', 0),
(1257, NULL, '24XFY0W8O9uCcVhMTLaSoM', '2017-09-28 10:19:11', '2017-09-28 10:54:14', '2017-09-28 10:54:14', 0),
(1258, NULL, '5KfBT5nbySWFu4ItdhTzXX', '2017-09-28 10:19:40', '2017-09-28 10:54:14', '2017-09-28 10:54:14', 0),
(1259, NULL, '2nnWq4E9LpPlMBhBFDtKgB', '2017-09-28 10:20:20', '2017-09-28 10:54:14', '2017-09-28 10:54:14', 0),
(1260, NULL, '5l1f0CRpKFlAS3FbtH6Vw8', '2017-09-28 10:20:23', '2017-09-28 10:54:14', '2017-09-28 10:54:14', 0),
(1261, NULL, '3BGNKnjc7m2Ji19D0v5kwO', '2017-09-28 10:20:25', '2017-09-28 10:54:14', '2017-09-28 10:54:14', 0),
(1262, NULL, '3atDTcEzHRDb9G1xfmzXHk', '2017-09-28 10:29:03', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1263, NULL, '7A29RPbfDvcvOFkzrHo7r6', '2017-09-28 10:29:04', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1264, NULL, 'lKTqMb0hWt6OVaDhUgh6f', '2017-09-28 10:29:06', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1265, NULL, '3mPlDSzkE4UaXoAVw7Djo8', '2017-09-28 10:29:48', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1266, NULL, '5Fe3l22KzUG9ulFOdD0aTx', '2017-09-28 10:29:50', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1267, NULL, '6pyfjowLCx8UBhnrbEvAk9', '2017-09-28 10:29:52', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1268, NULL, 'mUTPeE6vsHbKLaDmVUZAE', '2017-09-28 10:30:31', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1269, NULL, '1WXdJHivPSSmBZhfypIeJJ', '2017-09-28 10:30:31', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1270, NULL, '36EI5M61kXy5H0MmanZ83a', '2017-09-28 10:30:31', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1271, NULL, '2guZhXfzwEAK9DhlNarEq', '2017-09-28 10:30:52', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1272, NULL, '3MejbbHXVdXK5em4j3dl7e', '2017-09-28 10:30:54', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1273, NULL, 'ICYZXzcWjxHfhvwodWelz', '2017-09-28 10:30:56', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1274, NULL, '2XFQgderQok33C2SaoUsfs', '2017-09-28 10:31:24', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1275, NULL, '3G5Pl3BQ4keoT2aX20OoYk', '2017-09-28 10:31:26', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1276, NULL, '225swKGQR7cDp1XC5hWIQf', '2017-09-28 10:31:28', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1277, NULL, '3Py3OrvDfYV0EaCmcxStW6', '2017-09-28 10:31:46', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1278, NULL, '3D6I0P4pjxlhePYTQ5Me9v', '2017-09-28 10:31:47', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1279, NULL, '2u6SheiscOyDYrfbuBMWha', '2017-09-28 10:31:49', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1280, NULL, '4uuk4XiFAm51gTVcDjVMw', '2017-09-28 10:34:06', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1281, NULL, '32UZgzRX6ULtm0MTESdfnV', '2017-09-28 10:34:08', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1282, NULL, 'oXvdjV2uujaMY9jPzh1w2', '2017-09-28 10:34:10', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1283, NULL, '1CC7a9CWgti8vEwYRnhZYd', '2017-09-28 10:34:30', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1284, NULL, '5zvG7rcFCYxXDxfFKNhOHC', '2017-09-28 10:34:33', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1285, NULL, '2dmopzelengWVMkA2ICZvR', '2017-09-28 10:34:34', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1286, NULL, '2BQoNaXAHQaH9zxb76X7nl', '2017-09-28 10:34:55', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1287, NULL, '6Ak7E3pyQSQdwzJx6WPPAP', '2017-09-28 10:34:57', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1288, NULL, 'yVvSPUU9Z0px1vvdj2vfr', '2017-09-28 10:34:59', '2017-09-28 11:05:21', '2017-09-28 11:05:21', 0),
(1289, NULL, '1I4o6ov99GqQ7H8yCeJsxg', '2017-09-28 10:42:08', '2017-09-28 11:12:39', '2017-09-28 11:12:39', 0),
(1290, NULL, 'veigZAG9aVOiGGqtoPds', '2017-09-28 10:44:00', '2017-09-28 11:17:48', '2017-09-28 11:17:48', 0),
(1291, NULL, '3F27jWfiYw9fhsDeMFp27m', '2017-09-28 10:53:11', '2017-09-28 11:25:19', '2017-09-28 11:25:19', 0),
(1292, NULL, '2gfyh8mT8ueyV37svkRmPh', '2017-09-28 10:54:14', '2017-09-28 11:25:19', '2017-09-28 11:25:19', 0),
(1293, NULL, '10rd1Ae2uH5nHBzWKLnaLO', '2017-09-28 10:54:23', '2017-09-28 11:25:19', '2017-09-28 11:25:19', 0),
(1294, NULL, '4patjdISWZv4TIe79umRdE', '2017-09-28 10:54:27', '2017-09-28 11:25:19', '2017-09-28 11:25:19', 0),
(1295, NULL, '1wdA5PEUhLs7fpUiwCL9t', '2017-09-28 10:54:32', '2017-09-28 11:25:19', '2017-09-28 11:25:19', 0),
(1296, NULL, '4zi11XQEzRnRYKCtV2Rv1I', '2017-09-28 10:54:32', '2017-09-28 11:25:19', '2017-09-28 11:25:19', 0),
(1297, NULL, '6dNhzAUsMHUGs13yCAsE6C', '2017-09-28 10:58:38', '2017-09-28 11:35:02', '2017-09-28 11:35:02', 0),
(1298, NULL, '513FIWYblAjWW6RjhbRF6C', '2017-09-28 11:00:05', '2017-09-28 11:35:02', '2017-09-28 11:35:02', 0),
(1299, NULL, '2xlwbd37bCzE2xHAuuOI8h', '2017-09-28 11:03:41', '2017-09-28 11:35:02', '2017-09-28 11:35:02', 0),
(1300, NULL, '35J77AyzqmSBkPdtRiv8OL', '2017-09-28 11:05:21', '2017-09-28 11:37:44', '2017-09-28 11:37:44', 0),
(1301, NULL, 'uOtH99fMNYsRiaLAsI6A', '2017-09-28 11:05:24', '2017-09-28 11:37:44', '2017-09-28 11:37:44', 0),
(1302, NULL, '3oFITvon6iA70g6cYEmrus', '2017-09-28 11:05:27', '2017-09-28 11:37:44', '2017-09-28 11:37:44', 0),
(1303, NULL, '5fvlJtpAiKa598jxQAaYtP', '2017-09-28 11:05:28', '2017-09-28 11:37:44', '2017-09-28 11:37:44', 0),
(1304, NULL, '66HrORhGjAT7PN3GrrR4Hi', '2017-09-28 11:06:18', '2017-09-28 11:37:44', '2017-09-28 11:37:44', 0),
(1305, NULL, '336kPmaNa7mEl90AYzHSpB', '2017-09-28 11:06:20', '2017-09-28 11:37:44', '2017-09-28 11:37:44', 0),
(1306, NULL, '2JmNgonErOtMKDyNJyaqst', '2017-09-28 11:06:24', '2017-09-28 11:37:44', '2017-09-28 11:37:44', 0),
(1307, NULL, '2PYwAiGOTNL2ZL2pOqoe9q', '2017-09-28 11:06:24', '2017-09-28 11:37:44', '2017-09-28 11:37:44', 0),
(1308, NULL, '2eBx0CdRoJNKIq0wacZhHj', '2017-09-28 11:09:13', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1309, NULL, '7QLUSFETRT6WRVOXpi5hhR', '2017-09-28 11:09:16', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1310, NULL, '2Nh4DVMZFmB7LGeisAwdAY', '2017-09-28 11:09:20', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1311, NULL, '79cNAnPTmawe1ISm7oyhpi', '2017-09-28 11:09:22', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1312, NULL, '15H0r0CZQFcCJIvt54UxnC', '2017-09-28 11:10:41', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1313, NULL, '5wEojaee5UBcHd87J1gKqj', '2017-09-28 11:10:43', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1314, NULL, '7RIXJJVBejDkirC3MltMWB', '2017-09-28 11:10:44', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1315, NULL, '47rX9i5QYrK5vNa7z9UxoV', '2017-09-28 11:12:39', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1316, NULL, '9jbcKZPy9wZnoxVdSVNEA', '2017-09-28 11:12:40', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1317, NULL, '29hGxwdxfnhOe5kAGAjCRk', '2017-09-28 11:12:41', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1318, NULL, '6FaTWqXLIpXQvmVFpzKnGS', '2017-09-28 11:12:58', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1319, NULL, '3hZXGiC9OiydtaRYQ2Sx7Q', '2017-09-28 11:13:45', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1320, NULL, 'irHp2IvvdZGWIRN0bKn3f', '2017-09-28 11:15:14', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1321, NULL, '1f7xAkqFBa3REAlevJrClc', '2017-09-28 11:15:14', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1322, NULL, '2OPNBeCKSIwxKGrRhvoPsP', '2017-09-28 11:17:34', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1323, NULL, '3hC2IjqgIrKYjJwHytdVmm', '2017-09-28 11:17:35', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1324, NULL, '4i17qh6ifcTJ2rYeKNNQzO', '2017-09-28 11:17:48', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1325, NULL, '7XPa0IqpTep6kv0Fco6ta5', '2017-09-28 11:20:46', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1326, NULL, 'TUlJV1iBizlMr8KDmhItj', '2017-09-28 11:20:48', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1327, NULL, '2f5mg8QAcy1rUS2MZDrucL', '2017-09-28 11:21:28', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1328, NULL, 'VoEMQYS0A1Su1PQrznZL5', '2017-09-28 11:21:32', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1329, NULL, '4AbXsPuEFEQc40zzGZLnCz', '2017-09-28 11:21:38', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1330, NULL, '5FEx7SK9DxBzTKzII5nHl3', '2017-09-28 11:22:31', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1331, NULL, '4txRO1UQ2l9pyM4fb7WhYS', '2017-09-28 11:22:35', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1332, NULL, '3sz5vADTpFABoS5TkZH2kY', '2017-09-28 11:22:35', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1333, NULL, '4xnnK7vCBgbFvN6X0o0Hz8', '2017-09-28 11:23:04', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1334, NULL, '2h2SLacY8Z3a9Fae546i2S', '2017-09-28 11:24:46', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1335, NULL, '4fW1CGx1KUBaOiiRpFM5R6', '2017-09-28 11:25:14', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1336, NULL, '76AWWcnWvZ4AuxFvqiFKHH', '2017-09-28 11:25:19', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1337, NULL, '2p3EZ6iqeZ0u4CFnJTVb9z', '2017-09-28 11:25:19', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1338, NULL, 'd1IwdoAjegRSTMFUypnD8', '2017-09-28 11:25:50', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1339, NULL, '3OA0pd2kHHG0qCmExMrrGB', '2017-09-28 11:25:54', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1340, NULL, '5JkWttk9WNr5pWVba9Jbtf', '2017-09-28 11:25:54', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1341, NULL, '56bGPgSJfiPwXBaRKXjz8k', '2017-09-28 11:26:14', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1342, NULL, '7NJqIX36ftXm0Yu3Sf3jZz', '2017-09-28 11:26:20', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1343, NULL, '1baHpMSSt84LloFPqBcceb', '2017-09-28 11:26:20', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1344, NULL, '6kfMAtX8y5hZVxnU4SqEO', '2017-09-28 11:26:44', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1345, NULL, '26pciNP4PediUIvNf2TzZw', '2017-09-28 11:26:49', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1346, NULL, '3Jp3dI7U78Pq9zTbckMVju', '2017-09-28 11:26:50', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1347, NULL, '2N1YG0NCHAbtmMQvyazS12', '2017-09-28 11:27:03', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1348, NULL, '3wGrfWNdJ4pnxEt5J8xZ7q', '2017-09-28 11:27:07', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1349, NULL, '5qU25U1nY0PXSvNdaZnpLm', '2017-09-28 11:27:08', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1350, NULL, '26a55quxaPnhHzKzjcVdZf', '2017-09-28 11:27:27', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1351, NULL, '10wN32cUSmF0slwK0vR7jR', '2017-09-28 11:27:31', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1352, NULL, 'kegFbhi6rG5EostLSR3jt', '2017-09-28 11:27:31', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1353, NULL, 'b3XrGjk6wNWcG2XIrvYBu', '2017-09-28 11:35:02', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1354, NULL, '3wOlScOO5x1OXIqJOw5DnZ', '2017-09-28 11:35:56', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1355, NULL, '4aP4V4o6BOdmyIVlPaB2ly', '2017-09-28 11:37:44', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1356, NULL, '7bhvleydL49KdzxAINbMCa', '2017-09-28 11:37:55', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1357, NULL, '5hDxNMEV7V7mV8mbzWKGMl', '2017-09-28 11:41:20', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1358, NULL, '1vZIhZRPpumvoysBwNoTuT', '2017-09-28 11:45:39', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1359, NULL, '1s1iQDNSgFpAzDMoo6NrWV', '2017-09-28 12:06:53', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1360, NULL, '7Fpgbz71NVUDO9saH86zeK', '2017-09-28 12:39:56', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1361, NULL, '3CPKEdwbK2QxrVxryznTMW', '2017-09-28 13:18:34', '2017-09-29 07:07:48', '2017-09-29 07:07:48', 0),
(1362, NULL, '603EMcn2kYyUCiTr0fk1IA', '2017-09-29 07:07:37', '2017-09-29 07:53:38', '2017-09-29 07:53:38', 0),
(1363, NULL, '6mkc61oKAylp5U3s2MGZJR', '2017-09-29 07:07:48', '2017-09-29 07:53:38', '2017-09-29 07:53:38', 0),
(1364, NULL, '5diBxEqJ1ngVCphfJyohML', '2017-09-29 07:53:38', '2017-09-29 08:26:16', '2017-09-29 08:26:16', 0),
(1365, NULL, '5Zbp9uirdrH54XcXP05VdC', '2017-09-29 07:53:38', '2017-09-29 08:26:16', '2017-09-29 08:26:16', 0),
(1366, NULL, '1TycKcz4CP9Df5RLaVYfhj', '2017-09-29 07:57:42', '2017-09-29 08:28:05', '2017-09-29 08:28:05', 0),
(1367, NULL, '2S63fQjKg1V5Yon2ntHf3K', '2017-09-29 08:14:40', '2017-09-29 09:43:36', '2017-09-29 09:43:36', 0),
(1368, NULL, '1Hj7BBXwlE3iCEkIvxksa3', '2017-09-29 08:26:16', '2017-09-29 09:43:36', '2017-09-29 09:43:36', 0),
(1369, NULL, '2VHx3Vz7zgleVqXfynnKlX', '2017-09-29 08:27:07', '2017-09-29 09:43:36', '2017-09-29 09:43:36', 0),
(1370, NULL, '6wl8RDwG45O8nBLVxmNRga', '2017-09-29 08:30:54', '2017-09-29 09:43:36', '2017-09-29 09:43:36', 0),
(1371, NULL, 'IoA6x1Fh4sGSGsn9ABuaZ', '2017-09-29 09:43:36', '2017-09-29 10:13:54', '2017-09-29 10:13:54', 0),
(1372, NULL, '7mP7kQra7VzJRGKamGRIfy', '2017-09-29 10:03:34', '2017-09-29 10:36:01', '2017-09-29 10:36:01', 0),
(1373, NULL, '1OTT0h8rytfIZ6GeAgJ7u4', '2017-09-29 10:07:04', '2017-09-29 10:39:51', '2017-09-29 10:39:51', 0),
(1374, NULL, '6E8rTwQEgKjyiD57zr4h7o', '2017-09-29 10:22:40', '2017-09-29 11:00:59', '2017-09-29 11:00:59', 0),
(1375, NULL, '5OV1JYSGqBzLIxplrl94oB', '2017-09-29 10:36:03', '2017-09-29 11:08:04', '2017-09-29 11:08:04', 0),
(1376, NULL, '71etlvxebCTaTnMFaYVHnh', '2017-09-29 11:04:12', '2017-09-29 11:34:13', '2017-09-29 11:34:13', 0),
(1377, NULL, '6eno5czJwAIPyr6aEXjoTf', '2017-09-29 11:08:07', '2017-09-29 11:38:09', '2017-09-29 11:38:09', 0),
(1378, NULL, 'hAymb1Ac3u93jQQPmlggR', '2017-09-29 11:10:59', '2017-09-29 11:42:34', '2017-09-29 11:42:34', 0),
(1379, NULL, '2ajjg8uK5m8LNkguLCUiGb', '2017-09-29 11:19:43', '2017-09-29 11:54:27', '2017-09-29 11:54:27', 0),
(1380, NULL, '2Aw5ageAMyKyqVL9xaDPXn', '2017-09-29 11:21:02', '2017-09-29 11:54:27', '2017-09-29 11:54:27', 0),
(1381, NULL, '40FRnXESOu0L6oxsfHu6Oh', '2017-09-29 11:34:25', '2017-09-29 12:07:17', '2017-09-29 12:07:17', 0),
(1382, NULL, '2IEayWeZh94umg0CrK3bTY', '2017-09-29 11:38:13', '2017-09-29 12:25:50', '2017-09-29 12:25:50', 0),
(1383, NULL, '1noUT7mpKCn8xVpKUyOUnN', '2017-09-29 11:49:18', '2017-09-29 12:25:50', '2017-09-29 12:25:50', 0),
(1384, NULL, '2G5E0HTDjpNxHvJ3GFQYE0', '2017-09-29 11:54:26', '2017-09-29 12:25:50', '2017-09-29 12:25:50', 0),
(1385, NULL, 'sk2zqO6q8cFWSyKQmm4LI', '2017-09-29 12:07:17', '2017-09-29 12:37:19', '2017-09-29 12:37:19', 0),
(1386, NULL, '1cmGMQznCSn3QhKsQQfiYH', '2017-09-29 12:25:49', '2017-09-29 12:56:21', '2017-09-29 12:56:21', 0),
(1387, NULL, '2T8HFmDPrO5q3QAX1GwRKu', '2017-09-29 12:27:08', '2017-09-29 13:05:34', '2017-09-29 13:05:34', 0),
(1388, NULL, '6exqMQ8xeCihmndF3HNrgQ', '2017-09-29 12:32:30', '2017-09-29 13:05:34', '2017-09-29 13:05:34', 0),
(1389, NULL, '1twhvM5LhqguCkezLqwk1a', '2017-09-29 12:42:05', '2017-09-29 13:12:47', '2017-09-29 13:12:47', 0),
(1390, NULL, '2kotFTegsvP0WCjaYqSx1f', '2017-09-29 12:56:21', '2017-09-29 13:27:05', '2017-09-29 13:27:05', 0),
(1391, NULL, '1GMsnLhgsQE2JJvb1q8q8L', '2017-09-29 13:05:33', '2017-09-29 15:25:47', '2017-09-29 15:25:47', 0),
(1392, NULL, '4LPGPW8YFrUNS7BwIVX6zz', '2017-09-29 13:13:44', '2017-09-29 15:25:47', '2017-09-29 15:25:47', 0),
(1393, NULL, '45j9cRwHycsgRjy642WlU2', '2017-09-29 13:14:22', '2017-09-29 15:25:47', '2017-09-29 15:25:47', 0),
(1394, NULL, '7gYMbaUtuO5nJDjIPXqnRl', '2017-09-29 15:25:47', '2017-09-29 16:12:09', '2017-09-29 16:12:09', 0),
(1395, NULL, '3nKBJmbupwPLSumFGgHZpc', '2017-09-29 16:12:09', '2017-10-02 07:20:54', '2017-10-02 07:20:54', 0),
(1396, NULL, '66tn3SDNjCVcJTZrp2Yql6', '2017-09-29 16:15:51', '2017-10-02 07:20:54', '2017-10-02 07:20:54', 0),
(1397, NULL, '1ADomZFjCkXTLFhlUVmCU3', '2017-10-02 07:20:54', '2017-10-02 08:04:24', '2017-10-02 08:04:24', 0),
(1398, NULL, '79iDYDf8VnoAe7bFuY68FD', '2017-10-02 07:21:17', '2017-10-02 08:04:24', '2017-10-02 08:04:24', 0),
(1399, NULL, '7KOErl7X8gno9PC4FNPeYq', '2017-10-02 08:04:24', '2017-10-02 08:49:59', '2017-10-02 08:49:59', 0),
(1400, NULL, '3SHAMcD2uQ5UZVPmPiAphW', '2017-10-02 08:13:38', '2017-10-02 08:49:59', '2017-10-02 08:49:59', 0),
(1401, NULL, '5yssrbVzdQY5FRL2O70uxB', '2017-10-02 08:49:58', '2017-10-02 09:20:01', '2017-10-02 09:20:01', 0),
(1402, NULL, '6zBSrmwiEG6Zs58eHAdhvL', '2017-10-02 09:18:50', '2017-10-02 09:50:48', '2017-10-02 09:50:48', 0),
(1403, NULL, '2lRbVTrNCkwUXoi0FxNHO9', '2017-10-02 09:20:01', '2017-10-02 09:50:48', '2017-10-02 09:50:48', 0),
(1404, NULL, '63KVc1JkL8aDAABSv1rLlf', '2017-10-02 09:50:47', '2017-10-02 10:21:36', '2017-10-02 10:21:36', 0),
(1405, NULL, '71MvCpPQeulsvWrs1tWPBU', '2017-10-02 10:21:35', '2017-10-02 11:02:02', '2017-10-02 11:02:02', 0),
(1406, NULL, '42lbTXs5aGNgzHDaxSc0oL', '2017-10-02 10:27:57', '2017-10-02 11:02:02', '2017-10-02 11:02:02', 0),
(1407, NULL, '46uLOkBgJFef83deqhqjNS', '2017-10-02 11:02:01', '2017-10-02 11:37:57', '2017-10-02 11:37:57', 0),
(1408, NULL, 'W2E4fZjHAS8sw4Kc8G4kE', '2017-10-02 11:02:45', '2017-10-02 11:37:57', '2017-10-02 11:37:57', 0),
(1409, NULL, '6ZzDH0RT0pytd1ETISYc0Z', '2017-10-02 11:02:51', '2017-10-02 11:37:57', '2017-10-02 11:37:57', 0),
(1410, NULL, '4Sea424evWZ017T5NpGv82', '2017-10-02 11:10:12', '2017-10-02 11:41:56', '2017-10-02 11:41:56', 0),
(1411, NULL, '5lkplHPdMX6qYBrRSrDM2', '2017-10-02 11:37:57', '2017-10-02 12:15:49', '2017-10-02 12:15:49', 0),
(1412, NULL, '48TUA6ummlvyW0ipOSPOOG', '2017-10-02 11:38:47', '2017-10-02 12:15:49', '2017-10-02 12:15:49', 0),
(1413, NULL, '2xZBZlqjjtXWyG4bYBTMJr', '2017-10-02 12:15:48', '2017-10-02 12:47:27', '2017-10-02 12:47:27', 0),
(1414, NULL, '6CMp1GwUR7dmcLFoV5pqk7', '2017-10-02 12:15:50', '2017-10-02 12:47:27', '2017-10-02 12:47:27', 0),
(1415, NULL, 'jMcnoXmvr4oBTZQgvXzCL', '2017-10-02 12:35:30', '2017-10-02 13:07:11', '2017-10-02 13:07:11', 0),
(1416, NULL, '4ofIrbiAZFY8eTAG2PiULO', '2017-10-02 12:49:38', '2017-10-02 14:32:04', '2017-10-02 14:32:04', 0),
(1417, NULL, 'blo8GJMyHMd4qKMQlaN6U', '2017-10-02 13:07:19', '2017-10-02 14:32:04', '2017-10-02 14:32:04', 0),
(1418, NULL, '5s0H5gfpPPZ1SmNbQRXi3Z', '2017-10-02 14:32:04', '2017-10-02 15:08:50', '2017-10-02 15:08:50', 0),
(1419, NULL, '4nbURV2qXIuByKV1FwhomS', '2017-10-02 15:08:50', '2017-10-02 17:41:30', '2017-10-02 17:41:30', 0),
(1420, NULL, '6OmP10mZKY5gpm75kSFv6V', '2017-10-02 17:49:00', '2017-10-03 06:29:18', '2017-10-03 06:29:18', 0),
(1421, NULL, '2OBOEybeTPEgOlVIJZO0wA', '2017-10-03 06:29:18', '2017-10-03 07:17:46', '2017-10-03 07:17:46', 0),
(1422, NULL, '4Tw7Z6dHhy2o2uSBKDRcOQ', '2017-10-03 06:32:32', '2017-10-03 07:17:46', '2017-10-03 07:17:46', 0),
(1423, NULL, '4iMXb1FQTv93u2ABKQMWRw', '2017-10-03 06:55:09', '2017-10-03 07:26:25', '2017-10-03 07:26:25', 0),
(1424, NULL, '3XY7tTAyjOWYCc9Y2hjPaT', '2017-10-03 07:26:25', '2017-10-03 08:08:32', '2017-10-03 08:08:32', 0),
(1425, NULL, 'f52ar6FGaxnAkuVD10cdc', '2017-10-03 07:26:58', '2017-10-03 08:08:32', '2017-10-03 08:08:32', 0),
(1426, NULL, 'KcyJHgguP4PkEO2WnJeIY', '2017-10-03 07:49:46', '2017-10-03 08:22:33', '2017-10-03 08:22:33', 0),
(1427, NULL, '4RcqZs9oun3vF6YHC2a3xx', '2017-10-03 08:08:32', '2017-10-03 08:38:33', '2017-10-03 08:38:33', 0),
(1428, NULL, '4EB8v2L0cN1ZWJVNJCWAup', '2017-10-03 08:08:45', '2017-10-03 08:38:55', '2017-10-03 08:38:55', 0),
(1429, NULL, '1VlAWqU5YRBXGgUtTIzkzQ', '2017-10-03 08:12:09', '2017-10-03 08:42:18', '2017-10-03 08:42:18', 0),
(1430, NULL, '3kfoJ7HbwD3fRISq3Kd7El', '2017-10-03 08:12:11', '2017-10-03 08:42:18', '2017-10-03 08:42:18', 0),
(1431, NULL, '4GCycnBqV8gayyds3RGipB', '2017-10-03 08:17:48', '2017-10-03 08:48:38', '2017-10-03 08:48:38', 0),
(1432, NULL, '5PSvVMl0U7BSpuMuUsaPqp', '2017-10-03 08:23:46', '2017-10-03 08:54:26', '2017-10-03 08:54:26', 0);
INSERT INTO `users__tokens` (`id`, `id_usuario`, `codigo`, `creacion`, `modificacion`, `expiracion`, `valido`) VALUES
(1433, NULL, '3tXiRQAgsWJxg4bUduDqth', '2017-10-03 08:28:26', '2017-10-03 09:42:08', '2017-10-03 09:42:08', 0),
(1434, NULL, '4EQNXTYCTKzWHGTYPz4eED', '2017-10-03 08:42:16', '2017-10-03 09:42:08', '2017-10-03 09:42:08', 0),
(1435, NULL, '1flyuJeAA8DzM4UphHaAXz', '2017-10-03 08:42:18', '2017-10-03 09:42:08', '2017-10-03 09:42:08', 0),
(1436, NULL, '4WePfxJzjFwYe9VHDlI4Nz', '2017-10-03 09:42:07', '2017-10-03 10:12:55', '2017-10-03 10:12:55', 0),
(1437, NULL, '3N9cVFgSO9ywdKwUJncQDz', '2017-10-03 09:42:16', '2017-10-03 10:12:55', '2017-10-03 10:12:55', 0),
(1438, NULL, '6KWcdEIFACErrJrGYazr3B', '2017-10-03 09:43:19', '2017-10-03 10:13:37', '2017-10-03 10:13:37', 0),
(1439, NULL, '46lYXLZ0e0Zgt3BUIz45Np', '2017-10-03 09:52:12', '2017-10-03 10:26:38', '2017-10-03 10:26:38', 0),
(1440, NULL, 'hQERkUHJJLiFX0aHbIsYt', '2017-10-03 10:12:55', '2017-10-03 10:43:00', '2017-10-03 10:43:00', 0),
(1441, NULL, '37svHGcFrwrKPAZmMoFqYO', '2017-10-03 10:12:57', '2017-10-03 10:43:00', '2017-10-03 10:43:00', 0),
(1442, NULL, '1sRZJZPBc3OOssBvc3L8g1', '2017-10-03 10:27:17', '2017-10-03 10:57:57', '2017-10-03 10:57:57', 0),
(1443, NULL, '3wmo1zRlFN8T4zOgbqMiGA', '2017-10-03 10:43:00', '2017-10-03 11:16:08', '2017-10-03 11:16:08', 0),
(1444, NULL, '7PDUsNqbuu7RGfzNMGd6Hs', '2017-10-03 10:43:02', '2017-10-03 11:16:08', '2017-10-03 11:16:08', 0),
(1445, NULL, '6wNBqUPbRnaVoS2durahA9', '2017-10-03 11:16:08', '2017-10-03 11:46:29', '2017-10-03 11:46:29', 0),
(1446, NULL, '6SUTZ53f1hy91iiYuMbxZZ', '2017-10-03 11:26:26', '2017-10-03 11:57:22', '2017-10-03 11:57:22', 0),
(1447, NULL, '2pfpiOSZCljf4DkIFb1XfF', '2017-10-03 11:46:29', '2017-10-03 12:18:27', '2017-10-03 12:18:27', 0),
(1448, NULL, '7KlLVageAlHePDh9VXgVDl', '2017-10-03 12:18:27', '2017-10-03 12:49:00', '2017-10-03 12:49:00', 0),
(1449, NULL, 'MGfXbRCz1IA5M82WpsWkB', '2017-10-03 12:49:00', '2017-10-03 13:20:31', '2017-10-03 13:20:31', 0),
(1450, NULL, '3pUibencc1xY9HvfyfbJJM', '2017-10-03 13:20:30', '2017-10-03 14:36:06', '2017-10-03 14:36:06', 0),
(1451, NULL, '2TahJjX9o6zsCvoQBKmdXj', '2017-10-03 13:47:59', '2017-10-03 14:36:06', '2017-10-03 14:36:06', 0),
(1452, NULL, 'e3z0yFYeVCtWV5mFCFYYR', '2017-10-03 14:36:06', '2017-10-04 06:52:44', '2017-10-04 06:52:44', 0),
(1453, NULL, '7MldLxHcUlL0XHL3UGCYtO', '2017-10-04 06:52:44', '2017-10-04 07:29:09', '2017-10-04 07:29:09', 0),
(1454, NULL, '74JFWuGJ0jAlXr18KIPCXu', '2017-10-04 06:54:23', '2017-10-04 07:29:09', '2017-10-04 07:29:09', 0),
(1455, NULL, '4sjRqGAT6INq2aqY5whYzv', '2017-10-04 07:15:55', '2017-10-04 07:46:43', '2017-10-04 07:46:43', 0),
(1456, NULL, '4aEjMnqrXVRoHbrkQhpSul', '2017-10-04 07:16:44', '2017-10-04 07:46:52', '2017-10-04 07:46:52', 0),
(1457, NULL, '1KjrIlyUo6RGT54xJS117e', '2017-10-04 07:29:09', '2017-10-04 07:59:15', '2017-10-04 07:59:15', 0),
(1458, NULL, '6FNLo0aTCtTm86uOsD9tYs', '2017-10-04 07:51:25', '2017-10-04 08:23:58', '2017-10-04 08:23:58', 0),
(1459, NULL, 'WcPd1pSJvjY2a3aTv56zN', '2017-10-04 07:59:15', '2017-10-04 08:32:04', '2017-10-04 08:32:04', 0),
(1460, NULL, '1hZ8rWz84KinXwyrI8PwGN', '2017-10-04 08:04:07', '2017-10-04 08:34:23', '2017-10-04 08:34:23', 0),
(1461, NULL, '6sHl63KfTfxvn2FbvFSYjA', '2017-10-04 08:18:07', '2017-10-04 08:48:55', '2017-10-04 08:48:55', 0),
(1462, NULL, '6tGwLfJjYd7djxauIaEMsT', '2017-10-04 08:21:20', '2017-10-04 08:54:22', '2017-10-04 08:54:22', 0),
(1463, NULL, '7Btnytc2CkZ7XioHBhPd3', '2017-10-04 08:23:58', '2017-10-04 08:54:22', '2017-10-04 08:54:22', 0),
(1464, NULL, '330jn7R9In1Fj9f1DbnTKv', '2017-10-04 08:44:09', '2017-10-04 09:25:11', '2017-10-04 09:25:10', 0),
(1465, NULL, '4FSg2xTFAarl6ihw41gFeu', '2017-10-04 08:47:25', '2017-10-04 09:25:11', '2017-10-04 09:25:10', 0),
(1466, NULL, '3o5PCEiz3otAa9u9kr0Dat', '2017-10-04 08:54:22', '2017-10-04 09:25:11', '2017-10-04 09:25:10', 0),
(1467, NULL, '1f4peR0GrNiTnDu7dscNiD', '2017-10-04 09:25:10', '2017-10-04 09:58:08', '2017-10-04 09:58:08', 0),
(1468, NULL, '6oKvLAPerb03sqNkcGAS9S', '2017-10-04 09:50:10', '2017-10-04 10:21:56', '2017-10-04 10:21:56', 0),
(1469, NULL, 'NH8kmoRf4xe6JeIyTtOlb', '2017-10-04 10:21:56', '2017-10-04 10:52:36', '2017-10-04 10:52:36', 0),
(1470, NULL, '2tMVBvLVeoeQZZnghdwO3e', '2017-10-04 10:22:22', '2017-10-04 10:52:36', '2017-10-04 10:52:36', 0),
(1471, NULL, '5yA76hvrSECb6peWLCz1qU', '2017-10-04 10:52:36', '2017-10-04 11:25:59', '2017-10-04 11:25:59', 0),
(1472, NULL, 'oZX250EqbBqltF3AT5lpv', '2017-10-04 11:25:59', '2017-10-04 12:02:15', '2017-10-04 12:02:15', 0),
(1473, NULL, '3WhwP17EdZ2Xj5CCzOjcO0', '2017-10-04 11:45:53', '2017-10-04 12:16:19', '2017-10-04 12:16:19', 0),
(1474, NULL, '3pzZdTphSPknw88GEiC4dK', '2017-10-04 12:16:16', '2017-10-04 12:54:41', '2017-10-04 12:54:41', 0),
(1475, NULL, 'OlBJy8nqdhAAjVIcSaQYY', '2017-10-04 12:17:18', '2017-10-04 12:54:41', '2017-10-04 12:54:41', 0),
(1476, NULL, 'wB5buoYDzg8PTVps6M1vF', '2017-10-04 12:54:41', '2017-10-04 13:25:21', '2017-10-04 13:25:21', 0),
(1477, NULL, '36yTTsUYNN6PibinfkhPKk', '2017-10-04 12:54:49', '2017-10-04 13:25:21', '2017-10-04 13:25:21', 0),
(1478, NULL, '3Hd5U31gFyfuG2unKYL99m', '2017-10-04 13:25:21', '2017-10-04 15:41:12', '2017-10-04 15:41:12', 0),
(1479, NULL, 'Rw4am72ctT6B0GfTHubre', '2017-10-05 06:02:08', '2017-10-05 07:06:11', '2017-10-05 07:06:11', 0),
(1480, NULL, '1dkjE5gi8EuuYFk8UecMSD', '2017-10-05 07:06:11', '2017-10-05 07:51:30', '2017-10-05 07:51:30', 0),
(1481, NULL, '43KimmukubD3YXiYJVTkR1', '2017-10-05 07:10:33', '2017-10-05 07:51:30', '2017-10-05 07:51:30', 0),
(1482, NULL, '9dIOXsi07xCsBgP9kErFb', '2017-10-05 07:12:00', '2017-10-05 07:51:30', '2017-10-05 07:51:30', 0),
(1483, NULL, '1SATvlNUE4BDkVxB47bQzd', '2017-10-05 07:15:33', '2017-10-05 07:51:30', '2017-10-05 07:51:30', 0),
(1484, NULL, 'am7FOPM5ltvqWRWJH4jCb', '2017-10-05 07:16:16', '2017-10-05 07:51:30', '2017-10-05 07:51:30', 0),
(1485, NULL, '628jF6pYZPOAKK6PeaXjCi', '2017-10-05 07:31:23', '2017-10-05 08:02:32', '2017-10-05 08:02:32', 0),
(1486, NULL, '3saLltW9M5wFoxmqpysDFo', '2017-10-05 07:33:32', '2017-10-05 08:03:57', '2017-10-05 08:03:57', 0),
(1487, NULL, '1YZfzeFkKwuRi44csrhjfM', '2017-10-05 07:51:30', '2017-10-05 08:23:20', '2017-10-05 08:23:20', 0),
(1488, NULL, '5b47WjbHgi34vdHJPAJ0wM', '2017-10-05 07:59:01', '2017-10-05 08:34:33', '2017-10-05 08:34:33', 0),
(1489, NULL, '1z8stLfLIOjZT73ypyAsMU', '2017-10-05 08:23:22', '2017-10-05 08:54:30', '2017-10-05 08:54:30', 0),
(1490, NULL, '6ySrGP19Jbdzdj6ySynIBy', '2017-10-05 08:34:33', '2017-10-05 10:43:01', '2017-10-05 10:43:01', 0),
(1491, NULL, 'Ts3ncMOnFH5H50nVUESV6', '2017-10-05 08:54:29', '2017-10-05 10:43:01', '2017-10-05 10:43:01', 0),
(1492, NULL, '7KexpFoaBrdP2nBzLXIOSn', '2017-10-05 08:54:41', '2017-10-05 10:43:01', '2017-10-05 10:43:01', 0),
(1493, 5, '7EBRKjZyZJjwNzuPdrah5h', '2017-10-05 10:46:48', '2017-10-05 11:18:51', '2017-10-05 11:18:51', 0),
(1494, 5, '724uo49nJzR0t0cBIbPZcO', '2017-10-05 11:18:58', '2017-10-05 11:20:15', '2017-10-05 11:20:15', 0),
(1495, 5, '4H1daYAbS9SZtXiO7njSWg', '2017-10-05 11:20:17', '2017-10-05 11:51:41', '2017-10-05 11:51:41', 0),
(1496, NULL, 'pjvgRvMpRudGjRCid30gw', '2017-10-05 11:28:34', '2017-10-05 11:58:49', '2017-10-05 11:58:49', 0),
(1497, NULL, 'JFwG2lrPMHiBfvd0DXFJO', '2017-10-05 11:50:37', '2017-10-05 12:21:31', '2017-10-05 12:21:31', 0),
(1498, 5, '30fmxwMu3bSdpVf7ZwHkX2', '2017-10-05 11:50:37', '2017-10-05 13:26:46', '2017-10-05 13:26:46', 0),
(1499, NULL, '50Q8FVhjDQSr4HPQYA3WfK', '2017-10-05 13:26:46', '2017-10-05 14:06:34', '2017-10-05 14:06:34', 0),
(1500, NULL, '1ciz7Gi1mw1YmHo3jNGzPg', '2017-10-05 13:31:19', '2017-10-05 14:06:34', '2017-10-05 14:06:34', 0),
(1501, NULL, '7396L1GFcVF3hVNQwdVdX4', '2017-10-05 13:32:35', '2017-10-05 14:06:34', '2017-10-05 14:06:34', 0),
(1502, NULL, '5PrmoM0eUfgesGkICGj8A1', '2017-10-05 14:06:34', '2017-10-05 14:50:13', '2017-10-05 14:50:13', 0),
(1503, NULL, '1Yu0YE8xfp1B8hxFlIZUVV', '2017-10-05 14:50:13', '2017-10-05 17:07:07', '2017-10-05 17:07:07', 0),
(1504, NULL, '6R81aBJcAKxN7Upg8qrRa7', '2017-10-05 15:13:16', '2017-10-05 17:07:07', '2017-10-05 17:07:07', 0),
(1505, NULL, '7HCzV4JTSSWXQRSDtopMVB', '2017-10-05 15:13:17', '2017-10-05 17:07:07', '2017-10-05 17:07:07', 0),
(1506, NULL, 'xude4pe6tbmv4l0uFAWC0', '2017-10-06 06:19:50', '2017-10-06 06:52:53', '2017-10-06 06:52:53', 0),
(1507, NULL, '3FljGJGY6F9ZLOAVj4oHxw', '2017-10-06 06:52:53', '2017-10-06 07:23:30', '2017-10-06 07:23:30', 0),
(1508, NULL, '344vQ2X0IvbQC7W07SOo6j', '2017-10-06 06:56:04', '2017-10-06 07:30:15', '2017-10-06 07:30:15', 0),
(1509, NULL, '3r44tW1hvwBpoIPzFbZoHq', '2017-10-06 07:18:47', '2017-10-06 07:53:59', '2017-10-06 07:53:59', 0),
(1510, NULL, '3Blwk10WLsFttbEU2Bitu', '2017-10-06 07:23:29', '2017-10-06 07:53:59', '2017-10-06 07:53:59', 0),
(1511, NULL, '76KoDzZeCQpIgNuUpoiSz8', '2017-10-06 07:25:55', '2017-10-06 07:57:04', '2017-10-06 07:57:04', 0),
(1512, NULL, 'pmtOvRY3P9qmDAlOLS7aP', '2017-10-06 07:30:14', '2017-10-06 08:02:42', '2017-10-06 08:02:42', 0),
(1513, NULL, '726cFubty5DhUYBpNgaUTL', '2017-10-06 07:33:04', '2017-10-06 20:00:58', '2017-10-06 20:00:58', 0),
(1514, NULL, '5mqbxtNsUoX8UmU0jcSP47', '2017-10-06 07:33:37', '2017-10-06 20:00:58', '2017-10-06 20:00:58', 0),
(1515, NULL, 'rx27acG8RRu31lMUHyn8z', '2017-10-06 07:34:06', '2017-10-06 20:00:58', '2017-10-06 20:00:58', 0),
(1516, NULL, '2DozbYcE6UrNopVs3Yy9mg', '2017-10-06 07:57:04', '2017-10-06 20:00:58', '2017-10-06 20:00:58', 0),
(1517, NULL, 'HdYB2qVB7bshOGjT89zF9', '2017-10-06 07:57:04', '2017-10-06 20:00:58', '2017-10-06 20:00:58', 0),
(1518, NULL, '3OI9js7Eua7PEz9rnILwmq', '2017-10-09 07:01:00', '2017-10-09 09:34:19', '2017-10-09 09:34:19', 0),
(1519, NULL, '45dhsoxC0DAzqFpYfSbiJa', '2017-10-09 07:05:04', '2017-10-09 09:34:19', '2017-10-09 09:34:19', 0),
(1520, NULL, 'k45qeS8FlhUyAoPa0N2gF', '2017-10-09 09:34:19', '2017-10-09 11:21:19', '2017-10-09 11:21:19', 0),
(1521, NULL, '2MHQ7K52bOaP3wS8HELQUf', '2017-10-10 08:09:12', '2017-10-11 07:19:58', '2017-10-11 07:19:58', 0),
(1522, NULL, '9GEukexW4WghmHTjFUu65', '2017-10-11 07:29:05', '2017-10-11 07:29:05', NULL, 1),
(1523, NULL, '74wANzdeFN9YuC006slG9X', '2017-10-11 07:31:44', '2017-10-11 07:31:44', NULL, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `formularios`
--
ALTER TABLE `formularios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `idiomas`
--
ALTER TABLE `idiomas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `region` (`region`);

--
-- Indices de la tabla `idiomas__claves`
--
ALTER TABLE `idiomas__claves`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQUE` (`id_seccion`,`nombre`) USING BTREE;

--
-- Indices de la tabla `idiomas__secciones`
--
ALTER TABLE `idiomas__secciones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indices de la tabla `idiomas__textos`
--
ALTER TABLE `idiomas__textos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_clave` (`id_clave`,`id_idioma`),
  ADD KEY `FK_IDIOMAS` (`id_idioma`);

--
-- Indices de la tabla `noticias__archivos`
--
ALTER TABLE `noticias__archivos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_noticia` (`id_noticia`);

--
-- Indices de la tabla `noticias__categorias`
--
ALTER TABLE `noticias__categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticias__categories`
--
ALTER TABLE `noticias__categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticias__comments`
--
ALTER TABLE `noticias__comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indices de la tabla `noticias__files`
--
ALTER TABLE `noticias__files`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `owner_id` (`owner_id`,`owner_type`,`type`);

--
-- Indices de la tabla `noticias__galeria`
--
ALTER TABLE `noticias__galeria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_noticia` (`id_noticia`);

--
-- Indices de la tabla `noticias__posts`
--
ALTER TABLE `noticias__posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `POST_URL` (`url`) USING BTREE,
  ADD KEY `category_id` (`category_id`);

--
-- Indices de la tabla `noticias__rel_tags`
--
ALTER TABLE `noticias__rel_tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `post_id` (`post_id`,`tag_id`),
  ADD KEY `FK_NOTICIAS_REL_TAGS_TAG` (`tag_id`);

--
-- Indices de la tabla `noticias__tags`
--
ALTER TABLE `noticias__tags`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `codigo_cliente` (`codigo_cliente`),
  ADD KEY `codigo_token` (`id_token`),
  ADD KEY `provincia_dir_facturacion` (`provincia_dir_facturacion`),
  ADD KEY `pais_dir_facturacion` (`pais_dir_facturacion`),
  ADD KEY `codigo_dir_envio` (`codigo_dir_envio`);

--
-- Indices de la tabla `orders__articulos`
--
ALTER TABLE `orders__articulos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_order_2` (`id_order`,`articulo`,`talla`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `articulo` (`articulo`);

--
-- Indices de la tabla `orders__refunds`
--
ALTER TABLE `orders__refunds`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orders__request`
--
ALTER TABLE `orders__request`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_order` (`id_order`);

--
-- Indices de la tabla `password_requests`
--
ALTER TABLE `password_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `portes__paises`
--
ALTER TABLE `portes__paises`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `locale` (`locale`);

--
-- Indices de la tabla `portes__provincias`
--
ALTER TABLE `portes__provincias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `portes__zonas`
--
ALTER TABLE `portes__zonas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cod_pais` (`id_pais`,`id_provincia`,`id_transportista`);

--
-- Indices de la tabla `preferences`
--
ALTER TABLE `preferences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `preferences__files`
--
ALTER TABLE `preferences__files`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `owner_id` (`owner_id`,`owner_type`,`type`);

--
-- Indices de la tabla `preferences__galeria`
--
ALTER TABLE `preferences__galeria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indices de la tabla `productos__categorias`
--
ALTER TABLE `productos__categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos__comments`
--
ALTER TABLE `productos__comments`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos__files`
--
ALTER TABLE `productos__files`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos__galeria`
--
ALTER TABLE `productos__galeria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos__relaccionados`
--
ALTER TABLE `productos__relaccionados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos__rel_tags`
--
ALTER TABLE `productos__rel_tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `post_id` (`post_id`,`tag_id`),
  ADD KEY `FK_NOTICIAS_REL_TAGS_TAG` (`tag_id`);

--
-- Indices de la tabla `productos__tags`
--
ALTER TABLE `productos__tags`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos__valores`
--
ALTER TABLE `productos__valores`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_producto` (`id_producto`,`clave`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `username_2` (`username`,`baja`);

--
-- Indices de la tabla `users__data`
--
ALTER TABLE `users__data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_USERS_DATA_USER_ID` (`id_user`);

--
-- Indices de la tabla `users__files`
--
ALTER TABLE `users__files`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `owner_id` (`owner_id`,`owner_type`,`type`);

--
-- Indices de la tabla `users__login_attempts`
--
ALTER TABLE `users__login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users__rel_roles`
--
ALTER TABLE `users__rel_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id_2` (`user_id`,`role_id`),
  ADD UNIQUE KEY `user_id_3` (`user_id`,`role_id`),
  ADD UNIQUE KEY `user_id_4` (`user_id`,`role_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indices de la tabla `users__roles`
--
ALTER TABLE `users__roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `users__tokens`
--
ALTER TABLE `users__tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `formularios`
--
ALTER TABLE `formularios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT de la tabla `idiomas`
--
ALTER TABLE `idiomas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `idiomas__claves`
--
ALTER TABLE `idiomas__claves`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=214;
--
-- AUTO_INCREMENT de la tabla `idiomas__secciones`
--
ALTER TABLE `idiomas__secciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT de la tabla `idiomas__textos`
--
ALTER TABLE `idiomas__textos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=189;
--
-- AUTO_INCREMENT de la tabla `noticias__archivos`
--
ALTER TABLE `noticias__archivos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT de la tabla `noticias__categorias`
--
ALTER TABLE `noticias__categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `noticias__categories`
--
ALTER TABLE `noticias__categories`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `noticias__comments`
--
ALTER TABLE `noticias__comments`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `noticias__files`
--
ALTER TABLE `noticias__files`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT de la tabla `noticias__galeria`
--
ALTER TABLE `noticias__galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT de la tabla `noticias__posts`
--
ALTER TABLE `noticias__posts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `noticias__rel_tags`
--
ALTER TABLE `noticias__rel_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `noticias__tags`
--
ALTER TABLE `noticias__tags`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=453;
--
-- AUTO_INCREMENT de la tabla `orders__articulos`
--
ALTER TABLE `orders__articulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=428;
--
-- AUTO_INCREMENT de la tabla `orders__refunds`
--
ALTER TABLE `orders__refunds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `orders__request`
--
ALTER TABLE `orders__request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=210000380;
--
-- AUTO_INCREMENT de la tabla `password_requests`
--
ALTER TABLE `password_requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `portes__paises`
--
ALTER TABLE `portes__paises`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `portes__provincias`
--
ALTER TABLE `portes__provincias`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT de la tabla `portes__zonas`
--
ALTER TABLE `portes__zonas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `preferences__files`
--
ALTER TABLE `preferences__files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `preferences__galeria`
--
ALTER TABLE `preferences__galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `productos__categorias`
--
ALTER TABLE `productos__categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `productos__comments`
--
ALTER TABLE `productos__comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `productos__files`
--
ALTER TABLE `productos__files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;
--
-- AUTO_INCREMENT de la tabla `productos__galeria`
--
ALTER TABLE `productos__galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `productos__relaccionados`
--
ALTER TABLE `productos__relaccionados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT de la tabla `productos__rel_tags`
--
ALTER TABLE `productos__rel_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `productos__tags`
--
ALTER TABLE `productos__tags`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `productos__valores`
--
ALTER TABLE `productos__valores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=353;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `users__data`
--
ALTER TABLE `users__data`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `users__files`
--
ALTER TABLE `users__files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `users__login_attempts`
--
ALTER TABLE `users__login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `users__rel_roles`
--
ALTER TABLE `users__rel_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT de la tabla `users__tokens`
--
ALTER TABLE `users__tokens`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1524;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `idiomas__claves`
--
ALTER TABLE `idiomas__claves`
  ADD CONSTRAINT `FK_SECCIONES` FOREIGN KEY (`id_seccion`) REFERENCES `idiomas__secciones` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `idiomas__textos`
--
ALTER TABLE `idiomas__textos`
  ADD CONSTRAINT `FK_CLAVES` FOREIGN KEY (`id_clave`) REFERENCES `idiomas__claves` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_IDIOMAS` FOREIGN KEY (`id_idioma`) REFERENCES `idiomas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `noticias__archivos`
--
ALTER TABLE `noticias__archivos`
  ADD CONSTRAINT `FK_NOTICIAS_ARCHIVOS_POST` FOREIGN KEY (`id_noticia`) REFERENCES `noticias__posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `noticias__comments`
--
ALTER TABLE `noticias__comments`
  ADD CONSTRAINT `FK_NOTICIAS_COMMENTS_POST` FOREIGN KEY (`post_id`) REFERENCES `noticias__posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `noticias__galeria`
--
ALTER TABLE `noticias__galeria`
  ADD CONSTRAINT `FK_NOTICIAS_GALERIA_POST` FOREIGN KEY (`id_noticia`) REFERENCES `noticias__posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `noticias__posts`
--
ALTER TABLE `noticias__posts`
  ADD CONSTRAINT `FK_NOTICIAS_POSTS_CATEGORIA` FOREIGN KEY (`category_id`) REFERENCES `noticias__categories` (`id`);

--
-- Filtros para la tabla `noticias__rel_tags`
--
ALTER TABLE `noticias__rel_tags`
  ADD CONSTRAINT `FK_NOTICIAS_REL_TAGS_POST` FOREIGN KEY (`post_id`) REFERENCES `noticias__posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_NOTICIAS_REL_TAGS_TAG` FOREIGN KEY (`tag_id`) REFERENCES `noticias__tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FK_ORDERS_TOKEN_ID` FOREIGN KEY (`id_token`) REFERENCES `users__tokens` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ORDERS_USER_ID` FOREIGN KEY (`codigo_cliente`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `orders__articulos`
--
ALTER TABLE `orders__articulos`
  ADD CONSTRAINT `FK_ORDERS_ARTICULOS_ID` FOREIGN KEY (`id_order`) REFERENCES `orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `orders__request`
--
ALTER TABLE `orders__request`
  ADD CONSTRAINT `FK_ORDERS_REQUEST_ORDER_ID` FOREIGN KEY (`id_order`) REFERENCES `orders` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `productos__valores`
--
ALTER TABLE `productos__valores`
  ADD CONSTRAINT `FK_PRODUCTOS_VALORES_PRODUCTO` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `users__data`
--
ALTER TABLE `users__data`
  ADD CONSTRAINT `FK_USERS_DATA_ID` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `users__rel_roles`
--
ALTER TABLE `users__rel_roles`
  ADD CONSTRAINT `FK_USERS_REL_ROLES_ROLE_ID` FOREIGN KEY (`role_id`) REFERENCES `users__roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `USERS_REL_ROLES_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `users__tokens`
--
ALTER TABLE `users__tokens`
  ADD CONSTRAINT `FK_USERS_TOKENS_USER_ID` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
