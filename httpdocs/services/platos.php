<?php

	require '../mod/base.php';

	# variables de respuesta
	$arrOutData = [];

	# parámetros
	$strRequestAct 		= isset($_REQUEST['a']) && $_REQUEST['a'] ? $_REQUEST['a']: false; # $1
	$strRequestItem 	= isset($_REQUEST['u']) && $_REQUEST['u'] ? $_REQUEST['u']: false; # $2 string
	$idRequestItem 		= isset($_REQUEST['i']) && $_REQUEST['i'] ? $_REQUEST['i']: false; # $2 int

	if(isset($_SERVER['REQUEST_METHOD'])) {	
		if(!isset($clsPlatos)) $clsPlatos = new Platos($b->db);

		switch ($_SERVER['REQUEST_METHOD']) {
			case 'GET':

				# comerypunto.es/services/platos/nombre_de_accion
				switch ($strRequestAct) {

						# comerypunto.es/services/platos
						case false:
							header("HTTP/1.0 200 OK");
							$arrOutData['platos'] = $clsPlatos->getPlatos ();
							break;

						# comerypunto.es/services/platos/categorias
						# comerypunto.es/services/plato?a=categorias
						case 'categorias':
							header("HTTP/1.0 200 OK");
							$arrOutData['categorias'] = $clsPlatos->getCategorias ();
							break;

						# comerypunto.es/services/platos/menu
						# comerypunto.es/services/platos?a=menu
						case 'menu':
							header("HTTP/1.0 200 OK");
							$arrOutData['menu'] = $clsPlatos->getCategoriasIndexadas ();
							break;

						# comerypunto.es/services/platos/alergenos
						# comerypunto.es/services/platos?a=alergenos
						case 'alergenos':
							header("HTTP/1.0 200 OK");
							$arrOutData['alergenos'] = $clsPlatos->getAlergenos ();
							break;

						# comerypunto.es/services/platos/macarrones-con-chorizo
						# comerypunto.es/services/platos?a=macarrones-con-chorizo
						default:
							$result = $clsPlatos->getPlato ($strRequestAct);
							if($result) {
								header("HTTP/1.0 200 OK");
								$arrOutData['plato'] = $result;
							}
							else {
								header("HTTP/1.0 404 Not Found");
							}
							break;
					}
				break;

			default:
				header("HTTP/1.0 400 Bad Request");
				break;
		}
	}
	else {
		header("HTTP/1.0 400 Bad Request");
	}


	header('Content-Type: application/json');
	if(empty($arrOutData)) {
	}
	$strOut = json_encode($arrOutData, JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
	$strOut = preg_replace('/^(  +?)\\1(?=[^ ])/m', '$1', $strOut);
	echo $strOut;
