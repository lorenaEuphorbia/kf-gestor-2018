<?php
	require '../mod/base.php';

	# variables
	$arrOutData = [];

	# parámetros
	$strRequestMod = isset($_GET['m']) && $_GET['m'] ? $_GET['m']: false; # $1
	$strRequestAct = isset($_GET['a']) && $_GET['a'] ? $_GET['a']: false; # $2

	$idRequestItem = isset($_GET['i']) && $_GET['i'] ? $_GET['i']: false; # $3
	$strRequestItem = isset($_GET['u']) && $_GET['u'] ? $_GET['u']: false; # $3

	switch ($strRequestMod) {

		case 'secciones':
			# clases
			if(!isset($secciones)) $secciones = new Secciones($b->db);

			# muestra el texto según la seccion id
			if ($idRequestItem)
				$arrOutData['secciones'] = $secciones-> getTextosPorSeccion ($strRequestAct,$idRequestItem) ;

			# muestra el texto según la seccion slug
			else if ($strRequestItem)
				$arrOutData['secciones'] = $secciones-> getTextosPorSeccion ($strRequestAct,$strRequestItem);

			# muestra las secciones
			else
				$arrOutData['secciones'] = $secciones-> getSecciones ();

			break;

		# mostra todos los idiomas
		case 'idiomas':
			# clases
			if(!isset($secciones)) $secciones = new Secciones($b->db);

			$arrOutData['idiomas'] = $secciones-> getIdiomas ();
			break;

		case 'noticias':
			# clases
			if(!isset($noticias)) $noticias = new Noticias($b->db);

			# mostramos todas las noticias
			if($strRequestAct) 
				switch ($strRequestAct) {
					case 'en':
					case 'es':
						# mostramos lista de histórico
						$arrOutData['noticias'] = $noticias-> getListado ($strRequestAct);
						break;

					case 'historico':
						# mostramos lista de histórico
						$arrOutData['historico'] = $noticias-> getHistorico ();
						break;

					case 'categorias':
						# mostramos lista de categorías
						if($strRequestItem )
							$arrOutData['categorias'] = $noticias-> getCategorias ($strRequestItem);
						break;

					case 'tags':
						# mostramos lista de tags
						if($strRequestItem )
							$arrOutData['tags'] = $noticias-> getTags ($strRequestItem);
						break;
					
					default:
						# buscamos la noticia
						if (($noticia=$noticias->get($strRequestAct)) || ($noticia=$noticias->getEn($strRequestAct))) {
							$noticia['tags'] = $noticias-> getPostTags ($noticia['id'],$noticia['idioma']);
							$noticia['foto'] = $noticias->files-> searchByOwner($noticia['id'], 'img')? $noticias->files-> getFilePath() : false;
							$noticia['pdf'] = $noticias->files-> searchByOwner($noticia['id'], 'pdf_'.$noticia['idioma'])? $noticias->files-> getFilePath() : false;

							# actualizamos visualización
							$noticias->setRecord(['leido'=>(((int)$noticia['leido'])+1)], ['id'=>$noticia['id']], 'Posts');

							# mostramos la noticia
							$arrOutData['noticia'] = $noticia;
						}
						break;
				}
			break;
	}

	header('Content-Type: application/json');
	$strOut = json_encode($arrOutData, JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
	$strOut = preg_replace('/^(  +?)\\1(?=[^ ])/m', '$1', $strOut);
	echo $strOut;