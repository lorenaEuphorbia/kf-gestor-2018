<?php

	require '../mod/base.php';

	# variables de respuesta
	$arrOutData = [];

	# parámetros
	$strRequestAct 		= isset($_REQUEST['a']) && $_REQUEST['a'] ? $_REQUEST['a']: false; # $1
	$strRequestItem 	= isset($_REQUEST['u']) && $_REQUEST['u'] ? $_REQUEST['u']: false; # $2 string
	$idRequestItem 		= isset($_REQUEST['i']) && $_REQUEST['i'] ? $_REQUEST['i']: false; # $2 int

	if(isset($_SERVER['REQUEST_METHOD'])) {	
		if(!isset($clsPlanes)) $clsPlanes = new Planes($b->db);

		switch ($_SERVER['REQUEST_METHOD']) {
			case 'GET':

				# comerypunto.es/services/planes/nombre_de_accion
				switch ($strRequestAct) {

						# comerypunto.es/services/planes
						case false:
							header("HTTP/1.0 200 OK");
							$arrOutData['planes'] = $clsPlanes->getPlanes ();
							break;

						# comerypunto.es/services/planes/semanas
						# comerypunto.es/services/planes?a=semanas
						case 'semanas':
							header("HTTP/1.0 200 OK");
							$arrOutData['semanas'] = $clsPlanes->getSemanas ();
							break;

						# comerypunto.es/services/planes/tipos-plato
						# comerypunto.es/services/planes?a=tipos-plato
						case 'tipos-plato':
							header("HTTP/1.0 200 OK");
							$arrOutData['platos'] = $clsPlanes->getPropuestasPlatosTipos ();
							break;

						default:
							header("HTTP/1.0 404 Not Found");
							break;
					}
				break;

			default:
				header("HTTP/1.0 400 Bad Request");
				break;
		}
	}
	else {
		header("HTTP/1.0 400 Bad Request");
	}


	header('Content-Type: application/json');
	if(empty($arrOutData)) {
	}
	$strOut = json_encode($arrOutData, JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
	$strOut = preg_replace('/^(  +?)\\1(?=[^ ])/m', '$1', $strOut);
	echo $strOut;
