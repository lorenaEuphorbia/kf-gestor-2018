<?php
	include "mod/base.php";
	if (false) break;

	# CASOS DE PRUEBA #

	# comprobar cuenta de correo y envio OK
	else if (isset($_REQUEST['correo'])) {
		if(!isset($correos)) $correos = new Correos($_objClsBase);
	    if($correos->send('TEST', 'Prueba de evio de emails', ['cesar@euphorbia.es','César']))
	    	echo 'email enviado';
	    else echo 'error';
	}

	# enviar resumen de la compra según el id de pedido
	else if (isset($_REQUEST['correo-pedido'])) {
		if(!isset($correos)) $correos = new Correos($_objClsBase);
	    if($correos->sendOrderByID($_REQUEST['correo-pedido']))
	    	echo 'email resumen de pedido enviado';
	    else echo 'error resumen de pedido';
	}

	else echo 'error en los parámetros enviados';