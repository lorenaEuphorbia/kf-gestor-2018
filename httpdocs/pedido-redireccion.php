<?php
	include "mod/base.php";

	if (!empty($_REQUEST['token'])) {
		$strJwt = $_REQUEST['token'];

		// cargamos contenido de SLIM
		loadSlimDependencies();
		$settings = getSlimSettings()['settings'];

		// configuramos
    	\Firebase\JWT\JWT::$leeway = 60 * $settings['token_time'];


		$objJwt = \Firebase\JWT\JWT::decode($strJwt, $settings['api_key_public'], array($settings['api_algorithms']));
		$objToken = $_objClsBase->users->getTokenValid ($objJwt->jti);

		if ($objToken) {
			$arrOrders = $_objClsBase->orders->getList ('Orders', [], ['id_token' => $objToken['id'], 'codigo_cliente' => $objToken['id_usuario'], 'historico' => 0], ['modification_date DESC']);
			$objOrder = $arrOrders ? $arrOrders[0] : false;

			if ($objOrder) {
				$_objClsBase->security->forceLogin(['id' => $objToken['id_usuario']]);
				$_objClsBase->carrito->switchOrder($objOrder);
				$_SESSION['is_jwt_access'] = true;

				switch ($_objClsBase->carrito->getMetodoDePago ()) {
					case 'tarjeta':
						header('Location: /banco-redireccion/');
						exit;

					case 'paypal':
						header('Location: /paypal-redireccion/');
						exit;
				}
			}
		}
		exit;
	}

	if (!$_objClsBase->security->isLogin) {
		$_SESSION['RESULT'] = [
			'mensaje' => 'Debes loguearte primero.',
			'resultado' => false,
			'reload' => true
		];
		header('Location: /es/login-registro');
		exit;
	}
	
	if ($_objClsBase->carrito->getPrecioTotal () > 0) {
		switch ($_objClsBase->carrito->getMetodoDePago ()) {
			case 'tarjeta':
				header('Location: /banco-redireccion/');
				exit;

			case 'paypal':
				header('Location: /paypal-redireccion/');
				exit;
		}
	}


	echo "No es posible realizar el pedido, pruebe pasados unos minutos o contacte con el administrador de la página";
	exit;