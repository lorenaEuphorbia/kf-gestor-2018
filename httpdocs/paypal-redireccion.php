<?php
	include "mod/base.php";
	$paypal = new Paypal ($b);

	$idPag = "";
	$tituloWeb = "Redirigiendo al banco";

	if (!$b->security->isLogin) {
		$_SESSION['RESULT'] = [
			'mensaje' => 'Debes loguearte primero.',
			'resultado' => false,
			'reload' => true
		];
		header('Location: /login');
		exit;
	}
	
	$idPedido = $b->carrito->sendRequest();
	$objPedido = $b->orders->getRecord ('Request', null, ['id'=>$idPedido]);
	$objDatosPedido = $b->orders->getRecord ('Orders', null, ['id'=>$objPedido['id_order']]);


	if (!$idPedido || !$objPedido) {
		$_SESSION['RESULT'] = [
			'mensaje' => 'No es posible conectar con el banco, intentelo pasados unos minutos y si el error persiste pongase en contacto con nosotros, por favor.',
			'resultado' => false,
			'reload' => true
		];
		header('Location: '.$b->getLastPage());
		exit;
	}

	?><!DOCTYPE html>
<html>
<head>
	<title>Redirigiendo a paypal</title>

  	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link rel="stylesheet" href="/css/font-awesome.min.css">
 	<link rel="stylesheet" href="/css/redirecciones.css">

</head>
<body>

	<header>
		<a href="/" class="logo"><img src="/imagenes/comunes/logo.png" alt="logotipo"></a>
	</header>

	<section>
		<div class="logo">
			<i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw" ></i>
		</div>
		<div class="texto">
			<h4 >Espere, por favor:</h4>
			<h4 >Conectando con paypal...</h4>
		</div>
	</section>

	<form name="reenvio" action="<?= $paypal->url_formulario; ?>" method="POST" >

		<!-- Datos del pedido. -->
		<input type="hidden" name="custom" value="<?= $objPedido['id'] ?>" /> <!-- variable oculta (tag) -->
		<input type="hidden" name="amount" value="<?= $objPedido['pvp_total'] ?>" />
		<input type="hidden" name="item_name" value="Pedido en <?= $paypal->nombre_titular ?>" /> <!-- Descipción del artículo -->
		<input type="hidden" name="item_number" value="<?= $objPedido['id_order'] ?>">
		<input type="hidden" name="quantity" value="1">
		<input type="hidden" name="business" value="<?= $paypal->business ?>" />
		<!--<input type="hidden" name="quantity" value="1" />-->
		<input type="hidden" name="invoice" value="<?= $objPedido['id_order'] ?>" /> <!-- nº de pedido -->
		<input type="hidden" name="currency_code" value="<?= $paypal->moneda ?>" /> <!-- moneda -->
		<input type="hidden" name="lc" value="ES" /> <!-- idioma -->
		<input type="hidden" name="no_note" value="1" /> <!-- desactivar introducir comentario de pedido -->
		<!-- Dirección de envío. -->
		<input type="hidden" name="address_override" value="1" /> <!-- Usa la dirección personalizada. -->
		<input type="hidden" name="no_shipping" value="1" /> <!-- No pregunta la dirección a los compradores. -->
		<input type="hidden" name="first_name" value="<?= $objDatosPedido['nombre_dir_facturacion'] ?>" />
		<input type="hidden" name="last_name"  value="" />
		<input type="hidden" name="address1" value="<?= $objDatosPedido['direccion_dir_facturacion'] ?>" />
		<input type="hidden" name="city" value="<?= $objDatosPedido['localidad_dir_facturacion'] ?>" />
		<input type="hidden" name="zip" value="<?= $objDatosPedido['cp_dir_facturacion'] ?>" />
		<input type="hidden" name="email" value="<?= $objDatosPedido['email'] ?>" />
		<input type="hidden" name="country" value="ES" />
		<!-- URLs de retorno -->
		<input type="hidden" name="notify_url" value="<?= $paypal->urlNotificacion ?>" />
		<input type="hidden" name="return" value="<?= $paypal->urlRespuestaOk; ?>" />
		<input type="hidden" name="cancel_return" value="<?= $paypal->urlRespuestaKo; ?>" />
		<input type="hidden" name="rm" value="2" /> <!-- Define los parámetros de retorno a $_POST -->
		<input type="hidden" name="cbt" value="Volver" /> <!-- Define el texto del botón de retorno -->
		<!-- Cutomizar ventana -->
		<!--<input type="hidden" name="image_url" value="<?= $paypal->url_comercio; ?>imagenes/comunes/logo.png" />
		<input type="hidden" name="cpp_header_image" value="<?= $paypal->url_comercio; ?>imagenes/comunes/logo.png" />
		<input type="hidden" name="cpp_headerback_color" value="95bd4c" />
		<input type="hidden" name="cpp_headerborder_color" value="95bd4c" />
		<input type="hidden" name="cpp_cart_border_color" value="95bd4c" />
		<input type="hidden" name="charset" value="utf-8" />-->

		<input type="hidden" name="cmd" value="_xclick" />
		<input type="hidden" name="upload" value="1" />

		<input type="submit" name="" value="Si su navegador no le redirecciona, haz click aquí.">
	</form>
	<script>
		window.setTimeout("EnviaForm()", 2000);
		function EnviaForm () { document.reenvio.submit(); }
	</script>
</body>
</html>