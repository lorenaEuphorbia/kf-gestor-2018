<?php
	include 'mod/base.php';
	$banco = new Pasarela($b);

	if (!empty($_POST)) {

		if(isset($_POST['Ds_Signature'])) {

			$version = $_POST['Ds_SignatureVersion'];
			$parametros = $_POST['Ds_MerchantParameters'];
			$signatureRecibida = $_POST['Ds_Signature'];

			$decodec = $banco->redsysAPI->decodeMerchantParameters($parametros);
			$firma = $banco->redsysAPI->createMerchantSignatureNotif($banco->claveSecreta, $parametros);

			$decodec = json_decode($decodec, true);
			Utilities::dumpToFile($decodec);

			//$idFactura = !empty($decodec['Ds_Order']) ? substr($decodec['Ds_Order'], 0, sizeof($decodec['Ds_Order'])-2) : '';

			if ($firma === $signatureRecibida) {
				$idPedido = !empty($decodec['Ds_Order'])? $decodec['Ds_Order']: '';
				$b->orders->setRequestResult($decodec, $idPedido);

				require 'pedido-notificacion.php';
			}
			else {
				trigger_error('Firma del banco no válida');
			}

		}
		else {
			trigger_error('No se encontró signature');
		}

	}
	else {
		trigger_error('No hay request');
	}

