<?php
	include 'mod/base.php';

	# Encode INPUT
	if (!empty($_REQUEST)) {
		$paypal = new Paypal($b);
		Utilities::dumpToFile($_REQUEST);

		foreach ($_REQUEST as $key => &$value) {
			$value = utf8_encode($value);
		}


		# REQUEST Fields
		$idPedido = isset($_REQUEST['custom'])? $_REQUEST['custom'] : false;
		$merchantId = isset($_REQUEST["receiver_id"]) && $_REQUEST["receiver_id"]? $_REQUEST["receiver_id"]: false; // Nº ID
		$idInvoice = isset($_REQUEST["invoice"]) && $_REQUEST["invoice"]? $_REQUEST["invoice"]: false; // Nº Pedido
		$itemNumber = isset($_REQUEST["item_number"]) && $_REQUEST["item_number"]? $_REQUEST["item_number"]: false; // Nº Pedido

		# DB Fields
		$arrRequestData = $_objClsBase->orders->getRecord('Request', null, ['id'=>$idPedido]);
		$arrOrderData = $_objClsBase->orders->getRecord('Orders', null, ['id'=>$idInvoice]);
		$idOrder = isset($arrRequestData["id_order"]) && $arrRequestData["id_order"]? $arrRequestData["id_order"]: false; // Nº Pedido
		# $isPagando = isset($arrRequestData["pagando"]) && $arrRequestData["pagando"]? true: false;
		# $isHistorico = isset($arrRequestData["historico"]) && $arrRequestData["historico"]? true: false;
		# $idRequest = isset($arrRequestData["request_id"]) && $arrRequestData["request_id"]? true: false;

		# VERIFY Fields
		if($idInvoice == $idOrder && $idOrder = $itemNumber) {
			$_objClsBase->orders->setRecord(['response'=>json_encode($_REQUEST),'modification'=>date('Y-m-d H:i:s')], ['id'=>$idPedido], 'Request');
			require "pedido-notificacion.php";
		}
		else {
			$_objClsBase->orders->setRecord(['response'=>json_encode($_REQUEST),'modification'=>date('Y-m-d H:i:s')], ['id'=>'2'], 'Request');
		}
	}
	$_objClsBase->orders->setRecord(['response'=>'null','modification'=>date('Y-m-d H:i:s')], ['id'=>'3'], 'Request');
