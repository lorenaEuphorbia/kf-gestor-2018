<?php

	include 'mod/base.php';

	if($_cliente) {

		if(!empty($_REQUEST)) {

			$paypal = new Paypal($b);
			$idPedido = !empty($_REQUEST['cm'])? $_REQUEST['cm']: '';
			$idFactura = !empty($_REQUEST['item_number'])? $_REQUEST['item_number']: '';
			$arrRequestData = $orders->get('Request', ['id'=>$idPedido]);
			$arrRequestDataPost = isset($arrRequestData['response'])? unserialize($arrRequestData['response']): false;
			

			// Check data recived
			if(isset($arrRequestDataPost['txn_id']) &&
				$arrRequestDataPost['txn_id'] == $_REQUEST['tx'] &&
				isset($arrRequestDataPost['custom']) &&
				$arrRequestDataPost['custom'] == $_REQUEST['cm'] &&
				isset($arrRequestDataPost['item_number']) &&
				$arrRequestDataPost['item_number'] == $_REQUEST['item_number'] &&
				isset($arrRequestDataPost['invoice']) &&
				$arrRequestDataPost['invoice'] == $_REQUEST['item_number'] &&
				isset($arrRequestDataPost['receiver_id']) &&
				$arrRequestDataPost['receiver_id'] == $paypal->merchant_id ) {

				switch ($_REQUEST['st']) {
					case 'Completed':
						$orders->setRecord(
							['bancoRespConfirm'=>serialize($_REQUEST)],
							['id'=>$idFactura],
							'Orders');
						break;

					default:
						$orders->setRecord(
							['bancoRespNot'=>serialize($_REQUEST)],
							['id'=>$idFactura],
							'Orders');
						break;
				}
			}
		}
	}

	include 'pedido-respuesta.php';
	exit;