<?php


    # http://anexsoft.com/p/106/creando-una-rest-api-para-php-con-el-framework-slim-3 INICIO
    # http://www.bradcypert.com/building-a-restful-api-in-php-using-slim-eloquent/ EJEMPLO CON ELOQUENT

    if (PHP_SAPI == 'cli-server') {
        // To help the built-in PHP dev server, check if the request was actually for
        // something which should probably be served as a static file
        $url  = parse_url($_SERVER['REQUEST_URI']);
        $file = __DIR__ . $url['path'];
        if (is_file($file)) {
            return false;
        }
    }


    # INCLDUES #
    require __DIR__ . '/../../vendor/autoload.php';
    require __DIR__ . '/../gestor/_inc/config.php';
    require __DIR__ . '/../gestor/_classes/class-utilities.php';


    # VARIABLES #

    $base = __DIR__ . '/../../src/';
    $settings = require $base . 'settings.php';

    $app = new \Slim\App($settings);
    $capsule = new \Illuminate\Database\Capsule\Manager;
    $container = $app->getContainer();  
    

    # AUTO CARGADOR DE SCRIPTS #

    foreach($container['settings']['includes'] as $f)
    {
        foreach (glob($base . $f . '/*.php') as $filename)
        {
            require $filename;
        }
    }


    # INICIO #

    // session_start();
    $capsule->addConnection($container['settings']['db']);
    $capsule->setAsGlobal();
    $capsule->bootEloquent();

    $app->run();