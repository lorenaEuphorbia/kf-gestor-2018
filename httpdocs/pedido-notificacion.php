<?php
	include_once 'mod/base.php';

	if (!empty($_REQUEST)) {
		$msgError = '';

		$idPedido = isset($_REQUEST['id']) && !isset($idPedido)? $_REQUEST['id'] : $idPedido;
		$arrRequestData = $_objClsBase->orders->getRecord('Request', null, ['id'=>$idPedido]);
		$idFactura = $arrRequestData? $arrRequestData['id_order']: false;
		$isPagado = '0';

		if($_objClsBase->orders->searchOrder(['id'=>$idFactura]))
		{
			$_objClsBase->orders->setCurrentOrder([
				'bancoRespPre' => Utilities::encode($_REQUEST)
			]);

			// RECOGEMOS RESULTADO
			$bancoResp =  json_decode($arrRequestData['response'], true);
			$codigoOperacion = '';
			// FORMAS DE PAGO
			switch ($arrRequestData['tipo']) {

				case 'paypal':
					if($bancoResp ){
						// ERRORES PAYPAL
						if (! isset($bancoResp['payment_status'])) {
							$msgError = 'La respuesta de la transacción remitida por el banco no es válida.<br />No se realizará ningún cargo en tu cuenta bancaria.<br />Ponte en contacto con nosotros si deseas obtener más información, por favor.';
						}
						else if ($bancoResp['payment_status'] != 'Completed') {
							$msgError = "La transacción no se ha completado satisfactoriamente.<br />No se realizará ningún cargo en tu cuenta bancaria. ERR$bancoRespPre";
						}
						else {
							$codigoOperacion = $bancoResp['txn_id'];
							$isPagado = '1';
						}
					}
					else {
						$msgError = 'Nuestro sistema no ha recibido ninguna respuesta sobre la transacción bancaria.<br />No se realizará ningún cargo en tu cuenta.<br />Ponte en contacto con nosotros si deseas obtener más información, por favor.';
					}
					break;

				case 'tarjeta':
					if($bancoResp ){
						// ERRORES SERMEPA
						if (! isset($bancoResp['Ds_Response'])) {
							$msgError = 'La respuesta de la transacción remitida por el banco no es válida.<br />No se realizará ningún cargo en tu cuenta bancaria.<br />Ponte en contacto con nosotros si deseas obtener más información, por favor.';
						}
						else if ($bancoResp['Ds_Response'] < 0 || $bancoResp['Ds_Response']> 99) {
							$msgError = "La transacción no se ha completado satisfactoriamente.<br />No se realizará ningún cargo en tu cuenta bancaria. ERR$bancoRespPre";
						}
						else {
							$codigoOperacion = $bancoResp['Ds_AuthorisationCode'];
							$isPagado = '1';
						}
					}
					else {
						$msgError = 'Nuestro sistema no ha recibido ninguna respuesta sobre la transacción bancaria.<br />No se realizará ningún cargo en tu cuenta.<br />Ponte en contacto con nosotros si deseas obtener más información, por favor.';
					}
					break;

				case 'transferencia':
				case 'contrareembolso':
					break;

				default:
					$msgError = 'Nuestro sistema ha detectado un error en la asignación de forma de pago de tu pedido.<br />Ponte en contacto con nosotros si deseas obtener más información, por favor.';
					break;
			}

			// GRABAMOS RESULTADO ERROR
			if($msgError) {
				$_objClsBase->orders->setCurrentOrder([
						'validacion' => $msgError,
						'pagando' => '0',
					]);
				header( "HTTP/1.1 401 Unauthorized" );
				trigger_error( "Error con el pedido, " . $msgError);
				exit;
			}
			// GRABAMOS RESULTADO OK
			else {
				$_objClsBase->orders->setCurrentOrder([
						'validacion' => 'El proceso se ha realizado correctamente',
						'pagando' => '0',
						'request_id' => $idPedido,
						'historico'=>'1',
					]);
				$_objClsBase->orders->setRecord(['historico'=>'1'], ['id'=>$idPedido], 'Request');

				# CORREOS
				if(!isset($correos)) $correos = new Correos($_objClsBase);
				switch ($_objClsBase->orders->getCurrentFieldValue('id_tipo', 'Orders')) {

					case Orders::TIPO_PEDIDO_REFERENCIA:
						break;

					case Orders::TIPO_REFERENCIA:
						# enviamos y guardamos el resultado
						$arrDestinatario = [
							$_objClsBase->orders->getCurrentFieldValue('email', 'Orders'),
							$_objClsBase->orders->getCurrentFieldValue('nombre', 'Orders') . ' ' . $_objClsBase->orders->getCurrentFieldValue('apellidos', 'Orders'),
						];
					    if($correos->sendEmail ('Tu suscripción está activa!', 'La respuesta del banco ha sido satisfactoria, tu suscripción se ha activado correctamente.<br />A partir de ahora recibirás los planes semanalmente.', null, null, $arrDestinatario)) {
							$_objClsBase->orders->setCurrentOrder(['correo_resumen'=>'1']);
					    }
						break;

					default:
						# enviamos y guardamos el resultado
					    if($correos->sendOrderByID($arrRequestData['id_order'])) {
							$_objClsBase->orders->setCurrentOrder(['correo_resumen'=>'1']);
					    }
						break;
				}


				header( "HTTP/1.1 200 OK" );
			}
		}
		else {
			header( "HTTP/1.1 401 Bad Request" );
			echo 'Datos erroneos';
		}
	}