<?php	
 
 	# tipo de plan semanal
	$id = isset($_POST["id"]) ? $_POST["id"] : (isset($_GET["id"]) ? $_GET["id"] : "");
	$id = preg_replace("/[^\d]/", "", $id);

	include $_SERVER['DOCUMENT_ROOT'].'/mod/base.php';

	if(!isset($clsPlanes)) $clsPlanes = new Planes ($b->db);

	$arrSemanas = $clsPlanes->getSemanas ();

	if($clsPlanes->search(['id'=>$id], 'Tipos')) {
		foreach ($arrSemanas as $idSemana => $semana) {
			if(! $semana['historico'] ) {
				$arrParams = ['id_tipo_plan'=>$id,'id_semana'=>$idSemana];
				if(! $clsPlanes->search($arrParams, 'Semanales' )) {
					 $clsPlanes->setRecord($arrParams,null, 'Semanales');
				}
			}
		}
		$arrData['id'] = $clsPlanes->getCurrentFieldValue('id_plan', 'Tipos');
	}
	else {
		echo 'No hay un Tipo de Plan con esos datos';
		exit;
	}
?>
	
	<form id="dataForm" action="<?= $_SERVER['HTTP_REFERER'] ?>" method="post">
	<?php if (isset($arrData)): ?>
		<?php foreach ($arrData as $name => $value) : ?>
			<input type="hidden" name="<?= htmlentities($name) ?>" value="<?= htmlentities($value) ?>">
		<?php endforeach ?>
	<?php endif ?>
	</form>
	<script type="text/javascript"> document.getElementById('dataForm').submit();</script>
