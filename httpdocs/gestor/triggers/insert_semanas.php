<?php	

	include $_SERVER['DOCUMENT_ROOT'].'/mod/base.php';

	define('NL', "<br/>\n");
	if(!isset($clsPlanes)) $clsPlanes = new Planes ($b->db);

	$year           = 2016;
	$maxYear        = 2018;
	$firstDayOfYear = mktime(0, 0, 0, 1, 1, $year);
	$nextMonday     = strtotime('monday', $firstDayOfYear);
	$nextSunday     = strtotime("+0 week 6 days 23 hours 59 minutes 59 seconds", $nextMonday);
	//$nextSunday     = strtotime('sunday', $nextMonday);

	while ($year < $maxYear) {
		$count 	= 1;
		while (date('Y', $nextMonday) == $year) {
			$mes 		= date('m', $nextMonday);
			$fecha_ini  = date('Y-m-d H:i:s', $nextMonday);
			$fecha_fin  = date('Y-m-d H:i:s', $nextSunday);
			$historico  = $fecha_fin < date('Y-m-d H:i:s');
			$arrParams = [
		   	 		'anno'		=> $year,
		   	 		'mes' 		=> $mes,
		    		'numero' 	=> $count,
		    		'fecha_ini' => $fecha_ini,
		    		'fecha_fin' => $fecha_fin
		    	];
			if(!$historico) {
				if($clsPlanes->getRecord ('Historico', [], $arrParams) == false) {
		   	 		$clsPlanes->setRecord( $arrParams, [], 'Historico');
				}
			    // echo date('Y-m-d H:i:s', $nextMonday), '-', date('Y-m-d H:i:s', $nextSunday)," $count", !$historico?' historico':' no_historico', NL;
		   	} else {
		   	 	$clsPlanes->setRecord(['historico'=>'1'] , $arrParams, 'Historico');
		   	}

		    $nextMonday = strtotime('+1 week', $nextMonday);
		    $nextSunday = strtotime('+1 week', $nextSunday);	    
			$count++;
		}
		$year++;
	}

