<?php

	ob_start();
	//error_reporting(0);
    $_arrResponse['messages'] = [];

	/// Cargamos otras clases necesarias (estáticas)
	require $_SERVER['DOCUMENT_ROOT'] . '/gestor/_inc/config.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/gestor/_inc/routes.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/gestor/_inc/functions.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-utilities.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-domElements.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-controller.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-adminPreferences.php';


	setAutoloader();

	/// Arrancamos el panel de control
	$i18n = new I18n("admin", $lang_admin);
	$b = new Base();

	/// Recogemos módulo y acción
	setPathVars ($mod, $act, $opt, $url, $id);
	$arrPermisions = getPermisions($arrMenu);

	/// Definimos las variables que se utilizarán
	$base_path = "/". ADMIN_FOLDER . "/";

	/// Sólo permitimos acceder a estos métodos si se está logueado como administrador
	if ($b->security->hasPermission($arrPermisions)) { 
		if($_SERVER['REQUEST_METHOD'] === 'POST') {
			$_POST = $_REQUEST; 

			$id = isset($_GET["id"]) ? $_GET["id"] : (isset($_POST["id"]) ? $_POST["id"] : "");
			$id = preg_replace("/[^a-zA-Z]+/", "", $id);

			switch ($id) {
				case 'deleteImage':
					$mod = isset($_POST["m"]) ? $_POST["m"] : "";
					$file_id = isset($_POST["i"]) ? $_POST["i"] : "";

					$className = ucfirst($mod);
					$class = new $className($b->db);

					$result = $class->files->deleteFile($file_id);				
					if ($result===true || $result>0) break;
					$_arrResponse['error']["A001"] = $result;
				break;
				case 'deleteMenuButton':
					$mod = isset($_POST["m"]) ? $_POST["m"] : "";
					$act = isset($_POST["a"]) ? $_POST["a"] : "";
					$button_id = isset($_POST["i"]) ? $_POST["i"] : "";
					
					//echo $act; return;
					$class = new $mod($b->db);

					$result = $class->deleteRecord($button_id,$act);				
					if ($result===true || $result>0) break;
					$_arrResponse['error']["B001"] = $result;
				break;
				case "editItem":
					//$tmp = preg_replace('/[^(\/\w-%?#.~:_=&)]/', "", $_POST['fields']);
					parse_str($_POST['fields'], $arrFields);

					//print_r($arrFields);

					foreach ($arrFields as $key => $value) {
						$_POST[$key] = $value;
					}

					$mod = !empty($arrFields["m"]) ? $arrFields["m"] : "";
					$act = !empty($arrFields["a"]) ? $arrFields["a"] : "";
					$id = !empty($arrFields["id"]) ? $arrFields["id"] : "";

					$modClass = new $mod($b->db);

					$opt = "EditorSave";

					require getFileByAct ($mod, $act, $arrPermisions);

					if (!empty($messages)) {
						//print_r($messages);

						if (count($messages) == 1 && $messages[0][0] == "ok") break;
						$_arrResponse['error']['C001'] = 'Parámetros de entrada incorrectos';
						$_arrResponse['messages'] = $messages;
					}
					else {
						$_arrResponse['error']["C002"] = 'Error interno';
					}
				break;
				case 'getSlug':
					$string = isset($_POST["s"]) ? $_POST["s"] : "";
					echo Utilities::getSlug($string);
				break;
				case 'sortList':
					$mod = isset($_POST["m"]) ? $_POST["m"] : "";
					$act = isset($_POST["a"]) ? $_POST["a"] : "";
					$list = isset($_POST["l"]) ? $_POST["l"] : "";

					$list = json_decode($list);

					/// El primer elemento es el contenedor
					array_shift($list);

					//echo json_encode($list); break;

					$numItems = count($list);
					$numUpdated = 0;

					if ($numItems) {
						$class = new $mod($b->db);

						for ($i=0; $i < $numItems; $i++) {
							//echo $list[$i]->parent_id . " | ";

							$tmpId = $list[$i]->id;
							$tmpPosition = -$i-1;

							$saved_ok = $class->setPosition($tmpId, $tmpPosition, $act);

							if ($saved_ok >= 0) $numUpdated ++;
						}

						if ($numItems == $numUpdated) break;						
						$_arrResponse['error']["D001"] = "Los datos eran iguales";
					}
					else {	
						$_arrResponse['error']["D002"] = "No hay datos para realizar esa acción";
					}
				break;
				case 'sortMenu':
					$mod = isset($_POST["m"]) ? $_POST["m"] : "";
					$act = isset($_POST["a"]) ? $_POST["a"] : "";
					$list = isset($_POST["l"]) ? $_POST["l"] : "";

					$list = json_decode($list);

					/// El primer elemento es el contenedor
					array_shift($list);

					$numItems = count($list);
					$numUpdated = 0;

					if ($numItems) {
						$class = new $mod($b->db);

						for ($i=0; $i < $numItems; $i++) {
							//echo $list[$i]->parent_id . " | ";

							/*
							$saved_ok = $class->setRecord(
								[
									"id"		=> $list[$i]->id,
									"owner_id" => !empty($list[$i]->parent_id) ? $list[$i]->parent_id : 0,
									"position"	=> -$i - 1
								],
								["id" => $list[$i]->id]
							);*/


							$tmpId = $list[$i]->id;
							$tmpParent = !empty($list[$i]->parent_id) ? $list[$i]->parent_id : 0;
							$tmpPosition = -$i - 1;

							$saved_ok = $class->setOrder( $tmpId, $tmpParent, $tmpPosition, $act );

							if ($saved_ok >= 0) $numUpdated ++;
						}

						if ($numItems == $numUpdated) break;
						$_arrResponse['error']["F001"] = "Los datos eran iguales";
					}
					else {
						$_arrResponse['error']["F002"] = "No hay datos para realizar esa acción";
					}
				break;
				default:
					$_arrResponse['error']["0002"] = 'Parámetros de entrada incorrectos';
				break;
			}
		}
		else {
			$_arrResponse['error']["90001"] = 'Parámetros de entrada incorrectos';
		}
	}
	else {
		$_arrResponse['error']["90002"] = 'Debes tener acceso';
	}

	$_arrResponse['data'] = ob_get_clean();
	if(empty($_arrResponse['error'])){
		//if(empty($_arrResponse['data'])){ echo "ok"; exit; }
		
		unset($_arrResponse['error']);
   		$_arrResponse['status'] = 'success';
   		$_arrResponse['result'] = 'ok';		
	}
	else {
   		//$_arrResponse['status'] = 'error';	
   		$_arrResponse['result'] = 'error';		
	}

	header('Content-Type: application/json; charset=UTF-8');
    echo json_encode($_arrResponse);