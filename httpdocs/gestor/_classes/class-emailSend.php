<?php
	require "phpmailer/class.phpmailer.php";
	
	class EmailSend {
		private $preferences;

		function __construct($preferences) {
			$this->preferences = $preferences;
		}
		
		function SendMail($from, $to, $cc = [], $bcc = [], $subject, $body, $attachment = []) {
			global $b;
			
			$php_mailer = new PHPMailer();
			
			$php_mailer->ClearAllRecipients();
			$php_mailer->ClearAttachments();
			$php_mailer->ClearCustomHeaders();
			
			$php_mailer->From = $from[1];
			$php_mailer->FromName = $from[0];

			foreach ($to as $el) {
				$php_mailer->AddAddress($el[1], $el[0]);
			}
			
			if ($cc) {
				foreach ($cc as $el) {
					$php_mailer->AddCC($el[1], $el[0]);
				}
			}

			if ($bcc) {
				foreach ($bcc as $el) {
					$php_mailer->AddBCC($el[1], $el[0]);
				}
			}
			
			$php_mailer->IsHTML(true);
			$php_mailer->CharSet = 'UTF-8';
			$php_mailer->Subject = $subject;
			$php_mailer->Body = $body;
			
			if (!empty($attachment['path']) && !empty($attachment['filename'])) {
				$php_mailer->AddAttachment($attachment['path'], $attachment['filename']);
			}

			$php_mailer->Mailer		= "smtp";
			$php_mailer->Host		= $this->preferences->getValue('forms_smtp_server');
			$php_mailer->Port		= $this->preferences->getValue('forms_smtp_port');
			$php_mailer->SMTPSecure	= $this->preferences->getValue('forms_smtp_security');
			$php_mailer->Timeout	= 30;
			$php_mailer->SMTPAuth	= $this->preferences->getValue('forms_smtp_auth') == 1;
			$php_mailer->Username	= $this->preferences->getValue('forms_smtp_user');
			$php_mailer->Password	= $this->preferences->getValue('forms_smtp_password');
			
			$resp = $php_mailer->Send();
			
			return $resp;
		}
	}