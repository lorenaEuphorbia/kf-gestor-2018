<?php

class FilesMS {

	private $filesTable, $parentTable;
	private $filesDirName;
	private $moduleName;
	private $result, $currentId, $currentCode, $currentOrden;
	const REAL_PATH = "C:/FotosWeb";
	const PATH = "/imagen";

	function __construct($db, $moduleName, $tableName = "", $parentTable) {

		$this->filesTable = new TableMS($db, $tableName );
		$this->parentTable = $parentTable;
		$this->moduleName = $moduleName;
		$this->filesDirName = "files";
		$this->result = false;		
		$this->currentId = 0;		
		$this->currentCode = "";		
		$this->currentOrden = 0;	
	}

	public function __destruct() {
		return true;
	}


	/// MÉTODOS PÚBLICOS ///

	public function deleteFile($fotoCode) {

		global $act;
		$act = isset($act)? $act: isset($_POST["t"]) && !empty($_POST["t"])? $_POST["t"] : "Products";
		//var_dump($act); exit;

		switch ($act) {
			case 'Brands':

				# reseteamos campo en la tabla
				$result = $this->parentTable->getList($act, ["Imagen","Row_Id"],["GRP_ID"=>GRP_ID,"Codigo"=>$fotoCode]);
				if($result) {
					$path = self::REAL_PATH .  $result[0]["Imagen"];
					is_file($path) && unlink($path);
					return $this->parentTable->setRecord(["Imagen"=>""],["GRP_ID"=>GRP_ID,"Codigo"=>$fotoCode], $act);
				}
				break;

			
			default:				
				$result = $this->filesTable->selectRecords(
							["FotoMG","FotoG","FotoB","FotoP","FotoR"],
							["GRP_ID"=>GRP_ID,"CodigoFoto"=>$fotoCode],
							[],
							0, 1);
				if ($result) {
					foreach ($result[0] as $path) {
						$file = self::REAL_PATH . Utilities::replaceSlashes($path);
						if (is_file($file)) unlink($file);

						echo $file . "  |  ";
					}
					return $this->filesTable->deleteRecord(["GRP_ID"=>GRP_ID,"CodigoFoto" => $fotoCode]);
				}
				break;
		}		
		return false;
	}

	public function getFileData($fieldName) {

		global $act;
		$keyCode = $act=="Brands"? "Codigo": "CodigoFoto";

		if($this->result) {
			//var_dump($this->currentOrden); exit;
			switch ($fieldName) {

				case 'id': return isset($this->result[$this->currentOrden])? $this->result[$this->currentOrden][$keyCode]: "";
				case 'type': return $act;

				default: echo "class-filesMS<br/>"; var_dump($fieldName); exit;
			}

		}
		return $this->filesTable->getFieldCurrentValue($fieldName);
		
	}

	public function getExtension($mimeType) {
		switch($mimeType) {
			case "image/pjpeg":
			case "image/jpeg":
				return '.jpg';
			break;
			case "image/gif":
				return '.gif';
			break;
			case "image/png":
				return '.png';
			break;
			default:
				return '.b';
			break;
		}
	}

	public function getModuleName() {
		return $this->moduleName;
	}

	public function getFilePath() {

		global $act;
		$keyCode = $act=="Brands"? "Imagen": "FotoP";

		$outStr = $this->result && isset($this->result[$this->currentOrden])? self::PATH . $this->result[$this->currentOrden][$keyCode] : "";
		return Utilities::replaceSlashes($outStr);
	}

	public function getFileType() {
		return "img";
	}

	public function getList($arrFields = null, $arrFilter = null, $arrOrder = null, $startAt = null, $numRecords = null, $fetchStyle = "FETCH_ASSOC") {
		return $this->filesTable->selectRecords($arrFields, $arrFilter, $arrOrder, $startAt, $numRecords, $fetchStyle);
	}

	public function searchById($fotoCode) {

		global $act;
		$keyCode = $act=="Brands"? "Codigo": "CodigoFoto";

		return $this->filesTable->selectRecords(
					["Row_Id"],
					[$keyCode=>$fotoCode],
					[],
					0, 1);
	}

	public function getOwnerCodigo($id="") {

		global $act;
		if (!empty($id) ) {

			if( $this->currentId==$id ) return $this->currentCode;

			$result = $this->parentTable->getList($act,["Codigo"],["Row_Id"=>$id]);
			if($result) {
				$this->currentId = $id;
				$this->currentCode = $result[0]["Codigo"];
				return $this->currentCode;
			} 
		}

		$this->currentId = "";
		$this->currentCode = "";
		return false;
	}

	public function searchByOwner($id, $imgOrden) {

		global $act;

		# busqueda
		if($this->getOwnerCodigo($id)) {

			if( isset($this->result["Codigo"]) && $this->result["Codigo"]==$this->currentCode ) {
				return true;
			}

			switch ($act) {
				case 'Brands':
					$this->result = $this->parentTable->getList(
							$act,
							["Row_Id","Codigo","Imagen"],
							["Row_Id"=>$id],
							[]);

					return $this->result!==false;

				default:			
					# num de imagen
					$this->currentOrden = str_replace("image_", "", $imgOrden);

					$result = $this->filesTable->selectRecords(
							["Row_Id","Codigo","CodigoFoto","OrdenWeb","FotoMG","FotoG","FotoB","FotoR","FotoP"],
							["Codigo"=>$this->currentCode],
							["OrdenWeb asc"]);
					$this->result = array_fill(1, 4, false);
					foreach ($result as $row) {
						$this->result[$row["OrdenWeb"]] = $row;
					}
					//var_dump($this->result); exit();
					return $this->result[$this->currentOrden]!==false;
			}
		}

	}

	public function setFile($arrFields) {
		global $act;
		switch ($act) {
			case 'Brands':

				$fotoCode = isset($_POST["codigo"]) && !empty($_POST["codigo"])? $_POST["codigo"]: Utilities::getRandomString(10);

				$strExtension	= pathinfo($_FILES[$arrFields["type"]]['name'], PATHINFO_EXTENSION);
				$strFilename	= $fotoCode . "." . $strExtension;		
				$strRelativePath = "/MarcasBanner/" . $strFilename;

				$save = $this->parentTable->setRecord(["Imagen"=>$strRelativePath], ["Row_Id"=>$arrFields["owner_id"]], 'Brands');

				if (!empty($save)) {

					$strSearch = self::REAL_PATH . "/MarcasBanner/" . $fotoCode . ".*";
					$result = glob($strSearch);
					if (!empty($result)) unlink($result[0]);

					if (!empty($save)) {
						copy($_FILES[$arrFields["type"]]['tmp_name'], self::REAL_PATH . $strRelativePath);
					}

					return true;
				}
				break;
			
			default:
				# num de imagen
				$this->currentOrden = str_replace("image_", "", $arrFields["type"]);
				$isInsert = $this->searchByOwner($arrFields["owner_id"], $this->currentOrden)===false;

				$fotoCode = isset($_POST[$arrFields["type"]."-alt"]) && !empty($_POST[$arrFields["type"]."-alt"])? $_POST[$arrFields["type"]."-alt"]: false;					

				# recogemos el siguiente codigo de la foto
				if(!$fotoCode) {
					if($isInsert) {
						# comprobamos si ya está añadido
						$result = $this->filesTable->selectRecords(["CodigoFoto"],[],["CodigoFoto DESC"],0,1);
						if($result)
							$fotoCode = $result[0]["CodigoFoto"]+1;			
						else
							return false;	
					} 
					else {				
						$fotoCode = $this->result[$this->currentOrden]['CodigoFoto'];
					}
				}

				$strFilename 		= $this->currentCode."_".$fotoCode;
				$strExtension		= pathinfo($_FILES[$arrFields["type"]]['name'], PATHINFO_EXTENSION);
				$strRelativeDir 	= "/" . substr($this->currentCode, 0, 3) . "/" . $this->currentCode . "/"; # directorio			
				$strRelativePath 	= $strRelativeDir .  $strFilename . "." . $strExtension; # nombre

				$arr_fields = [
					"FotoMG"	 => $strRelativePath,
					"FotoG"	 	 => $strRelativePath,
					"FotoB"	 	 => $strRelativePath,
					"FotoR"	 	 => $strRelativePath,
					"FotoP"	 	 => $strRelativePath,
				];
				
				if ($isInsert) {

					# definimos parámetros por defecto
					$arr_fields["GRP_ID"] 		= GRP_ID;
					$arr_fields["CodigoFoto"] 	= $fotoCode;
					$arr_fields["Publicar"] 	= "1";
					$arr_fields["Codigo"] 		= $this->currentCode;
					$arr_fields["OrdenWeb"] 	= $this->currentOrden;

					$save = $this->filesTable->insertRecord($arr_fields);
					//$arr_fields["CodigoFoto"] = $this->filesTable->getLastInsertId();

				}
				else {				
					//var_dump($arr_fields); exit;
					$save = $this->updateFileData($fotoCode, $arr_fields);
				}

					//var_dump($_FILES); exit;
				if (!empty($save)) {
					$base_path = self::REAL_PATH . $strRelativeDir;

					if (!file_exists($base_path)) {
						mkdir($base_path);
					}
					else if (!isset($arr_fields["CodigoFoto"])) {
						$strSearch = $base_path . $strFilename . ".*";
						$result = glob($strSearch);

						if (!empty($result)) unlink($result[0]);
					}

					//var_dump($save); exit;
					copy($_FILES[$arrFields["type"]]['tmp_name'], $base_path . $strFilename . "." . $strExtension);

					return true;
				}
				break;
		
		
		}
		return false;
	}

	public function updateFileData($codFoto, $arrFields) {
		if ($this->searchById($codFoto)) {
			return $this->filesTable->updateRecord($arrFields, ["CodigoFoto" => $codFoto]);
		}
		return false;
	}
}