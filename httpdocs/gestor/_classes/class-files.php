<?php

class Files extends Table {
	private $filesTable;
	private $filesDirName;
	private $moduleName;
	const MAX_FILE_SIZE = '5M';

	function __construct($db, $moduleName, $tableName = "") {
		ini_set('upload_max_filesize', self::MAX_FILE_SIZE);

		$this->moduleName = $moduleName;
		$this->filesDirName = "files";

		$this->db = $db;
		$this->fields = array();
		$this->tableName = $tableName == "" ? $moduleName . "__files" : $tableName;
		$this->tableKey = null;
		$this->init();

		$this->filesTable = $this;
	}

	public function __destruct() {
		return true;
	}


	/// MÉTODOS PÚBLICOS ///

	public function deleteFile($id) {
		if ($this->searchById($id)) {
			if ($this->filesTable->deleteRecord(["id" => $id])) {
				$path = $_SERVER['DOCUMENT_ROOT'] . $this->getFilePath();
				if (is_file($path)) unlink($path);

				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}

	public function getFileData($fieldName) {
		return $this->filesTable->getFieldCurrentValue($fieldName);
	}

	public function getExtension($mimeType) {
		return FileTypes::getExtension($mimeType);
		switch($mimeType) {
			case "image/pjpeg":
			case "image/jpeg":
				return '.jpg';
			break;
			case "image/gif":
				return '.gif';
			break;
			case "image/png":
				return '.png';
			break;
			case "text/plain":
				return '.txt';
			break;
			default:
				return '.b';
			break;
		}
	}

	public function getModuleName() {
		return $this->moduleName;
	}

	public function getFilePath() {
		$path = "/" . $this->filesDirName . "/" . $this->moduleName . "/" . $this->filesTable->getFieldCurrentValue(strtolower("owner_type")) . "/" . strtolower($this->getFileType()) . "/" ;
		$path .= $this->filesTable->getFieldCurrentValue("id");
		$mime = $this->filesTable->getFieldCurrentValue("mime_type");
		if($mime=='application/octet-stream') {
			$path .= '.'.pathinfo($this->filesTable->getFieldCurrentValue("name"), PATHINFO_EXTENSION);
		}
		else {
			$path .= $this->getExtension($mime);
		}
		return $path;
	}

	public function getFileType() {
		return $this->filesTable->getFieldCurrentValue("type");
	}

	public function getList($arrFields = null, $arrFilter = null, $arrOrder = null, $startAt = null, $numRecords = null, $fetchStyle = "FETCH_ASSOC") {
		return $this->filesTable->selectRecords($arrFields, $arrFilter, $arrOrder, $startAt, $numRecords, $fetchStyle);
	}

	public function searchById($id) {
		return $this->filesTable->searchRecord(["id" => $id]);
	}

	public function searchByOwner($ownerId, $type = "", $ownerType = "") {
		$arr_where = array ();
		$arr_where['owner_id'] = $ownerId;
		$arr_where['owner_type'] = strtolower(empty($ownerType)? $this->moduleName: $ownerType);
		$arr_where["type"] = strtolower($type);

		// var_dump($arr_where);
		// exit;

		return $this->filesTable->searchRecord($arr_where);
	}

	public function setFile($arrFields, $arrFiles) {
		if (is_uploaded_file($arrFiles[$arrFields["type"]]['tmp_name'])) {
			$image_size = getimagesize($arrFiles[$arrFields["type"]]['tmp_name']);
			$image_size = $image_size? $image_size: [0,0];
			
			$arr_fields = [
				"owner_id"	 => $arrFields["owner_id"],
				"type"		 => $arrFields["type"],
				"title_1"	 => $arrFiles[$arrFields["type"]]['name'],
				"name"		 => $arrFiles[$arrFields["type"]]['name'],
				"mime_type"	 => $arrFiles[$arrFields["type"]]['type'],
				"size"		 => $arrFiles[$arrFields["type"]]['size'],
				"height"	 => $image_size[1],
				"width"		 => $image_size[0],
				"creacion"	 => date("Y-m-d H:i:s"),
			];

			if (!empty($arrFields["owner_type"])) {
				$arr_fields["owner_type"] = $arrFields["owner_type"];
			}
			$arr_fields['owner_type'] = strtolower($arr_fields['owner_type']);

			if (isset($arrFields['id'])) {
				$arr_fields["id"] = $arrFields['id'];				
				$save = $this->updateFileData($arr_fields["id"], $arr_fields);
			}
			else {
				$save = $this->filesTable->insertRecord($arr_fields);

				$arr_fields["id"] = $this->filesTable->getLastInsertId();
			}

			if (!empty($save)) {
				$base_path = $_SERVER['DOCUMENT_ROOT'] . "/" . $this->filesDirName . "/" . $this->moduleName . "/" . strtolower($arr_fields["owner_type"]) . "/" . strtolower($arr_fields["type"]) . "/";

				if (!file_exists($base_path)) {
					mkdir($base_path,0777,true);
				}
				else if (!empty($arr_fields["id"])) {
					$result = glob($base_path . $arr_fields["id"] . ".*");

					if (!empty($result)) unlink($result[0]);
				}

				copy($arrFiles[$arrFields["type"]]['tmp_name'], $base_path . $arr_fields["id"] . $this->getExtension($arr_fields["mime_type"]));

				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}

	public function updateFileData($fileId, $arrFields) {
		if ($this->searchById($fileId)) {
			$arrFields["id"] = $fileId;
			return $this->filesTable->updateRecord($arrFields, ["id" => $fileId]);
		}
		else {
			return false;
		}
	}

	public static function codeToMessage($code) 
    { 
        switch ($code) { 
            case UPLOAD_ERR_INI_SIZE: 
                $message = 'El archivo supera el límite de '.Utilities::file_upload_max_size().'.';  
                break; 
            case UPLOAD_ERR_FORM_SIZE: 
                $message = 'El archivo supera el límite de '.Utilities::file_upload_max_size().' especificado.'; 
                break; 
            case UPLOAD_ERR_PARTIAL: 
                $message = 'El archivo no se ha subido del todo. Prueba de nuevo, por favor.'; 
                break; 
            case UPLOAD_ERR_NO_FILE: 
                $message = 'No se ha seleccionado ningún archivo.'; 
                break; 
            case UPLOAD_ERR_NO_TMP_DIR: 
                $message = 'Error, directorio de destino inaccesible.';
                break; 
            case UPLOAD_ERR_CANT_WRITE: 
                $message = 'Error al escribir el archivo en el disco. Compruebe permisos.'; 
                break; 
            case UPLOAD_ERR_EXTENSION: 
                $message = 'Error, una extension está bloqueando la subida de archivos.'; 
                break; 

            default: 
                $message = 'Error '.$code.', por favor contacte con el administrador.'; 
                break; 
        } 
        return $message; 
    } 
}

class FileTypes {

	# tipos SIMPLES
	const TIPO_TXT			= 0x1;
	const TIPO_IMAGE		= 0x2;
	const TIPO_MEDIA		= 0x4;
	const TIPO_ADOBE		= 0x8;
	const TIPO_MS			= 0x16;
	const TIPO_ARCHIVES		= 0x32;
	const TIPO_OPEN			= 0x64;

	# tipos COMPUESTOS
	const TIPO_TEXTO 	= 0x128;
	const TIPO_IMAGEN 	= 0x256;
	const TIPO_VIDEO 	= 0x512;
	const TIPO_ARCHIVO 	= 0x1024;

	public static $TXT = [
        'txt'	=> 'text/plain',
        'htm'	=> 'text/html',
        'html'	=> 'text/html',
        'php'	=> 'text/html',
        'css'	=> 'text/css',
        'js'	=> 'application/javascript',
        'json'	=> 'application/json',
        'xml'	=> 'application/xml',
	];

	public static $IMAGE = [	
        // images
        'png' 	=> 'image/png',
        'jpg' 	=> 'image/jpeg',
        'jpeg' 	=> 'image/jpeg',
        'jpg' 	=> 'image/jpeg',
        'gif' 	=> 'image/gif',
        'bmp' 	=> 'image/bmp',
        'ico' 	=> 'image/vnd.microsoft.icon',
        'tiff' 	=> 'image/tiff',
        'tif' 	=> 'image/tiff',
        'svg' 	=> 'image/svg+xml',
        'svgz' 	=> 'image/svg+xml',
	];

    public static $MEDIA = [
        // audio/video
        'mp3' 	=> 'audio/mpeg',
        'qt' 	=> 'video/quicktime',
        'mov' 	=> 'video/quicktime',
        'swf' 	=> 'application/x-shockwave-flash',
        'flv' 	=> 'video/x-flv',
    ];

    public static $ADOBE = [        
        // adobe
        'pdf' 	=> 'application/pdf',
        'psd' 	=> 'image/vnd.adobe.photoshop',
        'ai' 	=> 'application/postscript',
        'eps' 	=> 'application/postscript',
        'ps' 	=> 'application/postscript',
    ];

    public static $MS = [       
		// ms office
		// http://stackoverflow.com/a/4212908
		'doc' 	=> 'application/msword',
		'dot' 	=> 'application/msword',
		'docx' 	=> 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		'dotx' 	=> 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
		'docm' 	=> 'application/vnd.ms-word.template.macroEnabled.12',
		'dotm' 	=> 'application/vnd.ms-word.template.macroEnabled.12',
		'rtf' 	=> 'application/rtf',
		'xls' 	=> 'application/vnd.ms-excel',
		'xlsx' 	=> 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
		'xltx' 	=> 'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
		'xlsm' 	=> 'application/vnd.ms-excel.sheet.macroEnabled.12',
		'xltm' 	=> 'application/vnd.ms-excel.template.macroEnabled.12',
		'xlam' 	=> 'application/vnd.ms-excel.addin.macroEnabled.12',
		'xlsb' 	=> 'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
		'ppt'	=> 'application/vnd.ms-powerpoint',
		'pot'	=> 'application/vnd.ms-powerpoint',
		'pps'	=> 'application/vnd.ms-powerpoint',
		'ppa'	=> 'application/vnd.ms-powerpoint',
		'pptx'	=> 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
		'potx'	=> 'application/vnd.openxmlformats-officedocument.presentationml.template',
		'ppsx'	=> 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
		'ppam'	=> 'application/vnd.ms-powerpoint.addin.macroEnabled.12',
		'pptm'	=> 'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
		'potm'	=> 'application/vnd.ms-powerpoint.template.macroEnabled.12',
		'ppsm'	=> 'application/vnd.ms-powerpoint.slideshow.macroEnabled.12',
		'mdb'	=> 'application/vnd.ms-access',
    ];

    public static $ARCHIVES = [
        // archives
        'zip' 	=> 'application/zip',
        'rar' 	=> 'application/x-rar-compressed',
        'exe' 	=> 'application/x-msdownload',
        'msi' 	=> 'application/x-msdownload',
        'cab' 	=> 'application/vnd.ms-cab-compressed',
    ];	

    public static $OPEN = [
        // open office
        'odt' 	=> 'application/vnd.oasis.opendocument.text',
        'odt' 	=> 'application/vnd.oasis.opendocument.text',
        'ods' 	=> 'application/vnd.oasis.opendocument.spreadsheet',
    ];

	public static function getMimeType($filename) {
	    $mimetype = false;

	        // open with mime_content_type
	    if (function_exists('mime_content_type')) {
	       $mimetype = mime_content_type($filename);

	        // open with FileInfo
	    } else if (function_exists('finfo_fopen')) {
    		$finfo = finfo_open(FILEINFO_MIME_TYPE); 
    		$mimetype = finfo_file($finfo, $filename);
			finfo_close($finfo);

	        // open with GD
	    } elseif (function_exists('getimagesize')) {
	        $finfo = getimagesize($filename);
	       	$mimetype =  $finfo['mime'];

	       // open with EXIF
	    } elseif (function_exists('exif_imagetype')) {
	       $mimetype = image_type_to_mime_type(exif_imagetype($filename));
	   	}

	    return $mimetype;
	}

	public static function isTipo ($filename = '', $tipos) {
		$arrTipos = self::getList ($tipos);
		$strMimeType = self::getMimeType($filename);
		if (array_search($strMimeType, $arrTipos))
			return true;
		return false;
	}

	public static function getList ($tipos) {
		$arrDatos = [];

		if ($tipos & self::TIPO_TXT) 
			$arrDatos = $arrDatos + self::$TXT;
		if ($tipos & self::TIPO_IMAGE) 
			$arrDatos = $arrDatos + self::$IMAGE;
		if ($tipos & self::TIPO_MEDIA) 
			$arrDatos = $arrDatos + self::$MEDIA;
		if ($tipos & self::TIPO_ADOBE) 
			$arrDatos = $arrDatos + self::$ADOBE;
		if ($tipos & self::TIPO_MS) 
			$arrDatos = $arrDatos + self::$MS;
		if ($tipos & self::TIPO_ARCHIVES) 
			$arrDatos = $arrDatos + self::$ARCHIVES;
		if ($tipos & self::TIPO_OPEN) 
			$arrDatos = $arrDatos + self::$OPEN;
		if ($tipos & self::TIPO_TEXTO)
			$arrDatos = $arrDatos + self::$ADOBE + self::$MS + self::$OPEN;
		if ($tipos & self::TIPO_IMAGEN)
			$arrDatos = $arrDatos + self::$IMAGE;
		if ($tipos & self::TIPO_VIDEO)
			$arrDatos = $arrDatos + self::$MEDIA;

		return $arrDatos;
	}
	
	public static function lista() {
		return array_merge(self::$TXT,self::$IMAGE,self::$MEDIA,self::$ADOBE,self::$MS,self::$ARCHIVES,self::$OPEN);
	}

	public static function getListToString ($tipos) {
		$arrTipos = self::getList ($tipos);
		if (!empty($arrTipos)) {
			return '.' . implode(", .", array_keys($arrTipos));
		}
		return 'ninguno';
	}

	public static function search($strMimeType) {
		if(array_search($strMimeType, $TXT)) return "TXT";
		if(array_search($strMimeType, $IMAGE)) return "IMAGE";
		if(array_search($strMimeType, $MEDIA)) return "MEDIA";
		if(array_search($strMimeType, $ADOBE)) return "ADOBE";
		if(array_search($strMimeType, $MS)) return "MS";
		if(array_search($strMimeType, $ARCHIVES)) return "ARCHIVES";
		if(array_search($strMimeType, $OPEN)) return "OPEN";
		return "";
	}

	public static function mimes($extensions) {
		return array_intersect_key(self::lista(),  $extensions);
	}

	public static function getExtension($strMimeType) {
		if($strMimeType=='application/octet-stream') {
			return '.'.pathinfo($this->filesTable->getFieldCurrentValue("name"), PATHINFO_EXTENSION);
		}
		else {
			return "." . array_search($strMimeType, self::lista());
		}
	}
}
