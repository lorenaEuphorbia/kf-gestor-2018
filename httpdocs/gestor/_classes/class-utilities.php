<?php

class Utilities {
	

	/// MÉTODOS PÚBLICOS ///

	public static function getFormData($formName,$arrData,$strKey,$strUrl="") {
		$outString = "<form id='$formName' method='post' action='$strUrl'>\n";
		$outString .= self::getFormElementData("save-filters-form",$arrData,$strKey);
		//$outString .= '<input type="hidden" name="'.$strKey.'-data" value=\''.serialize($arrData).'\' >'."\n";
		$outString .= "</form>";
		return $outString;
	}

	protected static function getFormElementData($formName,$data,$strNode="") {
		$outString = "";
		foreach ($data as $key => $value) {
			if(is_array($value)) {
				$outString .= self::getFormElementData($formName,$value,$strNode.'['.$key.']');
			} else {
				$outString .= '<input type="hidden" name="'.$strNode.'[]" value="'.$value.'" >'."\n";							
			}
			
		}
		
		return $outString;
	}


	public static function checkEmail($string) {
		return preg_match('/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i', $string);
	}

	public static function checkPassword($password) {		
		// La contraseña debe tener al menos 6 caracteres y contener como mínimo una letra y un número.
		return preg_match('/(?=.*?[a-zA-Z])(?=.*?[0-9]).{6,}/', $password);
	}

	public static function convertToPascalCase($string) {
		$string = preg_replace('/[^\w]+/u', ' ', $string);
		$string = trim($string);
		$string = mb_convert_case($string, MB_CASE_TITLE, "UTF-8");
		$string = preg_replace('/[^\w-]+/u', '', $string);

		return $string;
	}

	public static function getRegex($pattern){
	    if (preg_match('/[^-:\/_{}()a-zA-Z\d]/', $pattern))
	        return false; // Invalid pattern

	    // Turn "(/)" into "/?"
	    $pattern = preg_replace('#\(/\)#', '/?', $pattern);

	    // Create capture group for ":parameter"
	    $allowedParamChars = '[a-zA-Z0-9\_\-]+';
	    $pattern = preg_replace(
	        '/:(' . $allowedParamChars . ')/',   # Replace ":parameter"
	        '(?<$1>' . $allowedParamChars . ')', # with "(?<parameter>[a-zA-Z0-9\_\-]+)"
	        $pattern
	    );

	    // Create capture group for '{parameter}'
	    $pattern = preg_replace(
	        '/{('. $allowedParamChars .')}/',    # Replace "{parameter}"
	        '(?<$1>' . $allowedParamChars . ')?', # with "(?<parameter>[a-zA-Z0-9\_\-]+)"
	        $pattern
	    );

	    // Add start and end matching
	    $patternAsRegex = "@^" . $pattern . "$@D";

	    return $patternAsRegex;
	}

    public static function dump ($var) {
        echo "<pre>";

        error_reporting(E_ALL & ~(E_STRICT|E_NOTICE));
        ini_set("display_errors", 1);

        ini_set('xdebug.var_display_max_depth', 25);
        ini_set('xdebug.var_display_max_children', 256);
        ini_set('xdebug.var_display_max_data', 1024);

        var_dump($var); exit;
    }

	public static function getUniqueCode() {
		$code = array (1,1,3,2,4,5);
		$serie = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$serie_length = strlen($serie);
		srand((double)microtime()*1000000);
		$resp = '';
		$date = Date('Y m d H i s');
		$date = explode(" ", $date);
		
		for ($x=0; $x < count($code); $x++) {
			for ($y=0; $y < $code[$x]; $y++)
				$resp .= substr($serie, rand(0, $serie_length - 1), 1);
			$resp .= $date[$x];
		}

		return $resp;
	}

	public static function getRandomString($length = 20) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		
		$randomString = '';
		
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		
		return $randomString;
	}
	
	public static function getSlug($string,$glue='-') {
		/// Quitamos los espacios por guiones 
		$string = str_replace(' ',$glue,$string);

		/// Se eliminan lo símbolos extraños
		$string = self::quitarAcentos($string);

		/// Se quitan los guiones inicial y final
		$string = trim($string, '-');

		/// Transliteración: convierte caracteres con acentos y similares
		$string = iconv('utf-8', 'us-ascii//TRANSLIT', $string);

		/// Última limpieza de caracteres extraños que quedan tras la transliteración: ', ", etc
		$string = preg_replace('/[^A-Za-z0-9\-\/]/', '', $string);

		/// Se convierte el string a minúsculas
		$string = strtolower($string);

		return $string;
	}

    public static function isMobile() {
        return strstr($_SERVER['HTTP_USER_AGENT'],'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'],'iPod') || strstr($_SERVER['HTTP_USER_AGENT'],'iPad');
    }

	public static function quitarAcentos($string) {
		$a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
		$b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
  		return str_replace($a, $b, $string); 
	}


	public static function LimitaCaracteresPalabras ($cual, $caracteres) {
	//$cual = $this->LimpiaHtml($cual, false);
	// limitar el número de caracteres y devolver solo palabras enteras
		$orig_str_length = strlen($cual);
		$words = explode(' ', $cual);
			
		if($orig_str_length < $caracteres) return $cual;	
		$i=0;
		$str_length = 0;
		while($str_length < $caracteres)
		{
			$str_length = $str_length + strlen($words[$i]) + 1;
			$new_words[$i] = $words[$i];
			$i++;
		}

		$cual = implode(' ', $new_words);

		if(strlen($cual) > $caracteres) {
			$new_words = explode(' ', $cual);
			$word_count = count($new_words);
			$word_limit = $word_count - 1;
			$cual = implode(' ', array_slice($new_words, 0, $word_limit));
		}

		if($orig_str_length > strlen($cual)) $cual = "$cual...";
			
		return $cual;
	}



	public static function getURL( $arrString=[] , $isExterna=false ) {
		$strURL = implode("/", $arrString);

		if(!$isExterna)
			$strURL = "/$strURL/";
		else
			if (!$this->checkURL($strURL)) 
				$strURL = "http://" . $strURL;
		
		return $strURL;
   
	}

	public static function checkURL( $strUrl ) {
		return preg_match("~^(?:f|ht)tps?://~i", $strURL);   
	}


	public static function replaceSlashes( $inString ) {
		return str_replace('\\', '/', $inString);
	}

	public static function encode($a=false) {
		if (is_null($a)) return 'null';
		if ($a === false) return 'false';
		if ($a === true) return 'true';

		if (is_scalar($a)) {
			if (is_float($a)) {
				// Always use "." for floats.
				return floatval(str_replace(",", ".", strval($a)));
			}
			if (is_string($a)) {
				static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
				return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
			}
			else
				return $a;
		}

		$isList = true;
		for ($i = 0, reset($a); $i < count($a); $i++, next($a)) {
			if (key($a) !== $i) {
				$isList = false;
				break;
			}
		}
		$result = array();
		if ($isList) {
			foreach ($a as $v) $result[] = self::encode($v);
				return '[' . join(',', $result) . ']';
		}
		else
		{
			foreach ($a as $k => $v) $result[] = self::encode($k).':'.self::encode($v);
				return '{' . join(',', $result) . '}';
		}
	}

	// Returns a file size limit formated based on the PHP upload_max_filesize
	// and post_max_size
	public static function file_upload_max_size() {
	  return self::formatSizeUnits(self::file_upload_max_size_bytes());
	}

	// Returns a file size limit in bytes based on the PHP upload_max_filesize
	// and post_max_size
	public static function file_upload_max_size_bytes() {
	  static $max_size = -1;

	  if ($max_size < 0) {
	    // Start with post_max_size.
	    $max_size = self::parse_size(ini_get('post_max_size'));

	    // If upload_max_size is less, then reduce. Except if upload_max_size is
	    // zero, which indicates no limit.
	    $upload_max = self::parse_size(ini_get('upload_max_filesize'));
	    if ($upload_max > 0 && $upload_max < $max_size) {
	      $max_size = $upload_max;
	    }
	  }
	  return $max_size;
	}

	public static function parse_size($size) {
	  $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
	  $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
	  if ($unit) {
	    // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
	    return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
	  }
	  else {
	    return round($size)  / 1024;
	  }
	}

	public static function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = round($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = round($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = round($bytes / 1024, 2) . ' kB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
}
}