<?php

class Errors extends Controller {


	protected $db, $outDebug, $errorsTable;
	protected $arrListIp = ['88.4.24.111','84.125.56.192','213.149.250.131','::1','http://localhost:8001/','http://localhost/'];


	public function __construct($db) {
		$this->db = $db;
		
		$this->outDebug = $this->isDebug();
	}

	public function __destruct() {
		return true;
	}

	public function isDebug() {		
		return in_array($_SERVER['REMOTE_ADDR'],$this->arrListIp);
	}

	public function init() {
		if($this->outDebug){
			error_reporting(E_ALL);
		} else {
			error_reporting(0);
		}


		// establecer el gestro de errores definido por el usuario
		$gestor_errores_antiguo = set_error_handler("errorHandler");
	}
}

// función de gestión de errores
function errorHandler($errno, $errstr, $errfile, $errline)
{
    if (!(error_reporting() & $errno)) {
        // Este código de error no está incluido en error_reporting
        return;
    }

    $msg = "$errStr in $errFile on line $errLine";
    if ($errNo == E_NOTICE || $errNo == E_WARNING) {
		throw new ErrorException( $errno, $errstr, $errfile, $errline );
	} else {
        echo $msg;
    }
    return true;
}