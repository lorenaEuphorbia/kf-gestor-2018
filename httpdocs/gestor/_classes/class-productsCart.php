<?php 

class ProductsCart extends ProductsScope
{
	const SESSION_ORDER_ID = 'order_id';
	const SESSION_ORDER_CODE = 'order_code';
	const SESSION_METODO_PAGO = "METODO_PAGO_CART";	
	const KEY_CART = "CARRITO";

	public $objOrder;

	protected $orders;
	private $indexMetodoPago, $numTotal;
	private $arrMetodosPago = [
		'0' => 'tarjeta',
		'1' => 'transferencia',
	];

	# PESO(gr) => PRECIO
	private $arrPrecioPortesPeso = [
		5000 	=> 0,
		//10000 	=> 7.30,
		//15000 	=> 7.99,
		//20000 	=> 9.29,
		//25000 	=> 10.29,
		//30000 	=> 10.87,
	];
	private $numPrecioPortesKilo = 0;

	/* INIT & DESTRUCT */

	public function __construct($orders) {
		$this->orders = $orders;
		$this->clsProducts = $orders->productos;

		$this->arrProducts = $this->retrieveData();
	}

	public function __destruct() {
		$this->saveData($this->arrProducts);
	}

	protected function retrieveData() {
		$arrProducts = array();
		$this->objOrder = $this->getCurrentOrder ();
		if($this->objOrder!==false) {
			foreach ($this->orders->getArticulos ($this->objOrder['id']) as $objLinea) {
				$arrProducts[$objLinea['articulo']] = $this->getProductData($objLinea['articulo']);
				$arrProducts[$objLinea['articulo']][self::KEY_CART][$objLinea['talla']] = $objLinea['unidades'];
			}
			$this->objOrder['pagando'] = '0';
		}
		$this->setPrecioTotal($this->calculaPrecioArticulos($arrProducts));
		$this->setPesoTotal($this->calcularPesoArticulos($arrProducts));
		$this->setPrecioEnvio($this->calculaPrecioGastos($this->getPesoArticulos()));

		return $arrProducts;
	}

	protected function saveData ($arrProduct) {
		if($this->objOrder !== false){
			$idOrder = $this->objOrder['id'];
			// actualizar 
			$arrLinea = [];
			foreach ($arrProduct as $objProduct) {
				foreach ($objProduct[self::KEY_CART] as $talla => $unidades) {
					$arrLinea[] =  [
						'id_order' => $idOrder,
						'articulo' => $objProduct['id'],
						'talla'	=> $talla,
						'precio' => $objProduct['precio'],
						'iva' => $objProduct['iva'],
						'precio_iva' => $objProduct['precio'] * $objProduct['iva'] / 100,
						'unidades' => $unidades,
					];
				}
			}
			$this->orders->updateOrderData ($this->objOrder);
			$this->orders->updateProductsOrder ($idOrder,$arrLinea);
			// echo '<pre>';
			// print_r($this->orders->updateOrderData ($this->objOrder));
			// print_r($this->orders->updateProductsOrder ($idOrder,$arrLinea));			
			// echo '</pre>';
		}
		return false;
	}

	public function saveOrder ($objOrder) {
		if ($objOrder!==false) {
			$_SESSION[self::SESSION_ORDER_ID] = $objOrder['id'];
			$_SESSION[self::SESSION_ORDER_CODE] = $objOrder['code'];
			return true;
		}
		return false;
	}

	public function switchOrder ($objOrder) {
		$this->saveData($this->arrProducts);
		$this->saveOrder ($objOrder);
		$this->arrProducts = $this->retrieveData();
	}

	public function retrieveOrder ($idOrder) {
		global $_objClsBase;

		$objOrder = $this->orders->getRecord ('Orders', null, ['id'=>$idOrder,'historico'=>'0','request_id'=>['IS','null']], true);
		//var_dump($objOrder);

		if ($objOrder) {
			# comprobar usuario correcto
			// if(empty($objOrder['codigo_cliente']) ||   ($objOrder['codigo_cliente'] == $_objClsBase->security->userId && $objOrder['email'] == $_objClsBase->security->username)) {
				return $objOrder;
			// };
		}
		return false;
	}

	/* FUNCTIONS */

	/**
	 * Añade unidades de producto al carrito
	 * @param String  $strProducto El código de producto
	 * @param integer $numUnidades El número de unidades a añadir o a restar
	 * @return boolean false si se borra el producto o true si las unidades se han añadido correctamente
	 */
	public function addUnidades ($strProducto,$numUnidades=1,$strTalla='') {
		$this->addProduct($strProducto);

		if(isset($this->arrProducts[$strProducto][self::KEY_CART])) {
			$arrCarrito = &$this->arrProducts[$strProducto][self::KEY_CART];
			$arrCarrito[$strTalla] = empty($arrCarrito[$strTalla])? $numUnidades: $arrCarrito[$strTalla]+$numUnidades;
			if($arrCarrito[$strTalla]>0)  {
				$this->setPrecioTotal($this->calculaPrecioArticulos($this->arrProducts));
				$this->setPesoTotal($this->calcularPesoArticulos($this->arrProducts));
				$this->setPrecioEnvio($this->calculaPrecioGastos($this->getPesoArticulos()));	
				return true;
			}

			unset($arrCarrito[$strTalla]);
		}
		$this->setPrecioTotal($this->calculaPrecioArticulos($this->arrProducts));
		$this->setPesoTotal($this->calcularPesoArticulos($this->arrProducts));
		$this->setPrecioEnvio($this->calculaPrecioGastos($this->getPesoArticulos()));		
		return false;
	}

	public function addProduct ($strProducto) {		
		$result = parent::addProduct($strProducto);

		if(!isset($this->arrProducts[$strProducto][self::KEY_CART])) {
			$this->arrProducts[$strProducto][self::KEY_CART] = [];
		}

		return $result;
	}

	public function calculaPrecioArticulos ($arrArticulos) {
		$numPrecioTotal = 0;
		foreach ($arrArticulos as $objArticulo) {
			foreach ($objArticulo[self::KEY_CART] as $talla => $unidades) {
				$numPrecioTotal += $objArticulo['precio']*$unidades;
			}
		}
		return $numPrecioTotal;
	}
	public function calcularPesoArticulos ($arrArticulos) {
		$numPesoTotal = 0;
		foreach ($arrArticulos as $objArticulo) {
			foreach ($objArticulo[self::KEY_CART] as $talla => $unidades) {
				$numPesoTotal += $objArticulo['peso']*$unidades;
			}
		}
		return $numPesoTotal;
	}
	public function calculaPrecioGastos ($numGramos) {
		$numPrecio = 0;
		$numLastGramos = 0;
		// var_dump($numGramos);
		foreach ($this->arrPrecioPortesPeso as $gramos => $precio) {
			// var_dump($gramos);
			if($numGramos<=$gramos) return $precio;
			$numLastGramos = $gramos;
			$numPrecio = $precio;
		}
		do {
			$numPrecio += $this->numPrecioPortesKilo;
			$numLastGramos += 1000;
		} while ($numGramos < $numLastGramos);
		//exit;
		return $numPrecio;
	}

	public function deleteProduct ($strProducto,$strTalla='') {

		if(isset($this->arrProducts[$strProducto][self::KEY_CART])) {
			$arrCarrito = &$this->arrProducts[$strProducto][self::KEY_CART];
			unset($arrCarrito[$strTalla]);
			$this->setPrecioTotal($this->calculaPrecioArticulos($this->arrProducts));
			$this->setPesoTotal($this->calcularPesoArticulos($this->arrProducts));
			$this->setPrecioEnvio($this->calculaPrecioGastos($this->getPesoArticulos()));			
			return true;

		}
		$this->setPrecioTotal($this->calculaPrecioArticulos($this->arrProducts));
		$this->setPesoTotal($this->calcularPesoArticulos($this->arrProducts));
		$this->setPrecioEnvio($this->calculaPrecioGastos($this->getPesoArticulos()));	
		return false;

	}

	public function isOrder () {
		return !empty($_SESSION[self::SESSION_ORDER_ID]) && !empty($_SESSION[self::SESSION_ORDER_CODE]);
	}

	public function getCurrentOrder () {
		$idOrder = $this->getOrderId();

		if ($idOrder!==false) {
			$objOrder = $this->retrieveOrder ($idOrder);

			if ($objOrder) {
				return $objOrder;
			}
		}

		$objOrder = $this->orders->createOrder ($this->orders->security->isLogin? $this->orders->security->data: null );
		if ($objOrder && $this->saveOrder ($objOrder)) {
			return $objOrder;
		}

		return false;
	}

	/*
	public function newCurrentOrder () {
		$objOrder = $this->orders->createOrder ($this->orders->security->isLogin? $this->orders->security->data: null );
		if ($objOrder!==false && $this->saveOrder ($objOrder)) {

			$this->$objOrder = $objOrder;
			$this->arrProducts = array();
		}
	}
	*/

	public function getMetodoDePago () {
		return $this->objOrder['formaDePago'];
	}
	public function getMetodoDePagoKey () {
		return array_search($this->objOrder['formaDePago']);
	}
	public function getOrderId () {
		return $this->isOrder()? $_SESSION[self::SESSION_ORDER_ID]: false;
	}
	public function getPesoArticulos () {
		return $this->objOrder['peso_total'];
	}
	public function getPrecioTotal () {
		return $this->objOrder['pvp_total'];
	}
	public function getPrecioEnvio () {
		return $this->objOrder['envioImporte'];
	}
	public function getPrecioSuma () {
		return 
			$this->getPrecioTotal () +
			$this->getPrecioEnvio () +
			$this->getPrecioDescuento () +
			$this->getPrecioEnvolverRegalo () +
			$this->getPrecioMetodoPago () +
			$this->getPrecioTarjetaRegalo ();
	}
	public function getPrecioIVA () {
		$arrIVA = array();
		foreach ($this->arrProducts as $objArticulo) {
			foreach ($objArticulo[self::KEY_CART] as $talla => $unidades) {
				if(empty($arrIVA[$objArticulo['iva']])) $arrIVA[$objArticulo['iva']] = 0;

				$arrIVA[$objArticulo['iva']] = $objArticulo['iva'] / 100 * $objArticulo['precio'];
			}
		}
		return $arrIVA;
	}
	public function getPrecioDescuento () {
		return 0;
	}
	public function getPrecioEnvolverRegalo () {
		return 0;
	}
	public function getPrecioMetodoPago () {
		return 0;
	}
	public function getPrecioTarjetaRegalo () {
		return 0;
	}

	public function updateUserOrder( $customerData = []) {
		$this->objOrder["codigo_cliente"]				= !(empty($customerData['id'])) ? $customerData['id'] : $this->objOrder["codigo_cliente"];
		$this->objOrder["nombre"]						= !(empty($customerData['nombre'])) ? $customerData['nombre'] : $this->objOrder["nombre"];
		$this->objOrder["apellidos"]					= !(empty($customerData['apellidos'])) ? $customerData['apellidos'] : $this->objOrder["apellidos"];
		$this->objOrder["email"]						= !(empty($customerData['email'])) ? $customerData['email'] : $this->objOrder["email"];
		$this->objOrder["nif"]							= !(empty($customerData['cif'])) ? $customerData['cif'] : $this->objOrder["nif"];
		$this->objOrder["telefono"]						= !(empty($customerData['telefono'])) ? $customerData['telefono'] : $this->objOrder["telefono"];
		$this->objOrder["fecha_nacimiento"]				= !(empty($customerData['fecha_nacimiento'])) ? $customerData['fecha_nacimiento'] : $this->objOrder["fecha_nacimiento"];
		$this->objOrder["nif_dir_facturacion"]			= !(empty($customerData['cif'])) ? $customerData['cif'] : $this->objOrder["nif_dir_facturacion"];
		$this->objOrder["nombre_dir_facturacion"]		= !(empty($customerData['razon'])) ? $customerData['razon'] : $this->objOrder["nombre_dir_facturacion"];
		$this->objOrder["direccion_dir_facturacion"]	= !(empty($customerData['facturacion_direccion'])) ? $customerData['facturacion_direccion'] : $this->objOrder["direccion_dir_facturacion"];
		$this->objOrder["cp_dir_facturacion"]			= !(empty($customerData['facturacion_codigo_postal'])) ? $customerData['facturacion_codigo_postal'] : $this->objOrder["cp_dir_facturacion"];
		$this->objOrder["localidad_dir_facturacion"]	= !(empty($customerData['facturacion_localidad'])) ? $customerData['facturacion_localidad'] : $this->objOrder["localidad_dir_facturacion"];
		$this->objOrder["provincia_dir_facturacion"]	= !(empty($customerData['facturacion_provincia'])) ? $customerData['facturacion_provincia'] : $this->objOrder["provincia_dir_facturacion"];
		$this->objOrder["pais_dir_facturacion"]			= !(empty($customerData['facturacion_pais'])) ? $customerData['facturacion_pais'] : $this->objOrder["pais_dir_facturacion"];
		$this->objOrder["nombre_dir_envio"]				= !(empty($customerData['envio_nombre'])) ? $customerData['envio_nombre'] : $this->objOrder["nombre_dir_envio"];
		$this->objOrder["direccion_dir_envio"]			= !(empty($customerData['envio_direccion'])) ? $customerData['envio_direccion'] : $this->objOrder["direccion_dir_envio"];
		$this->objOrder["cp_dir_envio"]					= !(empty($customerData['envio_codigo_postal'])) ? $customerData['envio_codigo_postal'] : $this->objOrder["cp_dir_envio"];
		$this->objOrder["localidad_dir_envio"]			= !(empty($customerData['envio_localidad'])) ? $customerData['envio_localidad'] : $this->objOrder["localidad_dir_envio"];
		$this->objOrder["provincia_dir_envio"]			= !(empty($customerData['envio_provincia'])) ? $customerData['envio_provincia'] : $this->objOrder["provincia_dir_envio"];
		$this->objOrder["pais_dir_envio"]				= !(empty($customerData['envio_pais'])) ? $customerData['envio_pais'] : $this->objOrder["pais_dir_envio"];
	}

	public function sendRequest() {
		if ($this->objOrder !== false && count($this->arrProducts)) {
			$this->objOrder['pagando'] = '1';

			$arrParams = array();
			$arrParams['id_order'] = $this->objOrder['id'];
			$arrParams['tipo'] = $this->getMetodoDePago();
			$arrParams['articulos'] = count($this->arrProducts);
			$arrParams['pvp_articulos'] = $this->getPrecioTotal();
			$arrParams['pvp_total'] = $this->getPrecioSuma();
			$arrParams['pvp_envio'] = $this->getPrecioEnvio();
			$arrParams['pvp_iva'] = array_sum($this->getPrecioIVA());
			$arrParams['pvp_descuento'] = $this->getPrecioDescuento();
			$arrParams['pvp_pago'] = $this->getPrecioMetodoPago();
			$arrParams['pvp_tarjeta'] = $this->getPrecioTarjetaRegalo();
			$arrParams['pvp_envolver'] = $this->getPrecioEnvolverRegalo();
			$arrParams['pvp_puntos'] = 0;
			return $this->orders->setRequestData($arrParams);	
		}
		return false;
	}

	public function setMetodoDePago ($strMetodoPago) {
		if(array_search(strtolower($strMetodoPago), $this->arrMetodosPago)!==false) {
			$this->objOrder['formaDePago'] = $strMetodoPago;
			return true;
		}
		return false;
	}
	public function setMetodoDePagoKey ($indexMetodoPago) {
		if(isset($this->arrMetodosPago[$indexMetodoPago])) {
			$this->objOrder['formaDePago'] = $strMetodoPago;
			return true;
		}
		return false;
	}
	public function setPesoTotal ($numPeso) {
		$this->objOrder['peso_total'] = $numPeso;
	}
	public function setPrecioTotal ($numPrecio) {
		$this->objOrder['pvp_total'] = $numPrecio;
	}
	public function setPrecioEnvio ($numPrecio) {
		$this->objOrder['envioImporte'] = $numPrecio;
	}
	public function setUnidades ($strProducto,$numUnidades=1,$strTalla='') {
		$this->addProduct($strProducto);

		if(isset($this->arrProducts[$strProducto][self::KEY_CART])) {
			$arrCarrito = &$this->arrProducts[$strProducto][self::KEY_CART];
			$arrCarrito[$strTalla] = $numUnidades;
			if($arrCarrito[$strTalla]>0)  {
				$this->setPrecioTotal($this->calculaPrecioArticulos($this->arrProducts));
				$this->setPesoTotal($this->calcularPesoArticulos($this->arrProducts));
				$this->setPrecioEnvio($this->calculaPrecioGastos($this->getPesoArticulos()));	
				return true;
			}

			unset($arrCarrito[$strTalla]);
		}
		$this->setPrecioTotal($this->calculaPrecioArticulos($this->arrProducts));
		$this->setPesoTotal($this->calcularPesoArticulos($this->arrProducts));
		$this->setPrecioEnvio($this->calculaPrecioGastos($this->getPesoArticulos()));	
		return false;
	}
}