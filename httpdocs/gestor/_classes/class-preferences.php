<?php

class Preferences extends Controller {
	protected $db, $preferencesTable;

	public function __construct($db) {
		$this->db = $db;
		
		$this->preferencesTable = new Table($this->db, "preferences");
		
		$this->files = new Files($db, 'preferences');
	}

	public function __destruct() {
		return true;
	}


	/// MÉTODOS PÚBLICOS ///
	
	public function getValue($fieldId) {
		$val = $this->preferencesTable->selectRecords(["text_1"], ["id" => $fieldId], null, null, null, "FETCH_COLUMN");
		return isset($val[0]) ? $val[0] : "";
	}

	public function setValue($fieldId, $value) {
		if (!$this->exists($fieldId)) {
			return $this->create($fieldId, $value);
		}

		return $this->preferencesTable->updateRecord(["text_1" => $value, "id" => $fieldId], ["id" => $fieldId]);
	}

	public function exists($fieldId) {
		return $this->getRecord ('Preferences', ['id'], ['id' => $fieldId]) !== false;
	}

	public function create($fieldId, $value = '') {
		return $this->preferencesTable->insertRecord(["text_1" => $value, "id" => $fieldId]);		
	}
}