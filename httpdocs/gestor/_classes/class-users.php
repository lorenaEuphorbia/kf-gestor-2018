<?php

class Users extends Controller {
	protected $db, $usersTable, $userdataTable, $userrolesTable;

	public function __construct($db) {
		$this->db = $db;

		$this->usersTable = new Table($db, "users");

		$this->userdataTable = new Table($db, "users__data");
		$this->userrolesTable = new Table($db, "users__roles");
		$this->tokensTable = new Table($db, "users__tokens");

		$this->userRelRolesTable = new Table($db, "users__rel_roles");
	}

	public function __destruct() {
		return true;
	}


    public function getTokenValid ($token) {
    	$token = $this->getRecord('Tokens', null, ['codigo' => $token]);

        if ($token && $token['valido'] && empty($token['expiracion'])) {
            return $token;
        }

        return false;
    }

	public function getRoleName($roleId) {
		$role = $this->userrolesTable->selectRecords(
			array("name"),
			array("id" => $roleId),
			null,
			null,
			null,
			"FETCH_COLUMN"
		);

		return count($role) == 1 ? $role[0] : "";
	}

	public function getUserRoles($userId) {
		return $this->userRelRolesTable->selectRecords(
			array("role_id"),
			array("user_id" => $userId),
			null,
			null,
			null,
			"FETCH_COLUMN"
		);
	}

	public function getUserData($userId) {
		$result = $this->userdataTable->selectRecords(
			null,
			array("id_user" => $userId),
			null,
			null,
			null,
			"FETCH_ASOC"
		);
		return $result? $result[0] : false;
	}

	public function getUsersList($arrFields = null, $arrFilter = null, $arrOrder = null) {
		return $this->usersTable->selectRecords($arrFields, $arrFilter, $arrOrder);
	}

	public function hasRole($userId, $roleId) {
		return count($this->userRelRolesTable->selectRecords(null, array("user_id" => $userId, "role_id" => $roleId)));
	}

	public function searchUser($id = null, $username = null) {
		if (!empty($id) || !empty($username)) {
			$arrSearch = array();

			if (!empty($id)) $arrSearch["id"] = $id;
			if (!empty($username)) $arrSearch["username"] = $username;

			return $this->usersTable->searchRecord($arrSearch) && $this->usersTable->getCurrentFieldValue('baja')==false;
		}
		else return false;
	}

	public function newUser($username, $password, $creador=0)  {
		$results = $this->usersTable->selectRecords(null,["username" => $username, "baja" => ['IS','NULL']]);
		if (!count($results)) {

			$new_salt = hash('sha512', Utilities::getRandomString(20));
			$password = hash('sha512', $password . $new_salt);
			$new_password = hash('sha512', $password . $new_salt);
			$user_data = [
				'username'	=> $username,
				'password'	=> $new_password,
				'salt'		=> $new_salt,
				'creador'	=> $creador,
			];

			return $this->usersTable->insertRecord($user_data);
		}
		else {
			return false;
		}
	}

	public function setData($userId, $data) {
		$data['id_user'] = $userId;
		$arrWhere = $this->userdataTable->searchRecord(['id_user'=>$userId])==true? ['id_user'=>$userId]: [];
		return $this->setRecord($data, $arrWhere, 'UserData');
	}
	
	public function setPassword($userId, $password) {
		$new_salt = hash('sha512', Utilities::getRandomString(20));
		$password = hash('sha512', $password . $new_salt);
		$new_password = hash('sha512', $password . $new_salt);
		$new_data = [
			'password'	=> $new_password,
			'salt'		=> $new_salt,
			//'fecha_modificacion'	=> date("Y-m-d H:i:s")
		];
		return $this->usersTable->updateRecord($new_data,['id'=>$userId]);
	}

	public function setUserRole($userId, $roleId) {
		return $this->userRelRolesTable->insertRecord(['user_id'=>$userId,'role_id'=>$roleId]);
	}


	public function bajaUser($username)  {
		$arrSearch = [
			"username" => $username, 
			"baja" => ['IS','NULL']
		];
		$user_data = [
			'baja' => date("Y-m-d H:i:s")
		];

		return $this->usersTable->updateRecord($user_data,$arrSearch);

	}

}