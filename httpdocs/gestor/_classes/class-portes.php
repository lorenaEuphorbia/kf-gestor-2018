<?php  # http://www.geonames.org/


class Portes extends Controller {
	protected $db, $portesTable, $paisesTable, $provinciasTable, $transportistasTable;
	public $files;

	public function __construct($db) {
		$this->db = $db;
		
		$this->portesTable = new Table($this->db, "portes__zonas");
		$this->paisesTable = new Table($this->db, "portes__paises");
		$this->provinciasTable = new Table($this->db, "portes__provincias");

		// $this->files = new Files($db, "blog");
	}

	public function getPaisesnombres () {
		return $this->getList('Paises', ['id','nombre'], ['activo'=>'1'], ['nombre ASC'], null, null, 'FETCH_KEY_PAIR');
	}

	public function getPaisesPortes () {
		$arrOutData = [];
		$arrnombres = $this->getPaisesnombres();	
		$result = $this->getList('Portes', ['id_pais'], ['activo'=>'1','envio'=>'1'], null, null, null, 'FETCH_COLUMN');	
		if($result) {
			foreach ($result as $codigo) {
				if (isset($arrnombres[$codigo]) ) {
					$arrOutData[$codigo] = $arrnombres[$codigo];
				}
			}
		}
		return $arrOutData;
	}

	public function getProvinciasPortes ($codPais) {
		$arrProvincias = $this->getProvinciasnombres ($codPais);
		$result = $this->getList('Portes', ['id_provincia'], ['id_pais'=>$codPais,'envio'=>'0'],null, null, null,'FETCH_COLUMN');
		if($result) {
			foreach ($result as $codigo) {
				if(isset($arrProvincias[$codigo])) unset($arrProvincias[$codigo]);
			}
		}
		return $arrProvincias;
	}

	public function getPaisnombre ($codPais) {
		return $this->search(['id_pais'=>$codPais], 'Paises')? $this->getCurrentFieldValue('nombre', 'Paises'): '';
	}

	public function getProvincias () {
		return $this->getList('Provincias', ['id_pais','id_provincia','nombre'], [], ['nombre ASC']);
	}

	public function getProvinciasnombres ($codPais) {
		return $this->getList('Provincias', ['id','nombre'], ['id_pais'=>$codPais], ['nombre ASC'], null, null, 'FETCH_KEY_PAIR');
	}

	public function getProvincianombre ($codPais,$codProvi) {
		return $this->search(['id_pais'=>$codPais,'id_provincia'=>$codProvi], 'Provincias')? $this->getCurrentFieldValue('nombre', 'Provincias'): '';
	}

	private function calculaPortesZona ($pvp, &$row) {
		$row['coste_rebaja'] = $row['is_rebaja']==true && $row['limite_rebaja']>0 ? number_format($row['limite_rebaja']): false;
		$row['coste'] = $row['coste_rebaja']!==false && $pvp > $row['coste_rebaja']? $row['pvp_rebaja']: $row['pvp'];

		$row['pvp'] = number_format($row['pvp'], 2);
		$row['pvp_rebaja'] = number_format($row['pvp_rebaja'], 2);
		$row['pvp_devolver'] = number_format($row['pvp_devolver'], 2);
		$row['limite_rebaja'] = number_format($row['limite_rebaja'], 2);
		$row['coste'] = number_format($row['coste'], 2);

		return $row['coste'];
	}

	public function getPortesZona ($pvp, $codProvincia, $codPais) {
		$arrOutPortes = [];

		# buscamos los transportistas		# 
		$arrTransportistas = $this->getTransportistasnombres ();
		foreach ($arrTransportistas as $codTransportista => $transportista) {
			$result = $this->getList('Portes',[],['id_pais'=>$codPais,'id_transportista'=>$codTransportista,'activo'=>'1'],['id_provincia ASC']);
			if($result) {
				foreach ($result as $rowPorte) {
					# comprobamos si hay valor general
					if(!isset($rowPorte['id_provincia']) || $rowPorte['id_provincia']==null || empty($rowPorte['id_provincia'])) {
						if ($rowPorte['envio']==true) {
							$this->calculaPortesZona($pvp, $rowPorte);
							$arrOutPortes[$codTransportista] = $rowPorte['coste'];
						}
					}
					else if ($rowPorte['id_provincia']==$codProvincia) {
						if ($rowPorte['envio']==true) {
							$this->calculaPortesZona($pvp, $rowPorte);
							$arrOutPortes[$codTransportista] = $rowPorte['coste'];
						}
						else if (isset($arrOutPortes[$codTransportista])){
							unset($arrOutPortes[$codTransportista]);
						}
					}
				}
			}
		}
		return count($arrOutPortes)? $arrOutPortes: false;
	}

	public function getPortesDevolucionZona ($pvp, $codProvincia, $codPais) {
		$arrOutPortes = [];

		# buscamos los transportistas		# 
		$arrTransportistas = $this->getTransportistasnombres ();
		foreach ($arrTransportistas as $codTransportista => $transportista) {
			$result = $this->getList('Portes',[],['id_pais'=>$codPais,'id_transportista'=>$codTransportista,'activo'=>'1'],['id_provincia ASC']);
			if($result) {
				foreach ($result as $rowPorte) {
					# comprobamos si hay valor general
					if(!isset($rowPorte['id_provincia']) || $rowPorte['id_provincia']==null || empty($rowPorte['id_provincia'])) {
						if ($rowPorte['envio']==true) {
							$this->calculaPortesZona($pvp, $rowPorte);
							$arrOutPortes[$codTransportista] = $rowPorte['pvp_devolver'];
						}
					}
					else if ($rowPorte['id_provincia']==$codProvincia) {
						if ($rowPorte['envio']==true) {
							$this->calculaPortesZona($pvp, $rowPorte);
							$arrOutPortes[$codTransportista] = $rowPorte['pvp_devolver'];
						}
						else if (isset($arrOutPortes[$codTransportista])){
							unset($arrOutPortes[$codTransportista]);
						}
					}
				}
			}
		}
		return count($arrOutPortes)? $arrOutPortes: false;
	}

	public function getTransportistasnombres () {
		return $this->getList('Transportistas', ['Codigo','nombre'], ['GRP_ID'=>GRP_ID], ['nombre ASC'], null, null, 'FETCH_KEY_PAIR');
	}

	public function getTransportistanombre ($codTransportista) {
		return $this->search(['Codigo'=>$codTransportista], 'Transportistas')? $this->getCurrentFieldValue('nombre', 'Transportistas'): '';
	}
}