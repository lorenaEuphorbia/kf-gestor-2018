<?php

class I18n {
	private $area, $arrLang, $defaultLang, $folderLangFiles;
	private $arrAreas = ["admin", "web"];
	public $currentLanguage, $translatedTexts;

	public function __construct($area, $arrLanguages, $default = "") {
		if (!function_exists("gettext")) {
			trigger_error("La extensión \"gettext\" de PHP no está instalada.", E_USER_ERROR);
		}

		if (in_array($area, $this->arrAreas)) {
			$this->area = $area;

			switch ($this->area) {
				case "admin": $this->folderLangFiles = realpath(__DIR__ . '/../lang/'); break;
				case "web": $this->folderLangFiles = realpath(__DIR__ . '/../../lang/'); break;
			}
		}
		else {
			trigger_error("IDIOMAS: área de actuación incorrecta.", E_USER_ERROR);
		}

		if (is_array($arrLanguages) && $arrLanguages) {
			$this->arrLang = $arrLanguages;
		}
		else {
			trigger_error("IDIOMAS: listado de idiomas disponibles incorrecto.", E_USER_ERROR);
		}

		$this->defaultLang = $this->CheckLanguage($default) ? $default : $this->arrLang[0];

		// Detectamos el idioma y cargamos los strings correspondientes
		$this->AutoSelectCurrentLanguage();
	}


	/// MÉTODOS PÚBLICOS ///

	public function GetTranslation($id) {
		return !empty($this->translatedTexts[$id]) ? $this->translatedTexts[$id] : $id;
	}

	public function GetUserLanguage() {
		if ($_SERVER['HTTP_ACCEPT_LANGUAGE']) {
			return substr ($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
		}
		else if ($_SERVER['HTTP_USER_AGENT']) {
			$user_agents = explode(";" , $_SERVER['HTTP_USER_AGENT']);
			
			for ($x=0; $x < count($user_agents); $x++) {
				$user_languages = explode("-", $user_agents[$x]);
				
				if (count($user_languages) == 2) {
					if (strlen(trim($user_languages[0])) == 2) {
						$user_languages[count($user_languages)] = trim($user_languages[0]);
					}
				}
			}
			
			return $user_languages[0];
		}
		else {
			return $this->defaultLang;
		}
	}
	
	public function SetCurrentLanguage($lang = null) {
		if ($lang) {
			$_SESSION['LANG_' . strtoupper($this->area)] = $lang;
		}
		
		if (@!$_SESSION['LANG_' . strtoupper($this->area)]) {
			$_SESSION['LANG_' . strtoupper($this->area)] = $this->GetUserLanguage();
		}
		
		if (!$this->CheckLanguage($_SESSION['LANG_' . strtoupper($this->area)])) {
			$_SESSION['LANG_' . strtoupper($this->area)] = $this->defaultLang;
			
		}

		$this->currentLanguage = $_SESSION['LANG_' . strtoupper($this->area)];

		/// P: En lugar de esto se puede crear una tabla de conversión correcta, para por ejemplo: "en" => "en_GB" ó "ca" => "ca_ES"
		$locale = $this->currentLanguage . "_" . strtoupper($this->currentLanguage);

		putenv("LC_ALL=$locale");
		setlocale(LC_ALL, $locale);

		$domain = "text"; /// nombre del archivo que contiene los textos

		bindtextdomain($domain, $this->folderLangFiles);
		textdomain($domain);
	}


	/// MÉTODOS PRIVADOS ///

	private function AutoSelectCurrentLanguage() {
		$tmp_lang = isset($_POST["lang"]) ? $_POST["lang"] : (isset ($_GET["lang"]) ? $_GET["lang"] : "");
		$tmp_lang = strlen($tmp_lang) == 2 ? $tmp_lang : "";

		$this->SetCurrentLanguage($tmp_lang);
	}

	private function CheckLanguage($lang = null) {
		return in_array($lang, $this->arrLang) && file_exists("$this->folderLangFiles/$lang.php");
	}
}