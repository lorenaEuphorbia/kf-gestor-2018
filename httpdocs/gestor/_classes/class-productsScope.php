<?php

interface IProductsScope
{

	public function addProduct($code);
	public function removeProduct($code);
	public function setProduct($code,$params);
	public function getProduct($code);

	public function addProductAt($index);
	public function removeProductAt($index);
	public function setProducAt($index,$params);
	public function getProductAt($index);
}


abstract class ProductsScope implements IProductsScope
{

	protected $arrProducts;
	protected $clsProducts;

	abstract protected function saveData($arrData);
	abstract protected function retrieveData();

	public function __construct($clsProducts) {
		$this->clsProducts = $clsProducts;
		$this->arrProducts = $this->retrieveData();
	}

	public function __destruct() {
		$this->saveData($this->arrProducts);
	}

	public function addProduct($code) {
		if(isset($this->arrProducts[$code]))
			return false;
		else
			$this->arrProducts[$code] = $this->getProductData($code);
		return true;
	}
	public function removeProduct($code) {
		if(isset($this->arrProducts[$code])){
			unset($this->arrProducts[$code]);
			return true;
		} 
		return false;
	}
	public function setProduct($code,$params=[],$strCode="") {
		foreach ($params as $key => $value) {
			if($strCode)
				$this->arrProducts[$code][$strCode][$key] = $value;
			else
				$this->arrProducts[$code][$key] = $value;
		}
		return true;
	}
	public function unsetProduct($code,$param,$strCode="") {
		//var_dump($this->arrProducts); exit;
		if($strCode) {
			if(!isset($this->arrProducts[$code][$strCode][$param])) return false;
			unset($this->arrProducts[$code][$strCode][$param]);
		} 
		else {
			if(!isset($this->arrProducts[$code][$param])) return false;
			unset($this->arrProducts[$code][$param]);
		}
		return true;		
	}
	public function unsetProductByKey($code,$keyParam,$strCode="") {
		$index = $strCode? array_search($keyParam, $this->arrProducts[$code][$strCode]) : array_search($keyParam, $this->arrProducts[$code]);
		//var_dump($index); exit;
		if($index!==false) return $this->unsetProduct($code,$index,$strCode);
		return false;		
	}
	public function getProduct($code) {
		return $this->arrProducts[$code];
	}

	public function addProductAt($index) {
		return $code = $this->getProductCode($index)!==false ? $this->addProduct($code) : false;
	}
	public function removeProductAt($index) {
		return $code = $this->getProductCode($index)!==false ? $this->removeProduct($code) : false;
	}
	public function setProducAt($index,$params) {
		return $code = $this->getProductCode($index)!==false ? $this->setProduct($code,$params) : false;
	}
	public function getProductAt($index) {
		return $code = $this->getProductCode($index)!==false ? $this->getProduct($code) : false;
	}

	protected function getProductCode($index) {
		$arrKeys = array_keys($this->arrProducts[$index]);
		return isset($arrKeys[$index]) ? $arrKeys[$index] : false;
	}
	public function clear() {
		$this->arrProducts = [];
	}
	protected function getProductData($code) {
		return $this->clsProducts->getProductoById($code);
	}
	protected function getProductsData($arrCodes) {		
		$arrData = [];
		foreach ($arrCodes as $key ) {
			$arrData[$key] = $this->getProductData($key);
		}
		return $arrData;
	}
	public function getProducts() {
		return $this->arrProducts;
	}
}

