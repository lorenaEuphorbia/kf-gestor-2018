<?php

class Base {
	const KEY_HISTORY = 'historial';

	private $dbMySQL, $arrSecciones;
	public $db, $security, $users, $error, $productos, $noticias, $orders, $preferences, $portes;

	public function __construct() {
		$dbMySQL = new Db(DB_DRIVER, DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS);
		$dbMySQL->connect();

		if ($dbMySQL) {
			$this->users = new Users($dbMySQL->db);
			$this->productos = new Productos($dbMySQL->db);
			$this->noticias = new Noticias($dbMySQL->db);
			$this->security = new Security($dbMySQL->db, $this->users);
			$this->portes = new Portes($dbMySQL->db);
			$this->orders = new Orders($dbMySQL->db,$this->productos,$this->security,$this->portes);
			$this->preferences = new Preferences($dbMySQL->db);
			$this->carrito = new ProductsCart($this->orders);

			$this->db = $dbMySQL->db;

			# mejor cogerlo de la base de datos
			$this->arrSecciones = [
				'home',
				'quienes-somos',
				'nuestras-marcas',
				'calidad',
				'seguridad-alimentaria',
				'innovacion',
				'responsabilidad-social',
				'tienda-gran-seleccion',
				'tus-datos',
				'carrito-resumen',
				'noticias',
				'contacto',
			];

			$this->saveHistory ();
		}
		else {
			$this->error = _("Error de conexión con base de datos.");
		}
	}

	public function __destruct() {
		return true;
	}

	public function saveHistory () {
		$uri = str_replace(['/es','/en','/it','/fr','/pt'] , '', $_SERVER['REQUEST_URI']);

		switch ($uri) {
			case '':
			case '/':
				$uri = 'home';
				break;

			default:
				$uri = trim($uri, '/');
				break;
		}


		if(empty($_SESSION[self::KEY_HISTORY])) {
			$_SESSION[self::KEY_HISTORY] = [];
		}

		# borrar página visitada del historial
		if ($result=array_search($uri , $_SESSION[self::KEY_HISTORY])!==false) {
			$_SESSION[self::KEY_HISTORY] = array_splice($_SESSION[self::KEY_HISTORY], $result, 1);
		}

		# añadir última página del historial
		if ($result=array_search($uri, $this->arrSecciones)!==false) {
			array_unshift($_SESSION[self::KEY_HISTORY] , $uri);
		}

	}

	public function setHistory ($arrHistory) {
		$_SESSION[self::KEY_HISTORY] = $arrHistory;
	}

	public function getHistory ($arrHistory) {
		return !empty($_SESSION[self::KEY_HISTORY])? $arrHistory: [];
	}

	public function getLastPage () {
		if(count($_SESSION[self::KEY_HISTORY])>0) {
			$uri = $_SESSION[self::KEY_HISTORY][0];

			switch ($uri) {
				case '':
				case 'home':
					$uri = '/';
					break;

				case 'tus-datos':
					if (!empty($_SESSION[self::KEY_HISTORY][1]) && $_SESSION[self::KEY_HISTORY][1]=='carrito-resumen') {
						$uri = '/' . $_SESSION[self::KEY_HISTORY][1] . '/';
						break;
					}

				default:
					$uri = '/' . $uri . '/';
					break;
			}

			return $uri;
		}
		return '/';
	}
}
