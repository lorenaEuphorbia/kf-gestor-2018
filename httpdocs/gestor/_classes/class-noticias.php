<?php

class Noticias extends Controller {
	protected $categoriesTable, $commentsTable, $postsTable, $tagsTable, $relTagsTable, $archivosTable;

	public function __construct($db) {
		$this->db = $db;

		$this->categoriasTable = new Table($db, 'noticias__categorias');
		$this->commentsTable = new Table($db, 'noticias__comments');
		$this->postsTable = new Table($db, 'noticias__posts');
		$this->tagsTable = new Table($db, 'noticias__tags');
		$this->relTagsTable = new Table($db, 'noticias__rel_tags');
		$this->archivosTable = new Table($db, 'noticias__archivos');
		$this->galeriaTable = new Table($db, 'noticias__galeria');

		$this->files = new Files($db, 'noticias');
		$this->map_options = ['Files' => ['Galeria','Archivos'] ];
	}
	

	/// MÉTODOS PÚBLICOS ///

	public function get ($slug) {

		$strQuery = 'SELECT
            noticias__posts.id as id,
            noticias__posts.title_1 as titulo,
            noticias__posts.intro_1 as introduccion,
            noticias__posts.content_1 as contenido,
		    noticias__categorias.url_1 as url_categoria,
		    noticias__categorias.text_1 as categoria,
            noticias__posts.leido as leido,
            noticias__posts.date as creacion,
            noticias__posts.publishing_date as publicacion,
            noticias__posts.modification_date as modificacion,
            noticias__posts.url_1 as url,
            noticias__posts.url_1 as url,
            noticias__posts.url_2 as url_en,
            \'es\' as idioma
		FROM
			noticias__posts,
		    noticias__categorias
		WHERE
			noticias__posts.category_id = noticias__categorias.id AND
		    noticias__posts.publishing_date < :fecha AND
		    noticias__posts.url_1 = :slug1';

		$arrParams = ['fecha'=>date('Y-m-d H:i:s'),'slug1'=>$slug];
		$result = $this->postsTable->query($strQuery,$arrParams);
		return $result? $result[0] : false;
	}
	public function getEn ($slug) {

		$strQuery = 'SELECT
            noticias__posts.id as id,
            noticias__posts.title_2 as titulo,
            noticias__posts.intro_2 as introduccion,
            noticias__posts.content_2 as contenido,
		    noticias__categorias.url_2 as url_categoria,
		    noticias__categorias.text_2 as categoria,
            noticias__posts.leido as leido,
            noticias__posts.date as creacion,
            noticias__posts.publishing_date as publicacion,
            noticias__posts.modification_date as modificacion,
            noticias__posts.url_2 as url,
            noticias__posts.url_1 as url_es,
            noticias__posts.url_2 as url_en,
            \'en\' as idioma
		FROM
			noticias__posts,
		    noticias__categorias
		WHERE
			noticias__posts.category_id = noticias__categorias.id AND
		    noticias__posts.publishing_date < :fecha AND
		    (noticias__posts.url_2 = :slug2)';

		$arrParams = ['fecha'=>date('Y-m-d H:i:s'),'slug2'=>$slug];
		$result = $this->postsTable->query($strQuery,$arrParams);
		return $result? $result[0] : false;
	}

	public function getHistorico () {
		$arrHistorico = array();
		$strQuery = 'select distinct(DATE_FORMAT(publishing_date,\'%Y-%m\')) from '.$this->postsTable->getTableName().' where publishing_date < :fecha';
		$arrParams = ['fecha'=>date('Y-m-d H:i:s')];
		$result = $this->postsTable->query($strQuery,$arrParams);
		foreach ($result as $date) {
			$strDate = reset($date);
			$arrDate = explode('-', $strDate);
			$arrHistorico[$arrDate[0]][$arrDate[1]] = $strDate;
		}
		return $arrHistorico;
	}

	public function getCategorias ($locale='es') {
		$arrCategorias = array();
		$strQuery = 'select p.category_id as id, c.url_1 as url_es, c.url_2 as url_en, c.text_1 as nombre_es, c.text_2 as nombre_en from  '.$this->postsTable->getTableName(). ' p, '.$this->categoriesTable->getTableName().' c where p.category_id = c.id and p.publishing_date < :fecha';
		$arrParams = ['fecha'=>date('Y-m-d H:i:s')];
		$result = $this->postsTable->query($strQuery,$arrParams);
		foreach ($result as $data) {
			$arrCategorias[$data['id']] = [
				'id' => $data['id'],
				'url' => $data['url_'.$locale],
				'nombre' => $data['nombre_'.$locale],
			];
		}
		return $arrCategorias;
	}

	public function getTags ($locale='es') {
		$arrTags = array();
		$strQuery = 'select DISTINCT r.tag_id as id, t.url_1 as url_es, t.url_2 as url_en, t.text_1 as nombre_es, t.text_2 as nombre_en from '.$this->tagsTable->getTableName().' t, '.$this->relTagsTable->getTableName(). ' r INNER JOIN '.$this->postsTable->getTableName().' as p ON p.id = r.post_id where r.tag_id = t.id and p.publishing_date < :fecha ORDER BY nombre_'.$locale.' ASC';
		$arrParams = ['fecha'=>date('Y-m-d H:i:s')];
		$result = $this->postsTable->query($strQuery,$arrParams);
		foreach ($result as $data) {
			$arrTags[$data['id']] = [
				'id' => $data['id'],
				'url' => $data['url_'.$locale],
				'nombre' => $data['nombre_'.$locale],
			];
		}
		return $arrTags;
	}

	public function getPostTags ($idPost,$locale='es') {
		$arrTags = array();
		$strQuery = 'select DISTINCT r.tag_id as id, t.url_1 as url_es, t.url_2 as url_en, t.text_1 as nombre_es, t.text_2 as nombre_en from '.$this->tagsTable->getTableName().' t, '.$this->relTagsTable->getTableName(). ' r where r.tag_id = t.id and r.post_id = :Codigo ORDER BY nombre_'.$locale.' ASC ';
		$arrParams = ['Codigo'=>$idPost];
		$result = $this->postsTable->query($strQuery,$arrParams);
		foreach ($result as $data) {
			$arrTags[] = [
				'id' => $data['id'],
				'url' => $data['url_'.$locale],
				'nombre' => $data['nombre_'.$locale],
			];
		}
		return $arrTags;
	}

	public function getListado ($locale='es') {
		$arrNoticias = [];
		$strQuery = 'SELECT
            noticias__posts.id as id,
            noticias__posts.title_1 as titulo_es,
            noticias__posts.intro_1 as introduccion_es,
            substr(noticias__posts.content_1, 1, 200) as contenido_es,
		    noticias__categorias.url_1 as url_categoria_es,
		    noticias__categorias.text_1 as categoria_es,
            noticias__posts.title_2 as titulo_en,
            noticias__posts.intro_2 as introduccion_en,
            substr(noticias__posts.content_2, 1, 200) as contenido_en,
		    noticias__categorias.url_2 as url_categoria_en,
		    noticias__categorias.text_2 as categoria_en,
            noticias__posts.leido as leido,
            noticias__posts.date as creacion,
            noticias__posts.publishing_date as publicacion,
            noticias__posts.modification_date as modificacion,
            noticias__posts.url_1 as url_es,
            noticias__posts.url_2 as url_en
		FROM
			noticias__posts,
		    noticias__categorias
		WHERE
			noticias__posts.category_id = noticias__categorias.id AND
		    noticias__posts.publishing_date < :fecha';

		$arrParams = ['fecha'=>date('Y-m-d H:i:s')];
		$result = $this->postsTable->query($strQuery,$arrParams);
		foreach ($result as $data) {
			$arrNoticias[] = [
				'id' => $data['id'],
				'titulo' => $data['titulo_'.$locale],
				'introduccion' => $data['introduccion_'.$locale],
				'contenido' => $data['contenido_'.$locale],
				'url_categoria' => $data['url_categoria_'.$locale],
				'categoria' => $data['categoria_'.$locale],
				'leido' => $data['leido'],
				'creacion' => $data['creacion'],
				'publicacion' => $data['publicacion'],
				'modificacion' => $data['modificacion'],
				'url' => $data['url_'.$locale],
				'url_es' => $data['url_es'],
				'url_en' => $data['url_en'],
				'tags' => $this->getPostTags ($data['id'],$locale),
			];
		}
		return $arrNoticias;
	}

	public function getAll($option='Categories') {
		return $this->getChilds($option, ['url_1','text_1'], ['position DESC']);
	}

	public function getChilds ($option, $arrSelect, $arrOrder=[], $ownerId=0, $key='id', $keyOwner='owner_id') {

		if(!in_array($key, $arrSelect)) $arrSelect[]=$key;
		if(!in_array($keyOwner, $arrSelect)) $arrSelect[]=$keyOwner;

		$arr_tmp = $this->getList(
			$option,
			$arrSelect,
			[$keyOwner=>$ownerId],
			$arrOrder
		);

		for ($i=0; $i < count($arr_tmp); $i++) {
			$arr_tmp[$i]['childs'] = $this->getChilds($option, $arrSelect, $arrOrder, $arr_tmp[$i][$key], $key, $keyOwner);
		}

		return $arr_tmp;
	}

}
