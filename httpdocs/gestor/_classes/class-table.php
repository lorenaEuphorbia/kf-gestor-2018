<?php

class Table {
	protected $db, $fields, $tableName;

	public function __construct($db, $tableName, $tableKey=null) {
		$this->db = $db;
		$this->fields = array();
		$this->tableName = $tableName;
		$this->tableKey = $tableKey;
		$this->init();
	}

	public function __destruct() {
		return true;
	}


	function checkField($field, $value = "") {
		$field = preg_replace("/\s(DESC|ASC)/i", "", $field);

		if (!isset($this->fields[$field])) {
			trigger_error("El campo '$field' no se encuentra en la tabla '$this->tableName'.", E_USER_ERROR);
			return false;
		}

		if ($value != "") {
			/// Comprobar si es un tipo de valor válido y está dentro de la longitud permitida
			$type = preg_split("/[\(\)]/", trim($this->fields[$field]['Type']));

			//echo "<pre>"; print_r($type); exit;

			switch ($type[0]) {
				case "int":
					/// comprobar que es un entero dentro del margen permitido y si es mayor que 0 (unsigned)
				break;
				case "varchar":
					if (!empty($type[1]) && strlen($value) > $type[1]) {
						trigger_error("El valor introducido para el campo '$field' tiene más caracteres de los permitidos: $type[1].", E_USER_ERROR);
						return false;
					}
				break;
			}

			/// Si existe uno ya y es un valor que debe ser único o primario
			if (in_array($this->fields[$field]['Key'], array("PRI", "UNI"))) {
				/// Debe permitir actualizaciones y esto no lo hace
				/*$tmp = $this->selectRecords(
					array($field),
					array($field => $value)
				);

				//echo "<pre>"; print_r($tmp); exit;

				if (count($tmp)) {
					trigger_error("El valor introducido para el campo '$field' no es válido por estar repetido.", E_USER_ERROR);
					return false;
				}*/
			}
		}

		return true;
	}

	function createFieldAssignments($arrFields, $checks = true) {
		$assignments = "";
		
		foreach ($arrFields as $fieldKey => $value) {
			if (!$checks || $this->checkField($value)) {

				if (strtolower($value) === 'null' || $value === null)
					$assignments .= (strlen($assignments) ? ", " : "") . " $fieldKey = NULL";
				else
					$assignments .= (strlen($assignments) ? ", " : "") . " $fieldKey = :SET$fieldKey";

			}
		}

		return $assignments;
	}

	function createFieldList($arrFields, $prefix = "", $checks = true) {
		$fieldList = "";

		if (is_array($arrFields) && $arrFields) {
			foreach ($arrFields as $field) {
				if (!$checks || $this->checkField($field)) {
					$fieldList .= (strlen($fieldList) ? ", " : "") . $prefix . $field;
				}
			}
		}
		else {
			$fieldList = "*";
		}

		return $fieldList;
	}

	public function createWhere($arrFields, $prepared = true, $checks = true) {
		$where = "";

		$arrNexos = array("AND", "OR", "(", ")");
		$arrOperators = array("=", ">", "<", ">=", "<=", "<>","in","like",'is','like','is like','is not');

		foreach ($arrFields as $fieldKey => $value) {

			# ARRAY = [0]:operador* , [1]:valor* , [2]:nombre_unico
			if (is_array($value)) {
				$where_operator = $value[0];
				$where_value = $value[1];
				$where_fieldname = isset($value[2]) && !empty($value[2]) ? $value[2] : $fieldKey;
			}
			else {
				$where_operator = "=";
				$where_value = $value;
				$where_fieldname = $fieldKey;
			}

			# comprobamos el operador
			if (!in_array(strtolower($where_operator), $arrOperators)) trigger_error("Operador no válido en cláusula WHERE.", E_USER_ERROR);

			//if ($checks || $this->checkField($fieldName)) 
			if (strtolower($where_value) === 'null' || $where_value === null)
				$where .= (strlen($where) ? " AND " : "") . $where_fieldname . " $where_operator NULL";
			else
				$where .= (strlen($where) ? " AND " : "") . $where_fieldname . " $where_operator " . ($prepared ? ":$fieldKey" : $where_value);
		}

		return $where;
	}

	public function deleteRecord($arrFilter) {
		if (empty($arrFilter)) return 0;

		$query = "DELETE FROM $this->tableName WHERE " . $this->createWhere($arrFilter);

		$stmt = $this->db->prepare($query);
		$stmt->execute($arrFilter);

		return $stmt->rowCount();
	}

	public function unsetRecord() {
		if(!empty($this->fields)) {
			$this->fields = array();
			return true;
		}
		return false;

	}

	public function getFieldNames() {
		return array_keys($this->fields);
	}

	public function getData() {
		$arrOutData = array();
		foreach ($this->fields as $fieldName => $value) {
			$arrOutData[$fieldName] = $value['Current'];
		}
		return $arrOutData;
	}

	public function getFieldCurrentValue($fieldName) {
		return !empty($this->fields[$fieldName]['Current']) ? $this->fields[$fieldName]['Current'] : "";
	}

	public function getLastInsertId() {
		return $this->db->lastInsertId();
	}

	function init() {
		/// Comprobamos si existe la tabla
		$stmt = $this->db->prepare("SHOW TABLES LIKE '$this->tableName'");
		$stmt->execute();

		//$tables = $stmt->fetchAll(PDO::FETCH_COLUMN); // $stmt->rowCount() sólo vale para MySQL

		if (!$stmt->rowCount()) {
			trigger_error("La tabla '$this->tableName' no existe.", E_USER_ERROR);
		}

		/// Recopilamos los campos que tiene esta tabla
		$stmt = $this->db->prepare("DESCRIBE $this->tableName");
		$stmt->execute();
		
		$fields = $stmt->fetchAll(PDO::FETCH_ASSOC);

		//echo "<pre>"; print_r($fields); exit;

		/// Comprobamos si hay campos
		if (count($fields)) {
			/// Buscamos los valores por defecto y los asignamos
			foreach ($fields as $field) {
				$this->fields[$field['Field']] = $field;
				$this->fields[$field['Field']]['Current'] = $field['Default'];

				if($field['Key']=='PRI') $this->tableKey=$field['Field'];
			}

			//echo "<pre>"; print_r($this->fields); exit;

			return true;
		}
		else {
			trigger_error("La tabla '$this->tableName' no tiene campos.", E_USER_ERROR);
		}

		if( !$this->tableKey ) $this->tableKey = array_values($this->fields)[0]['Field'];
	}

	public function insertRecord($arrFields) {
		$arrExec = [];
		foreach ($arrFields as $key => $value) {
			$this->checkField($key, $value);
			if (strtolower($value) !== 'null' && $value !== null) $arrExec["SET$key"] = $value;
			else unset($arrFields[$key]);
		}

		$fieldNames = array_keys($arrFields);

		$query = "INSERT INTO $this->tableName (" . $this->createFieldList($fieldNames) . ") VALUES (" . $this->createFieldList($fieldNames, ":") . ")";

		try {
			$stmt = $this->db->prepare($query);
			$stmt->execute($arrFields);			
		} catch (PDOException $e) {
			echo "<center><div style='text-align:left; display: inline-block; margin: 50px auto'><h2>ERROR: No es posible ejecutar la consulta</h2><p> $query</p><h4>Parámetros:</h4><pre>";
			print_r($arrFields);
			echo "</pre></div></center>";
			exit;
		}

		return $this->db->lastInsertId(); // $stmt->rowCount(); // 
		if( $stmt->rowCount()>0 ) return $stmt->rowCount();
		else return $stmt->errorInfo();
	}

	public function searchRecord($arrWhere = null) {
		$results = $this->selectRecords(null, $arrWhere);

		if (count($results) > 0) {
			foreach ($results[0] as $field => $value) {
				if (isset($this->fields[$field])) {
					$this->fields[$field]['Current'] = $value;
				}
			}

			//echo "<pre>"; print_r($this->fields); exit;

			return true;
		}

		return false;
	}

	public function selectRecords($arrFields = null, $arrWhere = null, $arrOrder = null, $startAt = null, $numRecords = null, $fetchStyle = "FETCH_ASSOC") {
		$query = "SELECT " . $this->createFieldList($arrFields) . " FROM $this->tableName";

		$arrExec = array();

		if ($arrWhere) {
			$query .= " WHERE " . $this->createWhere($arrWhere);

			foreach ($arrWhere as $key => $value) {
				$nombre_campo = is_array($value) && isset($value[2]) && !empty($value[2]) ? $value[2] : $key;
				$valor_campo =  is_array($value) && isset($value[1]) && !empty($value[1]) ? $value[1] : $value;
				if(strtolower($valor_campo) != 'null' && $valor_campo !== null) $arrExec[$key] = $valor_campo;
			}
		}
		
		if ($arrOrder) $query .= " ORDER BY " . $this->createFieldList($arrOrder);
		if ($numRecords) $query .= " LIMIT " . intval($numRecords);

		//echo "<pre>";
		//var_dump($query);
		//var_dump($arrWhere);
		//var_dump($arrExec);

		$stmt = $this->db->prepare($query);
		$stmt->execute($arrExec);

		switch ($fetchStyle) {
			case 'FETCH_BOTH': $fetchStyle = PDO::FETCH_BOTH; break; // keys = nombres y numeros de columna
			case 'FETCH_COLUMN': $fetchStyle = PDO::FETCH_COLUMN; break; // datos de la 1ª fila encontrada
			case 'FETCH_NUM': $fetchStyle = PDO::FETCH_NUM; break; // keys = numeros de columna
			case 'FETCH_KEY_PAIR': $fetchStyle = PDO::FETCH_KEY_PAIR; break; // keys = numeros de columna
			default: $fetchStyle = PDO::FETCH_ASSOC; break; // keys = nombres de columna
		}

		return $stmt->fetchAll($fetchStyle);
	}

	public function query($strQuery, $params = []) {
		$stmt = $this->db->prepare($strQuery);
		$stmt->execute($params);
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public function countRecords($arrFields = null, $arrWhere = null, $arrOrder = null, $startAt = null, $numRecords = null, $fetchStyle = "FETCH_ASSOC") {
		$query = "SELECT count(*) FROM $this->tableName";

		$arrExec = array();

		if ($arrWhere) {
			$query .= " WHERE " . $this->createWhere($arrWhere);

			foreach ($arrWhere as $key => $value) {
				$nombre_campo = is_array($value) && isset($value[2]) && !empty($value[2]) ? $value[2] : $key;
				$valor_campo =  is_array($value) && isset($value[1]) && !empty($value[1]) ? $value[1] : $value;
				if(strtolower($valor_campo) != 'null' && $valor_campo !== null) $arrExec[$key] = $valor_campo;
			}
		}

		//echo "<pre>";
		//var_dump($query);
		//var_dump($arrWhere);
		//var_dump($arrExec);

		$stmt = $this->db->prepare($query);
		$stmt->execute($arrExec);

		switch ($fetchStyle) {
			case 'FETCH_BOTH': $fetchStyle = PDO::FETCH_BOTH; break; // keys = nombres y numeros de columna
			case 'FETCH_COLUMN': $fetchStyle = PDO::FETCH_COLUMN; break; // datos de la 1ª fila encontrada
			case 'FETCH_NUM': $fetchStyle = PDO::FETCH_NUM; break; // keys = numeros de columna
			case 'FETCH_KEY_PAIR': $fetchStyle = PDO::FETCH_KEY_PAIR; break; // keys = numeros de columna
			default: $fetchStyle = PDO::FETCH_ASSOC; break; // keys = nombres de columna
		}

		$result=$stmt->fetch($fetchStyle);
		return $result? reset($result) : 0;
	}

	public function updateRecord($arrFields, $arrWhere) {
		$arrExec =  [];

		if (empty($arrFields)) return 0;

		foreach ($arrFields as $key => $value) {
			$this->checkField($key, $value);
			if (strtolower($value) !== 'null' && $value !== null) $arrExec["SET$key"] = $value;
		}
		foreach ($arrWhere as $key => $value) {
			$nombre_campo = is_array($value) && isset($value[2]) && !empty($value[2]) ? $value[2] : $key;
			$valor_campo =  is_array($value) && isset($value[1]) && !empty($value[1]) ? $value[1] : $value;
			if(strtolower($valor_campo) != 'null' && $valor_campo !== null) $arrExec[$key] = $valor_campo;
		}

		$query = "UPDATE $this->tableName SET " . $this->createFieldAssignments($arrFields, false) . " WHERE " . $this->createWhere($arrWhere);

		// echo $query; echo "<br/><br/>"; var_dump($arrExec); 
		$stmt = $this->db->prepare($query);
		$stmt->execute($arrExec);

		return $stmt->rowCount();
		//return $stmt->rowCount()? true: $this->getError($stmt, $query, $arrExec);
		//return $this->getError($stmt, $query, $arrExec);
	}

	public function getError($stmt, $query, $arrFields ){
		if($stmt->rowCount()>0) return $stmt->rowCount();
		$outData = $stmt->errorInfo();
		$outData["query"] = $query;
		$outData["fields"] = $arrFields;
		//var_dump($outData); exit;
		return $outData;

	}

	public function getTableKey() {
		return $this->tableKey;
	}

	public function getTableName() {
		return $this->tableName;
	}
}