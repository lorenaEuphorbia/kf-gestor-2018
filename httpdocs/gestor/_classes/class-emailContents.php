<?php

class EmailContents {
	function __construct() {}
	
	function getContents($option = "", $lang = "es", $to = "", $params = []) {
		$base_path = URL_BASE;
		$subject = COMPANY_NAME;
		$domain_name = MAIN_DOMAIN;
		$link_color = "#FF8200";
		
		/// HEADER
		$body = '<html xmlns="http://www.w3.org/1BBB/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>'.$subject.'</title>
</head>
<body style="background: #FFF; color: #000; font: 10pt arial, sans-serif; margin-top:0px">
	<table align="center" border="0" cellpadding="0" cellspacing="0" width="650" style="margin-top: 20px;">
		<tr height="100" valign="top">
			<td style="border-bottom: 1px solid #000;">
				<a href="'.$base_path.'" target="_blank" style="color:' .  $link_color . ';"><img src="'.$base_path.'/img/common/mail-logo.png" border="0" height="80" alt="'.$subject.'" /></a>
			</td>
		</tr>
		<tr valign="top">
			<td style="border-bottom: 1px solid #000; font: 9pt Arial, Sans-serif; padding-bottom:30px; padding-top: 20px;" >';
		
			$body .= "Hola" . ($to != "" ? " $to" : "") . ":<br /><br />";
		
			// CUERPO Y OPCIONES DEL CORREO
			switch ($option) {
				case 'recoverPassword': // RECUPERAR LA CONTRASEÑA
					$subject = COMPANY_NAME . " :: Restablecer Contraseña";
					
					$body .='
				Para volver a establecer tu contraseña de <a href="' . $base_path . '" target="_blank" style="color:' .  $link_color . ';">' . $domain_name . '</a> haz click en el siguiente enlace:<br /><br />
				 <div style="margin-left: 10px;">
					<!--[if mso]>
						<v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="' . $base_path . '/restablecer-contrasena/' . $params[0] . '/' . $params[1] . '/" style="height:36px;v-text-anchor:middle;width:240px;" arcsize="12%" strokecolor="' .  $link_color . '" fillcolor="' .  $link_color . '">
							<w:anchorlock/>
							<center style="color:#ffffff;font-family:Arial,sans-serif;font-size:14px;">RESTAURAR CONTRASEÑA</center>
						</v:roundrect>
					<![endif]-->
					<a href="' . $base_path . '/restablecer-contrasena/' . $params[0] . '/' . $params[1] . '/" style="background-color:' .  $link_color . ';border:1px solid ' .  $link_color . ';border-radius:4px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:14px;line-height:36px;text-align:center;text-decoration:none;width:240px;-webkit-text-size-adjust:none;mso-hide:all;">RECUPERAR CONTRASEÑA</a>
				</div>
				<br />
				Si no has solicitado un restablecimiento de contraseña, puedes simplemente eliminar este mensaje.<br /><br />
				Y si necesitas ayuda adicional, ponte en contacto con nosotros, por favor.';
				break;
			}
			
			/// FOOTER
			$body .= '
				<br /><br />Saludos cordiales
			</td>
		</tr>
		<tr>
			<td style="color:#AAAAAA;font:7pt Arial, Sans-serif;padding-bottom: 20px; padding-right:10px; padding-top: 30px;">
				Advertencia legal:<br />
				Este mensaje se dirige en exclusiva a su destinatario y puede contener información privilegiada o confidencial. 
				Si no es vd. el destinatario indicado, queda notificado que la utilización, divulgación y/o copia sin autorización está 
				prohibida en virtud de la legislación vigente. Si ha recibido este mensaje por error, le rogamos que nos lo comunique 
				inmediatamente por esta misma vía o proceda a su destrucción.
			</td>
		</tr>
	</table>
</body>
</html>';
		
		return [$subject, $body];
	}
}