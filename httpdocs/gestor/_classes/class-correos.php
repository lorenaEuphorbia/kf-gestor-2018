<?php
    include_once("phpmailer/class.phpmailer.php");

    /***************************************************************
     *                       CLASE: Correos                        *
     ***************************************************************/
    /**
     * Gestión del envío de correos.
     *
     * Gestión del sistema de envío de correos electrónicos.
     *
     * @package     EUPHORBIA
     * @subpackage  BaseDatos
     * @author      Pablo Enjuto Martín <cesar@euphorbia.es>
     * @copyright   Copyright 2014, Euphorbia Comunicación, S.L.L.
     * @access      public
     * @version     3.00 - Final
     * @link        http://www.euphorbia.es Euphorbia Comunicación, S.L.L.
     *
     */

class Correos {

	const NUMERO_CUENTA     = "ES 88 0049 5395 91 2316223392";
    public $SERVER          =  URL_BASE;
    public $TELEFONO        = '983 79 70 70';
    public $ATENCION        = 'Atención al cliente:';
    public $FACEBOOK        = '';
    public $TWITTER         = '';
    public $GOOGLE          = '';
    public $INSTAGRAM       = '';
    public $HORARIO         = 'Horario de lunes a viernes: 00:00 -00:00 | 00:00 - 00:00';
    public $ENVIOS          = '¡ENVÍOS EN 00 HORAS!';
    public $EMAIL           = 'consultas@' . MAIN_DOMAIN;
    public $HOST            = MAIN_DOMAIN;
    public $TXTFOOTER       = 'ENVÍOS GRATIS A PARTIR DE 00 EUROS Y EN 00 HORAS';
    public $CONTACTO        = '';

    public $FROM_EMAIL       = '';
    public $FROM_NAME        = '';

    private $b;
    private $arr_brands = [];
    private $arr_colors = [];

    private $TITULO;

	function __construct($_objClsBase, $remite=[]) {
        $this->b = $_objClsBase;

        $this->preferences = $_objClsBase->preferences;
        $this->TITULO = $this->preferences->getValue("general_titulo_web");
        $this->CONTACTO = $this->preferences->getValue("contacto_direccion");
        $this->FACEBOOK = $this->preferences->getValue("contacto_facebook");
        $this->TWITTER = $this->preferences->getValue("contacto_twitter");
        $this->INSTAGRAM = $this->preferences->getValue("contacto_instagram");

        if(empty($remite)) {
            $this->FROM_EMAIL = $this->preferences->getValue("forms_email");
            $this->FROM_NAME = $this->preferences->getValue("forms_nombre");
        }
        else {
            $this->FROM_EMAIL = $remite[0];
            $this->FROM_NAME = $remite[1];
        }

        $this->SERVER = rtrim($this->SERVER, '/');
        $this->SERVER = rtrim($this->SERVER, '/');
    }

	function send($asunto, $texto, $destino, $copiaCC = array(), $copiaBCC = array()) {

		$correoClase = new PHPMailer();

		$correoClase->ClearAllRecipients();
		$correoClase->ClearAttachments();
		$correoClase->ClearCustomHeaders();

        $correoClase->isSMTP();
        $correoClase->Host = $this->preferences->getValue('forms_smtp_server');
        $correoClase->SMTPAuth = boolval($this->preferences->getValue('forms_smtp_auth'));
        $correoClase->Username = $this->preferences->getValue('forms_smtp_user');
        $correoClase->Password = $this->preferences->getValue('forms_smtp_password');
        $correoClase->SMTPSecure = 'tls';
        $correoClase->Port = intval($this->preferences->getValue('forms_smtp_port'));
        $correoClase->Timeout = 30;
        //$correoClase->SMTPDebug = 1;

        $correoClase->setFrom($this->FROM_EMAIL, $this->FROM_NAME);
        $correoClase->addReplyTo($this->preferences->getValue('contacto_email'), $this->preferences->getValue('contacto_nombre'));

        if(is_array($destino))
            $correoClase->AddAddress($destino[0], $destino[1]);
        else
            $correoClase->AddAddress($destino);

        if(!empty($copiaCC)) 
            foreach ($copiaCC as $destinatario)
                if(is_array($destinatario))
                    $correoClase->AddCC($destinatario[0], $destinatario[1]);
                else
                    $correoClase->AddCC($destinatario);


        if(!empty($copiaBCC)) 
            foreach ($copiaBCC as $destinatario) 
                if(is_array($destinatario))
                    $correoClase->AddBCC($destinatario[0], $destinatario[1]);
                else
                    $correoClase->AddBCC($destinatario);
            
        
        $correoClase->IsHTML(true);
        $correoClase->CharSet = 'UTF-8';
		$correoClase->Subject = $asunto;
        $correoClase->Body = $texto;

		if($correoClase->Send()) return true;

        trigger_error("No es posible enviar el email".$correoClase->ErrorInfo);
        return false;

	}

    /**
     * Método que envía correo de una devolución
     * @param  array() $arrParamsDevolucion Asociativo de los datos recogidos de Dimoni. Estas son sus claves: 
     *                                      RefNPedidoWeb, Articulo, Cantidad, Talla, MotivoDev,      
     * @return boolean true si se envió correctamente
     */
    function sendDevolucion ($cliente, $arrParamsDevolucion) {


        $titulo = 'Devolución Tramitada!';
        $destino = [ $cliente['email'], "$cliente[nombre] $cliente[apellidos]"];

        $arrDatos = [];
        $arrDatos[] =[
            'tipo'=>'h3',
            'texto'=>"Hola $cliente[nombre]",
        ];
        $arrDatos[] = [
                'tipo'=>'p',
                'texto'=>'Queremos informarle que su petición de devolución se ha tramitado correctamente. Los artículos tienen que estar en perfecto estado y con su etiqueta en caso de presentarla. Deberás tener el artículo debidamente envalado para que el transportista pueda pasar a recogerlo.',
        ];
        $arrDatos[] = [
                'tipo'=>'h2',
                'texto'=>'DEVOLUCIÓN TRAMITADA',
        ];
        $arrDatos[] = [
                'tipo'=>'p',
                'texto'=>'Tras verificar que el artículo está en perfecto estado y que los datos aportados coinciden, se prodecerá el reembolso del importe de la devolución.',
        ];
        $arrDatos[] = [
                'tipo'=>'h4',
                'texto'=>'¿Cuándo recibiré el reembolso?',
        ];
        $arrDatos[] = [
                'tipo'=>'p',
                'texto'=>'El tiempo hasta recibir el ingreso depende de la forma de pago empleada pudiendo variar de 1-3 días. En el caso del pago con PayPal recibirás un correo en el momento de la devolución, el tiempo transcurrido hasta su reembolso depende exclusivamente de Paypal.',
        ];
        $arrDatos[] = [
                'tipo'=>'h4',
                'texto'=>'Coste adiccional',
        ];
        $arrDatos[] = [
                'tipo'=>'p',
                'texto'=>'Todas las devoluciones tienen un coste de 3,90€ para la península, en otros casos están sujetas a las mismas tarifas de envío. En caso de tratarse de un cambio, el usuario tambien deberá asumir el coste de transporte del nuevo artículo hasta su dirección.',
        ];
        $arrDatos[] = [
                'tipo'=>'h4',
                'texto'=>'Datos de tu devolución',
        ];
        $arrDatos[] = [
                'tipo'=>'table',
                'rows'=>[
                    'Pedido Nº'     => $arrParamsDevolucion['id_pedido'],
                    'Devolución Nº' => $arrParamsDevolucion['id'],
                    'Gastos de envío'     => $arrParamsDevolucion['pvp_envio'],
                    'Artículos'     => count($arrParamsDevolucion['articulos']),
                ],
        ]; 
        $arrDatos[] = [
                'tipo'=>'h4',
                'texto'=>'Datos de recogida',
        ];
        $arrDatos[] = [
                'tipo'=>'table',
                'rows'=>[
                    'Nombre'        => $arrParamsDevolucion['nombre'],
                    'Dirección'     => $arrParamsDevolucion['direccion'],
                    'Localidad'     => $arrParamsDevolucion['localidad'],
                    'Código Postal' => $arrParamsDevolucion['codigo_postal'],
                    'Pais'          => $arrParamsDevolucion['nombre_pais'],
                    'Provincia'     => $arrParamsDevolucion['nombre_provincia'],
                ],
        ]; 
        foreach ( $arrParamsDevolucion['articulos'] as $index => $row) {
            $arrDatos[] = [
                    'tipo'=>'h4',
                    'texto'=>'Artículo 1: Referencia '.$row['Articulo'],
            ];  
            $arrDatos[] = [
                    'tipo'=>'table',
                    'rows'=>[
                        'Unidades'   => $row['Cantidad'],
                        'Talla'      => $row['Talla'],
                        'Importe'     => $row['Importe'],
                        'Motivo'     => $row['MotivoDev'],
                    ],
            ]; 
        }

        $texto = $this->getEmailPedido($titulo, $arrDatos);
        //echo $texto;
        return $this->send($titulo, $texto, $destino);
    }


    /**
     * Método que envía correo de Quiero que me lo regalen
     * @param  array() $arrParams Asociativo de los datos: 
     *                                      nombre, email, nombre-amigo, email-amigo,      
     * @return boolean true si se envió correctamente
     */
    function sendSolicitudRegalo ($arrParams) {

        $titulo = $arrParams['nombre'].' quiere un regalo';
        $destino = [ $arrParams['email-amigo'], $arrParams['nombre-amigo']];

        $arrDatos = [];
        $arrDatos[] =[
            'tipo'=>'h3',
            'texto'=>'Hola '.$arrParams['nombre-amigo'],
        ];
        $arrDatos[] = [
                'tipo'=>'p',
                'texto'=>'Recibes esta notificación porque a '.$arrParams['nombre'].' le ha gustado el artículo <a href="'.$this->SERVER.'/'.$arrParams['articulo']['Url'].'.html" target="_blank" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;color:#000000;" class="hover">'.$arrParams['articulo']['DescripcionWeb'].'</a>.',
        ];
        $arrDatos[] = [
                'tipo'=>'h4',
                'texto'=>'¿Quién quiere este artículo?',
        ];
        $arrDatos[] = [
                'tipo'=>'table',
                'rows'=>[
                    'Nombre del amigo'=>$arrParams['nombre'],
                    'Email del amigo'=> '<a href="mailto:'.$arrParams['email'].'" target="_blank" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;color:#000000;" class="hover">'.$arrParams['email'].'</a>',
                ],
        ];

        $texto = $this->getEmailPedido($titulo, $arrDatos);
        //echo $texto;
        return $this->send($titulo, $texto, $destino);
    }

    function sendOrderByID($orderId) {
        $errors = false;

        $orderData = $this->b->orders->getRecord ('Orders', null, ['id'=>$orderId]);
        $arrProducts = $this->b->orders->getList ('Articulos', null, ['id_order'=>$orderId]);

        foreach ($arrProducts as &$rowProduct) {
           $result = $this->b->productos->getProductoById($rowProduct['articulo']);
           if ($result) {
             $rowProduct += $result;
           }
           else $errors = true;
        }

        if(!empty($orderData) && !empty($arrProducts) && $errors===false) {
            return $this->sendOrder($orderData,$arrProducts);
        }

        trigger_error('Faltan parámetros del correo. Contacte con el administrador del website, por favor.');
        return false;
    }

    function sendOrder($orderData,$arrProducts) {

        $titulo = 'Tu pedido está en camino!';
        $destino = [ $orderData['email'], "$orderData[nombre] $orderData[apellidos]"];

        $arrDatos = $arrDatosProductos = [];
        $arrDatos[] =[
            'tipo'=>'h3',
            'texto'=>"Hola $orderData[nombre]",
        ];
        $arrDatos[] = [
                'tipo'=>'p',
                'texto'=>$this->getTextoModalidad($orderData['formaDePago']),
        ];
        $arrDatos[] = [
                'tipo'=>'h2',
                'texto'=>'PEDIDO REALIZADO',
        ];
        $arrDatos[] = [
                'tipo'=>'h4',
                'texto'=>'¿Dónde lo enviaremos?',
        ];
        $arrDatos[] = [
                'tipo'=>'table',
                'rows'=>[
                    'Nombre'=>$orderData['nombre'],
                    'Dirección'=>$orderData['direccion_dir_envio'],
                    'Localidad'=>"$orderData[cp_dir_envio] $orderData[localidad_dir_envio]",
                ],
        ];
        if($orderData['is_factura']){
            $arrDatos[] = [
                    'tipo'=>'h4',
                    'texto'=>'Datos de tu factura',
            ];
            $arrDatos[] = [
                    'tipo'=>'table',
                    'rows'=>[
                        'Nombre'=>$orderData['nombre_dir_facturacion'],
                        'Fecha'=>$orderData['modification_date'],
                        'NIF/CIF'=>$orderData['nif_dir_facturacion'],
                        'Dirección'=>$orderData['direccion_dir_facturacion'],
                        'Localidad:'=>"$orderData[cp_dir_envio] $orderData[localidad_dir_envio]",
                    ],
            ];
        }
        $arrDatos[] = [
                'tipo'=>'h4',
                'texto'=>'Resumen de la compra:',
        ];
        $arrDatos[] = [
                'tipo'=>'table',
                'rows'=>[
                    'Pedido Nº '=>$orderData['id'],
                    'Método de pago'=>$orderData['formaDePago'],
                    'Importe '=>number_format($orderData['pvp_total'],2). ' €',
                    'Gastos de Envío '=>number_format($orderData['envioImporte'],2). ' €',
                    'Importe TOTAL '=>number_format($orderData['pvp_total']+$orderData['envioImporte'],2). ' €',
                    'Peso del paquete '=>number_format($orderData['peso_total']/100,1). ' kg',
                    //'Puntos gastados'=>$arrRequestData['pvp_puntos'],
                ],
        ];
        $arrDatos[] = [
                'tipo'=>'h4',
                'texto'=>'¿Cuando llega mi pedido?',
        ];
        $arrDatos[] = [
                'tipo'=>'p',
                'texto'=>'Tán pronto cómo recibamos el pago realizaremos el envío, recibirás un correo cuando el pedido salga de nuestros almacenes. Ten en cuenta que sólo se tramitarán pedidos en horario comercial de Lunes a Viernes, para más información puedes consultar el <a href="'.$this->SERVER.'/es/aviso-legal/" target="_blank" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;color:#000000;" class="hover">condiciones de uso</a> en nuestra web.',
        ];

        foreach ($arrProducts as $codigo => $row) {
            //var_dump($row); exit;
                $arrDatosProductos[] = [
                    'ruta'      => str_replace('\\', '/', $row['imagen']),
                    'url'       => $row['url'],
                    'titulo'    => $row['nombre'],
                    'table'     => [                    
                        'Referencia'        => str_pad($row['articulo'], 5, "0", STR_PAD_LEFT),
                        'Unidades'          => $row['unidades'],
                        'PVP'               => $row['precio'] . ' €',
                    ],
                ];
        }

        $texto = $this->getEmailPedido($titulo, $arrDatos, $arrDatosProductos);
        //echo $texto;
        return $this->send($titulo, $texto, $destino);
    }

    function sendContacto($datos) {

        $titulo = 'Contacto ' . COMPANY_NAME . '!';
        $destino = [$this->preferences->getValue('contacto_email'), $this->preferences->getValue('contacto_nombre')];

        $arrDatos = $arrDatosProductos = [];
        $arrDatos[] =[
            'tipo'=>'h3',
            'texto'=>"Hola",
        ];
        $arrDatos[] = [
                'tipo'=>'p',
                'texto'=>'Quieren contactar contigo. Han rellenado el fomulario de contacto via web.'
        ];
        $arrDatos[] = [
                'tipo'=>'h2',
                'texto'=>'FORMULARIO DE CONTCTO',
        ];
        $arrDatos[] = [
                'tipo'=>'h4',
                'texto'=>'Datos de la persona:',
        ];
        $arrDatos[] = [
                'tipo'=>'table',
                'rows'=>$datos,
        ];
        $arrDatos[] = [
                'tipo'=>'p',
                'texto'=>'Contacte con el usuario web tán pronto como sea posible, un saludo.',
        ];


        $texto = $this->getEmailPedido($titulo, $arrDatos);
        //echo $texto;
        return $this->send($titulo, $texto, $destino);
    }

    function sendRecoverPassword ($cliente) {
        $password_requests = new PasswordRequests($this->b);

        /// Guardamos la nueva petición
        $request_token = $password_requests->setRequest($cliente['email']);

        $titulo = 'Reestablece tu contrase&nacute;a!';
        $destino = [ $cliente['email'], "$cliente[nombre] $cliente[apellidos]"];
        $rutaLink = $this->SERVER . '/restablecer-contrasena/' . $cliente['email'] . '/' . $request_token . '/';

        $arrDatos[] =[
            'tipo'=>'h3',
            'texto'=>"Hola $cliente[nombre]",
        ];
        $arrDatos[] = [
                'tipo'=>'p',
                'texto'=>'Para volver a establecer tu contrase&nacute;a de <a href="' . $this->SERVER . '" target="_blank" style="color:#4cae4c;">' . $this->HOST . '</a> haz click en el siguiente enlace:',
        ];
        $arrDatos[] = [
                'tipo'=>'a',
                'texto'=>'RECUPERAR CONTRASE&Ntilde;A',
                'link'=>$rutaLink,
        ];
        $arrDatos[] = [
                'tipo'=>'p',
                'texto'=>'Si no has solicitado un restablecimiento de contraseña, puedes simplemente eliminar este mensaje.<br/>Y si necesitas ayuda adicional, ponte en contacto con nosotros, por favor.',
        ];   

        $texto = $this->getEmailPedido($titulo, $arrDatos);
        //para imprimir el correo que se va a enviar
       // echo $texto; exit;
        return $this->send($titulo, $texto, $destino);
    }


    function getEmailPedido($titulo, $arrDatos, $arrProducts=[]) {
        ob_start();
        $this->startPrint($titulo);
        $this->printHeader();
        $this->printBody($arrDatos);
        if($arrProducts)
        $this->printProducts($arrProducts);
        $this->printFooter();
        $this->endPrint();
        return ob_get_clean();
    }

    function getTextoModalidad($strModalidad) {
        switch ($strModalidad) {
            case 'paypal':
                return 'Tu pedido se ha realizado correctamente mediante paypal.<br/>En caso de efectuar una devolución el tiempo desde que se retrocede la operación hasta que PayPal te ingresa el dinero depende exclusivamente de PayPal y el tipo de pago que utilieces en tu cuenta pudiendo variar de a 5 a 30 días.';
                break;

            case 'tarjeta':
                return 'Tu pedido se ha realizado correctamente a través del banco.<br/>Tendrás el pedido en 48 horas una vez que salga de nuestros almacenes.';
                break;

            case 'contrareembolso':
                return 'Tu pedido se ha realizado correctamente, deberás disponer en efectivo la cantidad que detallamos a continuación.<br/>Tendrás el pedido en 48 horas una vez que salga de nuestros almacenes.';
                break;

            case 'transferencia':
                return 'Ya queda poco, deberás abonar en la cuanta '.self::NUMERO_CUENTA.' la cantidad que se detalla a continuación.<br/>Una vez tramitada la transferencia, tendrás el pedido en un plazo de 48 horas desde que este salga de nuestros almacenes.';
                break;
        }
        return '';
    }

	function startPrint ($title) { ?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="author" content="Euphorbia SL, http://www.euphorbia.es">
            <title> <?= $title ?> | <?= $this->TITULO ?>  </title>
            <base href="<?= $this->SERVER ?>/" target="_blank">
            <style type="text/css">

                a.hover:hover {
                    color: #45b1e1 !important;
                }
                @media only screen and (max-width: 480px){

                body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;}
                body{width:100% !important; min-width:100% !important;}

                #bodyCell{
                    padding-top:0px !important;
                    padding-right:0px !important;
                    padding-left:0px !important;
                    padding-top:0px !important;
                }
                #templateContainer{
                    max-width:600px !important;
                     width:100% !important;
                }
                .mediaHide{display:none !important;}
                #headerImage{
                    height:auto !important;
                     max-width:600px !important;
                     width:100% !important;
                }
                .headerContent{
                     font-size:18px !important;
                     line-height:125% !important;
                }
                .bodyContent{
                     line-height:135% !important;
                }
                .templateColumnContainer{display:block !important; width:100% !important; max-width: 100% !important;}
                .tableColumnContainer{width:100% !important; max-width: 100% !important; }
                .columnImage{
                    height:auto !important;
                     max-width:480px !important;
                     width:100% !important;
                }
                .columnTable{
                     padding-bottom:2px !important;

                }
                .leftColumnContent{
                     font-size:12px !important;
                     line-height:125% !important;
                }
                .centerColumnContent{
                     font-size:12px !important;
                     line-height:125% !important;
                }
                .rightColumnContent{
                     font-size:12px !important;
                     line-height:125% !important;
                }
                .footerContent{
                     font-size:12px !important;
                     line-height:130% !important;
                }
                .footerLink{
                     display:block !important;
                }
            }
        </style>
        </head>
        <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;height:100% !important;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;width:100% !important;background-color:#FFFFFF;" >
        <center>
        <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;height:100% !important;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;width:100% !important;background-color:#FFFFFF;" >
        <tr>
            <td align="center" valign="top" id="bodyCell" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;height:100% !important;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-bottom:0;width:100% !important;padding-top:0px;padding-right:20px;padding-left:20px;border-top-width:4px;border-top-style:solid;border-top-color:#45b1e1;" >
                <!-- BEGIN TEMPLATE // -->
                <table border="0" cellpadding="0" cellspacing="0" id="templateContainer" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;width:600px;border-width:1px;border-style:solid;border-color:#FFFFFF;" >
    <?php }


    function printHeader($strRutaImagen='') { ?>
                <!-- BEGIN PREHEADER // -->
                <tr>
                    <td align="center" valign="top" style="padding-top:20px;padding-bottom:0;padding-right:10px;padding-left:10px;box-sizing:border-box;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;" >
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templatePreheader" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;background-color:#FFFFFF;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#000000;" >
                            <tr >
                                <td valign="middle" class="preheaderContent"  mc:edit="preheader_content00" style="padding-top:20px;padding-right:0px;padding-bottom:20px;padding-left:0px;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#808080;font-family:'PT Sans',sans-serif;font-size:8pt;line-height:125%;text-align:left;" >
                                    <a href="<?= $this->SERVER ?>/" target="_blank" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;color:#606060;font-weight:normal;text-decoration:underline;" ><img src="<?= $this->SERVER ?>/img/comunes/logotipo.png" width="150px" style="max-height:60px !important;-ms-interpolation-mode:bicubic;border-width:0;height:auto;line-height:100%;outline-style:none;text-decoration:none;" /></a><br/>
                                </td>
                                </td>
                                <!-- *|IFNOT:ARCHIVE_PAGE|* -->
                                <td valign="bottom" width="" class="preheaderContent mediaHide"  mc:edit="preheader_content02" style="padding-top:0px;padding-right:0px;padding-bottom:20px;padding-left:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#000000;font-family:'PT Sans',sans-serif;font-size:8pt;line-height:125%;text-align:left;" >
                                <span style="color:#000000;text-align:right !important;text-transform:uppercase;display:block;padding-top:5px;padding-bottom:5px;margin-left:30px;font-family:'PT Sans',sans-serif;font-size:8pt;line-height:125%;" ><?= $this->CONTACTO ?></span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- // END PREHEADER -->

                <?php if($strRutaImagen): ?>
                <!-- BEGIN HEADER // -->
                <tr>
                    <td align="center" valign="top" style="padding-top:0;padding-bottom:0;padding-right:10px;padding-left:10px;box-sizing:border-box;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;" >
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;background-color:#FFFFFF;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#000000;" >
                            <tr>
                                <td valign="top" class="headerContent" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#505050;font-family:'PT Sans',sans-serif;font-size:16px;font-weight:bold;line-height:100%;padding-right:0;padding-left:0;text-align:left;vertical-align:middle;padding-top:20px;padding-bottom:20px;" >
                                    <img src="<?= $this->SERVER ?>/imagen<?= $strRutaImagen ?>" alt="Banner decorativo" id="headerImage" mc:label="header_image" mc:edit="header_image" mc:allowdesigner mc:allowtext style="max-width:600px;-ms-interpolation-mode:bicubic;border-width:0;line-height:100%;outline-style:none;text-decoration:none;height:auto;" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- // END HEADER -->
                <?php endif ?>
    <?php }


    function printBody($arrDatos = []) { ?>
                <!-- BEGIN BODY // -->
                <tr>
                    <td align="center" valign="top" style="padding-top:0;padding-bottom:0;padding-right:10px;padding-left:10px;box-sizing:border-box;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;" >
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;background-color:#FFFFF;border-top-width:1px;border-top-style:solid;border-top-color:#FFFFFF;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#000000;" >
                            <tr>
                                <td valign="top" class="bodyContent" mc:edit="body_content" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#505050;font-family:'PT Sans',sans-serif;font-size:9pt;letter-spacing:0.01em;line-height:150%;padding-top:20px;padding-right:0px;padding-bottom:10px;padding-left:0px;text-align:left;" >
                                <?php foreach ($arrDatos as $index => $rowData){ ?>
                                    <?php switch ($rowData['tipo']) {

                                        case 'h2': ?>
                                            <h2 class="lead" style="color:#404040 !important;display:block;font-family:'PT Sans',sans-serif;font-size:16px;font-style:normal;font-weight:bold;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left;text-transform:uppercase;" ><?= $rowData['texto'] ?></h2>
                                            <?php break;

                                        case 'h3': ?>
                                            <h3 style="color:#606060 !important;display:block;font-family:'PT Sans',sans-serif;font-size:14px;font-style:normal;font-weight:normal;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left;" ><?= $rowData['texto'] ?></h3>
                                            <?php break;

                                        case 'h4': ?>
                                            <h4 style="color:#808080 !important;display:block;font-family:'PT Sans',sans-serif;font-size:9pt;font-style:italic;font-weight:normal;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left;" ><?= $rowData['texto'] ?></h4>
                                            <?php break;

                                        case 'a' ?>
                                            <a href="<?= $rowData['link'] ?>" alt="<?= $rowData['texto'] ?>" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#95BD4C;color:#ffffff;display:block;text-decoration:none;width:240px !important; max-width: 95% !important; font-family:'Arial',sans-serif;font-size:11pt;font-style:normal;font-weight:normal;letter-spacing:0.01em; margin-bottom: 20px; margin-top: 20px; margin-left: auto; margin-right: auto; padding: 8px 0;text-align:center;" ><?= $rowData['texto'] ?></a>
                                            <?php break;

                                        case 'p': ?>
                                            <p style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;color:#000000 !important;font-family:'PT Sans',sans-serif;font-size:9pt;font-style:normal;font-weight:normal;letter-spacing:0.01em;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left;" ><?= $rowData['texto'] ?></p>
                                            <?php break;

                                        case 'table': ?>
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="column" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;margin-bottom:10px;" >
                                            <?php foreach ($rowData['rows'] as $clave => $valor){ ?>
                                                <tr>
                                                    <td width="40%" valign="top"  class="columnTable" style="padding-right:20px;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#000000 !important;font-family:'PT Sans',sans-serif;font-size:11pt;font-style:normal;font-weight:normal;letter-spacing:0.01em;" ><?= $clave ?></td>
                                                    <td width="60%" align="" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#000000 !important;font-family:'PT Sans',sans-serif;font-size:11pt;font-style:normal;font-weight:normal;letter-spacing:0.01em;" ><?= $valor ?></td>
                                                </tr>
                                            <?php } ?>
                                            </table>
                                        <?php break;

                                        default: ?>
                                            <?= $rowData['texto'] ?>
                                            <?php break;
                                    } ?>
                                <?php } ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- // END BODY -->
    <?php }


    function printProducts($arrProducts=[]) { ?>
                <!-- BEGIN COLUMNS // -->
                <tr>
                    <td align="left" valign="top" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt; padding-top: 10px !important; padding-bottom: 10px !important; " >
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateColumns" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;background-color:#FFFFFFF;" >
                            <tr mc:repeatable>
                            <?php foreach ($arrProducts as $index => $rowData) {
                                $mod = $index%3;
                                switch ($mod) {
                                    case 0: $align = 'left';
                                        break;
                                    case 1: $align = 'center';
                                        break;
                                    case 2: $align = 'right';
                                        break;
                                } ?>
                                <?php if($index && $mod==0) { ?></tr><tr mc:repeatable><?php } ?>
                                <!-- COLUMN // -->
                                <td align="<?= $align ?>" valign="top" class="templateColumnContainer" style="padding-top:0px;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt; width:180px; max-width:180px;" >
                                    <table border="0" cellpadding="20" cellspacing="0" width="100%" class="tableColumnContainer"  style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important; width:180px;max-width:180px;" >
                                        <tr>
                                            <td class="<?= $align ?>ColumnContent" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#000000;font-family:'PT Sans',sans-serif;font-size:9pt;line-height:150%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;width:100%;text-align:left;" >
                                                <img src="<?= $this->SERVER ?><?= $rowData['ruta'] ?>" alt="<?= $rowData['titulo'] ?>" title="<?= $rowData['titulo'] ?>" class="columnImage" mc:label="left_column_image" mc:edit="left_column_image" style="max-width:172px;-ms-interpolation-mode:bicubic;border-width:0;line-height:100%;outline-style:none;text-decoration:none;border-top-width:1px;border-top-style:solid;border-top-color:#000000;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#000000;border-right-width:1px;border-right-style:solid;border-right-color:#000000;border-left-width:1px;border-left-style:solid;border-left-color:#000000;display:inline;height:auto;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="<?= $align ?>ColumnContent" mc:edit="<?= $align ?>_column_content" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#000000;font-family:'PT Sans',sans-serif;font-size:9pt;line-height:150%;padding-top:0;padding-right:10px;padding-bottom:0px;padding-left:10px;width:100%;text-align:left;" >
                                                <h3 style="color:#606060 !important;display:block;font-family:'PT Sans',sans-serif;font-style:normal;font-weight:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left;font-size:18px;line-height:125% !important;text-decoration:none !important;" ><a href="<?= $this->SERVER ?>/es/tienda-gran-seleccion/" title="<?= $rowData['titulo'] ?>" target="_blank" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;display:block !important;font-size:15px;line-height:125% !important;color:#000000;font-weight:normal;text-decoration:none !important;" class="hover"><?= $rowData['titulo'] ?></a></h3>
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="column" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;margin-bottom:10px;" >
                                                <?php foreach($rowData['table'] as $clave => $valor){ ?>
                                                    <tr>
                                                        <td valign="top"  class="columnTable" style="padding-right:20px;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#000000 !important;font-family:'PT Sans',sans-serif;font-size:11pt;font-style:normal;font-weight:normal;letter-spacing:0.01em;" ><?= $clave ?></td>
                                                        <td align="" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#000000 !important;font-family:'PT Sans',sans-serif;font-size:11pt;font-style:normal;font-weight:normal;letter-spacing:0.01em;" ><?= $valor ?></td>
                                                    </tr>
                                                <?php } ?>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            <?php } ?>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- // END COLUMNS -->
    <?php }


    function printFooter() { ?>
                <tr>
                    <td align="center" valign="top" style="padding-top:0;padding-bottom:0;padding-right:10px;padding-left:10px;box-sizing:border-box;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;" >
                        <!-- BEGIN FOOTER // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;background-color:#FAFAFA;border-top-width:1px;border-top-style:solid;border-top-color:#FAFAFA;" >
                            <tr>
                                <td valign="top" class="footerContent footerSocial" mc:edit="footer_content00" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#000000;font-family:'PT Sans',sans-serif;font-size:8pt;line-height:150%;padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px;text-align:center;" >
                                    <a href="https://www.facebook.com/<?= $this->FACEBOOK ?>" target="_blank" alt="Facebook" style="-ms-interpolation-mode:bicubic;border-width:0;height:auto;line-height:100%;outline-style:none;text-decoration:none;"><img src="<?= $this->SERVER ?>/img/comunes/facebook.png" alt="facebook"></a>&nbsp;&nbsp;&nbsp;
                                    <a href="https://twitter.com/<?= $this->TWITTER ?>" target="_blank" alt="Twitter" style="-ms-interpolation-mode:bicubic;border-width:0;height:auto;line-height:100%;outline-style:none;text-decoration:none;"><img src="<?= $this->SERVER ?>/img/comunes/twitter.png" alt="twitter"></a>&nbsp;&nbsp;&nbsp;
                                    <a href="https://www.instagram.com/<?= $this->INSTAGRAM ?>" target="_blank" alt="Google+" style="-ms-interpolation-mode:bicubic;border-width:0;height:auto;line-height:100%;outline-style:none;text-decoration:none;"><img src="<?= $this->SERVER ?>/img/comunes/instagram.png" alt="instagram"></a>
                                 </td>
                            </tr>
                            <tr>
                                <td valign="top" class="footerContent footerBox" mc:edit="footer_content00" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#000000;font-family:'PT Sans',sans-serif;font-size:8pt;line-height:150%;text-align:center;padding-top:0px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >
                                    <em>
                                         &copy; 2016 - <?= date('Y') ?> <a href="<?= $this->SERVER ?>/" target="_blank" style="text-decoration:none !important;color: #000000 !important;"><?= $this->HOST ?></a>
                                    </em>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="footerContent"  mc:edit="footer_content02" style="padding-top:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#999999;font-family:'PT Sans',sans-serif;font-size:8pt;line-height:150%;padding-right:20px;padding-bottom:20px;padding-left:20px;text-align:center;" >
                                    <a href="<?= $this->SERVER ?>/es/aviso-legal/" target="_blank" class="footerBlock" style="display: inline-block;white-space: nowrap;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;color:#999999;font-weight:normal;text-decoration:underline; margin-left: 5px; margin-right: 5px; padding-left: 0 !important; padding-right: 0 !important;" >Aviso legal</a>
                                    <a href="<?= $this->SERVER ?>/es/aviso-legal/#politica-de-privacidad/" target="_blank" class="footerBlock" style="display: inline-block;white-space: nowrap;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;color:#999999;font-weight:normal;text-decoration:underline; margin-left: 5px; margin-right: 5px; padding-left: 0 !important; padding-right: 0 !important;" >Política de privacidad</a>
                                    <a href="<?= $this->SERVER ?>/es/aviso-legal/#politica-cookies/" target="_blank" class="footerBlock" style="-display: inline-block;webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;color:#999999;font-weight:normal;text-decoration:underline; margin-left: 5px; margin-right: 5px; padding-left: 0 !important; padding-right: 0 !important;" >Política de cookies</a>
                                    <a href="<?= $this->SERVER ?>/es/dinamica-de-compra/" target="_blank" class="footerBlock" style="display: inline-block;white-space: nowrap;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;color:#999999;font-weight:normal;text-decoration:underline; margin-left: 5px; margin-right: 5px; padding-left: 0 !important; padding-right: 0 !important;" >Dinámica de compra</a>
                                    <a href="<?= $this->SERVER ?>/es/preguntas-frecuentes/" target="_blank" class="footerBlock" style="display: inline-block;white-space: nowrap;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;color:#999999;font-weight:normal;text-decoration:underline; margin-left: 5px; margin-right: 5px; padding-left: 0 !important; padding-right: 0 !important;" >Preguntas frecuentes</a>
                                </td>
                            </tr>
                        </table>
                        <!-- // END FOOTER -->
                    </td>
                </tr>
    <?php }


    function endPrint() { ?>
        </table>
        <!-- // END TEMPLATE -->
        </td>
        </tr>
        </table>
        </center>
        </body>
        </html>
    <?php }


}