<?php

class Security extends Controller {
	const SESSION_USER_ID = 'id_user';
	const SESSION_USERNAME = 'username';	
	const SESSION_TOKEN = 'token';	

	protected $db, $loginAttemptsTable, $users;
	public $userId, $username, $roles, $data, $isLogin;

	public function __construct($db, $users) {
		$this->db = $db;
		$this->loginAttemptsTable = new Table($db, "users__login_attempts");
		$this->users = $users;

		$this->roles = array();

		if (isset($_GET["logout"]) || isset($_POST["logout"])) {
			$this->sessionEnd();
		}
		
		$this->sessionStart();

		$this->isLogin = $this->checkLoginStatus();
	}


	/// MÉTODOS PÚBLICOS ///

	public function checkLoginStatus() {
		if (isset($_SESSION[self::SESSION_USER_ID], $_SESSION[self::SESSION_USERNAME], $_SESSION[self::SESSION_TOKEN])) {
			$this->userId = $_SESSION[self::SESSION_USER_ID];
			$this->username = $_SESSION[self::SESSION_USERNAME];
			$login_string = $_SESSION[self::SESSION_TOKEN];
	 
			$results = $this->users->getUsersList(array("password"), array("id" => $this->userId));

			if (count($results) == 1) {
				$login_check = hash('sha512', $results[0]['password'] . (isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:'Undefined/0.0 (SYSNULL 0.0; null)'));

				if ($login_check == $login_string) {
					$this->roles = $this->users->getUserRoles($this->userId);
					$this->data = $this->users->getUserData($this->userId);
					$this->data['email'] = $this->username;

					return true;
				}
			}
		}

		return false;
	}

	public function checkUser($username, $password) {
		$results = $this->users->getUsersList(null, array("username" => $username));

		//echo "<pre>"; print_r($results); // exit;

		if (count($results) === 1) {
			$user_data = $results[0];

			$password = hash('sha512', $password . $user_data['salt']);

			if (!$this->checkbrute($user_data['id'])) {
				$password = hash('sha512', $password . $user_data['salt']);

				//echo $password;

				if ($password == $user_data['password']) {
					return true;
				}
				else {
					/// Se crea un nuevo intento fallido de login
					$this->loginAttemptsTable->insertRecord(array(
						"user_id"	=> $user_data['id'],
						"time"		=> date("Y-m-d H:i:s"),
						"ip"		=> $_SERVER['REMOTE_ADDR'] // P: conectar con una función que realmente obtenga la ip
					));
					
					return false;
				}
			}
			else {
				// Cuenta bloqueada
				return false;
			}
		}
		else {
			return false;
		}
	}

	public function getUserField ($strName) {
		return !empty($this->data[$strName])? $this->data[$strName]: '';
	}

	public function hasPermission ($arrRoles) {
		//if(!is_array($arrRoles)) $arrRoles = [$arrRoles];
		foreach ($arrRoles as $roleId) {
			if(in_array($roleId, $this->roles)) return true;
		}
		return false;
	}

	public function forceLogin ($params, $objOrder = []) {
		$results = $this->users->getUsersList(null, $params);

		if (count($results) === 1) {
			$user_data = $results[0];

			// XSS protection
			$user_id = preg_replace("/[^0-9]+/", "", $user_data['id']);
			$username = preg_replace("/[^a-zA-Z0-9_\-@.]+/", "", $user_data['username']);

			
			$_SESSION[self::SESSION_USER_ID] = $this->userId = $user_data['id'];
			$_SESSION[self::SESSION_USERNAME] = $this->userName = $user_data['username'];
			$_SESSION[self::SESSION_TOKEN] = hash('sha512', $user_data['password'] . (isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:'Undefined/0.0 (SYSNULL 0.0; null)'));

			if (!empty($objOrder['id']) && !empty($objOrder['code'])) {
				$_SESSION[ProductsCart::SESSION_ORDER_ID] = $objOrder['id'];
				$_SESSION[ProductsCart::SESSION_ORDER_CODE] = $objOrder['code'];				
			}

			$this->roles = $this->users->getUserRoles($this->userId);
			$this->data = $this->users->getUserData($this->userId);
			$this->data['email'] = $this->username;

			return true;
		}
		return false;
	}

	public function login ($username, $password) {
		$results = $this->users->getUsersList(null, array("username" => $username));

		//echo "<pre>"; print_r($results); // exit;

		if (count($results) === 1) {
			$user_data = $results[0];

			$password = hash('sha512', $password . $user_data['salt']);

			if (!$this->checkbrute($user_data['id'])) {
				$password = hash('sha512', $password . $user_data['salt']);

				if ($password == $user_data['password']) {					
					return $this->doLogin ($username);
				}
				else {
					/// Se crea un nuevo intento fallido de login
					$this->loginAttemptsTable->insertRecord(array(
						"user_id"	=> $user_data['id'],
						"time"		=> date("Y-m-d H:i:s"),
						"ip"		=> $_SERVER['REMOTE_ADDR'] // P: conectar con una función que realmente obtenga la ip
					));
					
					return false;
				}
			}
			else {
				echo "cuenta bloqueada ";
				// Cuenta bloqueada
				return false;
			}
		}
		else {
			return false;
		}
	}

	public function doLogin ($username) {		
		return $this->forceLogin (['username' => $username]);
	}

	public function secureSessionEnd() {
		// Eliminar las variables de sesión 
		$_SESSION = array();

		// Borrar la cookie actual
		$params = session_get_cookie_params();
		setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
		
		/// Destruir la sesión
		if (!session_id()) session_start();
		session_unset();
		session_destroy();
	}

	public function secureSessionStart() {
		$secure = "SECURE";

		// Set a custom session name
		$session_name = 'euphCMS';
		
		// This stops JavaScript being able to access the session id.
		$httponly = true;
		
		// Forces sessions to only use cookies.
		if (ini_set('session.use_only_cookies', 1) === FALSE) {
			header("Location: ../error.php?err=Could not initiate a safe session (ini_set)");
			exit();
		}

		// Gets current cookies params.
		$cookieParams = session_get_cookie_params();
		session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
		
		// Sets the session name to the one set above.
		session_name($session_name);
		session_start();            // Start the PHP session 
		//session_regenerate_id(true);    // regenerated the session, delete the old one. 
	}

	public function sessionEnd() {
		/// Si la sesión no está iniciada se debe iniciar
		if (!session_id()) session_start();
		
		session_unset();
		session_destroy();
	}

	public function sessionStart() {
		/// Si la sesión no está iniciada se inicia
		if (!session_id()) {
			/// Se cierra la sesión tras un tiempo de inactividad
			$lifetime = 2 * 60 * 60; // 2 horas
			ini_set('session.cookie_lifetime', $lifetime);
			ini_set('session.gc_maxlifetime', $lifetime);

			session_start();
			//session_regenerate_id(true);
		}
	}


	/// MÉTODOS PRIVADOS ///

	public function checkBrute($userId) {
		$valid_time = date("Y-m-d H:i:s", strtotime("- 2 hours"));

		$attempts = $this->loginAttemptsTable->selectRecords(
			array(),
			array(
				"user_id"	=> $userId,
				"time" 		=> array(">", $valid_time)
			)
		);

		//echo "<pre>"; print_r($attempts); exit;

		return count($attempts) >= 5;
	}


	public function isUserDataCompleted () {
		// var_dump($this->data); exit;
		return $this->isLogin &&
			!empty($this->data['nombre']) &&
			!empty($this->data['apellidos']) &&
			!empty($this->data['telefono']) &&
			//!empty($this->data['fecha_nacimiento']) &&
			//!empty($this->data['newsletter']) &&
			!empty($this->data['razon']) &&
			!empty($this->data['cif']) &&
			!empty($this->data['facturacion_direccion']) &&
			!empty($this->data['facturacion_localidad']) &&
			!empty($this->data['facturacion_provincia']) &&
			!empty($this->data['facturacion_codigo_postal']) &&
			!empty($this->data['facturacion_pais']) &&
			//!empty($this->data['envio_nombre']) &&
			!empty($this->data['envio_direccion']) &&
			!empty($this->data['envio_localidad']) &&
			!empty($this->data['envio_provincia']) &&
			!empty($this->data['envio_codigo_postal']) &&
			!empty($this->data['envio_pais']) &&
			true;
	}
}