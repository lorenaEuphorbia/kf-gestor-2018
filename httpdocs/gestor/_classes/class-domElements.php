<?php

class DomElements {

	/// MÉTODOS PÚBLICOS ///

	public static function getField($data) {
		$el = "";

		if (!empty($data["skip"]) && $data["skip"]) {
			return $data["skip"]===true? '' : $data["skip"];
		}

		if (isset($data["div"]) && $data["div"]) {
			$el .= '<div';
			$el .= !empty($data["div_class"]) ? ' class="'.$data["div_class"].'"' : "";
			$el .= '>';
		}

		if (!empty($data["label"])) {
			$el .= '<label for="'.$data["id"].'">'.$data["label"].'</label>';
		}


		switch ($data["type"]) {           
			case 'array':
                $el .= '<fieldset id="'.$data['id'].'-options" class="sortable-array"';
                $el .= ' data-mod="' . $data['mod'] . '" data-act="' . $data['act'] . '" data-id="' . $data["id"] . '"';
                $el .= '>';
                if (!empty($data["legend"])) {
                    $el .= '<legend>'.$data['legend'].'</legend>';
                }

                if (!empty($data["childs"])) {
                    $num_el = $data["array_count"];

                    $el .= '<ul ';
					$el .= !empty($data["disabled"]) && (is_callable($data['disabled']) ? $data['disabled']() : $data['disabled']) ? ' disabled' : "";
					$el .= !empty($data["class"]) ? ' class="sortable-fieldsets '.$data["class"].'"' : "sortable-fieldsets";
                    $el .= '<input type="hidden" data-numItems="1" name="' . $data["id"] . '-num-items" value="'.$num_el.'" />';

                    for ($i=0; $i <= $num_el; $i++) {
                   		$nameParent = $data["id"] . '[' . ($i - 1) . ']';

                        $el .= '<li' . ($i == 0 ? ' hidden' : '') . '>';
                        $el .= '<i class="move fa fa-sort"></i>';
                        $el .= '<i class="delete fa fa-trash"></i>';
                        $el .= '<fieldset' . ($i == 0 ? ' disabled' : '') . '>';

                        foreach ($data['childs'] as $index => $field) {
                            $field['data'] = ['id' => $field['id'], 'db' => true];
                            $field['id'] = $nameParent . '[' . $field['id'] . ']';

                            $field['val'] = $i > 0? $field['val'][$i - 1]: '';

                            $el .= DomElements::getField($field);
                        }

                        $el .= '<input type="hidden" id="position-banner-group-'.$i.'" name="' . $nameParent . '[position]" data-position="1" value="'.($i > 0 ? -$i : 0).'" />';
                        $el .= '</fieldset>';
                        $el .= "</li>";
                    }

                    $el .= '<button type="button" class="add"><i class="fa fa-plus"></i> Añadir </button>';
                    $el .= '</ul>';
                }

                $el .= '</fieldset>';
            break;
			case 'ckeditor':
				$el .= '<div class="ckeditor"><textarea id="'.$data["id"].'" name="'.$data["id"].'"';

				if (!empty($data["data"])) {
					foreach ($data["data"] as $data_tag => $value) {
						$el .= ' data-'.$data_tag.'="'.$value.'"';
					}
				}


				$el .= !empty($data["placeholder"]) ? ' placeholder="'.$data["placeholder"].'"' : "";
				$el .= !empty($data["length"]) ? ' maxlength="'.$data["length"].'"' : "";
				$el .= !empty($data["required"]) ? ' required' : "";
				$el .= !empty($data["readonly"]) ? ' readonly' : "";
				$el .= !empty($data["disabled"]) && (is_callable($data['disabled']) ? $data['disabled']() : $data['disabled']) ? ' disabled' : "";
				$el .= !empty($data["class"]) ? ' class="'.$data["class"].'"' : "";
				
				$el .= '>';
				$el .= !empty($data["val"]) ? htmlspecialchars($data["val"]) : "";
				$el .= '</textarea></div>';
			break;
			case 'galery':
                $el .= '<div class="galery">';
                $el .= '</div>';
            break;
			case 'textarea':
				$el .= '<textarea id="'.$data["id"].'" name="'.$data["id"].'"';

				if (!empty($data["data"])) {
					foreach ($data["data"] as $data_tag => $value) {
						$el .= ' data-'.$data_tag.'="'.$value.'"';
					}
				}


				$el .= !empty($data["placeholder"]) ? ' placeholder="'.$data["placeholder"].'"' : "";
				$el .= !empty($data["length"]) ? ' maxlength="'.$data["length"].'"' : "";
				$el .= !empty($data["required"]) ? ' required' : "";
				$el .= !empty($data["readonly"]) ? ' readonly' : "";
				$el .= !empty($data["disabled"]) && (is_callable($data['disabled']) ? $data['disabled']() : $data['disabled']) ? ' disabled' : "";
				$el .= !empty($data["class"]) ? ' class="'.$data["class"].'"' : "";

				$el .= '>';
				$el .= !empty($data["val"]) ? $data["val"] : "";
				$el .= '</textarea>';
			break;
			case 'font-awesome-picker':
				$arr_icons = ['fa-500px', 'fa-adjust', 'fa-adn', 'fa-align-center', 'fa-align-justify', 'fa-align-left', 'fa-align-right', 'fa-amazon', 'fa-ambulance', 'fa-anchor', 'fa-android', 'fa-angellist', 'fa-angle-double-down', 'fa-angle-double-left', 'fa-angle-double-right', 'fa-angle-double-up', 'fa-angle-down', 'fa-angle-left', 'fa-angle-right', 'fa-angle-up', 'fa-apple', 'fa-archive', 'fa-area-chart', 'fa-arrow-circle-down', 'fa-arrow-circle-left', 'fa-arrow-circle-o-down', 'fa-arrow-circle-o-left', 'fa-arrow-circle-o-right', 'fa-arrow-circle-o-up', 'fa-arrow-circle-right', 'fa-arrow-circle-up', 'fa-arrow-down', 'fa-arrow-left', 'fa-arrow-right', 'fa-arrow-up', 'fa-arrows', 'fa-arrows-alt', 'fa-arrows-h', 'fa-arrows-v', 'fa-asterisk', 'fa-at', 'fa-automobile (alias)', 'fa-backward', 'fa-balance-scale', 'fa-ban', 'fa-bank (alias)', 'fa-bar-chart', 'fa-bar-chart-o (alias)', 'fa-barcode', 'fa-bars', 'fa-battery-0 (alias)', 'fa-battery-1 (alias)', 'fa-battery-2 (alias)', 'fa-battery-3 (alias)', 'fa-battery-4 (alias)', 'fa-battery-empty', 'fa-battery-full', 'fa-battery-half', 'fa-battery-quarter', 'fa-battery-three-quarters', 'fa-bed', 'fa-beer', 'fa-behance', 'fa-behance-square', 'fa-bell', 'fa-bell-o', 'fa-bell-slash', 'fa-bell-slash-o', 'fa-bicycle', 'fa-binoculars', 'fa-birthday-cake', 'fa-bitbucket', 'fa-bitbucket-square', 'fa-bitcoin (alias)', 'fa-black-tie', 'fa-bluetooth', 'fa-bluetooth-b', 'fa-bold', 'fa-bolt', 'fa-bomb', 'fa-book', 'fa-bookmark', 'fa-bookmark-o', 'fa-briefcase', 'fa-btc', 'fa-bug', 'fa-building', 'fa-building-o', 'fa-bullhorn', 'fa-bullseye', 'fa-bus', 'fa-buysellads', 'fa-cab (alias)', 'fa-calculator', 'fa-calendar', 'fa-calendar-check-o', 'fa-calendar-minus-o', 'fa-calendar-o', 'fa-calendar-plus-o', 'fa-calendar-times-o', 'fa-camera', 'fa-camera-retro', 'fa-car', 'fa-caret-down', 'fa-caret-left', 'fa-caret-right', 'fa-caret-square-o-down', 'fa-caret-square-o-left', 'fa-caret-square-o-right', 'fa-caret-square-o-up', 'fa-caret-up', 'fa-cart-arrow-down', 'fa-cart-plus', 'fa-cc', 'fa-cc-amex', 'fa-cc-diners-club', 'fa-cc-discover', 'fa-cc-jcb', 'fa-cc-mastercard', 'fa-cc-paypal', 'fa-cc-stripe', 'fa-cc-visa', 'fa-certificate', 'fa-chain (alias)', 'fa-chain-broken', 'fa-check', 'fa-check-circle', 'fa-check-circle-o', 'fa-check-square', 'fa-check-square-o', 'fa-chevron-circle-down', 'fa-chevron-circle-left', 'fa-chevron-circle-right', 'fa-chevron-circle-up', 'fa-chevron-down', 'fa-chevron-left', 'fa-chevron-right', 'fa-chevron-up', 'fa-child', 'fa-chrome', 'fa-circle', 'fa-circle-o', 'fa-circle-o-notch', 'fa-circle-thin', 'fa-clipboard', 'fa-clock-o', 'fa-clone', 'fa-close (alias)', 'fa-cloud', 'fa-cloud-download', 'fa-cloud-upload', 'fa-cny (alias)', 'fa-code', 'fa-code-fork', 'fa-codepen', 'fa-codiepie', 'fa-coffee', 'fa-cog', 'fa-cogs', 'fa-columns', 'fa-comment', 'fa-comment-o', 'fa-commenting', 'fa-commenting-o', 'fa-comments', 'fa-comments-o', 'fa-compass', 'fa-compress', 'fa-connectdevelop', 'fa-contao', 'fa-copy (alias)', 'fa-copyright', 'fa-creative-commons', 'fa-credit-card', 'fa-credit-card-alt', 'fa-crop', 'fa-crosshairs', 'fa-css3', 'fa-cube', 'fa-cubes', 'fa-cut (alias)', 'fa-cutlery', 'fa-dashboard (alias)', 'fa-dashcube', 'fa-database', 'fa-dedent (alias)', 'fa-delicious', 'fa-desktop', 'fa-deviantart', 'fa-diamond', 'fa-digg', 'fa-dollar (alias)', 'fa-dot-circle-o', 'fa-download', 'fa-dribbble', 'fa-dropbox', 'fa-drupal', 'fa-edge', 'fa-edit (alias)', 'fa-eject', 'fa-ellipsis-h', 'fa-ellipsis-v', 'fa-empire', 'fa-envelope', 'fa-envelope-o', 'fa-envelope-square', 'fa-eraser', 'fa-eur', 'fa-euro (alias)', 'fa-exchange', 'fa-exclamation', 'fa-exclamation-circle', 'fa-exclamation-triangle', 'fa-expand', 'fa-expeditedssl', 'fa-external-link', 'fa-external-link-square', 'fa-eye', 'fa-eye-slash', 'fa-eyedropper', 'fa-facebook', 'fa-facebook-f (alias)', 'fa-facebook-official', 'fa-facebook-square', 'fa-fast-backward', 'fa-fast-forward', 'fa-fax', 'fa-feed (alias)', 'fa-female', 'fa-fighter-jet', 'fa-file', 'fa-file-archive-o', 'fa-file-audio-o', 'fa-file-code-o', 'fa-file-excel-o', 'fa-file-image-o', 'fa-file-movie-o (alias)', 'fa-file-o', 'fa-file-pdf-o', 'fa-file-photo-o (alias)', 'fa-file-picture-o (alias)', 'fa-file-powerpoint-o', 'fa-file-sound-o (alias)', 'fa-file-text', 'fa-file-text-o', 'fa-file-video-o', 'fa-file-word-o', 'fa-file-zip-o (alias)', 'fa-files-o', 'fa-film', 'fa-filter', 'fa-fire', 'fa-fire-extinguisher', 'fa-firefox', 'fa-flag', 'fa-flag-checkered', 'fa-flag-o', 'fa-flash (alias)', 'fa-flask', 'fa-flickr', 'fa-floppy-o', 'fa-folder', 'fa-folder-o', 'fa-folder-open', 'fa-folder-open-o', 'fa-font', 'fa-fonticons', 'fa-fort-awesome', 'fa-forumbee', 'fa-forward', 'fa-foursquare', 'fa-frown-o', 'fa-futbol-o', 'fa-gamepad', 'fa-gavel', 'fa-gbp', 'fa-ge (alias)', 'fa-gear (alias)', 'fa-gears (alias)', 'fa-genderless', 'fa-get-pocket', 'fa-gg', 'fa-gg-circle', 'fa-gift', 'fa-git', 'fa-git-square', 'fa-github', 'fa-github-alt', 'fa-github-square', 'fa-gittip (alias)', 'fa-glass', 'fa-globe', 'fa-google', 'fa-google-plus', 'fa-google-plus-square', 'fa-google-wallet', 'fa-graduation-cap', 'fa-gratipay', 'fa-group (alias)', 'fa-h-square', 'fa-hacker-news', 'fa-hand-grab-o (alias)', 'fa-hand-lizard-o', 'fa-hand-o-down', 'fa-hand-o-left', 'fa-hand-o-right', 'fa-hand-o-up', 'fa-hand-paper-o', 'fa-hand-peace-o', 'fa-hand-pointer-o', 'fa-hand-rock-o', 'fa-hand-scissors-o', 'fa-hand-spock-o', 'fa-hand-stop-o (alias)', 'fa-hashtag', 'fa-hdd-o', 'fa-header', 'fa-headphones', 'fa-heart', 'fa-heart-o', 'fa-heartbeat', 'fa-history', 'fa-home', 'fa-hospital-o', 'fa-hotel (alias)', 'fa-hourglass', 'fa-hourglass-1 (alias)', 'fa-hourglass-2 (alias)', 'fa-hourglass-3 (alias)', 'fa-hourglass-end', 'fa-hourglass-half', 'fa-hourglass-o', 'fa-hourglass-start', 'fa-houzz', 'fa-html5', 'fa-i-cursor', 'fa-ils', 'fa-image (alias)', 'fa-inbox', 'fa-indent', 'fa-industry', 'fa-info', 'fa-info-circle', 'fa-inr', 'fa-instagram', 'fa-institution (alias)', 'fa-internet-explorer', 'fa-intersex (alias)', 'fa-ioxhost', 'fa-italic', 'fa-joomla', 'fa-jpy', 'fa-jsfiddle', 'fa-key', 'fa-keyboard-o', 'fa-krw', 'fa-language', 'fa-laptop', 'fa-lastfm', 'fa-lastfm-square', 'fa-leaf', 'fa-leanpub', 'fa-legal (alias)', 'fa-lemon-o', 'fa-level-down', 'fa-level-up', 'fa-life-bouy (alias)', 'fa-life-buoy (alias)', 'fa-life-ring', 'fa-life-saver (alias)', 'fa-lightbulb-o', 'fa-line-chart', 'fa-link', 'fa-linkedin', 'fa-linkedin-square', 'fa-linux', 'fa-list', 'fa-list-alt', 'fa-list-ol', 'fa-list-ul', 'fa-location-arrow', 'fa-lock', 'fa-long-arrow-down', 'fa-long-arrow-left', 'fa-long-arrow-right', 'fa-long-arrow-up', 'fa-magic', 'fa-magnet', 'fa-mail-forward (alias)', 'fa-mail-reply (alias)', 'fa-mail-reply-all (alias)', 'fa-male', 'fa-map', 'fa-map-marker', 'fa-map-o', 'fa-map-pin', 'fa-map-signs', 'fa-mars', 'fa-mars-double', 'fa-mars-stroke', 'fa-mars-stroke-h', 'fa-mars-stroke-v', 'fa-maxcdn', 'fa-meanpath', 'fa-medium', 'fa-medkit', 'fa-meh-o', 'fa-mercury', 'fa-microphone', 'fa-microphone-slash', 'fa-minus', 'fa-minus-circle', 'fa-minus-square', 'fa-minus-square-o', 'fa-mixcloud', 'fa-mobile', 'fa-mobile-phone (alias)', 'fa-modx', 'fa-money', 'fa-moon-o', 'fa-mortar-board (alias)', 'fa-motorcycle', 'fa-mouse-pointer', 'fa-music', 'fa-navicon (alias)', 'fa-neuter', 'fa-newspaper-o', 'fa-object-group', 'fa-object-ungroup', 'fa-odnoklassniki', 'fa-odnoklassniki-square', 'fa-opencart', 'fa-openid', 'fa-opera', 'fa-optin-monster', 'fa-outdent', 'fa-pagelines', 'fa-paint-brush', 'fa-paper-plane', 'fa-paper-plane-o', 'fa-paperclip', 'fa-paragraph', 'fa-paste (alias)', 'fa-pause', 'fa-pause-circle', 'fa-pause-circle-o', 'fa-paw', 'fa-paypal', 'fa-pencil', 'fa-pencil-square', 'fa-pencil-square-o', 'fa-percent', 'fa-phone', 'fa-phone-square', 'fa-photo (alias)', 'fa-picture-o', 'fa-pie-chart', 'fa-pied-piper', 'fa-pied-piper-alt', 'fa-pinterest', 'fa-pinterest-p', 'fa-pinterest-square', 'fa-plane', 'fa-play', 'fa-play-circle', 'fa-play-circle-o', 'fa-plug', 'fa-plus', 'fa-plus-circle', 'fa-plus-square', 'fa-plus-square-o', 'fa-power-off', 'fa-print', 'fa-product-hunt', 'fa-puzzle-piece', 'fa-qq', 'fa-qrcode', 'fa-question', 'fa-question-circle', 'fa-quote-left', 'fa-quote-right', 'fa-ra (alias)', 'fa-random', 'fa-rebel', 'fa-recycle', 'fa-reddit', 'fa-reddit-alien', 'fa-reddit-square', 'fa-refresh', 'fa-registered', 'fa-remove (alias)', 'fa-renren', 'fa-reorder (alias)', 'fa-repeat', 'fa-reply', 'fa-reply-all', 'fa-retweet', 'fa-rmb (alias)', 'fa-road', 'fa-rocket', 'fa-rotate-left (alias)', 'fa-rotate-right (alias)', 'fa-rouble (alias)', 'fa-rss', 'fa-rss-square', 'fa-rub', 'fa-ruble (alias)', 'fa-rupee (alias)', 'fa-safari', 'fa-save (alias)', 'fa-scissors', 'fa-scribd', 'fa-search', 'fa-search-minus', 'fa-search-plus', 'fa-sellsy', 'fa-send (alias)', 'fa-send-o (alias)', 'fa-server', 'fa-share', 'fa-share-alt', 'fa-share-alt-square', 'fa-share-square', 'fa-share-square-o', 'fa-shekel (alias)', 'fa-sheqel (alias)', 'fa-shield', 'fa-ship', 'fa-shirtsinbulk', 'fa-shopping-bag', 'fa-shopping-basket', 'fa-shopping-cart', 'fa-sign-in', 'fa-sign-out', 'fa-signal', 'fa-simplybuilt', 'fa-sitemap', 'fa-skyatlas', 'fa-skype', 'fa-slack', 'fa-sliders', 'fa-slideshare', 'fa-smile-o', 'fa-soccer-ball-o (alias)', 'fa-sort', 'fa-sort-alpha-asc', 'fa-sort-alpha-desc', 'fa-sort-amount-asc', 'fa-sort-amount-desc', 'fa-sort-asc', 'fa-sort-desc', 'fa-sort-down (alias)', 'fa-sort-numeric-asc', 'fa-sort-numeric-desc', 'fa-sort-up (alias)', 'fa-soundcloud', 'fa-space-shuttle', 'fa-spinner', 'fa-spoon', 'fa-spotify', 'fa-square', 'fa-square-o', 'fa-stack-exchange', 'fa-stack-overflow', 'fa-star', 'fa-star-half', 'fa-star-half-empty (alias)', 'fa-star-half-full (alias)', 'fa-star-half-o', 'fa-star-o', 'fa-steam', 'fa-steam-square', 'fa-step-backward', 'fa-step-forward', 'fa-stethoscope', 'fa-sticky-note', 'fa-sticky-note-o', 'fa-stop', 'fa-stop-circle', 'fa-stop-circle-o', 'fa-street-view', 'fa-strikethrough', 'fa-stumbleupon', 'fa-stumbleupon-circle', 'fa-subscript', 'fa-subway', 'fa-suitcase', 'fa-sun-o', 'fa-superscript', 'fa-support (alias)', 'fa-table', 'fa-tablet', 'fa-tachometer', 'fa-tag', 'fa-tags', 'fa-tasks', 'fa-taxi', 'fa-television', 'fa-tencent-weibo', 'fa-terminal', 'fa-text-height', 'fa-text-width', 'fa-th', 'fa-th-large', 'fa-th-list', 'fa-thumb-tack', 'fa-thumbs-down', 'fa-thumbs-o-down', 'fa-thumbs-o-up', 'fa-thumbs-up', 'fa-ticket', 'fa-times', 'fa-times-circle', 'fa-times-circle-o', 'fa-tint', 'fa-toggle-down (alias)', 'fa-toggle-left (alias)', 'fa-toggle-off', 'fa-toggle-on', 'fa-toggle-right (alias)', 'fa-toggle-up (alias)', 'fa-trademark', 'fa-train', 'fa-transgender', 'fa-transgender-alt', 'fa-trash', 'fa-trash-o', 'fa-tree', 'fa-trello', 'fa-tripadvisor', 'fa-trophy', 'fa-truck', 'fa-try', 'fa-tty', 'fa-tumblr', 'fa-tumblr-square', 'fa-turkish-lira (alias)', 'fa-tv (alias)', 'fa-twitch', 'fa-twitter', 'fa-twitter-square', 'fa-umbrella', 'fa-underline', 'fa-undo', 'fa-university', 'fa-unlink (alias)', 'fa-unlock', 'fa-unlock-alt', 'fa-unsorted (alias)', 'fa-upload', 'fa-usb', 'fa-usd', 'fa-user', 'fa-user-md', 'fa-user-plus', 'fa-user-secret', 'fa-user-times', 'fa-users', 'fa-venus', 'fa-venus-double', 'fa-venus-mars', 'fa-viacoin', 'fa-video-camera', 'fa-vimeo', 'fa-vimeo-square', 'fa-vine', 'fa-vk', 'fa-volume-down', 'fa-volume-off', 'fa-volume-up', 'fa-warning (alias)', 'fa-wechat (alias)', 'fa-weibo', 'fa-weixin', 'fa-whatsapp', 'fa-wheelchair', 'fa-wifi', 'fa-wikipedia-w', 'fa-windows', 'fa-won (alias)', 'fa-wordpress', 'fa-wrench', 'fa-xing', 'fa-xing-square', 'fa-y-combinator', 'fa-y-combinator-square (alias)', 'fa-yahoo', 'fa-yc (alias)', 'fa-yc-square (alias)', 'fa-yelp', 'fa-yen (alias)', 'fa-youtube', 'fa-youtube-play', 'fa-youtube-square'];

				$el .= '<select id="'.$data["id"].'" name="'.$data["id"].'" class="icon-picker">';
				$el .= '<option value="">No icon</option>';

				foreach ($arr_icons as $icon) {
					$el .= '<option value="fa '.$icon.'"'. (!empty($data["val"]) && $data["val"] == 'fa '.$icon ? ' selected' : '') .'>fa ' . $icon . '</icon>';
				}

				$el .= '</select>';
			break;
			case 'input-image':
				$el .= '<input type="file" id="'.$data["id"].'" name="'.$data["id"].'"';
				$el .= !empty($data["required"]) && empty($data["val"]) ? ' required' : "";
				$el .= !empty($data["class"]) ? ' class="'.$data["class"].'"' : "";
				$el .= ' />';
				//var_dump($data['val']); //exit;


				if (!empty($data["val"])) {
					$el .= '<input type="hidden" id="'.$data["id"].'-alt" name="'.$data["id"].'-alt"';
					$el .= ' data-mod="'.$data["val"][1].'"';
					$el .= ' data-type="'.$data["val"][2].'"';
					$el .= ' value="'.$data["val"][0].'" />';

					$rand_num = rand();

					$el .= '<div class="preview">';
					$el .= '<img src="' . $data["val"][3] . '?r='.$rand_num.'" />';
					$el .= '<div>';
					$el .= '<a href="' . $data["val"][3] . '?r='.$rand_num.'" target="_blank" data-zoom="1"><i class="fa fa-search-plus"></i> <span>' . _("Ampliar") . '</span></a>';

					if (empty($data["required"])) {
						$el .= '<a href="javascript:void(0);" data-remove="1"><i class="fa fa-trash"></i> <span>' . _("Eliminar") . '</span></a>';
					}

					$el .= '</div>';
					$el .= '</div>';
				}
			break;
			case 'input-file':
				$el .= '<input type="file" id="'.$data["id"].'" name="'.$data["id"].'"';
				$el .= !empty($data["required"]) && empty($data["val"]) ? ' required' : "";
				$el .= !empty($data["class"]) ? ' class="'.$data["class"].'"' : "";
				$el .= ' />';


				if (!empty($data["val"])) {
					$el .= '<input type="hidden" id="'.$data["id"].'-alt" name="'.$data["id"].'-alt"';
					$el .= ' data-mod="'.$data["val"][1].'"';
					$el .= ' data-type="'.$data["val"][2].'"';
					$el .= ' value="'.$data["val"][0].'" />';

					$rand_num = rand();

					$el .= '<div class="download">';
					$el .= '<a href="' . $data["val"][3] . '?r='.$rand_num.'" target="_blank" ><i class="fa fa-download"></i> <span>' . _("Descargar") . '</span></a>';

					if (empty($data["required"])) {
						$el .= '<a href="javascript:void(0);" data-remove="1"><i class="fa fa-trash"></i> <span>' . _("Eliminar") . '</span></a>';
					}

					$el .= '</div>';
				}
			break;
			case 'heading':
				$arr_headings = [
					"h1" => "H1",
					"h2" => "H2",
					"h3" => "H3",
					"h4" => "H4",
					"h5" => "H5",
					"h6" => "H6",
				];

				$el .= '<input type="text" id="'.$data["id"].'" name="'.$data["id"].'" value="'.$data["val"][0].'" '.(!empty($data["required"]) ? 'required ' : "").'/>';
				$el .= '<select id="'.$data["id"].'-type" name="'.$data["id"].'-type">';

				foreach ($arr_headings as $value => $text) {
					$el .= '<option value="'.$value.'"' . (isset($data["val"][1]) && $value == $data["val"][1] ? ' selected' : "") . '>'.$text.'</option>';
				}

				$el .= '</select>';
			break;
			case 'link':
				$arr_link_targets = [
					"_self" => "en la misma ventana",
					"_blank" => "en ventana nueva"
				];

				$el .= '<input type="text" id="'.$data["id"].'" name="'.$data["id"].'" value="'.$data["val"][0].'" '.(!empty($data["required"]) ? 'required ' : "").'/>';
				$el .= '<select id="'.$data["id"].'-target" name="'.$data["id"].'-target">';

				foreach ($arr_link_targets as $value => $text) {
					$el .= '<option value="'.$value.'"' . (isset($data["val"][1]) && $value == $data["val"][1] ? ' selected' : "") . '>'.$text.'</option>';
				}

				$el .= '</select>';
			break;
			case 'select':
			case 'select-chosen':
				$el .= '<select id="'.$data["id"].'" name="'.$data["id"].'"';
				$el .= !empty($data["required"]) ? ' required' : "";
				$el .= !empty($data["disabled"]) ? ' disabled' : "";
				$el .= !empty($data["hidden"]) ? ' hidden' : "";
				$el .= !empty($data["class"]) ? ' class="'.$data["class"].'"' : "";
				$el .= '>';

				if (isset($data["options"]["empty"])) {
					$el .= '<option value="">'.$data["options"]["empty"].'</option>';
				}
				
				if (!empty($data["options"]["values"])) {
					foreach ($data["options"]["values"] as $value => $text) {
						$el .= '<option value="'.$value.'"' . 
							(isset($data["val"]) && $value == $data["val"] ? ' selected' : "") . '>'.$text.'</option>';
					}
				}

				$el .= '</select>';
			break;
			case 'tags':
				$el .= '<select multiple id="'.$data["id"].'" name="'.$data["id"].'[]"';
				$el .= !empty($data["required"]) ? ' required' : "";
				$el .= !empty($data["readonly"]) ? ' readonly' : "";
				$el .= !empty($data["disabled"]) && (is_callable($data['disabled']) ? $data['disabled']() : $data['disabled']) ? ' disabled' : "";

				$el .= ' class="chosen' . (!empty($data["class"]) ? ' ' . $data["class"] : "") . '"';
				$el .= '>';

				if (isset($data["options"]["empty"])) {
					$el .= '<option value="" ';
					$el .= '>'.$data["options"]["empty"].'</option>';
				}

				if (!empty($data["options"]["values"])) {
					foreach ($data["options"]["values"] as $opt) {
						$el .= '<option value="'.$opt[0].'"' . (isset($data["val"]) && in_array($opt[0], $data["val"]) ? ' selected' : "");
						$el .='>'.$opt[1].'</option>';
					}
				}

				$el .= '</select>';
			break;
			case 'title':
				if (!empty($data["options"]["text"])) {
					$type = !empty($data["options"]["type"]) ? $data["options"]["type"] : 'div';
					
					$el .= "<$type";
					$el .= !empty($data["options"]["class"]) ? ' class="'.$data["options"]["class"].'"' : "";
					$el .= '>';
					$el .= $data["options"]["text"];
					$el .= "</$type>";
				}
				else {
					$type = !empty($data["options"]["type"]) ? $data["options"]["type"] : 'div';
					
					$el .= "<$type";
					$el .= !empty($data["options"]["class"]) ? ' class="'.$data["options"]["class"].'"' : "";
					$el .= '>';
					$el .= !empty($data["val"]) ? $data["val"] : "";
					$el .= "</$type>";

				}
			break;
			default:
				$el .= '<input type="'.$data["type"].'" id="'.$data["id"].'" name="'.$data["id"].'"';

				if ( isset($data["val"]) && $data["val"]!="" ) {
					switch ($data["type"]) {
						case 'date':
							$val = date("Y-m-d", strtotime($data["val"] == "0000-00-00" ? date("Y-m-d") : $data["val"]));
						break;
						case 'datetime-local':
							$val = date("Y-m-d\TH:i:s", strtotime($data["val"] == "0000-00-00 00:00:00" ? date("Y-m-d H:i") : $data["val"]));
						break;
						case 'checkbox':	
							$el .= $data["val"] == 1 ? ' checked' : '';						
							$val = "true";
						break; 
						default:
						//var_dump($data["val"]);
							$val = htmlspecialchars($data["val"]);
						break;
					}

					$el .=  ' value="'.$val.'"';
				} else {
					switch ($data["type"]) {
						case 'checkbox':							
							$el .=  ' value="true"';
						break; 
					}
				}

				if (!empty($data["data"])) {
					foreach ($data["data"] as $data_tag => $value) {
						$el .= ' data-'.$data_tag.'="'.$value.'"';
					}
				}

				if (!empty($data["custom"])) {
					foreach ($data["custom"] as $data_custom => $value) {
						$el .= $data_custom.'="'.$value.'"';
					}
				}

				$el .= !empty($data["placeholder"]) ? ' placeholder="'.$data["placeholder"].'"' : "";
				$el .= !empty($data["length"]) ? ' maxlength="'.$data["length"].'"' : "";
				$el .= !empty($data["required"]) ? ' required' : "";
				$el .= !empty($data["readonly"]) ? ' readonly' : "";
				$el .= !empty($data["disabled"]) && (is_callable($data['disabled']) ? $data['disabled']() : $data['disabled']) ? ' disabled' : "";
				$el .= !empty($data["class"]) ? ' class="'.$data["class"].'"' : "";
				$el .= ' />';
			break;
		}

		if(isset($data['data']) && isset($data['data']['function']))
			$el .= '<script type="text/javascript">'.$data['data']['function'].'($("#'.$data["id"].'"));</script>';
		if (isset($data["div"]) && $data["div"]) {
			$el .= '</div>';
		}


		return $el;
	}

	public static function getForm($form) {
		$el = '<form';
		$el .= !empty($form["action"]) ? ' action="' . $form["action"] . '"' : "";
		$el .= ' method="' . (!empty($form["method"]) ? $form["method"] : "post") . '"';
		$el .= !empty($form["id"]) ? ' id="' . $form["id"] . '" name="' . $form["id"] . '"' : "";
		$el .= !empty($form["class"]) ? ' class="' . $form["class"] . '"' : "";

		if (!empty($form["data"])) {
			foreach ($form["data"] as $dataKey => $dataVal) {
				$el .= " data-$dataKey=\"$dataVal\"";
			}
		}

		$el .= '>';


		if (!empty($form["fields"])) {
			foreach ($form["fields"] as $field) {
				$el .= self::getField($field);
			}
		}

		$el .= '<button type="submit">' . (!empty($form["button"]) ? $form["button"] : _("Enviar")) . '</button>';
		
		$el .= '</form>';

		return $el;
	}

	public static function getFormsGroup($forms_group) {
		$el = "";

		if (!empty($forms_group) && !empty($forms_group["forms"])) {
			$el .= "<div";
			$el .= !empty($forms_group["id"]) ? ' id="' . $forms_group["id"] . '"' : "";
			$el .= !empty($forms_group["class"]) ? ' class="' . $forms_group["class"] . '"' : "";
			$el .= ">";
		
			foreach ($forms_group["forms"] as $form) {
				$el .= self::getForm($form);
			}
		
			$el .= "</div>";
		}

		return $el;
	}
}