<?php

class PasswordRequests {
	protected $db, $table;

	public function __construct($base) {
		$this->db = $base->db;
		
		$this->table = new Table($this->db, "password_requests");
	}

	public function __destruct() {
		return true;
	}


	/// MÉTODOS PÚBLICOS ///
	
	/*public function getValue($fieldId) {
		$val = $this->preferencesTable->selectRecords(["text_1"], ["id" => $fieldId], null, null, null, "FETCH_COLUMN");
		return isset($val[0]) ? $val[0] : "";
	}*/

	public function deleteRequest($id) {
		$this->table->deleteRecord(['id' => $id]);
	}
	
    /**
     * Función que borra el registro creado en la tabla password_request cuando un usuario solicita una recuperación de contraseña
     * Para ello primeramente se valida el mail y el token comprobando que realmente existen en la base de datos
     * @param type $mail
     * @param type $token
     * @return objeto borrado ó en caso contrario false
     */
    public function procesarRequest($email,$token){
        if($objRequest = $this->searchRequest($email, $token)){
            $this->deleteRequest($objRequest['id']);
            return true;
        }
        return false;

    }

	public function purgeRequests() {
		$deadline = Date("Y-m-d H:i:s", strtotime("- 30 min"));

		$arr_outdated = $this->table->selectRecords(
			['id'],
			['date' => ['<', $deadline]]
		);

		foreach ($arr_outdated as $el) {
			$this->table->deleteRecord(['id' => $el['id']]);
		}
	}

	public function searchRequest($email, $token) {
		return $this->table->selectRecords(
			['id'],
			['email' => $email, 'token' => $token]
		);
	}

	public function setRequest($email) {
		$arr_repeated = $this->table->selectRecords(
			['id'],
			['email' => $email]
		);

		foreach ($arr_repeated as $el) {
			$this->table->deleteRecord(['id' => $el['id']]);
		}

		$token = Utilities::getUniqueCode();

		$new_request = $this->table->insertRecord([
			'email'	=> $email,
			'token' => $token,
			'date'	=> Date('Y-m-d H:i:s')
		]);

		if ($new_request) return $token;
		else return false;
	}
}