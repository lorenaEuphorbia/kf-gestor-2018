<?php
	/*
	 ***************************************************************
	 *                   CLASE: Pasarela Paypal                    *
	 ***************************************************************
	 */
	/**
	 * Gestión de las paginación.
	 * 
	 * Prepara la paginación y devuelve el código necesario.
	 *
	 * @package     EUPHORBIA
	 * @subpackage  Paypal
	 * @author      Pablo Enjuto Martín <cesar@euphorbia.es>
	 * @copyright   Copyright 2013, Euphorbia Comunicación, S.L.L.
	 * @access      public
	 * @version     1.00 - Final
	 * @link        http://www.euphorbia.es Euphorbia Comunicación, S.L.L.
	 *
	 */

	class Paypal {
		private $_b;

		public	$business		= 'cesar-facilitator@euphorbia.es';
		public $nombre_titular	= 'TEST PAYPAL';

		public $api_version	= '95.0';
		public $api_username	= 'cesar-facilitator_api1.euphorbia.es';
		public $api_password	= '4JQSR8QZHJRSLRJQ';
		public $api_signature	= 'AFcWxV21C7fd0v3bYYYRCpSSRl31A5tedPw1RwFLGyBrmxoJ3V3XRy1q';
		
		public 	$urlComercio;
		public 	$urlRespuestaOk;
		public 	$urlRespuestaKo;
		public 	$urlNotificacion;

		public	$url_formulario;
		public $url_api;
		public $nvp_credentials;
		public	$moneda;

		private $debug			= false;

		const MONEDAS = array (
			'EUR' => 'EUR',
			'USD' => 'USD',
		);

		function __construct ($base) {
			$this->_b = $base;

			$this->urlComercio  	= "http://$_SERVER[HTTP_HOST]/";
			$this->urlRespuestaOk 	= "http://$_SERVER[HTTP_HOST]/" . $base->preferences->getValue('paypal_url_ok');
			$this->urlRespuestaKo 	= "http://$_SERVER[HTTP_HOST]/" . $base->preferences->getValue('paypal_url_ko');
			$this->urlNotificacion 	= "http://$_SERVER[HTTP_HOST]/paypal-notificacion/";

			$this->moneda = $base->preferences->getValue('paypal_moneda');

			if ($base->preferences->getValue('paypal_is_real')) {
				$this->business = $base->preferences->getValue('paypal_email_titular');
				$this->nombre_titular = $base->preferences->getValue('paypal_nombre_titular');
				$this->api_username		= $base->preferences->getValue('paypal_email_api');
				$this->api_password = $base->preferences->getValue('paypal_password_secreta');
				$this->api_signature = $base->preferences->getValue('paypal_clave_secreta');

				$this->url_formulario	= "https://www.paypal.com/cgi-bin/webscr";
				$this->url_api			= "https://api-3t.paypal.com/nvp";
			}
			else {
				$this->url_formulario	= "https://www.sandbox.paypal.com/cgi-bin/webscr";
				$this->url_api			= "https://api-3t.sandbox.paypal.com/nvp";
			}

			$this->nvp_credentials = 'USER=' . $this->api_username . '&PWD=' . $this->api_password . '&SIGNATURE=' . $this->api_signature . '&VERSION=' . $this->api_version;
		}

		// FUNCIONES PÚBLICAS //

		public function DoAuthorization ($DataArray) {
			$camposNVP = '&METHOD=DoAuthorization';
			
			$DAFields = isset($DataArray['DAFields']) ? $DataArray['DAFields'] : array();
			
			foreach($DAFields as $DAFieldsVar => $DAFieldsVal) {
				$camposNVP .= $DAFieldsVal != '' ? '&' . strtoupper($DAFieldsVar) . '=' . urlencode($DAFieldsVal) : '';
			}
			
			$NVPRequest = $this->nvp_credentials . $camposNVP;
			$NVPResponse = $this->CURLRequest($NVPRequest);
			
			$NVPResponseArray = $this->NVPToArray($NVPResponse);

			if ($this->debug) {
				$Errors = $this->GetErrors($NVPResponseArray);
				$NVPRequestArray = $this->NVPToArray($NVPRequest);
				
				$NVPResponseArray['ERRORS'] = $Errors;
				$NVPResponseArray['REQUESTDATA'] = $NVPRequestArray;
				$NVPResponseArray['RAWREQUEST'] = $NVPRequest;
				$NVPResponseArray['RAWRESPONSE'] = $NVPResponse;
			}
										
			return $NVPResponseArray;
		}

		public function DoCapture ($DataArray) {
			$camposNVP = '&METHOD=DoCapture';
			
			$DCFields = isset($DataArray['DCFields']) ? $DataArray['DCFields'] : array();
			
			foreach($DCFields as $DCFieldsVar => $DCFieldsVal) {
				$camposNVP .= $DCFieldsVal != '' ? '&' . strtoupper($DCFieldsVar) . '=' . urlencode($DCFieldsVal) : '';
			}
			
			$NVPRequest = $this->nvp_credentials . $camposNVP;

			//echo $NVPRequest; exit;

			$NVPResponse = $this->CURLRequest($NVPRequest);
			
			$NVPResponseArray = $this->NVPToArray($NVPResponse);

			if ($this->debug) {
				$Errors = $this->GetErrors($NVPResponseArray);
				$NVPRequestArray = $this->NVPToArray($NVPRequest);
				
				$NVPResponseArray['ERRORS'] = $Errors;
				$NVPResponseArray['REQUESTDATA'] = $NVPRequestArray;
				$NVPResponseArray['RAWREQUEST'] = $NVPRequest;
				$NVPResponseArray['RAWRESPONSE'] = $NVPResponse;
			}
										
			return $NVPResponseArray;
		}

		public function DoVoid ($DataArray) {
			$camposNVP = '&METHOD=DoVoid';
			
			$DCFields = isset($DataArray['DCFields']) ? $DataArray['DCFields'] : array();
			
			foreach($DCFields as $DCFieldsVar => $DCFieldsVal) {
				$camposNVP .= $DCFieldsVal != '' ? '&' . strtoupper($DCFieldsVar) . '=' . urlencode($DCFieldsVal) : '';
			}
			
			$NVPRequest = $this->nvp_credentials . $camposNVP;

			//echo $NVPRequest; exit;

			$NVPResponse = $this->CURLRequest($NVPRequest);
			
			$NVPResponseArray = $this->NVPToArray($NVPResponse);

			if ($this->debug) {
				$Errors = $this->GetErrors($NVPResponseArray);
				$NVPRequestArray = $this->NVPToArray($NVPRequest);
				
				$NVPResponseArray['ERRORS'] = $Errors;
				$NVPResponseArray['REQUESTDATA'] = $NVPRequestArray;
				$NVPResponseArray['RAWREQUEST'] = $NVPRequest;
				$NVPResponseArray['RAWRESPONSE'] = $NVPResponse;
			}
										
			return $NVPResponseArray;
		}



		// FUNCIONES PRIVADAS //

		private function CURLRequest($request) {
			$curl = curl_init();
			// curl_setopt ($curl, CURLOPT_HEADER, TRUE);
			curl_setopt ($curl, CURLOPT_VERBOSE, 1);
			curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt ($curl, CURLOPT_TIMEOUT, 30);
			curl_setopt ($curl, CURLOPT_URL, $this->url_api);
			curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt ($curl, CURLOPT_POSTFIELDS, $request);
					
			/*if ($this->APIMode == 'Certificate') {
				curl_setopt($curl, CURLOPT_SSLCERT, $this->PathToCertKeyPEM);
			}*/
			
			$resp = curl_exec($curl);		
			curl_close($curl);
			
			return $resp;	
		}

		private function GetErrors ($DataArray) {
		
			$Errors = array();
			$n = 0;
			
			while(isset($DataArray['L_ERRORCODE' . $n . ''])) {
				$LErrorCode = isset($DataArray['L_ERRORCODE' . $n . '']) ? $DataArray['L_ERRORCODE' . $n . ''] : '';
				$LShortMessage = isset($DataArray['L_SHORTMESSAGE' . $n . '']) ? $DataArray['L_SHORTMESSAGE' . $n . ''] : '';
				$LLongMessage = isset($DataArray['L_LONGMESSAGE' . $n . '']) ? $DataArray['L_LONGMESSAGE' . $n . ''] : '';
				$LSeverityCode = isset($DataArray['L_SEVERITYCODE' . $n . '']) ? $DataArray['L_SEVERITYCODE' . $n . ''] : '';
				
				$CurrentItem = array(
									'L_ERRORCODE' => $LErrorCode, 
									'L_SHORTMESSAGE' => $LShortMessage, 
									'L_LONGMESSAGE' => $LLongMessage, 
									'L_SEVERITYCODE' => $LSeverityCode
									);
									
				array_push($Errors, $CurrentItem);
				$n++;
			}
			
			return $Errors;
		}
		
		private function NVPToArray($NVPString) {
			$proArray = array();

			while(strlen($NVPString)) {
				// name
				$keypos= strpos($NVPString,'=');
				$keyval = substr($NVPString,0,$keypos);
				// value
				$valuepos = strpos($NVPString,'&') ? strpos($NVPString,'&'): strlen($NVPString);
				$valval = substr($NVPString,$keypos+1,$valuepos-$keypos-1);
				// decoding the respose
				$proArray[$keyval] = urldecode($valval);
				$NVPString = substr($NVPString,$valuepos+1,strlen($NVPString));
			}
			
			return $proArray;
		}

		private function simplexml_to_array($xmlobj) {
			$a = array();
			foreach ($xmlobj->children() as $node) {
				if (count($node)) {
					$a[$node->getName()][] = $this->simplexml_to_array($node);
				}
				else {
					if (isset($a[$node->getName()])) {
						if (is_array($a[$node->getName()])) {
							array_push ($a[$node->getName()], (string) $node);
						}
						else {
							$tmp = $a[$node->getName()];

							$a[$node->getName()] = array ($tmp, (string) $node);
						}
					}
					else {
						$a[$node->getName()] = (string) $node;
					}
				}
			}
			return $a;
		}
	}