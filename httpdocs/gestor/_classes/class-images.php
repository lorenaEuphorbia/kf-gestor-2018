<?php

class Files {
	private $baseName;
	private $extensions;

	function __construct($baseName,$extensions) {
		$this->baseName = $baseName;
		$this->extensions = $extensions;
	}

	public function __destruct() {
		return true;
	}

	/// MÉTODOS PÚBLICOS ///

	public function deleteFile($filename) {
		if (file_exists($filename)) {
			unlink($filename);
			return true;
		}
		return false;
	}

	public function isImage($filename) {
		$strType = getMimeType($filename);
		return in_array($strType,Types::$IMAGE);
	}


	public function getExtension($strType) {
		return array_search($strType, Types::list());
	}

	public function getFileType($filename) {
		return Types::search(mime_content_type($filename));		
	}

	public function checkType($filename) {
		$arrMimes = Types::mimes($this->extensions);
		return in_array(mime_content_type($filename), $arrMimes);
	}

	public function getList($dirname="") {
		$arrFiles = [];
		$dirname = empty($dirname)? $this->baseName: $dirname;
		$arrMimes = Types::mimes($this->extensions);
		if ($handle = opendir($dirname)) {
			while (false !== ($filename = readdir($handle))) {

				if ($entry != "." && $entry != "..") continue;

				$file = $dirname.$filename;
				if(!is_dir($file)) {
					if(empty($extensions)) {
						$arrFiles[] = $file;
					}
					else if(array_search(mime_content_type($file), $arrMimes)) {
						$arrFiles[] = $file;
					}
				}
			}			
    		closedir($handle);
		}
		return $arrFiles;
    }
    
	public function upload($filename, $name="", $maxSize=500000) {
		//$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
		$strFilename = empty($name)? basename($filename): $name;
		$strTarget = $this->baseName.$strFilename;
		$arrErrors = [];

		# Tamaño del archivo
		if(!checkSize($filename, $maxSize)) $arrErrors[] = "El archivo supera los $maxSizeKb";
		# Comprobar si existe
		if (file_exists($strTarget)) $arrErrors[] = "Ya existe un archivo con ese nombre";
		# Comprobar tipo de archivo
		if(!checkType($filename, $extensions)) $arrErrors[] = "Sólo se permiten estos tipos de archivos: ".implode(", ", $array).".";

		if(empty($arrErrors)) {
			if (move_uploaded_file($filename, $strTarget)) {
				return ["result":"ok", "path":$strTarget];
			} 
			else return ["result":"ok" , "data":["Contacte con el administrador"], "path":$filename];
		}
		deleteFile($filename);
		return ["result":"error" , "data":$arrErrors ];
    }

    public function size($filename) {
		return getimagesize($file["tmp_name"]);
    }

    public function checkSize($filename, $maxSize=500000) {
    	$curSize = $this->size($file["tmp_name"]);
		return $curSize>0 && $curSize<$maxSize;
    }

	public function searchById($id) {
		return $this->filesTable->searchRecord(["id" => $id]);
	}

}

class Types {

	public static $TXT = [
        'txt' => 'text/plain',
        'htm' => 'text/html',
        'html' => 'text/html',
        'php' => 'text/html',
        'css' => 'text/css',
        'js' => 'application/javascript',
        'json' => 'application/json',
        'xml' => 'application/xml',
	];

	public static $IMAGE = [	
        // images
        'png' => 'image/png',
        'jpe' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
        'gif' => 'image/gif',
        'bmp' => 'image/bmp',
        'ico' => 'image/vnd.microsoft.icon',
        'tiff' => 'image/tiff',
        'tif' => 'image/tiff',
        'svg' => 'image/svg+xml',
        'svgz' => 'image/svg+xml',
	];

    public static $MEDIA = [
        // audio/video
        'mp3' => 'audio/mpeg',
        'qt' => 'video/quicktime',
        'mov' => 'video/quicktime',
        'swf' => 'application/x-shockwave-flash',
        'flv' => 'video/x-flv',
    ];

    public static $ADOBE = [        
        // adobe
        'pdf' => 'application/pdf',
        'psd' => 'image/vnd.adobe.photoshop',
        'ai' => 'application/postscript',
        'eps' => 'application/postscript',
        'ps' => 'application/postscript',
    ];

    public static $MS = [       
		// ms office
		'doc' => 'application/msword',
		'rtf' => 'application/rtf',
		'xls' => 'application/vnd.ms-excel',
		'ppt' => 'application/vnd.ms-powerpoint',
    ];

    public static $ARCHIVES = [
        // archives
        'zip' => 'application/zip',
        'rar' => 'application/x-rar-compressed',
        'exe' => 'application/x-msdownload',
        'msi' => 'application/x-msdownload',
        'cab' => 'application/vnd.ms-cab-compressed',
    ];

    public static $OPEN = [
        // open office
        'odt' => 'application/vnd.oasis.opendocument.text',
        'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
    ];

	public static function list() {
		return array_merge($TXT,$IMAGE,$MEDIA,$ADOBE,$MS,$ARCHIVES,$OPEN);
	}

	public static function search($strMimeType) {
		if(array_search($strMimeType, $TXT)) return "TXT";
		if(array_search($strMimeType, $IMAGE)) return "IMAGE";
		if(array_search($strMimeType, $MEDIA)) return "MEDIA";
		if(array_search($strMimeType, $ADOBE)) return "ADOBE";
		if(array_search($strMimeType, $MS)) return "MS";
		if(array_search($strMimeType, $ARCHIVES)) return "ARCHIVES";
		if(array_search($strMimeType, $OPEN)) return "OPEN";
		return "";
	}

	public function mimes($extensions) {
		return array_intersect_key(self::list(),  $extensions));
	}
}