<?php

class Secciones extends Controller {

	protected $idiomasTable, $seccionesTable, $textosTable, $clavesTable;

	public function __construct($db) {
		parent::__construct($db);

		$this->idiomasTable = new Table($db, "idiomas");
		$this->seccionesTable = new Table($db, "idiomas__secciones");
		$this->textosTable = new Table($db, "idiomas__textos");
		$this->clavesTable = new Table($db, "idiomas__claves");

		$this->isLoginRequired = true;
	}

	public function getTextosPorSeccion ($strRegion,$strSeccion) {
		$strQuery = 'SELECT
			    idiomas__claves.nombre,
			    idiomas__textos.valor
			FROM
			    idiomas__secciones,
			    idiomas__claves
			JOIN
			    idiomas
			LEFT JOIN
			    idiomas__textos ON
			    idiomas__textos.id_clave = idiomas__claves.id AND
			    idiomas__textos.id_idioma = idiomas.id
			WHERE
			    idiomas.region = :IdiomaRegion AND
			    idiomas.activo = 1 AND
			    idiomas__secciones.slug = :SlugSeccion AND
			    idiomas__claves.id_seccion = idiomas__secciones.id
			GROUP BY
			    idiomas__claves.id,
			    idiomas.id,
			    idiomas__secciones.id';

		$stmt = $this->db->prepare($strQuery);
		$stmt->execute([
			'IdiomaRegion'=>$strRegion,
			'SlugSeccion'=>$strSeccion,
		]);

		return $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
	}

	public function getSecciones ($isOcultas=true) {
		$arrFilter = [];
		if(!$isOcultas) $arrFilter['oculto'] = '1';

		return $this->getList('Secciones', [], $arrFilter, ['position DESC']);
	}

	public function getIdiomas ($isOcultas=true) {
		$arrFilter = ['activo'=>'1'];
		if(!$isOcultas) $arrFilter['oculto'] = '1';

		return $this->getList('Idiomas',[] , $arrFilter, ['region ASC']);
	}

	public function getTable ($option) {
		$table_name = strtolower($option) . "Table";

		if(isset($this->$table_name))
			return $this->$table_name;

		else
			if($this->search(['nombre'=>$option], 'Secciones'))
				return $this->clavesTable;

		return null;
	}
}
