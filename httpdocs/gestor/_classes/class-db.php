<?php

class Db {
	public $db, $dbType;
	protected $dbDriver, $dbHost, $dbPort, $dbName, $dbUser, $dbPass, $dbDsn, $dbAttr;

	public function __construct($driver, $host, $port, $name, $user, $pass) {
		$this->dbDriver = $driver;
		$this->dbHost = $host;
		$this->dbPort = $port;
		$this->dbName = $name;
		$this->dbUser = $user;
		$this->dbPass = $pass;

		switch ($this->dbDriver) {
			case "sqlsrv":
				//$this->dbDsn = "$this->dbDriver:Server=$this->dbHost,$this->dbPort;Database=$this->dbName";
				$this->dbDsn = "$this->dbDriver:Server=$this->dbHost,$this->dbPort;Database=$this->dbName";
				$this->dbAttr = [
					[PDO::ATTR_EMULATE_PREPARES, false],
				];
				$this->dbType = "MSSQL";
			break;
			default:
				$this->dbDsn = "$this->dbDriver:host=$this->dbHost;port=$this->dbPort;dbname=$this->dbName";
				$this->dbAttr = [
					[PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION],
					[PDO::ATTR_EMULATE_PREPARES, false],
					//[PDO::ATTR_PERSISTENT, true],
				];
				$this->dbType = "MySQL";
			break;
		}
	}

	public function __destruct() {
		return true;
	}

	public function checkSettings() {
		// Comprobamos que tenemos la extensión PDO para php
		if (!extension_loaded('PDO')) {
			echo "ERROR: driver PDO no disponible.";
			return false;
		}

		// Comprobamos que tenemos el driver requerido
		$pdo_drivers = PDO::getAvailableDrivers();

		if (!in_array($this->dbDriver, $pdo_drivers)) {
			echo "ERROR: driver \"$this->dbDriver\" para PDO no disponible.";
			return false;
		}
	}

	public function connect() {
		try {
			$this->db = new PDO($this->dbDsn, $this->dbUser, $this->dbPass);

			foreach ($this->dbAttr as $el)
				$this->db->setAttribute($el[0], $el[1]);

			$this->db->exec("set names utf8");
			$this->db->exec('set sql_mode = ""');

			return true;
		}
		catch (PDOException $e) {
			echo "Error de conexión con la base de datos.";
			//echo $e->getMessage();
			///P: gestión de excepciones

			return false;
		}
	}

	public function getTables() {
		$stmt = $this->db->prepare("SHOW TABLES");
		$stmt->execute();

		return $stmt->fetchAll(PDO::FETCH_COLUMN);
	}
}