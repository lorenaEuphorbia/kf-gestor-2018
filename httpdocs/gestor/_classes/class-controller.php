<?php

class Controller {
	protected $db;
	public $map_options = [];
	public $files;

	public function __construct($db) {
		$this->db = $db;
	}

	public function __destruct() {
		return true;
	}

	/// MÉTODOS PÚBLICOS ///

	public function getTable ($option) {
		$lowerOption = lcfirst($option);
		$table_name = $lowerOption . "Table";
		$parentOption = $this->getOptionParent ($option);

		if (isset($this->$table_name)) return $this->$table_name;
		if (isset($this->$lowerOption)) return $this->$lowerOption;
		if ($parentOption !== false) return $this->getTable($parentOption);

		trigger_error('No se ha encontrado la tabla para la opción: ' . $option);
		return null;
	}

	public function getOptionParent ($option) {
		foreach ($this->map_options as $clave => $arrValues) {
			if (array_search($option, $arrValues)!==false) return $clave;
		}

		return false;
	}

	public function getOptionSiblings ($option) {
		foreach ($this->map_options as $clave => $arrValues) {
			if (array_search($option, $arrValues)!==false) return $arrValues;
		}

		return false;
	}

	public function deleteRecord ($id, $option) {
		if (isset($this->files)) {
			$related_files = $this->files->getList(["id"], ["owner_id" => $id, "owner_type" => $option]);
			foreach ($related_files as $file) $this->files->deleteFile($file["id"]);
		}

		return $this->getTable ($option)->deleteRecord(["id" => $id]);
	}

	public function emptyRelTags($post_id) {
		return $this->relTagsTable->deleteRecord(["post_id" => $post_id]);
	}

	public function emptyRel($option, $primaryKey, $id) {
		return $this->getTable ($option)->deleteRecord([$primaryKey => $id]);
	}

	public function getCurrentFieldValue($field_name, $option) {
		return $this->getTable ($option)->getFieldCurrentValue($field_name);
	}

	public function getCurrent($option) {
		return $this->getTable ($option)->getData();
	}

	public function getFieldValue($fieldName, $recordId, $option) {
		return $this->getFieldValueFrom($fieldName, ['id'=>$recordId], $option);
	}

	public function getFieldValueFrom($fieldName, $arrParams, $option) {
		$list = $this->getList($option, [$fieldName], $arrParams);

		return count($list) === 1 ? $list[0][$fieldName] : "";
	}

	public function getLastInsertId($option) {
		return $this->getTable ($option)->getLastInsertId();
	}

	public function getList($option, $arrFields = null, $arrFilter = null, $arrOrder = null, $startAt = null, $numRecords = null, $fetchStyle = "FETCH_ASSOC") {
		return $this->getTable ($option)->selectRecords($arrFields, $arrFilter, $arrOrder, $startAt, $numRecords, $fetchStyle);
	}

	public function getCount($option, $arrFields = null, $arrFilter = null, $arrOrder = null, $startAt = null, $numRecords = null, $fetchStyle = "FETCH_ASSOC") {
		return $this->getTable ($option)->countRecords($arrFields, $arrFilter, $arrOrder, $startAt, $numRecords, $fetchStyle);
	}

	public function search($arrWhere, $option) {
		return $this->getTable ($option)->searchRecord($arrWhere);
	}

	public function unsetRecord( $option) {
		return $this->getTable ($option)->unsetRecord();
	}

	public function searchById($id, $option) {
		return $this->search(["id" => $id], $option);
	}

	/**
	 * Devuelve una lista indexada de las categorías de productos
	 * @return array 	Un array de categorías indexadas según la clave categorias
	 */
	public function getCategoriasIndexadas ($option = "Categorias") {
		return $this->getIndexList ($option );
	}

	/**
	 * Devuelve una lista indexada de las categorías de productos
	 * @return array 	Un array de categorías indexadas según la clave categorias
	 */
	public function getCategoriasIndexadasString ($option = "Categorias") {
		$arrCategorias = ['submenu' => $this->getIndexList ($option)];
		$arrCategoriasSalida = [];

		$this->getCategoriaString ($arrCategorias, $arrCategoriasSalida, '');
		return $arrCategoriasSalida;
	}

	public function getCategoriaString ($objCategoria, &$arrCategorias, $strPath) {
		if (!empty($objCategoria['submenu'])) {
			foreach ($objCategoria['submenu'] as $key => $objSubCategoria) {
				$arrCategorias[$objSubCategoria['id']] = $strPath . $objSubCategoria['nombre'];

				if (!empty($objCategoria['submenu'])) {
					$this->getCategoriaString ($objSubCategoria, $arrCategorias, $strPath . $objSubCategoria['nombre'] . ' > ');
				}
			}
		}

	}

	public function setRecord($arrFields, $arrWhere = [], $option) {

		if (empty($arrWhere)) {
			return $this->getTable ($option)->insertRecord($arrFields);
		}

		else {
			return $this->getTable ($option)->updateRecord($arrFields, $arrWhere);
		}
	}

	public function getRecord ($option, $arrFields, $arrParams, $forceUnique = false, $fetchStyle = "FETCH_ASSOC") {
		$numTotal = $forceUnique === false? 1:null;
		$result = $this->getList ($option, $arrFields, $arrParams, null, 0, $numTotal, $fetchStyle);
		if($result) {
			$numTotal = count($result);
			if($forceUnique==true && $numTotal>1) {
				trigger_error("Imposible forzar el campo Único, $numTotal registros encontrados.");
				return false;
			}
			else {
				return $result[0];
			}
		}
		else return false;
	}

	/* LOGIN */
	/*********/

	public function checkLogin($id,$option) {
		return true;
	}

	/* BLOG */
	/********/

	public function getCommentsByPost($postId, $arrFields = ["id"], $arrFilter = []) {
		if (empty($arrFilter)) $arrFilter = ["post_id" => $postId];
		else $arrFilter["post_id"] = $postId;

		return $this->getList("comments", $arrFields, $arrFilter);
	}

	public function getRelTags($idPost, $arrOrder = null) {
		return $this->relTagsTable->selectRecords(["tag_id"], ["post_id" => $idPost], $arrOrder, null, null, "FETCH_COLUMN");
	}

	public function setRelTag($tag_id, $post_id) {
		return $this->relTagsTable->insertRecord(["tag_id" => $tag_id, "post_id" => $post_id]);
	}

	public function getRel($option, $id, $claveSelect, $claveWhere, $arrOrder = null) {
		return $this->getTable ($option)->selectRecords([$claveSelect], [$claveWhere => $id], $arrOrder, null, null, "FETCH_COLUMN");
	}

	public function setRel($option, $idPrimary, $clavePrimary, $idSecundaria, $claveSecundaria) {
		return $this->getTable ($option)->insertRecord([$clavePrimary => $idPrimary, $claveSecundaria => $idSecundaria]);
	}

	/* requiere campo position */
	public function setPosition($id, $position, $act="Pages") {
		return $this->setRecord(
			[
				"id"	=> $id,
				"position"	=> $position
			],
			["id" => $id],
			$act
		);
	}

	/* requiere campo owner_id y position */
	public function setOrder($id, $owner, $position, $act="Pages") {
		return $this->setRecord(
			[
				"id"		=> $id,
				"owner_id"  => $owner,
				"position"	=> $position
			],
			["id" => $id],
			$act
		);
	}


	/**
	 * Devuelve una lista indexada de la opción. Es necesario un campo position y otro owner_id
	 * @return array 	Un array elementos indexados
	 */
	public function getIndexList ($option="Buttons",$ownerId = 0) {
		return $this->createIndex($this, $ownerId, $option);
	}


	public function createIndex ($context, $ownerId, $option="Buttons") {
		$arr_tmp = $context->createSubIndex($ownerId,['position DESC'],$option);

		$num_buttons = count($arr_tmp);
		for ($i=0; $i < $num_buttons; $i++) {
			$arr_tmp[$i]['submenu'] = $this->createIndex($context, $arr_tmp[$i]['id'], $option);
		}

		return $arr_tmp;
	}
	public function createSubIndex ($ownerId, $arrOrder ,$option="Buttons") {
		$table_name = $this->getTable ($option);

		return $this->getList(
			$option,
			[],
			['owner_id' => $ownerId],
			$arrOrder
		);
	}
}
