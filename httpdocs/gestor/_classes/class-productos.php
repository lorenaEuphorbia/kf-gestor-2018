<?php

class Productos extends Controller {
	protected $productosTable, $categoriasTable;

	public function __construct ($db) {
		$this->db = $db;

		$this->productosTable = new Table($db, 'productos');
		$this->categoriasTable = new Table($db, 'productos__categorias');
		$this->commentsTable = new Table($db, 'productos__comments');
		$this->tagsTable = new Table($db, 'productos__tags');
		$this->relTagsTable = new Table($db, 'productos__rel_tags');
		$this->relaccionadosTable = new Table($db, 'productos__relaccionados');
		$this->galeriaTable = new Table($db, 'productos__galeria');
		$this->valoresTable = new Table($db, 'productos__valores');

		$this->files = new Files($db, 'productos');

		$this->map_options = ['Files' => ['Galeria','Archivos'] ];
	}


	/**
	 * Devuelve la información de un producto según su slug
	 * @param  String 	$strSlug 	el Slug o url única del producto
	 * @return array() 				el objeto del producto o false si no se encuentra
	 */
	public function getProducto ($strSlug) {
		$result = $this->getRecord ('Productos', null, ['url'=>$strSlug]);
		if($result) {
			$result['imagen'] = $this->getProductoImagen ($result['id'], $result['codigo']);
			$result['precio'] = number_format($result['precio'],2);
			unset($result['contenido']);
		}
		return $result;
	}

	/**
	 * Devuelve la información de un producto según su slug
	 * @param  int 	$id 	el ID de producto
	 * @return array() 				el objeto del producto o false si no se encuentra
	 */
	public function getProductoById ($id) {
		$result = $this->getRecord ('Productos', null, ['id'=>$id]);
		if($result) {
			$result['imagen'] = $this->getProductoImagen ($result['id'], $result['codigo']);
			$result['precio'] = number_format($result['precio'],2);
			unset($result['contenido']);
		}
		return $result;
	}

	/**
	 * Devuelve la imagen de un producto
	 * @param  int $id el código id del producto
	 * @return string     La ruta absoluta de la imagen, o false si el producto no tiene imagen
	 */
	public function getProductoImagen ($id, $codigo, $tipo = 'Galeria') {
		return $this->files->searchByOwner($id, $codigo, $tipo)? $this->files->getFilePath(): false;
	}

	/**
	 * Devuelve los nombres de todos los productos
	 * @return 	array<int,String> 	Un array con el id como clave y su nombre como valor
	 */
	public function getProductosNombres ()
	{
		return	$this->getList('Productos', ['id','nombre'], [], ['nombre'],null,null,'FETCH_KEY_PAIR');
	}


	/**
	 * Devuelve un listado de productos según la categoría
	 * @param  String 	$idCategoria 		El id de la categoría a buscar. Si no hay categoría no se aplicará ese filtro
	* @return array() 						Una lista de objetos productos
	 */
	public function getProductos ($idCategoria = false) {
		$arrParams = ['id','nombre','url','id_categoria','precio','etiqueta'];
		$arrWhere = [];
		$arrOrder = ['url ASC'];

		# filters
		if($idCategoria!==false) 	$arrWhere['id_categoria'] 	= $idCategoria;

		$result = $this->productosTable->selectRecords ($arrParams, $arrWhere, $arrOrder);
		foreach ($result as $index => $valor) {
			$valor['imagen'] = $this->getProductoImagen ($valor['id']);
			$valor['precio'] = number_format($valor['precio'],2);
			$arrProductos[$valor['id']] = $valor;
		}

		return $arrProductos;
	}

	/**
	 * Devuelve una lista asociativa de categorías de productos
	 * @return array()  Array<id,Array> 	Un array cuya clave es el id de categoría y el objeto su valor
	 */
	public function getCategorias () {
		$arrParams = [];
		$arrWhere = [];
		$arrOrder = ['position ASC'];

		$result = $this->categoriasTable->selectRecords ($arrParams, $arrWhere, $arrOrder);
		for ($i=0; $i < count ($result); $i++) {
			$arrCategorias[$result[$i]['id']] = $result[$i];
		}

		// var_dump($arrCategorias);
		return $arrCategorias;
	}

	/**
	 * Devuelve una lista asociativa de nombres de categorías de productos
	 * @return array()  Array<id,Array> 	Un array cuya clave es el id de categoría y el nombre su valor
	 */
	public function getNombresCategorias () {
		$arrParams = ['id','nombre'];
		$arrWhere = [];
		$arrOrder = ['position ASC'];

		$result = $this->categoriasTable->selectRecords ($arrParams, $arrWhere, $arrOrder,null,null,'FETCH_KEY_PAIR');

		// var_dump($arrCategorias);
		return $result;
	}


}
