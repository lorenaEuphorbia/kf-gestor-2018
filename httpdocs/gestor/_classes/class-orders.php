<?php

class Orders extends Controller {
	protected $base, $db, $ordersTable, $articulosTable, $requestTable, $refundsTable;
	public $productos, $security;

	public function __construct($db, $productos, $security, $portes = null) {
		$this->db = $db;
		$this->productos = $productos;
		$this->security = $security;
		$this->portes = $portes;

		$this->ordersTable = new Table($this->db, "orders");
		$this->articulosTable = new Table($this->db, "orders__articulos");
		$this->requestTable = new Table($this->db, "orders__request");
		$this->refundsTable = new Table($this->db, "orders__refunds");
	}

	public function __destruct() {
	}

	/// MÉTODOS PÚBLICOS ///
	
	public function getArticulos ($idOrder) {
		$arrArticulos = $this->getList ('Articulos', null, ['id_order'=>$idOrder]);

		if (!empty($arrArticulos)) {
			return $arrArticulos;
		}
		return [];
	}

	public function getOrder ($idRequest) {
		return $this->getRecord ('Orders', null, ['request_id'=>$idRequest], true);
	}

	public function closeOrder () {
	}
	public function closeRequest () {
	}

	public function createOrder ($customerData = []) {
		$strCode = Utilities::getUniqueCode();

		$arr_fields = [
			"code"			=> $strCode,
			"creation_date"	=> Date("Y-m-d H:i:s")
		];

		$arr_fields["codigo_cliente"]				= !(empty($customerData['id'])) ? $customerData['id'] : 'null';
		$arr_fields["nombre"]						= !(empty($customerData['nombre'])) ? $customerData['nombre'] : 'null';
		$arr_fields["apellidos"]					= !(empty($customerData['apellidos'])) ? $customerData['apellidos'] : 'null';
		$arr_fields["email"]						= !(empty($customerData['email'])) ? $customerData['email'] : 'null';
		$arr_fields["nif"]							= !(empty($customerData['cif'])) ? $customerData['cif'] : 'null';
		$arr_fields["telefono"]						= !(empty($customerData['telefono'])) ? $customerData['telefono'] : 'null';
		$arr_fields["fecha_nacimiento"]				= !(empty($customerData['fecha_nacimiento'])) ? $customerData['fecha_nacimiento'] : 'null';
		$arr_fields["nif_dir_facturacion"]			= !(empty($customerData['cif'])) ? $customerData['cif'] : 'null';
		$arr_fields["nombre_dir_facturacion"]		= !(empty($customerData['razon'])) ? $customerData['razon'] : 'null';
		$arr_fields["codigo_dir_facturacion"]		= 'null';
		$arr_fields["direccion_dir_facturacion"]	= !(empty($customerData['facturacion_direccion'])) ? $customerData['facturacion_direccion'] : 'null';
		$arr_fields["cp_dir_facturacion"]			= !(empty($customerData['facturacion_codigo_postal'])) ? $customerData['facturacion_codigo_postal'] : 'null';
		$arr_fields["localidad_dir_facturacion"]	= !(empty($customerData['facturacion_localidad'])) ? $customerData['facturacion_localidad'] : 'null';
		$arr_fields["provincia_dir_facturacion"]	= !(empty($customerData['facturacion_provincia'])) ? $customerData['facturacion_provincia'] : 'null';
		$arr_fields["pais_dir_facturacion"]			= !(empty($customerData['facturacion_pais'])) ? $customerData['facturacion_pais'] : 'null';
		$arr_fields["codigo_dir_envio"]				= 'null';
		$arr_fields["nombre_dir_envio"]				= !(empty($customerData['envio_nombre'])) ? $customerData['envio_nombre'] : 'null';
		$arr_fields["direccion_dir_envio"]			= !(empty($customerData['envio_direccion'])) ? $customerData['envio_direccion'] : 'null';
		$arr_fields["cp_dir_envio"]					= !(empty($customerData['envio_codigo_postal'])) ? $customerData['envio_codigo_postal'] : 'null';
		$arr_fields["localidad_dir_envio"]			= !(empty($customerData['envio_localidad'])) ? $customerData['envio_localidad'] : 'null';
		$arr_fields["provincia_dir_envio"]			= !(empty($customerData['envio_provincia'])) ? $customerData['envio_provincia'] : 'null';
		$arr_fields["pais_dir_envio"]				= !(empty($customerData['envio_pais'])) ? $customerData['envio_pais'] : 'null';
		$arr_fields["envioEmpresa"]					= 'null';
		$arr_fields["envioImporte"]					= 'null';
		$arr_fields["envioObservaciones"]			= 'null';
		$arr_fields["formaDePago"]					= 'tarjeta';
		$arr_fields["is_factura"]					= '0';
		$arr_fields["is_envolver_regalo"]			= '0';
		$arr_fields["is_tarjeta_regalo"]			= '0';
		$arr_fields["str_tarjeta_regalo"]			= 'null';
		$arr_fields["vale"]							= '0';
		$arr_fields["pvp_total"]					= '0';
		$arr_fields["pagando"]						= '0';
		$arr_fields["pagandoFecha"]					= 'null';

		$result = $this->ordersTable->insertRecord($arr_fields);

		if ($result) return $this->getRecord ('Orders', null, ['code'=>$strCode], true);
		else return false;
	}

	/**
	 * Método que actualiza los productos de un determinado pedido.
	 * Se debe comprobar que el pedido no sea histórico antes
	 * @param  string 	$idOrder    	El código del pedido
	 * @param  Array 	$arrProduct 	Array asociativo de productos. La clave es su código y el valor un array asociativo cuyo código es la talla del artículo y el valor las unidades 
	 * @return boolean	true si se realizó, false si hubo error
	 */
	public function updateProductsOrder ($idOrder,$arrProduct) {
		$numRegistros = 0;

		# búsqueda del pedido
		$objRequest = $this->requestTable->selectRecords([], ['id_order'=>$idOrder], null, 0, 1);
		$objOrder = $this->ordersTable->selectRecords([], ['id'=>$idOrder], null, 0, 1);
		$objOrder = $objOrder? $objOrder[0]: false;
		$objRequest = $objRequest? $objRequest[0]: false;

		if($objOrder && ($objRequest == false || !$objRequest['historico'])) {
			# limpiar artículos
			$this->articulosTable->deleteRecord(['id_order'=>$idOrder]);
			foreach ($arrProduct as $objProduct) {
				if($this->articulosTable->insertRecord($objProduct)) {
					$numRegistros++;
				}
				else {
					$numRegistros = false;
					trigger_error("No se puede añadir el artículo '$codigo' en la tabla '$articulosTable->tableName'.", E_USER_ERROR);
					break;
				}

			}
		} else {
			$numRegistros = false;
			trigger_error("El pedido '$idOrder' no se encuentra disponible.", E_USER_ERROR);		
		}
		return $numRegistros;
	}

	/**
	 * Método que actualiza los productos del pedido actual.
	 * Se debe comprobar que el pedido no sea histórico antes
	 * @param  Array 	$arrProduct 	Array asociativo de productos. La clave es su código y el valor un array asociativo cuyo código es la talla del artículo y el valor las unidades 
	 * @return boolean	true si se realizó, false si hubo error
	 */
	public function updateCurrentProductsOrder ($arrProduct) {
		return $this->objOrder && $this->updateProductsOrder($this->objOrder['id'],$arrProduct);
	}


	public function updateOrder($orderId, $customerData = null) {
		$arr_fields = [];

		if (!empty($customerData)) {
			$arr_fields["codigo_cliente"]				= !(empty($customerData['id'])) ? $customerData['id'] : 'null';
			$arr_fields["nombre"]						= !(empty($customerData['nombre'])) ? $customerData['nombre'] : 'null';
			$arr_fields["apellidos"]					= !(empty($customerData['apellidos'])) ? $customerData['apellidos'] : 'null';
			$arr_fields["email"]						= !(empty($customerData['email'])) ? $customerData['email'] : 'null';
			$arr_fields["nif"]							= !(empty($customerData['cif'])) ? $customerData['cif'] : 'null';
			$arr_fields["telefono"]						= !(empty($customerData['telefono'])) ? $customerData['telefono'] : 'null';
			$arr_fields["fecha_nacimiento"]				= !(empty($customerData['fecha_nacimiento'])) ? $customerData['fecha_nacimiento'] : 'null';
			$arr_fields["nif_dir_facturacion"]			= !(empty($customerData['cif'])) ? $customerData['cif'] : 'null';
			$arr_fields["nombre_dir_facturacion"]		= !(empty($customerData['razon'])) ? $customerData['razon'] : 'null';
			$arr_fields["direccion_dir_facturacion"]	= !(empty($customerData['facturacion_direccion'])) ? $customerData['facturacion_direccion'] : 'null';
			$arr_fields["cp_dir_facturacion"]			= !(empty($customerData['facturacion_codigo_postal'])) ? $customerData['facturacion_codigo_postal'] : 'null';
			$arr_fields["localidad_dir_facturacion"]	= !(empty($customerData['facturacion_localidad'])) ? $customerData['facturacion_localidad'] : 'null';
			$arr_fields["provincia_dir_facturacion"]	= !(empty($customerData['facturacion_provincia'])) ? $customerData['facturacion_provincia'] : 'null';
			$arr_fields["pais_dir_facturacion"]			= !(empty($customerData['facturacion_pais'])) ? $customerData['facturacion_pais'] : 'null';
			$arr_fields["nombre_dir_envio"]				= !(empty($customerData['envio_nombre'])) ? $customerData['envio_nombre'] : 'null';
			$arr_fields["direccion_dir_envio"]			= !(empty($customerData['envio_direccion'])) ? $customerData['envio_direccion'] : 'null';
			$arr_fields["cp_dir_envio"]					= !(empty($customerData['envio_codigo_postal'])) ? $customerData['envio_codigo_postal'] : 'null';
			$arr_fields["localidad_dir_envio"]			= !(empty($customerData['envio_localidad'])) ? $customerData['envio_localidad'] : 'null';
			$arr_fields["provincia_dir_envio"]			= !(empty($customerData['envio_provincia'])) ? $customerData['envio_provincia'] : 'null';
			$arr_fields["pais_dir_envio"]				= !(empty($customerData['envio_pais'])) ? $customerData['envio_pais'] : 'null';
		}

		return $this->ordersTable->updateRecord($arr_fields, ["id" => $orderId]);
	}

	public function updateOrderData ($objOrder) {
		$arrParams = [
			'codigo_cliente' => $objOrder['codigo_cliente'],
			'nombre' => $objOrder['nombre'],
			'apellidos' => $objOrder['apellidos'],
			'email' => $objOrder['email'],
			'nif' => $objOrder['nif'],
			'telefono' => $objOrder['telefono'],
			'fecha_nacimiento' => $objOrder['fecha_nacimiento'],
			'nif_dir_facturacion' => $objOrder['nif_dir_facturacion'],
			'nombre_dir_facturacion' => $objOrder['nombre_dir_facturacion'],
			'codigo_dir_facturacion' => $objOrder['codigo_dir_facturacion'],
			'direccion_dir_facturacion' => $objOrder['direccion_dir_facturacion'],
			'cp_dir_facturacion' => $objOrder['cp_dir_facturacion'],
			'localidad_dir_facturacion' => $objOrder['localidad_dir_facturacion'],
			'provincia_dir_facturacion' => $objOrder['provincia_dir_facturacion'],
			'pais_dir_facturacion' => $objOrder['pais_dir_facturacion'],
			'codigo_dir_envio' => $objOrder['codigo_dir_envio'],
			'nombre_dir_envio' => $objOrder['nombre_dir_envio'],
			'direccion_dir_envio' => $objOrder['direccion_dir_envio'],
			'cp_dir_envio' => $objOrder['cp_dir_envio'],
			'localidad_dir_envio' => $objOrder['localidad_dir_envio'],
			'provincia_dir_envio' => $objOrder['provincia_dir_envio'],
			'pais_dir_envio' => $objOrder['pais_dir_envio'],
			'envioEmpresa' => $objOrder['envioEmpresa'],
			'envioImporte' => $objOrder['envioImporte'],
			'envioObservaciones' => $objOrder['envioObservaciones'],
			'formaDePago' => $objOrder['formaDePago'],
			'is_factura' => $objOrder['is_factura'],
			'is_envolver_regalo' => $objOrder['is_envolver_regalo'],
			'is_tarjeta_regalo' => $objOrder['is_tarjeta_regalo'],
			'str_tarjeta_regalo' => $objOrder['str_tarjeta_regalo'],
			'vale' => $objOrder['vale'],
			'pvp_total' => $objOrder['pvp_total'],
			'peso_total' => $objOrder['peso_total'],
			'modification_date' => date('Y-m-d h:m:s'),
		];
		return $this->ordersTable->updateRecord($arrParams, ["id"=>$objOrder['id'],"code"=>$objOrder["code"],'historico'=>'0','request_id'=>['IS','null']]);
	}

	public function setOrderData($orderId, $customerData = []) {
		return $this->ordersTable->updateRecord($customerData, ["id" => $orderId]);
	}

	public function setCurrentOrder($customerData) {
		//echo $this->getField("id"); exit;
		return $this->setOrderData($this->getCurrentFieldValue("id","Orders"),$customerData);
	}

	public function setRequestData($orderData = [], $idRequest = null) {
		if($orderData) {
			# parámetros web
			$arrParams = array();
			if (!empty($orderData['response'])) $arrParams['response'] = $orderData['response'];
			if (!empty($orderData['modification'])) $arrParams['modification'] = $orderData['modification'];
			if (!empty($orderData['creacion'])) $arrParams['creacion'] = $orderData['creacion'];
			if (!empty($orderData['id_order'])) $arrParams['id_order'] = $orderData['id_order'];
			if (!empty($orderData['historico'])) $arrParams['historico'] = $orderData['historico'];
			if (!empty($orderData['tipo'])) $arrParams['tipo'] = $orderData['tipo'];
			if (!empty($orderData['articulos'])) $arrParams['articulos'] = $orderData['articulos'];
			if (!empty($orderData['pvp_articulos'])) $arrParams['pvp_articulos'] = $orderData['pvp_articulos'];
			if (!empty($orderData['pvp_total'])) $arrParams['pvp_total'] = $orderData['pvp_total'];
			if (!empty($orderData['pvp_envio'])) $arrParams['pvp_envio'] = $orderData['pvp_envio'];
			if (!empty($orderData['pvp_iva'])) $arrParams['pvp_iva'] = $orderData['pvp_iva'];
			if (!empty($orderData['pvp_descuento'])) $arrParams['pvp_descuento'] = $orderData['pvp_descuento'];
			if (!empty($orderData['pvp_pago'])) $arrParams['pvp_pago'] = $orderData['pvp_pago'];
			if (!empty($orderData['pvp_tarjeta'])) $arrParams['pvp_tarjeta'] = $orderData['pvp_tarjeta'];
			if (!empty($orderData['pvp_envolver'])) $arrParams['pvp_envolver'] = $orderData['pvp_envolver'];
			if (!empty($orderData['pvp_puntos'])) $arrParams['pvp_puntos'] = $orderData['pvp_puntos'];
			if (!empty($orderData['errors'])) $arrParams['errors'] = $orderData['errors'];

			// var_dump($arrParams);
			$arrWhere = $idRequest===null? null: ['id'=>$idRequest];
			return $this->setRecord($arrParams, $arrWhere, 'Request');

		} return false;
	}
	public function setRequestResult($responseData = [], $idRequest = null) {
		# recogemos la pasarela
		$objRequest = $this->requestTable->searchRecord(['id'=>$idRequest]) ? $this->requestTable->getData() : false;
		if($objRequest) {
			# recogemos el pedido
			$objOrder = $this->ordersTable->searchRecord(['id'=>$objRequest['id_order']]) ? $this->ordersTable->getData() : false;
			if($objOrder) {
				# guardamos la pasarela
				$arrParamsRequest = array();
				$arrParamsRequest['response'] = Utilities::encode($responseData);
				$arrParamsRequest['historico'] = '1';		
				if(!$this->setRequestData($arrParamsRequest, $idRequest)) {
					trigger_error('No se ha podido guardar la respuesta del pedido pagado');
				}
				# guardamos el pedido
				$arrParamsOrder = array();
				$arrParamsOrder['request_id'] = $idRequest;
				$arrParamsOrder['historico'] = '1';
				$arrParamsOrder['pagando'] = '0';
				if(!$this->setOrderData($objOrder['id'],$arrParamsOrder)) {
					trigger_error('No se ha podido guardar el pedido pagado');
				}
				return true;
			}
			else {
				trigger_error('No se ha encontrado el pedido');
			}		
		}
		else {
			trigger_error('No se ha encontrado la solicitud del pedido');
		}
		return false;
	}

	public function getData($option = 'Orders') {
		$this->getTable($option)->getData();
	}

	public function getCountries () {
		return array (
			'ES' => 'España',
			'AF' => 'Afghanistan',
			'AX' => 'Åland Islands',
			'AL' => 'Albania',
			'DZ' => 'Algeria',
			'VI' => 'American Virg.Islnd',
			'AD' => 'Andorra',
			'AO' => 'Angola',
			'AI' => 'Anguilla',
			'AQ' => 'Antarctica',
			'AG' => 'Antigua/Barbuda',
			'ARG' => 'Argelia',
			'AR' => 'Argentina',
			'AM' => 'Armenia',
			'AW' => 'Aruba',
			'AU' => 'Australia',
			'AT' => 'Austria',
			'AZ' => 'Azerbaijan',
			'BS' => 'Bahamas',
			'BH' => 'Bahrain',
			'BD' => 'Bangladesh',
			'BB' => 'Barbados',
			'BY' => 'Belarus',
			'BE' => 'Belgica',
			'BZ' => 'Belize',
			'BJ' => 'Benin',
			'BM' => 'Bermuda',
			'BT' => 'Bhutan',
			'BO' => 'Bolivia',
			'BA' => 'Bosnia-Herzegovina',
			'BW' => 'Botswana',
			'BV' => 'Bouvet Island',
			'BR' => 'Brazil',
			'IO' => 'Brit.Ind.Oc.Ter',
			'VG' => 'British Virg. Islnd',
			'BN' => 'Brunei Dar-es-S',
			'BG' => 'Bulgaria',
			'BF' => 'Burkina-Faso',
			'BI' => 'Burundi',
			'KH' => 'Cambodia',
			'CM' => 'Cameroon',
			'CA' => 'Canada',
			'CV' => 'Cape Verde',
			'KY' => 'Cayman Islands',
			'CF' => 'Central African Rep',
			'TD' => 'Chad',
			'CL' => 'Chile',
			'CN' => 'China',
			'CX' => 'Christmas Island',
			'CC' => 'Coconut Islands',
			'CO' => 'Colombia',
			'KM' => 'Comoros',
			'CG' => 'Congo',
			'CD' => 'Congo',
			'CK' => 'Cook Islands',
			'CR' => 'Costa Rica',
			'HR' => 'Croatia',
			'CU' => 'Cuba',
			'CY' => 'Cyprus',
			'CZ' => 'Czech Republic',
			'DK' => 'Denmark',
			'DJ' => 'Djibouti',
			'DM' => 'Dominica',
			'DO' => 'Dominican Republic',
			'AN' => 'Dutch Antilles',
			'TL' => 'East Timor',
			'TP' => 'East Timor',
			'EC' => 'Ecuador',
			'EG' => 'Egypt',
			'SV' => 'El Salvador',
			'GQ' => 'Equatorial Guinea',
			'ER' => 'Eritrea',
			'EE' => 'Estonia',
			'ET' => 'Ethiopia',
			'FK' => 'Falkland Islands',
			'FO' => 'Faroe Islands',
			'FJ' => 'Fiji',
			'FI' => 'Finland',
			'FR' => 'France',
			'GF' => 'French Guayana',
			'PF' => 'French Polynesia',
			'TF' => 'French S.Territ',
			'GA' => 'Gabon',
			'GM' => 'Gambia',
			'GE' => 'Georgia',
			'DE' => 'Germany',
			'GH' => 'Ghana',
			'GI' => 'Gibraltar',
			'GR' => 'Greece',
			'EL' => 'Greece',
			'GL' => 'Greenland',
			'GD' => 'Grenada',
			'GP' => 'Guadeloupe',
			'GU' => 'Guam',
			'GT' => 'Guatemala',
			'GG' => 'Guernsey',
			'GN' => 'Guinea',
			'GW' => 'Guinea-Bissau',
			'GY' => 'Guyana',
			'HT' => 'Haiti',
			'HM' => 'Heard/McDnld Islnds',
			'HN' => 'Honduras',
			'HK' => 'Hong Kong',
			'HU' => 'Hungary',
			'IS' => 'Iceland',
			'IN' => 'India',
			'ID' => 'Indonesia',
			'IR' => 'Iran',
			'IQ' => 'Iraq',
			'IE' => 'Ireland',
			'IM' => 'Isle of Man',
			'IL' => 'Israel',
			'IT' => 'Italy',
			'CI' => 'Ivory Coast',
			'JM' => 'Jamaica',
			'JP' => 'Japan',
			'JE' => 'Jersey',
			'JO' => 'Jordan',
			'KZ' => 'Kazakhstan',
			'KE' => 'Kenya',
			'KI' => 'Kiribati',
			'KW' => 'Kuwait',
			'KG' => 'Kyrgyzstan',
			'LA' => 'Laos',
			'LV' => 'Latvia',
			'LB' => 'Lebanon',
			'LS' => 'Lesotho',
			'LR' => 'Liberia',
			'LY' => 'Libya',
			'LI' => 'Liechtenstein',
			'LT' => 'Lithuania',
			'LU' => 'Luxembourg',
			'MO' => 'Macau',
			'MK' => 'Macedonia',
			'MG' => 'Madagascar',
			'MW' => 'Malawi',
			'MY' => 'Malaysia',
			'MV' => 'Maldives',
			'ML' => 'Mali',
			'MT' => 'Malta',
			'MH' => 'Marshall Islands',
			'MQ' => 'Martinique',
			'MR' => 'Mauretania',
			'MU' => 'Mauritius',
			'YT' => 'Mayotte',
			'MX' => 'Mexico',
			'FM' => 'Micronesia',
			'UM' => 'Minor Outl.Ins.',
			'MD' => 'Moldavia',
			'MC' => 'Monaco',
			'MN' => 'Mongolia',
			'ME' => 'Montenegro',
			'MS' => 'Montserrat',
			'MA' => 'Morocco',
			'MZ' => 'Mozambique',
			'MM' => 'Myanmar',
			'MP' => 'N.Mariana Island',
			'NA' => 'Namibia',
			'NR' => 'Nauru',
			'NP' => 'Nepal',
			'NL' => 'Netherlands',
			'NC' => 'New Caledonia',
			'NZ' => 'New Zealand',
			'NI' => 'Nicaragua',
			'NE' => 'Niger',
			'NG' => 'Nigeria',
			'NU' => 'Niue Islands',
			'NF' => 'Norfolk Island',
			'KP' => 'North Korea',
			'NO' => 'Norway',
			'OM' => 'Oman',
			'PK' => 'Pakistan',
			'PW' => 'Palau',
			'PS' => 'Palestinian Territory',
			'PA' => 'Panama',
			'PG' => 'Papua New Guinea',
			'PY' => 'Paraguay',
			'PE' => 'Peru',
			'PH' => 'Philippines',
			'PN' => 'Pitcairn Islands',
			'PL' => 'Poland',
			'PT' => 'Portugal',
			'PR' => 'Puerto Rico',
			'QA' => 'Qatar',
			'RE' => 'Reunion',
			'RO' => 'Romania',
			'RW' => 'Ruanda',
			'RU' => 'Russian Fed.',
			'GS' => 'S. Sandwich Ins',
			'ST' => 'S.Tome,Principe',
			'BL' => 'Saint Barthelemy',
			'MF' => 'Saint Martin',
			'AS' => 'Samoa American',
			'SM' => 'San Marino',
			'SA' => 'Saudi Arabia',
			'CH' => 'Schweiz',
			'SN' => 'Senegal',
			'RS' => 'Serbia',
			'CS' => 'Serbia Montenegro',
			'SC' => 'Seychelles',
			'SL' => 'Sierra Leone',
			'SG' => 'Singapore',
			'SK' => 'Slovakia',
			'SI' => 'Slovenia',
			'SB' => 'Solomon Islands',
			'SO' => 'Somalia',
			'ZA' => 'South Africa',
			'KR' => 'South Korea',
			'LK' => 'Sri Lanka',
			'KN' => 'St Kitts & Nevis',
			'SH' => 'St. Helena',
			'LC' => 'St. Lucia',
			'VC' => 'St. Vincent',
			'PM' => 'St.Pier,Miquel.',
			'SD' => 'Sudan',
			'SR' => 'Suriname',
			'SJ' => 'Svalbard',
			'SZ' => 'Swaziland',
			'SE' => 'Sweden',
			'SY' => 'Syria',
			'TW' => 'Taiwan',
			'TJ' => 'Tajikstan',
			'TZ' => 'Tanzania',
			'TH' => 'Thailand',
			'TG' => 'Togo',
			'TK' => 'Tokelau Islands',
			'TO' => 'Tonga',
			'TT' => 'Trinidad,Tobago',
			'TN' => 'Tunisia',
			'TR' => 'Turkey',
			'TM' => 'Turkmenistan',
			'TC' => 'Turksh Caicosin',
			'TV' => 'Tuvalu',
			'UG' => 'Uganda',
			'UA' => 'Ukraine',
			'AE' => 'United Arab Emir.',
			'GB' => 'United Kingdom',
			'UY' => 'Uruguay',
			'US' => 'USA',
			'UZ' => 'Uzbekistan',
			'VU' => 'Vanuatu',
			'VA' => 'Vatican City',
			'VE' => 'Venezuela',
			'VN' => 'Vietnam',
			'WF' => 'Wallis,Futuna',
			'EH' => 'West Sahara',
			'WS' => 'Western Samoa',
			'YE' => 'Yemen',
			'ZM' => 'Zambia',
			'ZW' => 'Zimbabwe'
		);
	}

	public function getSpainProvinces () {
		return array (
			'ALAVA' => 'Álava',
			'ALBACETE' => 'Albacete',
			'ALICANTE' => 'Alicante',
			'ALMERIA' => 'Almería',
			'ASTURIAS' => 'Asturias',
			'AVILA' => 'Ávila',
			'BADAJOZ' => 'Badajoz',
			'BARCELONA' => 'Barcelona',
			'BURGOS' => 'Burgos',
			'CACERES' => 'Cáceres',
			'CADIZ' => 'Cádiz',
			'CANTABRIA' => 'Cantabria',
			'CASTELLON' => 'Castellón',
			'CEUTA' => 'Ceuta',
			'CIUDAD REAL' => 'Ciudad Real',
			'CORDOBA' => 'Córdoba',
			'CUENCA' => 'Cuenca',
			'GERONA' => 'Gerona',
			'GRANADA' => 'Granada',
			'GUADALAJARA' => 'Guadalajara',
			'GUIPUZCOA' => 'Guipúzcoa',
			'HUELVA' => 'Huelva',
			'HUESCA' => 'Huesca',
			'ISLAS BALEARES' => 'Islas Baleares',
			'JAEN' => 'Jaén',
			'LA CORUÑA' => 'La Coruña',
			'LA RIOJA' => 'La Rioja',
			'LAS PALMAS' => 'Las Palmas',
			'LEON' => 'León',
			'LLEIDA' => 'Lleida',
			'LUGO' => 'Lugo',
			'MADRID' => 'Madrid',
			'MALAGA' => 'Málaga',
			'MELILLA' => 'Melilla',
			'MURCIA' => 'Murcia',
			'NAVARRA' => 'Navarra',
			'ORENSE' => 'Orense',
			'PALENCIA' => 'Palencia',
			'PONTEVEDRA' => 'Pontevedra',
			'SALAMANCA' => 'Salamanca',
			'SEGOVIA' => 'Segovia',
			'SEVILLA' => 'Sevilla',
			'SORIA' => 'Soria',
			'TARRAGONA' => 'Tarragona',
			'TENERIFE' => 'Tenerife',
			'TERUEL' => 'Teruel',
			'TOLEDO' => 'Toledo',
			'VALENCIA' => 'Valencia',
			'VALLADOLID' => 'Valladolid',
			'VIZCAYA' => 'Vizcaya',
			'ZAMORA' => 'Zamora',
			'ZARAGOZA' => 'Zaragoza'
		);
	}

	public function init() {
		return $this->ordersTable->init();
	}

	public function purgeOrders() {
		/*$cadaMinutos = $this->_b->preferencias->DevValor ('compras_BorrarCadaMinutos', 1);
		$ultimaVez = $this->_b->preferencias->DevValor ('compras_BorrarUltimaVez', 1);
		$tmpFecha = time() - ($cadaMinutos * 60);

		if ($ultimaVez <= $tmpFecha) {
			$this->_b->preferencias->PonValor ('compras_BorrarUltimaVez', time(), 1);

			$fecha = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' - ' . $this->_b->preferencias->DevValor ('compras_BorrarDiasAntiguedad', 1) . ' day'));

			$caducadas = $this->DevListadoSecciones (array ('id'), null, null, null, null, null, null, array ('fechaCreacion', '1700-00-00 00:00:00', $fecha));

			for ($i=0; $i < count($caducadas); $i++) $this->BorSeccion ($caducadas[$i][0]);
		}*/
	}



}