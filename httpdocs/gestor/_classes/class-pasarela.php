<?php
	/*
	 ***************************************************************
	 *                   CLASE: Pasarela Sermepa                   *
	 ***************************************************************
	 */
	/**
	 * Gestión de las paginación.
	 * 
	 * Prepara la paginación y devuelve el código necesario.
	 *
	 * @package     EUPHORBIA
	 * @subpackage  Pasarela
	 * @author      César Vega Martín <cesar@euphorbia.es>
	 * @copyright   Copyright 2016, Euphorbia Comunicación, S.L.L.
	 * @access      public
	 * @version     1.00 - Final
	 * @link        http://www.euphorbia.es Euphorbia Comunicación, S.L.L.
	 *
	 */


	class Pasarela {
		private $_b;
		public 	$redsysAPI;
		public 	$monedas;

		public  $codigoTitular	= '999008881';
		public  $nombreTitular	= 'TEST PASARELA';
		public  $claveSecreta	= 'sq7HjrUOBfKmC576ILgskD5srU870gJ7';
		public  $terminal		= '001';
		
		public 	$urlFormulario 	= "https://sis-t.redsys.es:25443/sis/realizarPago";
		public 	$urlXML			= "https://sis-t.redsys.es:25443/sis/operaciones";

		public 	$urlComercio;
		public 	$urlRespuestaOk;
		public 	$urlRespuestaKo;
		public 	$urlNotificacion;

		const MONEDAS = array (
			'EUR' => '978',
			'USD' => '840',
			'GBP' => '826',
			'JPY' => '392'
		);

		function __construct ($base) {
			$this->_b = $base;
			$this->monedas = self::MONEDAS;

			$this->urlComercio  	= "http://$_SERVER[HTTP_HOST]/";
			$this->urlRespuestaOk 	= "http://$_SERVER[HTTP_HOST]/" . $base->preferences->getValue('pasarela_url_ok');
			$this->urlRespuestaKo 	= "http://$_SERVER[HTTP_HOST]/" . $base->preferences->getValue('pasarela_url_ko');
			$this->urlNotificacion 	= "http://$_SERVER[HTTP_HOST]/banco-notificacion/";

			$this->moneda = $base->preferences->getValue('pasarela_moneda');

			if ($base->preferences->getValue('pasarela_is_real')) {
				$this->codigoTitular = $base->preferences->getValue('pasarela_codigo_titular');
				$this->nombreTitular = $base->preferences->getValue('pasarela_nombre_titular');
				$this->claveSecreta = $base->preferences->getValue('pasarela_clave_secreta');
				$this->terminal = $base->preferences->getValue('pasarela_codigo_terminal') ? $base->preferences->getValue('pasarela_codigo_terminal') : $this->terminal;
				$this->urlFormulario = $base->preferences->getValue('pasarela_url_formulario') ? $base->preferences->getValue('pasarela_url_formulario') : $this->urlFormulario;
				$this->urlXML = $base->preferences->getValue('pasarela_url_operaciones') ? $base->preferences->getValue('pasarela_url_operaciones') : $this->urlXML;
			}

			$this->redsysAPI = new RedsysAPI;
		}

		// FUNCIONES PÚBLICAS //

		public function CreaXML ($datos) {
			$xml = '<DATOSENTRADA>';
			$xml .= '<DS_Version>1.0</DS_Version>';
			$xml .= '<DS_MERCHANT_AMOUNT>' . (isset($datos['Ds_Amount']) ? $datos['Ds_Amount'] : ''). '</DS_MERCHANT_AMOUNT>';
			$xml .= '<DS_MERCHANT_CONSUMERLANGUAGE>' . (isset($datos['Ds_ConsumerLanguage']) ? $datos['Ds_ConsumerLanguage'] : '') . '</DS_MERCHANT_CONSUMERLANGUAGE>';
			$xml .= '<DS_MERCHANT_CURRENCY>' . (isset($datos['Ds_Currency']) ? $datos['Ds_Currency'] : '') . '</DS_MERCHANT_CURRENCY>';
			$xml .= '<DS_MERCHANT_ORDER>' . (isset($datos['Ds_Order']) ? $datos['Ds_Order'] : '') . '</DS_MERCHANT_ORDER>';
			$xml .= '<DS_MERCHANT_MERCHANTCODE>' . (isset($datos['Ds_MerchantCode']) ? $datos['Ds_MerchantCode'] : '') . '</DS_MERCHANT_MERCHANTCODE>';
			$xml .= '<DS_MERCHANT_MERCHANTURL>' . (isset($datos['Ds_Url']) ? $datos['Ds_Url'] : '') . '</DS_MERCHANT_MERCHANTURL>';
			$xml .= '<DS_MERCHANT_MERCHANTNAME>' . (isset($datos['Ds_MerchantName']) ? $datos['Ds_MerchantName'] : '') . '</DS_MERCHANT_MERCHANTNAME>';
			$xml .= '<DS_MERCHANT_MERCHANTDATA>' . (isset($datos['Ds_MerchantData']) ? $datos['Ds_MerchantData'] : '') . '</DS_MERCHANT_MERCHANTDATA>';
			$xml .= '<DS_MERCHANT_MERCHANTSIGNATURE>' . (isset($datos['Ds_Signature']) ? $datos['Ds_Signature'] : '') . '</DS_MERCHANT_MERCHANTSIGNATURE>';
			$xml .= '<DS_MERCHANT_TERMINAL>' . (isset($datos['Ds_Terminal']) ? $datos['Ds_Terminal'] : '') . '</DS_MERCHANT_TERMINAL>';
			$xml .= '<DS_MERCHANT_TRANSACTIONTYPE>' . (isset($datos['Ds_TransactionType']) ? $datos['Ds_TransactionType'] : '') . '</DS_MERCHANT_TRANSACTIONTYPE>';
			$xml .= '</DATOSENTRADA>';
			
			return $xml;
		}

		public function ErrorOpDescripcion ($codigoError) {
			$arrErrores = array (
				'SIS0007' => "Error al desmontar el XML de entrada",
				'SIS0008' => "Error falta Ds_Merchant_MerchantCode",
				'SIS0009' => "Error de formato en Ds_Merchant_MerchantCode",
				'SIS0010' => "Error falta Ds_Merchant_Terminal",
				'SIS0011' => "Error de formato en Ds_Merchant_Terminal",
				'SIS0014' => "Error de formato en Ds_Merchant_Order",
				'SIS0015' => "Error falta Ds_Merchant_Currency",
				'SIS0016' => "Error de formato en Ds_Merchant_Currency",
				'SIS0018' => "Error falta Ds_Merchant_Amount",
				'SIS0019' => "Error de formato en Ds_Merchant_Amount",
				'SIS0020' => "Error falta Ds_Merchant_MerchantSignature",
				'SIS0021' => "Error la Ds_Merchant_MerchantSignature viene vacía",
				'SIS0022' => "Error de formato en Ds_Merchant_TransactionType",
				'SIS0023' => "Error Ds_Merchant_TransactionType desconocido",
				'SIS0026' => "Error No existe el comercio / terminal enviado",
				'SIS0027' => "Error Moneda enviada por el comercio es diferente a la que tiene asignada para ese terminal",
				'SIS0028' => "Error Comercio / terminal está dado de baja",
				'SIS0030' => "Error en un pago con tarjeta ha llegado un tipo de operación no valido",
				'SIS0031' => "Método de pago no definido",
				'SIS0034' => "Error de acceso a la Base de Datos ",
				'SIS0038' => "Error en java",
				'SIS0040' => "Error el comercio / terminal no tiene ningún método de pago asignado",
				'SIS0041' => "Error en el cálculo de la firma de datos del comercio",
				'SIS0042' => "La firma enviada no es correcta",
				'SIS0046' => "El BIN de la tarjeta no está dado de alta",
				'SIS0051' => "Error número de pedido repetido",
				'SIS0054' => "Error no existe operación sobre la que realizar la devolución",
				'SIS0055' => "Error no existe más de un pago con el mismo número de pedido",
				'SIS0056' => "La operación sobre la que se desea devolver no está autorizada",
				'SIS0057' => "El importe a devolver supera el permitido",
				'SIS0058' => "Inconsistencia de datos, en la validación de una confirmación",
				'SIS0059' => "Error no existe operación sobre la que realizar la devolución",
				'SIS0060' => "Ya existe una confirmación asociada a la preautorización",
				'SIS0061' => "La preautorización sobre la que se desea confirmar no está autorizada",
				'SIS0062' => "El importe a confirmar supera el permitido",
				'SIS0063' => "Error. Número de tarjeta no disponible",
				'SIS0064' => "Error. El número de tarjeta no puede tener más de 19 posiciones",
				'SIS0065' => "Error. El número de tarjeta no es numérico",
				'SIS0066' => "Error. Mes de caducidad no disponible",
				'SIS0067' => "Error. El mes de la caducidad no es numérico",
				'SIS0068' => "Error. El mes de la caducidad no es válido",
				'SIS0069' => "Error. Año de caducidad no disponible",
				'SIS0070' => "Error. El año de la caducidad no es numérico",
				'SIS0071' => "Tarjeta caducada",
				'SIS0072' => "Operación no anulable",
				'SIS0074' => "Error falta Ds_Merchant_Order",
				'SIS0075' => "Error el Ds_Merchant_Order tiene menos de 4 posiciones o más de 12",
				'SIS0076' => "Error el Ds_Merchant_Order no tiene las cuatro primeras posiciones numéricas",
				'SIS0078' => "Método de pago no disponible",
				'SIS0079' => "Error al realizar el pago con tarjeta",
				'SIS0081' => "La sesión es nueva, se han perdido los datos almacenados",
				'SIS0089' => "El valor de Ds_Merchant_ExpiryDate no ocupa 4 posiciones",
				'SIS0092' => "El valor de Ds_Merchant_ExpiryDate es nulo",
				'SIS0093' => "Tarjeta no encontrada en la tabla de rangos",
				'SIS0112' => "Error. El tipo de transacción especificado en Ds_Merchant_Transaction_Type no esta permitido",
				'SIS0115' => "Error no existe operación sobre la que realizar el pago de la cuota",
				'SIS0116' => "La operación sobre la que se desea pagar una cuota no es una operación válida",
				'SIS0117' => "La operación sobre la que se desea pagar una cuota no está autorizada",
				'SIS0118' => "Se ha excedido el importe total de las cuotas",
				'SIS0119' => "Valor del campo Ds_Merchant_DateFrecuency no válido",
				'SIS0120' => "Valor del campo Ds_Merchant_CargeExpiryDate no válido",
				'SIS0121' => "Valor del campo Ds_Merchant_SumTotal no válido",
				'SIS0122' => "Valor del campo Ds_merchant_DateFrecuency o Ds_Merchant_SumTotal tiene formato incorrecto",
				'SIS0123' => "Se ha excedido la fecha tope para realizar transacciones",
				'SIS0124' => "No ha transcurrido la frecuencia mínima en un pago recurrente sucesivo",
				'SIS0132' => "La fecha de Confirmación de Autorización no puede superar en más de 7 días a la de Preautorización",
				'SIS0139' => "Error el pago recurrente inicial está duplicado SIS0142 Tiempo excedido para el pago",
				'SIS0216' => "Error Ds_Merchant_CVV2 tiene mas de 3/4 posiciones",
				'SIS0217' => "Error de formato en Ds_Merchant_CVV2",
				'SIS0221' => "Error el CVV2 es obligatorio",
				'SIS0222' => "Ya existe una anulación asociada a la preautorización",
				'SIS0223' => "La preautorización que se desea anular no está autorizada",
				'SIS0224' => "El comercio no permite anulaciones por no tener firma ampliada",
				'SIS0225' => "Error no existe operación sobre la que realizar la anulación",
				'SIS0226' => "Inconsistencia de datos, en la validación de una anulación",
				'SIS0227' => "Valor del campo Ds_Merchan_TransactionDate no válido",
				'SIS0252' => "El comercio no permite el envío de tarjeta",
				'SIS0253' => "La tarjeta no cumple el check-digit",
				'SIS0261' => "Operación detenida por superar el control de restricciones en la entrada al SIS SIS0274 Tipo de operación desconocida o no permitida por esta entrada al SIS"
			);
			
			if (in_array($codigoError, $arrErrores)) return $arrErrores[$codigoError];
			else return "Descripción de error no encontrada";
		}

		public function ErrorOpMensaje ($codigoError) {
			switch ($codigoError) {
				case 'SIS0030':
				case 'SIS0031':
				case 'SIS0034':
				case 'SIS0038':
				case 'SIS0071':
				case 'SIS0072':
				case 'SIS0114':
				case 'SIS0142':
					$resp = 'MSG0000'; // El sistema está ocupado, inténtelo más tarde
				break;
				case 'SIS0051':
					$resp = 'MSG0001'; // Número de pedido repetido
				break;
				case 'SIS0046':
					$resp = 'MSG0002'; // El Bin de la tarjeta no está dado de alta en FINANET
				break;
				case 'SIS0094':
					$resp = 'MSG0004'; // Error de Autenticación
				break;
				case 'SIS0078':
					$resp = 'MSG0005'; // No existe método de pago válido para su tarjeta
				break;
				case 'SIS0093':
					$resp = 'MSG0006'; // Tarjeta ajena al servicio
				break;
				case 'SIS0007':
				case 'SIS0008':
				case 'SIS0009':
				case 'SIS0010':
				case 'SIS0011':
				case 'SIS0014':
				case 'SIS0015':
				case 'SIS0016':
				case 'SIS0018':
				case 'SIS0019':
				case 'SIS0020':
				case 'SIS0021':
				case 'SIS0022':
				case 'SIS0023':
				case 'SIS0024':
				case 'SIS0025':
				case 'SIS0026':
				case 'SIS0027':
				case 'SIS0028':
				case 'SIS0040':
				case 'SIS0041':
				case 'SIS0042':
				case 'SIS0043':
				case 'SIS0054':
				case 'SIS0055':
				case 'SIS0056':
				case 'SIS0057':
				case 'SIS0058':
				case 'SIS0059':
				case 'SIS0060':
				case 'SIS0061':
				case 'SIS0062':
				case 'SIS0063':
				case 'SIS0064':
				case 'SIS0065':
				case 'SIS0066':
				case 'SIS0067':
				case 'SIS0068':
				case 'SIS0069':
				case 'SIS0070':
				case 'SIS0074':
				case 'SIS0075':
				case 'SIS0076':
				case 'SIS0112':
				case 'SIS0115':
				case 'SIS0116':
				case 'SIS0117':
				case 'SIS0132':
				case 'SIS0133':
				case 'SIS0139':
				case 'SIS0198':
				case 'SIS0199':
				case 'SIS0200':
				case 'SIS0214':
				case 'SIS0216':
				case 'SIS0217':
				case 'SIS0218':
				case 'SIS0219':
				case 'SIS0220':
				case 'SIS0221':
				case 'SIS0222':
				case 'SIS0223':
				case 'SIS0224':
				case 'SIS0225':
				case 'SIS0226':
				case 'SIS0227':
				case 'SIS0229':
				case 'SIS0252':
				case 'SIS0253':
				case 'SIS0254':
				case 'SIS0255':
				case 'SIS0256':
				case 'SIS0257':
				case 'SIS0258':
				case 'SIS0261':
				case 'SIS0270':
				case 'SIS0274':
				case 'SIS0281':
				case 'SIS0296':
				case 'SIS0297':
				case 'SIS0298 ':
					$resp = 'MSG0008'; // Error en datos enviados. Contacte con su comercio.
				break;
				default:
					$resp = '';
				break;
			}

			return $resp;
		}

		public function TransaccionDenegadaDesc ($codigoError) {
			$arrErrores = array (
				101 => "TARJETA CADUCADA. Transacción denegada porque la fecha de caducidad de la tarjeta que se ha informado en el pago, es anterior a la actualmente vigente.",
				102 => "TARJETA BLOQUEDA TRANSITORIAMENTE O BAJO SOSPECHA DE FRAUDE. Tarjeta bloqueada transitoriamente por el banco emisor o bajo sospecha de fraude.",
				104 => "OPERACIÓN NO PERMITIDA. Operación no permitida para ese tipo de tarjeta.",
				106 => "NÚMERO DE INTENTOS EXCEDIDO. Excedido el número de intentos con PIN erróneo.",
				107 => "CONTACTAR CON EL EMISOR. El banco emisor no permite una autorización automática. Es necesario contactar telefónicamente con su centro autorizador para obtener una aprobación manual.",
				109 => "IDENTIFICACIÓN INVALIDA DEL COMERCIO O TERMINAL. Denegada porque el comercio no está correctamentedado de alta en los sistemas internacionales de tarjetas."
			);

			return isset($arrErrores[$codigoError]) ? $arrErrores[$codigoError] : 'ERROR NO ENCONTRADO';
		}

		public function OperacionXML ($datos) {
			/*
			// SOLUCIÓN 1: Esta solución no sirve porque el parámetro "entrada" tiene que enviarse por POST

			if($resp = simplexml_load_file($banco->urlXML . '?entrada=' . $xml)){
				$resp = json_encode($resp);
			}
			else {
				$resp = "Error al contactar con el banco";
			}
			*/

			/*
			// SOLUCIÓN 2: Esta solución no sirve porque curl no funciona en todos los servidores

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, $banco->urlXML);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "entrada=" . $xml);
			
			$content = curl_exec($ch);
			*/

			// SOLUCIÓN 3:
			$post_data = array('entrada' => $this->CreaXML($datos));

			$params = array('http' => array(
				'method'	=> 'POST',
				'header'	=> "Content-Type: application/x-www-form-urlencoded\r\n",
				'content'	=> http_build_query($post_data)
			));
			
			$contexto  = stream_context_create($params);

			$resp = file_get_contents($this->urlXML, null, $contexto);

			//$resp = utf8_encode($resp);
			/*$resp = "<?xml version='1.0' encoding='ISO-8859-1' ?><RETORNOXML><CODIGO>0</CODIGO><Ds_Version>1.0</Ds_Version><OPERACION><Ds_Amount>90595</Ds_Amount><Ds_Currency>978</Ds_Currency><Ds_Order>000000008O33</Ds_Order><Ds_Signature>3E4DF61CF3049932BAF4C7398B762ED0739DCC0D</Ds_Signature><Ds_MerchantCode>327429841</Ds_MerchantCode><Ds_Terminal>1</Ds_Terminal><Ds_Response>0900</Ds_Response><Ds_AuthorisationCode>685616</Ds_AuthorisationCode><Ds_TransactionType>P</Ds_TransactionType><Ds_SecurePayment>1</Ds_SecurePayment><Ds_Language>1</Ds_Language><Ds_MerchantData>000000008O</Ds_MerchantData><Ds_Card_Country>724</Ds_Card_Country></OPERACION></RETORNOXML>";*/

			$resp = simplexml_load_string($resp);
			$resp = $this->simplexml_to_array($resp);
			
			return $resp;
		}


		// FUNCIONES PRIVADAS //

		private function simplexml_to_array($xmlobj) {
			$a = array();
			foreach ($xmlobj->children() as $node) {
				if (count($node)) {
					$a[$node->getName()][] = $this->simplexml_to_array($node);
				}
				else {
					if (isset($a[$node->getName()])) {
						if (is_array($a[$node->getName()])) {
							array_push ($a[$node->getName()], (string) $node);
						}
						else {
							$tmp = $a[$node->getName()];

							$a[$node->getName()] = array ($tmp, (string) $node);
						}
					}
					else {
						$a[$node->getName()] = (string) $node;
					}
				}
			}
			return $a;
		}
	}