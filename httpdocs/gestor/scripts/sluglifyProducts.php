<?php
	/// Cargamos las preferencias
	require "../_inc/config.php";

	/// Cargamos los idiomas y los textos
	require "../_classes/class-i18n.php";
	$i18n = new i18n("admin", $lang_admin);

	/// Cargamos otras clases necesarias
	require "../_classes/class-utilities.php";
	require "../_classes/class-adminPreferences.php";
	require "../_classes/class-products.php";

	/// Arrancamos el panel de control
	require "../_classes/class-base.php";
	$b = new Base();
	
	/// Definimos las variables que se utilizarán
	$base_path = "/". ADMIN_FOLDER . "/";

	//Flush (send) the output buffer and turn off output buffering
	//while (ob_get_level()>0)  ob_end_flush();

	/// Comprobamos permisos
	if (empty($b->error)) {
		if ($b->security->hasPermission(["administrator","master","root"])) {

			$clsProducts = new Products($b->db);
			$arr_brands = $arr_colors = $arr_products = [];

			echo "Iniciando SCRIPT "; flush(); ob_flush();
			usleep(300000); echo ". "; flush(); ob_flush();
			usleep(300000); echo ". "; flush(); ob_flush();
			usleep(300000); echo ". "; flush(); ob_flush();
			usleep(500000); echo " <br/>"; flush(); ob_flush();

			if(isset($_REQUEST["clear"])) { $clsProducts->setRecord(["Url"=>null], ["GRP_ID"=>GRP_ID,"Publicar"=>"1"], "Products"); }
			//exit;

			echo "Recopilando informaci&oacuten "; flush(); ob_flush();
			usleep(300000); echo ". "; flush(); ob_flush();
			usleep(300000); echo ". "; flush(); ob_flush();
			usleep(300000); echo ". "; flush(); ob_flush();
			usleep(500000); echo " <br/>"; flush(); ob_flush();

			/* Marcas */
			$result = $clsProducts->getList("Brands", ["Codigo", "Descripcion"], ["GRP_ID"=>GRP_ID], ["Descripcion"]);
			foreach ($result as $brand) $arr_brands[$brand["Codigo"]] = $brand["Descripcion"];

			/* Colores */
			$result = $clsProducts->getList("Colors", ["Codigo", "Descripcion"], ["GRP_ID"=>GRP_ID], ["Descripcion"]);
			foreach ($result as $color) $arr_colors[$color["Codigo"]] = $color["Descripcion"];			

			$numAffectedRows = 0;
			$numCount = 0;
			$result = $clsProducts->getList("Products", ["Row_Id","Url","DescripcionWeb","Marca","ColorWeb"], ["GRP_ID"=>GRP_ID,"Publicar"=>"1"]);

			echo "<br/>Iniciando proceso, la operaci&oacute;n puede tardar varios minutos. Recarga la p&aacute;gina si no termina correctamente: <br />"; flush(); ob_flush();

			foreach ($result as $row) {

				# testing
				// $numCount++; if($numCount>10) { echo "<br/> $numAffectedRows registros afectados"; exit; }

				if( $row["Url"]==null || empty($row["Url"]) ) {


					$numIteraciones = 1;
					$strUrl = "";
					//$strUrl .= isset($arr_brands[$row["Marca"]])? $arr_brands[$row["Marca"]] . " ": "sin marca ";
					$strUrl .= $row["DescripcionWeb"] . " ";
					$strUrl .= isset($arr_colors[$row["ColorWeb"]])? $arr_colors[$row["ColorWeb"]] . " ": "multicolor";
					$urlBase = $url = Utilities::getSlug( $strUrl );
					while ( $others = $clsProducts->getList("Products", ["Row_Id"], ["Url"=>$url],null,0,1) ){					
						$url = $urlBase . "-" . $numIteraciones++;
					} 

					if( $clsProducts->setRecord(["Url"=>$url], ["Row_Id"=>$row["Row_Id"]], "Products")) {
						$numAffectedRows++;
						echo "+ ";
					    flush();ob_flush();
						continue;
					}

				}
				echo "- ";
			    flush();ob_flush();

			}

			echo "<br/> Terminado"; flush(); ob_flush();

			usleep(900000); echo "<br/>"; flush(); ob_flush();
			echo "<br/> $numAffectedRows de " . sizeof($result) . " registos afectados" ;
			exit;

		}
	}

