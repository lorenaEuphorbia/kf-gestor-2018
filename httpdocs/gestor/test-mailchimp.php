<?php
	/// Cargamos otras clases necesarias (estáticas)
	require $_SERVER['DOCUMENT_ROOT'] . '/gestor/_inc/config.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-utilities.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-domElements.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-controller.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-catalogo.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-adminPreferences.php';

	date_default_timezone_set("Europe/Madrid");
	setlocale(LC_ALL,"es_ES");

	/// Definimos un autocargador para las clases:
	spl_autoload_register(function($className) {
		if(!class_exists($className)) {
			$file =  $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-' . lcfirst($className) . '.php';
			if(file_exists($file)) {
				require($file); 				
			}			
		}
	});

	/// Arrancamos el panel de control
	$i18n = new I18n("admin", $lang_admin);
	$b = new Base();

	$objClsMailChimp = new Suscripciones ($b);

?><!doctype html>
<html>
	<head>
		<meta charset="utf-8" >
	</head>
	<body>
	</body>
	<pre>


<?php

	// echo $objClsMailChimp->suscribe ('cevm88@gmail.com', 'Cesar', 'Vega');
	// 
	
	$result = $objClsMailChimp->getListMembers (Suscripciones::LISTA_DEFAULT);
	print_r ($result);

?></pre></body>
</html>