<?php

/// Cargamos las preferencias
require "../_inc/config.php";

/// Cargamos los idiomas y los textos
require "../_classes/class-i18n.php";
$i18n = new i18n("admin", $lang_admin);

/// Definimos los strings que se cargarán en el javascript
$arrStrings = [
	"cerrarSesion"	=> _("¿Realmente desea cerrar la sesión?"),
	"testString"	=> _("testString"),

	/// Mensajes
	"borrarArchivoConfirmN"		=> _("¿Realmente desea eliminar los %d archivos seleccionados?"),
	"borrarArchivoConfirm1"		=> _("¿Realmente desea eliminar el archivo seleccionado?"),
	"borrarArchivoError"		=> _("Se ha producido un error al intentar borrar el archivo seleccionado."),
	"borrarBotonMenuConfirm"	=> _("¿Realmente desea eliminar el botón seleccionado?\n\nAVISO: Si contiene un submenú también se suprimirá."),
	"borrarBotonMenuError"		=> _("Se ha producido un error al intentar borrar el botón seleccionado."),
	"borrarImagenConfirmN"		=> _("¿Realmente desea eliminar las %d imágenes seleccionadas?"),
	"borrarImagenConfirm1"		=> _("¿Realmente desea eliminar la imagen seleccionada?"),
	"borrarImagenError"			=> _("Se ha producido un error al intentar borrar la imagen seleccionada."),
	"borrarRegistroConfirmN"	=> _("¿Realmente desea eliminar los %d registros seleccionados?"),
	"borrarRegistroConfirm1"	=> _("¿Realmente desea eliminar el registro seleccionado?"),
	"borrarRegistroError"		=> _("Se ha producido un error al intentar borrar el registro seleccionado."),
	"guardadoError"				=> _("Se ha producido un error al guardar los datos introducidos."),
	"guardadoOk"				=> _("El guardado se ha realizado con éxito."),
	"ordenBotonMenuError"		=> _("Se ha producido un error al guardar el orden de los botones."),
	

	/// DataTables
	"dtProcessing"		=> _("Procesando..."),
	"dtLengthMenu"		=> _("Mostrar _MENU_ registros"),
	"dtZeroRecords"		=> _("No se encontraron resultados"),
	"dtEmptyTable"		=> _("Ningún dato disponible en esta tabla"),
	"dtInfo"			=> _("Mostrando del _START_ al _END_ de un total de _TOTAL_ registros"),
	"dtInfoEmpty"		=> _("Mostrando 0 de un total de 0 registros"),
	"dtInfoFiltered"	=> _("(filtrado de un total de _MAX_ registros)"),
	"dtInfoPostFix"		=> "",
	"dtSearch"			=> _("Buscar:"),
	"dtThousands"		=> _(","),
	"dtLoadingRecords"	=> _("Cargando..."),
	"dtFirst"			=> _("<<"),
	"dtLast"			=> _(">>"),
	"dtNext"			=> _(">"),
	"dtPrevious"		=> _("<"),
	"dtSortAscending"	=> _(": Activar para ordenar la columna de manera ascendente"),
	"dtSortDescending"	=> _(": Activar para ordenar la columna de manera descendente"),
	"dtSelectColsN"		=> _("%d columnas seleccionadas"),
	"dtSelectCols0"		=> "", // si se quiere que aparezca un mensaje cuando no haya columnas seleccionadas
	"dtSelectCols1"		=> _("1 columna seleccionada"),
	"dtSelectRowsN"		=> _("%d filas seleccionadas"),
	"dtSelectRows0"		=> "", // si se quiere que aparezca un mensaje cuando no haya filas seleccionadas
	"dtSelectRows1"		=> _("1 fila seleccionada"),
	"dtSelectCellsN"	=> _("%d celdas seleccionadas"),
	"dtSelectCells0"	=> "", // si se quiere que aparezca un mensaje cuando no haya celdas seleccionadas
	"dtSelectCells1"	=> _("1 celda seleccionada"),
	"dtCopy"			=> _("Copiar"),
	"dtCopyRowsN"		=> _("%d filas Copiadas"),
	"dtCopyRows0"		=> "",
	"dtCopyRows1"		=> _("1 fila Copiada"),
	"dtCopyTitle"		=> _("Copiar al portapapeles"),
	"dtCopyKeys"		=> _('Presiona <i>Ctlr</i> o <i>⌘</i> + <i>C</i> para copiar los datos de la tabla<br>a tu portapapeles.<br><br>para cancelar, haz click en este mensaje o bien pulsa escape.'),
];

echo json_encode($arrStrings);