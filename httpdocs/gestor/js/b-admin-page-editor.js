function PageEditor() {
	Editor();

	var $typeSelect = $("#type");
	var $optionBlocks = $(".editor-form").children("fieldset");

	var cambiaTipo = function() {
		$optionBlocks.prop("disabled", true).hide();

		var tipo = $typeSelect.val();
		$("#" + tipo + "-options").prop("disabled", false).show();
	}

	$typeSelect.on("change", cambiaTipo);
	cambiaTipo();

	/// Fieldset ordenables
	var $sortableFieldsets = $(".sortable-fieldsets");

	if ($sortableFieldsets.length) {
		var nestedSortableLoaded = typeof $.mjs == 'object' && typeof $.mjs.nestedSortable === 'function';

		/// Operaciones de añadir y eliminar elementos
		$.each($sortableFieldsets, function(index, value) {
			var $me = $sortableFieldsets.eq(index);
			var $meItems = $me.children("li");
			var $meFirstItem = $me.children("li").first();
			var $btAdd = $me.children("button.add");

			var numItems = $meItems.length;

			var updateNumItems = function() {
				$me.find("[data-numItems]").val($me.children("li").length - 1);
			}

			/// Añadir
			$btAdd.on("click", function(e) {
				e.preventDefault();
				e.stopPropagation();

				var $copy = $meFirstItem.clone(true).removeAttr("hidden");
				var $target =  $copy.children("fieldset").removeAttr("disabled");
				$copy.find("label").each(function() {
					var newFor = $(this).prop("for").replace("-0", "-" + numItems);
					$(this).prop("for",  newFor);
				});
				$copy.find("[id]").each(function() {
					var newId = $(this).attr("id").replace("-0", "-" + numItems);
					$(this).attr({"id": newId, "name": newId});
					if($(this).data("function")) window[$(this).data("function")]($(this));
				});
				$copy.find("[data-position]").each(function() { $(this).prop("value", -numItems); });
				$copy.insertBefore($btAdd);

				
				updateNumItems();

				numItems ++;
			});

			/// Eliminar
			$me.on("click", "i.delete", function() {
				$(this).parent().remove();

				updateNumItems();
			});

			/// Script para ordenar mediante drag&drop los elementos del listado
			if (nestedSortableLoaded) {
				var realign = function() {
					var $positionFields = $me.find("[data-position]");

					$positionFields.each(function(index, value) {
						$(this).prop("value", -index);
					});
				}

				$me.nestedSortable({
					handle					: 'i.move',
					forcePlaceholderSize	: true,
					items					: 'li',
					listType				: 'ul',
					maxLevels				: 0,
					placeholder				: 'placeholder',
					protectRoot				: true,
					opacity					: .6,
					relocate				: realign
				});
			}
			else {
				$me.find("i.move").remove();
			}
		});
	}
}

function loadEditor($ckeditor){

	console.log($ckeditor.html());
	var id=$ckeditor.attr("id").split("-");
	id=  id.length>0 ? parseInt(id[id.length-1]): false;

	if ($ckeditor.length && $().ckeditor && id!==0) {
		$ckeditor
			.wrap( "<div class='ckeditor'></div>" )
			.ckeditor({ customConfig: '/gestor/js/ckeditor_config.min.js', toolbar: 'Links', height: '90px' });
	}
}