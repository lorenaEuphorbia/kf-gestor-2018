/*PRUEBA*/

var ajaxRequest;
var basePath;
var jsStrings;
var funcQueue = [];
var st;

$(function() {
    $.fn.isBound = function(type, fn) {
        var data = this.data('events')[type];

        if (data === undefined || data.length === 0) {
            return false;
        }

        return (-1 !== $.inArray(fn, data));
    };

    basePath = $("#logo").attr("href");

    MenuOptions();
    LoadStrings();


    var $btExit = $("#user-exit");
    if ($btExit.length) {
        $btExit.on("click", function() {
            if (!confirm(jsStrings["cerrarSesion"])) return false;
        });
    }
});

function Cookies(operation, name, value, days) {
    var createCookie = function(name, value, days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        } else var expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    };

    var readCookie = function(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    };

    var eraseCookie = function(name) {
        createCookie(name, "", -1);
    };

    switch (operation) {
        case "create":
            createCookie(name, value, days);
            break;
        case "read":
            return readCookie(name);
            break;
        case "erase":
            eraseCookie(name);
            break;
    }
}

function Editor() {
    /// Botones
    var $btBack = $("#back");

    if ($btBack.length) {
        var $tit = $("h1.sectionTitle");

        $tit.append($("<button/>").html("<span>" + $btBack.find("button").html() + "</span>").addClass("link-back").html("<span>Volver</span>"));

        $tit.on("click", ".link-back", function() {
            $btBack.submit();
        });
    }

    /// Campos

    $slug = $("[data-slug]");
    if ($slug.length) {
        $slug.each(function() {
            var $source = $("#" + $(this).data("slug"));
            var $destination = $(this);

            if ($source.length) {
                $(this).prev().append('<button type="button" id="slug-refresh" class="slug-bt" title="generar automáticamente" tabindex="-1"><span>generar</span></button>');

                $destination.parent().find('.slug-bt').on("click", function() {
                    if (!$destination.is(":disabled")) {
                        if (ajaxRequest) ajaxRequest.abort();

                        ajaxRequest = $.ajax(basePath + 'ajax/', {
                            data: { id: 'getSlug', s: $source.val() },
                            type: 'post',
                            dataType: "json",
                            timeout: 5000,
                            success: function(callback) {
                                $destination.val(callback.data);
                            },
                            error: function(xhr, status, error) {
                                if (xhr.statusText != "abort") {
                                    alert('ERROR:\nStatus: ' + status + '\nHttp status: ' + error);
                                }
                            }
                        });
                    }
                });
            }
        });
    }

    $ckeditor = $("div.ckeditor > textarea");
    if ($ckeditor.length && $().ckeditor) {
        $ckeditor.ckeditor({ customConfig: basePath + 'js/ckeditor_config.min.js' },
            function() { // on instanceReady
                var dtd = CKEDITOR.dtd;
                for (var e in CKEDITOR.tools.extend({}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent)) {
                    this.dataProcessor.writer.setRules(e, {
                        //indent: true,
                        //breakBeforeOpen: false,
                        //breakAfterOpen: false,
                        //breakBeforeClose: false,
                        breakAfterClose: false
                    });
                }

                //this.setMode('source');
            }
        );
    }

    $charCounter = $("input[type=text].char-counter");
    if ($charCounter.length) {
        $charCounter.each(function() {
            var maxChars = $(this).prop("maxlength");

            if (maxChars) {
                var currentChars = $(this).val().length;

                $(this).parent().append('<div class="char-counter-num">' + (maxChars - currentChars) + '</div>');

                var counter = $(this).next();

                $(this).on("input", function() {
                    counter.html(maxChars - $(this).val().length);
                });
            }
        });
    }

    $dateFields = $("input[type=date]");
    if ($dateFields.length && $().datetimepicker) {
        $dateFields.each(function() {
            var d = new Date($(this).val());

            $(this)
                .attr({
                    'type': 'text',
                    'value': FillWithZeros(d.getUTCDate()) + '/' + FillWithZeros(d.getUTCMonth() + 1) + '/' + d.getUTCFullYear()
                })
                .datetimepicker({
                    dayOfWeekStart: 1,
                    format: 'd/m/Y',
                    lang: 'es',
                    timepicker: false,
                });
        });
    }

    $dateTimeFields = $("input[type=datetime-local]");
    if ($dateTimeFields.length && $().datetimepicker) {
        $dateTimeFields.each(function() {
            var val = $(this).val();
            var d = val ? new Date(val) : new Date();
            var dFilled = FillWithZeros(d.getUTCDate()) + '/' + FillWithZeros(d.getUTCMonth() + 1) + '/' + d.getUTCFullYear() + ' ' + FillWithZeros(d.getUTCHours()) + ':' + FillWithZeros(d.getUTCMinutes());

            // console.log(val);
            // console.log(d);
            // console.log(dFilled);

            $(this)
                .attr({
                    'type': 'text',
                    'value': dFilled
                })
                .datetimepicker({
                    dayOfWeekStart: 1,
                    format: 'd/m/Y H:i',
                    lang: 'es',
                    timepickerScrollbar: false,
                });
        });

    }

    $form = $("#DataForm");
    if ($form.length) {
        $form.on('submit', function() {
            if (setChildNames) setChildNames();
            $(".sortable-fieldsets fieldset[disabled]").remove();
        });
    }

    $iconPicker = $(".icon-picker");
    if ($iconPicker.length && typeof $.fn.fontIconPicker == 'function') {
        $iconPicker.fontIconPicker({ hasSearch: false });
    }

    $inputImage = $(".input-image");
    if ($inputImage.length) {
        /// Ampliar imagen
        $zoomLink = $inputImage.find("a[data-zoom]");
        if ($zoomLink && typeof $.fancybox === 'function') {
            $zoomLink.fancybox({ padding: 0 });
        }

        /// Borrar imagen
        $inputImage.on("click", "a[data-remove]", function() {
            var $altField = $(this).parent().parent().parent().children("input[type=hidden]");

            if ($altField.length && confirm(jsStrings["borrarImagenConfirm1"])) {
                if (ajaxRequest) ajaxRequest.abort();

                ajaxRequest = $.ajax(basePath + 'ajax/', {
                    data: { id: 'deleteImage', m: $altField.data("mod"), t: $altField.data("type"), i: $altField.val() },
                    type: 'post',
                    dataType: "json",
                    timeout: 5000,
                    success: function(callback) {
                        if (callback.result == "ok") {
                            $altField.next().remove();
                        } else {
                            alert(jsStrings["borrarImagenError"]);
                        }
                    },
                    error: function(xhr, status, error) {
                        if (xhr.statusText != "abort") {
                            alert('ERROR:\nStatus: ' + status + '\nHttp status: ' + error);
                        }
                    }
                });
            }
        });
    }

    $inputFile = $(".input-file");
    if ($inputFile.length) {

        /// Borrar archi
        $inputFile.on("click", "a[data-remove]", function() {
            var $altField = $(this).parents(".input-file").children("input[type=hidden]");

            if ($altField.length && confirm(jsStrings["borrarArchivoConfirm1"])) {
                if (ajaxRequest) ajaxRequest.abort();

                ajaxRequest = $.ajax(basePath + 'ajax/', {
                    data: { id: 'deleteImage', m: $altField.data("mod"), t: $altField.data("type"), i: $altField.val() },
                    type: 'post',
                    dataType: "json",
                    timeout: 5000,
                    success: function(callback) {
                        if (callback.result == "ok") {
                            $altField.next().remove();
                        } else {
                            alert(jsStrings["borrarArchivoError"]);
                        }
                    },
                    error: function(xhr, status, error) {
                        if (xhr.statusText != "abort") {
                            alert('ERROR:\nStatus: ' + status + '\nHttp status: ' + error);
                        }
                    }
                });
            }
        });
    }

    $revealablePass = $("input[type=password].revealable");
    if ($revealablePass.length) {
        $revealablePass.each(function() {
            $(this).parent().append('<div class="revealable-bt"><i class="fa fa-eye"></i> <span>mostrar</span></div>');

            $(this).next().hover(
                function() {
                    $(this).children("i").removeClass("fa-eye").addClass("fa-eye-slash");
                    $(this).prev().prop("type", "text");
                },
                function() {
                    $(this).children("i").removeClass("fa-eye-slash").addClass("fa-eye");
                    $(this).prev().prop("type", "password");
                }
            );

            var maxChars = $(this).prop("maxlength");

            if (maxChars) {
                var currentChars = $(this).val().length;


                var counter = $(this).next();

                $(this).on("input", function() {
                    counter.html(maxChars - $(this).val().length);
                });
            }
        });
    }

    /// Selects con componente chosen
    $selectChosen = $("select.chosen");
    if ($selectChosen.length && typeof $.fn.chosen === "function") {
        $selectChosen.each(function(i, e) {
            $selectChosen.eq(i).chosen({
                allow_single_deselect: !$(this).prop("required"),
                //disable_search_threshold  : 10, // no muestra la búsqueda si hay menos
                no_results_text: "No hay coincidencias",
                placeholder_text_multiple: " ",
                placeholder_text_single: " ",
                search_contains: true,
                single_backstroke_delete: false,
            });
        });
    }

    $selListado = $(".select-lst");
    $cbListado = $(".select-tbl");
    if ($selListado.length) {
        $listadoItems = $(".select-tbl-item");
        $textUrl = $(".select-tbu");
        $slugUrl = $(".select-tbu-item");

        var $x = function() {
            if (this.value == 1) {
                $cbListado.prop('disabled', false).parent().slideDown('fast');
                $textUrl.prop('disabled', true).parent().slideUp('fast');
                $slugUrl.prop('disabled', true).parent().slideUp('fast');
            } else if (this.value == 3 || this.value == 4) {
                $textUrl.prop('disabled', false).parent().slideDown('fast');
                $slugUrl.prop('disabled', true).parent().slideUp('fast');
                $cbListado.prop('disabled', true).parent().slideUp('fast');
            } else {
                $textUrl.prop('disabled', true).parent().slideUp('fast');
                $slugUrl.prop('disabled', false).parent().slideDown('fast');
                $cbListado.prop('disabled', true).parent().slideUp('fast');
            }
        }

        $selListado.on("change", $x);
        switch ($selListado.val()) {
            case "1":
                $textUrl.prop('disabled', true).parent().hide();
                break;
            case "2":
                $cbListado.prop('disabled', true).parent().hide();
                $textUrl.prop('disabled', true).parent().hide();
            case "3":
            case "4":
                $cbListado.prop('disabled', true).parent().hide();
                $slugUrl.prop('disabled', true).parent().hide();
            default:
                $cbListado.prop('disabled', true).parent().hide();
                break;
        }
    }

    if ($cbListado.length && $listadoItems.length) {
        var $b = function() {
            // Mostramos Configuración
            if ($cbListado.is(":checked"))
                $listadoItems.prop('disabled', false).parent().slideDown('fast');
            else
                $listadoItems.prop('disabled', true).parent().slideUp('fast');
        };
        $cbListado.on("change", $b);
        if (!$cbListado.is(":checked"))
            $listadoItems.prop('disabled', true).parent().hide();
    }

    /// Listado Ordenable
    var $sortableWrapper = $(".sortable-array");
    var $sortableList = $sortableWrapper.children("ul");

    if ($sortableList.length && $sortableList.length) {
        var SaveSorting = function() {
            var list = $sortableList.nestedSortable('toArray', { startDepthCount: 0 });
            list = JSON.stringify(list);

            $.ajax(basePath + 'ajax/', {
                data: { id: 'sortList', m: $sortableWrapper.data("mod"), a: $sortableWrapper.data("act"), l: list },
                type: 'post',
                dataType: "json",
                timeout: 5000,
                success: function(callback) {
                    if (callback.result != "ok") {
                        alert(jsStrings["ordenBotonMenuError"]);
                        location.reload();
                    }
                },
                error: function(xhr, status, error) {
                    if (xhr.statusText != "abort") {
                        alert('ERROR:\nStatus: ' + status + '\nHttp status: ' + error);
                    }
                }
            });
        }

        var findCurrentId = function($this) {
            return $this.siblings('fieldset').find('[data-id="id"]').val();
        }

        var setChildNames = function() {
            $parentID = $sortableWrapper.data("id");
            $sortableList = $sortableWrapper.children("ul");

            $sortableList.find('fieldset').each(function(indexParent, elParent) {
                $(this).find('[data-db]').each(function(indexChild, elChild) {
                    $strKey = $parentID + '[' + (indexParent - 1) + '][' + $(this).data("id") + ']';
                    $(this).attr('name', $strKey).attr('id', $strKey);
                });
            });

        }

        var DeleteItem = function() {
            $idCurrent = findCurrentId($(this));

            var $row = $(this).parent();
            var $group = $row.parent();
            var numSiblings = $group.children("li").length;

            if (confirm(jsStrings["borrarBotonMenuConfirm"])) {
                if (ajaxRequest) ajaxRequest.abort();

                if ($idCurrent) {
                    ajaxRequest = $.ajax(basePath + 'ajax/', {
                        data: { id: 'deleteMenuButton', m: $sortableWrapper.data("mod"), a: $sortableWrapper.data("act"), i: $idCurrent },
                        dataType: 'json',
                        type: 'post',
                        timeout: 5000,
                        success: function(callback) {
                            if (callback.result == "ok") {
                                if (numSiblings == 1) {
                                    if ($group.attr("id") != "sortable-menu") $group.remove();
                                    else $row.remove();
                                } else {
                                    $row.remove();
                                }
                            } else {
                                alert(jsStrings["borrarBotonMenuError"]);
                                location.reload();
                            }
                        },
                        error: function(xhr, status, error) {
                            if (xhr.statusText != "abort") {
                                alert('ERROR:\nStatus: ' + status + '\nHttp status: ' + error);
                            }
                        }
                    });
                } else {
                    $row.remove();
                }
            }
        }


        if (typeof $.mjs == 'object' && typeof $.mjs.nestedSortable === 'function') {
            $sortableList.nestedSortable({
                handle: 'i.move',
                forcePlaceholderSize: true,
                items: 'li',
                listType: 'ul',
                maxLevels: 0,
                placeholder: 'placeholder',
                protectRoot: true,
                opacity: .6,
                relocate: SaveSorting,
                disableNestingClass: 'placeholder-error'
            });
        } else {
            $sortableList.find("i.move").remove();
        }

        $sortableList.on('click', '.delete', DeleteItem);

        $sortableWrapper.on('click', ".add", function() {
            var $row = $(this);
            var $group = $row.parent();
            $newElement = $row.siblings('li').eq(0).clone();
            $newFieldset = $newElement.find('fieldset');

            $newElement.removeAttr('hidden');
            $newFieldset.removeAttr('disabled');


            $newElement.insertBefore($row);

            setChildNames();
        });


    }


}

function EditItem($mod, $act, id) {
    var modal = new Modal();

    modal.loadContent(
        basePath + $mod + '/' + $act + '/editor/' + (id ? ('id=' + id) : ''),
        function() {
            var $modal = $("#modal");
            var $modalMessages = $modal.find(".messages");

            $modal.on("submit", "form", function(e) {
                e.preventDefault();
                e.stopPropagation();

                var $form = $(this);

                if (ajaxRequest) ajaxRequest.abort();

                ajaxRequest = $.ajax(basePath + 'ajax/', {
                    data: { id: 'editItem', fields: $form.serialize() },
                    type: 'post',
                    dataType: 'json',
                    timeout: 5000,
                    beforeSend: function() {
                        $modalMessages.empty();
                    },
                    success: function(callback) {
                        if (callback.result == "ok") {
                            $modal.find(".modal-content").html(
                                $("<div/>").addClass("message ok").css("margin-bottom", "0").html(jsStrings["guardadoOk"])
                            );

                            $modal.find(".bt-close").attr("data-closemodal", "0").on("click", function() {
                                location.reload();
                            });
                        } else {
                            if (callback.messages) {

                                for (var i = 0; i < callback.messages.length; i++) {
                                    $modalMessages.append($("<div/>").addClass("message " + callback.messages[i][0]).html(callback.messages[i][1]));
                                }
                            } else {
                                $modalMessages.append($("<div/>").addClass("message error").html(jsStrings["guardadoError"]));
                            }
                        }
                    },
                    error: function(xhr, status, error) {
                        if (xhr.statusText != "abort") {
                            alert('ERROR:\nStatus: ' + status + '\nHttp status: ' + error);
                        }
                    }
                });
            });

            $modal.find(".bt-close").attr("data-closemodal", "0").on("click", function() {
                location.reload();
            });
        }
    );
}

function FillWithZeros(num, intPartTotalChars) {
    intPartTotalChars = intPartTotalChars || 2; /// valor por defecto si no se pasa este parámetro

    var n = Math.abs(num);
    var zeros = intPartTotalChars - Math.floor(n).toString().length;

    if (zeros > 0) {
        return (num < 0 ? '-' : '') + new Array(zeros + 1).join('0') + n;
    } else {
        return num + ""; /// convertir a string
    }
}

function List() {

    if (jsStrings) {
        var $table = $(".list-table");

        if ($table.length) {
            var initTables = function() {

                /// Botones
                var bt = [];

                /*var $btNew = $("#editor");

                if ($btNew.length) {
                    bt.push({
                        text : $btNew.find("button").html(),
                        action : function() { $btNew.submit(); }
                    });
                }*/

                /// Tabla
                if ($table.data("extra-buttons")) {
                    var arrButtons = $table.data("extra-buttons").split(",");

                    var fileTitle = $("h1.sectionTitle").html();

                    var exportableCols = [];
                    var $cols = $table.children("thead").find("th");
                    var numCols = $cols.length;

                    for (var i = 0; i < numCols; i++) {
                        if ($cols.eq(i).data("class-name") != "select-checkbox" &&
                            $cols.eq(i).data("class-name") != "actions-cell") {
                            if ($cols.eq(i).attr("data-printable") &&
                                $cols.eq(i).data("printable") === true) {
                                exportableCols.push(i);
                            } else if ($cols.eq(i).data("visible") != false) {
                                exportableCols.push(i);
                            }
                        }
                    };

                    $.each(arrButtons, function(i, v) {
                        switch (v) {
                            case "excel":
                                bt.push({
                                    extend: "excelHtml5",
                                    exportOptions: {
                                        columns: exportableCols,
                                        rows: { selected: true }
                                    },
                                    title: fileTitle
                                });
                                break;
                            case "copy":
                                bt.push({
                                    extend: "copyHtml5",
                                    exportOptions: {
                                        columns: exportableCols,
                                        rows: { selected: true }
                                    },
                                    title: fileTitle
                                });
                                break;
                            case "csv":
                                bt.push({
                                    extend: "csvHtml5",
                                    exportOptions: {
                                        columns: exportableCols,
                                        rows: { selected: true }
                                    },
                                    title: fileTitle
                                });
                                break;
                            case "pdf":
                                bt.push({
                                    extend: "pdfHtml5",
                                    exportOptions: {
                                        columns: exportableCols,
                                        rows: { selected: true }
                                    },
                                    title: fileTitle
                                });
                                break;
                            case "print":
                                bt.push({
                                    extend: "print",
                                    exportOptions: {
                                        columns: exportableCols,
                                        rows: { selected: true }
                                    },
                                    title: fileTitle
                                });
                                break;
                        }
                    });
                }

                dt = $table.DataTable({
                    dom: '<"top"fli>rt<"paginacion"p><"clear"><"buttons"B>',
                    /*columnDefs: [
                        {
                            defaultContent  : "",
                            data            : null,
                            orderable       : false,
                            className       : "select-checkbox",
                            targets         : 0,
                            width           : "33px"
                        },
                        {
                            visible         : false,
                            orderable       : false,
                            searchable      : false,
                            targets         : 1,
                        },
                        {
                            className       : "actions-cell",
                            orderable       : false,
                            targets         : -1,
                            width           : "100px"
                        }
                    ],*/
                    //order         : [[2, 'asc']],
                    pagingType: "full_numbers",
                    stateSave: true,
                    stateDuration: 60 * 60 * 24 * 2,
                    /// Servidor
                    //processing        : true,
                    //serverSide        : true,
                    //ajax          : basePath + "blog/post-list-json/",
                    deferRender: true,
                    fnDrawCallback: ListInit,
                    /// Extensiones
                    select: {
                        style: "multi",
                        selector: "td.select-checkbox"
                    },
                    buttons: bt,
                    /// Traducciones
                    language: {
                        buttons: {

                            copyKeys: jsStrings["dtCopyKeys"],
                            copy: jsStrings["dtCopy"],
                            copySuccess: {
                                _: jsStrings["dtCopyRowsN"],
                                0: jsStrings["dtCopyRows0"],
                                1: jsStrings["dtCopyRows1"]
                            },
                            copyTitle: jsStrings["dtCopyTitle"],
                        },
                        processing: jsStrings["dtProcessing"],
                        lengthMenu: jsStrings["dtLengthMenu"],
                        zeroRecords: jsStrings["dtZeroRecords"],
                        emptyTable: jsStrings["dtEmptyTable"],
                        info: jsStrings["dtInfo"],
                        infoEmpty: jsStrings["dtInfoEmpty"],
                        infoFiltered: jsStrings["dtInfoFiltered"],
                        infoPostFix: jsStrings["dtInfoPostFix"],
                        search: jsStrings["dtSearch"],
                        thousands: jsStrings["dtThousands"],
                        loadingRecords: jsStrings["dtLoadingRecords"],
                        paginate: {
                            first: jsStrings["dtFirst"],
                            last: jsStrings["dtLast"],
                            next: jsStrings["dtNext"],
                            previous: jsStrings["dtPrevious"],
                        },
                        aria: {
                            sortAscending: jsStrings["dtSortAscending"],
                            sortDescending: jsStrings["dtSortDescending"],
                        },
                        select: {
                            columns: {
                                _: jsStrings["dtSelectColsN"],
                                0: jsStrings["dtSelectCols0"],
                                1: jsStrings["dtSelectCols1"],
                            },
                            rows: {
                                _: jsStrings["dtSelectRowsN"],
                                0: jsStrings["dtSelectRows0"],
                                1: jsStrings["dtSelectRows1"],
                            },
                            cells: {
                                _: jsStrings["dtSelectCellsN"],
                                0: jsStrings["dtSelectCells0"],
                                1: jsStrings["dtSelectCells1"],
                            }
                        },
                    }
                });

                /// Botón seleccionar/deseleccionar todo
                $btSelectAll = $("#btSelectAll");

                if ($btSelectAll.length) {
                    $btSelectAll.on("click", function() {
                        $(this).parent().toggleClass("selected");

                        if ($(this).parent().hasClass("selected")) {
                            dt.rows({ page: 'all', search: 'applied' }).select();
                        } else {
                            dt.rows({ page: 'all', search: 'applied' }).deselect();
                            //dt.rows().deselect();
                        }
                    });

                    dt.on("deselect", function() { $btSelectAll.parent().removeClass("selected"); });
                }

                /// Botones de fila
                $table.on("click", ".actions-cell button", function(e) {
                    var $parentRow = $(this).closest('tr');
                    var action = $(this).data("action");
                    var rowData = dt.row($parentRow).data();
                    var $form = $("form#" + action);
                    var $foreignKey = $form.data("foreign-key") ? $form.data("foreign-key") : 1;
                    var idForeignKey = rowData[$foreignKey];

                    switch (action) {
                        case "delete":
                            if (confirm(jsStrings["borrarRegistroConfirm1"])) {
                                if (ajaxRequest) ajaxRequest.abort();

                                ajaxRequest = $.ajax($table.data("ajax").replace("json", "delete"), {
                                    data: { id: rowData[$foreignKey] },
                                    type: 'post',
                                    dataType: 'json',
                                    timeout: 10000,
                                    success: function(callback) {
                                        if (callback.result == "ok") {
                                            dt.row($parentRow).remove().draw(false);
                                        } else {
                                            alert(jsStrings["borrarRegistroError"]);
                                        }
                                    },
                                    error: function(xhr, status, error) {
                                        if (xhr.statusText != "abort") {
                                            alert('ERROR:\nStatus: ' + status + '\nHttp status: ' + error);
                                        }
                                    }
                                });
                            }
                            break;

                        case "locked":
                            break;

                        case "editor":

                            if ($form.hasClass("open-modal")){
                                $($form[0].id).val(idForeignKey);
                                $form.submit();
                            }
                            else {
                                var accion = $form.attr("action");
                                if (accion && !$(this).hasClass('disabled')) {
                                    window.location = accion + (idForeignKey? 'id=' + idForeignKey: '');
                                }
                                else {
                                    alert("ERROR: No existen datos para editar");
                                }
                            }
                            break;

                        default:

                            if ($form.length) {

                                if ($form.hasClass("open-modal")){
                                    $form.children("input[name=id]").val(idForeignKey);
                                    $form.submit();
                                }
                                else {
                                    var accion = $form.attr("action");
                                    if (accion) {

                                        if(!$(this).hasClass('disabled')) {
                                            window.location = accion + (idForeignKey? 'id=' + idForeignKey: '');
                                        }
                                        else {
                                            alert("ERROR: No existen datos para la acción seleccionada");
                                        }
                                    }
                                    else {                                        
                                        $form.children("input[name=id]").val(idForeignKey);
                                        $form.submit();
                                    }
                                }
                            } else {
                                alert("ERROR: cant't redirect to " + action + ".");
                            }
                            break;
                    }
                });

            }

            if ($().DataTable) initTables();
            else alert("Error loading plugin DataTable."); // se podría cargar el archivo dinámicamente en el header
        }
    } else {
        funcQueue.push(List);
    }


}

var flagListInit = false;

function ListInit() {
    if (!flagListInit) {
        flagListInit = true;
        var $formModal = $("form.open-modal");
        if ($formModal.length) {
            var submitFormModal = function(e) {
                e.stopPropagation();

                var mod = $(this.mod).val();
                var act = $(this.act).val();
                var id = $(this.id).val();

                mod = mod.charAt(0).toLowerCase() + mod.substr(1);
                act = act.charAt(0).toLowerCase() + act.substr(1);

                EditItem(mod, act, id);

                return false;
            };


            $formModal.on('submit', submitFormModal);
        }
    }
    else {
        var $table = $(".list-table");

        $table.find(".actions-cell button").each(function(){
            var $parentRow = $(this).closest('tr');
            var action = $(this).data("action");
            var rowData = dt.row($parentRow).data();
            var $form = $("form#" + action);
            var $foreignKey = $form.data("foreign-key") ? $form.data("foreign-key") : 1;
            var $requiredKey = $form.data("required-key")
            var idForeignKey = rowData[$foreignKey];

            switch (action) {
                case "delete":
                    break;

                case "locked":
                    break;

                case "editor":
                    if ($requiredKey && !idForeignKey) $(this).addClass('disabled');
                    break;

                default:
                    if ($requiredKey && !idForeignKey) $(this).addClass('disabled');
                    break;
            }
        });
    }
}

function LoadStrings() {
    $.ajax(basePath + 'js/js-strings-admin.php', {
        type: 'post',
        dataType: 'json',
        timeout: 5000,
        success: function(data) {
            jsStrings = data;
            ProcessFunctionQueue();
        },
        error: function(xhr, status, error) {
            $("body").remove();
            alert("Error al cargar los textos necesarios para javascript.\nError loading needed textstrings for javascript.\n\nStatus: " + status + "\nHttp status: " + error);
        }
    });
}

function Modal() {
    var context = this;

    var $modal, $overlay;

    this.disable = function() {
        if ($modal.length) {
            this.setLoading();
            $modal.find("input, button").prop("disabled", true);
        }
    }

    this.enable = function() {
        if ($modal.length) {
            $modal.find("input, button").prop("disabled", false);

            var $loading = $modal.find(".loading");
            if ($loading.length) $loading.remove();
        }
    }

    this.init = function() {
        //if (!$("body").hasClass("no-scroll")) $("body").addClass("no-scroll");

        $("body")
            .append(
                $("<div/>")
                .attr("id", "modal-overlay")
                .addClass("modal-overlay"),
                $("<div/>")
                .attr("id", "modal-wrapper")
                .addClass("modal-wrapper")
                .append($("<div/>")
                    .attr("id", "modal")
                    .addClass("modal loading")
                    .append(
                        $("<a/>")
                        .attr({ "href": "javascript:void(0);", "data-closemodal": "1" })
                        .addClass("bt-close").html('<i class="fa fa-close"></i>')
                    )
                )
            );

        $modal = $("#modal");
        $overlay = $("#modal-overlay");
        $wrapper = $("#modal-wrapper");

        $modal.on("click", "[data-closemodal=1]", this.close);

        this.disable();
    }

    this.loadContent = function(contentUrl, callback) {
        if ($modal.children("div").length) $modal.children("div").remove();

        this.setLoading();

        $modal.append(
            $("<div/>")
            .load(contentUrl, function() {
                context.enable();

                if (typeof callback === "function") callback();
            })
        );
    }

    this.close = function() {
        if ($modal.length) $modal.remove();
        if ($overlay.length) $overlay.remove();
        if ($wrapper.length) $wrapper.remove();

        $("body").removeClass("no-scroll");
    }

    this.setLoading = function() {
        if ($modal.length & !$modal.find(".loading").length) {
            $modal.append($("<div/>").addClass("loading").html('<i class="fa fa-spinner fa-pulse"></i>'));
        }
    }

    this.init();
}

function MenuOptions() {
    /// Visibilidad de flechas de menú y submenús
    var $menuFlechas = $("nav.menu").find(".arrow");

    if ($menuFlechas.length) {
        $menuFlechas.each(function() {
            $(this).parents(".arrow").removeClass("arrow");
        });

        $(".arrow").children("a").addClass("visible");
    }

    /// Modo de visualización del menú
    var $btMode = $("#menu-mode");
    var cookieName = "pref-menu-mode";
    var cookieVal = "collapsed";

    if ($btMode.length) {
        $btMode.on("click", function() {
            if ($("body").hasClass("menu-collapsed")) {
                $("body").removeClass("menu-collapsed");
                Cookies("erase", cookieName);
            } else {
                $("body").addClass("menu-collapsed");
                Cookies("create", cookieName, cookieVal, 365);
            }
        });
    }
}

function ProcessFunctionQueue() {
    if (funcQueue.length) {
        $.each(funcQueue, function(i, v) { v(); });

        funcQueue = [];
    }
}

function SortableList() {
    /// Botones
    var $btBack = $("#back");

    if ($btBack.length) {
        var $tit = $("h1.sectionTitle");

        $tit.append($("<button/>").html("<span>" + $btBack.find("button").html() + "</span>").addClass("link-back").html("<span>Volver</span>"));

        $tit.on("click", ".link-back", function() {
            $btBack.submit();
        });
    }

    /// Listado Ordenable
    var $sortableWrapper = $(".sortable-list");
    var $sortableList = $sortableWrapper.children("ul");

    if ($sortableList.length) {
        var SaveSorting = function() {
            var list = $sortableList.nestedSortable('toArray', { startDepthCount: 0 });
            list = JSON.stringify(list);

            $.ajax(basePath + 'ajax/', {
                data: { id: 'sortList', m: $sortableWrapper.data("mod"), a: $sortableWrapper.data("act"), l: list },
                type: 'post',
                dataType: "json",
                timeout: 5000,
                success: function(callback) {
                    if (callback.result != "ok") {
                        alert(jsStrings["ordenBotonMenuError"]);
                        location.reload();
                    }
                },
                error: function(xhr, status, error) {
                    if (xhr.statusText != "abort") {
                        alert('ERROR:\nStatus: ' + status + '\nHttp status: ' + error);
                    }
                }
            });
        }

        if (typeof $.mjs == 'object' && typeof $.mjs.nestedSortable === 'function') {
            $sortableList.nestedSortable({
                handle: 'i.move',
                forcePlaceholderSize: true,
                items: 'li',
                listType: 'ul',
                maxLevels: 0,
                placeholder: 'placeholder',
                protectRoot: true,
                opacity: .6,
                relocate: SaveSorting,
                disableNestingClass: 'placeholder-error'
            });
        } else {
            $sortableList.find("i.move").remove();
        }
    }
}

function SortableMenu() {
    var $sortableWrapper = $(".sortable-menu");

    if ($sortableWrapper.length) {
        var $sortableMenu = $sortableWrapper.children("ul").eq(0);

        var DeleteItem = function() {
            var $row = $(this).parent();
            var $group = $row.parent();
            var numSiblings = $group.children("li").length;

            if (confirm(jsStrings["borrarBotonMenuConfirm"])) {
                if (ajaxRequest) ajaxRequest.abort();

                ajaxRequest = $.ajax(basePath + 'ajax/', {
                    data: { id: 'deleteMenuButton', m: $sortableWrapper.data("mod"), a: $sortableWrapper.data("act"), i: $row.data("id") },
                    dataType: 'json',
                    type: 'post',
                    timeout: 5000,
                    success: function(callback) {
                        if (callback.result == "ok") {
                            if (numSiblings == 1) {
                                if ($group.attr("id") != "sortable-menu") $group.remove();
                                else $row.remove();
                            } else {
                                $row.remove();
                            }
                        } else {
                            alert(jsStrings["borrarBotonMenuError"]);
                            location.reload();
                        }
                    },
                    error: function(xhr, status, error) {
                        if (xhr.statusText != "abort") {
                            alert('ERROR:\nStatus: ' + status + '\nHttp status: ' + error);
                        }
                    }
                });
            }
        }


        $sortableMenu.on("click", "i.delete", DeleteItem);
        $sortableMenu.on("click", "i.edit", function() { EditItem($sortableWrapper.data("mod"), $sortableWrapper.data("act"), $(this).parent().data("id")); });

        $("#bt-new").on("click", function() { EditItem($sortableWrapper.data("mod"), $sortableWrapper.data("act"), ""); });

        if (typeof $.mjs == 'object' && typeof $.mjs.nestedSortable === 'function') {
            var SaveSorting = function() {
                var list = $sortableMenu.nestedSortable('toArray', { startDepthCount: 0 });
                list = JSON.stringify(list);

                $.ajax(basePath + 'ajax/', {
                    data: { id: 'sortMenu', m: $sortableWrapper.data("mod"), l: list, a: $sortableWrapper.data("act") },
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    cache: false,
                    timeout: 5000,
                    success: function(callback) {
                        if (callback.result != "ok") {
                            alert(jsStrings["ordenBotonMenuError"]);
                            location.reload();
                        }
                    },
                    error: function(xhr, status, error) {
                        if (xhr.statusText != "abort") {
                            alert('ERROR:\nStatus: ' + status + '\nHttp status: ' + error);
                        }
                    }
                });
            }

            $numLevels = $sortableWrapper.data("levels") ? $sortableWrapper.data("levels") : '5';
            // alert($numLevels);

            $sortableMenu.nestedSortable({
                handle: 'i.move',
                forcePlaceholderSize: true,
                items: 'li',
                listType: 'ul',
                maxLevels: $numLevels,
                placeholder: 'placeholder',
                //protectRoot               : true,
                opacity: .6,
                relocate: SaveSorting
            });
        } else {
            $sortableWrapper.find("i.move").remove();
        }
    }
}
