<?php

$imagebrowser1 = "Buscador de imagenes para CKEditor";
$imagebrowser2 = "En total:";
$imagebrowser3 = "Imagenes";
$imagebrowser4 = "Desliza tus imagenes aquí..";

$uploadpanel1 = "Selecciona un archivo:";
$uploadpanel2 = "La imagen se subirá a:";
$uploadpanel3 = "La ruta de subidas se puede configurar desde preferencias";

$panelsettings1 = "Ruta de subida:";
$panelsettings2 = "Selecciona un directorio existente, por favor:";
$panelsettings3 = "Historial de rutas:";
$panelsettings4 = "Preferencias:";
$panelsettings5 = "Ocultar extensión de archivo";
$panelsettings6 = "Mostrar extensión de archivo";
$panelsettings7 = "Contraseña:";
$panelsettings8 = "Logout";
$panelsettings9 = "Deshabilitar contraseña";
$panelsettings10 = "Te gusta este plugin?";
$panelsettings11 = "Una limosna, por favor!";
$panelsettings12 = "Soporte:";
$panelsettings13 = "Plugin FAQ";
$panelsettings14 = "Reportar un error";
$panelsettings15 = "Version:";
$panelsettings16 = "Creditos:";
$panelsettings17 = "Made with love by Moritz Maleck";
$panelsettings18 = "Icons:";
$panelsettings19 = "Icon pack by Icons8";
$panelsettings20 = "Cambiar idioma";
$panelsettings21 = "Ocultar seción noticias";
$panelsettings22 = "Ver seción noticias";

$newssection1 = "Puedes deshabilitar la sección de noticias desde el panel de control.";

$buttons1 = "Ver";
$buttons2 = "Descargar";
$buttons3 = "Usar";
$buttons4 = "Eliminar";
$buttons5 = "Cancelar";
$buttons6 = "Guardar";
$buttons7 = "Subir";

$alerts1 = "Gracias!";
$alerts2 = "Para usar este plign necesitas dar <b>permisos de escritura CHMOD (0777)</b> a la carpeta <i>imageuploader</i> de tu servidor.";
$alerts3 = "Como cambiar los permisos a través de filezilla - en Inglés (link externo)";
$alerts4 = "Contacte con <a href='http://www.deltacinco.es/' target='_blank'>Deltacinco</a> si necesitas ayuda.";
$alerts5 = "Necesitas tener <b>JavaScript</b> activado.";
$alerts6 = "Como activar JavaScript - en Inglés (link externo)";
$alerts7 = "Una nueva versión está disponible para este plugin";
$alerts8 = "Descargar ahora!";
$alerts9 = "La carpeta";
$alerts10 = "no encontrado.";
$alerts11 = "crear la carpeta";

$dltimageerrors1 = "Se ha producido un error.";
$dltimageerrors2 = "Solo puedes borrar imagenes. Por favor selecciona otro archivo.";
$dltimageerrors3 = "El archivo que quieres borrar no está disponible en el directorio seleccionado.";
$dltimageerrors4 = "No puedes borrar archivos de sistema. Intentelo de nuevo o seleccione otra imagen, por favor.";
$dltimageerrors5 = "El archivo seleccionado no puede ser borrado. Intentelo de nuevo o seleccione otra imagen, por favor. Ojo: Debes tener permisos CHMOD de escritura (0777) para subir al servidor.";
$dltimageerrors6 = "El archivo que intentas borrar no existe. Intentelo de nuevo o seleccione otra imagen, por favor.";

$uploadimgerrors1 = "Ese archivo no es una imagen.";
$uploadimgerrors2 = "Lo siento, el archivo ya existe.";
$uploadimgerrors3 = "Lo siento, el archivo es demasiado grande.";
$uploadimgerrors4 = "Lo siento, solo se permiten archivos JPG, JPEG, PNG y GIF.";
$uploadimgerrors5 = "Lo siento, su archivo no se ha subido. Debes tener permisos CHMOD de escritura (0777) para subir al servidor.";
$uploadimgerrors6 = "Lo siento, hubo un error al subir el archivo.";
$uploadimgerrors7 = "- No olvides tener permisos CHMOD de escritura (0777) para subir al servidor.";

$loginerrors1 = "Usuario no encontrado, nombre o contraseñas incorrectos!";

$configerrors1 = "Usa las preferencias del plugin para cambiar su visibilidad o intentalo de nuevo, por favor.";
$configerrors2 = "Usa las preferencias del visor de imagenes para cambiar su visibilidad o intentalo de nuevo, por favor.";

$loginsite1 = "Hola!";
$loginsite2 = "Por favor, logueate,";
$loginsite3 = "Usuario";
$loginsite4 = "Contraseña";
$loginsite5 = "Acceder";

$createaccount1 = "Crea una nueva cuanta (local) para prevenir que otros puedan modificar las imagenes del servidor.";
$createaccount2 = "Como puedo desactivar la protección por contraseña? (link externo)";
$createaccount3 = "El directorio de los archivos por defecto es <b>ckeditor/plugins/imageuploader/uploads</b>. Puedes cambiar esto a través del panel de control.";

$langpanel1 = "Selecciona un idioma:";
$langpanel2 = "Idioma actual:";
$langpanel3 = "Cerrar";