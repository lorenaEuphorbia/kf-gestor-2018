<?php
	/// Cargamos las clases necesarias
	$modClass = new Secciones($b->db);

	if(strlen($act) > 3 && strlen($act) < 50) {

		if(file_exists("./_act/$mod/$act.php"))
			require "./_act/$mod/$act.php";

		else if ($modClass->search(['nombre'=>$act], 'Secciones')) {
			require "./_act/$mod/Claves.php";
		}

		else echo "Página no encontrada.";
	}

