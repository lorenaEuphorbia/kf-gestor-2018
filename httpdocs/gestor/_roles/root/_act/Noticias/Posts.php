<?php

	$arrCategorias = $modClass->getCategoriasIndexadasString ();
	$arrCategorias['0'] = 'Ninguna';
	$arrFirmantes = ['Klickfoto', "Alfredo", "Andrés"];

	switch ($opt) {
		case 'Clone':
		case 'Editor':
			$titulo = ["Nueva noticia", "Editando noticia"];

			$parent_id = isset($_POST["parent-id"]) ? $_POST["parent-id"] : (isset($_GET["parent-id"]) ? $_GET["parent-id"] : "");
			$parent_id = preg_replace("/[^\d]/", "", $parent_id);

			$id = isset($_POST["id"]) ? $_POST["id"] : (isset($_GET["id"]) ? $_GET["id"] : "");
			$id = preg_replace("/[^\d]/", "", $id);

			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "text",
						"db"		=> "id",
						"div"		=> true,
						"default"	=> true,
						"readonly"	=> true,
						"label"		=> _("ID"),
						'placeholder' => 'autoincremento',
						'div_class'	=> '',
						'checks'	=> ['autoincrement']
					],
					[
						"id"		=> "title",
						"type"		=> "text",
						"db"		=> "title",
						"length"	=> 255,
						"required"	=> true,
						"unique"	=> true,
						"div"		=> true,
						"label"		=> _("Titulo"),
					],
					[
						"id"		=> "category_id",
						"type"		=> "select-chosen",
						"db"		=> "category_id",
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Categoría"),
						"options"	=> [
							"empty"	=> "",
							"values" => $arrCategorias
						],
						"class"		=> "chosen"
					],
					[
						"id"		=> "tags",
						"type"		=> "tags",
						"db"		=> "tags",
						"div"		=> true,
						"label"		=> _("Tags"),
						"options"	=> [
							"values" => $modClass->getList("Tags", ["id", "text"], null, ["text"], null, null, "FETCH_NUM")
						],
						"shown"		=> [
							"required"	=> ["id"],
							"transform" => function($p) {
								if (!empty($p["id"])) {
									global $modClass;
									//var_dump($modClass->getRelTags($p["id"])); exit;
									return $modClass->getRelTags($p["id"]);
								}
								else return [];
							},
						],
						"save"		=> [
							"after"			=> true,
							"transform"		=> function($post_id) {
								if (!empty($post_id)) {
									$new_tags = !empty($_POST["tags"]) ? $_POST["tags"] : [];
									sort($new_tags);

									global $modClass;
									$current_tags = $modClass->getRelTags($post_id, ["tag_id"]);

									//var_dump($new_tags); exit;
									if ($current_tags != $new_tags) {
										$modClass->emptyRelTags($post_id);

										foreach ($new_tags as $tag_id) {
											$modClass->setRelTag($tag_id, $post_id);
										}

										return [
											"result"	=> 1,
											"message"	=> ["ok", _("Nuevos tags insertados correctamente.")]
										];
									}
									else {
										return [
											"result"	=> 0,
											"message"	=> ["warning", _("No se han modificado los tags ya que los datos enviados eran iguales a los existentes")]
										];
									}
								}
								else {
									return [
										"result"	=> -1,
										"message"	=> ["error", _("Se ha producido un error en el guardado de tags.")]
									];
								}
							}
						]
					],
					[
						"id"		=> "img",
						"type"		=> "input-image",
						"db"		=> "",
						"required"	=> true,
						"checks"	=> [],
						"div"		=> true,
						"div_class"	=> "input-image",
						"label"		=> _("Imagen"),
					],


					# SEO
					[
						"id"		=> "lang_es",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "Datos SEO",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						"id"		=> "url",
						"type"		=> "text",
						"db"		=> "url",
						"unique"	=> true,
						"required"	=> true,
						"checks"	=> ["slug"],
						"length"	=> 255,
						"div"		=> true,
						"label"		=> _("Slug (url amigable)"),
						"data"		=> [
							"slug"	=> "title"
						],
					],
					[
						"id"		=> "meta_titulo",
						"type"		=> "text",
						"db"		=> "meta_titulo",
						"required"	=> false,
						"length"	=> 250,
						"div"		=> true,
						"label"		=> _("Meta Título"),
						"class"		=> "char-counter",
						'placeholder' => 'requerido',
					],
					[
						"id"		=> "meta_descripcion",
						"type"		=> "textarea",
						"db"		=> "meta_descripcion",
						"required"	=> false,
						"length"	=> 250,
						"div"		=> true,
						"label"		=> _("Meta Descripción"),
						"class"		=> "char-counter",
						'placeholder' => 'requerido',
					],
					[
						"id"		=> "leido",
						"type"		=> "number",
						"db"		=> "leido",
						"div"		=> true,
						"readonly"	=> false,
						"label"		=> _("Veces leido"),
					],


					# FICHA TÉCNICA
					[
						"id"		=> "lang_es",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "DATOS DE LA NOTICIA",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						"id"		=> "intro",
						"type"		=> "textarea",
						"db"		=> "intro",
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Entradilla")
					],
					[
						"id"		=> "content",
						"type"		=> "ckeditor",
						"db"		=> "content",
						"required"	=> false,
						"div"		=> true,
						"label"		=> _("Contenido")
					],
					[
						"id"		=> "firma",
						"type"		=> "select-chosen",
						"db"		=> "firma",
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Firma"),
						"options"	=> [
							"empty"	=> "",
							"values" => $arrFirmantes
						],
						"class"		=> "chosen"
					],
					[
						"id"		=> "publicado",
						"type"		=> "checkbox",
						"db"		=> "publicado",
						"div"		=> true,
						"label"		=> _("Publicar <br />(Ver en la web)"),
					],
					// INGLÉS
					/*
					[
						"id"		=> "lang_en",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "INGLÉS",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						"id"		=> "title_2",
						"type"		=> "text",
						"db"		=> "title_2",
						"length"	=> 255,
						"required"	=> true,
						"unique"	=> true,
						"div"		=> true,
						"label"		=> _("Título"),
					],
					[
						"id"		=> "url_2",
						"type"		=> "text",
						"db"		=> "url_2",
						"unique"	=> true,
						"required"	=> true,
						"checks"	=> ["slug"],
						"length"	=> 255,
						"div"		=> true,
						"label"		=> _("Slug (url amigable)"),
						"data"		=> [
							"slug"	=> "title_2"
						],
					],
					[
						"id"		=> "intro_2",
						"type"		=> "textarea",
						"db"		=> "intro_2",
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Introducción"),
					],
					[
						"id"		=> "content_2",
						"type"		=> "ckeditor",
						"db"		=> "content_2",
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Contenido")
					],
					*/
					// FIN IDIOMAS
					/*
					[
						"id"		=> "noticias-galeria",
						"type"		=> "array",
						"class"		=> "sortable-portfolio",
						"db"		=> "",
						"mod"		=> "noticias",
						"act"		=> "galeria",
						"filter"	=> [],
						"on_fields"	=> ['id' => 'id_noticia'],
						"legend"	=> _("Galería"),
						"childs"	=> [
							[
								"id"		=> "id",
								"type"		=> "hidden",
								"db"		=> "id",
								"readonly"	=> true,
							],
							[
								"id"		=> "id_noticia",
								"type"		=> "hidden",
								"db"		=> "id_noticia",
								"readonly"	=> true,
								"default"	=> $id,
							],
							[
								"id"		=> "galery-img",
								"type"		=> "input-image",
								"db"		=> "",
								"required"	=> true,
								"checks"	=> [],
								"div"		=> true,
								"div_class"	=> "input-image input-compact cols two right",
								"label"		=> _("Imagen"),
							],
							[
								"id"		=> "galery-title",
								"type"		=> "text",
								"db"		=> "nombre",
								"length"	=> 255,
								"div"		=> true,
								"label"		=> _("Título"),
								"div_class"	=> "cols two left",
							],
							[
								"id"		=> "galery-alt",
								"type"		=> "text",
								"db"		=> "alt",
								"length"	=> 255,
								"div"		=> true,
								"label"		=> _("Alt"),
								"div_class"	=> "cols two left",
							],
						],
						"two-cols"	=> true,
					],
					*/
					# OTROS DATOS
					[
						"id"		=> "lang_es",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "OTROS DATOS",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						"id"		=> "publishing_date",
						"type"		=> "datetime-local",
						"db"		=> "publishing_date",
						"default"	=> date('Y-m-d h:i:s'),
						"checks"	=> ["datetime"],
						"div"		=> true,
						"label"		=> _("Fecha de creación"),
					],
					[
						"id"		=> "modification_date",
						"type"		=> "datetime-local",
						"db"		=> "modification_date",
						"default"	=> date('Y-m-d h:i:s'),
						"checks"	=> ["datetime"],
						"div"		=> true,
						"label"		=> _("Fecha de modificación"),
					],

				],
				"hidden" => []
			];

			$buttons = [
				//"new"	=> "Nuevo Post",
				"back"	=> _("Volver")
			];
		break;
		default: /// Opciones para el listado
			$titulo = "Listado de noticias";

			$list = [
				"id" => "post",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "category_id",
						"db"	=> "category_id",
						"title"	=> _("Categoría"),
						"shown"		=> function($val) {
							global $arrCategorias;
							return isset($arrCategorias[$val])? $arrCategorias[$val]: '';
						},
					],
					[
						"id"	=> "title",
						"db"	=> "title",
						"title"	=> _("Titulo"),
					],
					[
						"id"	=> "date",
						"db"	=> "date",
						"title"	=> _('<i class="fa fa-calendar" aria-hidden="true"></i> Publicación'),
						"shown"		=> function($val) {
							$creacion = new DateTime($val);
							return $creacion->format('d/m/Y');
						},
						"options" => [
							"printable"		=> "true",
						]
					],
					[
						"id"	=> "firma",
						"db"	=> "firma",
						"title"	=> _("Propietario"),
						"shown"		=> function($val) {
							global $arrFirmantes;
							return isset($arrFirmantes[$val])? $arrFirmantes[$val]: '';
						},
					],
					[
						"id"	=> "leido",
						"db"	=> "leido",
						"title"	=> _('<i class="fa fa-star" aria-hidden="true"></i>'),
						"shown"		=> function($val) { return $val; },
					],
					[
						"id"	=> "publicado",
						"db"	=> "publicado",
						"title"	=> _('<i class="fa fa-eye" aria-hidden="true"></i>'),
						"shown"		=> function($val) { return $val? '<i class="fa fa-check" aria-hidden="true"></i>': ''; },
					],
					/*
					[
						"id"	=> "comments",
						"db"	=> "id",
						"title"	=> _("Comentarios"),
						"shown"	=> function($val) {
							global $modClass;
							return count($modClass->getCommentsByPost($val));
						},
					],
					*/
				],
				"sort" => [
					["date", "DESC"],
					["title", "ASC"]
				],
				"actions" => [
					/*
					"archivos" => [
						"action"		=> "noticias/archivos/",
						"class" 		=> "fa fa-file-o",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Arhivos"),
					],
					*/
					"editor" => [
						"form-bt"		=> _("Nueva noticia"),
						"class" 		=> "fa fa-pencil",
						"text"			=> _("Editar"),
					],
					"galeria" => [
						"action"		=> "noticias/galeria/list/",
						"class" 		=> "fa fa-image",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Galer&iacute;a"),
					],
					/*"clone" => [
						"form-bt"		=> _("Duplicar noticia"),
						"class" 		=> "fa fa-clipboard",
						"text"			=> _("Duplicar"),
					],
					*/
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];
			$buttons = [
				"sort"	=> [
					"action"=> strtolower("$mod/$act/sort/"),
					"hidden"=> [],
					"text" 	=> _("Ordenar")
				],
			];

			if (empty($opt)) $opt = "List";
		break;
	}

	//echo __DIR__; exit;

	require "./_act/Base/Opt/$opt.php";
