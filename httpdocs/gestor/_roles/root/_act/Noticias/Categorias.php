<?php

	$arrCategorias = $modClass->getCategoriasIndexadasString ();
	$arrCategorias['0'] = 'Ninguna';

	switch ($opt) {
		case 'Editor':
			$titulo = ["Nueva categoría", "Editando categoría"];

			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "text",
						"db"		=> "id",
						"div"		=> true,
						"default"	=> true,
						"readonly"	=> true,
						"label"		=> _("ID"),
						'placeholder' => 'autoincremento',
						'div_class'	=> '',
						'checks'	=> ['autoincrement']
					],
					[
						"id"		=> "nombre",
						"type"		=> "text",
						"db"		=> "nombre",
						"required"	=> true,
						"length"	=> 250,
						"div"		=> true,
						"label"		=> _("Nombre"),
						"class"		=> "char-counter",
						'placeholder' => 'requerido',
					],
					/*
					[
						"id"		=> "img-galeria",
						"type"		=> "input-image",
						"db"		=> "",
						"required"	=> true,
						"checks"	=> [],
						"div"		=> true,
						"div_class"	=> "input-image",
						"label"		=> _("Imagen"),
					],
					*/

					# SEO
					[
						"id"		=> "lang_es",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "Datos SEO",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						"id"		=> "slug",
						"type"		=> "text",
						"db"		=> "url",
						"unique"	=> true,
						"required"	=> true,
						"checks"	=> ["slug"],
						"length"	=> 250,
						"div"		=> true,
						"label"		=> _("Slug (url amigable)"),
						"data"		=> [
							"slug"	=> "nombre"
						],
						"class"		=> "char-counter",
						'placeholder' => 'requerido',
					],
					[
						"id"		=> "meta_titulo",
						"type"		=> "text",
						"db"		=> "meta_titulo",
						"required"	=> true,
						"length"	=> 250,
						"div"		=> true,
						"label"		=> _("Meta Título"),
						"class"		=> "char-counter",
						'placeholder' => 'requerido',
					],
					[
						"id"		=> "meta_descripcion",
						"type"		=> "textarea",
						"db"		=> "meta_descripcion",
						"required"	=> true,
						"length"	=> 250,
						"div"		=> true,
						"label"		=> _("Meta Descripción"),
						"class"		=> "char-counter",
						'placeholder' => 'requerido',
					],


					# OTROS DATOS
					[
						"id"		=> "lang_es",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "OTROS DATOS",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						"id"		=> "created_at",
						"type"		=> "datetime-local",
						"db"		=> "created_at",
						"default"	=> date('Y-m-d h:i:s'),
						"checks"	=> ["datetime"],
						"div"		=> true,
						"label"		=> _("Fecha de creación"),
					],
					[
						"id"		=> "updated_at",
						"type"		=> "datetime-local",
						"db"		=> "updated_at",
						"default"	=> date('Y-m-d h:i:s'),
						"checks"	=> ["datetime"],
						"div"		=> true,
						"label"		=> _("Fecha de modificación"),
					],
					[
						"id"		=> "position",
						"type"		=> "number",
						"db"		=> "position",
						"default"	=> "0",
						"div"		=> true,
						"label"		=> _("Posición"),
						"placeholder"	=> "Determina la ordenación de categorías",
					],
					[
						"id"		=> "owner_id",
						"type"		=> "select-chosen",
						"db"		=> "owner_id",
						"default"	=> "0",
						"required"	=> false,
						"div"		=> true,
						"label"		=> _("Categoría Padre"),
						"options"	=> [
							"values" => $arrCategorias
						],
						"class"		=> "chosen"
					],
				],
				"hidden" => []
			];

			$buttons = [
				//"editor"	=> "Nueva Categoría",
				"back"	=> _("Volver")
			];
		break;
		case 'Sort':
			$titulo = "Ordenar $mod";

			$options = [
				"id"	 => "nombre",
				"fields" => [ "nombre"],
				"shown"	 => function($data) {
					return $data['nombre'];
				},
				"filter" => [
				],
				"sort"	 => [
					["position", "DESC"],
					["nombre", "ASC"],
				]
			];

			$buttons = [
				"back"	=> [
					"action" => strtolower("$mod/$act/list/"),
					"class"	 => "hidden",
					"hidden" => [],
					"text" 	 => _("Volver")
				]
			];

		break;
		default: /// Opciones para el listado
			$titulo = "Listado de categorías";

			$list = [
				"id" => "category-list",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "nombre",
						"db"	=> "nombre",
						"title"	=> _("Nombre"),
					],
					[
						"id"	=> "position",
						"db"	=> "position",
						"title"	=> _("Posicion"),
					]
				],
				"sort" => [
					["position", "ASC"],
					["nombre", "ASC"],
				],
				"actions" => [
					"editor" => [
						"form-bt"		=> _("Nueva categoría"),
						"class" 		=> "fa fa-pencil",
						"text"			=> _("Editar"),
					],
					"delete" => [
						"class"			=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			$buttons = [
				"sort"	=> [
					"action"=> strtolower("$mod/$act/sort/"),
					"hidden"=> [],
					"text" 	=> _("Ordenar")
				],
			];

			if (empty($opt)) $opt = "List";
		break;
	}
	
	require "./_act/Base/Opt/$opt.php";