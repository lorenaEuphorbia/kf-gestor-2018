<?php


	switch ($opt) {
		case 'Editor':
			$parent_id = isset($_POST["parent-id"]) ? $_POST["parent-id"] : (isset($_GET["parent-id"]) ? $_GET["parent-id"] : "");
			$parent_id = preg_replace("/[^\d]/", "", $parent_id);

			$id = isset($_POST["id"]) ? $_POST["id"] : (isset($_GET["id"]) ? $_GET["id"] : "");
			$id = preg_replace("/[^\d]/", "", $id);

			$id_tipo = 'noticias-galeria';

			//$arrOpciones = $modClass->getOptionSiblings($act);
			//$arrOpciones = array_combine($arrOpciones, $arrOpciones);

			$titulo = ["Nueva foto", "Editando foto"];
			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "text",
						"db"		=> "id",
						"div"		=> true,
						"readonly"	=> true,
						"label"		=> _("ID"),
						'placeholder' => 'autoincremento',
						'div_class'	=> 'cols two',
					],
					[
						"id"		=> "propietario",
						"type"		=> "hidden",
						"db"		=> "owner_id",
						"readonly"	=> true,
						"default"	=>$parent_id,
					],
					[
						"id"		=> "tipo",
						"type"		=> "hidden",
						"db"		=> "owner_type",
						"readonly"	=> true,
						"default"	=>$id_tipo,
					],
					[
						"id"		=> "img",
						"type"		=> "input-image",
						"db"		=> "",
						"required"	=> false,
						"checks"	=> [],
						"div"		=> true,
						"div_class"	=> "input-image",
						"label"		=> _("Imagen principal"),
					],
					[
						"id"		=> "title_1",
						"type"		=> "text",
						"db"		=> "title_1",
						"label"		=> _("Nombre"),
						'placeholder' => '',
						"length"	=> 20,
						"div"		=> true,
						"unique"	=> false,
						"required"	=> false,
						"readonly"	=> false,
						"disabeld"	=> false,
						"checks"	=> [],
						"data"		=> [],
						"options"	=> [],
						"shown"		=> [],
						"save"		=> [],
						"custom"	=> [],
						"options"	=> [],
						"class"		=> "char-counter",
						"div_class"	=> "",
					],
					[
						"id"		=> "name",
						"type"		=> "text",
						"db"		=> "name",
						"label"		=> _("Name"),
						'placeholder' => '',
						"length"	=> false,
						"div"		=> true,
						"unique"	=> false,
						"required"	=> false,
						"readonly"	=> false,
						"disabeld"	=> false,
						"checks"	=> [],
						"data"		=> [],
						"options"	=> [],
						"shown"		=> [],
						"save"		=> [],
						"custom"	=> [],
						"options"	=> [],
						"class"		=> "char-counter",
						"div_class"	=> "",
					],
				],
				"hidden" => []
			];

			$buttons = [
				//"new"	=> "Nuevo Post",
				"back"	=> _("Volver")
			];
		break;
		default: /// Opciones para el listado
			$id = isset($_POST["id"]) ? $_POST["id"] : (isset($_GET["id"]) ? $_GET["id"] : "");
			$id = preg_replace("/[^\d]/", "", $id);
			//var_dump($id);
			//'owner_type' => 'Galeria',

			$objParent = $modClass->getRecord ($mod, null, ['id'=>$id], true);

			$titulo = "Fotos de " . $objParent['nombre'];
			$list = [
				"id" => "post",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> _("Imagen"),
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "true",
							"searchable"	=> "false"
						],
						"shown"		=> function($val) {
							global $modClass, $act;

							if ($modClass->files->searchById($val)) {
								return '<img class="thmbnail" src="'.$modClass->files->getFilePath().'" />';
							}
							return '';
						},
					],
					[
						"id"	=> "name",
						"db"	=> "name",
						"title"	=> _("Nombre"),
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "true",
							"searchable"	=> "true"
						]
					],
					[
						"id"	=> "creacion",
						"db"	=> "creacion",
						"title"	=> _("Creación"),
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "true",
							"searchable"	=> "true"
						]
					],
				],
				"filter" => [
					'owner_id' => $id,
					'owner_type' => 'productos'
				],
				"sort" => [
					["creacion", "DESC"],
				],
				"actions" => [
					"editor" => [
						"form-bt"		=> _("Añadir imagen"),
						"class" 		=> "fa fa-pencil",
						//"form-bt-class"	=> "hidden",
						"text"			=> _("Editar"),
						"hidden"	=> [
							"parent-id"	=> $id
						]
					],
					"delete" => [
						"action"		=> strtolower($mod) . '/files/delete/',
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			$buttons = [
				//"sort"	=> [
				//	"action"=> strtolower("$mod/galeria/sort/"),
				//	"hidden"=> ["id"=> $id],
				//	"text" 	=> _("Ordenar")
				//],
				"back"	=> [
					"action"=> strtolower("$mod/$mod/list/"),
					"hidden"=> [],
					"text" 	=> _("Volver")
				]
			];
			//var_dump($arr_usuarios);

			if (empty($opt)) $opt = "List";
		break;
	}

	//echo __DIR__; exit;

	require "./_act/Base/Opt/$opt.php";
