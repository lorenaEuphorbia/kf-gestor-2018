<?php

	// Códigos http://www.lingoes.net/en/translator/langcode.htm

	switch ($opt) {
		case 'Editor':
			$titulo = ["Nuevo Idioma", "Editando Idioma"];

			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "hidden",
						"db"		=> "id",
						"readonly"	=> true,
					],
					[
						"id"		=> "codigo",
						"type"		=> "text",
						"db"		=> "codigo",
						"label"		=> _("Código"),
						"div"		=> true,
						"required"	=> true,
						"length"	=> 2,
						"class"		=> "char-counter",
						"placeholder" => "es",
					],
					[
						"id"		=> "region",
						"type"		=> "text",
						"db"		=> "region",
						"label"		=> _("Región"),
						"div"		=> true,
						"length"	=> 5,
						"class"		=> "char-counter",
						"placeholder" => "es-es",
						"checks"	=> ["slug"],
						"data"		=> [
							"slug"	=> "text"
						],
					],
					[
						"id"		=> "nombre_es",
						"type"		=> "text",
						"db"		=> "nombre_es",
						"label"		=> _("Nombre ES"),
						"div"		=> true,
						"required"	=> true,
						"length"	=> 50,
						"class"		=> "char-counter",
						"placeholder" => "Nombre en español",
					],
					[
						"id"		=> "nombre",
						"type"		=> "text",
						"db"		=> "nombre",
						"label"		=> _("Nombre"),
						"div"		=> true,
						"required"	=> true,
						"length"	=> 50,
						"class"		=> "char-counter",
						"placeholder" => "Nombre en el idioma",
					],
					[
						"id"		=> "activo",
						"type"		=> "checkbox",
						"db"		=> "activo",
						"label"		=> _("Activo"),
						"div"		=> true,
					],
					[
						"id"		=> "oculto",
						"type"		=> "checkbox",
						"db"		=> "oculto",
						"label"		=> _("Ocultar en gestor"),
						"div"		=> true,
					],
				],
				"hidden" => [
				]
			];


			$buttons = [
				//"editor"	=> "Nuevo Post",
				"back"	=> _("Volver")
			];
		break;
		default: /// Opciones para el listado
			$titulo = "Listado de Idiomas";

			$list = [
				"id" => "claves-list",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "codigo",
						"db"	=> "codigo",
						"title"	=> _("Código"),
					],
					[
						"id"	=> "region",
						"db"	=> "region",
						"title"	=> _("Región"),
					],
					[
						"id"	=> "nombre",
						"db"	=> "nombre",
						"title"	=> _("Nombre"),
					],
					[
						"id"	=> "activo",
						"db"	=> "activo",
						"title"	=> _('<i class="fa fa-check-circle" aria-hidden="true"></i>'),
						"shown"		=> function($val) { return $val? 'x': ''; },
					],
					[
						"id"	=> "oculto",
						"db"	=> "oculto",
						"title"	=> _('<i class="fa fa-eye-slash" aria-hidden="true"></i>'),
						"shown"		=> function($val) { return $val? 'x': ''; },
					],
				],
				"sort" => [
					["region", "ASC"]
				],
				"filter" => [],
				"actions" => [
					"editor" => [
						"form-bt"		=> _("Nuevo idioma"),
						"class" 		=> "fa fa-pencil",
						"text"			=> _("Editar"),
					],
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			if (empty($opt)) $opt = "List";
		break;
	}

	require "./_act/Base/Opt/$opt.php";