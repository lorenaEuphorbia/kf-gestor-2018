<?php

	switch ($opt) {
		case 'Editor':
			$titulo = ["Nueva sección", "Editando sección"];

			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "hidden",
						"db"		=> "id",
						"readonly"	=> true,
					],
					[
						"id"		=> "nombre",
						"type"		=> "text",
						"db"		=> "nombre",
						"readonly"	=> true,
						"div"		=> true,
						"length"	=> 200,
						"label"		=> _("Nombre (Auto)"),
						"save"		=> [
							"after"			=> true,
							"transform"		=> function($post_id) {

								if (!empty($post_id)) {
									global $modClass;

									if($modClass->search(['id'=>$post_id], 'Secciones')) {

										$strNombre = $modClass->getCurrentFieldValue('slug', 'Secciones');
										$strNombre = Utilities::convertToPascalCase($strNombre);

										if($modClass->setRecord(['nombre'=>$strNombre], ['id'=>$post_id], 'Secciones'))
											return [
												"result"	=> 1,
												"message"	=> ["ok", _("Pascal case modificado correctamente.")]
											];
										else
											return [
												"result"	=> 0,
												"message"	=> ["warning", _("No se han modificado los textos ya que los datos enviados eran iguales a los existentes")]
											];

									}
									else {
										return [
											"result"	=> -1,
											"message"	=> ["error", _("Se ha producido un error en el guardado del pascal case.")]
										];
									}
								}
							}
						],
					],
					[
						"id"		=> "slug",
						"type"		=> "text",
						"db"		=> "slug",
						"unique"	=> true,
						"required"	=> true,
						"length"	=> 200,
						"checks"	=> ["slug"],
						"div"		=> true,
						"label"		=> _("Slug (id amigable)"),
						"data"		=> [
							"slug"	=> "text"
						],
					],
					[
						"id"		=> "logo",
						"type"		=> "text",
						"db"		=> "logo",
						"length"	=> 200,
						"div"		=> true,
						"label"		=> _("Logo FA"),
					],
					[
						"id"		=> "oculto",
						"type"		=> "checkbox",
						"db"		=> "oculto",
						"div"		=> true,
						"label"		=> _("Ocultar"),
					],
				],
				"hidden" => []
			];

			$buttons = [
				//"editor"	=> "Nuevo Post",
				"back"	=> _("Volver")
			];
		break;
		case 'Sort':
			$id = isset($_POST["id"]) ? $_POST["id"] : (isset($_GET["id"]) ? $_GET["id"] : "");
			$id = preg_replace("/[^\d]/", "", $id);
			$id = 0;

			$titulo = "Ordenar Secciones";

			$options = [
				"id"	 => "secciones-list",
				"fields" => ["nombre","oculto"],
				"shown"	 => function($row){
					return $row["nombre"] . ($row["oculto"]?' [oculto]':'') ;
				},
				"filter" => ["parent_id" => $id],
				"sort"	 => [
					["position", "DESC"],
				]
			];

			$buttons = [
				"back"	=> [
					"action" => strtolower("$mod/$act/list/"),
					"class"	 => "hidden",
					"hidden" => [],
					"text" 	 => _("Volver")
				]
			];

		break;
		default: /// Opciones para el listado
			$titulo = "Listado de secciones";

			$list = [
				"id" => "secciones-list",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "nombre",
						"db"	=> "nombre",
						"title"	=> _("Nombre"),
					],
					[
						"id"	=> "slug",
						"db"	=> "slug",
						"title"	=> _("Slug"),
					],
					[
						"id"	=> "oculto",
						"db"	=> "oculto",
						"title"	=> _('<i class="fa fa-eye-slash" aria-hidden="true"></i>'),
						"shown"		=> function($val) { return $val? 'x': ''; },
					],

				],
				"filter"=> [
				],
				"sort" => [
					["position", "DESC"]
				],
				"actions" => [
					"editor" => [
						"form-bt"		=> _("Nueva seccion"),
						"class" 		=> "fa fa-pencil",
						"text"			=> _("Editar"),
					],
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			$buttons = [
				"sort"	=> [
					"action"=> strtolower("$mod/$mod/sort/"),
					"hidden"=> [],
					"text" 	=> _("Ordenar")
				]
			];
		break;
	}

	require "./_act/Base/Opt/$opt.php";