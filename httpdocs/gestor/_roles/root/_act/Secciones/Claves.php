<?php

if($modClass->search(['nombre'=>$act], 'Secciones')) {

	$idSeccion =  $modClass->getCurrentFieldValue('id', 'Secciones');
	$arrIdiomas = $modClass->getList('Idiomas', [], ['activo'=>'1']);

	switch ($opt) {
		case 'Editor':

			$objClave = false;
			$strTipo = $strClass = 'text';
			if(isset($_REQUEST['id']) && $modClass->search(['id'=>$_REQUEST['id']], 'Claves')) {
				$objClave = $modClass->getCurrent('Claves');
				$strTipo = $objClave['type'];
				$strClass =  $objClave['type'];
			}

			$titulo = ["Nuevo Texto", "Editando Texto"];

			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "hidden",
						"db"		=> "id",
						"readonly"	=> true,
					],
					[
						"id"		=> "id_seccion",
						"type"		=> "hidden",
						"db"		=> "id_seccion",
						"readonly"	=> true,
						"default"	=> "$idSeccion",
					],
					[
						"id"		=> "nombre",
						"type"		=> "text",
						"db"		=> "nombre",
						"required"	=> true,
						"length"	=> 50,
						"checks"	=> ["slug"],
						"div"		=> true,
						"label"		=> _("Slug (url amigable)"),
						"data"		=> [
							"slug"	=> "text"
						],
					],
					/*
					[
						"id"		=> "type",
						"type"		=> "select",
						"db"		=> "type",
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Tipo"),
						"options"	=> [
							"values" => [
								"number" => "number",
								"ckeditor" => "ckeditor",
								"textarea" => "textarea",
								"checkbox" => "checkbox",
								"font-awesome-picker" => "font-awesome-picker",
								"input-image" => "input-image",
								"input-file" => "input-file",
								"heading" => "heading",
								"link" => "link",
								"select" => "select",
								"tags" => "tags",
								"text" => "text",
								"title" => "title",
							]
						],
					],
					*/
				],
				"hidden" => [
				]
			];

			foreach ($arrIdiomas as $index => $idioma) {
				$fields['db'][] = [
					"id"		=> "lang-".$idioma['region'],
					"type"		=> $strTipo,
					"db"		=> "",
					"div"		=> true,
					"div_class"		=> $strClass,
					"id_idioma" => $idioma['id'],
					"label"		=> _($idioma['nombre_es']),
					"shown"		=> [
						"required"	=> ["id"],
						"transform" => function($c) {

							if (!empty($c["id"])) {
								global $modClass, $field;
								$result = $modClass->getList('Textos', ['valor'], ['id_clave'=>$c["id"],'id_idioma'=>$field['id_idioma']]);
								return $result ? $result[0]['valor']: '';
							}
							else '';
						},
					],
					"save"		=> [
						"after"			=> true,
						"transform"		=> function($post_id) {

							if (!empty($post_id)) {
								global $modClass, $field;

								$valor = !empty($field["id"]) && isset($_POST[$field["id"]])? $_POST[$field["id"]] : [];
								$arrFields = [
									'id_clave' => $post_id,
									'id_idioma' => $field['id_idioma'],
									'valor' => $valor,
								];
								$arrWhere = [
									'id_clave' => $post_id,
									'id_idioma' => $field['id_idioma'],
								];

								if(!$modClass->getList('Textos', ['valor'], $arrWhere))
									$arrWhere = [];

								if($modClass->setRecord($arrFields, $arrWhere, 'Textos')) {
									return [
										"result"	=> 1,
										"message"	=> ["ok", _("Nuevos textos insertados correctamente.")]
									];
								}
								else {
									return [
										"result"	=> 0,
										"message"	=> ["warning", _("No se han modificado los textos ya que los datos enviados eran iguales a los existentes")]
									];
								}
							}
							else {
								return [
									"result"	=> -1,
									"message"	=> ["error", _("Se ha producido un error en el guardado de los textos.")]
								];
							}
						}
					],
				];
			}

			$buttons = [
				//"editor"	=> "Nuevo Post",
				"back"	=> _("Volver")
			];
		break;
		default: /// Opciones para el listado
			$titulo = "Listado de Claves";

			$list = [
				"id" => "claves-list",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "nombre",
						"db"	=> "nombre",
						"title"	=> _("Nombre"),
					],
				],
				"sort" => [
					["nombre", "ASC"]
				],
				"filter" => [
					'id_seccion' => $idSeccion
				],
				"actions" => [
					"editor" => [
						"form-bt"		=> _("Nuevo texto"),
						"class" 		=> "fa fa-pencil",
						"text"			=> _("Editar"),
					],
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			if (empty($opt)) $opt = "List";
		break;
	}

	require "./_act/Base/Opt/$opt.php";
}