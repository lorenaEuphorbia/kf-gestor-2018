<?php

	switch ($opt) {
		case 'Editor':
		case 'EditorSave':
			$titulo = ["Nuevo tag", "Editando tag"];

			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "text",
						"db"		=> "id",
						"div"		=> true,
						"default"	=> true,
						"readonly"	=> true,
						"label"		=> _("ID"),
						'placeholder' => 'autoincremento',
						'div_class'	=> '',
						'checks'	=> ['autoincrement']
					],
					[
						"id"		=> "text",
						"type"		=> "text",
						"db"		=> "text",
						"unique"	=> true,
						"required"	=> true,
						"length"	=> 255,
						"div"		=> true,
						"label"		=> _("Texto"),
					],
					[
						"id"		=> "url",
						"type"		=> "text",
						"db"		=> "url",
						"unique"	=> true,
						"required"	=> true,
						"length"	=> 255,
						"checks"	=> ["slug"],
						"div"		=> true,
						"label"		=> _("Slug (url amigable)"),
						"data"		=> [
							"slug"	=> "text"
						],
					],
				],
				"hidden" => [
					"m"	=> get_class($modClass), // módulo
					"a"	=> $act // acción
				]
			];

			$view = "./_inc/theme/editor-modal.php";

			// require "./_act/Base/Opt/$opt.php";
		break;

		default: /// Opciones para el listado
			$titulo = "Listado de tags";

			$list = [
				"id" => "tag-list",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "text",
						"db"	=> "text",
						"title"	=> _("Texto"),
					]
				],
				"sort" => [
					["text", "ASC"]
				],
				"actions" => [
					"editor" => [
						"form-bt"		=> _("Nuevo tag"),
						"form-bt-class"	=> "open-modal",
						"class" 		=> "fa fa-pencil",
						"text"			=> _("Editar"),
						"hidden"		=> [
							"mod"	=> $mod,
							"act"	=> $act
						]
					],
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			if (empty($opt)) $opt = "List";
		break;
	}
	
	require "./_act/Base/Opt/$opt.php";