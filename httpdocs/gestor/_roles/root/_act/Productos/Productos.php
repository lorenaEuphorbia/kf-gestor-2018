
<?php

	$arr_categories = $modClass->getCategoriasIndexadasString ();

	switch ($opt) {
		case 'Clone':
		case 'Editor':

		$id = isset($_POST["id"]) ? $_POST["id"] : (isset($_GET["id"]) ? $_GET["id"] : "");
		$id = preg_replace("/[^\d]/", "", $id);

			$titulo = ["Nuevo Trabajo ", "Editando Trabajo"];

			$fields = [
				"db" => [

					[
						"id"		=> "id",
						"type"		=> "text",
						"db"		=> "id",
						"div"		=> true,
						"readonly"	=> true,
						"label"		=> _("ID"),
						'placeholder' => 'autoincremento',
					],
					[
						"id"		=> "nombre",
						"type"		=> "text",
						"db"		=> "nombre",
						"required"	=> true,
						"length"	=> 250,
						"div"		=> true,
						"label"		=> _("Nombre"),
						"class"		=> "char-counter",
						'placeholder' => 'requerido',
					],
					[
						"id"		=> "url",
						"type"		=> "text",
						"db"		=> "url",
						"unique"	=> true,
						"required"	=> true,
						"checks"	=> ["slug"],
						"length"	=> 250,
						"div"		=> true,
						"label"		=> _("Slug (url amigable)"),
						"data"		=> [
							"slug"	=> "nombre"
							],
						"class"		=> "char-counter",
						'placeholder' => 'requerido',
					],
					[
						"id"		=> "id_categoria",
						"type"		=> "select-chosen",
						"db"		=> "id_categoria",
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Categoría"),
						"options"	=> [
							"empty"	=> "",
							"values" => $arr_categories
						],
						"class"		=> "chosen"
					],
					[
						"id"		=> "img",
						"type"		=> "input-image",
						"db"		=> "",
						"required"	=> false,
						"checks"	=> [],
						"div"		=> true,
						"div_class"	=> "input-image",
						"label"		=> _("Imagen principal"),
					],
					[
						"id"		=> "video",
						"type"		=> "text",
						"db"		=> "video",
						"required"	=> false,
						"div"		=> true,
						"label"		=> _("viemeo video"),
						'placeholder' => 'obligatorio si es de Klick Video',
					],


					# INFORMACIÓN
					[
						"id"		=> "lang_es",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "DESCRIPCIÓN DEL TRABAJO",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						"id"		=> "descripcion",
						"type"		=> "textarea",
						"db"		=> "descripcion",
						"div"		=> true,
						"label"		=> _("Texto introducción"),
						"placeholder"	=> "opcional",
					],
					[
						"id"		=> "caracteristicas",
						"type"		=> "ckeditor",
						"db"		=> "caracteristicas",
						"div"		=> true,
						"label"		=> _("Descripción del trabajo"),
					],
					[
						"id"		=> "lugar",
						"type"		=> "text",
						"db"		=> "lugar",
						"div"		=> true,
						"label"		=> _("Lugar del trabajo"),
						"placeholder"	=> "opcional",
					],
					[
						"id"		=> "fecha",
						"type"		=> "text",
						"db"		=> "fecha_realizacion",
						"div"		=> true,
						"label"		=> _("Fecha de realización del trabajo"),
						"placeholder"	=> "opcional",
					],

					# EXTRAS
					[
						"id"		=> "lang_es",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "MARCAR ESTE TRABAJO COMO",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						"id"		=> "publicado",
						"type"		=> "checkbox",
						"db"		=> "publicado",
						"div"		=> true,
						"label"		=> _("Publicar <br />(Ver en la web)"),
					],
					[
						"id"		=> "es_novedad",
						"type"		=> "checkbox",
						"db"		=> "es_novedad",
						"div"		=> true,
						"label"		=> _("Marcar como novedad"),
					],
					[
						"id"		=> "lang_es",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "Galeria de acceso privado",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],

					[
						"id"		=> "password",
						"type"		=> "text",
						"db"		=> "password",
						"required"	=> false,
						"div"		=> true,
						"label"		=> _("Password para ver galeria"),
						'placeholder' => 'obligatorio si tiene galería privada',
					],

					[
						"id"		=> "ruta",
						"type"		=> "text",
						"db"		=> "nombre_carpeta",
						"required"	=> false,
						"div"		=> true,
						"label"		=> _("nombre de la carpeta de fotos privadas"),
						'placeholder' => 'obligatorio si tiene galería privada',
					],

					[
						"id"		=> "numero",
						"type"		=> "text",
						"db"		=> "numero_fotos",
						"required"	=> false,
						"div"		=> true,
						"label"		=> _("numero de fotos privadas"),
						'placeholder' => 'obligatorio si tiene galería privada',
					],


					# SEO
					[
							"id"		=> "lang_es",
							"type"		=> "title",
							"db"		=> "",
							"options"	=> [
								"text" => "Datos SEO",
								"type" => "h4",
								"class" => "sectionTitle"
							],
							"label"		=> _(" "),
					],
					[
						"id"		=> "meta_titulo",
						"type"		=> "text",
						"db"		=> "meta_titulo",
						"required"	=> true,
						"length"	=> 250,
						"div"		=> true,
						"label"		=> _("Meta Título"),
						"class"		=> "char-counter",
						'placeholder' => 'requerido',
					],
					[
						"id"		=> "meta_descripcion",
						"type"		=> "textarea",
						"db"		=> "meta_descripcion",
						"required"	=> true,
						"length"	=> 250,
						"div"		=> true,
						"label"		=> _("Meta Descripción"),
						"class"		=> "char-counter",
						'placeholder' => 'requerido',
					],

					[
						"id"		=> "created_at",
						"type"		=> "datetime-local",
						"db"		=> "created_at",
						"default"	=> date('Y-m-d h:i:s'),
						"checks"	=> ["datetime"],
						"div"		=> true,
						"label"		=> _("Fecha de creación"),
					],

					# OTROS DATOS
/*[
						"id"		=> "lang_es",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "OTROS DATOS",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						"id"		=> "created_at",
						"type"		=> "datetime-local",
						"db"		=> "created_at",
						"default"	=> date('Y-m-d h:i:s'),
						"checks"	=> ["datetime"],
						"div"		=> true,
						"label"		=> _("Fecha de creación"),
					],
					[
						"id"		=> "updated_at",
						"type"		=> "datetime-local",
						"db"		=> "updated_at",
						"default"	=> date('Y-m-d h:i:s'),
						"checks"	=> ["datetime"],
						"div"		=> true,
						"label"		=> _("Fecha de modificación"),
					],
					[
						"id"		=> "locked",
						"type"		=> "checkbox",
						"db"		=> "locked",
						"div"		=> true,
						"label"		=> _("Bloqueado"),
					],
*/
				],
				"hidden" => [],
			];

			$buttons = [
				//"new"	=> "Nuevo Post",
				"back"	=> _("Volver")
			];
		break;
		case 'Sort':
			$id = isset($_POST["id"]) ? $_POST["id"] : (isset($_GET["id"]) ? $_GET["id"] : "");
			$id = preg_replace("/[^\d]/", "", $id);

			$titulo = "Ordenar Trabajos";

			$options = [
				"id"	 => "nombre",
				"fields" => ["id_categoria", "nombre"],
				"shown"	 => function($data) {
					global $modClass, $arr_categories;

					$categoria = !empty($arr_categories[$data['id_categoria']])? $arr_categories[$data['id_categoria']]: 'Cat. No encontrada';
					$outData = str_pad($categoria, 100, " _", STR_PAD_RIGHT) . $data['nombre'];
					return str_replace('_', '&nbsp;', $outData);
				},
				"filter" => [
				],
				"sort"	 => [
					["position", "DESC"],
					["nombre", "ASC"],
				]
			];

			$buttons = [
				"back"	=> [
					"action" => strtolower("$mod/$act/list/"),
					"class"	 => "hidden",
					"hidden" => ["id"=> $id],
					"text" 	 => _("Volver")
				]
			];

		break;
		default: /// Opciones para el listado
			$titulo = "Listado de trabajos";

			$list = [
				"id" => "productos",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "nombre",
						"db"	=> "nombre",
						"title"	=> _("Nombre"),
					],
					[
						"id"	=> "es_privado",
						"db"	=> "es_privado",
						"title"	=> _('Privado'),
						"shown"		=> function($val) { return $val? '<i class="fa fa-lock" aria-hidden="true"></i> privado': '<i class="fa fa-unlock" aria-hidden="true"></i> público'; },
					],

					[
						"id"	=> "id_categoria",
						"db"	=> "id_categoria",
						"title"	=> _("Categoría"),
						"shown"		=> function($val) {
							global $arr_categories;
							return isset($arr_categories[$val])? $arr_categories[$val]: '';
						},
					],


					[
						"id"	=> "created_at",
						"db"	=> "created_at",
						"title"	=> _('<i class="fa fa-calendar" aria-hidden="true"></i> '),
						"shown"		=> function($val) {
							$creacion = new DateTime($val);
							return $creacion->format('d/m/Y');
						},
						"options" => [
							"printable"		=> "true",
						]
					],
					[
						"id"	=> "publicado",
						"db"	=> "publicado",
						"title"	=> _('<i class="fa fa-eye" aria-hidden="true"></i>'),
						"shown"		=> function($val) { return $val? '<i class="fa fa-check" aria-hidden="true"></i>': ''; },
					],

				],
				"filter" => [
				],
				"sort" => [
					["url", "ASC"],
				],
				"actions" => [
					/*
					"comments" => [
						"action"		=> "productos/comments/list/",
						"class" 		=> "fa fa-commenting-o",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Comentarios"),
					],

					*/

					"galeria" => [
						"action"		=> "productos/galeria/list/",
						"class" 		=> "fa fa-image",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Galer&iacute;a"),
						"hidden"	=> [  ]
					],
					/*
					"files" => [
						"action"		=> "productos/files/list/",
						"class" 		=> "fa fa-file-o",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Archivos"),
						"hidden"	=> [  ]
					],
					*/
					"editor" => [
						"form-bt"		=> _("Añadir trabajo"),
						"class" 		=> "fa fa-pencil",
						//"form-bt-class"	=> "hidden",
						"text"			=> _("Editar"),
						"hidden"	=> [ ]
					],
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					],
				],
				"multiple_selection" => true,
				"extra-buttons" => ""
			];

			$buttons = [
				"sort"	=> [
					"action"=> strtolower("$mod/$act/sort/"),
					"hidden"=> [],
					"text" 	=> _("Ordenar")
				],
			];
			//var_dump($
			//var_dump($arr_usuarios);

			if (empty($opt)) $opt = "List";
		break;
	}

	//echo __DIR__; exit;

	require "./_act/Base/Opt/$opt.php";
