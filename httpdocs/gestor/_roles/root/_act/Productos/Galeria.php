<?php
	switch ($opt) {
		case 'Editor':
			$parent_id = isset($_POST["parent-id"]) ? $_POST["parent-id"] : (isset($_GET["parent-id"]) ? $_GET["parent-id"] : "");
			$parent_id = preg_replace("/[^\d]/", "", $parent_id);

			//var_dump($parent_id);exit;
			$objCategoria = $modClass->getRecord ('Productos', null, ['id'=>$parent_id], true);
			//var_dump($parent_id);

			$titulo = ["Nueva Imagen ", "Editando Imagen "];
			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "hidden",
						"db"		=> "id",
						"readonly"	=> true,
					],
					[
						"id"		=> "id_tipo",
						"type"		=> "hidden",
						"db"		=> "id_tipo",
						"readonly"	=> true,
						"default"	=> '0',
					],
					[
						"id"		=> "id_catalogo",
						"type"		=> "hidden",
						"db"		=> "id_catalogo",
						"readonly"	=> true,
						"default"	=> $parent_id,
					],
					[
						"id"		=> "owner_id",
						"type"		=> "hidden",
						"db"		=> "owner_id",
						"readonly"	=> true,
						"default"	=> $parent_id,
					],
					[
						"id"		=> "creacion",
						"type"		=> "hidden",
						"db"		=> "creacion",
						"readonly"	=> true,
						"default"	=> date("Y-m-d H:i:s"),
					],
					[
						"id"		=> "galeria",
						"type"		=> "input-image",
						"db"		=> "",
						"checks"	=> [],
						"div"		=> true,
						"div_class"	=> "input-image",
						"label"		=> _("Imagen"),
					],
					[
						"id"		=> "nombre",
						"type"		=> "text",
						"db"		=> "nombre",
						"length"	=> 250,
						"required"	=> false,
						"div"		=> true,
						"label"		=> _("Título"),
						"class"		=> "char-counter",
						'placeholder' => 'Título de la imagen',
					],
				],
				"hidden" => ['parent-id'=>$parent_id],
			];

			$buttons = [
				"back"	=> [
					"action" => strtolower("$mod/$act/list/"),
					"class"	 => "hidden",
					"hidden" => [],
					"text" 	 => _("Volver")
				]
			];

		break;
		case 'Sort':
			$id = isset($_POST["id"]) ? $_POST["id"] : (isset($_GET["id"]) ? $_GET["id"] : "");
			$id = preg_replace("/[^\d]/", "", $id);

			$titulo = "Ordenar Galería de imágenes";
				$options = [
					"id"	 => "galeria-list",
					"fields" => ["id", "nombre"],
					"shown"	 => function($data) {
						global $modClass;

						if ($modClass->files->searchByOwner($data['id'], 'galeria','Galeria')) {
							return'<div class="sort-list"><img class="thmbnail" src="'.$modClass->files->getFilePath().'" /><h3>'.$data['nombre'].'</h3></div>';
						}
						return '';
					},
					"filter" => [
						'id_tipo' => '0',
						'id_catalogo' => $id,
					],
					"sort"	 => [
						["position", "DESC"],
						["nombre", "ASC"],
					]
				];

				$buttons = [
					"back"	=> [
						"action" => strtolower("$mod/$act/list/"),
						"class"	 => "hidden",
						"hidden" => ["id"=> $id],
						"text" 	 => _("Volver")
					]
				];

		break;
		default: /// Opciones para el listado
			$id = isset($_POST["id"]) ? $_POST["id"] : (isset($_GET["id"]) ? $_GET["id"] : "");
			$id = preg_replace("/[^\d]/", "", $id);
			//var_dump($id);

			$objCategoria = $modClass->getRecord ('Archivos', null, ['id'=>$id], true);

			$titulo = "Galería de imágenes" . $objCategoria['nombre'];
			$list = [
				"id" => "post",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "position",
						"db"	=> "position",
						"title"	=> "POSICION",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> _("Imagen"),
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "true",
							"searchable"	=> "false"
						],
						"shown"		=> function($val) {
							global $modClass;

							if ($modClass->files->searchByOwner($val, 'galeria','Galeria')) {
								return'<img class="thmbnail" src="'.$modClass->files->getFilePath().'" />';
							}
							return '';
						},
					],
					[
						"id"	=> "nombre",
						"db"	=> "nombre",
						"title"	=> _("Nombre"),
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "true",
							"searchable"	=> "true"
						]
					],
				],
				"filter" => [
					'id_tipo' => '0',
					'id_catalogo' => $id,
				],
				"sort" => [
					["position", "DESC"],
					["nombre", "ASC"],
				],
				"actions" => [
					"editor" => [
						"form-bt"		=> _("Añadir imagen"),
						"class" 		=> "fa fa-pencil",
						//"form-bt-class"	=> "hidden",
						"text"			=> _("Editar"),
						"hidden"	=> [
							"parent-id"	=> $id
						]
					],
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			$buttons = [
				"sort"	=> [
					"action"=> strtolower("$mod/galeria/sort/"),
					"hidden"=> ["id"=> $id],
					"text" 	=> _("Ordenar")
				],
				"back"	=> [
					"action"=> strtolower("$mod/productos/list/"),
					"hidden"=> [],
					"text" 	=> _("Volver")
				]
			];
			//var_dump($arr_usuarios);

			if (empty($opt)) $opt = "List";
		break;
	}

	//echo __DIR__; exit;

	require "./_act/Base/Opt/$opt.php";
