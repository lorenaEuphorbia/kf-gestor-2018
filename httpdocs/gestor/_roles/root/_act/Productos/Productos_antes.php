
<?php

	$arr_categories = $modClass->getCategoriasIndexadasString ();

	switch ($opt) {
		case 'Clone':
		case 'Editor':

		$id = isset($_POST["id"]) ? $_POST["id"] : (isset($_GET["id"]) ? $_GET["id"] : "");
		$id = preg_replace("/[^\d]/", "", $id);

			$titulo = ["Nuevo Trabajo ", "Editando Trabajo"];

			$fields = [
				"db" => [

					[
						"id"		=> "id",
						"type"		=> "text",
						"db"		=> "id",
						"div"		=> true,
						"readonly"	=> true,
						"label"		=> _("ID"),
						'placeholder' => 'autoincremento',
					],
					[
						"id"		=> "nombre",
						"type"		=> "text",
						"db"		=> "nombre",
						"required"	=> true,
						"length"	=> 250,
						"div"		=> true,
						"label"		=> _("Nombre"),
						"class"		=> "char-counter",
						'placeholder' => 'requerido',
					],
					[
						"id"		=> "url",
						"type"		=> "text",
						"db"		=> "url",
						"unique"	=> true,
						"required"	=> true,
						"checks"	=> ["slug"],
						"length"	=> 250,
						"div"		=> true,
						"label"		=> _("Slug (url amigable)"),
						"data"		=> [
							"slug"	=> "nombre"
							],
						"class"		=> "char-counter",
						'placeholder' => 'requerido',
					],
					[
						"id"		=> "id_categoria",
						"type"		=> "select-chosen",
						"db"		=> "id_categoria",
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Categoría"),
						"options"	=> [
							"empty"	=> "",
							"values" => $arr_categories
						],
						"class"		=> "chosen"
					],
					[
						"id"		=> "img",
						"type"		=> "input-image",
						"db"		=> "",
						"required"	=> false,
						"checks"	=> [],
						"div"		=> true,
						"div_class"	=> "input-image",
						"label"		=> _("Imagen principal"),
					],


					# INFORMACIÓN
					[
						"id"		=> "lang_es",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "DESCRIPCIÓN DEL TRABAJO",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						"id"		=> "descripcion",
						"type"		=> "textarea",
						"db"		=> "descripcion",
						"div"		=> true,
						"label"		=> _("Texto introducción"),
						"placeholder"	=> "opcional",
					],
					[
						"id"		=> "caracteristicas",
						"type"		=> "ckeditor",
						"db"		=> "caracteristicas",
						"div"		=> true,
						"label"		=> _("Descripción del trabajo"),
					],
					[
						"id"		=> "lugar",
						"type"		=> "text",
						"db"		=> "lugar",
						"div"		=> true,
						"label"		=> _("Lugar del trabajo"),
						"placeholder"	=> "opcional",
					],
					[
						"id"		=> "fecha",
						"type"		=> "text",
						"db"		=> "fecha_realizacion",
						"div"		=> true,
						"label"		=> _("Fecha de realización del trabajo"),
						"placeholder"	=> "opcional",
					],

					# EXTRAS
					[
						"id"		=> "lang_es",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "MARCAR ESTE TRABAJO COMO",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						"id"		=> "publicado",
						"type"		=> "checkbox",
						"db"		=> "publicado",
						"div"		=> true,
						"label"		=> _("Publicar <br />(Ver en la web)"),
					],
					[
						"id"		=> "es_novedad",
						"type"		=> "checkbox",
						"db"		=> "es_novedad",
						"div"		=> true,
						"label"		=> _("Marcar como novedad"),
					],
					[
						"id"		=> "lang_es",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "Si este es un trabajo de acceso privado (galería) marcar como privado y poner password",
							"type" => "h4",
							"class" => "sectionSubTitle"
						],
						"label"		=> _(" "),
					],
					[
						"id"		=> "es_privado",
						"type"		=> "checkbox",
						"db"		=> "es_privado",
						"div"		=> true,
						"label"		=> _("Marcar si es galería privada"),
					],
					[
						"id"		=> "password",
						"type"		=> "text",
						"db"		=> "password",
						"required"	=> false,
						"div"		=> true,
						"label"		=> _("Password para ver galeria"),
						'placeholder' => 'obligatorio si es una galería privada',
					],

					# SELECCION DE TRABAJOS RELACIONADOS
					[
						"id"		=> "lang_es",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "Si este es un trabajo de acceso público y tiene asociado una galería privada selecciona la galería aquí",
							"type" => "h4",
							"class" => "sectionSubTitle"
						],
						"label"		=> _(" "),
					],
					[
						"id"		=> "relaccionados",
						"type"		=> "tags",
						"db"		=> "relaccionados",
						"div"		=> true,
						"label"		=> _("Galeria(s) privada(s) relacionada(s)"),
						"options"	=> [
							"values" => $modClass->getList("Productos", ["id", "nombre"], null, ["position DESC"], null, null, "FETCH_NUM")
						],
						"shown"		=> [
							"required"	=> ["id"],
							"transform" => function($p) {
								if (!empty($p["id"])) {
									global $modClass;
									//var_dump($modClass->getRelTags($p["id"])); exit;
									return $modClass->getRel('Relaccionados', $p["id"], 'id_producto_relaccionado', 'id_producto');
								}
								else return [];
							},
						],
						"save"		=> [
							"after"			=> true,
							"transform"		=> function($product_id) {
								if (!empty($product_id)) {
									$new_relaccionados = !empty($_POST["relaccionados"]) ? $_POST["relaccionados"] : [];
									sort($new_relaccionados);

									global $modClass;
									$current_relaccionados = $modClass->getRel('Relaccionados', $product_id,'id_producto_relaccionado', 'id_producto', ["id_producto_relaccionado"]);

									//var_dump($new_tags); exit;
									if ($current_relaccionados != $new_relaccionados) {
										$modClass->emptyRel('Relaccionados' ,'id_producto', $product_id);

										foreach ($new_relaccionados as $id_rel) {
											$modClass->setRel('Relaccionados', $id_rel, 'id_producto_relaccionado', $product_id,'id_producto');
										}

										return [
											"result"	=> 1,
											"message"	=> ["ok", _("Nuevos relaccionados insertados correctamente.")]
										];
									}
									else {
										return [
											"result"	=> 0,
											"message"	=> ["warning", _("No se han modificado los relaccionados ya que los datos enviados eran iguales a los existentes")]
										];
									}
								}
								else {
									return [
										"result"	=> -1,
										"message"	=> ["error", _("Se ha producido un error en el guardado de relaccionados.")]
									];
								}
							}
						]
					],

					/*
					[
						"id"		=> "productos-galeria",
						"type"		=> "array",
						"class"		=> "sortable-portfolio",
						"db"		=> "",
						"mod"		=> "productos",
						"act"		=> "files",
						"filter"	=> ['owner_type' => 'galeria'],
						"on_fields"	=> ['id' => 'owner_id'],
						"legend"	=> _("Fotos"),
						"childs"	=> [

							[
								"id"		=> "id",
								"type"		=> "hidden",
								"db"		=> "id",
								"readonly"	=> true,
							],

							[
								"id"		=> "id_producto",
								"type"		=> "hidden",
								"db"		=> "owner_id",
								"readonly"	=> true,
								"default"	=> $id,
							],
							[
								"id"		=> "owner_type",
								"type"		=> "hidden",
								"db"		=> "owner_type",
								"readonly"	=> true,
								"default"	=> 'galeria',
							],
							[
								"id"		=> "img",
								"type"		=> "input-image",
								"db"		=> "",
								"required"	=> true,
								"checks"	=> [],
								"div"		=> true,
								"div_class"	=> "input-image",
								"label"		=> _("Imagen"),
							],
							[
								"id"		=> "galery-title",
								"type"		=> "text",
								"db"		=> "title_1",
								"length"	=> 255,
								"div"		=> true,
								"label"		=> _("Título"),
								"div_class"	=> "cols two left",
							],
						],
						"two-cols"	=> true,
					],
					*/

					# SEO
					[
							"id"		=> "lang_es",
							"type"		=> "title",
							"db"		=> "",
							"options"	=> [
								"text" => "Datos SEO",
								"type" => "h4",
								"class" => "sectionTitle"
							],
							"label"		=> _(" "),
					],
					[
						"id"		=> "meta_titulo",
						"type"		=> "text",
						"db"		=> "meta_titulo",
						"required"	=> true,
						"length"	=> 250,
						"div"		=> true,
						"label"		=> _("Meta Título"),
						"class"		=> "char-counter",
						'placeholder' => 'requerido',
					],
					[
						"id"		=> "meta_descripcion",
						"type"		=> "textarea",
						"db"		=> "meta_descripcion",
						"required"	=> true,
						"length"	=> 250,
						"div"		=> true,
						"label"		=> _("Meta Descripción"),
						"class"		=> "char-counter",
						'placeholder' => 'requerido',
					],

					# OTROS DATOS
/*[
						"id"		=> "lang_es",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "OTROS DATOS",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						"id"		=> "created_at",
						"type"		=> "datetime-local",
						"db"		=> "created_at",
						"default"	=> date('Y-m-d h:i:s'),
						"checks"	=> ["datetime"],
						"div"		=> true,
						"label"		=> _("Fecha de creación"),
					],
					[
						"id"		=> "updated_at",
						"type"		=> "datetime-local",
						"db"		=> "updated_at",
						"default"	=> date('Y-m-d h:i:s'),
						"checks"	=> ["datetime"],
						"div"		=> true,
						"label"		=> _("Fecha de modificación"),
					],
					[
						"id"		=> "locked",
						"type"		=> "checkbox",
						"db"		=> "locked",
						"div"		=> true,
						"label"		=> _("Bloqueado"),
					],
*/
				],
				"hidden" => [],
			];

			$buttons = [
				//"new"	=> "Nuevo Post",
				"back"	=> _("Volver")
			];
		break;
		case 'Sort':
			$id = isset($_POST["id"]) ? $_POST["id"] : (isset($_GET["id"]) ? $_GET["id"] : "");
			$id = preg_replace("/[^\d]/", "", $id);

			$titulo = "Ordenar Trabajos";

			$options = [
				"id"	 => "nombre",
				"fields" => ["id_categoria", "nombre"],
				"shown"	 => function($data) {
					global $modClass, $arr_categories;

					$categoria = !empty($arr_categories[$data['id_categoria']])? $arr_categories[$data['id_categoria']]: 'Cat. No encontrada';
					$outData = str_pad($categoria, 100, " _", STR_PAD_RIGHT) . $data['nombre'];
					return str_replace('_', '&nbsp;', $outData);
				},
				"filter" => [
				],
				"sort"	 => [
					["position", "DESC"],
					["nombre", "ASC"],
				]
			];

			$buttons = [
				"back"	=> [
					"action" => strtolower("$mod/$act/list/"),
					"class"	 => "hidden",
					"hidden" => ["id"=> $id],
					"text" 	 => _("Volver")
				]
			];

		break;
		default: /// Opciones para el listado
			$titulo = "Listado de trabajos";

			$list = [
				"id" => "productos",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "nombre",
						"db"	=> "nombre",
						"title"	=> _("Nombre"),
					],
					[
						"id"	=> "es_privado",
						"db"	=> "es_privado",
						"title"	=> _('Privado'),
						"shown"		=> function($val) { return $val? '<i class="fa fa-lock" aria-hidden="true"></i> privado': '<i class="fa fa-unlock" aria-hidden="true"></i> público'; },
					],

					[
						"id"	=> "id_categoria",
						"db"	=> "id_categoria",
						"title"	=> _("Categoría"),
						"shown"		=> function($val) {
							global $arr_categories;
							return isset($arr_categories[$val])? $arr_categories[$val]: '';
						},
					],


					[
						"id"	=> "created_at",
						"db"	=> "created_at",
						"title"	=> _('<i class="fa fa-calendar" aria-hidden="true"></i> '),
						"shown"		=> function($val) {
							$creacion = new DateTime($val);
							return $creacion->format('d/m/Y');
						},
						"options" => [
							"printable"		=> "true",
						]
					],
					[
						"id"	=> "publicado",
						"db"	=> "publicado",
						"title"	=> _('<i class="fa fa-eye" aria-hidden="true"></i>'),
						"shown"		=> function($val) { return $val? '<i class="fa fa-check" aria-hidden="true"></i>': ''; },
					],

				],
				"filter" => [
				],
				"sort" => [
					["url", "ASC"],
				],
				"actions" => [
					/*
					"comments" => [
						"action"		=> "productos/comments/list/",
						"class" 		=> "fa fa-commenting-o",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Comentarios"),
					],

					*/

					"galeria" => [
						"action"		=> "productos/galeria/list/",
						"class" 		=> "fa fa-image",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Galer&iacute;a"),
						"hidden"	=> [  ]
					],
					/*
					"files" => [
						"action"		=> "productos/files/list/",
						"class" 		=> "fa fa-file-o",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Archivos"),
						"hidden"	=> [  ]
					],
					*/
					"editor" => [
						"form-bt"		=> _("Añadir trabajo"),
						"class" 		=> "fa fa-pencil",
						//"form-bt-class"	=> "hidden",
						"text"			=> _("Editar"),
						"hidden"	=> [ ]
					],
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					],
				],
				"multiple_selection" => true,
				"extra-buttons" => ""
			];

			$buttons = [
				"sort"	=> [
					"action"=> strtolower("$mod/$act/sort/"),
					"hidden"=> [],
					"text" 	=> _("Ordenar")
				],
			];
			//var_dump($
			//var_dump($arr_usuarios);

			if (empty($opt)) $opt = "List";
		break;
	}

	//echo __DIR__; exit;

	require "./_act/Base/Opt/$opt.php";
