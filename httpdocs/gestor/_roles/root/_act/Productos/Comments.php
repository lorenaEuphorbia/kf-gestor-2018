<?php
	$arrUserASOC = $b->users->getList('Users', ['id', 'username'], null, null, null, null, "FETCH_KEY_PAIR");
	$arrParent = $modClass->getList($mod, ['id', 'nombre'], null, null, null, null, "FETCH_KEY_PAIR");
	for ($i=0; $i <= 10; $i++) {
		$strCadena = '<p hidden>' . $i . '</p>';
		for ($j=0; $j < $i; $j++) { 
			$strCadena .= '<i class="fa fa-star" aria-hidden="true"></i>';
		}
		$arrPuntuacion[] = $strCadena;
	}

	# recogemos comentarios formateados
	$arrComents = array();
	foreach ($modClass->getList('Comments', ['id', 'email_usuario', 'titulo'], []) as $index => $row) {
		$arrComents[$row['id']] = $row['email_usuario'] . '  ' . $row['titulo'];
	}

	switch ($opt) {
		case 'Editor':


			$titulo = ["Nuevo Comentario" , "Editando Comentario "];

			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "text",
						"db"		=> "id",
						"div"		=> true,
						"readonly"	=> true,
						"label"		=> _("ID"),
						'placeholder' => 'autoincremento',
						'div_class'	=> '',
					],
					[
						"id"		=> "id_parent",
						"type"		=> "select-chosen",
						"db"		=> "id_parent",
						"default"	=> '0',
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Producto"),
						"options"	=> [
							"empty"	=> "",
							"values" => $arrParent
						],
						"class"		=> "chosen"
					],
					[
						"id"		=> "id_usuario",
						"type"		=> "select-chosen",
						"db"		=> "id_usuario",
						"default"	=> $b->security->userId,
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Usuario"),
						"options"	=> [
							"empty"	=> "",
							"values" => $arrUserASOC
						],
						"class"		=> "chosen"
					],
					[
						"id"		=> "nombre_usuario",
						"type"		=> "text",
						"db"		=> "nombre_usuario",
						"required"	=> true,
						"length"	=> 60,
						"readonly"	=> false,
						"div"		=> true,
						"label"		=> _("Nombre"),
						'placeholder' => 'requerido',
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'email_usuario',
						'type'		=> 'text',
						'db'		=> 'email_usuario',
						'label'		=> _('Email'),
						'placeholder' => 'Email',
						'readonly' 	=> false,
						'length'	=> 250,
						'div'		=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],

					# VENTA AL PÚBLICO
					[
						"id"		=> "lang_es",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "DATOS PUBLICOS",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						'id'		=> 'titulo',
						'type'		=> 'text',
						'db'		=> 'titulo',
						'label'		=> _('Título'),
						'placeholder' => 'Título',
						'readonly' 	=> false,
						'length'	=> 200,
						'div'		=> true,
						'class'		=> 'char-counter',
						'div_class' => '',
					],
					[
						"id"		=> "comentario",
						"type"		=> "ckeditor",
						"db"		=> "comentario",
						"div"		=> true,
						"label"		=> _("Comentario"),
					],
					[
						"id"		=> "puntuacion",
						"type"		=> "number",
						"db"		=> "puntuacion",
						"default"	=> "0",
						"div"		=> true,
						"label"		=> _("Puntuación"),
						"checks"	=> ['decimal'],
						"custom"	=> [
							'step' 	=> '1',
							'min'	=> '0',
							'max'	=> '5',
						],
						"placeholder"	=> "Puntuación",
						'div_class'	=> '',
					],
					
					# OTROS DATOS
					[
						"id"		=> "lang_es",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "OTROS DATOS",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						"id"		=> "position",
						"type"		=> "number",
						"db"		=> "position",
						"default"	=> "0",
						"div"		=> true,
						"label"		=> _("Posición"),
						"placeholder"	=> "Indica qué articulos se tienen que mostrar primero",
					],
					[
						"id"		=> "owner_id",
						"type"		=> "select-chosen",
						"db"		=> "owner_id",
						"default"	=> "0",
						"required"	=> false,
						"div"		=> true,
						"label"		=> _("Respuesta a"),
						"options"	=> [
							"empty"	=> "",
							"values" => $arrComents
						],
						"class"		=> "chosen"
					],
					[
						"id"		=> "created_at",
						"type"		=> "datetime-local",
						"db"		=> "created_at",
						"default"	=> date('Y-m-d h:i:s'),
						"checks"	=> ["datetime"],
						"div"		=> true,
						"label"		=> _("Fecha de creación"),
					],
					[
						"id"		=> "updated_at",
						"type"		=> "datetime-local",
						"db"		=> "updated_at",
						"default"	=> date('Y-m-d h:i:s'),
						"checks"	=> ["datetime"],
						"div"		=> true,
						"label"		=> _("Fecha de modificación"),
					],
					[
						"id"		=> "deleted_at",
						"type"		=> "datetime-local",
						"db"		=> "deleted_at",
						"default"	=> date('Y-m-d h:i:s'),
						"checks"	=> ["datetime"],
						"div"		=> true,
						"label"		=> _("Fecha de eliminación"),
					],

				],
				"hidden" => [],

			];

			$buttons = [
				"back"	=> _("Volver"),
				// "new"	=> "Nuevo Relaccionado",
			];
		break;
		default: /// Opciones para el listado


			$titulo = "Listado de Comentarios";

			$list = [
				"id" => "comments-list",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "id_parent",
						"db"	=> "id_parent",
						"title"	=> "ID parent",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "id_usuario",
						"db"	=> "id_usuario",
						"title"	=> _("ID usuario"),
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "nombre_usuario",
						"db"	=> "nombre_usuario",
						"title"	=> _("Nombre Usuario"),
					],
					[
						"id"	=> "email_usuario",
						"db"	=> "email_usuario",
						"title"	=> _("Email Usuario"),
					],
					[
						"id"	=> "titulo",
						"db"	=> "titulo",
						"title"	=> _("Título"),
					],
					[
						"id"	=> "puntuacion",
						"db"	=> "puntuacion",
						"title"	=> _('Puntuación'),
						"shown"		=> function($val) {
							global $arrPuntuacion;
							return $arrPuntuacion[$val];
						},
					],
					[
						"id"	=> "created_at",
						"db"	=> "created_at",
						"title"	=> _('<i class="fa fa-calendar" aria-hidden="true"></i> '),
						"shown"		=> function($val) { 
							$creacion = new DateTime($val);
							return $creacion->format('d/m/Y');
						},
						"options" => [
							"printable"		=> "true",
						]
					],
				],				
				"filter"=> $id? ['id_parent' => $id]: [],
				"sort" => [
					["created_at", "DESC"]
				],
				"actions" => [
					"editor" => [
						"form-bt"		=> _("Añadir Comentario"),
						"class" 		=> "fa fa-pencil",
						"text"			=> _("Editar"),
						"hidden"	=> [
							"parent-id"	=> $id,
						]
					],
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
						"hidden"	=> [
							"parent-id"	=> $id,
						]
					]
				],
				"multiple_selection" => true,
				"extra-buttons" => "", //"excel,csv,pdf,print"
			];

			$buttons = [
				"back"	=> [
					"action"=> strtolower("$mod/productos/list/"),
					"hidden"=> ["id"=> $id],
					"text" 	=> _("Volver"),
				]
			];

			if (empty($opt)) $opt = "List";
		break;
	}
	
	require "./_act/Base/Opt/$opt.php";