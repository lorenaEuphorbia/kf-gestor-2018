<?php

	$arrUserDataASOC = $modClass->getList('Userdata', ['id_user', 'id'], null, null, null, null, "FETCH_KEY_PAIR");
	$arrUserASOC = $modClass->getList('Users', ['id', 'username'], null, null, null, null, "FETCH_KEY_PAIR");
	$arrUserASOC['0'] = 'Web';

	switch ($opt) {
		case 'Editor':

			$titulo = ["Nuevo usuario", "Editando usuario"];
			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "text",
						"db"		=> "id",
						"div"		=> true,
						"default"	=> true,
						"readonly"	=> true,
						"label"		=> _("ID"),
						'placeholder' => 'autoincremento',
						'div_class'	=> '',
						'checks'	=> ['autoincrement']
					],
					[
						"id"		=> "username",
						"type"		=> "text",
						"db"		=> "username",
						"required"	=> true,
						"length"	=> 100,
						"div"		=> true,
						"label"		=> _("Usuario<br/>(Email de acceso)"),
						"checks"	=> ["email"],
						"unique"	=> true,
						"class"		=> "char-counter",
						'placeholder' => 'requerido',
					],
					[
						"id"		=> "creador",
						"type"		=> "select-chosen",
						"db"		=> "creador",
						"required"	=> false,
						"default"	=> $b->security->userId,
						"div"		=> true,
						"label"		=> _("Creador"),
						"options"	=> [
							"empty"	=> "",
							"values" => $arrUserASOC
						],
						"class"		=> "chosen"
					],
					[
						"id"		=> "roles",
						"type"		=> "tags",
						"db"		=> "roles",
						"div"		=> true,
						"label"		=> _("Roles"),
						"options"	=> [
							"values" => $modClass->getList("Userroles", ["id", "name"], null, ["level DESC"], null, null, "FETCH_NUM")
						],
						"shown"		=> [
							"required"	=> ["id"],
							"transform" => function($p) {
								if (!empty($p["id"])) {
									global $modClass;
									//var_dump($modClass->getRelTags($p["id"])); exit;
									return $modClass->getRel('UserRelRoles', $p["id"], 'role_id', 'user_id');
								}
								else return [];
							},
						],
						"save"		=> [
							"after"			=> true,
							"transform"		=> function($user_id) {
								if (!empty($user_id)) {
									$new_roles = !empty($_POST["roles"]) ? $_POST["roles"] : [];
									sort($new_roles);

									global $modClass;
									$current_roles = $modClass->getRel('UserRelRoles', $user_id, 'role_id', 'user_id');

									//var_dump($new_roles); exit;
									if ($current_roles != $new_roles) {
										$modClass->emptyRel('UserRelRoles', 'user_id', $user_id);

										foreach ($new_roles as $role_id) {
											$modClass->setRel('UserRelRoles', $user_id, 'user_id', $role_id, 'role_id');
										}

										return [
											"result"	=> 1,
											"message"	=> ["ok", _("Nuevos roles insertados correctamente.")]
										];
									}
									else {
										return [
											"result"	=> 0,
											"message"	=> ["warning", _("No se han modificado los roles ya que los datos enviados eran iguales a los existentes")]
										];
									}
								}
								else {
									return [
										"result"	=> -1,
										"message"	=> ["error", _("Se ha producido un error en el guardado de roles.")]
									];
								}
							}
						]
					],

					# OTROS DATOS
					[
						"id"		=> "lang_es",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "OTROS DATOS",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						"id"		=> "fecha_creacion",
						"type"		=> "datetime-local",
						"db"		=> "fecha_creacion",
						"placeholder" => "fecha del alta del usuario",
						"default"	=> date('Y-m-d h:i:s'),
						"checks"	=> ["datetime"],
						"div"		=> true,
						"label"		=> _("Fecha de creación"),
					],
					[
						"id"		=> "fecha_modificacion",
						"type"		=> "datetime-local",
						"db"		=> "fecha_modificacion",
						"placeholder" => "fecha de la última modificación",
						"default"	=> date('Y-m-d h:i:s'),
						"checks"	=> ["datetime"],
						"div"		=> true,
						"label"		=> _("Fecha de modificación"),
					],
					[
						"id"		=> "baja",
						"type"		=> "text",
						"db"		=> "baja",
						"placeholder" => "2017-12-31 00:00:00 (o null)",
						"div"		=> true,
						"label"		=> _("Fecha de baja"),
					],

					# SEGURIDAD
					[
						"id"		=> "lang_es",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "SEGURIDAD",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						"id"		=> "password",
						"type"		=> "password",
						"db"		=> "password",
						"label"		=> _("Contraseña"),
						"placeholder"		=> _("Rellenar sólo si se desea regenerar la contaseña del usuario"),
						"div"		=> true,
						"shown"		=> [
							"required"	=> ["id"],
							"transform" => function($p) {
								return '';
							},
						],
						"save"		=> [
							"after"			=> true,
							"transform"		=> function($id_user) {
								global $modClass;

								$strPassword = !empty($_POST['password'])? $_POST['password']: false;
								if (!empty($id_user) && $strPassword) {

									if (preg_match('/(?=.*?[a-zA-Z])(?=.*?[0-9]).{6,}/', $strPassword)) {
										if ($modClass->setPassword($id_user, $strPassword)) {

											return [
												"result"	=> 1,
												"message"	=> ["ok", _("Nuevo contraseña insertada correctamente.")]
											];
										}

										return [
											"result"	=> 0,
											"message"	=> ["warning", _("No se ha guardado la contraseña")]
										];
									}
									else {
										return [
											"result"	=> -1,
											"message"	=> ["error", _("La contraseña debe tener al menos 6 caracteres y contener como mínimo una letra y un número.\nRellena el campo teniendo esto en cuenta, por favor.")]
										];
									}

								}
								else {
									return [
										"result"	=> 0,
										"message"	=> ["warning", _("Contraseña o usuario no defeinido.")]
									];
								}
							}
						],
						"class"		=> "revealable",
					],
					[
						"id"		=> "password_gral",
						"type"		=> "text",
						"db"		=> "password",
						"required"	=> false,
						"readonly"	=> true,
						"length"	=> 128,
						"div"		=> true,
						"label"		=> _("Contraseña<br/>Codidificada en md5"),
						"class"		=> "char-counter",
						'placeholder' => 'requerido',
					],
					[
						"id"		=> "salt",
						"type"		=> "text",
						"db"		=> "salt",
						"required"	=> false,
						"readonly"	=> true,
						"length"	=> 128,
						"div"		=> true,
						"label"		=> _("Salt<br/>Codidificada en md5"),
						"class"		=> "char-counter",
						'placeholder' => 'requerido',
					],
					/* Genera un rol predeterminado
					[
						"id"		=> "role",
						"type"		=> "hidden",
						"db"		=> "role",
						"save"		=> [
							"after"			=> true,
							"transform"		=> function($id_user) {
								global $modClass;

								if (!empty($id_user)) {

									# generamos los datos
									if (!$modClass->search (['id_user'=>$id_user], 'Userdata')) {
										$modClass->setRecord(['id_user'=>$id_user,'email1'=>$_POST['username']], [], 'Userdata');
									}
									
									# generamos el rol
									if (!$modClass->hasRole($id_user, 'master') &&
										$modClass->setUserRole($id_user, 'master')) {

										return [
											"result"	=> 1,
											"message"	=> ["ok", _("Nuevo rol insertado correctamente.")]
										];
									}

									return [
										"result"	=> 0,
										"message"	=> ["warning", _("No se ha insertado el rol")]
									];
								}
								else {
									return [
										"result"	=> -1,
										"message"	=> ["error", _("Se ha producido un error al establecer el rol.")]
									];
								}
							}
						]
					],
					*/
				
				],
				"hidden" => []
			];

			$buttons = [
				//"new"	=> "Nuevo Cliente",
				"back"	=> _("Volver")
			];

		break;

		
		default: /// Opciones para el listado
			$titulo = "Listado de Usuarios";

			$list = [
				"id" => "users-list",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false",
						]
					],
					[
						"id"	=> "id_userdata",
						"db"	=> "id",
						"title"	=> _("ID Datos de usuario"),
						"shown"	=> function($val) {  
							global $modClass;
							$data = $modClass->getUserData($val);
							if(!empty($data['id'])) {
								return $data['id'];
							}
							return '';

						},
						"options" => [
							"printable"		=> "true",
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "true",
						]
					],
					/*
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> _("DNI/NIF"),
						"shown"	=> function($val) {  
							global $modClass;
							$data = $modClass->getUserData($val);
							if(!empty($data['dni'])) {
								return $data['dni'];
							}
							return '';

						},
						"options" => [
							"printable"		=> "true",
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "true",
						]
					],
					*/
					[
						"id"		=> "username",
						"db"		=> "username",
						"title"		=> _("Email de acceso (usuario)"),
						"unique"	=> "true"
					],
					[
						"id"		=> "creador",
						"db"		=> "creador",
						"title"		=> _("Creador"),
						"unique"	=> "true",
						"shown"	=> function($val) {  
							global $arrUserASOC;

							return !empty($arrUserASOC[$val])? $arrUserASOC[$val]: 'Web';

						},
					],
					[
						"id"	=> "fecha_creacion",
						"db"	=> "fecha_creacion",
						"title"	=> _('<i class="fa fa-calendar" aria-hidden="true"></i> Creación'),
						"shown"		=> function($val) { 
							$creacion = new DateTime($val);
							return $creacion->format('d/m/Y');
						},
						"options" => [
							"printable"		=> "true",
						]
					],
					[
						"id"	=> "baja",
						"db"	=> "baja",
						"title"	=> _('<i class="fa fa-calendar" aria-hidden="true"></i> Baja'),
						"shown"		=> function($val) { 

							if ($val) {
								$creacion = new DateTime($val);
								return $creacion->format('d/m/Y');								
							}
							return '';
						},
						"options" => [
							"printable"		=> "true",
						]
					],
					[
						"id"	=> "oculto",
						"db"	=> "oculto",
						"title"	=> _('<i class="fa fa-eye-slash" aria-hidden="true"></i>'),
						"shown"		=> function($val) { return $val? '<i class="fa fa-check" aria-hidden="true"></i>': ''; },
					],
				],
				"filter"=> [
					// "oculto" => '0',
				],
				"sort" => [
					["fecha_creacion", "ASC"]
				],
				"actions" => [
					"userdata" => [
						"action"		=> "users/userdata/editor/",
						"class" 		=> "fa fa-address-card",
						"form-bt-class"	=> "hidden ",
						"form-bt"		=> _("Datos"),
						"data"			=> [
							"foreign-key"	=> 2,
							"required-key"	=> 'true'
						],
						"text"			=> _("Ver/Editar Datos"),
					],		
					"editor" => [
						"form-bt"		=> _("Crear usuario"),
						"class" 		=> "fa fa-pencil",
						//"form-bt-class"	=> "hidden",
						"text"			=> _("Editar"),
						"hidden"	=> [ ]
					],			
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
					/**/
				],
				"multiple_selection" => true,
				"extra-buttons" => "copy,csv" //"excel,copy,csv,pdf,print"
			];

			if (empty($opt)) $opt = "List";
		break;
	}

	require "./_act/Base/Opt/$opt.php";