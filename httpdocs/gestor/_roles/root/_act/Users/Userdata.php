<?php
	
	$arrUserASOC = $modClass->getList('Users', ['id', 'username'], null, null, null, null, "FETCH_KEY_PAIR");
	$arrPaises = $b->portes->getList('Paises', ['id', 'nombre'], null, null, null, null, "FETCH_KEY_PAIR");
	$arrProvncias = $b->portes->getList('Provincias', ['id', 'nombre'], null, null, null, null, "FETCH_KEY_PAIR");
	//var_dump($id); exit;

	switch ($opt) {
		case 'Editor':

    
			$titulo = ["Nueva ficha de usuario", "Editando Ficha de usuario"];

			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "text",
						"db"		=> "id",
						"div"		=> true,
						"default"	=> true,
						"readonly"	=> true,
						"label"		=> _("ID"),
						'placeholder' => 'autoincremento',
						'div_class'	=> '',
						'checks'	=> ['autoincrement']
					],
					[
						"id"		=> "id_user",
						"type"		=> "select-chosen",
						"db"		=> "id_user",
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Usuario"),
						"options"	=> [
							"empty"	=> "",
							"values" => $arrUserASOC
						],
						"class"		=> "chosen"
					],
					[
						"id"		=> "nombre",
						"type"		=> "text",
						"db"		=> "nombre",
						"required"	=> true,
						"length"	=> 60,
						"readonly"	=> false,
						"div"		=> true,
						"label"		=> _("Nombre"),
						'placeholder' => 'requerido',
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						"id"		=> "apellidos",
						"type"		=> "text",
						"db"		=> "apellidos",
						"required"	=> true,
						"length"	=> 250,
						"readonly"	=> false,
						"div"		=> true,
						"label"		=> _("Apellidos"),
						'placeholder' => 'requerido',
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						"id"		=> "razon",
						"type"		=> "text",
						"db"		=> "razon",
						"required"	=> true,
						"length"	=> 180,
						"readonly"	=> false,
						"div"		=> true,
						"label"		=> _("Razón social"),
						'placeholder' => 'requerido',
						'class'		=> 'char-counter',
					],
					[
						"id"		=> "dni",
						"type"		=> "text",
						"db"		=> "dni",
						"required"	=> true,
						"length"	=> 200,
						"readonly"	=> false,
						"div"		=> true,
						"label"		=> _("DNI/CIF<br />Nº de pasaporte"),
						'placeholder' => 'requerido',
						'class'		=> 'char-counter',
						'div_class' => '',
					],

					// DATOS FACTURACIÓN
					[
						"id"		=> "datos_ficha",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "INFORMACIÓN DE CONTACTO",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						'id'		=> 'direccion',
						'type'		=> 'text',
						'db'		=> 'direccion',
						'label'		=> _('Dirección'),
						'placeholder' => 'Dirección',
						'readonly' 	=> false,
						'length'	=> 250,
						'div'		=> true,
						'class'		=> 'char-counter',
						'div_class' => '',
					],
					[
						'id'		=> 'id_pais',
						'type'		=> 'select',
						'db'		=> 'id_pais',
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Pais"),
						"options"	=> [
							"empty"	=> "",
							"values" => $arrPaises
						],
						"class"		=> "chosen",
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'id_provincia',
						'type'		=> 'select',
						'db'		=> 'id_provincia',
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Provincia"),
						"options"	=> [
							"empty"	=> "",
							"values" => $arrProvncias
						],
						"class"		=> "chosen",
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'poblacion',
						'type'		=> 'text',
						'db'		=> 'poblacion',
						'label'		=> _('Población'),
						'placeholder' => 'Población',
						'readonly' 	=> false,
						'length'	=> 120,
						'div'		=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'codigo_postal',
						'type'		=> 'text',
						'db'		=> 'codigo_postal',
						'label'		=> _('Código Postal'),
						'placeholder' => 'Código Postal',
						'readonly' 	=> false,
						'length'	=> 20,
						'div'		=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'email',
						'type'		=> 'text',
						'db'		=> 'email',
						'label'		=> _('Email'),
						'placeholder' => 'Email',
						'readonly' 	=> false,
						'length'	=> 250,
						'div'		=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'telefono',
						'type'		=> 'text',
						'db'		=> 'telefono',
						'label'		=> _('Teléfono'),
						'placeholder' => 'Teléfono',
						'readonly' 	=> false,
						'length'	=> 120,
						'div'		=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
				],
				"hidden" => [],
			];

			$buttons = [
				//"new"	=> "Nuevo Cliente",
				"back"	=> _("Volver")
			];

			break;

		default: /// Opciones para el listado
			$titulo = "Listado de Usuarios";

			$list = [
				"id" => "users-list",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false",
						]
					],
					[
						"id"	=> "id_user",
						"db"	=> "id_user",
						"title"	=> "ID usuario",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "true",
						]
					],
					[
						"id"		=> "dni",
						"db"		=> "dni",
						"title"		=> _("DNI"),
						"unique"	=> "true"
					],
					[
						"id"		=> "nombre",
						"db"		=> "nombre",
						"title"		=> _("Nombre "),
						"unique"	=> "true"
					],
					[
						"id"		=> "codigo_postal",
						"db"		=> "codigo_postal",
						"title"		=> _("Código Postal"),
						"unique"	=> "true"
					],
				],
				"filter"=> [
					// "oculto" => '0',
				],
				"sort" => [
					// ["fecha_creacion", "ASC"]
				],
				"actions" => [
					"users" => [
						"action"		=> "users/users/editor/",
						"class" 		=> "fa fa-address-card",
						"form-bt-class"	=> "hidden ",
						"form-bt"		=> _("Usuario"),
						"data"			=> [
							"foreign-key"	=> 2,
							"required-key"	=> 'true'
						],
						"text"			=> _("Ver/Editar Datos"),
					],
					"editor" => [
						"form-bt"		=> _("Nuevos datos de usuario"),
						"class" 		=> "fa fa-pencil",
						//"form-bt-class"	=> "hidden",
						"text"			=> _("Editar"),
						"hidden"	=> [ ]
					],
					
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
					/**/
				],
				"multiple_selection" => true,
				"extra-buttons" => "copy,csv" //"excel,copy,csv,pdf,print"
			];

			if (empty($opt)) $opt = "List";
		break;
	}

	require "./_act/Base/Opt/$opt.php";