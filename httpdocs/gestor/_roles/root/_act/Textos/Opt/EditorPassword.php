<?php

/// Iniciamos variables necesarias
$messages = [];

/// Guardado
if (isset($_POST['SaveData']) && !empty($fields)) {
	//echo "<pre>"; print_r($_POST); exit;
	
	$arr_fields = [];
	$arr_errors = [];
	$post_saving_fields = [];
	$post_saving_oks = 0;

	$password_old = isset($_POST['password-old'])? $_POST['password-old']: false;
	$password_new = isset($_POST['password-new'])? $_POST['password-new']: false;
	$password_retype = isset($_POST['password-retype'])? $_POST['password-retype']: false;

	//var_dump($_POST);

	if ($password_old && $password_new && $password_retype) {

		$result = $b->users->getList('Users', null, ["id"=>$b->security->userId,"baja"=>["IS","null"]]);
		if (count($result) === 1) {
			$user_data = $result[0];

			$password_old = hash('sha512', $password_old . $user_data['salt']);
			if (!$b->security->checkbrute($user_data['id'])) {
				$password_old = hash('sha512', $password_old . $user_data['salt']);

				if ($password_old == $user_data['password']) {
					if (preg_match('/(?=.*?[a-zA-Z])(?=.*?[0-9]).{6,}/', $password_new)) {
						if ($password_new === $password_retype) {

							//Utilities::dump($b->users->setPassword($b->security->userId,$password_new));
							if ($b->users->setPassword($b->security->userId,$password_new)) {
								 $b->security->login($user_data['username'], $password_new);
								 $post_saving_oks = 1;
							} else {
								$arr_errors[] = sprintf(_("Error de guardado, contacte con el administrador. Por favor. "));
							}
						}
						else {
							$arr_errors[] = sprintf(_("Las contraseñas no coinciden. "));
						}
					}
					else {
						$arr_errors[] = sprintf(_("La nueva contraseña debe tener al menos 6 caracteres y contener como mínimo una letra y un número. "));
					}
				}
				else {
					$arr_errors[] = sprintf(_("La contraseña Antigua no es correcta. "));
				}
			}
			else {
				$arr_errors[] = sprintf(_("Error, la cuenta no está disponible. "));
			}
		}
		else {
			$arr_errors[] = sprintf(_("Error, no hay un token disponible. "));
		}
	}
	else {
		$arr_errors[] = sprintf(_("Debes introduccir todos los campos %s. "));
	}

	if (empty($arr_errors)) {
		$oks = 0;
		$warnings = 0;
		$errors = 0;

		if (!$errors) {
			if ($oks || $post_saving_oks) {
				$messages[] = ["ok", _("Los datos se han modificado correctamente.")];
			}
			else {
				$messages[] = ["warning", _("No se ha realizado ninguna modificación ya que los datos enviados eran iguales a los existentes.")];
			}
		}
	}
	else {
		$tmp = "";
		foreach ($arr_errors as $error) {
			$tmp .= "<li>" . $error . "</li>";
		}

		$messages[] = ["error", "Corrija los siguientes errores, por favor:<ul>$tmp</ul>"];
	}
}

/// Datos para la vista
$arr_fields = [];

if (!empty($fields)) {
	foreach ($fields as $field) {
		if ($field["type"] == "input-image") {
			if ($modClass->files->searchByOwner(1, $field["id"])) {
				$field["val"] = [
					$modClass->files->getFileData("id"),
					$modClass->files->getModuleName(),
					$modClass->files->getFileData("type"),
					$modClass->files->getFilePath()
				];
				//var_dump($field["val"]); exit;
			}
		}
		else if (!empty($field["db"])) $field["val"] = $modClass->getValue($field["db"]);
		if(isset($field["two-cols"]) && $field["two-cols"])
			$arr_fields[] = "<div class=\"two cols\">" . DomElements::getField($field) . '</div>';
		else
			$arr_fields[] = DomElements::getField($field);
	}
}

/// Botones del pie
if (!empty($buttons)) {
	$form_buttons = [
		"id"	=> "form-buttons",
		"class"	=> "form-buttons",
		"forms"	=> []
	];

	foreach($buttons as $bt_id => $bt_text) {
		switch ($bt_id) {
			case "new":
				if ($id != "") {
					$form_buttons["forms"][] = [
						"id"		=> $bt_id,
						"class"		=> "inline",
						"button"	=> $bt_text
					];
				}
			break;
			case "back":
				$form_buttons["forms"][] = [
					"id"		=> $bt_id,
					"action"	=> $base_path . strtolower("$mod/$act/list/"),
					"class"		=> "inline",
					"button"	=> $bt_text
				];
			break;
		}
	}
}

$bottom_buttons = !empty($form_buttons) ? DomElements::getFormsGroup($form_buttons) : "";

/// Archivos css y javascript
if (!empty($fields)) {
	$types = array_column($fields, "type");

	if (in_array("ckeditor", $types)) {
		AdminPreferences::addCssJs($js, $base_path."js/vendors/ckeditor/ckeditor.js");
		AdminPreferences::addCssJs($js, $base_path."js/vendors/ckeditor/adapters/jquery.js");
	}

	if (in_array("date", $types) || in_array("datetime-local", $types)) {
		AdminPreferences::addCssJs($css, $base_path."js/vendors/datetimepicker/jquery.datetimepicker.css");
		AdminPreferences::addCssJs($js, $base_path."js/vendors/datetimepicker/jquery.datetimepicker.js");
	}

	if (in_array("tags", $types) || in_array("select-chosen", $types)) {
		AdminPreferences::addCssJs($js, $base_path."js/vendors/chosen/chosen.jquery.min.js");
	}
}

require "./_inc/theme/editor.php";