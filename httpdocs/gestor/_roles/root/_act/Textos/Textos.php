<?php

	$act = 'preferences';
	$mod = 'preferences';

	switch ($opt) {
		case 'EditorSave':
		case 'Editor':
		default:
			$titulo = ["", "EDITANDO TEXTOS"];

			$parent_id = isset($_POST["parent-id"]) ? $_POST["parent-id"] : (isset($_GET["parent-id"]) ? $_GET["parent-id"] : "");
			$id = isset($_POST["id"]) ? $_POST["id"] : (isset($_GET["id"]) ? $_GET["id"] : "");

			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "hidden",
						"db"		=> "id",
						//"div"		=> true,
						"readonly"	=> true,
						//"label"		=> _("ID"),
						'placeholder' => 'autoincremento',
						'div_class'	=> '',
					],
					[
						"id"		=> "img",
						"type"		=> "input-image",
						"db"		=> "",
						"required"	=> true,
						"checks"	=> [],
						"div"		=> true,
						"div_class"	=> "input-image",
						"label"		=> _("Imagen"),
					],
					[
						"id"		=> "text_1",
						"type"		=> "ckeditor",
						"db"		=> "text_1",
						"required"	=> true,
						"checks"	=> [],
						"div"		=> true,
						"div_class"	=> "ckeditor",
						"label"		=> _("Contenido"),
					],
					[
						"id"		=> "updated_at",
						"type"		=> "hidden",
						"db"		=> "updated_at",
						"default"	=> date('Y-m-d h:i:s'),
						"set"	=> date('Y-m-d h:i:s'),
					],

					# ESPAÑOL
				],
				"hidden" => []
			];

			$buttons = [
				//"new"	=> "Nuevo Post",
				"back"	=> _("Volver")
			];

			if (empty($opt)) $opt = "Editor";
		break;
	}

	//echo __DIR__; exit;

	require "./_act/Base/Opt/$opt.php";
