<?php

	$arr_categories = [];
	$tmp_categories = $modClass->getList("Catalbaranes", ["id", "nombre"], null, ["nombre"]);
	foreach ($tmp_categories as $cat) $arr_categories[$cat["id"]] = $cat["nombre"];

	$id = isset($_REQUEST['id']) && !empty($_REQUEST['id']) ? preg_replace("/[^\d]/", "", $_REQUEST["id"]) : false;
	$parent_id = isset($_REQUEST['parent-id']) && !empty($_REQUEST['parent-id']) ? preg_replace("/[^\d]/", "", $_REQUEST["parent-id"]) : false;

	switch ($opt) {
		case 'Editor':

			$titulo = ["Nuevo Albarán ", "Editando Albarán"];

			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "hidden",
						"db"		=> "id",
						"readonly"	=> true,
					],
					[
						"id"		=> "id_usuario",
						"type"		=> "hidden",
						"db"		=> "id_usuario",
						"default"	=> $parent_id,
					],
					[
						"id"		=> "creacion",
						"type"		=> "text",
						"db"		=> "creacion",
						"readonly"	=> true,
						"div"		=> true,
						"label"		=> _("Fecha Creación"),
					],
					[
						"id"		=> "categoria",
						"type"		=> "select-chosen",
						"db"		=> "id_categoria",
						"div"		=> true,
						"required"	=> true,
						"label"		=> _("Categoría"),
						"options"	=> [
							"empty"	=> "",
							"values" => $arr_categories
						],
						"class"		=> "chosen"
					],
					[
						"id"		=> "descripcion",
						"type"		=> "textarea",
						"db"		=> "descripcion",
						"lenght"	=> 1000,
						"div"		=> true,
						"label"		=> _("Descipción"),
					],
					[
						"id"		=> "albaran",
						"type"		=> "input-file",
						"db"		=> "",
						"required"	=> true,
						"checks"	=> [],
						"div"		=> true,
						"div_class"	=> "input-file",
						"label"		=> _("Archivo"),
					],
				],
				"hidden" => ["parent-id" => $parent_id],
			];

			$buttons = [
				//"new"	=> "Nuevo Post",
				"back"	=> _("Volver")
			];
		break;
		default: /// Opciones para el listado
			$titulo = "Listado de albaranes";

			$list = [
				"id" => "post",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "creacion",
						"db"	=> "creacion",
						"title"	=> _("Fecha"),
					],
					[
						"id"	=> "descripcion",
						"db"	=> "descripcion",
						"title"	=> _("Descripción"),
					],
				],
				"filter" => [
					"id_usuario" => "$id",
				],
				"sort" => [
					["creacion", "DESC"],
				],
				"actions" => [
					"editor" => [
						"form-bt"		=> _("Nuevo albarán"),
						"class" 		=> "fa fa-pencil",
						//"form-bt-class"	=> "hidden",
						"text"			=> _("Editar"),
						"hidden"	=> [
							"parent-id"	=> $id,
						]
					],
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			//var_dump($arr_usuarios);

			if (empty($opt)) $opt = "List";
		break;
	}

	//echo __DIR__; exit;
	
	require "./_act/Base/Opt/$opt.php";