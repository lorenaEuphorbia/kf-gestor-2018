<?php

	$id = isset($_REQUEST['id']) && !empty($_REQUEST['id']) ? preg_replace("/[^\d]/", "", $_REQUEST["id"]) : false;
	$parent_id = isset($_REQUEST['parent-id']) && !empty($_REQUEST['parent-id']) ? preg_replace("/[^\d]/", "", $_REQUEST["parent-id"]) : false;

	switch ($opt) {
		case 'Editor':

			$titulo = ["Nuevo Contrato ", "Editando Contrato"];

			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "hidden",
						"db"		=> "id",
						"readonly"	=> true,
					],
					[
						"id"		=> "id_usuario",
						"type"		=> "hidden",
						"db"		=> "id_usuario",
						"default"	=> $parent_id,
					],
					[
						"id"		=> "creacion",
						"type"		=> "text",
						"db"		=> "creacion",
						"readonly"	=> true,
						"div"		=> true,
						"label"		=> _("Fecha Creación"),
					],
					[
						"id"		=> "descripcion",
						"type"		=> "textarea",
						"db"		=> "descripcion",
						"lenght"	=> 1000,
						"div"		=> true,
						"label"		=> _("Descipción"),
					],
					[
						"id"		=> "contrato",
						"type"		=> "input-file",
						"db"		=> "",
						"required"	=> true,
						"checks"	=> [],
						"div"		=> true,
						"div_class"	=> "input-file",
						"label"		=> _("Archivo"),
					],
				],
				"hidden" => ["parent-id" => $parent_id],
			];

			$buttons = [
				//"new"	=> "Nuevo Post",
				"back"	=> _("Volver")
			];
		break;
		default: /// Opciones para el listado
			$titulo = "Listado de Contratos";

			$list = [
				"id" => "post",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "creacion",
						"db"	=> "creacion",
						"title"	=> _("Fecha"),
					],
					[
						"id"	=> "descripcion",
						"db"	=> "descripcion",
						"title"	=> _("Descripción"),
					],
				],
				"filter" => [
					"id_usuario" => "$id",
				],
				"sort" => [
					["creacion", "DESC"],
				],
				"actions" => [
					"editor" => [
						"form-bt"		=> _("Nuevo Contrato"),
						"class" 		=> "fa fa-pencil",
						//"form-bt-class"	=> "hidden",
						"text"			=> _("Editar"),
						"hidden"	=> [
							"parent-id"	=> $id,
						]
					],
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			//var_dump($arr_usuarios);

			if (empty($opt)) $opt = "List";
		break;
	}

	//echo __DIR__; exit;
	
	require "./_act/Base/Opt/$opt.php";