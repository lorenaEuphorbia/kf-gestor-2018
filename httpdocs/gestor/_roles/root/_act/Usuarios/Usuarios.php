<?php

	switch ($opt) {
		case 'Editor':
			$titulo = ["Nuevo usuario", "Editando usuario"];

			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "hidden",
						"db"		=> "id",
						"readonly"	=> true,
					],
					[
						"id"		=> "nombre",
						"type"		=> "text",
						"db"		=> "nombre",
						"length"	=> 255,
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Nombre"),
					],
					[
						"id"		=> "apellidos",
						"type"		=> "text",
						"db"		=> "apellidos",
						"length"	=> 255,
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Apellidos"),
					],
					[
						"id"		=> "email",
						"type"		=> "text",
						"db"		=> "email",
						"length"	=> 255,
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Email"),
					],
					[
						"id"		=> "direccion",
						"type"		=> "text",
						"db"		=> "direccion",
						"length"	=> 255,
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Dirección"),
					],
					/*
					[
						"id"		=> "newsletter",
						"type"		=> "checkbox",
						"db"		=> "newsletter",
						"div"		=> true,
						"label"		=> _("Newsletter"),
					],
					*/
					[
						"id"		=> "password",
						"type"		=> "text",
						"db"		=> "password",
						"length"	=> 32,
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Contraseña"),
					],
				],
				"hidden" => []
			];

			$buttons = [
				//"new"	=> "Nuevo Post",
				"back"	=> _("Volver")
			];
		break;
		default: /// Opciones para el listado
			$titulo = "Listado de usuarios";

			$list = [
				"id" => "post",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "nombre",
						"db"	=> "nombre",
						"title"	=> _("Nombre"),
					],
					[
						"id"	=> "apellidos",
						"db"	=> "apellidos",
						"title"	=> _("Apellidos"),
					],
					[
						"id"	=> "email",
						"db"	=> "email",
						"title"	=> _("Email"),
					],
				],
				"sort" => [
					["email", "ASC"],
				],
				"actions" => [
					"consultas" => [
						"action"		=> "usuarios/consultas/list",
						"class" 		=> "fa fa-comments",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Consultas"),
					],
					"albaranes" => [
						"action"		=> "usuarios/albaranes/list",
						"class" 		=> "fa fa-file-text-o",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Albaranes"),
					],
					"contratos" => [
						"action"		=> "usuarios/contratos/list",
						"class" 		=> "fa fa-exchange",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Contratos"),
					],
					"editor" => [
						"form-bt"		=> _("Nuevo usuario"),
						"class" 		=> "fa fa-pencil",
						"text"			=> _("Editar"),
					],
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			if (empty($opt)) $opt = "List";
		break;
	}

	//echo __DIR__; exit;
	
	require "./_act/Base/Opt/$opt.php";