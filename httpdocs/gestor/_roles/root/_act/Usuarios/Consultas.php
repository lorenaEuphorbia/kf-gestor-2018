<?php

	$id = isset($_REQUEST['id']) && !empty($_REQUEST['id']) ? preg_replace("/[^\d]/", "", $_REQUEST["id"]) : false;
	$parent_id = isset($_REQUEST['parent-id']) && !empty($_REQUEST['parent-id']) ? preg_replace("/[^\d]/", "", $_REQUEST["parent-id"]) : false;
	$arr_usuarios = [];
	foreach ($modClass->getList("Usuarios", ["id","nombre","apellidos","email"], null, ["email"]) as $dataRow) {
		$arr_usuarios[$dataRow['id']] = $dataRow['nombre'] . " " . $dataRow['apellidos'] . " , " . $dataRow['email'];
	}

	switch ($opt) {
		case 'Editor':

			$titulo = ["Nueva Consulta ", "Editando consulta"];

			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "hidden",
						"db"		=> "id",
						"readonly"	=> true,
					],
					[
						"id"		=> "asunto",
						"type"		=> "text",
						"db"		=> "asunto",
						"length"	=> 255,
						"readonly"	=> true,
						"div"		=> true,
						"label"		=> _("Asunto"),
					],
					[
						"id"		=> "mensaje_entrada",
						"type"		=> "textarea",
						"db"		=> "mensaje_entrada",
						"length"	=> 1000,
						"readonly"	=> true,
						"div"		=> true,
						"label"		=> _("Mensaje"),
					],
					[
						"id"		=> "mensaje_salida",
						"type"		=> "textarea",
						"db"		=> "mensaje_salida",
						"length"	=> 1000,
						"div"		=> true,
						"label"		=> _("Respuesta"),
					],
					[
						"id"		=> "fecha_entrada",
						"type"		=> "text",
						"db"		=> "fecha_entrada",
						"readonly"	=> true,
						"div"		=> true,
						"label"		=> _("Fecha Mensaje"),
					],
					[
						"id"		=> "fecha_salida",
						"type"		=> "text",
						"db"		=> "fecha_salida",
						"set"		=>  function () {
							//echo "<pre>"; var_dump($_REQUEST); exit;
									return 	isset($_REQUEST['mensaje_salida']) && !empty($_REQUEST['mensaje_salida']) && 
											(!isset($_REQUEST['fecha_salida']) || empty($_REQUEST['fecha_salida']))
											? date('Y-m-d H:i:s') : false;
						},
						"readonly"	=> true,
						"div"		=> true,
						"label"		=> _("Fecha Respuesta"),
					],
					[
						"id"		=> "respondido",
						"type"		=> "hidden",
						"db"		=> "respondido",
						"set"		=>  function () {
								return 	isset($_REQUEST['mensaje_salida']) && !empty($_REQUEST['mensaje_salida']) && 
										(!isset($_REQUEST['respondido']) || empty($_REQUEST['respondido']))
										? "1" : false;
						},
						"readonly"	=> true,
					],
				],
				"hidden" => ["parent-id" => $parent_id],
			];

			$buttons = [
				//"new"	=> "Nuevo Post",
				"back"	=> _("Volver")
			];

			if (isset($_POST['SaveData']))
				$modClass->triggerConsultas();

		break;
		default: /// Opciones para el listado
			$titulo = "Listado de consultas";

			$list = [
				"id" => "post",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "asunto",
						"db"	=> "asunto",
						"title"	=> _("Asunto"),
					],
				],
				"filter" => [
					"id_usuario" => "$id",
				],
				"sort" => [
					["fecha_entrada", "DESC"],
				],
				"actions" => [
					"editor" => [
						"form-bt"		=> _("Nueva consulta"),
						"class" 		=> "fa fa-pencil",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Editar"),
						"hidden"	=> [
							"parent-id"	=> $id,
						]
					],
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			if($id) {
			 	$list['filter'] = ["id_usuario" => "$id"];
				$list['fields'][] = 
					[
						"id"	=> "respondido",
						"db"	=> "respondido",
						"title"	=> _("Respondido"),
						"shown"	=> function($val) { return $val? "SI" : "NO"; },
					];
			}
			else {
				$list['filter'] = ["respondido" => "0"];
				$list['fields'][] = 
					[
						"id"	=> "usuario",
						"db"	=> "id_usuario",
						"title"	=> _("Usuario"),
						"shown"	=> function($val) { global $arr_usuarios; return isset($arr_usuarios[$val]) ? $arr_usuarios[$val]: ""; },
					];
			}

			//var_dump($arr_usuarios);

			if (empty($opt)) $opt = "List";
		break;
	}

	//echo __DIR__; exit;
	
	require "./_act/Base/Opt/$opt.php";