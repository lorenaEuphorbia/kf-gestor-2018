<?php

	switch ($opt) {
		default: /// Opciones para el listado
			$titulo = "Listado de Paises";

			$list = [
				"id" => "paises-list",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"		=> "locale",
						"db"		=> "locale",
						"title"		=> _("Código local de País"),
					],
					[
						"id"		=> "code",
						"db"		=> "code",
						"title"		=> _("Código del Pais"),
					],
					[
						"id"		=> "nombre",
						"db"		=> "nombre",
						"title"		=> _("Nombre del Pais<br />en Español"),
					],
					[
						"id"		=> "nombre_idioma",
						"db"		=> "nombre_idioma",
						"title"		=> _("Nombre del Pais<br />en el propio idioma"),
					],
					[
						"id"		=> "activo",
						"db"		=> "activo",
						"title"		=> _("Activo"),
					],
					[
						"id"		=> "envio",
						"db"		=> "envio",
						"title"		=> _("Envío"),
					],
				],
				"filter" => [
				],
				"sort" => [
				],
				"actions" => [
					"provincias" => [
						"action"		=> "portes/provincias/list/",
						"form-bt-class"	=> "hidden",
						"class" 		=> "fa fa-list",
						"text"			=> _("Provincias"),
					],
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			if (empty($opt)) $opt = "List";
		break;
	}
	
	require "./_act/Base/Opt/$opt.php";