<?php

	switch ($opt) {
		default: /// Opciones para el listado
			$id = isset($_REQUEST["id"]) ? preg_replace("/[^\d]/", "", $_REQUEST["id"]) : "";
			$codPais = $modClass->search(['ROW_ID'=>$id,'GRP_ID'=>'SIS'], 'Paises')? $modClass->getCurrentFieldValue('CodPais', 'Paises') : 0;

			$titulo = "Listado de Transportistas";

			$list = [
				"id" => "transportista-list",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "ROW_ID",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"		=> "Codigo",
						"db"		=> "Codigo",
						"title"		=> _("Código de Transportista"),
					],
					[
						"id"		=> "Nombre",
						"db"		=> "Nombre",
						"title"		=> _("Nombre de Transportista"),
					],
				],
				"filter" => [
					"GRP_ID" => GRP_ID
				],
				"sort" => [
				],
				"actions" => [],
				"multiple_selection" => false,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			if (empty($opt)) $opt = "List";
		break;
	}
	
	require "./_act/Base/Opt/$opt.php";