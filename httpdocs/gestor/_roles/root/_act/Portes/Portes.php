<?php

	$arrPaises = $modClass->getPaisesnombres ();
	$arrProvincias = $modClass->getProvinciasnombres (1);

	switch ($opt) {
		case 'Editor':

			$titulo = ["Nuevo porte", "Editando porte"];
			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "hidden",
						"db"		=> "id",
						"readonly"	=> true,
					],
					[
						"id"		=> "id_pais",
						"type"		=> "select-chosen",
						"db"		=> "id_pais",
						"label"		=> _("Pais"),
						"required"	=> true,
						"div"		=> true,
						"options"	=> [
							"empty"	=> "",
							"values" => $arrPaises
						],
						"class"		=> "chosen"
					],
					[
						"id"		=> "id_provincia",
						"type"		=> "select-chosen",
						"db"		=> "id_provincia",
						"label"		=> _("Provincia"),
						"required"	=> true,
						"div"		=> true,
						"options"	=> [
							"empty"	=> "",
							"values" => $arrProvincias
						],
						"class"		=> "chosen"
					],
					[
						"id"		=> "id_transportista",
						"type"		=> "text",
						"db"		=> "id_transportista",
						"label"		=> _("Transportista"),
						"div" 		=> true,
						//"readonly" 	=> true,
						//"length"	=> 5,
						//"shown"		=> [
						//	"required" => ["id_transportista"],
						//	"transform" => function ($params) {
						//		global $modClass;
						//		return $modClass->getTransportistaNombre($params['id_transportista']);		
						//	},
						//]
					],
					[
						"id"		=> "pvp",
						"type"		=> "number",
						"db"		=> "pvp",
						"label"		=> _("Precio"),
						"placeholder"	=> "Precio de los portes",
						"div" 		=> true,
						"default"	=> "0",
						"checks"	=> ['decimal'],
						"custom"	=> [
							'step' 	=> '0.01',
							'min'	=> '0',
						],
					],
					[
						"id"		=> "is_rebaja",
						"type"		=> "checkbox",
						"db"		=> "is_rebaja",
						"label"		=> _("Portes Rebaja"),
						"div" 		=> true,
					],
					[
						"id"		=> "pvp_rebaja",
						"type"		=> "number",
						"db"		=> "pvp_rebaja",
						"label"		=> _("Precio Portes Rebaja"),
						"placeholder"	=> "Precio del porte rebajado",
						"div" 		=> true,
						"default"	=> "0",
						"checks"	=> ['decimal'],
						"custom"	=> [
							'step' 	=> '0.01',
							'min'	=> '0',
						],
					],
					[
						"id"		=> "limite_rebaja",
						"type"		=> "number",
						"db"		=> "limite_rebaja",
						"label"		=> _("Límite Rebaja"),
						"placeholder"	=> "Precio a partir del cuál se aplica el 'Precio Portes Rebaja'",
						"div" 		=> true,
						"default"	=> "0",
						"checks"	=> ['decimal'],
						"custom"	=> [
							'step' 	=> '0.01',
							'min'	=> '0',
						],
					],
					[
						"id"		=> "pvp_devolver",
						"type"		=> "number",
						"db"		=> "pvp_devolver",
						"label"		=> _("Precio Devolución"),
						"placeholder"	=> "Precio de la devolución",
						"div" 		=> true,
						"default"	=> "0",
						"checks"	=> ['decimal'],
						"custom"	=> [
							'step' 	=> '0.01',
							'min'	=> '0',
						],
					],
					[
						"id"		=> "dias_entrega",
						"type"		=> "number",
						"db"		=> "dias_entrega",
						"label"		=> _("Días de entrega"),
						"div" 		=> true,
						"default"	=> "1",
						"checks"	=> ['decimal'],
						"custom"	=> [
							'step' 	=> '1',
							'min'	=> '1',
						],
					],
					[
						"id"		=> "activo",
						"type"		=> "checkbox",
						"db"		=> "activo",
						"label"		=> _("Activo"),
						"div" 		=> true,
					],
					[
						"id"		=> "envio",
						"type"		=> "checkbox",
						"db"		=> "envio",
						"label"		=> _("Envío"),
						"div" 		=> true,
					],
				],
				"hidden" => []
			];


			$buttons = [
				//"new"	=> "Nuevo Cliente",
				"back"	=> _("Volver")
			];

		break;

		
		default: /// Opciones para el listado
			$titulo = "Listado de Portes";
			$arrPaises = $modClass->getPaisesNombres ();

			$list = [
				"id" => "portes-list",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					/*
					[
						"id"		=> "id_pais",
						"db"		=> "id_pais",
						"title"		=> _("Código Pais"),
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"		=> "id_provincia",
						"db"		=> "id_provincia",
						"title"		=> _("Código Provincia"),
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					*/
					[
						"id"		=> "id_pais",
						"db"		=> "id_pais",
						"title"		=> _("Pais"),
						"shown"		=> function($val) { 
							global $arrPaises;
							return isset($arrPaises[$val])? $arrPaises[$val]:'' ;
						},
					],
					[
						"id"		=> "id_provincia",
						"db"		=> "id_provincia",
						"title"		=> _("Provincia"),
						"shown"		=> 
						function($val) {
							global $record, $modClass;
							return $modClass->getProvinciaNombre($record['id_pais'],$record['id_provincia']);
						},
					],
				],
				"filter" => [
					"activo" => '1',
				],
				"sort" => [
					["id_pais","ASC"],
					["id_provincia","ASC"],
				],
				"actions" => [

					"editor" => [
						"class" 		=> "fa fa-pencil",
						"form-bt"		=> _("Nuevo porte"),
						//"form-bt-class"	=> "hidden",
						"text"			=> _("Editar"),
					],
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					],
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			if (empty($opt)) $opt = "List";
		break;
	}
	
	require "./_act/Base/Opt/$opt.php";