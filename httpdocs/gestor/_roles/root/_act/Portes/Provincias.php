<?php

	switch ($opt) {
		default: /// Opciones para el listado
			$id = isset($_REQUEST["id"]) ? preg_replace("/[^\d]/", "", $_REQUEST["id"]) : "";

			$titulo = "Listado de Provincias";

			$list = [
				"id" => "provincias-list",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"		=> "nombre",
						"db"		=> "nombre",
						"title"		=> _("Nombre de la Provincia<br />en Español"),
					],
					[
						"id"		=> "nombre_idioma",
						"db"		=> "nombre_idioma",
						"title"		=> _("Nombre del Pais<br />en el propio idioma"),
					],
					[
						"id"		=> "activo",
						"db"		=> "activo",
						"title"		=> _("Activo"),
					],
					[
						"id"		=> "envio",
						"db"		=> "envio",
						"title"		=> _("Envío"),
					],
				],
				"filter" => [
					"id_pais" => $id
				],
				"sort" => [
					//["Nif", "ASC"],
				],
				"actions" => [],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			if (empty($opt)) $opt = "List";
		break;
	}
	
	require "./_act/Base/Opt/$opt.php";