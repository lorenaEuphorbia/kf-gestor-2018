<?php
/**
 * Entry point for PHP connector, put your customizations here.
 *
 * @license     MIT License
 * @author      Pavel Solomienko <https://github.com/servocoder/>
 * @copyright   Authors
 */

// only for debug
// error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
// ini_set('display_errors', '1');


    /// Cargamos otras clases necesarias (estáticas)
    require $_SERVER['DOCUMENT_ROOT'] . '/gestor/_inc/config.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/gestor/_inc/functions.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-utilities.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-domElements.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-controller.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-adminPreferences.php';

    setAutoloader();

    /// Arrancamos el panel de control
    $i18n = new I18n("admin", $lang_admin);
    $b = new Base();

    /// Recogemos módulo y acción
    setPathVars ($mod, $act, $opt, $url, $id);

    ///  Enrutamiento y permisos
    require $_SERVER['DOCUMENT_ROOT'] . '/gestor/_inc/routes.php';
    $arrPermisions = getPermisions($arrMenu);

    /// Definimos las variables que se utilizarán
    $base_path = "/". ADMIN_FOLDER . "/";


require 'vendor/autoload.php';

// fix display non-latin chars correctly
// https://github.com/servocoder/RichFilemanager/issues/7
setlocale(LC_CTYPE, 'en_US.UTF-8');

// fix for undefined timezone in php.ini
// https://github.com/servocoder/RichFilemanager/issues/43
if(!ini_get('date.timezone')) {
    date_default_timezone_set('GMT');
}


// This function is called for every server connection. It must return true.
//
// Implement this function to authenticate the user, for example to check a
// password login, or restrict client IP address.
//
// This function only authorizes the user to connect and/or load the initial page.
// Authorization for individual files or dirs is provided by the two functions below.
//
// NOTE: If using session variables, the session must be started first (session_start()).
function fm_authenticate()
{
    global $b, $arrPermisions;
    // Customize this code as desired.

    // If this function returns false, the user will just see an error.
    // If this function returns an array with "redirect" key, the user will be redirected to the specified URL:
    // return ['redirect' => 'http://domain.my/login'];
    // 
   return  empty($b->error)? true: ['redirect' => '/gestor/base/nopermissionout/'];
}


// This function is called before any filesystem read operation, where
// $filepath is the file or directory being read. It must return true,
// otherwise the read operation will be denied.
//
// Implement this function to do custom individual-file permission checks, such as
// user/group authorization from a database, or session variables, or any other custom logic.
//
// Note that this is not the only permissions check that must pass. The read operation
// must also pass:
//   * Filesystem permissions (if any), e.g. POSIX `rwx` permissions on Linux
//   * The $filepath must be allowed according to config['patterns'] and config['extensions']
//
function fm_has_read_permission($filepath)
{
    global $b, $arrPermisions, $arrMenu;


    // Customize this code as desired.
    return true;
}


// This function is called before any filesystem write operation, where
// $filepath is the file or directory being written to. It must return true,
// otherwise the write operation will be denied.
//
// Implement this function to do custom individual-file permission checks, such as
// user/group authorization from a database, or session variables, or any other custom logic.
//
// Note that this is not the only permissions check that must pass. The write operation
// must also pass:
//   * Filesystem permissions (if any), e.g. POSIX `rwx` permissions on Linux
//   * The $filepath must be allowed according to config['patterns'] and config['extensions']
//   * config['read_only'] must be set to false, otherwise all writes are disabled
//
function fm_has_write_permission($filepath)
{
    global $b, $arrPermisions, $arrMenu;


    // Customize this code as desired.
    return $b->security->hasPermission($arrPermisions);
}


$config = [];

// example to override the default config
// https://github.com/servocoder/RichFilemanager-PHP/blob/master/src/config/config.local.php
$config = [
    'security' => [
        'extensions' => [
            'policy' => 'DISALLOW_LIST',
            'restrictions' => [
                 "cfg",
                 "cfg",
            ],
        ],    
    ],
];

$app = new \RFM\Application();

$local = new \RFM\Repository\Local\Storage($config);

// example to setup files root folder
$local->setRoot('/', true, true);

$app->setStorage($local);

// set application API
$app->api = new RFM\Api\LocalApi();

$app->run();