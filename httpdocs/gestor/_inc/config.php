<?php

	// Configuración de entorno: "dev" => desarrollo, "test" => pruebas, "prod" => producción
	const ENVIRONMENT	= "pruebas";

	// Configuración de la base de datos de la web:
	const DB_DRIVER 	= "mysql";
const DB_PORT		= "3306";
const DB_NAME		= "klickfotografia";
const DB_HOST		= "localhost";
const DB_USER		= "klickfotografia";
const DB_PASS		= "rl8hK96*";

	// Idiomas (en php 5.6+ se podrían definir los arrays como constantes):
	$lang_web = ["es"];
	$lang_admin = ["es", "en"];

	// Constantes:
	const ADMIN_FOLDER		= "gestor";
	const COMPANY_ID		= "KLICK FOTO";
	const COMPANY_NAME		= "Klickfoto";
	const MAIN_DOMAIN		= "klickfotografia.com";
	const URL_BASE			= "http://www.klickfotografia.com";

	const FILES_URL 		= "files/img/";
