		<nav class="menu">
			<ul>
<?php


	function setSelectedMenu (&$arrMenu) {		
		$searchPath =  trim (str_replace (('/' . ADMIN_FOLDER) , '', $_SERVER['REQUEST_URI']), '/');
		
		# buscar submenu
		foreach ($arrMenu as &$objMenu) {
			if (!empty($objMenu['submenu'])) {
				foreach ($objMenu['submenu'] as &$objSubmenu) {
					if ($searchPath == trim ($objSubmenu['rel_path'], '/')) {
						$objMenu["current"] = true;
						$objSubmenu["current"] = true;
						// echo "<pre>"; print_r($objMenu); exit;
						return;
					}
				}
			}
		}

		# buscar menu
		foreach ($arrMenu as &$objMenu) {
			if ($searchPath == trim ($objMenu['rel_path'], '/')) {
				$objMenu["current"] = true;
				// echo "<pre>"; print_r($objMenu); exit;
				return;
			}			
		}

		# buscar menu padre
		$searchPath = explode('/', $searchPath);
		$searchPath = reset($searchPath);
		foreach ($arrMenu as &$objMenu) {
			if (strpos(trim($objMenu['rel_path'], '/'), $searchPath) === 0) {
				$objMenu["current"] = true;
				// echo "<pre>"; print_r($objMenu); exit;
				return;
			}			
		}
	}
	// setSelectedMenu ($arrMenu);


	/*
	// Recoger elemento seleccionado
	$rel_path = strtolower($mod).'/'.strtolower($act);
	$rel_path_lenght = strlen($rel_path);

	foreach ($arrMenu as $indexParent => $bt) {
		if(isset($bt['submenu']) && !empty($bt['submenu'])) {
			foreach ($bt['submenu'] as $indexChild => $btChild) {
				$str_path = substr($btChild['rel_path'], 0, $rel_path_lenght);
				if ($str_path == $rel_path) {
					$arrMenu[$indexParent]["current"] = true;
					$arrMenu[$indexParent][$indexChild]["current"] = true;
					break;
				}
			}
		}
		$str_path = substr($bt['rel_path'], 0, $rel_path_lenght);
		if ($str_path == $rel_path) {
			$arrMenu[$indexParent]["current"] = true;
			break;
		}
	}
	*/


	foreach ($arrMenu as $bt) {
		$hasPermission = $b->security->hasPermission($bt["permission"]);

		if (!empty($bt["hidden"])) continue;
		if ($hasPermission) {
			$active = isset($bt["current"]) && $bt["current"];
			// $active = in_array($bt["selected"], [$mod, $mod.$act, $mod.$act.$opt]);
?>
				<li<?= $active ? ' class="active arrow"' : ""; ?>>
					<?php if (isset($bt['new_window'])) {
						if ($bt['new_window'] === true) {
							?><a href="<?= $bt["rel_path"]; ?>" target="_blank"><?php
						} else {
							?><a href="#" onclick="window.open('<?= $bt['new_window'] ?>','winname','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no')"><?php
						}
					}
					else { 
						?><a href="<?= $base_path, $bt["rel_path"]; ?>"><?php
					} ?>
					<?php if (empty($bt["custom_icon"])): ?> 
						<i class="<?= $bt["icon"]; ?>"></i> 
					<?php else: ?>
						<?= $bt["custom_icon"]; ?>				
					<?php endif ?>
						<span><?= $bt["text"]; ?></span>
					</a>
<?php
			if (!empty($bt["submenu"])) {
?>
					<ul>
<?php
				foreach ($bt["submenu"] as $sub_bt) {
					$hasPermission = false;

					$hasPermission = $b->security->hasPermission($sub_bt["permission"]);

					if (!empty($sub_bt["hidden"])) continue;
					if ($hasPermission) {
						$active = isset($sub_bt["current"]) && $sub_bt["current"];
						// $active = isset() in_array($sub_bt["selected"], [$mod, $mod.$act, $mod.$act.$opt]);
?>
						<li<?= $active ? ' class="active arrow"' : ""; ?>>
							<?php if (isset($sub_bt['new_window'])) {
								if ($sub_bt['new_window'] === true) {
									?><a href="<?= $sub_bt["rel_path"]; ?>" target="_blank"><?php
								} else {
									?><a href="#" onclick="window.open('<?= $sub_bt['new_window'] ?>','winname','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no')"><?php
								}
							}
							else { 
								?><a href="<?= $base_path, $sub_bt["rel_path"]; ?>"><?php
							} ?>
							<?php if (empty($sub_bt["custom_icon"])): ?> 
								<i class="<?= $sub_bt["icon"]; ?>"></i> 
							<?php else: ?>
								<?= $sub_bt["custom_icon"]; ?>				
							<?php endif ?>
								<span><?= $sub_bt["text"]; ?></span>
							</a>
						</li>
<?php
					}
				}
?>
					</ul>
<?php
			}
?>
				</li>
<?php
		}
	}
?>	
			</ul>
			<div id="menu-mode" class="bt-mode">
				<i class="fa fa-arrow-right"></i>
				<i class="fa fa-arrow-left"></i>
			</div>
		</nav>
