<?php
	require "page-out-top.php";

	if (!empty($message)) {
?>
			<div class="error"><i class="fa fa-exclamation-triangle"></i> <?php echo $message; ?></div>
<?php
	}
?>
			<form method="post">
				<label for="usuario"><?php echo _("Nombre de usuario"); ?>:</label>
				<input type="text" id="username" name="username" placeholder="<?php echo mb_strtoupper(_("Nombre de usuario"), 'UTF-8'); ?>" required autocomplete="off" />
				<label for="clave"><?php echo _("Contraseña"); ?>:</label>
				<input type="password" id="password" name="password" placeholder="<?php echo mb_strtoupper(_("password"), 'UTF-8'); ?>" required autocomplete="off" />
				<button type="submit"><?php echo _("Enviar"); ?></button>
			</form>
			<!--<a href="#"><?php echo _("¿Has olvidado tu contraseña?"); ?></a>-->
<?php
	require "page-out-bottom.php";
