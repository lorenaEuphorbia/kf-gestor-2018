<?php
	require "page-in-top.php";
?>	
				<h1 class="sectionTitle"><?php echo !empty($titulo) ? $titulo : _("Editor"); ?></h1>
<?php
	if (!empty($messages)) {
		foreach ($messages as $m) {
?>
				<div id="msg" class="message <?php echo $m[0]; ?>" ><?php echo $m[1]; ?></div>
<?php
		}
	}

	if (!empty($arr_fields)) {
?>
				<form id="DataForm" name="DataForm" method="post" enctype="multipart/form-data" class="editor-form">
<?php
		foreach ($arr_fields as $field) echo $field;
?>
<?php if (isset($actions['guardar'])) {
		echo $actions['guardar'];
	} else { ?>
	
					<button type="submit" name="SaveData"><?php echo _("Guardar"); ?></button>
<?php } ?>
				</form>
<?php
		echo !empty($bottom_buttons) ? $bottom_buttons : "";
?>	
				<script>$(Editor);</script>
				<?php if (false && !empty($types) && array_search('array', $types) !== false): ?><script>$(SortableList);</script><?php endif ?>
<?php
	}

	require "page-in-bottom.php";