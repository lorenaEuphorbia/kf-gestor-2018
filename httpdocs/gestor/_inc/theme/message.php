<?php
	require "page-top.php";
?>	
				<div class="mensaje error">
					<i class="fa fa-times-circle-o"></i>
					<p>No tienes permiso para acceder a esta página ni a ninguna de las otras si no inicias sesión, mendrugo.</p>
				</div>
<?php
	require "page-bottom.php";