<?php
	AdminPreferences::addCssJs($js, $base_path."js/vendors/datatables/datatables.min.js");
	AdminPreferences::addCssJs($js, $base_path."js/vendors/datatables/plugins/sorting/date-eu.min.js");
	
	require "page-in-top.php";
?>	
				<h1 class="sectionTitle"><?php echo !empty($titulo) ? $titulo : _("Listado"); ?></h1>
				<div class="listado">
<?php
	if (!empty($arr_fields)) {
?>
					<table id="<?php echo $id_list; ?>" data-ajax="<?php echo $ajax_url; ?>" data-order="<?php echo $order; ?>" data-extra-buttons="<?php echo $extra_buttons; ?>" class="list-table display cell-border" width="100%">
						<thead>
							<tr>
<?php
		if ($col_multiple_selection) {
?>
								<th id="btSelectAll" data-default-content="" data-data="null" data-orderable="false" data-class-name="select-checkbox" data-width="33px"></th>
<?php
		}

		//echo "<pre>"; print_r($arr_fields); echo "</pre>";exit;
		foreach ($arr_fields as $field) {
			$options = "";

			if (!empty($field["options"])) {
				foreach ($field["options"] as $name => $value) {
					$options .= " data-$name=\"$value\"";
				}
			}
?>	
								<th<?php echo $options; ?>><?php echo $field["title"]; ?></th>
<?php
		}

		if ($col_actions) {
?>
								<th data-orderable="false" data-class-name="actions-cell" data-width="<?php echo ($col_actions * 34); ?>px"><?php echo _("Acciones"); ?></th>
<?php 
		}
?>
							</tr>
						</thead>
<?php  /*
						<tfoot>
							<tr>
								<th></th>
<?php
	foreach ($arr_fields as $field) {
?>	
								<th><?php echo $field["title"]; ?></th>
<?php
	}
?>
								<th>Acciones</th>
							</tr>
						</tfoot>
*/ ?>
					</table>
<?php 
	}

	echo !empty($bottom_buttons) ? $bottom_buttons : "";
?>	
				</div>
				<script>$(List);</script>
<?php
	require "page-in-bottom.php";