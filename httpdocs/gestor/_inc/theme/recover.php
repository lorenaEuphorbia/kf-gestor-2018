<?php
	require "page-out-top.php";

	if (!empty($message)) {
?>	
			<div class="error"><i class="fa fa-exclamation-triangle"></i> <?php echo $message; ?></div>
<?php
	}
?>
			<form method="post">
				<label for="usuario"><?php echo _("Nombre de usuario"); ?>:</label>
				<input type="text" id="username" name="username" placeholder="<?php echo mb_strtoupper(_("Nombre de usuario"), 'UTF-8'); ?>" required autocomplete="off" />
				<button type="submit"><?php echo _("Enviar"); ?></button>
			</form>
			<a href="/gestor/"><?php echo _("Volver a intentar"); ?></a>
<?php
	require "page-out-bottom.php";