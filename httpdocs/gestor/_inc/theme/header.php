<!doctype html>
<html lang="es">
	<head>
		<title><?php echo COMPANY_NAME, " | ", _("Panel de Control"); ?></title>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" >
		<meta charset="utf-8" >

		<meta name="Copyright"          content="Trama Comunicación: diseño y desarrollo. Copyright 2015">
		<meta name="Resource-Type"      content="document">
		<meta name="Robots"             content="noindex,nofollow">
<?php
	if (isset ($css) && $css) {
		echo PHP_EOL;

		foreach ($css as $cssFile) {
?>
		<link rel="stylesheet" href="<?php echo $cssFile; ?>">
<?php
		}
	}

	if (isset ($js) && $js) {
		echo PHP_EOL;

		foreach ($js as $jsFile) {
?>
		<script src="<?php echo $jsFile; ?>"></script>
<?php
		}
	}

	if (!empty($_COOKIE['pref-menu-mode']) && $_COOKIE['pref-menu-mode'] == "collapsed") $body_class = "menu-collapsed";
?>	
		<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	</head>
	<body<?php echo !empty($body_class) ? " class=\"$body_class\"" : ""; ?>>