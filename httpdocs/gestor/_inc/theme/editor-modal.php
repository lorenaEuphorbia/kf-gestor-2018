				<h1><?php echo !empty($titulo) ? $titulo : _("Editor"); ?></h1>
				<div class="modal-content">
					<div class="messages"></div>
<?php
	if (!empty($messages)) {
		foreach ($messages as $m) {
?>
					<div id="msg" class="message <?php echo $m[0]; ?>" ><?php echo $m[1]; ?></div>
<?php
		}
	}

	if (!empty($arr_fields)) {
?>
					<form id="DataForm" name="DataForm" method="post" enctype="multipart/form-data" class="editor-form">
<?php
		foreach ($arr_fields as $field) echo $field;
?>
						<button type="submit" name="SaveData"><?php echo _("Guardar"); ?></button>
					</form>
<?php
		echo !empty($bottom_buttons) ? $bottom_buttons : "";
?>	
					<script>$(Editor);</script>
<?php
	}
?>	
				</div>