<?php
	require "page-out-top.php";
?>	
			<div class="mensaje error">
				<i class="fa fa-exclamation-triangle"></i> <?php echo $message; ?>
<?php
	if (!empty($arrButtons)) {
?>
				<div>
<?php
		$button_class = count($arrButtons) == 1 ? "full-width" : "middle-width";

		foreach ($arrButtons as $button) {
?>
					<a href="<?php echo $button[0]; ?>" class="<?php echo $button_class; ?>"><button><?php echo $button[1]; ?></button></a>
<?php
		}
?>
				</div>
<?php
	}
?>
			</div>
<?php
	require "page-out-bottom.php";