<?php
	require "header.php";
?>
		<header class="cab">
			<a href="<?php echo $base_path; ?>" id="logo" class="logo"><img src="<?php echo $base_path; ?>img/logo.png" alt="<?php echo COMPANY_NAME; ?>" />KLICKFOTO</a>
			<div class="user-options">
				<a href="#" class="user-profile"><?php echo $b->security->username; ?></a>
				<a href="<?php echo $base_path; ?>logout/" id="user-exit" class="user-exit"><i class="fa fa-power-off"></i> <span>salir</span></a>
			</div>
		</header>
<?php
	require "menu.php";
?>
		<main>
			<div class="content-wrapper">
