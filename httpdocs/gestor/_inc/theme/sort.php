<?php
	//AdminPreferences::addCssJs($css, $base_path."js/vendors/jquery-ui-1.11.4/jquery-ui.min.css");
	AdminPreferences::addCssJs($js, $base_path."js/vendors/jquery-ui-1.11.4/jquery-ui.min.js");
	AdminPreferences::addCssJs($js, $base_path."js/vendors/jquery.mjs.nestedSortable.js");

	require "page-in-top.php";
?>	
				<h1 class="sectionTitle"><?php echo !empty($titulo) ? $titulo : _("Listado"); ?></h1>
<?php
	if (!empty($messages)) {
		foreach ($messages as $m) {
?>
				<div id="msg" class="message <?php echo $m[0]; ?>" ><?php echo $m[1]; ?></div>
<?php
		}
	}
	
	if (!empty($list)) {
?>
				<div class="sortable-list" data-mod="<?php echo lcfirst(get_class($modClass)); ?>" data-act="<?php echo lcfirst($act); ?>">
					<ul>
<?php
		$html = "";

		foreach ($list as $id => $el) {
			$html .= '<li id="'.$id.'-'.$el["id"].'" data-id="'.$el["id"].'">';
			$html .= '<i class="move fa fa-arrows"></i>';
			$html .= $el["text"];
			$html .= '</li>';
		}

		echo $html;
?>
					</ul>
				</div>
				<form id="ResetForm" name="ResetForm" method="post" class="editor-form" style="margin-top: 15px;">
					<input type="hidden" name="id" value="<?php echo $id; ?>" />
					<button name="reset" type="submit"><?php echo _("Resetear Orden"); ?></button>
				</form>
<?php
	}

	echo !empty($bottom_buttons) ? $bottom_buttons : "";
?>	
				<script>$(SortableList);</script>
<?php
	require "page-in-bottom.php";