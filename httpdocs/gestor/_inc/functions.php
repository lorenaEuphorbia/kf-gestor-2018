<?php

	function getFileByMod ($mod, $arrPermisions) {
		# comprobamos tamaño del módulo y accion
		if (strlen($mod) > 3 && strlen($mod)) {

			# Buscamos el módulo específico
			foreach ($arrPermisions as $permision) {
				$filename = $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_roles/' . $permision . '/_mod/' . $mod . '.php';
				if (file_exists($filename)) {
					return $filename;
				}
				# Buscamos el módulo base
				$filename = $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_roles/' . $permision . '/_mod/Base.php';
				if (file_exists($filename)) {
					return $filename;
				}
			}
		}
		$filename = $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_mod/' . $mod . '.php';
		if (file_exists($filename)) {
			return $filename;
		}
		return $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_mod/Base.php';;
	}

	function getFileByAct ($mod, $act, $arrPermisions) {
		# comprobamos tamaño del módulo y accion
		if (strlen($act) > 3 && strlen($act) ) {

			# Buscamos el módulo específico
			foreach ($arrPermisions as $permision) {
				$filename = $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_roles/' . $permision . '/_act/' . $mod . '/' . $act .'.php';
				if (file_exists($filename)) {
					return $filename;
				}
			}
			$filename = $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_act/' . $mod . '/' . $act .'.php';
			if (file_exists($filename)) {
				return $filename;
			}

			# Buscamos la acción predeterminada
			foreach ($arrPermisions as $permision) {
				$filename = $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_roles/' . $permision . '/_act/Base/NotFound.php';
				if (file_exists($filename)) {
					return $filename;
				}
			}
		}

		return $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_act/Base/NotFound.php';;
	}

	function getPermisions (&$arrMenu) {
		global $b;
		$arrPermisions = array();

		$adminFolder = str_replace(ADMIN_FOLDER, '', strtok($_SERVER["REQUEST_URI"],'?'));
		$adminFolder = trim($adminFolder, '/');

		if ($adminFolder === 'ajax') {
			$adminFolder = str_replace($_SERVER['HTTP_ORIGIN'] . '/' . ADMIN_FOLDER, '', strtok($_SERVER["HTTP_REFERER"],'?'));
			$adminFolder = trim($adminFolder, '/');
			// var_dump($adminFolder); exit;
		}

		$routeSearch = searchAndSetCurrentUrl ($arrMenu, $adminFolder);
		foreach (array_column($routeSearch, 'permission') as $value) {
			$arrPermisions = array_merge($arrPermisions, $value);
		};
		$arrPermisions = array_unique($arrPermisions, SORT_REGULAR);

		# ORDENAR PERMISOS SEGUN SU NIVEL
		$arrAllPermisions = $b->users->getList('Userroles',['level','id'],[],['level ASC'],null,null,'FETCH_KEY_PAIR');
		foreach ($arrAllPermisions as $level => $permision) {
			if (!in_array($permision, $arrPermisions)) {
				unset($arrAllPermisions[$level]);
			}
		}
		return array_values($arrAllPermisions);
	}

	function searchAndSetCurrentUrl (&$arrMenu, $url) {

		$urlsEncontradas = array ();
		foreach ($arrMenu as $key => &$value) {
			$urlActual = trim($value['rel_path'], '/');

			if (!empty($value['submenu'])) {
				$result = searchAndSetCurrentUrl($value['submenu'], $url);
				if(!empty($result))  {
					$urlsEncontradas = $urlsEncontradas + $result;
					$value['current'] = true;
				}
			}

			if ($urlActual === $url) {
				$urlsEncontradas[] = $value;
				$value['current'] = true;
			}
			else if ($value['rel_path'] && strpos($url, $value['rel_path']) === 0) {
				$urlsEncontradas[] = $value;
				$value['current'] = true;
			}

		}
		return $urlsEncontradas;
	}

	function setAutoloader () {		
		date_default_timezone_set("Europe/Madrid");
		setlocale(LC_ALL,"es_ES");

		/// Definimos un autocargador para las clases:
		spl_autoload_register(function($className) {
			if(!class_exists($className)) {
				$file =  $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-' . lcfirst($className) . '.php';
				if(file_exists($file)) {
					require($file); 				
				}			
			}
		});
	}

	function setPathVars (&$mod, &$act, &$opt, &$url, &$id) {
		$mod = isset($_POST["mod"]) ? $_POST["mod"] : (isset($_GET["mod"]) ? $_GET["mod"] : "");
		$act = isset($_POST["act"]) ? $_POST["act"] : (isset($_GET["act"]) ? $_GET["act"] : "");
		$opt = isset($_POST["opt"]) ? $_POST["opt"] : (isset($_GET["opt"]) ? $_GET["opt"] : "");
		$url = isset($_POST["url"]) ? $_POST["url"] : (isset($_GET["url"]) ? $_GET["url"] : "");
		$id = isset($_POST["id"]) ? $_POST["id"] : (isset($_GET["id"]) ? $_GET["id"] : "");

		$mod = Utilities::convertToPascalCase($mod);
		$act = Utilities::convertToPascalCase($act);
		$opt = Utilities::convertToPascalCase($opt);
		$url = Utilities::convertToPascalCase($url);
	}

	function loadSlimDependencies () {
		foreach (glob( dirname ($_SERVER['DOCUMENT_ROOT']) . '/vendor/firebase/php-jwt/src/*.php') as $filename) {
			include $filename;
		}

	}

	function getSlimSettings () {
		return include dirname ($_SERVER['DOCUMENT_ROOT']) . '/src/settings.php';
	}