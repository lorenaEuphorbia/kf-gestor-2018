<?php // https://stackoverflow.com/questions/30130913/how-to-do-url-matching-regex-for-routing-framework

	$arrSecciones = [];
	/* recoger las seccionaes de la clase
	if($mod === "Secciones") {
		foreach ($modClass->getSecciones() as $index => $row) {
			$arrSecciones[] = [
					"text"			=>  $row['nombre'],
					"rel_path"		=> "secciones/".$row['slug'],
					"icon"			=> "fa fa-".($row['logo']? $row['logo']:"caret-right"),
					"permission"	=> ["root"],
					"selected"		=> "Secciones".$row['nombre'],
				];
		}
	}
		*/

	$arrMenu = [
		[
			"text"			=> "Inicio",
			"rel_path"		=> "",
			"icon"			=> "fa fa-home",
			"permission"	=> ["root","administrator","master","publisher"],
			"selected"		=> "BaseDashboard",
			"submenu"		=> [
				[
					"rel_path"		=> "js/vendors/ckeditor/",
					"permission"	=> ["root","administrator"],
					"hidden"		=> true
				],
			]
		],
		[
			"new_window"	=> '',
			"text"			=> "Newsletter",
			"rel_path"		=> "suscripciones/maquinariaes",
			"icon"			=> "fa fa-envelope",
			"permission"	=> [],
			"selected"		=> "Productos",
			"submenu"		=> [
				[
					"new_window"	=> "https://login.mailchimp.com/",
					"text"			=> "Acceso Newsletter",
					"rel_path"		=> "https://login.mailchimp.com/",
					"icon"			=> "fa fa-user-o",
					"permission"	=> [],
					"selected"		=> "NewsletterMalchimp"
				],
				[
					"text"			=> "Maq. Agrícola ES",
					"rel_path"		=> "suscripciones/maquinariaes",
					"icon"			=> "fa fa-leaf",
					"permission"	=> [],
					"selected"		=> "NewsletterMaquinariaes"
				],
				[
					"text"			=> "Línea Verde ES",
					"rel_path"		=> "suscripciones/lineaverdees",
					"icon"			=> "fa fa-envira",
					"permission"	=> [],
					"selected"		=> "NewsletterLineaverdees"
				],
				[
					"text"			=> "Maq. Agrícola PT",
					"rel_path"		=> "suscripciones/maquinariapt",
					"icon"			=> "fa fa-leaf",
					"permission"	=> [],
					"selected"		=> "NewsletterMaquinariapt"
				],
				[
					"text"			=> "Línea Verde PT",
					"rel_path"		=> "suscripciones/lineaverdept",
					"icon"			=> "fa fa-envira",
					"permission"	=> [],
					"selected"		=> "NewsletterLineaverdept"
				],
			]
		],
		//TRABAJOS
		[
			"text"			=> "Trabajos",
			"rel_path"		=> "productos/productos",
			"icon"			=> "fa fa-camera-retro",
			"permission"	=> ["root","administrator","master"],
			"selected"		=> "Trabajos",
			"submenu"		=> [
				[
					"text"			=> "Comentarios",
					"rel_path"		=> "productos/comments",
					"icon"			=> "fa fa-commenting-o",
					"permission"	=> ["root","administrator","master"],
					"selected"		=> "Trabajos",
					"hidden"		=> true,
				],
				[
					"text"			=> "Trabajos",
					"rel_path"		=> "productos/productos/",
					"icon"			=> "fa fa-picture",
					"permission"	=> ["root","administrator","master"],
					"selected"		=> "Trabajos",
					"hidden"		=> true,
				],
				[
					"text"			=> "Categorías",
					"rel_path"		=> "productos/categorias",
					"icon"			=> "fa fa-sitemap",
					"permission"	=> ["root"],
					"selected"		=> "Trabajos"
				],
				[
					"text"			=> "Tags",
					"rel_path"		=> "productos/tags",
					"icon"			=> "fa fa-tag",
					"permission"	=> ["root","administrator","master"],
					"selected"		=> "ProductosTags"
				],
				[
					"text"			=> "Galeria",
					"rel_path"		=> "productos/galeria/",
					"icon"			=> "fa fa-image",
					"permission"	=> ["root","administrator","master"],
					"selected"		=> "Trabajos",
					"hidden"		=> true,
				],
				[
					"text"			=> "Files",
					"rel_path"		=> "productos/files/",
					"icon"			=> "fa fa-file-o",
					"permission"	=> ["root","administrator","master"],
					"selected"		=> "Trabajos",
					"hidden"		=> true,
				],
			]
		],
		/* COMPRAS
		[
			"text"			=> "Compras",
			"rel_path"		=> "orders/orders/",
			"icon"			=> "fa fa-credit-card",
			"permission"	=> ["root","administrator","master","publisher"],
			"selected"		=> "Compras",
			"submenu"		=> [
				[
					"text"			=> "Pedidos",
					"rel_path"		=> "orders/orders",
					"icon"			=> "fa fa-file-o",
					"permission"	=> ["root","administrator","master"],
					"selected"		=> "ComprasPedidos"
				],
				[
					"text"			=> "Devoluciones",
					"rel_path"		=> "orders/refunds",
					"icon"			=> "fa fa-file-o",
					"permission"	=> ["root","administrator","master","publisher"],
					"selected"		=> "ComprasDevoluciones"
				],
			]
		],
		[
			"text"			=> "Portes",
			"rel_path"		=> "portes/portes/",
			"icon"			=> "fa fa-truck",
			"permission"	=> ["root"],
			"selected"		=> "Portes",
			"submenu"		=> [
				[
					"text"			=> "Portes",
					"rel_path"		=> "portes/portes/",
					"icon"			=> "fa fa-money",
					"permission"	=> ["root"],
					"selected"		=> "PortesPortes"
				],
				[
					"text"			=> "Paises",
					"rel_path"		=> "portes/paises/",
					"icon"			=> "fa fa-map",
					"permission"	=> ["root"],
					"selected"		=> "PortesPaises"
				],
				[
					"text"			=> "Transportistas",
					"rel_path"		=> "portes/transportistas/",
					"icon"			=> "fa fa-road",
					"permission"	=> ["root"],
					"selected"		=> "PortesTransportistas"
				],
				[
					"text"			=> "Provincias",
					"rel_path"		=> "portes/provincias/",
					"icon"			=> "fa fa-road",
					"permission"	=> ["root"],
					"selected"		=> "PortesProvincias",
					"hidden"		=> true,
				],
			]
		],
		*/
		[
			"text"			=> "Blog",
			"rel_path"		=> "noticias/posts",
			"icon"			=> "fa fa-sticky-note",
			"permission"	=> ["root","administrator","master","publisher"],
			"selected"		=> "Blog",
			"submenu"		=> [
				[
					"text"			=> "Noticias",
					"rel_path"		=> "noticias/posts",
					"icon"			=> "fa fa-file-o",
					"permission"	=> ["root","administrator","master","publisher"],
					"selected"		=> "Blog",
					"hidden"		=> true
				],
				[
					"text"			=> "Categorías",
					"rel_path"		=> "noticias/categorias",
					"icon"			=> "fa fa-sitemap",
					"permission"	=> ["root","administrator","master","publisher"],
					"selected"		=> "Blog"
				],
				[
					"text"			=> "Tags",
					"rel_path"		=> "noticias/tags",
					"icon"			=> "fa fa-tag",
					"permission"	=> ["root","administrator","master","publisher"],
					"selected"		=> "Blog"
				],
				[
					"text"			=> "Galeria",
					"rel_path"		=> "noticias/galeria/",
					"icon"			=> "fa fa-image",
					"permission"	=> ["root","administrator","master"],
					"selected"		=> "Blog",
					"hidden"		=> true,
				],
				[
					"text"			=> "Files",
					"rel_path"		=> "noticias/files/",
					"icon"			=> "fa fa-file-o",
					"permission"	=> ["root","administrator","master"],
					"selected"		=> "Blog",
					"hidden"		=> true,
				],
			]
		],
		[
			"text"			=> "Secciones",
			"rel_path"		=> "secciones/secciones/",
			"icon"			=> "fa fa-list",
			"permission"	=> [""],
			"selected"		=> "Secciones",
			"submenu"		=> $arrSecciones
		],
		[
			"text"			=> "Area privada",
			"rel_path"		=> "users/users/",
			"icon"			=> "fa fa-male",
			"permission"	=> ["root"],
			"selected"		=> "Users",
			"submenu"		=> [
				[
					"text"			=> "Usuarios",
					"rel_path"		=> "users/users",
					"icon"			=> "fa fa-male",
					"permission"	=> ["root","administrator"],
					"selected"		=> "Users"
				],
				[
					"text"			=> "Datos de usuarios",
					"rel_path"		=> "users/userdata/",
					"icon"			=> "fa fa-address-card-o",
					"permission"	=> ["root","administrator"],
					"selected"		=> "UserData"
				],
				[
					"text"			=> "Mis datos",
					"rel_path"		=> "users/userdata/editor/" . (!empty($b->security->data['id'])? 'id=' . $b->security->data['id']: ''),
					"icon"			=> "fa fa-address-card-o",
					"permission"	=> ["root","administrator"],
					"selected"		=> "UserData"
				],
				[
					"text"			=> "Documentos",
					"rel_path"		=> "users/archivos",
					"icon"			=> "fa fa-file-pdf-o",
					"permission"	=> ["root","master","publisher"],
					"selected"		=> "DistribuidoresDocumentos"
				],
			]
		],
		/*
		[
			"text"			=> "Archivos",
			"rel_path"		=> "media/server/iframe",
			"icon"			=> "fa fa-file-archive-o",
			"permission"	=> ["root"],
			"selected"		=> "Archivos",
			"submenu"		=> [
				[
					"text"			=> "Archivos",
					"rel_path"		=> "_media",
					"icon"			=> "fa fa-file-o",
					"permission"	=> ["root"],
					"selected"		=> "Archivos",
					"hidden"		=> true,
				],
				[
					"new_window"	=> true,
					"text"			=> "Externo",
					"rel_path"		=> "/gestor/_media",
					"icon"			=> "fa fa-external-link",
					"permission"	=> ["root"],
					"selected"		=> "Archivos",
				],
			]
		],
		*/
		[
			"text"			=> "Contenido web",
			"rel_path"		=> "preferences/textos",
			"icon"			=> "fa fa-font",
			"permission"	=> ["root","administrator","master","publisher"],
			"selected"		=> "Contenido",
			"submenu"		=> [

				[
					"text"			=> "Textos",
					"rel_path"		=> "preferences/textos",
					"icon"			=> "fa fa-font",
					"permission"	=> ["root","administrator","master","publisher"],
					"selected"		=> "Contenido"
				],
			]
		],
		[
			"text"			=> "Prueba de textos",
			"rel_path"		=> "detrasdelreflejo/productos",
			"icon"			=> "fa fa-book",
			"permission"	=> ["root"],
			"selected"		=> "Textos",
			"submenu"		=> [
				[
					"text"			=> "Apartado Boda Home",
					"rel_path"		=> "textos/textos/editor/id=apartado_boda_home",
					"icon"			=> "fa",
					"permission"	=> ["root","administrator","master"],
					"selected"		=> "TextosApartadoBodaHome"
				],

			]
		],

		//apartado_boda_home
		[
			"text"			=> "Preferencias",
			"rel_path"		=> "preferences/contacto",
			"icon"			=> "fa fa-gears",
			"permission"	=> ["root","administrator","master","publisher"],
			"selected"		=> "Preferences",
			"submenu"		=> [
				[
					"text"			=> "SEO",
					"rel_path"		=> "preferences/general",
					"icon"			=> "fa fa-line-chart",
					"permission"	=> ["root","administrator","master","publisher"],
					"selected"		=> "PreferencesGeneral"
				],
				[
					"text"			=> "Pasarela Banco",
					"rel_path"		=> "preferences/pasarela",
					"icon"			=> "fa fa-bank",
					"permission"	=> ["root"],
					"selected"		=> "PreferencesPasarela",
					"hidden"		=> true,
				],
				[
					"text"			=> "Pasarela Paypal",
					"rel_path"		=> "preferences/paypal",
					"icon"			=> "fa fa-paypal",
					"permission"	=> ["root"],
					"selected"		=> "PreferencesPaypal",
					"hidden"		=> true,
				],
				[
					"text"			=> "Newsletter",
					"rel_path"		=> "preferences/newsletter",
					"icon"			=> "fa fa-envelope-o",
					"permission"	=> ["root"],
					"selected"		=> "PreferencesNewsletter",
					"hidden"		=> true,
				],
				[
					"text"			=> "Contacto",
					"rel_path"		=> "preferences/contacto",
					"icon"			=> "fa fa-id-card-o",
					"permission"	=> ["root","administrator","master"],
					"selected"		=> "PreferencesContacto"
				],
				[
					"text"			=> "Seguimiento",
					"rel_path"		=> "preferences/analytics",
					"icon"			=> "fa fa-bar-chart-o",
					"permission"	=> ["root","administrator","master"],
					"selected"		=> "PreferencesAnalytics"
				],
				[
					"text"			=> "Formularios",
					"rel_path"		=> "preferences/forms",
					"icon"			=> "fa fa-envelope-o",
					"permission"	=> ["root"],
					"selected"		=> "PreferencesForms"
				],
				[
					"text"			=> "Textos legales",
					"rel_path"		=> "preferences/legal",
					"icon"			=> "fa fa-gavel",
					"permission"	=> ["root","administrator","master"],
					"selected"		=> "PreferencesLegal"
				],
				[
					"text"			=> "Textos genéricos",
					"rel_path"		=> "preferences/genericos",
					"icon"			=> "fa fa-align-left",
					"permission"	=> ["root","administrator","master"],
					"selected"		=> "PreferencesGenricos"
				],
				[
					"text"			=> "Contraseña",
					"rel_path"		=> "preferences/password",
					"icon"			=> "fa fa-key",
					"permission"	=> ["root","administrator","master","publisher"],
					"selected"		=> "PreferencesPassword",
					"submenu"		=> [],
				],
				[
					"text"			=> "Idiomas",
					"rel_path"		=> "secciones/idiomas",
					"icon"			=> "fa fa-language",
					"permission"	=> ["root"],
					"selected"		=> "PreferencesIdiomas",
					"submenu"		=> [],
					"hidden"		=> true,
				],
			]
		],
	];

	/*
	function searchAndSetCurrentUrl (&$arrMenu, $url) {
		$urlsEncontradas = array ();
		foreach ($arrMenu as $key => &$value) {
			$value['reg_exp'] = Utilities::getRegex($value['rel_path']);

			if (!empty($value['submenu'])) {
				$result = searchAndSetCurrentUrl($value['submenu'], $url);
				if(!empty($result))  {
					$urlsEncontradas = $urlsEncontradas + $result;
					$value['current'] = true;
				}
			}

			if ($value['reg_exp'] && preg_match($value['reg_exp'], $url, $matches)) {
				$urlsEncontradas[] = $value;
					$value['current'] = true;
			}

			$value['rel_path'] = strtok($value['rel_path'], ':');
			$value['rel_path'] = strtok($value['rel_path'], '(');
			$value['rel_path'] = strtok($value['rel_path'], '{');

		}
		return $urlsEncontradas;
	}
	*/
