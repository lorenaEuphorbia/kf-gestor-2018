<?php


	switch ($opt) {
		case 'Editor':

			$titulo = ["Nuevo Producto ", "Editando Producto"];

			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "hidden",
						"db"		=> "id",
						"readonly"	=> true,
					],
					[
						"id"		=> "nombre",
						"type"		=> "text",
						"db"		=> "nombre",
						"required"	=> true,
						"length"	=> 250,
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Nombre"),
						"class"		=> "char-counter",
						'placeholder' => 'requerido',
					],
					[
						"id"		=> "slug",
						"type"		=> "text",
						"db"		=> "url",
						"unique"	=> true,
						"required"	=> true,
						"checks"	=> ["slug"],
						"length"	=> 250,
						"div"		=> true,
						"label"		=> _("Slug (url amigable)"),
						"data"		=> [
							"slug"	=> "nombre"
						],
						"class"		=> "char-counter",
						'placeholder' => 'requerido',
					],
					[
						"id"		=> "img",
						"type"		=> "input-image",
						"db"		=> "",
						"required"	=> true,
						"checks"	=> [],
						"div"		=> true,
						"div_class"	=> "input-image",
						"label"		=> _("Imagen"),
					],
					[
						"id"		=> "descripcion",
						"type"		=> "textarea",
						"db"		=> "descripcion",
						"div"		=> true,
						"label"		=> _("Descripción"),
						"placeholder"	=> "opcional",
					],
					[
						"id"		=> "caracteristicas",
						"type"		=> "ckeditor",
						"db"		=> "caracteristicas",
						"div"		=> true,
						"label"		=> _("Características"),
					],
					[
						"id"		=> "etiqueta",
						"type"		=> "text",
						"db"		=> "etiqueta",
						"div"		=> true,
						"length"	=> 10,
						"label"		=> _("Etiqueta"),
						"class"		=> "char-counter",
						"placeholder"		=> "opcional",
						'div_class'	=> 'cols two',
					],
					[
						"id"		=> "peso",
						"type"		=> "number",
						"db"		=> "peso",
						"default"	=> "0",
						"div"		=> true,
						"label"		=> _("Peso (gr)"),
						"placeholder"	=> "Peso en gramos para envío",
						'div_class'	=> 'cols two',
					],
					[
						"id"		=> "id_categoria",
						"type"		=> "hidden",
						"db"		=> "id_categoria",
						"default"	=> '0',
						"readonly"	=> true,
					],
					[
						"id"		=> "precio",
						"type"		=> "number",
						"db"		=> "precio",
						"default"	=> "0",
						"div"		=> true,
						"label"		=> _("Precio con IVA"),
						"checks"	=> ['decimal'],
						"custom"	=> [
							'step' 	=> '0.01',
							'min'	=> '0',
						],
						'div_class'	=> 'cols two',
					],
					[
						"id"		=> "iva",
						"type"		=> "number",
						"db"		=> "iva",
						"default"	=> "4",
						"div"		=> true,
						"label"		=> _("IVA %"),
						"checks"	=> ['decimal'],
						"custom"	=> [
							'step' 	=> '0.5',
							'min'	=> '0',
						],
						"placeholder"	=> "% de iva para desglosar",
						'div_class'	=> 'cols two',
					],
					[
						"id"		=> "publicado",
						"type"		=> "checkbox",
						"db"		=> "publicado",
						"div"		=> true,
						"label"		=> _("Publicar"),
					],

				],
				"hidden" => [],
			];

			$buttons = [
				//"new"	=> "Nuevo Post",
				"back"	=> _("Volver")
			];
		break;
		default: /// Opciones para el listado
			$titulo = "Listado de productos";

			$list = [
				"id" => "productos",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "id",
						"db"	=> "nombre",
						"title"	=> _("Nombre"),
					],
					[
						"id"	=> "url",
						"db"	=> "url",
						"title"	=> _("Url"),
					],
					[
						"id"	=> "id_categoria",
						"db"	=> "id_categoria",
						"title"	=> _("Categoría"),
						"shown"		=> function($val) {
							global $arrCategorias;
							return isset($arrCategorias[$val])? $arrCategorias[$val]: '';
						},
					],
				],
				"filter" => [
				],
				"sort" => [
					["url", "ASC"],
				],
				"actions" => [
					"editor" => [
						"form-bt"		=> _("Añadir producto"),
						"class" 		=> "fa fa-pencil",
						//"form-bt-class"	=> "hidden",
						"text"			=> _("Editar"),
						"hidden"	=> [ ]
					],
					"galeria" => [
						"action"		=> "productos/galeria/list/",
						"class" 		=> "fa fa-image",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Galer&iacute;a"),
					],
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			//var_dump($arr_usuarios);

			if (empty($opt)) $opt = "List";
		break;
	}

	//echo __DIR__; exit;

	require "./_act/Base/Opt/$opt.php";
