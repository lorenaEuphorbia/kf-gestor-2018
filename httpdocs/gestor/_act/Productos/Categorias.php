<?php


	switch ($opt) {
		case 'Editor':
		case 'EditorSave':
			$titulo = ["Nueva categoría", "Editando categoría"];

			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "hidden",
						"db"		=> "id",
						"readonly"	=> true,
					],
					[
						"id"		=> "nombre",
						"type"		=> "text",
						"db"		=> "nombre",
						"required"	=> true,
						"length"	=> 250,
						"div"		=> true,
						"label"		=> _("Nombre"),
						"class"		=> "char-counter",
					],
					[
						"id"		=> "slug",
						"type"		=> "text",
						"db"		=> "url",
						"unique"	=> true,
						"required"	=> true,
						"length"	=> 250,
						"checks"	=> ["slug"],
						"div"		=> true,
						"label"		=> _("Slug (url amigable)"),
						"data"		=> [
							"slug"	=> "text"
						],
						"class"		=> "char-counter",
					],
				],
				"hidden" => [
					"m"	=> get_class($modClass), // módulo
					"a"	=> $act // acción
				]
			];

			$view = "./_inc/theme/editor-modal.php";

			require "./_act/Base/Opt/$opt.php";
		break;
		default: /// Opciones para el listado
			$titulo = "Categorías de productos";
			$levels = 2;

			$arr_buttons = $modClass->getCategoriasIndexadas();
			// echo "<pre>"; print_r($arr_buttons); exit;

			require "./_act/Base/Opt/Levels.php";
		break;
	}
