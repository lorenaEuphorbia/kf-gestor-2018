<?php


	switch ($opt) {
		case 'Editor':
			$parent_id = isset($_POST["parent-id"]) ? $_POST["parent-id"] : (isset($_GET["parent-id"]) ? $_GET["parent-id"] : "");
			$parent_id = preg_replace("/[^\d]/", "", $parent_id);

			$id = isset($_POST["id"]) ? $_POST["id"] : (isset($_GET["id"]) ? $_GET["id"] : "");
			$id = preg_replace("/[^\d]/", "", $id);			

			$arrOpciones = $modClass->getOptionSiblings($act);
			$arrOpciones = array_combine($arrOpciones, $arrOpciones);

			$titulo = ["Nueva foto", "Editando foto"];
			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "hidden",
						"db"		=> "id",
						"readonly"	=> true,
					],
					[
						"id"		=> "owner_id",
						"type"		=> "select-chosen",
						"db"		=> "owner_id",
						"div"		=> true,
						"required"	=> true,
						"label"		=> _("Owner ID (propietario)"),
						"options"	=> [
							"empty"	=> "",
							"values" => $modClass->getList($mod, ['id', 'nombre'], null, ['nombre ASC'], null, null, 'FETCH_KEY_PAIR'),
						],
						"class"		=> "chosen",
					],
					[
						"id"		=> "owner_type",
						"type"		=> $arrOpciones? "select-chosen": "text",
						"db"		=> "owner_type",
						"div"		=> true,
						"required"	=> true,
						"label"		=> _("Owner Type (tipo propietario)"),
						"options"	=> [
							"values" => $arrOpciones,
						],
						"class"		=> $arrOpciones? "chosen": "char-counter",
					],
					[
						"id"		=> "type",
						"type"		=> "text",
						"db"		=> "type",
						"label"		=> _("Type (tipo de archivo)"),
						'placeholder' => 'requerido',
						"length"	=> 20,
						"div"		=> true,
						"unique"	=> false,
						"required"	=> false,
						"readonly"	=> false,
						"disabeld"	=> false,
						"checks"	=> [],
						"data"		=> [],
						"options"	=> [],
						"shown"		=> [],
						"save"		=> [],
						"custom"	=> [],
						"options"	=> [],
						"class"		=> "char-counter",
						"div_class"	=> "",
					],
					[
						"id"		=> "title_1",
						"type"		=> "text",
						"db"		=> "title_1",
						"label"		=> _("Title (nombre original)"),
						'placeholder' => '',
						"length"	=> 20,
						"div"		=> true,
						"unique"	=> false,
						"required"	=> false,
						"readonly"	=> false,
						"disabeld"	=> false,
						"checks"	=> [],
						"data"		=> [],
						"options"	=> [],
						"shown"		=> [],
						"save"		=> [],
						"custom"	=> [],
						"options"	=> [],
						"class"		=> "char-counter",
						"div_class"	=> "",
					],
					[
						"id"		=> "name",
						"type"		=> "text",
						"db"		=> "name",
						"label"		=> _("Name"),
						'placeholder' => '',
						"length"	=> false,
						"div"		=> true,
						"unique"	=> false,
						"required"	=> false,
						"readonly"	=> false,
						"disabeld"	=> false,
						"checks"	=> [],
						"data"		=> [],
						"options"	=> [],
						"shown"		=> [],
						"save"		=> [],
						"custom"	=> [],
						"options"	=> [],
						"class"		=> "char-counter",
						"div_class"	=> "",
					],
					[
						"id"		=> "mime_type",
						"type"		=> "select-chosen",
						"db"		=> "mime_type",
						"label"		=> _("Mime Type"),
						'placeholder' => '',
						"length"	=> 250,
						"div"		=> true,
						"unique"	=> false,
						"required"	=> false,
						"readonly"	=> false,
						"disabeld"	=> false,
						"checks"	=> [],
						"data"		=> [],
						"shown"		=> [],
						"save"		=> [],
						"custom"	=> [],
						"options"	=> [
							"values" => array_flip(array_reverse(FileTypes::lista())),	
						],
						"class"		=> "chosen",
						"div_class"	=> "",
					],
					[
						"id"		=> "size",
						"type"		=> "number",
						"db"		=> "size",
						"label"		=> _("Tamaño"),
						'placeholder' => '',
						"length"	=> 20,
						"div"		=> true,
						"unique"	=> false,
						"required"	=> false,
						"readonly"	=> false,
						"disabeld"	=> false,
						"checks"	=> [],
						"data"		=> [],
						"options"	=> [],
						"shown"		=> [],
						"save"		=> [],
						"custom"	=> [],
						"options"	=> [],
						"class"		=> "",
						"div_class"	=> "",
					],
					[
						"id"		=> "height",
						"type"		=> "number",
						"db"		=> "height",
						"label"		=> _("Altura (si imagen)"),
						'placeholder' => '',
						"length"	=> 10,
						"div"		=> true,
						"unique"	=> false,
						"required"	=> false,
						"readonly"	=> false,
						"disabeld"	=> false,
						"checks"	=> [],
						"data"		=> [],
						"options"	=> [],
						"shown"		=> [],
						"save"		=> [],
						"custom"	=> [],
						"options"	=> [],
						"class"		=> "",
						"div_class"	=> "",
					],
					[
						"id"		=> "width",
						"type"		=> "number",
						"db"		=> "width",
						"label"		=> _("Anchura (si imagen)"),
						'placeholder' => '',
						"length"	=> 10,
						"div"		=> true,
						"unique"	=> false,
						"required"	=> false,
						"readonly"	=> false,
						"disabeld"	=> false,
						"checks"	=> [],
						"data"		=> [],
						"options"	=> [],
						"shown"		=> [],
						"save"		=> [],
						"custom"	=> [],
						"options"	=> [],
						"class"		=> "",
						"div_class"	=> "",
					],
					[
						"id"		=> "creacion",
						"type"		=> "text",
						"db"		=> "creacion",
						"label"		=> _("Fecha Creación"),
						'placeholder' => '',
						"length"	=> 10,
						"div"		=> true,
						"unique"	=> false,
						"required"	=> false,
						"readonly"	=> true,
						"disabeld"	=> false,
						"checks"	=> [],
						"data"		=> [],
						"options"	=> [],
						"shown"		=> [],
						"save"		=> [],
						"custom"	=> [],
						"options"	=> [],
						"class"		=> "",
						"div_class"	=> "",
					],
					[
						"id"		=> "modificacion",
						"type"		=> "text",
						"db"		=> "modificacion",
						"label"		=> _("Fecha Modificación"),
						'placeholder' => '',
						"length"	=> 10,
						"div"		=> true,
						"unique"	=> false,
						"required"	=> false,
						"readonly"	=> true,
						"disabeld"	=> false,
						"checks"	=> [],
						"data"		=> [],
						"options"	=> [],
						"shown"		=> [],
						"save"		=> [],
						"custom"	=> [],
						"options"	=> [],
						"class"		=> "",
						"div_class"	=> "",
					],
				],
				"hidden" => []
			];

			$buttons = [
				//"new"	=> "Nuevo Post",
				"back"	=> _("Volver")
			];
		break;
		default: /// Opciones para el listado
			$id = isset($_POST["id"]) ? $_POST["id"] : (isset($_GET["id"]) ? $_GET["id"] : "");
			$id = preg_replace("/[^\d]/", "", $id);
			//var_dump($id);

			$objParent = $modClass->getRecord ($mod, null, ['id'=>$id], true);

			$titulo = "Archivos de " . $act;
			$list = [
				"id" => "post",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> _("Imagen"),
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "true",
							"searchable"	=> "false"
						],
						"shown"		=> function($val) {
							global $modClass, $act;

							if ($modClass->files->searchById($val)) {
								return '<img class="thmbnail" src="'.$modClass->files->getFilePath().'" />';
							}
							return '';
						},
					],
					[
						"id"	=> "name",
						"db"	=> "name",
						"title"	=> _("Nombre"),
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "true",
							"searchable"	=> "true"
						]
					],
					[
						"id"	=> "creacion",
						"db"	=> "creacion",
						"title"	=> _("Creación"),
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "true",
							"searchable"	=> "true"
						]
					],
				],
				"filter" => [
					'owner_id' => $id,
				],
				"sort" => [
					["creacion", "DESC"],
				],
				"actions" => [
					"editor" => [
						"form-bt"		=> _("Añadir imagen"),
						"class" 		=> "fa fa-pencil",
						//"form-bt-class"	=> "hidden",
						"text"			=> _("Editar"),
						"hidden"	=> [
							"parent-id"	=> $id
						]
					],
					"delete" => [
						"action"		=> strtolower($mod) . '/files/delete/',
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			$buttons = [
				//"sort"	=> [
				//	"action"=> strtolower("$mod/galeria/sort/"),
				//	"hidden"=> ["id"=> $id],
				//	"text" 	=> _("Ordenar")
				//],
				"back"	=> [
					"action"=> strtolower("$mod/$mod/list/"),
					"hidden"=> [],
					"text" 	=> _("Volver")
				]
			];
			//var_dump($arr_usuarios);

			if (empty($opt)) $opt = "List";
		break;
	}

	//echo __DIR__; exit;
	
	require "./_act/Base/Opt/$opt.php";