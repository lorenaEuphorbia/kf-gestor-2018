<?php

	switch ($opt) {
		case 'Editor':
			$titulo = ["Nuevo tag", "Editando tag"];

			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "hidden",
						"db"		=> "id",
						"readonly"	=> true,
					],
					[
						"id"		=> "text",
						"type"		=> "text",
						"db"		=> "text_1",
						"unique"	=> true,
						"required"	=> true,
						"length"	=> 255,
						"div"		=> true,
						"label"		=> _("Texto"),
					],
					[
						"id"		=> "slug",
						"type"		=> "text",
						"db"		=> "url_1",
						"unique"	=> true,
						"required"	=> true,
						"length"	=> 255,
						"checks"	=> ["slug"],
						"div"		=> true,
						"label"		=> _("Slug (url amigable)"),
						"data"		=> [
							"slug"	=> "text"
						],
					],
				],
				"hidden" => []
			];

			$buttons = [
				//"editor"	=> "Nuevo Post",
				"back"	=> _("Volver")
			];
		break;
		default: /// Opciones para el listado
			$titulo = "Listado de tags";

			$list = [
				"id" => "tag-list",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "text",
						"db"	=> "text_1",
						"title"	=> _("Texto"),
					]
				],
				"sort" => [
					["text_1", "ASC"]
				],
				"actions" => [
					"editor" => [
						"form-bt"		=> _("Nuevo tag"),
						"class" 		=> "fa fa-pencil",
						"text"			=> _("Editar"),
					],
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			if (empty($opt)) $opt = "List";
		break;
	}
	
	require "./_act/Base/Opt/$opt.php";