<?php

	$id = isset($_REQUEST['id']) && !empty($_REQUEST['id']) ? preg_replace("/[^\d]/", "", $_REQUEST["id"]) : false;
	$parent_id = isset($_REQUEST['parent-id']) && !empty($_REQUEST['parent-id']) ? preg_replace("/[^\d]/", "", $_REQUEST["parent-id"]) : false;

	switch ($opt) {
		case 'Editor':

			$titulo = ["Nuevo Archivo ", "Editando Archivo"];

			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "hidden",
						"db"		=> "id",
						"readonly"	=> true,
					],
					[
						"id"		=> "id_noticia",
						"type"		=> "hidden",
						"db"		=> "id_noticia",
						"default"	=> $parent_id,
					],
					[
						"id"		=> "creacion",
						"type"		=> "text",
						"db"		=> "creacion",
						"readonly"	=> true,
						"div"		=> true,
						"label"		=> _("Fecha Creación"),
					],
					[
						"id"		=> "descripcion",
						"type"		=> "textarea",
						"db"		=> "descripcion",
						"lenght"	=> 1000,
						"div"		=> true,
						"label"		=> _("Descipción"),
					],
					[
						"id"		=> "archivos",
						"type"		=> "input-file",
						"db"		=> "",
						"required"	=> true,
						"checks"	=> [],
						"div"		=> true,
						"div_class"	=> "input-file",
						"label"		=> _("Archivo"),
					],
				],
				"hidden" => ["parent-id" => $parent_id],
			];

			$buttons = [
				//"new"	=> "Nuevo Post",
				"back"	=> _("Volver")
			];
		break;
		default: /// Opciones para el listado
			$titulo = "Listado de archivos";

			$list = [
				"id" => "post",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "creacion",
						"db"	=> "creacion",
						"title"	=> _("Fecha"),
					],
					[
						"id"	=> "descripcion",
						"db"	=> "descripcion",
						"title"	=> _("Descripción"),
					],
				],
				"filter" => [
					"id_noticia" => "$id",
				],
				"sort" => [
					["creacion", "DESC"],
				],
				"actions" => [
					"editor" => [
						"form-bt"		=> _("Añadir archivo"),
						"class" 		=> "fa fa-pencil",
						//"form-bt-class"	=> "hidden",
						"text"			=> _("Editar"),
						"hidden"	=> [
							"parent-id"	=> $id,
						]
					],
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			//var_dump($arr_usuarios);

			if (empty($opt)) $opt = "List";
		break;
	}

	//echo __DIR__; exit;
	
	require "./_act/Base/Opt/$opt.php";