<?php

	switch ($opt) {
		case 'Editor':
			$titulo = ["Nueva noticia", "Editando noticia"];

			$arr_categories = [];
			$tmp_categories = $modClass->getList("Categories", ["id", "text_1"], null, ["text_1"]);
			foreach ($tmp_categories as $cat) $arr_categories[$cat["id"]] = $cat["text_1"];

			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "hidden",
						"db"		=> "id",
						"readonly"	=> true,
					],
					// ESPAÑOL
					[
						"id"		=> "lang_es",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "ESPAÑOL",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						"id"		=> "title",
						"type"		=> "text",
						"db"		=> "title_1",
						"length"	=> 255,
						"required"	=> true,
						"unique"	=> true,
						"div"		=> true,
						"label"		=> _("Título"),
					],
					[
						"id"		=> "slug",
						"type"		=> "text",
						"db"		=> "url_1",
						"unique"	=> true,
						"required"	=> true,
						"checks"	=> ["slug"],
						"length"	=> 255,
						"div"		=> true,
						"label"		=> _("Slug (url amigable)"),
						"data"		=> [
							"slug"	=> "title"
						],
					],
					[
						"id"		=> "intro",
						"type"		=> "textarea",
						"db"		=> "intro_1",
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Introducción"),
					],
					[
						"id"		=> "content",
						"type"		=> "ckeditor",
						"db"		=> "content_1",
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Contenido")
					],
					// INGLÉS
					[
						"id"		=> "lang_en",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "INGLÉS",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						"id"		=> "title_en",
						"type"		=> "text",
						"db"		=> "title_2",
						"length"	=> 255,
						"required"	=> true,
						"unique"	=> true,
						"div"		=> true,
						"label"		=> _("Título"),
					],
					[
						"id"		=> "slug_en",
						"type"		=> "text",
						"db"		=> "url_2",
						"unique"	=> true,
						"required"	=> true,
						"checks"	=> ["slug"],
						"length"	=> 255,
						"div"		=> true,
						"label"		=> _("Slug (url amigable)"),
						"data"		=> [
							"slug"	=> "title_en"
						],
					],
					[
						"id"		=> "intro_en",
						"type"		=> "textarea",
						"db"		=> "intro_2",
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Introducción"),
					],
					[
						"id"		=> "content_en",
						"type"		=> "ckeditor",
						"db"		=> "content_2",
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Contenido")
					],
					// FIN IDIOMAS
					[
						"id"		=> "lang_fin",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => " ",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					// SEPARADOR
					[
						"id"		=> "category",
						"type"		=> "select-chosen",
						"db"		=> "category_id",
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Categoría"),
						"options"	=> [
							"empty"	=> "",
							"values" => $arr_categories
						],
						"class"		=> "chosen"
					],
					[
						"id"		=> "tags",
						"type"		=> "tags",
						"db"		=> "tags",
						"div"		=> true,
						"label"		=> _("Tags"),
						"options"	=> [
							"values" => $modClass->getList("Tags", ["id", "text_1"], null, ["text_1"], null, null, "FETCH_NUM")
						],
						"shown"		=> [
							"required"	=> ["id"],
							"transform" => function($p) {
								if (!empty($p["id"])) {
									global $modClass;
									//var_dump($modClass->getRelTags($p["id"])); exit;
									return $modClass->getRelTags($p["id"]);
								}
								else return [];
							},
						],
						"save"		=> [
							"after"			=> true,
							"transform"		=> function($post_id) {
								if (!empty($post_id)) {
									$new_tags = !empty($_POST["tags"]) ? $_POST["tags"] : [];
									sort($new_tags);

									global $modClass;
									$current_tags = $modClass->getRelTags($post_id, ["tag_id"]);

									//var_dump($new_tags); exit;
									if ($current_tags != $new_tags) {
										$modClass->emptyRelTags($post_id);

										foreach ($new_tags as $tag_id) {
											$modClass->setRelTag($tag_id, $post_id);
										}

										return [
											"result"	=> 1,
											"message"	=> ["ok", _("Nuevos tags insertados correctamente.")]
										];
									}
									else {
										return [
											"result"	=> 0,
											"message"	=> ["warning", _("No se han modificado los tags ya que los datos enviados eran iguales a los existentes")]
										];
									}
								}
								else {
									return [
										"result"	=> -1,
										"message"	=> ["error", _("Se ha producido un error en el guardado de tags.")]
									];
								}
							}
						]
					],
					[
						"id"		=> "publishing",
						"type"		=> "datetime-local",
						"db"		=> "publishing_date",
						"required"	=> true,
						"checks"	=> ["datetime"],
						"div"		=> true,
						"label"		=> _("Fecha de publicación"),
					],
					[
						"id"		=> "img",
						"type"		=> "input-image",
						"db"		=> "",
						"required"	=> true,
						"checks"	=> [],
						"div"		=> true,
						"div_class"	=> "input-image",
						"label"		=> _("Imagen"),
					],
					[
						"id"		=> "pdf",
						"type"		=> "input-file",
						"db"		=> "",
						"checks"	=> [],
						"div"		=> true,
						"div_class"	=> "input-file",
						"label"		=> _("Archivo"),
					],
					/*
					[
						"id"		=> "galery",
						"type"		=> "array",
						"db"		=> "galery",
						"legend"	=> _("Galería"),
						"childs"	=> [
							[
								"id"		=> "galery-title",
								"type"		=> "text",
								"act"		=> "galeria",
								"db"		=> "titulo",
								"length"	=> 255,
								"div"		=> true,
								"label"		=> _("Título"),
							],
							[
								"id"		=> "galery-alt",
								"type"		=> "text",
								"act"		=> "galeria",
								"db"		=> "alt",
								"length"	=> 255,
								"div"		=> true,
								"label"		=> _("Alt"),
							],
							[
								"id"		=> "galery-img",
								"type"		=> "input-image",
								"act"		=> "galeria",
								"db"		=> "",
								"required"	=> true,
								"checks"	=> [],
								"div"		=> true,
								"div_class"	=> "input-image",
								"label"		=> _("Imagen"),
							],
						],
						"two-cols"	=> true,
					],
					*/
				],
				"hidden" => []
			];

			$buttons = [
				//"new"	=> "Nuevo Post",
				"back"	=> _("Volver")
			];
		break;
		default: /// Opciones para el listado
			$titulo = "Listado de noticias";

			$list = [
				"id" => "post",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "title",
						"db"	=> "title_1",
						"title"	=> _("Título"),
					],
					[
						"id"		=> "date",
						"db"		=> "date",
						"title"		=> _("Fecha"),
						"shown"		=> function($val) { return date("d/m/Y", strtotime($val)); },
						"options"	=> [
							"type"	=> "date-eu"
						]
					],
					/*
					[
						"id"	=> "comments",
						"db"	=> "id",
						"title"	=> _("Comentarios"),
						"shown"	=> function($val) {
							global $modClass;
							return count($modClass->getCommentsByPost($val));
						},
					],
					*/
				],
				"sort" => [
					["date", "DESC"],
					["title_1", "ASC"]
				],
				"actions" => [
					/*
					"archivos" => [
						"action"		=> "noticias/archivos/",
						"class" 		=> "fa fa-file-o",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Arhivos"),
					],
					*/
					"editor" => [
						"form-bt"		=> _("Nueva noticia"),
						"class" 		=> "fa fa-pencil",
						"text"			=> _("Editar"),
					],
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			if (empty($opt)) $opt = "List";
		break;
	}

	//echo __DIR__; exit;
	
	require "./_act/Base/Opt/$opt.php";