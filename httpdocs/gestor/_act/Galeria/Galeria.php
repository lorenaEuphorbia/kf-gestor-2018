<?php

	$arr_categories = [];
	$tmp_categories = $modClass->getList("Categories", ["id", "nombre"], null, ["nombre"]);
	foreach ($tmp_categories as $cat) $arr_categories[$cat["id"]] = $cat["nombre"];

	switch ($opt) {
		case 'Editor':
			$titulo = ["Nueva foto", "Editando foto"];

			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "hidden",
						"db"		=> "id",
						"readonly"	=> true,
					],
					[
						"id"		=> "nombre",
						"type"		=> "text",
						"db"		=> "nombre",
						"length"	=> 255,
						"required"	=> true,
						"unique"	=> true,
						"div"		=> true,
						"label"		=> _("Título"),
					],
					[
						"id"		=> "descripcion",
						"type"		=> "textarea",
						"db"		=> "descripcion",
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Descripción"),
					],
					[
						"id"		=> "categoria",
						"type"		=> "select-chosen",
						"db"		=> "id_categoria",
						"div"		=> true,
						"required"	=> true,
						"label"		=> _("Categoría"),
						"options"	=> [
							"empty"	=> "",
							"values" => $arr_categories
						],
						"class"		=> "chosen"
					],
					[
						"id"		=> "img",
						"type"		=> "input-image",
						"db"		=> "",
						"required"	=> true,
						"checks"	=> [],
						"div"		=> true,
						"div_class"	=> "input-image",
						"label"		=> _("Imagen"),
					],
					[
						"id"		=> "alt",
						"type"		=> "text",
						"db"		=> "alt",
						"required"	=> true,
						"div"		=> true,
						"label"		=> _("Alt de Foto"),
					],
				],
				"hidden" => []
			];

			$buttons = [
				//"new"	=> "Nuevo Post",
				"back"	=> _("Volver")
			];
		break;
		default: /// Opciones para el listado
			$titulo = "Listado de fotos";

			$list = [
				"id" => "post",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "title",
						"db"	=> "nombre",
						"title"	=> _("Título"),
					],
					[
						"id"	=> "categoria",
						"db"	=> "id_categoria",
						"title"	=> _("Categoría"),
						"shown"	=> function($val) {  global $arr_categories; return isset($arr_categories[$val])? $arr_categories[$val]: ""; },
					],
				],
				"sort" => [
					["nombre", "ASC"],
				],
				"actions" => [
					"editor" => [
						"form-bt"		=> _("Nueva foto"),
						"class" 		=> "fa fa-pencil",
						"text"			=> _("Editar"),
					],
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			if (empty($opt)) $opt = "List";
		break;
	}

	//echo __DIR__; exit;
	
	require "./_act/Base/Opt/$opt.php";