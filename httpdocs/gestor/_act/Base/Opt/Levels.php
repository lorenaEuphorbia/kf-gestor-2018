<?php
	//AdminPreferences::addCssJs($css, $base_path."js/vendors/jquery-ui-1.11.4/jquery-ui.min.css");
	AdminPreferences::addCssJs($js, $base_path."js/vendors/jquery-ui-1.11.4/jquery-ui.min.js");
	AdminPreferences::addCssJs($js, $base_path."js/vendors/jquery.mjs.nestedSortable.js");

	require $_SERVER['DOCUMENT_ROOT'].'/'.ADMIN_FOLDER."/_inc/theme/page-in-top.php";
	
?>	
				<h1 class="sectionTitle"><?php echo !empty($titulo) ? $titulo : _("Listado"); ?></h1>
				<div class="sortable-list sortable-menu" data-mod="<?php echo lcfirst(get_class($modClass)); ?>" data-act="<?php echo lcfirst($act); ?>" data-levels="<?= isset($levels) ? $levels : '5' ?>">
<?php
	$id_listado = "sortable-menu";

	if (!empty($arr_buttons)) {
		echo CreateList($arr_buttons, $id_listado);
	}
	else {
		echo '<ul id="'.$id_listado.'"></ul>';
	}
?>
				</div>
				<div class="sortable-list-buttons">
					<button id="bt-new">Añadir Nuevo</button>
				</div>
				<script>$(SortableMenu);</script>
<?php
	function CreateList($arrButtons, $id = "sortable-list", $level = 0) {
		$html = "<ul" . ($level == 0 ? ' id="'.$id.'" ' : "") . ">";

		foreach ($arrButtons as $bt) {
			$html .= '<li id="'.$id.'-'.$bt["id"].'" data-id="'.$bt["id"].'">';
			$html .= '<i class="move fa fa-arrows"></i>';
			$html .= $bt["nombre"];
			$html .= '<i class="delete fa fa-trash"></i>';
			$html .= '<i class="edit fa fa-pencil"></i>';

			if (!empty($bt["submenu"])) {
				$html .= CreateList($bt["submenu"], $id, $level + 1);
			}

			$html .= '</li>';
		}

		$html .= "</ul>";

		return $html;
	}

	require $_SERVER['DOCUMENT_ROOT'].'/'.ADMIN_FOLDER."/_inc/theme/page-in-bottom.php";