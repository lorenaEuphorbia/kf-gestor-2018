<?php
/// Parámetros obligatorios
if(!isset($id)) {
	$id = isset($_GET["id"]) ? $_GET["id"] : (isset($_POST["id"]) ? $_POST["id"] : "");
	$id = preg_replace("/[^\d]/", "", $id);
}

//echo "<pre>"; print_r($_POST); exit;

/// Iniciamos variables necesarias
$messages = [];

/// Guardado
if (isset($_POST['SaveData'])) require "EditorSave.php";


if ($id != "") {
	if (!$modClass->searchById($id, $act)) {
		$messages[] = ["error", _("Registro no encontrado")];
		$fields = [];
	}

	# RECOGEMOS DATOS

	foreach ($fields["db"] as $field) {
		if (!empty($field['db'])) {
			if ($field['db'] !== $modClass->getTable($act)->getTableKey()) {
				$lastValue = $modClass->getCurrentFieldValue($field['db'], $act);

				if (!empty($field['unique']) && !!$field['unique']) {

					$contador = 0;
					do { 
						$contador++;
						$newValue = is_numeric($lastValue)? $lastValue + $contador: "$lastValue-$contador";
					}
					while ($modClass->getCount($act, [$field['db']], [$field['db'] => $newValue]));
					$lastValue = $newValue;
				}
				
				$_POST[$field['db']] = $lastValue;
			}
		}
	}

}
$id = $_POST['id'] = $_GET['id'] = '';


require "EditorSave.php";
$uri =  '/' . ADMIN_FOLDER . '/' . lcfirst($mod) . '/' . lcfirst($act) . '/editor/id=' . $id;
header('Location: ' . $uri);
exit;