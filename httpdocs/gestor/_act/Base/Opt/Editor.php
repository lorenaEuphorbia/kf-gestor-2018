<?php
/// Parámetros obligatorios
if(!isset($id)) {
	$id = isset($_GET["id"]) ? $_GET["id"] : (isset($_POST["id"]) ? $_POST["id"] : "");
	$id = preg_replace("/[^\d]/", "", $id);
}

//echo "<pre>"; print_r($_POST); exit;

/// Iniciamos variables necesarias
$messages = [];

/// Guardado
if (isset($_POST['SaveData'])) require "EditorSave.php";

/// Datos para la vista
$titulo = $titulo[$id != ""];

if ($id != "") {
	if (!$modClass->searchById($id, $act)) {
		$messages[] = ["error", _("Registro no encontrado")];
		$fields = [];
	}
}
else {
	require "EditorSave.php";
	$uri =  '/' . ADMIN_FOLDER . '/' . lcfirst($mod) . '/' . lcfirst($act) . '/editor/id=' . $id;
	header('Location: ' . $uri);
	exit;
}
$arr_fields = [];

if (!empty($fields["db"])) {
	foreach ($fields["db"] as $field) {
		if ($field["type"] == "input-image" || $field["type"] == "input-file") {
			if ($id != "") {
				//var_dump($id, $field["id"], $field["propietario"], $modClass->files->searchByOwner($id, $field["id"], $act)); exit;
				if ($modClass->files->searchByOwner($id, $field["id"], $act)) {
					$field["val"] = [
						$modClass->files->getFileData("id"),
						$modClass->files->getModuleName(),
						$modClass->files->getFileData("type"),
						$modClass->files->getFilePath()
					];
				}
			}
			else {
				$field["val"] = [];
			}
		}
		else if ($field["type"] == 'array') {
			if ($id != "") {

				$actTemp = !empty($field['act'])? lcfirst($field['act']): $act;
				$modTemp = !empty($field['mod'])? lcfirst($field['mod']): $mod;
				$modClassTemp = $modTemp === $mod? $modClass: new $modTemp($b->db);
				$arrFilterTemp = !empty($field['filter']) ? $field['filter']: [];
				$arrFieldsTemp = array_column($field['childs'], 'db');
				$intSearchTemp = array_search('' ,$arrFieldsTemp);
				$idKeyTemp = $modClassTemp->getTable($act)->getTableKey();

				if ($intSearchTemp !== false) unset($arrFieldsTemp[$intSearchTemp]);

				if (!empty($field['on_fields'])) {
					foreach ($field['on_fields'] as $idOrigen => $idDestino) {
						$arrFilterTemp[$idDestino] = $modClass->getCurrentFieldValue($idOrigen, $act);
					}
				}


				$arrResultTemp = $modClassTemp->getList($actTemp, $arrFieldsTemp, $arrFilterTemp, ['position DESC']);
				var_dump($arrResultTemp);
				//$arrResultTemp = $modClassTemp->getList($actTemp, $arrFieldsTemp, $arrFilterTemp, ['position DESC']);
				foreach ($field['childs'] as &$fieldTemp) {

						if ($fieldTemp["type"] == "input-image" || $fieldTemp["type"] == "input-file") {
							$fieldTemp["val"] = [];
							foreach ($arrResultTemp as $idFieldChildTemp) {
								//var_dump($modClassTemp->files->searchByOwner($idFieldChildTemp[$idKeyTemp], $fieldTemp["id"], $actTemp)); exit;
								if ($modClassTemp->files->searchByOwner($idFieldChildTemp[$idKeyTemp], $fieldTemp["id"], $actTemp)) {
									$fieldTemp["val"][] = [
										$modClassTemp->files->getFileData("id"),
										$modClassTemp->files->getModuleName(),
										$modClassTemp->files->getFileData("type"),
										$modClassTemp->files->getFilePath()
									];
								}
								else {
									$fieldTemp["val"][] = false;
								}
							}
						}
						else if (isset($fieldTemp["shown"]) && !empty($fieldTemp["shown"]["transform"])) {
							$fieldTemp["val"] = [];

							foreach ($arrResultTemp as $idFieldChildTemp) {
								$fieldTemp["val"][] = $fieldTemp["shown"]["transform"]($idFieldChildTemp);
							}

						} else if (!isset($fieldTemp["val"]) || $fieldTemp["val"]=="") {
							$fieldTemp["val"] = [];

							if(isset($fieldTemp["db"])) {
								foreach ($arrResultTemp as $idFieldChildTemp) {
									$fieldTemp["val"][] = $idFieldChildTemp[$fieldTemp["db"]];
								}
							}
						}
				}
				$field['array_count'] = count($arrResultTemp);
				//echo "<pre/>"; print_r($field['childs']); exit;

			}
			else {
				$field["val"] = [];
				$field['array_count'] = 0;
			}
		}
		else if (isset($field["shown"]) && !empty($field["shown"]["transform"])) {
			$params = [];

			if (!empty($field["shown"]["required"])) {
				foreach ($field["shown"]["required"] as $el) {
					$params[$el] = $modClass->getCurrentFieldValue($el, $act);
				}
			}
			$field["val"] = $field["shown"]["transform"]($params);

		} else if (!isset($field["val"]) || $field["val"]=="") {
			if(isset($field["db"])) {
				$field["val"] = $modClass->getCurrentFieldValue($field["db"], $act);
			}
		}

		$arr_fields[] = DomElements::getField($field);
	}
}

if (!empty($fields["hidden"])) {
	foreach ($fields["hidden"] as $name => $value) {
		$field = [
			"id"	=> $name,
			"type"	=> "hidden",
			"val"	=> $value,
		];

		$arr_fields[] = DomElements::getField($field);
	}
}

/// Botones del pie
if (!empty($buttons)) {
	$form_buttons = [
		"id"	=> "form-buttons",
		"class"	=> "form-buttons",
		"forms"	=> []
	];

	foreach($buttons as $bt_id => $bt_text) {
		switch ($bt_id) {
			case "new":
				if ($id != "") {
					$form_buttons["forms"][] = [
						"id"		=> $bt_id,
						"class"		=> "inline",
						"button"	=> $bt_text
					];
				}
			break;
			case "back":
				$form_buttons["forms"][] = [
					"id"		=> $bt_id,
					"action"	=> $base_path . strtolower("$mod/$act/list/" . (!empty($parent_id) ? "id=$parent_id/" : "")),
					"class"		=> "hidden",
					"button"	=> $bt_text
				];
			break;
			case "custom":
				$form_buttons["forms"][] = $bt_text;
			break;
			default:
				$form_buttons["forms"][] = [
					"id"		=> $bt_id,
					"action"	=> $base_path . strtolower("$mod/$act/$bt_id/"),
					"class"		=> "hidden",
					"button"	=> $bt_text
				];
			break;
		}
	}

	//var_dump($buttons); exit;

	$bottom_buttons = DomElements::getFormsGroup($form_buttons);
}

/// Archivos css y javascript
if (!empty($fields["db"])) {
	$types = [];

	function getFieldTypesRecursive ($value, &$arrOutdata) {
		$tipo = !empty($value["type"])? $value['type']: '';
        if(!empty($value['childs'])) {
            foreach ($value['childs'] as $child) {
            	$arrOutDataChild= getFieldTypesRecursive($child, $arrOutdata);
            }
        }
        if($tipo && array_search($tipo, $arrOutdata)===false) {
            $arrOutdata[] = $tipo;
        }
	}

	foreach ($fields["db"] as $value) {
		getFieldTypesRecursive ($value, $types);
	}

    // var_dump($types); exit;

	if (in_array("array", $types)) {
		AdminPreferences::addCssJs($js, $base_path."js/vendors/jquery-ui-1.11.4/jquery-ui.min.js");
		AdminPreferences::addCssJs($js, $base_path."js/vendors/jquery.mjs.nestedSortable.js");
	}

	if (in_array("ckeditor", $types)) {
		AdminPreferences::addCssJs($js, $base_path."js/vendors/ckeditor/ckeditor.js");
		AdminPreferences::addCssJs($js, $base_path."js/vendors/ckeditor/adapters/jquery.js");
	}

	if (in_array("date", $types) || in_array("datetime-local", $types)) {
		AdminPreferences::addCssJs($css, $base_path."js/vendors/datetimepicker/jquery.datetimepicker.css");
		AdminPreferences::addCssJs($js, $base_path."js/vendors/datetimepicker/jquery.datetimepicker.js");
	}

	if (in_array("input-image", $types)) {
		AdminPreferences::addCssJs($css, $base_path."js/vendors/fancybox/jquery.fancybox.css");
		AdminPreferences::addCssJs($js, $base_path."js/vendors/fancybox/jquery.fancybox.pack.js");
	}

	if (in_array("tags", $types) || in_array("select-chosen", $types)) {
		AdminPreferences::addCssJs($js, $base_path."js/vendors/chosen/chosen.jquery.min.js");
	}
}

require !empty($view) ? $view : "./_inc/theme/editor.php";
