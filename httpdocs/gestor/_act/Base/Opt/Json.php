<?php

$arr_returned = [];

if (!empty($list)) {
	
	$arr_sort = [];
	foreach ($list["sort"] as $el) $arr_sort[] = implode(" ", $el);

	$arr_records = $modClass->getList(
		$act,
		array_column($list["fields"], "db"),
		!empty($list["filter"]) ? $list["filter"] : null,
		$arr_sort
	);

	//echo "<pre>"; print_r($arr_records); exit;


	foreach ($arr_records as $record) {
		$arr_record = [];

		if (isset($list["multiple_selection"]) && $list["multiple_selection"]) {
			$arr_record[] = "";
		}

		foreach ($list["fields"] as $el) {
			if ( isset($el['values']) ) {
				$record[$el["db"]] = @$el['values'][$record[$el["db"]]];
			}
			$arr_record[] =empty($el["shown"]) ? $record[$el["db"]] : $el["shown"]($record[$el["db"]]);
		}

		if (!empty($list["actions"])) {
			$actions = "";

			foreach ($list["actions"] as $action => $options) {
				if ($action == "delete" && !empty($record["locked"])) {
					$action = "locked";
					$options['class'] .= " locked";
					$options['text'] = "";
				}

				$attributes = ' class="'.$options['class'].'"';
				$attributes .= ' title="'.strtolower($options['text']).'"';

				$actions .= '<button data-action="'.$action.'"'.$attributes.'><span>'.$options['text'].'</span></button> ';
			}

			$arr_record[] = strlen($actions) ? "<div>$actions</div>" : "";
		}

		$arr_returned[] = $arr_record;
	}
}

//echo "<pre>"; print_r($arr_returned); exit;
function utf8ize($d) {
    if (is_array($d)) {
        foreach ($d as $k => $v) {
            $d[$k] = utf8ize($v);
        }
    } else if (is_string ($d)) {
        return utf8_encode($d); 
    }
    return $d;
}

$num_records = count($arr_returned);

/*
echo json_encode([
	"recordsTotal"=>$num_records,
	"recordsFiltered"=>$num_records,
	"data"=>$arr_returned
], JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
*/

function encode($a=false)
  {
    if (is_null($a)) return 'null';
    if ($a === false) return 'false';
    if ($a === true) return 'true';
    if (is_scalar($a))
    {
      if (is_float($a))
      {
        // Always use "." for floats.
        return floatval(str_replace(",", ".", strval($a)));
      }
      if (is_string($a))
      {
        static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
        return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
      }
      else
        return $a;
    }
    $isList = true;
    for ($i = 0, reset($a); $i < count($a); $i++, next($a))
    {
      if (key($a) !== $i)
      {
        $isList = false;
        break;
      }
    }
    $result = array();
    if ($isList)
    {
      foreach ($a as $v) $result[] = encode($v);
      return '[' . join(',', $result) . ']';
    }
    else
    {
      foreach ($a as $k => $v) $result[] = encode($k).':'.encode($v);
      return '{' . join(',', $result) . '}';
    }
  }

//echo print_r($arr_returned); exit;

?>{
	"recordsTotal": <?php echo $num_records; ?>,
	"recordsFiltered": <?php echo $num_records; ?>,
	"data": <?php echo encode($arr_returned); ?>
}