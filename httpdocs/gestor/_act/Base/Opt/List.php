<?php

if (!empty($list)) {
	$id_list = $list["id"];

	$arr_fields = $list["fields"];

	$ajax_url = $base_path . strtolower("$mod/$act".(!empty($url) ? "/$url" : "")."/json/" . (!empty($id) ? "id=$id/" : ""));

	/// Opciones
	$col_multiple_selection = isset($list["multiple_selection"]) && $list["multiple_selection"];
	$col_actions = !empty($list["actions"]) ? count($list["actions"]) : 0;

	/// Orden
	$positions = array_column($list["fields"], "db");

	$order = "";
	foreach ($list["sort"] as $el) {
		$order .= (strlen($order) ? ", " : "") . "[" . (array_search($el[0], $positions) + 1) . ", &quot;" . strtolower($el[1]) . "&quot;]";
	}
	$order = "[$order]";

	/// Botones del pie
	if (!empty($list["actions"])) {
		$form_buttons = [
			"id"	=> "form-buttons",
			"class"	=> "form-buttons",
			"forms"	=> []
		];

		foreach($list["actions"] as $action => $options) {
			$hidden_fields = [
				["id" => "id", "type" => "hidden", "val" => ""]
			];

			if (!empty($options["hidden"])) {
				foreach ($options["hidden"] as $key => $value) {
					$hidden_fields[] = ["id" => $key, "type" => "hidden", "val" => $value];
				}
			}

			//switch ($bt_id) {
				//default:
					$form_buttons["forms"][] = [
						"id"		=> $action,
						"action"	=> $base_path . (!empty($options["action"]) ? $options["action"] : strtolower("$mod/$act/$action/")),
						"data"		=> !empty($options["data"]) ? $options["data"] : [],
						"class"		=> (!empty($options["form-bt-class"]) ? $options["form-bt-class"] : ""),
						"fields"	=> $hidden_fields,
						"button"	=> !empty($options["form-bt"]) ? $options["form-bt"] : $options["text"]
					];
				//break;
			//}
		}
	}

	/// Botones de exportación e impresión
	$extra_buttons = !empty($list["extra-buttons"]) ? $list["extra-buttons"] : "";
}

if (!empty($buttons)) {
	if (empty($form_buttons)) {
		$form_buttons = [
			"id"	=> "form-buttons",
			"class"	=> "form-buttons",
			"forms"	=> []
		];
	}

	foreach ($buttons as $bt => $options) {
		$hidden_fields = [];

		if (!empty($options["hidden"])) {
			foreach ($options["hidden"] as $key => $value) {
				$hidden_fields[] = [
					"id"	=> $key,
					"type"	=> "hidden",
					"val"	=> $value
				];
			}
		}

		$form_buttons["forms"][] = [
			"id"		=> $bt,
			"action"	=> $base_path . (!empty($options["action"]) ? $options["action"] : strtolower("$mod/$act/$bt/")),
			"data"		=> !empty($options["data"]) ? $options["data"] : [],
			"fields"	=> $hidden_fields,
			"button"	=> $options["text"]
		];
	}
}

$bottom_buttons = !empty($form_buttons) ? DomElements::getFormsGroup($form_buttons) : "";

require !empty($view) ? $view : "./_inc/theme/list.php";