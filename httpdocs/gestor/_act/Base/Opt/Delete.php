<?php

$_arrResponse['messages'] = [];
$_arrResponse['status'] = 'error';
$_arrResponse['result'] = 'error';
ob_start();

$id = isset($_POST["id"]) ? $_POST["id"] : (isset($_GET["id"]) ? $_GET["id"] : "");
$id = preg_replace("/[^\d]/", "", $id);
//var_dump($_POST);exit();

if ($id != "" && $modClass->searchById($id, $act)) {
	if ($modClass->deleteRecord($id, $act)) {
		$_arrResponse['status'] = 'success';
		$_arrResponse['result'] = 'ok';
	}
}

$_arrResponse['data'] = ob_get_clean();

header('Content-Type: application/json; charset=UTF-8');
echo json_encode($_arrResponse);
