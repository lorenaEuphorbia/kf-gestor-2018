<?php

  if (!empty($fields["db"])) {

  	$resp = 0;
	$post_saving_errors = [];
	$post_saving_oks = 0;


	function procesarRequestGuardado ($arrFields, $postData, $actTemp, $modTemp, $modClassTemp, $arrFiles) {
		global $b, $resp, $post_saving_errors, $post_saving_oks, $messages;

		$_arrFields = [];
		$_arrErrors = [];
		$_arrPostFields = [];
		$_saveFields = [];
		$_arrMessages = isset($_arrMessages) ? $_arrMessages : [];

		$_keyId = $modClassTemp->getTable($actTemp)->getTableKey();

		$id = isset($postData[$_keyId]) ? $postData[$_keyId] : (isset($_POST[$_keyId]) ? $_POST[$_keyId] : (isset($_GET[$_keyId]) ? $_GET["id"] : ""));
		// $id = preg_replace("/[^\d]/", "", $id);

		// var_dump($field, $actTemp . $modTemp);

		$_isEdit = isset($id) && !empty($id);
		if ($_isEdit){ 
			$modClassTemp->searchById($id, $actTemp);
		}

		foreach ($arrFields as $keyField => $field) {



			$_idField	= isset($field['id']) 	? $field['id']		: false;
			$_type 		= isset($field['type']) ? $field['type']	: false;
			$_dbField 	= isset($field['db']) 	? $field['db']		: false;

			if( $_idField===false || $_type===false || $_dbField===false ) continue;

			$_value 		= isset($postData[$field["id"]]) 	?$postData[$field["id"]] : false;		
			$_readonly 		= isset($field['readonly'])		?$field['readonly']	: false;
			$_required 		= isset($field['required'])		?$field['required']	: false;
			$_disabled 		= isset($field['disabled'])		?$field['disabled']	: false;
			$_disabledValue = isset($field['disabled-value'])?$field['disabled-value']	: false;
			$_default 		= isset($field['default'])		?$field['default']	: false;
			$_unique 		= isset($field['unique'])		?$field['unique']	: false;
			$_fillWith 		= isset($field['fillWith'])		?$field['fillWith']	: "";
			$_lenght 		= isset($field['lenght'])		?$field['lenght']	: 0;
			$_save 			= isset($field['save'])			?$field['save']		: false;
			$_label			= isset($field['label'])		?$field['label']	: $_idField;
			$_checks		= isset($field['checks'])		?$field['checks']	: [];
			$_set			= isset($field['set'])			?$field['set']		: false;
			$_skip			= isset($field['skip'])			?$field['skip']		: false;

			$_readonly	= $_readonly && is_callable($_readonly)	? $_readonly()	: $_readonly===true;
			$_required	= $_required && is_callable($_required)	? $_required()	: $_required===true;
			$_disabled	= $_disabled && is_callable($_disabled)	? $_disabled()	: $_disabled===true;
			$_unique	= $_unique && is_callable($_unique)		? $_unique()	: $_unique===true;
			$_default	= $_default && is_callable($_default)	? $_default()	: $_default;
			$_fillWith	= $_fillWith && is_callable($_fillWith)	? $_fillWith()	: $_fillWith;
			$_lenght	= $_lenght && is_callable($_lenght)		? $_lenght()	: intval($_lenght);
			$_set		= $_set && is_callable($_set)			? $_set()		: $_set;


			# Manipular tipos específicosç
			
			switch ($_type) {
				case 'array': 
					$post_saving_fields[] = $_idField;
					$_disabled = true;
					break;

				case 'input-image': # P: ponerlo ok para las comprobaciones previas
					# P: checks image

					if (!empty($arrFiles[$_idField])) {
						if($arrFiles[$_idField]["error"]===0) {
							# Check image
							if (getimagesize($arrFiles[$_idField]["tmp_name"])) {

								if($arrFiles[$_idField]["size"] < (Utilities::file_upload_max_size_bytes())) { # 5MB

									# si no admite function mimeType
									if(FileTypes::isTipo ($_FILES[$_idField]["tmp_name"], FileTypes::TIPO_IMAGEN)) {
										$post_saving_fields[] = $_idField;
										$uploadOk = 0;

									} else $_arrErrors[] = sprintf(_('El tipo de imagen no es correcto. Formatos soportados: ' . FileTypes::getListToString (FileTypes::TIPO_IMAGEN)), $_label);

								} else $_arrErrors[] = sprintf(_('La imagen es no puede superar los '.Utilities::file_upload_max_size()), $_label);

							} else $_arrErrors[] = sprintf(_('La imagen no es correcta.'), $_label);

						} else if($arrFiles[$_idField]["error"]!=UPLOAD_ERR_NO_FILE) $_arrErrors[] = sprintf(_(Files::codeToMessage($arrFiles[$_idField]["error"])), $_label);
					}
					$_skip = true;
					break;

				case 'input-file': # P: ponerlo ok para las comprobaciones previas
				# P: checks image
				//var_dump($arrFiles); exit;

				if (!empty($arrFiles[$_idField])) {
					if($arrFiles[$_idField]["error"]===0) {
						# Check image
						if (filesize ($arrFiles[$_idField]["tmp_name"])) {

							if($arrFiles[$_idField]["size"] < (Utilities::file_upload_max_size_bytes())) { # 5MB

								# si no admite function mimeType
								if(FileTypes::isTipo ($_FILES[$_idField]["tmp_name"], FileTypes::TIPO_MS | FileTypes::TIPO_ADOBE | FileTypes::TIPO_ARCHIVES | FileTypes::TIPO_OPEN)) {
									$post_saving_fields[] = $_idField;
									$uploadOk = 0;

								} else $_arrErrors[] = sprintf(_('El tipo de archivo no es correcto. Formatos soportados: ' . FileTypes::getListToString (FileTypes::TIPO_TEXTO)), $_label);

							} else $_arrErrors[] = sprintf(_('El archivo no puede superar los '.Utilities::file_upload_max_size()), $_label);

						} else $_arrErrors[] = sprintf(_('El archivo no es correcto.'), $_label);

					}  else if($arrFiles[$_idField]["error"]!=UPLOAD_ERR_NO_FILE) $_arrErrors[] = sprintf(_(Files::codeToMessage($arrFiles[$_idField]["error"])), $_label);
				}
				$_skip = true;
				break;

				case 'checkbox': 
					$_value = $_value==="true"? "1": "0";
					break;
			}

			if ($_skip) {
				continue;
			}


			# Transformación de campo mediante función
			if ($_save && isset($_save["transform"])) {
				if (isset($_save["after"]) && $_save["after"]==true) {
					$post_saving_fields[] = $_idField;
					continue;
				}
				else  {
					$_value = $_save["transform"]();
				}
			}
			
			if($_value!==false) {
				foreach ($_checks as $check) {
					switch ($check) {
						case "date": # Forzamos entrada de fecha
							$_value = date("Y-m-d", strtotime(str_replace("/", "-", $_value)));
						break;
						case "datetime": # Forzamos entrada de tiempo
							$_value = date("Y-m-d H:i:s", strtotime(str_replace("/", "-", $_value)));
						break;
						case "email": # Comprobamos email
							if (!Utilities::checkEmail($_value)) {
								$arrFields[$keyField]["val"] = $_value;
								$_arrErrors[] = sprintf(_('El valor introducido en el campo <em>"%s"</em> no es un e-mail válido.'), $_label);
								continue;
							}
						break;
						case "int-unsigned-empty": # Comprobamos número entero
							if (!(preg_match("/[0-9]/", $_value) && strlen($_value)>0)) {
								$arrFields[$keyField]["val"] = $_value;
								$_arrErrors[] = sprintf(_('El campo <em>"%s"</em> debe ser un número entero positivo.'), $_label);
								continue;
							} 
						break;
						case "slug": # Forzamos slug
							$_value = Utilities::getSlug($_value);
						break;
						case "autoincrement": # Autoincremento si está vacío
							if($_value==="") {
								$result = $modClassTemp->getList($actTemp, [$_dbField], null, ["$_dbField DESC"] );
								$maxResult = 0;
								foreach ($result as $row) { // Bucle por si el tipo es string
									$num = intval($row[$_dbField]);
									if($num > $maxResult) $maxResult = $num;
								}				
								$_value = (string) $maxResult+1;
								//var_dump($_value); exit;
							} 
						break;
						case "number": # Forzamos autoincremento
							$_value = intval($_value)? (string)(intval($_value)): "0";
						break;
						case "decimal": # Forzamos autoincremento
							$_value = floatval($_value)? (string)(floatval($_value)): "0";
						break;
						case "skip": # Saltamos campo
							continue;
						break;
					}
				}
				# Rellenamos según patrón
				if ($_fillWith) $_value = str_pad($_value, strlen($_fillWith), $_fillWith, STR_PAD_LEFT);

				# Comprobamos longitud
				if ($_lenght>0 && strlen($_value)>$_lenght ) {
					$arrFields[$keyField]["val"] = $_value;
					$_arrErrors[] = sprintf(_('El campo <em>"%s"</em> puede tener un máximo de %d caracteres.'), $_label, $_lenght);
					continue;
				}
			}

			# Insertamos el valor forzado
			if($_set!==false) {
				$_value = $_set === true ? $_value : $_set;
				$_arrFields[$field["db"]] = $_value;
				$arrFields[$keyField]["val"] = $_value;
				continue;
			}

			# Insertamos el valor por defecto
			if(!$_isEdit && $_default!==false ) {
				if($_default===true) {
					$_arrFields[$_dbField] = $_value;	
					$arrFields[$keyField]["val"] = $_value;
					continue;			
				}
				else if(empty($_value)) {
					$_arrFields[$_dbField] = $_default;
					$arrFields[$keyField]["val"] = $_default;
					continue;
				}			
			}

			# Añadimos valor disabled
			if($_disabled || $_readonly || $_value===false) {
				if($_disabledValue!==false) {
					$_arrFields[$field["db"]] = $_disabledValue;
					$arrFields[$keyField]["val"] = $_disabledValue;
				}
				continue;
			}

			# Comprobamos que no halla dos iguales
			if ($_unique) {			
				$others = $modClassTemp->getList($actTemp, $_keyId, [$_dbField => $_value]);

				if($others && (sizeof($others)>1 || ((string)$others[0][$_keyId]) != ((string)$id))) {	
					// echo "<pre>"; var_dump(((string)$others[0][$_keyId]) , ((string)$id)); exit;
					$arrFields[$keyField]["val"] = $_value;
					$_arrErrors[] = sprintf(_('Ya existe un registro con el campo <em>"%s"</em> con el valor introducido y no puede estar repetido.'), $_label);
					// $messages[] = ["resp", json_encode(["searchID: ".$others[0][$_keyId],"currID: ".$id])];
					continue; 
				}
			}

			# Comprobamos si está requerido
			if ($_required && empty($_value)) {
				$arrFields[$keyField]["val"] = $_value;
				$_arrErrors[] = sprintf(_('Rellene el campo <em>"%s"</em>.'), $_label);
				continue;
			}

			# Añadimos
			$arrFields[$keyField]["val"] = $_value;
			$_arrFields[$field["db"]] = $_value;

		}

		if (empty($_arrErrors)) {


			if (!empty($postData['position'])) {
				$_arrFields['position'] = $postData['position'];
			}


			if($_isEdit) {

				$resp += $modClassTemp->setRecord(
					$_arrFields, /// Campos a guardar o actualizar
					[$_keyId => $id], /// Búsqueda del elemento a actualizar
					$actTemp /// Grupo, equivalente a la tabla donde se insertará
				);
			}
			else {

				// Comprpbamos la actualización de la página para que no se introduzcan duplicados
				//$result = $modClassTemp->getList($actTemp, null, $_arrFields, null, 0, 1);
				//var_dump($result ); exit;
				//if(!$result) {
					// var_dump($_arrFields);
					$id = $modClassTemp->setRecord(
						$_arrFields, /// Campos a guardar o actualizar
						null, /// Búsqueda del elemento a actualizar
						$actTemp /// Grupo, equivalente a la tabla donde se insertará
					);
					if($id)	$resp++;
				//}
				//else {
				//	$id = reset($result)['id'];
				//	$resp += -1;
				//}
			}

			if ($id == "") $id = $modClassTemp->getLastInsertId($actTemp);
			//echo "<pre>"; var_dump($resp, $id); echo "</pre>"; exit;

			
			//var_dump($post_saving_fields, $_POST); exit;

			if ($resp >= 0 && !empty($post_saving_fields)) {
				// var_dump($arrFields, $post_saving_fields);
				foreach ($arrFields as $field) {
					if(!empty($field["disabled"]) || !empty($field["readonly"])) continue;
			

					if (in_array($field["id"], $post_saving_fields)) {
						if(isset($field["save"]["transform"])) {
							$tmp = $field["save"]["transform"]($id);

							if ($tmp["result"] > 0) $post_saving_oks ++;
							else if ($tmp["result"] < 0) $post_saving_errors[] = $tmp["message"];
						}
						else if ($field["type"] == "input-image" || $field["type"] == "input-file" ) {

							$arrFileTempParams = [
								"type"		 => $field["id"],
								"owner_id"	 => $id,
								"owner_type" => $actTemp
							];

							if (isset($_POST[$field["id"] . "-alt"]) && $modClassTemp->files->searchByOwner($id, $field["id"], $actTemp)) {
								$arrFileTempParams['id'] = $modClassTemp->files->getFileData('id');
							}

							$tmp = $modClassTemp->files->setFile($arrFileTempParams, $arrFiles);

							if ($tmp) {
								$post_saving_oks ++;
							}
						}
						else if ($field["type"] == 'array') {
							if (!empty($postData[$field['id']]))
							foreach ($postData[$field['id']] as $index => $objRequestChild) {

								$arrChildFiles = [];
								if (!empty($arrFiles[$field['id']])) {
									foreach ($arrFiles[$field['id']] as $key => $arrChilds) {
										if (!empty($arrChilds[$index]))
										foreach ($arrChilds[$index] as $keyChild => $value) {
											$arrChildFiles[$keyChild][$key] = $value;
										}
									}
								}

								$strActParam = !empty($field['act'])? lcfirst($field['act']): $actTemp;
								$strModParam = !empty($field['mod'])? lcfirst($field['mod']): $modTemp;
								$objModParam = $strModParam === $modTemp? $modClassTemp: new $strModParam($b->db);
								procesarRequestGuardado($field["childs"], $objRequestChild, $strActParam, $strModParam, $objModParam, $arrChildFiles);
							}
						}
					}
				}
			}
			else {
				//$messages[] = ["error", _("Los datos de entrada son erroneos.")];
				//$messages[] = ["resp",json_encode($resp)];
			}

		}
		else {
			$tmp = "";
			foreach ($_arrErrors as $error) {
				$tmp .= "<li>" . $error . "</li>";
			}

			$messages[] = ["error", "Corrija los siguientes errores, por favor:<ul>$tmp</ul>"];
		}

		return $id;
	}
	$id = procesarRequestGuardado($fields['db'], $_POST, $act, $mod, $modClass, $_FILES);

	// var_dump($resp);

	if ($resp || $post_saving_oks) {
		if (empty($post_saving_errors)) {
			$messages[] = ["ok", $id != "" ? _("Los datos se han modificado correctamente.") : _("Los datos se han guardado correctamente.")];
		}
		else {
			$messages[] = ["warning", _("Los datos introducidos se han guardado correctamente con las siguientes salvedades:")];
			$messages = array_merge($messages, $post_saving_errors);
		}
	}
	else {
		if (empty($post_saving_errors)) {
			if ($resp === 0) {
				$messages[] = ["warning", _("No se ha realizado ninguna modificación ya que los datos enviados eran iguales a los existentes.")];
			}
			else {
				$messages[] = ["error", _("Se ha producido un error en el guardado.")];
			}
		}
		else {
			$messages = array_merge($messages, $post_saving_errors);
		}
	}
}