<?php

$messages = [];

if (!empty($options)) {
	$id_list = $options["id"];

	/// Resetear orden
	if (isset($_POST['reset']))	{
		$arr_records = $modClass->getList(
			$act,
			["id"],
			!empty($options["filter"]) ? $options["filter"] : null
		);

		//echo "<pre>"; print_r($arr_records); exit;
		foreach ($arr_records as $record) {
			$modClass->setRecord(
				["id" => $record["id"], "position" => "0"], /// Campos a guardar o actualizar
				["id" => $record["id"]], /// Búsqueda del elemento a actualizar
				$act /// Grupo, equivalente a la tabla donde se insertará
			);
		}
	}

	/// Listado de registros
	$arr_sort = [];
	foreach ($options["sort"] as $el) $arr_sort[] = implode(" ", $el);

	$arr_records = $modClass->getList(
		$act,
		array_merge(["id", "position"], $options["fields"]),
		!empty($options["filter"]) ? $options["filter"] : null,
		$arr_sort
	);

	if (!empty($arr_records)) {
		$list = [];

		foreach ($arr_records as $record) {
			$list[] = [
				"id"	=> $record["id"],
				"text"	=> $options["shown"]($record)
			];
		}
	}
	else {
		$messages[] = ["error", _("No hay registros disponibles para ordenar.")];
	}
}

if (!empty($buttons)) {
	if (empty($form_buttons)) {
		$form_buttons = [
			"id"	=> "form-buttons",
			"class"	=> "form-buttons",
			"forms"	=> []
		];
	}

	foreach ($buttons as $bt => $options) {
		$hidden_fields = [];

		if (!empty($options["hidden"])) {
			foreach ($options["hidden"] as $key => $value) {
				$hidden_fields[] = [
					"id"	=> $key,
					"type"	=> "hidden",
					"val"	=> $value
				];
			}
		}

		$form_buttons["forms"][] = [
			"id"		=> $bt,
			"action"	=> $base_path . (!empty($options["action"]) ? $options["action"] : strtolower("$mod/$act/$bt/")),
			"class"		=> !empty($options["class"]) ? $options["class"] : "",
			"fields"	=> $hidden_fields,
			"button"	=> $options["text"]
		];
	}
}

$bottom_buttons = !empty($form_buttons) ? DomElements::getFormsGroup($form_buttons) : "";

require !empty($view) ? $view : "./_inc/theme/sort.php";