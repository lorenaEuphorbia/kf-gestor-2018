<?php

if (!empty($_POST['username'])) {
	$username = preg_replace("/[^a-zA-Z0-9_\-@.]+/", "", $_POST['username']);


	if ( true /*Mandar correo */) {
		$message = _("Mensaje enviado.");
	}
	else {
		$message = _("Nombre de usuario o contraseña incorrectos.");		
	}
}

/// Redirigimos la raíz del gestor para que se carguen las preferencias del usuario logueado
if ($b->security->hasPermission(["administrator","master","root"])) {
	header("Location: $base_path");
}

require "./_inc/theme/recover.php";