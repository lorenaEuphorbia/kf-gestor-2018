<?php

	$titulo = "Textos";

	$fields = [
		[
			"id"		=> "lang_es",
			"type"		=> "title",
			"db"		=> "",
			"options"	=> [
				"text" => "KLICK BODAS",
				"type" => "h4",
				"class" => "sectionSubTitle"
			],
			"label"		=> _(" "),
		],
		[
			"id"		=> "apartado_boda_home",
			"type"		=> "ckeditor",
			"db"		=> "apartado_boda_home",
			"required"	=> true,
			"div"		=> true,
			"label"		=> _("Texto home"),
		],
	];

	require "./_act/Preferences/Opt/Editor.php";
