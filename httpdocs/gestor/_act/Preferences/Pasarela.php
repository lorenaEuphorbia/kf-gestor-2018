<?php

	$titulo = "Pasarela por banco";

	$fields = [
		[
			"id"		=> "pasarela_is_real",
			"type"		=> "select",
			"db"		=> "pasarela_is_real",
			"div"		=> true,
			"label"		=> _("Entorno Real"),
			"options"	=> [
				"values" => [
					"0"	 => _("No"),
					"1"  => _("Si")
				]
			]
		],
		[
			"id"		=> "pasarela_url_ok",
			"type"		=> "text",
			"db"		=> "pasarela_url_ok",
			"required"	=> true,
			"length"	=> 250 - strlen("http://$_SERVER[HTTP_HOST]/"),
			"div"		=> true,
			"default" 	=> "banco-respuesta",
			"placeholder" => "banco-respuesta",
			"label"		=> _("URL OK (*)"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "pasarela_url_ko",
			"type"		=> "text",
			"db"		=> "pasarela_url_ko",
			"required"	=> true,
			"length"	=> 250 - strlen("http://$_SERVER[HTTP_HOST]/"),
			"div"		=> true,
			"default" 	=> "banco-no-respuesta",
			"placeholder" => "banco-no-respuesta",
			"label"		=> _("URL KO (*)"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "pasarela_codigo_titular",
			"type"		=> "text",
			"db"		=> "pasarela_codigo_titular",
			"required"	=> false,
			"length"	=> 9,
			"div"		=> true,
			"placeholder" => "123456789",
			"label"		=> _("Código Titular"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "pasarela_codigo_terminal",
			"type"		=> "text",
			"db"		=> "pasarela_codigo_terminal",
			"default"	=> "001",
			"required"	=> false,
			"length"	=> 3,
			"div"		=> true,
			"placeholder" => "001",
			"label"		=> _("Código Terminal"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "pasarela_clave_secreta",
			"type"		=> "text",
			"db"		=> "pasarela_clave_secreta",
			"required"	=> false,
			"length"	=> 32,
			"div"		=> true,
			"placeholder" => "sq7HjrUOBfKmC576ILgskD5srU870gJ7",
			"label"		=> _("Clave secreta"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "pasarela_nombre_titular",
			"type"		=> "text",
			"db"		=> "pasarela_nombre_titular",
			"required"	=> false,
			"length"	=> 60,
			"div"		=> true,
			"placeholder" => "Nombre del titular",
			"label"		=> _("Nombre del titular"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "pasarela_moneda",
			"type"		=> "select-chosen",
			"db"		=> "pasarela_moneda",
			"required"	=> true,
			"div"		=> true,
			"label"		=> _("Moneda"),
			"options"	=> [
				"values" => array_flip (Pasarela::MONEDAS)
			],
			"class"		=> "chosen"
		],
		[
			"id"		=> "pasarela_url_formulario",
			"type"		=> "text",
			"db"		=> "pasarela_url_formulario",
			"default"	=> "https://sis.redsys.es/sis/realizarPago",
			"required"	=> false,
			"length"	=> 250,
			"div"		=> true,
			"placeholder" => "https://sis.redsys.es/sis/realizarPago",
			"label"		=> _("URL del Formulario"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "pasarela_url_operaciones",
			"type"		=> "text",
			"db"		=> "pasarela_url_operaciones",
			"default"	=> "https://sis.redsys.es/sis/operaciones",
			"required"	=> false,
			"length"	=> 250,
			"div"		=> true,
			"placeholder" => "https://sis.redsys.es/sis/operaciones",
			"label"		=> _("URL del Envío XML"),
			"class"		=> "char-counter"
		],
	];
	
	require "./_act/Preferences/Opt/Editor.php";