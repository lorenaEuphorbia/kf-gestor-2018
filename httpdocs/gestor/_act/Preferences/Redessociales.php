<?php

	$titulo = "Redes sociales";

	$fields = [
		[
			"id"		=> "facebook",
			"type"		=> "text",
			"db"		=> "contacto_facebook",
			"required"	=> false,
			"length"	=> 100,
			"div"		=> true,
			"label"		=> _("Usuario Facebook"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "twitter",
			"type"		=> "text",
			"db"		=> "contacto_twitter",
			"required"	=> false,
			"length"	=> 100,
			"div"		=> true,
			"label"		=> _("Usuario Twitter"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "instagram",
			"type"		=> "text",
			"db"		=> "contacto_instagram",
			"required"	=> false,
			"length"	=> 100,
			"div"		=> true,
			"label"		=> _("Usuario Instagram"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "youtube",
			"type"		=> "text",
			"db"		=> "contacto_youtube",
			"required"	=> false,
			"length"	=> 100,
			"div"		=> true,
			"label"		=> _("Dirección de youtube"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "googleplus",
			"type"		=> "text",
			"db"		=> "contacto_googleplus",
			"required"	=> false,
			"length"	=> 100,
			"div"		=> true,
			"label"		=> _("Dirección de Google+"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "linkedin",
			"type"		=> "text",
			"db"		=> "contacto_linkedin",
			"required"	=> false,
			"length"	=> 100,
			"div"		=> true,
			"label"		=> _("Dirección de LinKedin"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "pintrest",
			"type"		=> "text",
			"db"		=> "contacto_pintrest",
			"required"	=> false,
			"length"	=> 100,
			"div"		=> true,
			"label"		=> _("Dirección de Pintrest"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "whatsapp",
			"type"		=> "text",
			"db"		=> "contacto_whatsapp",
			"required"	=> false,
			"length"	=> 100,
			"div"		=> true,
			"label"		=> _("Número de WhatsApp"),
			"class"		=> "char-counter"
		],
	];

	require "Opt/Editor.php";