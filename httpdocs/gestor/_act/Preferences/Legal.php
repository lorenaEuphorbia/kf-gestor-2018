<?php

	$titulo = "Textos legales";

	$fields = [
		[
			"id"		=> "aviso_legal",
			"type"		=> "ckeditor",
			"db"		=> "legal_aviso_legal",
			"required"	=> true,
			"div"		=> true,
			"label"		=> _("Aviso legal"),
		],
		[
			"id"		=> "politica_de_privacidad",
			"type"		=> "ckeditor",
			"db"		=> "legal_politica_de_privacidad",
			"required"	=> true,
			"div"		=> true,
			"label"		=> _("Política de privacidad"),
		],
		[
			"id"		=> "politica_de_cookies",
			"type"		=> "ckeditor",
			"db"		=> "legal_politica_de_cookies",
			"required"	=> true,
			"div"		=> true,
			"label"		=> _("Política de cookies"),
		],
	];
	
	require "./_act/Preferences/Opt/Editor.php";