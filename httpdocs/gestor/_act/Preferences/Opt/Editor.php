<?php

/// Iniciamos variables necesarias
$messages = [];

/// Guardado
if (isset($_POST['SaveData']) && !empty($fields)) {
	//echo "<pre>"; print_r($_POST); exit;

	$arr_fields = [];
	$arr_errors = [];
	$post_saving_fields = [];

	foreach ($fields as $field) {
		$val = !empty($_POST[$field["id"]]) ? $_POST[$field["id"]] : "";

		if (isset($field["save"]) && !empty($field["save"]["after"]) && isset($field["save"]["transform"])) {
			$post_saving_fields[] = $field["id"];
		}

		/// Comprobaciones y pre-procesado de datos
		if (isset($field["required"]) && $field["required"] && $val == "") {
			$arr_errors[] = sprintf(_('Rellene el campo <em>"%s"</em>.'), (!empty($field["label"]) ? $field["label"] : $field["id"]));
		}
		else if (isset($field["length"]) && strlen($val) > $field["length"]) {
			$arr_errors[] = sprintf(_('El campo <em>"%s"</em> puede tener un máximo de %d caracteres.'), (!empty($field["label"]) ? $field["label"] : $field["id"]), $field["length"]);
		}
		else if (!empty($field["checks"])) {
			foreach ($field["checks"] as $check) {
				switch ($check) {
					case "date":
						$arr_fields[$field["db"]] = date("Y-m-d", strtotime(str_replace("/", "-", $val)));
					break;
					case "datetime":
						$arr_fields[$field["db"]] = date("Y-m-d H:i:s", strtotime(str_replace("/", "-", $val)));
					break;
					case "email":
						if (!Utilities::checkEmail($val)) {
							$arr_errors[] = sprintf(_('El valor introducido en el campo <em>"%s"</em> no es un e-mail válido.'),  (!empty($field["label"]) ? $field["label"] : $field["id"]));
						}
					break;
					case "int-unsigned-empty":
						if (!($val == "" || (preg_match("/[0-9]/", $val) && $val >= 0))) {
							$arr_errors[] = sprintf(_('El campo <em>"%s"</em> debe ser un número entero positivo.'),  (!empty($field["label"]) ? $field["label"] : $field["id"]));
						}
					break;
					case "slug":
						$arr_fields[$field["db"]] = Utilities::getSlug($val);
					break;
				}
			}
		}
	}

	if (empty($arr_errors)) {
		$oks = 0;
		$warnings = 0;
		$errors = 0;

		foreach ($fields as $field) {
			if (!empty($field["db"])) {
				$resp = $modClass->setValue($field["db"], $_POST[$field["id"]]);

				if ($resp) $oks ++;
				else if ($resp === 0) $warnings ++;
				else {
					$errors ++;
					$messages[] = ["error", sprintf(_("Se ha producido un error al guardar el campo %s."), $field["label"])];
				}
			}
		}

		$post_saving_errors = [];
		$post_saving_oks = 0;

		if (!$errors && !empty($post_saving_fields)) {
			foreach ($fields as $field) {
				if (in_array($field["id"], $post_saving_fields)) {
					$tmp = $field["save"]["transform"]();

					if ($tmp["result"] > 0) $post_saving_oks ++;
					else if ($tmp["result"] < 0) {
						$errors ++;
						$post_saving_errors[] = $tmp["message"];
					}
				}
			}
		}

		if (!$errors) {
			if ($oks || $post_saving_oks) {
				$messages[] = ["ok", _("Los datos se han modificado correctamente.")];
			}
			else {
				$messages[] = ["warning", _("No se ha realizado ninguna modificación ya que los datos enviados eran iguales a los existentes.")];
			}
		}
	}
	else {
		$tmp = "";
		foreach ($arr_errors as $error) {
			$tmp .= "<li>" . $error . "</li>";
		}

		$messages[] = ["error", "Corrija los siguientes errores, por favor:<ul>$tmp</ul>"];
	}
}

/// Datos para la vista
$arr_fields = [];

if (!empty($fields)) {
	foreach ($fields as $field) {
		//if (!empty($field["db"])) $field["val"] = $modClass->getValue($field["db"]);
		if ($field["type"] == "input-image" || $field["type"] == "input-file") {

				//var_dump($modClass->files->searchByOwner($id, $field["id"])); exit;
			if ($modClass->files->searchByOwner($id, $field["id"], $act)) {
				$field["val"] = [
					$modClass->files->getFileData("id"),
					$modClass->files->getModuleName(),
					$modClass->files->getFileData("type"),
					$modClass->files->getFilePath()
				];
			}
			else {
				$field["val"] = [];
			}
		}
		else if (isset($field["shown"]) && !empty($field["shown"]["transform"])) {
			$params = [];

			if (!empty($field["shown"]["required"])) {
				foreach ($field["shown"]["required"] as $el) {
					$params[$el] = $modClass->getCurrentFieldValue($el, $act);
				}
			}
			$field["val"] = $field["shown"]["transform"]($params);

		} else if (!empty($field["db"])) $field["val"] = $modClass->getValue($field["db"]);

		$arr_fields[] = DomElements::getField($field);
	}
}

/// Botones del pie
if (!empty($buttons)) {
	$form_buttons = [
		"id"	=> "form-buttons",
		"class"	=> "form-buttons",
		"forms"	=> []
	];

	foreach($buttons as $bt_id => $bt_text) {
		switch ($bt_id) {
			case "new":
				if ($id != "") {
					$form_buttons["forms"][] = [
						"id"		=> $bt_id,
						"class"		=> "inline",
						"button"	=> $bt_text
					];
				}
			break;
			case "back":
				$form_buttons["forms"][] = [
					"id"		=> $bt_id,
					"action"	=> $base_path . strtolower("$mod/$act/list/"),
					"class"		=> "inline",
					"button"	=> $bt_text
				];
			break;
		}
	}
}

$bottom_buttons = !empty($form_buttons) ? DomElements::getFormsGroup($form_buttons) : "";

/// Archivos css y javascript
if (!empty($fields)) {
	$types = array_column($fields, "type");

	if (in_array("ckeditor", $types)) {
		AdminPreferences::addCssJs($js, $base_path."js/vendors/ckeditor/ckeditor.js");
		AdminPreferences::addCssJs($js, $base_path."js/vendors/ckeditor/adapters/jquery.js");
	}

	if (in_array("date", $types) || in_array("datetime-local", $types)) {
		AdminPreferences::addCssJs($css, $base_path."js/vendors/datetimepicker/jquery.datetimepicker.css");
		AdminPreferences::addCssJs($js, $base_path."js/vendors/datetimepicker/jquery.datetimepicker.js");
	}

	if (in_array("tags", $types) || in_array("select-chosen", $types)) {
		AdminPreferences::addCssJs($js, $base_path."js/vendors/chosen/chosen.jquery.min.js");
	}
}

require "./_inc/theme/editor.php";
