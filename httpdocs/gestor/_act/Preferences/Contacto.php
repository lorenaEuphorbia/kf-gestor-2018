<?php

	$titulo = "Datos de contacto";

	$fields = [
		[
			"id"		=> "nombre",
			"type"		=> "text",
			"db"		=> "contacto_nombre",
			"required"	=> true,
			"length"	=> 50,
			"div"		=> true,
			"label"		=> _("Nombre"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "direccion",
			"type"		=> "ckeditor",
			"db"		=> "contacto_direccion",
			"required"	=> false,
			"length"	=> 1000,
			"div"		=> true,
			"label"		=> _("Dirección de contacto"),
		],
		[
			"id"		=> "correo",
			"type"		=> "text",
			"db"		=> "contacto_email",
			"required"	=> true,
			"length"	=> 100,
			"div"		=> true,
			"label"		=> _("Email"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "facebook",
			"type"		=> "text",
			"db"		=> "contacto_facebook",
			"required"	=> false,
			"length"	=> 100,
			"div"		=> true,
			"label"		=> _("Usuario Facebook"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "twitter",
			"type"		=> "text",
			"db"		=> "contacto_twitter",
			"required"	=> false,
			"length"	=> 100,
			"div"		=> true,
			"label"		=> _("Usuario Twitter"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "instagram",
			"type"		=> "text",
			"db"		=> "contacto_instagram",
			"required"	=> false,
			"length"	=> 100,
			"div"		=> true,
			"label"		=> _("Usuario Instagram"),
			"class"		=> "char-counter"
		],
	];

	require "./_act/Preferences/Opt/Editor.php";