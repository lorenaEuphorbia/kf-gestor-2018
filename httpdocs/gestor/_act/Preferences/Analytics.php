<?php

	$titulo = "Analítica (Códigos de seguimiento)";

	$fields = [
		[
			"id"			=> "google-analytics",
			"type"			=> "text",
			"db"			=> "analytics_google_analytics",
			"div"			=> true,
			"label"			=> _("Google Analytics"),
			"placeholder"	=> "UA-XXXXXXXXX-X",
		],
	];
	
	require "./_act/Preferences/Opt/Editor.php";