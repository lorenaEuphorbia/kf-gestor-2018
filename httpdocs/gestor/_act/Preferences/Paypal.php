<?php

	$titulo = "Pasarela por paypal";

	$fields = [
		[
			"id"		=> "paypal_is_real",
			"type"		=> "select",
			"db"		=> "paypal_is_real",
			"div"		=> true,
			"label"		=> _("Entorno Real"),
			"options"	=> [
				"values" => [
					"0"	 => _("No"),
					"1"  => _("Si")
				]
			]
		],
		[
			"id"		=> "paypal_url_ok",
			"type"		=> "text",
			"db"		=> "paypal_url_ok",
			"required"	=> true,
			"length"	=> 250 - strlen("http://$_SERVER[HTTP_HOST]/"),
			"div"		=> true,
			"default" 	=> "banco-respuesta",
			"placeholder" => "banco-respuesta",
			"label"		=> _("URL OK (*)"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "paypal_url_ko",
			"type"		=> "text",
			"db"		=> "paypal_url_ko",
			"required"	=> true,
			"length"	=> 250 - strlen("http://$_SERVER[HTTP_HOST]/"),
			"div"		=> true,
			"default" 	=> "banco-no-respuesta",
			"placeholder" => "banco-no-respuesta",
			"label"		=> _("URL KO (*)"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "paypal_nombre_titular",
			"type"		=> "text",
			"db"		=> "paypal_nombre_titular",
			"required"	=> false,
			"length"	=> 60,
			"div"		=> true,
			"placeholder" => "Nombre del titular",
			"label"		=> _("Nombre del titular"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "paypal_email_titular",
			"type"		=> "text",
			"db"		=> "paypal_email_titular",
			"required"	=> false,
			"div"		=> true,
			"placeholder" => "cesar-facilitator@euphorbia.es",
			"label"		=> _("Usuario Titular (email)"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "paypal_email_api",
			"type"		=> "text",
			"db"		=> "paypal_email_api",
			"required"	=> false,
			"div"		=> true,
			"placeholder" => "cesar-facilitator_api1.euphorbia.es",
			"label"		=> _("Usuario de la API (email)"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "paypal_clave_secreta",
			"type"		=> "text",
			"db"		=> "paypal_clave_secreta",
			"required"	=> false,
			"div"		=> true,
			"placeholder" => "AFcWxV21C7fd0v3bYYYRCpSSRl31A5tedPw1RwFLGyBrmxoJ3V3XRy1q",
			"label"		=> _("Firma de la API"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "paypal_password_secreta",
			"type"		=> "text",
			"db"		=> "paypal_password_secreta",
			"required"	=> false,
			"div"		=> true,
			"placeholder" => "4JQSR8QZHJRSLRJQ",
			"label"		=> _("Clave de la API"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "paypal_moneda",
			"type"		=> "select-chosen",
			"db"		=> "paypal_moneda",
			"required"	=> true,
			"div"		=> true,
			"label"		=> _("Moneda"),
			"options"	=> [
				"values" => array_flip (Paypal::MONEDAS)
			],
			"class"		=> "chosen"
		],
	];
	
	require "./_act/Preferences/Opt/Editor.php";