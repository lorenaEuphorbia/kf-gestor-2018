<?php

	$titulo = "Configuración formularios";

	$fields = [
		[
			"id"			=> "email",
			"type"			=> "text",
			"db"			=> "forms_email",
			"required"		=> true,
			"checks"		=> ["email"],
			"div"			=> true,
			"label"			=> _("E-mail remite/destino")
		],
		[
			"id"		=> "subtitulo-smtp",
			"type"		=> "title",
			"options"	=> [
				"type"	=> "h3",
				"text"	=> _("Configuración de envío SMTP"),
				"class"	=> "sectionSubtitle"
			]
		],
		[
			"id"		=> "autenticacion-smtp",
			"type"		=> "select",
			"db"		=> "forms_smtp_auth",
			"div"		=> true,
			"label"		=> _("Autenticación SMTP"),
			"options"	=> [
				"values" => [
					"1"	 => _("Sí"),
					"0"  => _("No")
				]
			]
		],
		[
			"id"			=> "servidor-smtp",
			"type"			=> "text",
			"db"			=> "forms_smtp_server",
			"required"		=> true,
			"div"			=> true,
			"label"			=> _("Servidor"),
			"placeholder"	=> "mail.midominio.com"
		],
		[
			"id"			=> "puerto-smtp",
			"type"			=> "text",
			"db"			=> "forms_smtp_port",
			"required"		=> true,
			"div"			=> true,
			"label"			=> _("Puerto"),
			"placeholder"	=> "25"
		],
		/*
		[
			"id"		=> "seguridad-smtp",
			"type"		=> "select",
			"db"		=> "forms_smtp_security",
			"div"		=> true,
			"label"		=> _("Seguridad"),
			"options"	=> [
				"values" => [
					""		 => _("Ninguna"),
					"tls"	 => "TLS",
					"ssl"	 =>	"SSL"
				]
			]
		],
		*/
		[
			"id"			=> "usuario-smtp",
			"type"			=> "text",
			"db"			=> "forms_smtp_user",
			"required"		=> true,
			"div"			=> true,
			"label"			=> _("Usuario")
		],
		[
			"id"			=> "password-smtp",
			"type"			=> "password",
			"db"			=> "forms_smtp_password",
			"required"		=> true,
			"div"			=> true,
			"label"			=> _("Password"),
			"class"			=> "revealable"
		],
	];
	
	require "./_act/Preferences/Opt/Editor.php";