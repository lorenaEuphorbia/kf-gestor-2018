<?php

	$titulo = "Preferencias SEO";

	$fields = [
		[
			"id"		=> "titulo",
			"type"		=> "text",
			"db"		=> "general_titulo_web",
			"required"	=> true,
			"length"	=> 55,
			"div"		=> true,
			"label"		=> _("Título web"),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "metatitle",
			"type"		=> "text",
			"db"		=> "general_meta_title",
			"required"	=> true,
			"length"	=> 55,
			"div"		=> true,
			"label"		=> _("Meta-tag \"title\""),
			"class"		=> "char-counter"
		],
		[
			"id"		=> "metadescription",
			"type"		=> "text",
			"db"		=> "general_meta_description",
			"required"	=> true,
			"length"	=> 155,
			"div"		=> true,
			"label"		=> _("Meta-tag \"description\""),
			"class"		=> "char-counter"
		],
	];
	
	require "./_act/Preferences/Opt/Editor.php";