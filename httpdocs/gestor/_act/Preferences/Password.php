<?php

	$titulo = "Cambiar contraseña";

	$fields = [
		[
			"id"			=> "password-old",
			"type"			=> "password",
			"db"			=> "password-old",
			"required"		=> true,
			"div"			=> true,
			"label"			=> _("Contraseña antigua")
		],
		[
			"id"			=> "password-new",
			"type"			=> "password",
			"db"			=> "password-new",
			"required"		=> true,
			"div"			=> true,
			"label"			=> _("Nueva contraseña")
		],
		[
			"id"			=> "password-retype",
			"type"			=> "password",
			"db"			=> "password-retype",
			"required"		=> true,
			"div"			=> true,
			"label"			=> _("Volver a escribir")
		],
	];
	
	require "./_act/Preferences/Opt/EditorPassword.php";