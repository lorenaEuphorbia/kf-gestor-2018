<?php


	switch ($opt) {
		case 'Editor':
			$parent_id = isset($_POST["parent-id"]) ? $_POST["parent-id"] : (isset($_GET["parent-id"]) ? $_GET["parent-id"] : "");
			$parent_id = preg_replace("/[^\d]/", "", $parent_id);

			$arrProductos = $modClass->productos->getProductosNombres ();
    
			$titulo = ["Nuevo artículo ", "Mostrando artículo"];

			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "hidden",
						"db"		=> "id",
						"readonly"	=> true,
					],
					[
						"id"		=> "id_order",
						"type"		=> "hidden",
						"db"		=> "id_order",
						"readonly"	=> true,
						"default"	=> $parent_id
					],
					[
						"id"		=> "articulo",
						"type"		=> "text",
						"db"		=> "articulo",
						"required"	=> true,
						"length"	=> 10,
						"readonly"	=> true,
						"div"		=> true,
						"label"		=> _("ID Artículo"),
						'placeholder' => 'requerido',
					],	
					[
						"id"		=> "nombre_articulo",
						"type"		=> "text",
						"db"		=> "",
						"readonly"	=> true,
						"div"		=> true,
						"label"		=> _("Nombre Artículo"),
						'placeholder' => 'requerido',
						"shown"		=> [
							"required"	=> ["articulo"],
							"transform" => function($p) {
								if (!empty($p["articulo"])) {
									global $arrProductos;
									return isset($arrProductos[$p["articulo"]])? $arrProductos[$p["articulo"]]: "ID. $p[articulo]"; ;
								}
								else return '';
							},
						],
					],
					/*
					[
						"id"		=> "talla",
						"type"		=> "text",
						"db"		=> "talla",
						"default"	=> '0',
						"required"	=> true,
						"length"	=> 3,
						"readonly"	=> true,
						"div"		=> true,
						"label"		=> _("Talla"),
						'class'		=> 'char-counter',
					],
					*/
					[
						"id"		=> "precio",
						"type"		=> "number",
						"db"		=> "precio",
						"label"		=> _("Precio"),
						"default"	=> "0",
						"readonly"	=> true,
						"checks"	=> ['decimal'],
						"custom"	=> [
							'step' 	=> '0.01',
							'min'	=> '0',
						],
						"div"		=> true,
						'div_class'	=> 'cols two',
					],
					[
						"id"		=> "iva",
						"type"		=> "number",
						"db"		=> "iva",
						"label"		=> _("IVA %"),
						"default"	=> "0",
						"readonly"	=> true,
						"checks"	=> ['decimal'],
						"custom"	=> [
							'step' 	=> '0.05',
							'min'	=> '0',
						],
						"div"		=> true,
						'div_class'	=> 'cols two',
					],
					[
						"id"		=> "precio_iva",
						"type"		=> "number",
						"db"		=> "precio_iva",
						"label"		=> _("Precio IVA"),
						"default"	=> "0",
						"readonly"	=> true,
						"checks"	=> ['decimal'],
						"custom"	=> [
							'step' 	=> '0.01',
							'min'	=> '0',
						],
						"div"		=> true,
						'div_class'	=> 'cols two',
					],
					[
						"id"		=> "unidades",
						"type"		=> "number",
						"db"		=> "unidades",
						"label"		=> _("Unidades"),
						"default"	=> "0",
						"readonly"	=> true,
						"div"		=> true,
						'div_class'	=> 'cols two',
					],


				],
				"hidden" => ['parent-id'=>$parent_id],
			];

			$buttons = [
				"back"	=> _("Volver")
			];

			$actions = [
				"guardar"	=> ""
			];

		break;
		default:
			$titulo = "Listado de artículos"; /// Opciones para el listado
			$id = isset($_POST["id"]) ? $_POST["id"] : (isset($_GET["id"]) ? $_GET["id"] : "");
			$id = preg_replace("/[^\d]/", "", $id);

			$arrProductos = $modClass->productos->getProductosNombres ();

			$list = [
				"id" => "articulos",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"	=> "articulo",
						"db"	=> "articulo",
						"title"	=> _("Artículo"),
						"shown"		=> function($val) { 
							global $arrProductos;
							return isset($arrProductos[$val])? $arrProductos[$val]: "ID. $val"; },
					],
					/*
					[
						"id"	=> "talla",
						"db"	=> "talla",
						"title"	=> _("Talla"),
					],*/
					[
						"id"	=> "precio",
						"db"	=> "precio",
						"title"	=> _("Precio"),
					],
					[
						"id"	=> "unidades",
						"db"	=> "unidades",
						"title"	=> _("Unidades"),
					],
				],
				"filter" => [
					'id_order' => $id
				],
				"sort" => [
					["id", "ASC"],
				],
				"actions" => [
					"editor" => [
						"form-bt"		=> _("Añadir menú"),
						"class" 		=> "fa fa-eye",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Ver"),
						"hidden"	=> [
							"parent-id"	=> $id
						]
					],
					/*
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
					*/
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			$buttons = [
				"back"	=> [
					"action"=> strtolower("$mod/orders/list/"),
					"text" 	=> _("Volver")
				]
			];

			// var_dump($_REQUEST); exit;

			if (empty($opt)) $opt = "List";
		break;
	}

	//echo __DIR__; exit;
	
	require "./_act/Base/Opt/$opt.php";