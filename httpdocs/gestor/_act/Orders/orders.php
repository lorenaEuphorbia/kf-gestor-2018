<?php

	switch ($opt) {
		case 'Editor':
			$titulo = ["Nuevo Pedido", "Datos del pedido"];


			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "text",
						"db"		=> "id",
						"label"		=> _("ID Pedido"),
						'length'	=> 10,
						'div'		=> true,
						'readonly' 	=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						"id"		=> "code",
						"type"		=> "text",
						"db"		=> "code",
						"label"		=> _("Código Pedido"),
						'length'	=> 128,
						'div'		=> true,
						'readonly' 	=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						"id"		=> "code",
						"type"		=> "text",
						"db"		=> "codigo_cliente",
						"label"		=> _("Código Cliente"),
						'length'	=> 10,
						'div'		=> true,
						'readonly' 	=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id' 		=> 'nombre',
						'type'		=> 'text',
						'db'		=> 'nombre',
						'label'		=> _('Nombre'),
						'length'	=> 60,
						'div'		=> true,
						'readonly' 	=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id' 		=> 'apellidos',
						'type'		=> 'text',
						'db'		=> 'apellidos',
						'label'		=> _('Apellidos'),
						'length'	=> 120,
						'div'		=> true,
						'readonly' 	=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id' 		=> 'email',
						'type'		=> 'text',
						'db'		=> 'email',
						'label'		=> _('Email'),
						'length'	=> 60,
						'div'		=> true,
						'readonly' 	=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id' 		=> 'nif',
						'type'		=> 'text',
						'db'		=> 'nif',
						'label'		=> _('NIF/CIF'),
						'length'	=> 20,
						'div'		=> true,
						'readonly' 	=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id' 		=> 'telefono',
						'type'		=> 'text',
						'db'		=> 'telefono',
						'label'		=> _('Teléfono'),
						'length'	=> 20,
						'div'		=> true,
						'readonly' 	=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					// DATOS FACTURACIÓN
					[
						"id"		=> "datos_pedido",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "DATOS FACTURACIÓN",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						'id'		=> 'nombre_dir_facturacion',
						'type'		=> 'text',
						'db'		=> 'nombre_dir_facturacion',
						'label'		=> _('Nombre/Razón Social'),
						'readonly' 	=> true,
						'length'	=> 180,
						'div'		=> true,
						'class'		=> 'char-counter',
					],
					[
						'id'		=> 'nif_dir_facturacion',
						'type'		=> 'text',
						'db'		=> 'nif_dir_facturacion',
						'label'		=> _('NIF/CIF'),
						'readonly' 	=> true,
						'length'	=> 20,
						'div'		=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'codigo_dir_facturacion',
						'type'		=> 'hidden',
						'db'		=> 'codigo_dir_facturacion',
						'readonly' 	=> true,
						'length'	=> 20,
						'div'		=> false,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'direccion_dir_facturacion',
						'type'		=> 'text',
						'db'		=> 'direccion_dir_facturacion',
						'label'		=> _('Dirección'),
						'readonly' 	=> true,
						'length'	=> 120,
						'div'		=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'cp_dir_facturacion',
						'type'		=> 'text',
						'db'		=> 'cp_dir_facturacion',
						'label'		=> _('Código Postal'),
						'readonly' 	=> true,
						'length'	=> 20,
						'div'		=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'localidad_dir_facturacion',
						'type'		=> 'text',
						'db'		=> 'localidad_dir_facturacion',
						'label'		=> _('Localidad'),
						'readonly' 	=> true,
						'length'	=> 60,
						'div'		=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'provincia_dir_facturacion',
						'type'		=> 'number',
						'db'		=> 'provincia_dir_facturacion',
						'label'		=> _('Provincia'),
						'readonly' 	=> true,
						'div'		=> true,
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'pais_dir_facturacion',
						'type'		=> 'number',
						'db'		=> 'pais_dir_facturacion',
						'label'		=> _('Pais'),
						'readonly' 	=> true,
						'div'		=> true,
						'div_class' => 'cols two',
					],
					// DATOS ENVÍO
					[
						"id"		=> "datos_pedido",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "DATOS ENVÍO",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						'id'		=> 'codigo_dir_envio',
						'type'		=> 'hidden',
						'db'		=> 'codigo_dir_envio',
						'readonly' 	=> true,
						'length'	=> 20,
						'div'		=> false,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'nombre_dir_envio',
						'type'		=> 'text',
						'db'		=> 'nombre_dir_envio',
						'label'		=> _('Nombre'),
						'readonly' 	=> true,
						'length'	=> 180,
						'div'		=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'direccion_dir_envio',
						'type'		=> 'text',
						'db'		=> 'direccion_dir_envio',
						'label'		=> _('Dirección'),
						'readonly' 	=> true,
						'length'	=> 120,
						'div'		=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'cp_dir_envio',
						'type'		=> 'text',
						'db'		=> 'cp_dir_envio',
						'label'		=> _('Código Postal'),
						'readonly' 	=> true,
						'length'	=> 20,
						'div'		=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'localidad_dir_envio',
						'type'		=> 'text',
						'db'		=> 'localidad_dir_envio',
						'label'		=> _('Localidad'),
						'readonly' 	=> true,
						'length'	=> 60,
						'div'		=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'provincia_dir_envio',
						'type'		=> 'number',
						'db'		=> 'provincia_dir_envio',
						'label'		=> _('Provincia'),
						'readonly' 	=> true,
						'div'		=> true,
					],
					[
						'id'		=> 'pais_dir_envio',
						'type'		=> 'hidden',
						'db'		=> 'pais_dir_envio',
						'readonly' 	=> true,
						'length'	=> 50,
						'div'		=> false,
						'readonly' 	=> true,
						'div'		=> true,
					],
					// DATOS DE PAGO
					[
						"id"		=> "datos_pedido",
						"type"		=> "title",
						"db"		=> "",
						"options"	=> [
							"text" => "DATOS DEL PAGO",
							"type" => "h4",
							"class" => "sectionTitle"
						],
						"label"		=> _(" "),
					],
					[
						'id'		=> 'envioEmpresa',
						'type'		=> 'hidden',
						'db'		=> 'envioEmpresa',
						'readonly' 	=> true,
						'length'	=> 50,
						'div'		=> false,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'formaDePago',
						'type'		=> 'text',
						'db'		=> 'formaDePago',
						'label'		=> _('Método de pago'),
						'readonly' 	=> true,
						'length'	=> 50,
						'div'		=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'request_id',
						'type'		=> 'text',
						'db'		=> 'request_id',
						'label'		=> _('Código Pasarela'),
						'readonly'	=> true,
						'div'		=> true,
						'div_class'	=> 'cols two',
					],
					[
						'id'		=> 'validacion',
						'type'		=> 'text',
						'db'		=> 'validacion',
						'label'		=> _('Validación'),
						'readonly' 	=> true,
						'length'	=> 250,
						'div'		=> true,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'bancoRespPre',
						'type'		=> 'hidden',
						'db'		=> 'bancoRespPre',
						'readonly' 	=> true,
						'length'	=> 50,
						'div'		=> false,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'bancoRespConfirm',
						'type'		=> 'hidden',
						'db'		=> 'bancoRespConfirm',
						'readonly' 	=> true,
						'length'	=> 50,
						'div'		=> false,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'bancoRespNot',
						'type'		=> 'hidden',
						'db'		=> 'bancoRespNot',
						'readonly' 	=> true,
						'length'	=> 50,
						'div'		=> false,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'bancoRespCancel',
						'type'		=> 'hidden',
						'db'		=> 'bancoRespCancel',
						'readonly' 	=> true,
						'length'	=> 50,
						'div'		=> false,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'carrito',
						'type'		=> 'hidden',
						'db'		=> 'carrito',
						'readonly' 	=> true,
						'length'	=> 50,
						'div'		=> false,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'envioObservaciones',
						'type'		=> 'hidden',
						'db'		=> 'envioObservaciones',
						'readonly' 	=> true,
						'length'	=> 50,
						'div'		=> false,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'str_tarjeta_regalo',
						'type'		=> 'hidden',
						'db'		=> 'str_tarjeta_regalo',
						'readonly' 	=> true,
						'length'	=> 50,
						'div'		=> false,
						'class'		=> 'char-counter',
						'div_class' => 'cols two',
					],
					[
						'id'		=> 'is_factura',
						'type'		=> 'checkbox',
						'db'		=> 'is_factura',
						'label'		=> _('Necesita factura'),
						'disabled'	=> true,
						'div'		=> true,
						'div_class'	=> 'cols two',
					],
					[
						'id'		=> 'is_envolver_regalo',
						'type'		=> 'hidden',
						'db'		=> 'is_envolver_regalo',
						'readonly'	=> true,
						'div'		=> false,
						'div_class'	=> 'cols two',
					],
					[
						'id'		=> 'is_tarjeta_regalo',
						'type'		=> 'hidden',
						'db'		=> 'is_tarjeta_regalo',
						'readonly'	=> true,
						'div'		=> false,
						'div_class'	=> 'cols two',
					],
					[
						'id'		=> 'vale',
						'type'		=> 'hidden',
						'db'		=> 'vale',
						'readonly'	=> true,
						'div'		=> false,
						'div_class'	=> 'cols two',
					],
					[
						'id'		=> 'pagando',
						'type'		=> 'checkbox',
						'db'		=> 'pagando',
						'label'		=> _('Pagando'),
						'disabled'	=> true,
						'div'		=> true,
						'div_class'	=> 'cols two',
					],
					[
						'id'		=> 'historico',
						'type'		=> 'checkbox',
						'db'		=> 'historico',
						'label'		=> _('Histórico'),
						'disabled'	=> true,
						'div'		=> true,
						'div_class'	=> 'cols two',
					],
					[
						'id'		=> 'correo_resumen',
						'type'		=> 'checkbox',
						'db'		=> 'correo_resumen',
						'label'		=> _('Correo Enviado'),
						'disabled'	=> true,
						'div'		=> true,
						'div_class'	=> 'cols two',
					],
					[
						'id'		=> 'correo_envio',
						'type'		=> 'hidden',
						'db'		=> 'correo_envio',
						'readonly'	=> true,
						'div'		=> false,
						'div_class'	=> 'cols two',
					],


					[
						"id"		=> "fecha_nacimiento",
						"type"		=> "hidden",
						"db"		=> "fecha_nacimientoe",
						"checks"	=> ["datetime"],
						"readonly"	=> true,
						"div"		=> false,
					],
					[
						"id"		=> "pagandoFecha",
						"type"		=> "datetime-local",
						"db"		=> "pagandoFecha_date",
						"label"		=> _("Fecha"),
						"checks"	=> ["datetime"],
						"disabled"	=> true,
						"div"		=> true,
						'div_class'	=> 'cols two',
					],
					[
						"id"		=> "creation_date",
						"type"		=> "hidden",
						"db"		=> "creation_datedate",
						"checks"	=> ["datetime"],
						"readonly"	=> true,
						"div"		=> false,
					],
					[
						"id"		=> "modification_date",
						"type"		=> "hidden",
						"db"		=> "modification_date",
						"checks"	=> ["datetime"],
						"readonly"	=> true,
						"div"		=> false,
					],
					[
						"id"		=> "envioImporte",
						"type"		=> "number",
						"db"		=> "envioImporte",
						"label"		=> _("Gastos de Envío"),
						"default"	=> "0",
						"readonly"	=> true,
						"checks"	=> ['decimal'],
						"custom"	=> [
							'step' 	=> '0.05',
							'min'	=> '0',
						],
						"div"		=> true,
						'div_class'	=> 'cols two',
					],
					[
						"id"		=> "pvp_total",
						"type"		=> "number",
						"db"		=> "pvp_total",
						"label"		=> _("Total compra"),
						"default"	=> "0",
						"readonly"	=> true,
						"checks"	=> ['decimal'],
						"custom"	=> [
							'step' 	=> '0.05',
							'min'	=> '0',
						],
						"div"		=> true,
						'div_class'	=> 'cols two',
					],

				],
				"hidden" => []
			];

			$buttons = [
				//"new"	=> "Nuevo Post",
				"back"	=> _("Volver")
			];

			$actions = [
				"guardar"	=> ""
			];
		break;
		default: /// Opciones para el listado
			$titulo = "Listado de pedidos";

			$list = [
				"id" => "post",
				"fields" => [
					[
						"id"	=> "id",
						"db"	=> "id",
						"title"	=> "ID",
						"options" => [
							"orderable"		=> "false",
							"visible"		=> "false",
							"searchable"	=> "false"
						]
					],
					[
						"id"		=> "creation_date",
						"db"		=> "creation_date",
						"title"		=> _("Fecha"),
						"shown"		=> function($val) { return date("d/m/Y", strtotime($val)); },
						"options"	=> [
							"type"	=> "date-eu"
						]
					],
					[
						"id"	=> "nombre",
						"db"	=> "nombre",
						"title"	=> _("Nombre"),
					],
					[
						"id"	=> "apellidos",
						"db"	=> "apellidos",
						"title"	=> _("Apellidos"),
					],
					[
						"id"	=> "nif",
						"db"	=> "nif",
						"title"	=> _("NIF/CIF"),
					],
					[
						"id"	=> "formaDePago",
						"db"	=> "formaDePago",
						"title"	=> _("Método de Pago"),
					],
					/*
					[
						"id"	=> "comments",
						"db"	=> "id",
						"title"	=> _("Comentarios"),
						"shown"	=> function($val) {
							global $modClass;
							return count($modClass->getCommentsByPost($val));
						},
					],
					*/
				],
				"filter" => [
					"historico" => "1",
					"request_id" => ['IS NOT','null']
				],
				"sort" => [
					["creation_date", "DESC"],
				],
				"actions" => [
					"editor" => [
						"form-bt"		=> _("Nuevo pedido"),
						"form-bt-class"	=> "hidden",
						"class" 		=> "fa fa-eye",
						"text"			=> _("Ver datos"),
					],
					"request" => [
						"action"		=> "orders/request/",
						"class" 		=> "fa fa-bank",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Pasarela"),
					],
					"articulos" => [
						"action"		=> "orders/articulos/",
						"class" 		=> "fa fa-list",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Albar&aacute;n"),
					],
					/*
					"delete" => [
						"class" 		=> "fa fa-trash",
						"form-bt-class"	=> "hidden",
						"text"			=> _("Eliminar"),
					]
					*/
				],
				"multiple_selection" => true,
				"extra-buttons" => "" //"excel,csv,pdf,print"
			];

			if (empty($opt)) $opt = "List";
		break;
	}

	//echo __DIR__; exit;
	
	require "./_act/Base/Opt/$opt.php";