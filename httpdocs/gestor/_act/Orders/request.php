<?php


	switch ($opt) {
		default:

			$id_order = isset($_POST["id"]) ? $_POST["id"] : (isset($_GET["id"]) ? $_GET["id"] : "");
			$id_order = preg_replace("/[^\d]/", "", $id_order);

			$id = $modClass->search (['id'=>$id_order], 'Orders')? $modClass->getCurrentFieldValue('request_id','Orders'): false;
    
			$titulo = ["Nueva pasarela ", "Datos de la pasarela"];

			$fields = [
				"db" => [
					[
						"id"		=> "id",
						"type"		=> "hidden",
						"db"		=> "id",
						"readonly"	=> true,
					],
					[
						"id"		=> "id_order",
						"type"		=> "hidden",
						"db"		=> "id_order",
						"readonly"	=> true,
						"default"	=> $id_order
					],
					[
						"id"		=> "modification",
						"type"		=> "datetime-local",
						"db"		=> "modification",
						"label"		=> _("Fecha"),
						"checks"	=> ["datetime"],
						"disabled"	=> true,
						"div"		=> true,
						'div_class'	=> 'cols two',
					],
					[
						"id"		=> "creacion",
						"type"		=> "hidden",
						"db"		=> "creacion",
						"checks"	=> ["datetime"],
						"disabled"	=> true,
						"div"		=> false,
					],
					[
						'id'		=> 'historico',
						'type'		=> 'checkbox',
						'db'		=> 'historico',
						'label'		=> _('Pagado'),
						'disabled'	=> true,
						'div'		=> true,
						'div_class'	=> 'cols two',
					],
					[
						"id"		=> "tipo",
						"type"		=> "text",
						"db"		=> "tipo",
						"required"	=> true,
						"length"	=> 20,
						"readonly"	=> true,
						"div"		=> true,
						"label"		=> _("Método de Pago"),
						'placeholder' => 'requerido',
						'div_class'	=> 'cols two',
					],	
					[
						"id"		=> "articulos",
						"type"		=> "number",
						"db"		=> "articulos",
						"label"		=> _("Número de artículos"),
						"default"	=> "0",
						"readonly"	=> true,
						"div"		=> true,
						'div_class'	=> 'cols two',
					],
					[
						"id"		=> "pvp_articulos",
						"type"		=> "number",
						"db"		=> "pvp_articulos",
						"label"		=> _("Precio artículos"),
						"default"	=> "0",
						"readonly"	=> true,
						"checks"	=> ['decimal'],
						"custom"	=> [
							'step' 	=> '0.01',
							'min'	=> '0',
						],
						"div"		=> true,
						'div_class'	=> 'cols two',
					],
					[
						"id"		=> "pvp_total",
						"type"		=> "number",
						"db"		=> "pvp_total",
						"label"		=> _("Precio total"),
						"default"	=> "0",
						"readonly"	=> true,
						"checks"	=> ['decimal'],
						"custom"	=> [
							'step' 	=> '0.01',
							'min'	=> '0',
						],
						"div"		=> true,
						'div_class'	=> 'cols two',
					],
					[
						"id"		=> "pvp_envio",
						"type"		=> "number",
						"db"		=> "pvp_envio",
						"label"		=> _("Precio envío"),
						"default"	=> "0",
						"readonly"	=> true,
						"checks"	=> ['decimal'],
						"custom"	=> [
							'step' 	=> '0.01',
							'min'	=> '0',
						],
						"div"		=> true,
						'div_class'	=> 'cols two',
					],
					[
						"id"		=> "pvp_iva",
						"type"		=> "number",
						"db"		=> "pvp_iva",
						"label"		=> _("Precio de IVA"),
						"default"	=> "21",
						"readonly"	=> true,
						"checks"	=> ['decimal'],
						"custom"	=> [
							'step' 	=> '0.05',
							'min'	=> '0',
						],
						"div"		=> true,
						'div_class'	=> 'cols two',
					],
					/*
					[
						"id"		=> "pvp_descuento",
						"type"		=> "number",
						"db"		=> "pvp_descuento",
						"label"		=> _("Precio del descuento"),
						"default"	=> "0",
						"readonly"	=> true,
						"checks"	=> ['decimal'],
						"custom"	=> [
							'step' 	=> '0.01',
							'min'	=> '0',
						],
						"div"		=> true,
						'div_class'	=> 'cols two',
					],
					[
						"id"		=> "pvp_pago",
						"type"		=> "number",
						"db"		=> "pvp_pago",
						"label"		=> _("Precio del pago"),
						"default"	=> "0",
						"readonly"	=> true,
						"checks"	=> ['decimal'],
						"custom"	=> [
							'step' 	=> '0.01',
							'min'	=> '0',
						],
						"div"		=> true,
						'div_class'	=> 'cols two',
					],
					[
						"id"		=> "pvp_tarjeta",
						"type"		=> "number",
						"db"		=> "pvp_tarjeta",
						"label"		=> _("Precio de tarjeta regalo"),
						"default"	=> "0",
						"readonly"	=> true,
						"checks"	=> ['decimal'],
						"custom"	=> [
							'step' 	=> '0.01',
							'min'	=> '0',
						],
						"div"		=> true,
						'div_class'	=> 'cols two',
					],
					[
						"id"		=> "pvp_envolver",
						"type"		=> "number",
						"db"		=> "pvp_envolver",
						"label"		=> _("Precio de envolver regalo"),
						"default"	=> "0",
						"readonly"	=> true,
						"checks"	=> ['decimal'],
						"custom"	=> [
							'step' 	=> '0.01',
							'min'	=> '0',
						],
						"div"		=> true,
						'div_class'	=> 'cols two',
					],
					[
						"id"		=> "pvp_puntos",
						"type"		=> "number",
						"db"		=> "pvp_puntos",
						"label"		=> _("Precio en puntos"),
						"default"	=> "0",
						"readonly"	=> true,
						"div"		=> true,
						'div_class'	=> 'cols two',
					],
					*/
					[
						"id"		=> "errors",
						"type"		=> "textarea",
						"db"		=> "errors",
						"label"		=> _("Errores"),
						"readonly"	=> true,
						"length"	=> 100,
						"div"		=> true,
					],

				],
				"hidden" => ['parent-id'=>$id_order],
			];

			$buttons = [
				"custom"	=> [
					"id"		=> 'back',
					"action"	=> strtolower("$base_path$mod/$mod/"),
					"class"		=> "hidden",
					"button"	=> _('Atras'),
				],
			];

			$actions = [
				"guardar"	=> ""
			];

		break;
	}

	//echo __DIR__; exit;
	
	require "./_act/Base/Opt/Editor.php";