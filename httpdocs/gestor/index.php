<?php

	/// Cargamos otras clases necesarias (estáticas)
	require $_SERVER['DOCUMENT_ROOT'] . '/gestor/_inc/config.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/gestor/_inc/functions.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-utilities.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-domElements.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-controller.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-adminPreferences.php';

	setAutoloader();

	/// Arrancamos el panel de control
	$i18n = new I18n("admin", $lang_admin);
	$b = new Base();

	/// Recogemos módulo y acción
	setPathVars ($mod, $act, $opt, $url, $id);

	///  Enrutamiento y permisos
	require $_SERVER['DOCUMENT_ROOT'] . '/gestor/_inc/routes.php';
	$arrPermisions = getPermisions($arrMenu);

	/// Definimos las variables que se utilizarán
	$base_path = "/". ADMIN_FOLDER . "/";

	/// Comprobamos permisos
	if (empty($b->error)) {
		if ($b->security->hasPermission($arrPermisions)) {
			$css = [$base_path."css/b-admin.min.css"];
			$js = [$base_path."js/vendors/jquery-2.1.4.min.js", $base_path."js/b-admin.min.js"];

			if (empty($mod) && empty($act)) {
				$mod = "Base";
				$act = "Dashboard";
			}
		}
		else {

			$css = [$base_path."css/b.min.css"];
			$js = [$base_path."js/b.min.js"];

			$act = in_array($mod, ["", "Base"]) && in_array($act, ["", "Login"]) ? "Login" : "NoPermissionOut";
			$mod = "Base";
		}
	}
	else {
		$css = [$base_path."css/b.min.css"];
		$js = [$base_path."js/b.min.js"];

		$mod = "Base";
		$act = "Error";

		$message = mb_strtoupper($b->error, "UTF-8");
	}

	/// Cargamos el módulo seleccionado
	require getFileByMod ($mod, $arrPermisions);