<?php
	include "mod/base.php";

	if (!empty($_REQUEST['token'])) {
		$strJwt = $_REQUEST['token'];

		// cargamos contenido de SLIM
		loadSlimDependencies();
		$settings = getSlimSettings()['settings'];

		// configuramos
    	\Firebase\JWT\JWT::$leeway = 60 * $settings['token_time'];


		$objJwt = \Firebase\JWT\JWT::decode($strJwt, $settings['api_key_public'], array($settings['api_algorithms']));
		$objToken = $_objClsBase->users->getTokenValid ($objJwt->jti);

		if ($objToken) {
			$arrOrders = $_objClsBase->orders->getList ('Orders', [], ['id_token' => $objToken['id'], 'codigo_cliente' => $objToken['id_usuario'], 'historico' => 0], ['modification_date DESC']);
			$objOrder = $arrOrders ? $arrOrders[0] : false;

			if ($objOrder) {
				$_objClsBase->security->forceLogin(['id' => $objToken['id_usuario']]);
				$_objClsBase->carrito->switchOrder($objOrder);
				$_SESSION['is_jwt_access'] = true;
				header('Location: /banco-redireccion/');
			}
		}
		exit;
	}


	$idPag = "";
	$tituloWeb = "Redirigiendo al banco";

	if (!$_objClsBase->security->isLogin) {
		$_SESSION['RESULT'] = [
			'mensaje' => 'Debes loguearte primero.',
			'resultado' => false,
			'reload' => true
		];
		header('Location: /es/login-registro');
		exit;
	}
	
	$idPedido = $_objClsBase->carrito->sendRequest();
	$objPedido = $_objClsBase->orders->getRecord ('Request', null, ['id'=>$idPedido]);
	if (!$idPedido || !$objPedido) {
		$_SESSION['RESULT'] = [
			'mensaje' => 'No es posible conectar con el banco, intentelo pasados unos minutos y si el error persiste pongase en contacto con nosotros, por favor.',
			'resultado' => false,
			'reload' => true
		];
		//header('Location: '.$_objClsBase->getLastPage());
		var_dump($idPedido, $objPedido, $_SESSION);
		exit;
	}


	$banco = new Pasarela($_objClsBase);
	$banco->redsysAPI->setParameter("DS_MERCHANT_AMOUNT", round($objPedido['pvp_total']*100));
	$banco->redsysAPI->setParameter("DS_MERCHANT_ORDER", $objPedido['id']);
	$banco->redsysAPI->setParameter("DS_MERCHANT_MERCHANTCODE", $banco->codigoTitular);
	$banco->redsysAPI->setParameter("DS_MERCHANT_CURRENCY", $banco->moneda);
	$banco->redsysAPI->setParameter("DS_MERCHANT_TRANSACTIONTYPE", "0"); // 0 = estandar
	$banco->redsysAPI->setParameter("DS_MERCHANT_TERMINAL", $banco->terminal);
	$banco->redsysAPI->setParameter("DS_MERCHANT_MERCHANTURL", $banco->urlNotificacion);
	$banco->redsysAPI->setParameter("DS_MERCHANT_URLOK", $banco->urlRespuestaOk);
	$banco->redsysAPI->setParameter("DS_MERCHANT_URLKO", $banco->urlRespuestaOk);
	$banco->redsysAPI->setParameter("DS_MERCHANT_DATA", $objPedido['id_order']);
	$banco->redsysAPI->setParameter("DS_MERCHANT_TITULAR", $banco->nombreTitular);
	$banco->redsysAPI->setParameter("DS_MERCHANT_PRODUCTDESCRIPTION", "Compra con referencia $objPedido[id_order]");

	$params = $banco->redsysAPI->createMerchantParameters();
	$signature = $banco->redsysAPI->createMerchantSignature($banco->claveSecreta);

	// include "mod/cab.php";

	?>

	Redirigiendo al banco...
	<form name="reenvio" action="<?php echo $banco->urlFormulario; ?>" method="POST">
		<input type="hidden" name="Ds_SignatureVersion" value="HMAC_SHA256_V1"/>
		<input type="hidden" name="Ds_MerchantParameters" value="<?php echo $params; ?>"/>
		<input type="hidden" name="Ds_Signature" value="<?php echo $signature; ?>"/>
	</form>
	<script>
		window.setTimeout("EnviaForm()", 1000);
		function EnviaForm () { document.reenvio.submit(); }
	</script>
	


