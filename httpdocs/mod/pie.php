		<div class="bg-sucio">
			<div class="centrado-interior">
				<div class="marcas">
					<a href="http://www.laspatatasdelabuelo.com" target="_blank"><img src="/img/comunes/logos-color/lpda-logo.png" /></a>
					<a href="/<?= $idioma ?>/nuestras-marcas/#patatas-de-la-huerta"><img src="/img/comunes/logos-color/patatas-de-la-huerta-logo.png" /></a>
					<a href="/<?= $idioma ?>/nuestras-marcas/#patata-todo-uso"><img src="/img/comunes/logos-color/todo-uso-logo.png" /></a>
				</div>
			</div>
		</div>
		</main>
		<footer class="pie">
			Copyright © 2016 <?php $fecha = date("Y"); if($fecha > '2016') echo "- $fecha" ?> AGROINNOVA.
			<div class="enlaces ">
				<a href="/<?= $idioma ?>/aviso-legal/"><?= $textosIdiomas['home-aviso-legal-tit'] ?></a>
				<a href="/<?= $idioma ?>/aviso-legal/#politica-privacidad"><?= $textosIdiomas['home-politica-privacidad-tit'] ?></a>
				<a href="/<?= $idioma ?>/aviso-legal/#politica-cookies"><?= $textosIdiomas['home-politica-cookies-tit'] ?></a>
				<a href="/<?= $idioma ?>/dinamica-de-compra"><?= $textosIdiomas['dinamica-de-compra'] ?></a>
				<a href="/<?= $idioma ?>/preguntas-frecuentes"><?= $textosIdiomas['preguntas-frecuentes'] ?></a>
			</div>
		</footer>
		<noscript><div class="aviso" ><img src="img/comunes/error.png" ><?= $textosIdiomas['AvisoJavascript'] ?></div></noscript>

		<?php if (!empty($_SESSION['RESULT'])): ?>
		<script type="text/javascript"> $(document).ready(function(){ModalShow("<?= $_SESSION['RESULT']['mensaje'] ?>");});	</script>
		<?php unset($_SESSION['RESULT']); endif ?>

		<!--<?php var_dump($_SESSION) ?>-->
	</body>
</html>