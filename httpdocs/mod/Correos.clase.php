<?php

	/***************************************************************
	 *                       CLASE: Correos                        *
	 ***************************************************************/
	/**
	 * Gestión del envío de correos.
	 * 
	 * Gestión del sistema de envío de correos electrónicos.
	 *
	 * @package     EUPHORBIA
	 * @subpackage  BaseDatos
	 * @author      Pablo Enjuto Martín <cesar@euphorbia.es>
	 * @copyright   Copyright 2014, Euphorbia Comunicación, S.L.L.
	 * @access      public
	 * @version     3.00 - Final
	 * @link        http://www.euphorbia.es Euphorbia Comunicación, S.L.L.
	 *
	 */
	
	include_once("phpmailer/class.phpmailer.php");
	
	class Correos {
		protected $objClsPreferencias;

		function __construct($objClsPreferencias) {
			$this->objClsPreferencias=$objClsPreferencias;
		}
		
		function EnviaCorreo($opcion = '', $idioma = 'es', $remite = array(), $destino = array(), $copiaCC = array(), $copiaBCC = array(), $parametros = array()) {
			$idioma = strtoupper($idioma);
			
			$resp = false;
			
			if($opcion != '' && $remite && $destino) {
				$asunto = "Agroinnova";
				$raizURL =  "http://www.pruebas.agroinnova.com/";
				$color = "#45b1e1";
				
				// CABECERA DEL MAIL
				$contenido = '<html xmlns="http://www.w3.org/1BBB/xhtml" xml:lang="en" lang="en">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>' . $asunto . '</title>
		</head>
		<body style="font: 10pt arial, sans-serif;margin-top:0px">
			<table align="center" border="0" cellpadding="0" cellspacing="0" width="670" >
				<tr height="20" >
					<td></td>
				</tr>
				<tr height="120" valign="top" align="right">
					<td>
						<a href="'.$raizURL.'" target="_blank" ><img src="'.$raizURL.'/img/comunes/logotipo.png" border="0" height="76" /></a>
					</td>
				</tr>
				<tr valign="top" >
					<td style="font: 9pt Arial, Sans-serif;margin-bottom:20px" >
						Hola ' . $destino[0] . ':<br/><br/>';
				
				
				// CUERPO Y OPCIONES DEL CORREO
				
				switch ($opcion) {
					case 'contacto':
						$asunto = 'Agroinnova Web | '.$parametros['subject'].' quiere contactar contigo';
							
						$contenido .= $parametros['name'] . ' ha rellenado el formulario de contacto de  
						<a href="$raizURL" target="_blank" style="color:#333333;" >www.agroinnova.com</a><br />
						<h2 style="color:#333" >Datos del formulario:</h2>
						<table border="0" cellpadding="3" cellspacing="0" width="650" style="font: 9pt Arial, Sans-serif;border-collapse: collapse;" >
							<tr style="background:#EDC159;color:#FFF;" ><td style="padding:8px 5px;border: 1px solid #EDC159;" ><b>Campo</b></td><td style="border: 1px solid #EDC159;padding:8px 5px;" ><b>Dato</b></td></tr>
							<tr valign="middle" ><td style="padding:8px 5px;border: 1px solid #BBB;" >Nombre:</td><td style="border: 1px solid #BBB;padding:8px 5px;" ><b>'. $parametros['name'] .'</b></td></tr>
							<tr valign="middle" ><td style="padding:8px 5px;border: 1px solid #BBB;" >Email:</td><td style="border: 1px solid #BBB;padding:8px 5px;" ><b>'. $parametros['email'] .'</b></td></tr>
							<tr valign="middle" ><td style="padding:8px 5px;border: 1px solid #BBB;" >Idioma:</td><td style="border: 1px solid #BBB;padding:8px 5px;" ><b>'. $parametros['idioma'] .'</b></td></tr>
							<tr valign="middle" ><td style="padding:8px 5px;border: 1px solid #BBB;" >Mensaje:</td><td style="border: 1px solid #BBB;padding:8px 5px;" ><b>'. $parametros['message'] .'</b></td>
						</table>';
					break;
					case 'contacto-cliente':
						$asunto = 'Agroinnova Web | Resumen del contacto';
							
						$contenido .= ' Has rellenado el formulario de contacto de  
						<a href="$raizURL" target="_blank" style="color:#333333;" >www.agroinnova.com</a><br />
						<h2 style="color:#333333" >Datos enviados:</h2>
						<table border="0" cellpadding="3" cellspacing="0" width="650" style="font: 9pt Arial, Sans-serif;border-collapse: collapse;" >
							<tr style="background:#EDC159;color:#FFF;" ><td style="padding:8px 5px;border: 1px solid #EDC159;" ><b>Campo</b></td><td style="border: 1px solid #EDC159;padding:8px 5px;" ><b>Dato</b></td></tr>
							<tr valign="middle" ><td style="padding:8px 5px;border: 1px solid #BBB;" >Nombre:</td><td style="border: 1px solid #BBB;padding:8px 5px;" ><b>'. $parametros['name'] .'</b></td></tr>
							<tr valign="middle" ><td style="padding:8px 5px;border: 1px solid #BBB;" >Email:</td><td style="border: 1px solid #BBB;padding:8px 5px;" ><b>'. $parametros['email'] .'</b></td></tr>
							<tr valign="middle" ><td style="padding:8px 5px;border: 1px solid #BBB;" >Idioma:</td><td style="border: 1px solid #BBB;padding:8px 5px;" ><b>'. $parametros['idioma'] .'</b></td></tr>
							<tr valign="middle" ><td style="padding:8px 5px;border: 1px solid #BBB;" >Mensaje:</td><td style="border: 1px solid #BBB;padding:8px 5px;" ><b>'. $parametros['message'] .'</b></td>
						</table>
						<br />
						Nos pondremos en contacto lo antes posible.';
					break;
					case 'resumen-compra':
						$asunto = 'Agroinnova Resúmen de Compra - '.$parametros['idCompra'];
							
						$contenido .= $parametros['name'] . ' ha rellenado el formulario de contacto de  
						<a href="$raizURL" target="_blank" style="color:#333333;" >www.agroinnova.com</a><br />
						<h2 style="color:#333" >Datos del comprador:</h2>
						<br />
						Nombre
						Razón social:
						DNI:
						Email:
						Teléfono:

						<h2 style="color:#333" >Datos de facturación:</h2>
						Dirección:
						Población:
						Provincia:
						País:

						<h2 style="color:#333" >Datos de envío:</h2>
						Dirección:
						Población:
						Provincia:
						País:
						Código:


						<h2 style="color:#333" >Resúmen de la compra:</h2>
						<table border="0" cellpadding="3" cellspacing="0" width="650" style="font: 9pt Arial, Sans-serif;border-collapse: collapse;" >
							<tr style="background:#EDC159;color:#FFF;" ><td style="padding:8px 5px;border: 1px solid #EDC159;" ><b>Campo</b></td><td style="border: 1px solid #EDC159;padding:8px 5px;" ><b>Dato</b></td></tr>
							<tr valign="middle" ><td style="padding:8px 5px;border: 1px solid #BBB;" >Nombre:</td><td style="border: 1px solid #BBB;padding:8px 5px;" ><b>'. $parametros['name'] .'</b></td></tr>
							<tr valign="middle" ><td style="padding:8px 5px;border: 1px solid #BBB;" >Email:</td><td style="border: 1px solid #BBB;padding:8px 5px;" ><b>'. $parametros['email'] .'</b></td></tr>
							<tr valign="middle" ><td style="padding:8px 5px;border: 1px solid #BBB;" >Idioma:</td><td style="border: 1px solid #BBB;padding:8px 5px;" ><b>'. $parametros['idioma'] .'</b></td></tr>
							<tr valign="middle" ><td style="padding:8px 5px;border: 1px solid #BBB;" >Mensaje:</td><td style="border: 1px solid #BBB;padding:8px 5px;" ><b>'. $parametros['message'] .'</b></td>
						</table>';
					break;
					case 'compra':
						$asunto = "Agroinnova :: Resumen de tu pedido";

						$contenido .= '
						Hemos procesado tu pedido realizado a través de <a style="color: '.$color.'" href="' . $raizURL . '" target="_blank" >www.agroinnova.com</a>.<br />
						Te remitimos un resumen del mismo:<br /><br />';

						$contenido .= '
						<table border="0" cellpadding="5" cellspacing="0" style="font: 9pt Arial, Sans-serif;border-collapse: collapse;width: 100%;" >
							<tr style="background:'.$color.';color:#FFF;" ><td style="border: 1px solid #999;" colspan="2" ><b>DATOS DE FACTURACIÓN</b></td></tr>';

						if ($parametros['datosFacturacion']['tipoCliente'] == "empresa") {
							$contenido .= '
							<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >Empresa:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. $parametros['datosFacturacion']['empresa'] .'</b></td></tr>
							<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >NIF/CIF:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. $parametros['datosFacturacion']['nifCif'] .'</b></td></tr>';
						}

						$contenido .= '
							<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >Nombre:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. $parametros['datosFacturacion']['nombre'] .'</b></td></tr>
							<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >Apellidos:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. $parametros['datosFacturacion']['apellidos'] .'</b></td></tr>';

						if ($parametros['datosFacturacion']['telefono'] != "") {
							$contenido .= '
							<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >Teléfono:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. $parametros['datosFacturacion']['telefono'] .'</b></td></tr>';
						}
						
						$contenido .= '
							<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >E-mail:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. $parametros['datosFacturacion']['email'] .'</b></td></tr>
							<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >Dirección:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. $parametros['datosFacturacion']['direccion'] .'</b></td></tr>
							<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >Código Postal:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. $parametros['datosFacturacion']['codigoPostal'] .'</b></td></tr>
							<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >Población:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. $parametros['datosFacturacion']['poblacion'] .'</b></td></tr>
							<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >Provincia:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. $parametros['datosFacturacion']['provincia'] .'</b></td></tr>
							<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >País:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. $parametros['datosFacturacion']['pais'] .'</b></td></tr>
							<tr style="height: 10px;"><td colspan="2" ></td></tr>
							<tr style="background:'.$color.';color:#FFF;" ><td style="border: 1px solid #999;" colspan="2" ><b>DATOS DE ENVÍO</b></td></tr>';


						$contenido .= '
								<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >Nombre:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. $parametros['datosEnvio']['nombre'] .'</b></td></tr>
								<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >Apellidos:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. $parametros['datosEnvio']['apellidos'] .'</b></td></tr>';

							if ($parametros['datosEnvio']['telefono'] != "") {
								$contenido .= '
								<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >Teléfono:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. $parametros['datosEnvio']['telefono'] .'</b></td></tr>';
							}

						$contenido .= '
								<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >Dirección:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. $parametros['datosEnvio']['direccion'] .'</b></td></tr>
								<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >Código Postal:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. $parametros['datosEnvio']['codigoPostal'] .'</b></td></tr>
								<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >Población:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. $parametros['datosEnvio']['poblacion'] .'</b></td></tr>
								<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >Provincia:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. $parametros['datosEnvio']['provincia'] .'</b></td></tr>
								<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >País:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. $parametros['datosEnvio']['pais'] .'</b></td></tr>';


						$contenido .= '
							<tr style="height: 10px;"><td colspan="2" ></td></tr>
							<tr style="background:'.$color.';color:#FFF;" ><td style="border: 1px solid #999;" colspan="2" ><b>DATOS DE PEDIDO</b></td></tr>
							<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >Número de pedido:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. $parametros['pedido']['numeroPedido'] .'</b></td></tr>
							<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >Fecha:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. $parametros['pedido']['fecha'] .'</b></td></tr>
							<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >Importe total:</td><td style="background: #FFF;border: 1px solid #999;" ><b>'. number_format($parametros['pedido']['total'], 2, ',', '.') .'&nbsp;€</b></td></tr>
							<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" >Forma de pago:</td><td style="background: #FFF;border: 1px solid #999;" >';
							
						switch ($parametros['pedido']['formaDePago']) {
							case 'tarjeta':
								$contenido .= '
								<b>Tarjeta de crédito</b><br />
								En cuanto verifiquemos tu pago procederemos a la preparación y envío de tu pedido.';
							break;
	
							case 'transferencia':
								$contenido .= '
								<b>Transferencia bancaria</b><br />
								El número de cuenta en el que deberás realizar el ingreso del importe total de la compra es el <b> ******** nuemero de cuenta ******</b>.<br />
								En el momento en que recibamos y verifiquemos el pago procederemos a la preparación y envío de tu pedido.<br />
								Para agilizar el proceso puedes enviarnos un e-mail a agroinnova@agroinnova.com con el resguardo de ingreso.';
							break;
						}

						$contenido .= '
							</td></tr>
						</table><br />';

						$contenido .= '
						<h3 style="color: '.$color.'; margin-bottom: 5px;" >PRODUCTOS:</h3>
						<table border="0" cellpadding="4" cellspacing="0" style="font: 9pt Arial, Sans-serif;border-collapse: collapse;margin-top: 10px; width: 100%;" >
							<tr style="background:'.$color.';color:#FFF;" ><td style="border: 1px solid #999;" ><b>DESCRIPCIÓN</b></td><td style="border: 1px solid #999;" ><b>UNIDADES</b></td><td style="border: 1px solid #999;" ><b>PRECIO/UNIDAD</b></td><td style="border: 1px solid #999;" ><b>SUBTOTAL</b></td></tr>';
						
						$totalProductos = 0;

						
						foreach ($parametros['pedido']['productos'] as $producto) {
							$descripcion = $producto[3] . '<br />' . mb_strtoupper($producto[4], 'UTF-8') . ' ' . $producto[6] . '</a>';
						

							$precio = $producto[8] > 0 ? ('<s style="color: #999;">' . number_format($producto[7], 2, ',', '.') . "&nbsp;€</s><br />" . number_format($producto[8], 2, ',', '.')) . "&nbsp;€" : number_format($producto[7], 2, ',', '.') . "&nbsp;€";

							$subtotal = ($producto[8] > 0 ? $producto[8] : $producto[7]) * $producto[6];
							$totalProductos += $subtotal;

							$contenido .= '
								<tr>
									<td style="background: #FFF;border: 1px solid #999;" >'.$descripcion.'</td>
									<td style="background: #FFF;border: 1px solid #999; text-align: center;" >'.$producto[6].'</td>
									<td style="background: #FFF;border: 1px solid #999; text-align: right;" >'.$precio.'</td>
									<td style="background: #FFF;border: 1px solid #999; text-align: right;" >'. number_format($subtotal, 2, ',', '.') .'&nbsp;€</td>
								</tr>';
						}


						$contenido .= '
							<tr style="height: 5px;"><td colspan="4" ></td></tr>
							<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" colspan="3" >Total productos:</td><td style="background: #FFF;border: 1px solid #999;text-align: right;" ><b>'. number_format($totalProductos, 2, ',', '') .'&nbsp;€</b></td></tr>';
						
						$contenido .= '
							<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;" colspan="3" >Gastos de envío:</td><td style="background: #FFF;border: 1px solid #999;text-align: right;" ><b>'. number_format($parametros['pedido']['gastosEnvio'], 2, ',', '') .'&nbsp;€</b></td></tr>
							<tr style="height: 5px;"><td colspan="4" ></td></tr>
							<tr valign="top" ><td style="background: #FFF;border: 1px solid #999;color: '.$color.'; font-size: 12pt;" colspan="3" >Total</td><td style="background: #FFF;border: 1px solid #999;color: '.$color.'; font-size: 12pt;text-align: right;" ><b>'. number_format($parametros['pedido']['total'], 2, ',', '') .'&nbsp;€</b></td></tr>
						</table>
						<br /><br />
						Si tienes cualquier pregunta no dudes en ponerte en contacto con nosotros.<br />
						Muchas gracias por tu compra en <a style="color: '.$color.'" href="' . $raizURL . '" target="_blank" >www.agroinnova.com</a>.';
					break;
				}
				
				
				// PIE DEL MAIL
				$contenido .='<br/><br/>
									Saludos.
								</td>
							</tr>
							<tr height="30" >
								<td></td>
							</tr>
							<tr>
								<td style="color:#AAAAAA;font:7pt Arial, Sans-serif;padding-right:10px">
									Disclaimer:<br/>
									This message is intended exclusively for its addressee and may contain privileged or confidential information.
									If not vd. the recipient you are hereby notified that any use, dissemination and / or unauthorized copying is
									prohibited under current legislation. If you have received this email in error, please notify us immediately by electronic mail or delete it.
								</td>
							</tr>
						</table>
						</diV>
					</body>
				</html>';
				
				global $b;
				
				$correoClase = new PHPMailer();
				
				$correoClase->ClearAllRecipients();
				$correoClase->ClearAttachments();
				$correoClase->ClearCustomHeaders();
				
				$correoClase->From = $remite[1];
				$correoClase->FromName = $remite[0];
				$correoClase->Subject = $asunto;
				$correoClase->AddAddress($destino[1], $destino[0]);
				
				if($copiaCC) {
					foreach ($copiaCC as $destinatario) {
						$correoClase->AddCC($destinatario[1], $destinatario[0]);
					}
				}

				if($copiaBCC) {
					foreach ($copiaBCC as $destinatario) {
						$correoClase->AddBCC($destinatario[1], $destinatario[0]);
					}
				}
				
				$correoClase->IsHTML(true);
				$correoClase->CharSet = 'UTF-8';
				$correoClase->Body = $contenido;
				
				if(isset($adjuntoTmpName) && isset($adjuntoName))
					$correoClase->AddAttachment($adjuntoTmpName, $adjuntoName);
				
				$correoClase->Mailer = "smtp";
				$correoClase->Host = $this->objClsPreferencias->getValue('forms_smtp_server');
				$correoClase->Port = $this->objClsPreferencias->getValue('forms_smtp_port');
				$correoClase->Timeout = 30;
				$correoClase->SMTPAuth = $this->objClsPreferencias->getValue('forms_smtp_auth')==1;
				$correoClase->Username = $this->objClsPreferencias->getValue('forms_smtp_user');
				$correoClase->Password = $this->objClsPreferencias->getValue('forms_smtp_password');
				
				$resp = $correoClase->Send();
			}
			
			return $resp;
		}
	}