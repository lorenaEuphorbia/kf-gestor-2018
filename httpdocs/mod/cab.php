<?php

	$tituloWeb = (isset($tituloWeb) ? "$tituloWeb | " :'') . $_objClsPreferencias->getValue('general_titulo_web') ;
	$metaTitle = isset($metaTitle) && !empty($metaTitle) ? $metaTitle : $tituloWeb;
	$metaDescription = isset($descripcionWeb) && !empty($descripcionWeb) ? $descripcionWeb : $_objClsPreferencias->getValue('general_meta_description');
	$metaKeywords = isset($metaKeywords) && !empty($metaKeywords) ? $metaKeywords : $_objClsPreferencias->getValue('general_meta_keywords');
	$ogSiteName =  isset($ogSiteName) && !empty($ogSiteName) ? $ogSiteName : $_objClsPreferencias->getValue('general_titulo_web');
	$ogTitle =  isset($ogTitle) && !empty($ogTitle) ? $ogTitle : $tituloWeb;
	$ogDescription =  isset($ogDescription) && !empty($ogDescription) ? $ogDescription : $metaDescription;

	$arrHistoryMenu = array(
		'/',
		'/es/',
		'/es/quienes-somos/',
		'/es/nuestras-marcas/',
		'/es/calidad/',
		'/es/seguridad-alimentaria/',
		'/es/innovacion/',
		'/es/responsabilidad-social/',
		'/es/tienda-gran-seleccion/',
		'/es/tus-datos/',
		'/es/noticias/',
		'/es/contacto/'
	);

	if ($_objClsBase->security->isLogin) {
		$arrMenu = array(
			'home',
			'quienes-somos',
			'nuestras-marcas',
			'calidad',
			'seguridad-alimentaria',
			'innovacion',
			'responsabilidad-social',
			'tienda-gran-seleccion',
			'tus-datos',
			'logout',
			'noticias',
			'contacto',
			);
	}
	else {		
		$arrMenu = array(
			'home',
			'quienes-somos',
			'nuestras-marcas',
			'calidad',
			'seguridad-alimentaria',
			'innovacion',
			'responsabilidad-social',
			'tienda-gran-seleccion',
			'login-registro',
			'noticias',
			'contacto',
			);
	}


?><!doctype html>
	<html lang="<?= $idioma ?>">
	<head>
		<title><?= $tituloWeb; ?></title>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" >
		<meta charset="utf-8" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description"        content="<?= $metaDescription ?>" >

		<meta http-equiv="Title"        lang="<?= $idioma ?>" content="<?= $metaTitle ?>" >
		<meta name="DC.Title"           lang="<?= $idioma ?>" content="<?= $metaTitle ?>" >
		<meta name="Title"              lang="<?= $idioma ?>" content="<?= $metaTitle ?>" >
		<meta name="Description"        lang="<?= $idioma ?>" content="<?= $metaDescription ?>" >
		<meta name="DC.Description"     lang="<?= $idioma ?>" content="<?= $metaDescription ?>" >
		<meta name="Keywords"           lang="<?= $idioma ?>" content="<?= $metaKeywords ?>" >

		<!-- Schema.org markup for Google+ -->
		<meta itemprop="name" content="<?= $metaTitle ?>">
		<meta itemprop="description" content="<?= $metaDescription ?>">

		<!-- Twitter Card data -->
		<meta name="twitter:card" content="summary">
		<meta name="twitter:site" content="@<?= $_objClsPreferencias->getValue('contacto_twitter') ?>">
		<meta name="twitter:title" content="<?= $metaTitle ?>">
		<meta name="twitter:description" content="<?= substr($metaDescription, 200) ?>">
		<meta name="twitter:creator" content="@<?= $_objClsPreferencias->getValue('contacto_twitter') ?>">

		<!-- Open Graph data -->
		<meta property="og:url"         content="http://www.nytimes.com/2015/02/19/arts/international/when-great-minds-dont-think-alike.html" />
		<meta property="og:site_name"   content="<?= $ogSiteName ?>" />
		<meta property="og:title"       content="<?= $ogTitle ?>" />
		<meta property="og:description" content="<?= $ogDescription ?>" />
		<meta property="og:type" 		content="article" />

		<meta name="Copyright"          content="Euphorbia Comunicación SL (euphorbia@euphorbia.es): diseño, Euphorbia Comunicación SL (euphorbia@euphorbia.es): desarrollo. Copyright 2015 - "<?= date("Y") ?>>
		<meta name="Resource-Type"      content="document">
		<meta name="Robots"             content="index,follow,all">
		<meta name="Revisit-After"      content="7 days">
		<meta name="Revisit"            content="7 days">

		<?php if (isset ($css) && $css): ?>
		<?php foreach ($css as $cssFile): ?>
		<link rel="stylesheet" href="<?= $cssFile; ?>" />
		<?php endforeach ?>
		<?php endif ?>

		<script>window.jQuery || document.write('<script src="/js/jquery-1.11.3.min.js"><\/script>')</script>

		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800" rel="stylesheet"><link rel="stylesheet" href="/css/font-awesome.min.css" >

		<?php if (isset ($js) && $js): ?>
		<?php foreach ($js as $jsFile): ?>
		<script src="stylesheet" href="<?= $jsFile; ?>" ></script>
		<?php endforeach ?>
		<?php endif ?>

		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<link rel="stylesheet" href="/css/b.css" >

		<script src="/js/b.min.js" ></script>

		<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

	</head>
	<body >
		<header>
			<div class="contenido">
				<a href="/" class="logo out" id="logo-header">
					<img src="/img/comunes/logotipo.png" alt="logotipo <?= $tituloWeb ?>">
					<img src="/img/comunes/agroinnova.png" alt="favicon <?= $tituloWeb ?>">
				</a>

				<div class="enlaces">
					<a href="javascript:void(0);" id="bt-contacto"><i class="fa fa-map-marker"></i></a>
					<a href="javascript:void(0);" id="bt-menu"><i class="fa fa-bars"></i></a>
					<a href="javascript:void(0);" id="bt-carrito"><i class="fa fa-shopping-cart"></i></a>
				</div>
				<div class="redes">
					<a href="https://www.facebook.com/<?= $_objClsPreferencias->getValue('contacto_facebook') ?>" target="_blank"><img src="/img/comunes/facebook.png" alt="facebook"></a>
					<a href="https://twitter.com/<?= $_objClsPreferencias->getValue('contacto_twitter') ?>" target="_blank"><img src="/img/comunes/twitter.png" alt="twitter"></a>
					<a href="https://www.instagram.com/<?= $_objClsPreferencias->getValue('contacto_instagram') ?>/" target="_blank"><img src="/img/comunes/instagram.png" alt="instagram"></a>

				</div>
			</div>
		</header>
		<div class="menu fuera">
			<nav class="fuera bg-amarillo">
				<ul id="el-menu">
				<?php foreach ($arrMenu as $bt): ?>

					<?php if($bt == 'home'): ?>
					<li><a href="/<?php echo $idioma;?>/"><?php echo $textosIdiomas[$bt];?></a></li>

					<?php else: ?>
					<li><a class="<?php if($idPag == $bt) echo "picado";?>" href="/<?php echo $idioma;?>/<?php echo $bt; ?>/" ><?php echo $textosIdiomas[$bt];?></a></li>

					<?php endif ?>
				<?php endforeach ?>
				</ul>
				<ul id="la-direccion" >
					<?= $_objClsPreferencias->getValue('contacto_direccion') ?>
				</ul>
				<ul id="el-carrito" class="carrito">

					<?php require 'carrito-detalle.php' ?>

				</ul>
			</nav>
			<span id="cerrar" class="fuera">
				<i class="fa fa-times"></i> <?= $textosIdiomas['cerrar'] ?> </span>
			</span>
		</div>
		<main class="content">


