<?php



	/// Cargamos otras clases necesarias (estáticas)
	require $_SERVER['DOCUMENT_ROOT'] . '/gestor/_inc/config.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/gestor/_inc/functions.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-utilities.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-domElements.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-controller.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-adminPreferences.php';

	setAutoloader();

	/// Arrancamos el panel de control
	$i18n = new I18n("admin", $lang_admin);

	/* __init__ */
	$_objClsBase = $b = new Base();
	$_objClsPreferencias = &$_objClsBase->preferences;
	$_objClsNoticias = new Noticias($_objClsBase->db);

	/* IDIOMA */
	$arrIdiomas = ['es'=>'Español','en'=>'Inglés'];
	$idioma = (isset($_REQUEST["lang"]) && isset($arrIdiomas[$_REQUEST['lang']]))? $_REQUEST['lang']: array_keys($arrIdiomas)[0];
	@include $_SERVER['DOCUMENT_ROOT'] . "/idiomas/$idioma.php";

	/* URL ACTUAL */
	$strCurrentUrl = $_SERVER['REQUEST_URI'];

	/// Recogemos los datos de los formularios
	if(!empty($_REQUEST))
	require $_SERVER['DOCUMENT_ROOT'] . '/mod/request.php';

	//Moblie detct
	$mobile = Utilities::isMobile();



