<?php

	$mensaje = '';
	$resultado = false;
	$jsonResult = false;
	$reload = false;
	$urlResult = $strCurrentUrl;

	$id = !empty($_REQUEST['id']) ? $_REQUEST['id'] : '';
	$id = preg_replace('/[^\w-]+/u', '', $id);

	$method = in_array($_SERVER['REQUEST_METHOD'],['GET','POST'])? $_SERVER['REQUEST_METHOD']: false;
	$accion = isset($_REQUEST['accion']) && !empty($_REQUEST['accion']) ?  $_REQUEST['accion'] : null;

	switch ($id) {
		case 'usuario': 

			# campos obligatorios
			$strClave = isset($_REQUEST['clave']) && !empty($_REQUEST['clave']) ? $_REQUEST['clave'] : false;
			$strClave2 = isset($_REQUEST['clave2']) && !empty($_REQUEST['clave2']) ?  $_REQUEST['clave2'] : false;
			$strClaveActual = isset($_REQUEST['clave-actual']) && !empty($_REQUEST['clave-actual']) ? $_REQUEST['clave-actual'] : false;
			$strEmail = isset($_REQUEST['email']) && !empty($_REQUEST['email']) ? preg_replace("/[^a-zA-Z0-9_\-@.]+/", "", $_REQUEST['email']) : false;
			$isTerminos = isset($_REQUEST['terminos']) && !empty($_REQUEST['terminos']) && $_REQUEST['terminos']==true;
			# campos opcionales al registro
			$strNombre = isset($_REQUEST['nombre']) && !empty($_REQUEST['nombre']) ?  $_REQUEST['nombre'] : null;
			$strApellidos = isset($_REQUEST['apellidos']) && !empty($_REQUEST['apellidos']) ?  $_REQUEST['apellidos'] : null;
			$strTelefono = isset($_REQUEST['telefono']) && !empty($_REQUEST['telefono']) ?  $_REQUEST['telefono'] : null;
			$strFechaNacimiento = isset($_REQUEST['fecha-nacimiento']) && !empty($_REQUEST['fecha-nacimiento']) ?  $_REQUEST['fecha-nacimiento'] : null;
			$isNewsletter = isset($_REQUEST['newsletter']) && !empty($_REQUEST['newsletter']) && $_REQUEST['newsletter']==true ? '1': '0';
			# datos facturacion
			$strRazon = isset($_REQUEST['razon']) && !empty($_REQUEST['razon']) ?  $_REQUEST['razon'] : '';
			$strCif = isset($_REQUEST['cif']) && !empty($_REQUEST['cif']) ?  $_REQUEST['cif'] : '';
			$strFacturacionDireccion = isset($_REQUEST['direccion-facturacion']) && !empty($_REQUEST['direccion-facturacion']) ?  $_REQUEST['direccion-facturacion'] : null;
			$strFacturacionLocalidad = isset($_REQUEST['localidad-facturacion']) && !empty($_REQUEST['localidad-facturacion']) ?  $_REQUEST['localidad-facturacion'] : null;
			$strFacturacionProvincia = isset($_REQUEST['provincia-facturacion']) && !empty($_REQUEST['provincia-facturacion']) ?  $_REQUEST['provincia-facturacion'] : null;
			$strFacturacionCodigoPostal = isset($_REQUEST['cp-facturacion']) && !empty($_REQUEST['cp-facturacion']) ?  $_REQUEST['cp-facturacion'] : null;
			$strFacturacionPais = isset($_REQUEST['pais-facturacion']) && !empty($_REQUEST['pais-facturacion']) ?  $_REQUEST['pais-facturacion'] : '1';
			# datos envio
			$strEnvioNombre = isset($_REQUEST['nombre-envio']) && !empty($_REQUEST['nombre-envio']) ?  $_REQUEST['nombre-envio'] : null;
			$strEnvioDireccion = isset($_REQUEST['direccion-envio']) && !empty($_REQUEST['direccion-envio']) ?  $_REQUEST['direccion-envio'] : null;
			$strEnvioLocalidad = isset($_REQUEST['localidad-envio']) && !empty($_REQUEST['localidad-envio']) ?  $_REQUEST['localidad-envio'] : null;
			$strEnvioProvincia = isset($_REQUEST['provincia-envio']) && !empty($_REQUEST['provincia-envio']) ?  $_REQUEST['provincia-envio'] : null;
			$strEnvioCodigoPostal = isset($_REQUEST['cp-envio']) && !empty($_REQUEST['cp-envio']) ?  $_REQUEST['cp-envio'] : null;
			$strEnvioPais = isset($_REQUEST['pais-envio']) && !empty($_REQUEST['pais-envio']) ?  $_REQUEST['pais-envio'] : '1';
			# flags formulario

			switch ($method) {

				case 'GET':
					switch ($accion) {

						# logout	
						case 'logout':						
							$urlResult = $_objClsBase->getLastPage();
							$urlResult = empty($urlResult) || $urlResult==='/'? $urlResult: '/' . $idioma . $urlResult;

							$_objClsBase->security->sessionEnd();
							$_objClsBase->orders->init();							

							$resultado = true;
							$mensaje = 'Hasta pronto!';
							break;

					}
				break;

				case 'POST':
					switch ($accion) {

						# login	
						case 'login':						
							if ($strClave!==false && $strEmail!==false) {
								if (Utilities::checkEmail($strEmail) && 
									Utilities::checkPassword($strClave) && 
									$_objClsBase->security->login($strEmail, $strClave))
								{
									// var_dump($_objClsBase->carrito->objOrder['id']); exit;
									$_objClsBase->carrito->updateUserOrder($_objClsBase->security->data);

									$resultado = true;
									$mensaje = 'Usuario correcto';
									$urlResult = '/' . $idioma . $_objClsBase->getLastPage();
								}
								else {
									$mensaje = 'Usuario o contraseña incorrectos';
								}
							}
							else {
								$mensaje = 'Debes rellenar todos los campos';
							}
							break;

						# nuevo usuario
						case 'nuevo':
							if ($strClave!==false && $strClave2!==false && $strEmail!==false) {
								if ($strClave === $strClave2) {
									if (true || $isTerminos===true) {
										if (Utilities::checkEmail($strEmail)) {
											if (Utilities::checkPassword($strClave)) {
												if ($num = $_objClsBase->users->newUser($strEmail, $strClave, 0)) {
													$_objClsBase->security->login($strEmail, $strClave);
													$_objClsBase->carrito->updateUserOrder($_objClsBase->security->data);

													$mensaje = 'Usuario creado correctamente';
													$urlResult = '/' . $idioma . $_objClsBase->getLastPage();
													$resultado = true;
												}
												else {
													$mensaje = 'Ya exite un usuario con es email';
												}
											}
											else {
												$mensaje = 'La contraseña debe tener al menos 6 caracteres y contener como mínimo una letra y un número';
											}
										}
										else {
											$mensaje = 'El formato de email no es correcto';
										}
									}
									else {
										$mensaje = 'Debes aceptar los términos';
									}
								}
								else {
									$mensaje = 'Las contraseñas no coinciden';
								}
							}
							else {
								$mensaje = 'Debes rellenar todos los campos';
							}
							break;
						
						# actualizar datos usuario
						case 'editar': 
							if ($_objClsBase->security->isLogin) {
								if($strEmail!==false) {
									if (true ||$_objClsBase->security->checkUser($strEmail, $strClaveActual)) {
										$arrParams = [
											'nombre'					=> $strNombre,
											'apellidos'					=> $strApellidos,
											'telefono'					=> $strTelefono,
											'fecha_nacimiento'			=> $strFechaNacimiento,
											'newsletter'				=> $isNewsletter,
											'razon'						=> $strRazon,
											'cif'						=> $strCif,
											'facturacion_direccion'		=> $strFacturacionDireccion,
											'facturacion_localidad'		=> $strFacturacionLocalidad,
											'facturacion_provincia'		=> $strFacturacionProvincia,
											'facturacion_codigo_postal'	=> $strFacturacionCodigoPostal,
											'facturacion_pais'			=> $strFacturacionPais,
											'envio_nombre'				=> $strEnvioNombre,
											'envio_direccion'			=> $strEnvioDireccion,
											'envio_localidad'			=> $strEnvioLocalidad,
											'envio_provincia'			=> $strEnvioProvincia,
											'envio_codigo_postal'		=> $strEnvioCodigoPostal,
											'envio_pais'				=> $strEnvioPais,
										];

										if ($_objClsBase->users->setData($_objClsBase->security->userId, $arrParams)) {
											$_objClsBase->carrito->updateUserOrder($arrParams);

											$mensaje = 'Datos actualizados';
											$urlResult = $_objClsBase->getLastPage();
											$urlResult = empty($urlResult) || $urlResult==='/'? $urlResult: '/' . $idioma . $urlResult;
											$resultado = true;
										}
										else {
											$mensaje = 'Datos actualizados';
										}
									}
									else {
										$mensaje = 'La contraseña no es correcta';
									}
								}
								else {
									$mensaje = 'Debes rellenar todos los campos';
								}
							}
							else {
								$mensaje = 'Debes estar logueado';						
							}
							break;
						
						# actualizar datos usuario
						case 'cambiar-clave': 
							if ($_objClsBase->security->isLogin) {
								if(!empty($strClaveActual) && !empty($strClave) && !empty($strClave2)) {
									if ($strClave == $strClave2 ) {
										if ($_objClsBase->security->checkUser($_objClsBase->security->username, $strClaveActual)) {										

											if($_objClsBase->users->setPassword($_objClsBase->security->userId, $strClave) && 
												$_objClsBase->security->login($_objClsBase->security->username, $strClave)) {												
												$mensaje = 'Datos actualizados';
												$resultado = true;
											}
											else {
												$mensaje = 'No se puede modificar la contraseña';
											}
										}
										else {
											$mensaje = 'La contraseña antigua no es correcta';
										}
									}
									else {
										$mensaje = 'Las contraseñas no coinciden';
									}
								}
								else {
									$mensaje = 'Debes rellenar todos los campos';
								}
							}
							else {
								$mensaje = 'Debes estar logueado';						
							}
							break;
						
						# dar de baja usuario	
						case 'baja':						
							if ($_objClsBase->security->isLogin) {
								if($strEmail!==false && $strClaveActual!==false) {
									if ($_objClsBase->security->checkUser($strEmail, $strClaveActual)) {
										if ($_objClsBase->users->bajaUser($strEmail)) {
											$mensaje = 'Te has dado de baja';
											$urlResult = '/' . $idioma . $_objClsBase->getLastPage();
											$resultado = true;
										}
										else {
											$mensaje = 'Error al darse de baja';
										}
									}
									else {
										$mensaje = 'Usuario o contraseña incorrectos';
									}
								}
								else {
									$mensaje = 'Debes rellenar todos los campos';
								}
							}
							else {
								$mensaje = 'Debes estar logueado';	
							}
							break;
						
						default:
							$mensaje = 'Esa accion no existe';
							break;
					}
				break;

			}
		break;

		case 'recuperarPassword': //envia email con enlace al cambio de contraseña
			switch ($accion) {

				case 'POST':				
					# campos obligatorios
					$strEmail = isset($_REQUEST['email']) && !empty($_REQUEST['email']) ? preg_replace("/[^a-zA-Z0-9_\-@.]+/", "", $_REQUEST['email']) : false;
					
					if($strEmail!==false) {
						if (Utilities::checkEmail($strEmail)) {
							if ($_objClsBase->users->searchUser(null, $strEmail)) {
				                $_objClsCorreos = new Correos($b,$_objClsPreferencias);
				                if($_objClsCorreos->RecuperarPassword(
				                        'contacto',//tipo
				                        $idioma,//idioma
				                        array('Contacto Agroinnova Web: ' . $arrCampos['name'], $email),
				                        array("Agroinnova", $_objClsPreferencias->getValue('forms_email') ), // destino
				                        array(), //cc
				                        array(), // bcc
				                        $arrCampos // parametros
				                    ) &&
					                	$_objClsCorreos->EnviaCorreo(
					                        'contacto-cliente',//tipo
					                        $idioma,//idioma
					                        array('Agroinnova Web', $_objClsPreferencias->getValue('forms_email')),
					                        array($arrCampos['name'], $email ), // destino
					                        array(), //cc
					                        array(), // bcc
					                        $arrCampos // parametros
					                    )
				                	) {
				                    $mensaje = "Tu consulta se ha enviado con éxito. Muchas gracias.";
				                    $resultado = true;
				                } 
				                else { 
				                    $mensaje = "Ha habido un error con el envío, intentelo pasados unos minutos. Si el error persiste contacte con el Administrador, por favor.";
				                }
							}
							else {
								$mensaje = 'No existe una cuenta con ese email';
							}
						}
						else {
							$mensaje = 'El email tiene un formato incorrecto';
						}
					}
					else {
						$mensaje = 'Debes introducir todos los datos';
					}

			    break;
			    
			}
		break;

		case 'contacto':

			# campos obligatorios
			$isTerminos = isset($_REQUEST['terminos']) && !empty($_REQUEST['terminos']) && $_REQUEST['terminos']==true;
			$strEmail = isset($_REQUEST['email']) && !empty($_REQUEST['email']) ? preg_replace("/[^a-zA-Z0-9_\-@.]+/", "", $_REQUEST['email']) : false;
			# campos opcionales al registro
			$strNombre = isset($_REQUEST['nombre']) && !empty($_REQUEST['nombre']) ?  $_REQUEST['nombre'] : null;
			$strMensaje = isset($_REQUEST['apellidos']) && !empty($_REQUEST['apellidos']) ?  $_REQUEST['apellidos'] : null;
			$strTelefono = isset($_REQUEST['telefono']) && !empty($_REQUEST['telefono']) ?  $_REQUEST['telefono'] : null;
			switch ($accion) {

				case 'POST':

					if ($isTerminos!==false && $strEmail!=false) {

				        $arrCampos = array ();
						if($strEmail!==null) $arrCampos['Email'];
						if($strNombre!==null) $arrCampos['Nombre'];
						if($strMensaje!==null) $arrCampos['Mensaje'];
						if($strTelefono!==null) $arrCampos['Telefono'];

						if (Utilities::checkEmail($strEmail)) {

			                include 'mod/Correos.clase.php';
			                $_objClsCorreos = new Correos($b,$_objClsPreferencias);
			                if($_objClsCorreos->EnviaCorreo(
			                        'contacto',//tipo
			                        $idioma,//idioma
			                        array('Contacto Agroinnova Web: ' . $arrCampos['name'], $email),
			                        array("Agroinnova", $_objClsPreferencias->getValue('forms_email') ), // destino
			                        array(), //cc
			                        array(), // bcc
			                        $arrCampos // parametros
			                    ) &&
				                	$_objClsCorreos->EnviaCorreo(
				                        'contacto-cliente',//tipo
				                        $idioma,//idioma
				                        array('Agroinnova Web', $_objClsPreferencias->getValue('forms_email')),
				                        array($arrCampos['name'], $email ), // destino
				                        array(), //cc
				                        array(), // bcc
				                        $arrCampos // parametros
				                    )
			                	) {
			                    $mensaje = "Tu consulta se ha enviado con éxito. Muchas gracias.";
			                    $resultado = true;
			                } 
			                else { 
			                    $mensaje = "Ha habido un error con el envío, intentelo pasados unos minutos. Si el error persiste contacte con el Administrador, por favor.";
			                }
			            } 
			            else {                 
			                $mensaje = "Debes introducir un email válido";
			            }
				    }
			        else  {
			           $mensaje = "Rellena todos los campos correctamente";
			        }
			    break;

			}
		break;

		case 'carrito': # devuelve productos del carrito de la compra
			# campos opcionales al registro
			$strArticulo = isset($_REQUEST['articulo']) && !empty($_REQUEST['articulo']) ?  $_REQUEST['articulo'] : null;
			$strUnidades = isset($_REQUEST['unidades']) && !empty($_REQUEST['unidades']) ?  $_REQUEST['unidades'] : null;
			$strMetodoPago = isset($_REQUEST['metodo-de-pago']) && !empty($_REQUEST['metodo-de-pago']) ?  $_REQUEST['metodo-de-pago'] : null;

			switch ($accion) {

				# añade unidades al producto
				case 'add':
					$jsonResult = true;
					if ($strArticulo!==false && $strUnidades!==false) {						
						if($strUnidades!=='0') {
							$resultado = $_objClsBase->carrito->addUnidades($strArticulo,$strUnidades) == true;
						}
						else {
			         		$mensaje = "No se pueden añaidir 0 unidades al carrito";
						}
					}
					else {
			           $mensaje = "Rellena todos los campos correctamente";			
					}
					break;

				# cambia las unidades del producto
				case 'set':
					$jsonResult = true;
					if ($strArticulo!==false && $strUnidades!==false) {		
						$resultado = $_objClsBase->carrito->setUnidades($strArticulo,$strUnidades) == true;
					}
					else {
			           $mensaje = "Rellena todos los campos correctamente";			
					}
					break;

				# borra un artículo del carrito
				case 'remove':
					$jsonResult = true;
					if ($strArticulo!==false) {			
						if($_objClsBase->carrito->removeProduct($strArticulo)) {
			         		$mensaje = "Artículo borrado";		
							$resultado = true;
						}
						else {
			         		$mensaje = "Ese artículo no existe";
						}
					}
					else {
			           $mensaje = "Rellena todos los campos correctamente";			
					}
					break;

				# devuelve el carrito
				case 'get':
					$jsonResult = true;
					$mensaje = $_objClsBase->carrito->getProducts();
				break;

				# pasarela del carrito
				case 'pagar-compra':
					if($strMetodoPago!==false) {
						if(!$_objClsBase->carrito->setMetodoDePago($strMetodoPago)) {
							trigger_error('El método de pago '.$strMetodoPago.' no existe');
						}
					}
					# validaciones en stock y todos los datos introducidos
					if($_objClsBase->security->isLogin) {
						if($_objClsBase->security->isUserDataCompleted()) {							
							switch ($_objClsBase->carrito->getMetodoDePago()) {
								case 'tarjeta':
									$urlResult = '/' . $idioma . '/banco-redireccion/';
									$resultado = true;
									break;
									
								case 'transferencia':
									$idPedido = $_objClsBase->carrito->sendRequest();
									$arrRequestData = $_objClsBase->orders->getRecord('Request', null, ['id'=>$idPedido]);

									if ($idPedido) {
										if($_objClsBase->orders->setRequestResult(null, $idPedido)) {

											# CORREOS
											if(!isset($correos)) $correos = new Correos($_objClsBase);
											# enviamos y guardamos el resultado
										    if($correos->sendOrderByID($arrRequestData['id_order'])) {
												$_objClsBase->orders->setCurrentOrder(['correo_resumen'=>'1']);										    	
				          						$mensaje = "Pedido realizado. Te llegará un mensaje a tu email con los pasos a seguir.";
										    }
										    else {										    				    	
				          						$mensaje = "Pedido realizado. Ha habido un error en el envío del correo con el resumen, póngase en contacto con nosotros, por favor.";
										    }

											$urlResult = '/' . $idioma . '/pedido-realizado/';
											$resultado = true;
										}
										else {
				          					$mensaje = "No se ha podido tramitar el pedido, intentelo de nuevo y si el problama persiste contacte con nosotros, por favor.";
										}
									}
									else {
										$urlResult = '/' . $idioma . $_objClsBase->getLastPage();
				          				$mensaje = "No se ha podido guardar el pedido, intentelo de nuevo y si el problama persiste contacte con nosotros, por favor.";
									}
									break;

								default:
									$urlResult = '/' . $idioma . $_objClsBase->getLastPage();
					          		$mensaje = "El método de pago seleccionado es incorrecto, intentelo de nuevo y si el problama persiste contacte con nosotros, por favor.";
					          		break;
							}
						}
						else {
							$urlResult = '/' . $idioma . '/tus-datos/';
			          		$mensaje = "Debes rellenar todos tus datos";
						}
					}
					else {
						$urlResult = '/' . $idioma . '/login-registro/';
		          		$mensaje = "Debes estar logueado";
					}
				break;
			}

			break;

		case 'addProducto': //añadir producto al carrito
			# code...
			break;

		case 'borraProducto': //eliminar producto del carrito
			# code...
			break;

		case 'transferencia': //envío de email resumen con numero de cuenta e instrucciones.
			break;

		case "datos-pago": //recoger datos de pago para compra y redirigir en consecuencia
			if (!empty($_REQUEST['fields'])) {
				parse_str($_REQUEST['fields'], $arrFields);

				foreach ($arrFields as $key => $value) {
					$arrFields[$key] = $_objClsBase->Limpia($value);
				}

				if (!isset($arrFields['metodo-de-pago']) || !in_array($arrFields['metodo-de-pago'], array('tarjeta', 'transferencia'))) {
					$mensaje = 'Por favor, selecciona un método de pago.';
					$resultado = false;
				}

				//echo "Error::"; print_r($arrFields); exit;

				$datos = array(
					"formaDePago"	=> $arrFields['metodo-de-pago']
				);

				if ($compras->PonSeccion($datos)) {
					switch ($datos['formaDePago']) {
						case 'tarjeta': $destino = "/" . $_objClsBase->idioma . "/" . $textosWEB['banco-redireccion-link'] . $compras->DevCampoSeccion("codigo") . "/"; break;
						case 'transferencia': $destino = "/" . $_objClsBase->idioma . "/" . $textosWEB['paypal-redireccion-link'] . $compras->DevCampoSeccion("codigo") . "/"; break;
					}

					$result = array(
						"result" => "Ok",
						"destino" => $destino
					);

					echo json_encode($result);
				}
				else {
					$mensaje = 'Lo sentimos, se ha producido un error al seleccionar el método de pago. Por favor, inténtalo pasados unos minutos, si el error persiste ponte contacto con nosotros.';
					$resultado = false;
				}

			}
			else {
				$mensaje = 'Error';
				$resultado = false;
			}
		break;

		default:
			# code...
		break;
	}


	$arrayJson = 
	[
		'mensaje' => $mensaje,
		'resultado' => $resultado,
		'reload' => $reload,
	];

	// echo json_encode($arrayJson); para llamadas tipo json
	if($resultado===true || !empty($mensaje)) {
		if($jsonResult) {			
			header('Content-Type: application/json');
			echo Utilities::encode($arrayJson);
			exit;
		}
		$_SESSION['RESULT'] = $arrayJson;
		header('Location: '.$urlResult);
		exit;
	}