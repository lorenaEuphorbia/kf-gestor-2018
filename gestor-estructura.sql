-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-08-2017 a las 07:36:29
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gestor`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formularios`
--

DROP TABLE IF EXISTS `formularios`;
CREATE TABLE `formularios` (
  `id` int(11) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `email` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `mensaje` text COLLATE utf8_spanish2_ci,
  `is_aviso` tinyint(1) NOT NULL DEFAULT '0',
  `is_correo` tinyint(1) DEFAULT '0',
  `creacion` timestamp NULL DEFAULT NULL,
  `modificacion` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idiomas`
--

DROP TABLE IF EXISTS `idiomas`;
CREATE TABLE `idiomas` (
  `id` int(11) NOT NULL,
  `codigo` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `region` varchar(5) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre_es` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `oculto` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idiomas__claves`
--

DROP TABLE IF EXISTS `idiomas__claves`;
CREATE TABLE `idiomas__claves` (
  `id` int(11) NOT NULL,
  `id_seccion` int(11) NOT NULL COMMENT 'sección',
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre identificativo según la sección',
  `type` varchar(20) COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'text'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idiomas__secciones`
--

DROP TABLE IF EXISTS `idiomas__secciones`;
CREATE TABLE `idiomas__secciones` (
  `id` int(11) NOT NULL,
  `slug` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(200) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `logo` varchar(200) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `position` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `oculto` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Disparadores `idiomas__secciones`
--
DROP TRIGGER IF EXISTS `insertar_claves_comunes`;
DELIMITER $$
CREATE TRIGGER `insertar_claves_comunes` AFTER INSERT ON `idiomas__secciones` FOR EACH ROW INSERT INTO idiomas__claves (`id_seccion`, `nombre`)
VALUES
		(NEW.id, 'url'),
		(NEW.id, 'nombre'),
		(NEW.id, 'titulo'),
		(NEW.id, 'subtitulo'),
		(NEW.id, 'meta_titulo'),
		(NEW.id, 'meta_descripcion')
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idiomas__textos`
--

DROP TABLE IF EXISTS `idiomas__textos`;
CREATE TABLE `idiomas__textos` (
  `id` int(11) NOT NULL,
  `id_clave` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `valor` text COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__archivos`
--

DROP TABLE IF EXISTS `noticias__archivos`;
CREATE TABLE `noticias__archivos` (
  `id` int(11) NOT NULL,
  `id_noticia` int(11) UNSIGNED NOT NULL,
  `creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `descripcion` varchar(1000) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__categorias`
--

DROP TABLE IF EXISTS `noticias__categorias`;
CREATE TABLE `noticias__categorias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `url` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `titulo` varchar(120) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `meta_titulo` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `meta_descripcion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `owner_id` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__categories`
--

DROP TABLE IF EXISTS `noticias__categories`;
CREATE TABLE `noticias__categories` (
  `id` int(11) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'url en español',
  `nombre` varchar(255) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre en español',
  `position` int(11) NOT NULL DEFAULT '0',
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__comments`
--

DROP TABLE IF EXISTS `noticias__comments`;
CREATE TABLE `noticias__comments` (
  `id` int(11) UNSIGNED NOT NULL,
  `post_id` int(11) UNSIGNED NOT NULL,
  `parent_id` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `number` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `author` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `content` text COLLATE utf8_spanish2_ci NOT NULL,
  `date` datetime DEFAULT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `moderated` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__files`
--

DROP TABLE IF EXISTS `noticias__files`;
CREATE TABLE `noticias__files` (
  `id` int(11) UNSIGNED NOT NULL,
  `owner_id` int(11) UNSIGNED NOT NULL COMMENT 'propietario (id)',
  `owner_type` varchar(50) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'propietario (mod)',
  `type` varchar(20) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'tipo de archivo',
  `title_1` varchar(250) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'título',
  `name` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre original',
  `mime_type` varchar(250) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'tipo recogido',
  `size` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'tamaño',
  `height` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'alto (si imagen)',
  `width` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ancho (si imagen)',
  `creacion` timestamp NULL DEFAULT NULL,
  `modificacion` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__galeria`
--

DROP TABLE IF EXISTS `noticias__galeria`;
CREATE TABLE `noticias__galeria` (
  `id` int(11) NOT NULL,
  `id_noticia` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `alt` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__posts`
--

DROP TABLE IF EXISTS `noticias__posts`;
CREATE TABLE `noticias__posts` (
  `id` int(11) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'url en español',
  `title` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'título en español',
  `meta_titulo` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `meta_descripcion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `intro` text COLLATE utf8_spanish2_ci COMMENT 'entradilla en español',
  `content` text COLLATE utf8_spanish2_ci COMMENT 'contenido en español',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'fecha de creación',
  `leido` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'veces visto',
  `publicado` tinyint(1) NOT NULL DEFAULT '1',
  `category_id` int(11) UNSIGNED DEFAULT NULL COMMENT 'id de categoría',
  `publishing_date` timestamp NULL DEFAULT NULL COMMENT 'fecha de publicación',
  `modification_date` timestamp NULL DEFAULT NULL COMMENT 'ultima modificación'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__rel_tags`
--

DROP TABLE IF EXISTS `noticias__rel_tags`;
CREATE TABLE `noticias__rel_tags` (
  `id` int(11) NOT NULL,
  `post_id` int(11) UNSIGNED NOT NULL COMMENT 'id de post',
  `tag_id` int(11) UNSIGNED NOT NULL COMMENT 'id de tag'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__tags`
--

DROP TABLE IF EXISTS `noticias__tags`;
CREATE TABLE `noticias__tags` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_tipo` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'url en español',
  `text` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'nombre en español'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(128) NOT NULL,
  `codigo_cliente` varchar(10) DEFAULT NULL,
  `nombre` varchar(60) DEFAULT NULL,
  `apellidos` varchar(160) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `nif` varchar(20) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `nif_dir_facturacion` varchar(20) DEFAULT NULL,
  `nombre_dir_facturacion` varchar(60) DEFAULT NULL,
  `codigo_dir_facturacion` varchar(10) DEFAULT NULL,
  `direccion_dir_facturacion` varchar(120) DEFAULT NULL,
  `cp_dir_facturacion` varchar(20) DEFAULT NULL,
  `localidad_dir_facturacion` varchar(60) DEFAULT NULL,
  `provincia_dir_facturacion` int(11) DEFAULT NULL,
  `pais_dir_facturacion` int(11) DEFAULT NULL,
  `codigo_dir_envio` varchar(10) DEFAULT NULL,
  `nombre_dir_envio` varchar(60) DEFAULT NULL,
  `direccion_dir_envio` varchar(120) DEFAULT NULL,
  `cp_dir_envio` varchar(20) DEFAULT NULL,
  `localidad_dir_envio` varchar(60) DEFAULT NULL,
  `provincia_dir_envio` int(11) DEFAULT NULL,
  `pais_dir_envio` int(11) DEFAULT NULL,
  `envioEmpresa` varchar(100) DEFAULT NULL,
  `envioImporte` float NOT NULL DEFAULT '0',
  `envioObservaciones` varchar(1000) DEFAULT NULL,
  `formaDePago` varchar(20) DEFAULT NULL,
  `is_factura` tinyint(1) NOT NULL DEFAULT '0',
  `is_envolver_regalo` tinyint(1) NOT NULL DEFAULT '0',
  `is_tarjeta_regalo` tinyint(1) NOT NULL DEFAULT '0',
  `str_tarjeta_regalo` varchar(1000) DEFAULT NULL,
  `vale` int(11) DEFAULT '0',
  `pvp_total` float NOT NULL DEFAULT '0',
  `peso_total` int(11) NOT NULL DEFAULT '0',
  `pagando` tinyint(1) NOT NULL DEFAULT '0',
  `pagandoFecha` datetime DEFAULT NULL,
  `bancoRespPre` mediumtext,
  `bancoRespNot` mediumtext,
  `bancoRespConfirm` mediumtext,
  `bancoRespCancel` mediumtext,
  `validacion` varchar(250) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `carrito` mediumtext,
  `request_id` int(11) DEFAULT NULL,
  `historico` tinyint(1) NOT NULL DEFAULT '0',
  `correo_resumen` tinyint(1) NOT NULL DEFAULT '0',
  `correo_envio` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders__articulos`
--

DROP TABLE IF EXISTS `orders__articulos`;
CREATE TABLE `orders__articulos` (
  `id` int(11) NOT NULL,
  `id_order` int(10) NOT NULL,
  `articulo` varchar(10) CHARACTER SET utf8 NOT NULL,
  `talla` varchar(3) CHARACTER SET utf8 DEFAULT NULL,
  `precio` float NOT NULL DEFAULT '0',
  `iva` float NOT NULL DEFAULT '0',
  `precio_iva` float NOT NULL DEFAULT '0',
  `unidades` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders__refunds`
--

DROP TABLE IF EXISTS `orders__refunds`;
CREATE TABLE `orders__refunds` (
  `id` int(11) NOT NULL,
  `NPedidoWeb` int(11) NOT NULL,
  `RefNum` varchar(15) NOT NULL,
  `Cliente` varchar(10) NOT NULL,
  `nombre` varchar(50) NOT NULL DEFAULT '',
  `direccion` varchar(40) NOT NULL DEFAULT '',
  `localidad` varchar(40) NOT NULL DEFAULT '',
  `codigo_postal` varchar(20) NOT NULL DEFAULT '',
  `pais` int(4) NOT NULL DEFAULT '0',
  `zona` int(4) NOT NULL DEFAULT '0',
  `metodo_pago` varchar(20) NOT NULL DEFAULT '',
  `transportista` varchar(5) NOT NULL DEFAULT '',
  `comentario` text,
  `mail` tinyint(1) NOT NULL DEFAULT '0',
  `creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders__request`
--

DROP TABLE IF EXISTS `orders__request`;
CREATE TABLE `orders__request` (
  `id` int(11) NOT NULL,
  `response` text CHARACTER SET utf8,
  `modification` datetime DEFAULT NULL,
  `creacion` datetime NOT NULL,
  `id_order` int(11) DEFAULT NULL,
  `historico` tinyint(1) NOT NULL DEFAULT '0',
  `tipo` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `articulos` int(11) NOT NULL DEFAULT '0',
  `pvp_articulos` float NOT NULL DEFAULT '0',
  `pvp_total` float NOT NULL DEFAULT '0',
  `pvp_envio` float NOT NULL DEFAULT '0',
  `pvp_iva` float NOT NULL DEFAULT '0',
  `pvp_descuento` float NOT NULL DEFAULT '0',
  `pvp_pago` float NOT NULL DEFAULT '0',
  `pvp_tarjeta` float NOT NULL DEFAULT '0',
  `pvp_envolver` float NOT NULL DEFAULT '0',
  `pvp_puntos` int(11) NOT NULL DEFAULT '0',
  `errors` text COLLATE utf8_spanish2_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_requests`
--

DROP TABLE IF EXISTS `password_requests`;
CREATE TABLE `password_requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `token` varchar(128) COLLATE utf8_spanish2_ci NOT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portes__paises`
--

DROP TABLE IF EXISTS `portes__paises`;
CREATE TABLE `portes__paises` (
  `id` int(11) NOT NULL,
  `locale` varchar(5) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `code` varchar(2) CHARACTER SET latin1 DEFAULT NULL,
  `nombre` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `nombre_idioma` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '0',
  `envio` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portes__provincias`
--

DROP TABLE IF EXISTS `portes__provincias`;
CREATE TABLE `portes__provincias` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_pais` int(11) NOT NULL,
  `nombre` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `nombre_idioma` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '0',
  `envio` tinyint(1) NOT NULL DEFAULT '0',
  `code` varchar(60) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portes__zonas`
--

DROP TABLE IF EXISTS `portes__zonas`;
CREATE TABLE `portes__zonas` (
  `id` int(11) NOT NULL,
  `id_pais` varchar(4) NOT NULL,
  `id_provincia` varchar(4) DEFAULT NULL,
  `id_transportista` varchar(5) NOT NULL,
  `pvp` float NOT NULL,
  `pvp_rebaja` float NOT NULL,
  `pvp_devolver` float NOT NULL,
  `limite_rebaja` float NOT NULL,
  `is_rebaja` tinyint(1) NOT NULL DEFAULT '0',
  `dias_entrega` int(2) UNSIGNED NOT NULL DEFAULT '2',
  `activo` tinyint(1) NOT NULL DEFAULT '0',
  `envio` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preferences`
--

DROP TABLE IF EXISTS `preferences`;
CREATE TABLE `preferences` (
  `id` varchar(100) NOT NULL,
  `text_1` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preferences__files`
--

DROP TABLE IF EXISTS `preferences__files`;
CREATE TABLE `preferences__files` (
  `id` int(10) UNSIGNED NOT NULL,
  `owner_id` int(10) UNSIGNED NOT NULL COMMENT 'propietario (id)',
  `owner_type` varchar(50) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'propietario (mod)',
  `type` varchar(20) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'tipo de archivo',
  `title_1` varchar(250) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'título',
  `name` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre original',
  `mime_type` varchar(250) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'tipo recogido',
  `size` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'tamaño',
  `height` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'alto (si imagen)',
  `width` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ancho (si imagen)',
  `creacion` timestamp NULL DEFAULT NULL,
  `modificacion` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preferences__galeria`
--

DROP TABLE IF EXISTS `preferences__galeria`;
CREATE TABLE `preferences__galeria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `alt` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `link` varchar(250) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '/',
  `link_1` varchar(250) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '/',
  `nombre_1` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `alt_1` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `id_tipo` int(11) DEFAULT NULL,
  `id_preferencia` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `creacion` timestamp NULL DEFAULT NULL,
  `modificacion` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

DROP TABLE IF EXISTS `productos`;
CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `meta_titulo` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `meta_descripcion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8_spanish2_ci,
  `caracteristicas` text COLLATE utf8_spanish2_ci,
  `formato` text COLLATE utf8_spanish2_ci,
  `ingredientes` text COLLATE utf8_spanish2_ci,
  `id_categoria` int(11) DEFAULT '0',
  `precio` float NOT NULL DEFAULT '0',
  `precio_rebajado` float NOT NULL DEFAULT '0',
  `iva` float NOT NULL DEFAULT '4',
  `stock` int(11) NOT NULL DEFAULT '0',
  `peso` int(11) NOT NULL DEFAULT '0',
  `etiqueta` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `contenido` int(4) NOT NULL DEFAULT '100',
  `publicado` tinyint(1) NOT NULL DEFAULT '0',
  `es_novedad` tinyint(1) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__categorias`
--

DROP TABLE IF EXISTS `productos__categorias`;
CREATE TABLE `productos__categorias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `url` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `titulo` varchar(120) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `meta_titulo` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `meta_descripcion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `owner_id` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__comments`
--

DROP TABLE IF EXISTS `productos__comments`;
CREATE TABLE `productos__comments` (
  `id` int(11) NOT NULL,
  `id_parent` int(11) NOT NULL DEFAULT '0',
  `id_usuario` int(11) NOT NULL DEFAULT '0',
  `nombre_usuario` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email_usuario` varchar(120) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `titulo` varchar(200) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `comentario` text COLLATE utf8_spanish2_ci,
  `puntuacion` int(11) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__files`
--

DROP TABLE IF EXISTS `productos__files`;
CREATE TABLE `productos__files` (
  `id` int(10) UNSIGNED NOT NULL,
  `owner_id` int(10) UNSIGNED NOT NULL COMMENT 'propietario (id)',
  `owner_type` varchar(50) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'propietario (mod)',
  `type` varchar(20) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'tipo de archivo',
  `title_1` varchar(250) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '' COMMENT 'título',
  `name` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre original',
  `mime_type` varchar(250) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'tipo recogido',
  `size` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'tamaño',
  `height` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'alto (si imagen)',
  `width` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ancho (si imagen)',
  `path` varchar(200) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `creacion` timestamp NULL DEFAULT NULL,
  `modificacion` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__galeria`
--

DROP TABLE IF EXISTS `productos__galeria`;
CREATE TABLE `productos__galeria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `alt` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `link` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__relaccionados`
--

DROP TABLE IF EXISTS `productos__relaccionados`;
CREATE TABLE `productos__relaccionados` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `id_producto_relaccionado` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__rel_tags`
--

DROP TABLE IF EXISTS `productos__rel_tags`;
CREATE TABLE `productos__rel_tags` (
  `id` int(11) NOT NULL,
  `post_id` int(11) UNSIGNED NOT NULL COMMENT 'id de post',
  `tag_id` int(11) UNSIGNED NOT NULL COMMENT 'id de tag'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__tags`
--

DROP TABLE IF EXISTS `productos__tags`;
CREATE TABLE `productos__tags` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_tipo` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'url en español',
  `url_2` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'url en inglés',
  `text` varchar(255) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre en español',
  `text_2` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'nombre en inglés'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__valores`
--

DROP TABLE IF EXISTS `productos__valores`;
CREATE TABLE `productos__valores` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `clave` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `valor` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `position` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `salt` varchar(128) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `creador` int(11) DEFAULT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_modificacion` timestamp NULL DEFAULT NULL,
  `baja` timestamp NULL DEFAULT NULL,
  `oculto` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users__data`
--

DROP TABLE IF EXISTS `users__data`;
CREATE TABLE `users__data` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `apellidos` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `dni` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `razon` varchar(180) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `direccion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `poblacion` varchar(120) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `id_pais` int(11) DEFAULT NULL,
  `id_provincia` int(11) DEFAULT NULL,
  `codigo_postal` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users__files`
--

DROP TABLE IF EXISTS `users__files`;
CREATE TABLE `users__files` (
  `id` int(10) UNSIGNED NOT NULL,
  `owner_id` int(10) UNSIGNED NOT NULL COMMENT 'propietario (id)',
  `owner_type` varchar(50) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'propietario (mod)',
  `type` varchar(20) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'tipo de archivo',
  `title_1` varchar(250) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'título',
  `name` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre original',
  `mime_type` varchar(250) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'tipo recogido',
  `size` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'tamaño',
  `height` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'alto (si imagen)',
  `width` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ancho (si imagen)',
  `creacion` timestamp NULL DEFAULT NULL,
  `modificacion` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users__login_attempts`
--

DROP TABLE IF EXISTS `users__login_attempts`;
CREATE TABLE `users__login_attempts` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `time` datetime DEFAULT NULL,
  `ip` varchar(45) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users__rel_roles`
--

DROP TABLE IF EXISTS `users__rel_roles`;
CREATE TABLE `users__rel_roles` (
  `id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` varchar(20) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users__roles`
--

DROP TABLE IF EXISTS `users__roles`;
CREATE TABLE `users__roles` (
  `id` varchar(20) CHARACTER SET utf8 NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users__tokens`
--

DROP TABLE IF EXISTS `users__tokens`;
CREATE TABLE `users__tokens` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_usuario` int(11) UNSIGNED NOT NULL,
  `codigo` varchar(32) COLLATE utf8_spanish2_ci NOT NULL,
  `creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificacion` timestamp NULL DEFAULT NULL,
  `expiracion` timestamp NULL DEFAULT NULL,
  `valido` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `formularios`
--
ALTER TABLE `formularios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `idiomas`
--
ALTER TABLE `idiomas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `region` (`region`);

--
-- Indices de la tabla `idiomas__claves`
--
ALTER TABLE `idiomas__claves`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQUE` (`id_seccion`,`nombre`) USING BTREE;

--
-- Indices de la tabla `idiomas__secciones`
--
ALTER TABLE `idiomas__secciones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indices de la tabla `idiomas__textos`
--
ALTER TABLE `idiomas__textos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_clave` (`id_clave`,`id_idioma`),
  ADD KEY `FK_IDIOMAS` (`id_idioma`);

--
-- Indices de la tabla `noticias__archivos`
--
ALTER TABLE `noticias__archivos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_noticia` (`id_noticia`);

--
-- Indices de la tabla `noticias__categorias`
--
ALTER TABLE `noticias__categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticias__categories`
--
ALTER TABLE `noticias__categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticias__comments`
--
ALTER TABLE `noticias__comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indices de la tabla `noticias__files`
--
ALTER TABLE `noticias__files`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `owner_id` (`owner_id`,`owner_type`,`type`);

--
-- Indices de la tabla `noticias__galeria`
--
ALTER TABLE `noticias__galeria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_noticia` (`id_noticia`);

--
-- Indices de la tabla `noticias__posts`
--
ALTER TABLE `noticias__posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `POST_URL` (`url`) USING BTREE,
  ADD KEY `category_id` (`category_id`);

--
-- Indices de la tabla `noticias__rel_tags`
--
ALTER TABLE `noticias__rel_tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `post_id` (`post_id`,`tag_id`),
  ADD KEY `FK_NOTICIAS_REL_TAGS_TAG` (`tag_id`);

--
-- Indices de la tabla `noticias__tags`
--
ALTER TABLE `noticias__tags`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orders__articulos`
--
ALTER TABLE `orders__articulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orders__refunds`
--
ALTER TABLE `orders__refunds`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orders__request`
--
ALTER TABLE `orders__request`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_requests`
--
ALTER TABLE `password_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `portes__paises`
--
ALTER TABLE `portes__paises`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `locale` (`locale`);

--
-- Indices de la tabla `portes__provincias`
--
ALTER TABLE `portes__provincias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `portes__zonas`
--
ALTER TABLE `portes__zonas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cod_pais` (`id_pais`,`id_provincia`,`id_transportista`);

--
-- Indices de la tabla `preferences`
--
ALTER TABLE `preferences`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `preferences__files`
--
ALTER TABLE `preferences__files`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `owner_id` (`owner_id`,`owner_type`,`type`);

--
-- Indices de la tabla `preferences__galeria`
--
ALTER TABLE `preferences__galeria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos__categorias`
--
ALTER TABLE `productos__categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos__comments`
--
ALTER TABLE `productos__comments`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos__files`
--
ALTER TABLE `productos__files`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos__galeria`
--
ALTER TABLE `productos__galeria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos__relaccionados`
--
ALTER TABLE `productos__relaccionados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos__rel_tags`
--
ALTER TABLE `productos__rel_tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `post_id` (`post_id`,`tag_id`),
  ADD KEY `FK_NOTICIAS_REL_TAGS_TAG` (`tag_id`);

--
-- Indices de la tabla `productos__tags`
--
ALTER TABLE `productos__tags`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos__valores`
--
ALTER TABLE `productos__valores`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_producto` (`id_producto`,`clave`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `username_2` (`username`,`baja`);

--
-- Indices de la tabla `users__data`
--
ALTER TABLE `users__data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_USERS_DATA_USER_ID` (`id_user`);

--
-- Indices de la tabla `users__files`
--
ALTER TABLE `users__files`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `owner_id` (`owner_id`,`owner_type`,`type`);

--
-- Indices de la tabla `users__rel_roles`
--
ALTER TABLE `users__rel_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id_2` (`user_id`,`role_id`),
  ADD UNIQUE KEY `user_id_3` (`user_id`,`role_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indices de la tabla `users__roles`
--
ALTER TABLE `users__roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `users__tokens`
--
ALTER TABLE `users__tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo_unico` (`codigo`,`expiracion`),
  ADD UNIQUE KEY `valido_usuario` (`id_usuario`,`expiracion`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `formularios`
--
ALTER TABLE `formularios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT de la tabla `idiomas`
--
ALTER TABLE `idiomas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `idiomas__claves`
--
ALTER TABLE `idiomas__claves`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=214;
--
-- AUTO_INCREMENT de la tabla `idiomas__secciones`
--
ALTER TABLE `idiomas__secciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT de la tabla `idiomas__textos`
--
ALTER TABLE `idiomas__textos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=189;
--
-- AUTO_INCREMENT de la tabla `noticias__archivos`
--
ALTER TABLE `noticias__archivos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT de la tabla `noticias__categorias`
--
ALTER TABLE `noticias__categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `noticias__categories`
--
ALTER TABLE `noticias__categories`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `noticias__comments`
--
ALTER TABLE `noticias__comments`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `noticias__files`
--
ALTER TABLE `noticias__files`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT de la tabla `noticias__galeria`
--
ALTER TABLE `noticias__galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT de la tabla `noticias__posts`
--
ALTER TABLE `noticias__posts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `noticias__rel_tags`
--
ALTER TABLE `noticias__rel_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `noticias__tags`
--
ALTER TABLE `noticias__tags`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;
--
-- AUTO_INCREMENT de la tabla `orders__articulos`
--
ALTER TABLE `orders__articulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=302;
--
-- AUTO_INCREMENT de la tabla `orders__refunds`
--
ALTER TABLE `orders__refunds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `orders__request`
--
ALTER TABLE `orders__request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2002;
--
-- AUTO_INCREMENT de la tabla `password_requests`
--
ALTER TABLE `password_requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `portes__paises`
--
ALTER TABLE `portes__paises`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `portes__provincias`
--
ALTER TABLE `portes__provincias`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT de la tabla `portes__zonas`
--
ALTER TABLE `portes__zonas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `preferences__files`
--
ALTER TABLE `preferences__files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `preferences__galeria`
--
ALTER TABLE `preferences__galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;
--
-- AUTO_INCREMENT de la tabla `productos__categorias`
--
ALTER TABLE `productos__categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `productos__comments`
--
ALTER TABLE `productos__comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `productos__files`
--
ALTER TABLE `productos__files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT de la tabla `productos__galeria`
--
ALTER TABLE `productos__galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `productos__relaccionados`
--
ALTER TABLE `productos__relaccionados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `productos__rel_tags`
--
ALTER TABLE `productos__rel_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `productos__tags`
--
ALTER TABLE `productos__tags`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `productos__valores`
--
ALTER TABLE `productos__valores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `users__data`
--
ALTER TABLE `users__data`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `users__files`
--
ALTER TABLE `users__files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `users__rel_roles`
--
ALTER TABLE `users__rel_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT de la tabla `users__tokens`
--
ALTER TABLE `users__tokens`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `idiomas__claves`
--
ALTER TABLE `idiomas__claves`
  ADD CONSTRAINT `FK_SECCIONES` FOREIGN KEY (`id_seccion`) REFERENCES `idiomas__secciones` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `idiomas__textos`
--
ALTER TABLE `idiomas__textos`
  ADD CONSTRAINT `FK_CLAVES` FOREIGN KEY (`id_clave`) REFERENCES `idiomas__claves` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_IDIOMAS` FOREIGN KEY (`id_idioma`) REFERENCES `idiomas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `noticias__archivos`
--
ALTER TABLE `noticias__archivos`
  ADD CONSTRAINT `FK_NOTICIAS_ARCHIVOS_POST` FOREIGN KEY (`id_noticia`) REFERENCES `noticias__posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `noticias__comments`
--
ALTER TABLE `noticias__comments`
  ADD CONSTRAINT `FK_NOTICIAS_COMMENTS_POST` FOREIGN KEY (`post_id`) REFERENCES `noticias__posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `noticias__galeria`
--
ALTER TABLE `noticias__galeria`
  ADD CONSTRAINT `FK_NOTICIAS_GALERIA_POST` FOREIGN KEY (`id_noticia`) REFERENCES `noticias__posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `noticias__posts`
--
ALTER TABLE `noticias__posts`
  ADD CONSTRAINT `FK_NOTICIAS_POSTS_CATEGORIA` FOREIGN KEY (`category_id`) REFERENCES `noticias__categories` (`id`);

--
-- Filtros para la tabla `noticias__rel_tags`
--
ALTER TABLE `noticias__rel_tags`
  ADD CONSTRAINT `FK_NOTICIAS_REL_TAGS_POST` FOREIGN KEY (`post_id`) REFERENCES `noticias__posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_NOTICIAS_REL_TAGS_TAG` FOREIGN KEY (`tag_id`) REFERENCES `noticias__tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `productos__valores`
--
ALTER TABLE `productos__valores`
  ADD CONSTRAINT `FK_PRODUCTOS_VALORES_PRODUCTO` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `users__tokens`
--
ALTER TABLE `users__tokens`
  ADD CONSTRAINT `FK_USERS_TOKENS_USER` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
