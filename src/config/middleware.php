<?php

	// $app->add(new \Slim\Csrf\Guard);	
	 
	use \Slim\Middleware\TokenAuthentication as TokenAuthentication;
    use App\Response\Base as Response;
	use App\Model as Model;
    use \Firebase\JWT\JWT as JWT;

    JWT::$leeway = 60 * $container->settings['token_time'];

	$app->add(new TokenAuthentication([

		# Función de autentificación
	    'authenticator' => function(&$request, TokenAuthentication $tokenAuth) {
	    	global $container;

	    	$strUri = '';
	    	$objRoute = [];
	    	$objToken = false;
	    	$arrRoles = false;

			# recogemos preferencias de la ruta
			$strUri = '/' . trim($request->getUri()->getPath(), '/');

			/** Otherwise check if path matches and we should authenticate. */
			foreach ((array) $container['settings']['private_routes'] as $value) {
			    $strPath = rtrim($value['path'], "/");
			    if (preg_match("@^{$strPath}(/.*)?$@", $strUri)) {
			        $objRoute = $value;
			        break;
			    }
			}

	    	# borramos tokens antiguos
		    Model\Users\Tokens::setExpirados($container['settings']['token_time']);

		    # recogemos el token
		    try {
			    $strJwt = $tokenAuth->findToken($request);
				$objJwt = JWT::decode($strJwt, $container->settings['api_key_public'], array($container->settings['api_algorithms']));
			    $objToken = Model\Users\Tokens::getValid ($objJwt->jti);
				$request = $request->withAttribute('token', $objJwt->jti);

		    } catch (Exception $e) {
            	$tokenAuth->setResponseMessage($e->getMessage());
				//$request = $request->withAttribute('statusCode', Response::STATUS_ERROR);
            	//return false;
		    }

			# permitimos el paso a métodos públicos
			if (!empty($objRoute['public_methods'])) {

				if ($objRoute['public_methods'] === true) {
					return true;
				}

				if (array_search($request->getMethod(), $objRoute['public_methods']) !== false)  {
					return true;
				}
			}

			# comprobamos que tenga token
            if (!$objToken) {
            	$tokenAuth->setResponseMessage('El token enviado es inválido.');
				$request = $request->withAttribute('statusCode', Response::STATUS_UNAUTHORIZED);
			    return false;
		    }

		    # actualizamos el token
			$objToken->touch();	

		    # denegamos si no tiene el rol
			if (!empty($objRoute['roles'])) {
				if (!$objToken->usuario || !$objToken->usuario->roles->whereIn('role_id', $objRoute['roles'])->count()) {
            		$tokenAuth->setResponseMessage('No tienes acceso a esta parte.');
					$request = $request->withAttribute('statusCode', Response::STATUS_FORBIDDEN);
					return false;
				}
			}

			if (!empty($objRoute['log_path'])) {

				$directory = $objRoute['log_path'] . $objToken->codigo . '/';
				$filename =  str_replace('/', '_', $strUri) . '.' . date('Ymdhis') . '.log';

				if (!file_exists($directory)) {
					mkdir($directory, 0777, true);
				}

	            file_put_contents($directory . $filename, print_r($request->getParsedBody(), true));
			}

		},

		# Función cuando la autentificación es erronea
		'error' => function($request, $response, TokenAuthentication $tokenAuth) {
			$statusCode = $request->getAttribute("statusCode");
			$objResponse = Response::create($statusCode, $tokenAuth->getResponseMessage(), ['token'=>$tokenAuth->getResponseToken()]);

		    return $objResponse->putJSON($response);
		},

    	# Rutas privadas
    	'path' => array_column($container['settings']['private_routes'], 'path'),

    	# Rutas publicas
    	'passthrough' => array_column($container['settings']['public_routes'], 'path'),

    	# Requerir entrada por https
   		'secure' => false

   		# Modifica autentificación por basic <token>
    	# 'regex' => '/Basic\s+(.*)$/i', /* for without token type can use /\s+(.*)$/i */
	]));

	/* https://www.slimframework.com/docs/cookbook/enable-cors.html */
	$app->options('/{routes:.+}', function ($request, $response, $args) {
	    return $response;
	});

	
	$app->add(function ($req, $res, $next) {
	    $response = $next($req, $res);
	    return $response
	         ->withHeader('Access-Control-Allow-Origin', '*')
	         ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
	         ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
	});
		
	/*
	$app->add(function($request, $response, $next) {
	    $route = $request->getAttribute("route");

	    $methods = [];

	    if (!empty($route)) {
	        $pattern = $route->getPattern();

	        foreach ($this->router->getRoutes() as $route) {
	            if ($pattern === $route->getPattern()) {
	                $methods = array_merge_recursive($methods, $route->getMethods());
	            }
	        }
	        //Methods holds all of the HTTP Verbs that a particular route handles.
	    } else {
	        $methods[] = $request->getMethod();
	    }
	    
	    $response = $next($request, $response);


	    return $response->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
	    	->withHeader("Access-Control-Allow-Methods", implode(",", $methods));
	});
	*/