<?php

    namespace App\Model;

    class Formularios extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'formularios';

        const CREATED_AT = 'creacion';
        const UPDATED_AT = 'modificacion';

        protected $fillable = ['email', 'telefono', 'nombre', 'mensaje', 'is_aviso'];
    }