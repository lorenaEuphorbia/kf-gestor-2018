<?php

    namespace App\Model;

    class Orders extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'orders';

        protected $guarded = [];

        protected $visible = [
            'articulos',
            'request',
            'id',
            'nombre',
            'apellidos',
            'email',
            'nif',
            'telefono',
            'fecha_nacimiento',
            'nif_dir_facturacion',
            'nombre_dir_facturacion',
            'codigo_dir_facturacion',
            'direccion_dir_facturacion',
            'cp_dir_facturacion',
            'localidad_dir_facturacion',
            'provincia_dir_facturacion',
            'pais_dir_facturacion',
            'codigo_dir_envio',
            'nombre_dir_envio',
            'direccion_dir_envio',
            'cp_dir_envio',
            'localidad_dir_envio',
            'provincia_dir_envio',
            'pais_dir_envio',
            'envioEmpresa',
            'envioImporte',
            'envioObservaciones',
            'formaDePago',
            'is_factura',
            'is_envolver_regalo',
            'is_tarjeta_regalo',
            'str_tarjeta_regalo',
            'vale',
            'pvp_total',
            'peso_total',
            'pagando',
            'pagandoFecha',
            'validacion',
            'creation_date',
            'modification_date',
            'request_id',
            'historico',
            'correo_resumen',
            'correo_envio',
        ];

        protected $fillable = [
            'nombre',
            'apellidos',
            'email',
            'nif',
            'telefono',
            'fecha_nacimiento',
            'nif_dir_facturacion',
            'nombre_dir_facturacion',
            'codigo_dir_facturacion',
            'direccion_dir_facturacion',
            'cp_dir_facturacion',
            'localidad_dir_facturacion',
            'provincia_dir_facturacion',
            'pais_dir_facturacion',
            'codigo_dir_envio',
            'nombre_dir_envio',
            'direccion_dir_envio',
            'cp_dir_envio',
            'localidad_dir_envio',
            'provincia_dir_envio',
            'pais_dir_envio',
            'envioEmpresa',
            'envioObservaciones',
            'formaDePago',
            'is_factura',
            'is_envolver_regalo',
            'is_tarjeta_regalo',
            'str_tarjeta_regalo',
            'vale',
        ];

        const CREATED_AT = 'creation_date';
        const UPDATED_AT = 'modification_date';

        public function __construct (array $attributes = array()) {
            parent::__construct($attributes);

            $this->newCodigo ();
        }

        # RELATIONS

        public function articulos() {
            return $this->hasMany('App\Model\Orders\Articulos','id_order');
        }

        public function requests (){
            return $this->hasMany('App\Model\Orders\Request','id_order');
        }

        public function request (){
            return $this->hasOne('App\Model\Orders\Request','id','request_id');
        }

        public function token (){
            return $this->belongsTo('App\Model\Users\Token', 'id_token');
        }

        public function usuario () {
            return $this->belongsTo('App\Model\Users','codigo_cliente');
        }

        # STATICS

        # METHODS

        public function fillDatosUsuario ($assignements) {
            if ($this->usuario) {
                $objDatosUsuario = $this->usuario->datos;

                foreach ((array) $assignements as $localKey => $foreignKey) {
                    $this->attributes[$localKey] = $objDatosUsuario->$foreignKey;
                }
            }

            return $this;
        }

        public function fillDatosUsuarioEnvio () {
            return $this->fillDatosUsuario([
                'nombre_dir_envio' => 'razon',
                'direccion_dir_envio' => 'direccion',
                'cp_dir_envio' => 'codigo_postal',
                'localidad_dir_envio' => 'poblacion',
                'provincia_dir_envio' => 'id_provincia',
                'pais_dir_envio' => 'id_pais'
            ]);
        }

        public function fillDatosUsuarioFacturacion () {
            return $this->fillDatosUsuario([
                'nif_dir_facturacion' => 'dni',
                'nombre_dir_facturacion' => 'razon',
                'direccion_dir_facturacion' => 'direccion',
                'cp_dir_facturacion' => 'codigo_postal',
                'localidad_dir_facturacion' => 'poblacion',
                'provincia_dir_facturacion' => 'id_provincia',
                'pais_dir_facturacion' => 'id_pais',
            ]);
        }

        public function fillDatosUsuarioGlobales () {
            return $this->fillDatosUsuario([
                'nombre' => 'nombre',
                'apellidos' => 'apellidos',
                'email' => 'email',
                'nif' => 'dni',
                'telefono' => 'telefono',
                //'fecha_nacimiento' => 'fecha_nacimiento',
            ]);
        }

        public function hasDatosEnvio () {
            return
                !empty($this->attributes['nombre_dir_envio']) &&
                !empty($this->attributes['direccion_dir_envio']) &&
                !empty($this->attributes['cp_dir_envio']) &&
                !empty($this->attributes['localidad_dir_envio']) &&
                !empty($this->attributes['provincia_dir_envio']) &&
                !empty($this->attributes['pais_dir_envio']);
        }

        public function hasDatosFacturacion () {
            return
                !empty($this->attributes['nif_dir_facturacion']) &&
                !empty($this->attributes['nombre_dir_facturacion']) &&
                !empty($this->attributes['direccion_dir_facturacion']) &&
                !empty($this->attributes['cp_dir_facturacion']) &&
                !empty($this->attributes['localidad_dir_facturacion']) &&
                !empty($this->attributes['provincia_dir_facturacion']) &&
                !empty($this->attributes['pais_dir_facturacion']);
        }

        public function hasDatosGlobales () {
            return
                !empty($this->attributes['nombre']) &&
                !empty($this->attributes['apellidos']) &&
                !empty($this->attributes['email']) &&
                !empty($this->attributes['nif']) &&
                !empty($this->attributes['telefono']) &&
                !empty($this->attributes['fecha_nacimiento']);
        }

        public function newCodigo () {
            $this->attributes['code'] = \Utilities::getUniqueCode();
        }

        public function updateDatosPedido () {
            $this->pvp_total = 0;
            $this->peso_total = 0;
            foreach ($this->articulos as $articulo) {
                $articulo->updateDatosArticulo()->save();

                $this->pvp_total += $articulo->precio * $articulo->unidades;
                $this->peso_total += $articulo->producto->peso * $articulo->unidades;
            }
            return $this;
        }

    }