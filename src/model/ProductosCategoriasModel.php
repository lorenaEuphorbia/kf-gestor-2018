<?php

    namespace App\Model\Productos;

    class Categorias extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'productos__categorias';

        protected $guarded = [];

        protected $visible = [
            'id',
			      'id_tipo',
			      'url',
			      'nombre',
			      'titulo',
			      'meta_titulo',
			      'meta_descripcion',
			      'owner_id',
        ];

    }
