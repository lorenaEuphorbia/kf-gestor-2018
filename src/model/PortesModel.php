<?php

    namespace App\Model;

    class Portes extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'portes__zonas';
        protected $hidden = [];
        protected $visible = ['id','id_pais','id_provincia','dias_entrega','activo','envio'];

        # RELATIONS
        
        public function paises () {
            return $this->hasOne('App\Model\Portes\Paises','id_pais')->withDefault();
        }

        public function provincias () {
            return $this->hasMany('App\Model\Portes\Provincias','id_provincia');            
        }

        # STATICS
    }