<?php

    namespace App\Model\Noticias;

    class Archivos extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'noticias__archivos';

	    public function post ()
	    {
	        return $this->belongsTo('App\Model\Noticias', 'id_noticia');
	    }
    }