<?php

    namespace App\Model\Productos;

    class Galeria extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'productos__galeria';

        protected $visible = [
            'id',
            'nombre',
            'alt',
            'link',
            'owner_id',
            'imagenes'
        ];

	    public function producto ()
	    {
	        return $this->belongsTo('App\Model\Productos', 'owner_id');
	    }

        public function _imagenes () {
            return $this->hasMany('App\Model\Productos\Files','owner_id','id');
        }

        public function imagenes () {
            //return $this->_imagenes()->where('owner_type','=', "galeria");
            return $this->_imagenes();
        }
    }
