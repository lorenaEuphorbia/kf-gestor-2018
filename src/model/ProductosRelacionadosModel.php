<?php

    namespace App\Model\Productos;

    class Relacionados extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'productos__relaccionados';

        protected $guarded = [
            'id'
        ];

        protected $visible = [];

        # RELATIONS

        public function producto ()
        {
            return $this->belongsTo('App\Model\Productos', 'id_parent');
        }

        public function usuario ()
        {
            return $this->belongsTo('App\Model\Users', 'id_usuario');
        }

        public function parent ()
        {
            return $this->belongsTo('App\Model\Productos\Relacionados', 'id_producto_relaccionado');
        }

        public function child ()
        {
            return $this->hasOne('App\Model\Productos\Relacionados', 'id_producto_relaccionado');
        }

        # METHODS

        public function setNombreUsuario ($nombreUsuario = '', $apellidoUsuario = '') {
            $words = preg_split("/[\s,_-]+/", $apellidoUsuario);
            $siglas = "";

            foreach ($words as $w) {
              $siglas .= strtoupper($w[0]) . '.';
            }

            $this->nombre_usuario = $nombreUsuario . ' ' . $siglas;
            return $this;
        }
    }
