<?php

    namespace App\Model\Orders;

    class Articulos extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'orders__articulos';

        protected $guarded = [
            'id',
            //'articulo',
            // 'precio',
            // 'iva',
            // 'precio_iva',
        ];

        protected $visible = [
            'id',
            'articulo',
            'talla',
            'precio',
            'iva',
            'precio_iva',
            'unidades',
            'producto',
        ];

        public function __construct (array $attributes = array()) {
            parent::__construct($attributes);
        }

        # RELATIONS

        public function order () {
            return $this->belongsTo('App\Model\Orders', 'order_id');
        }

        public function producto () {
            return $this->belongsTo('App\Model\Productos', 'articulo');            
        }

        # STATICS

        # METHODS
        
        public function isVenta () {
            return $this->producto && $this->producto->publicado && $this->producto->precio_rebajado;            
        }

        public function isStock () {
            return $this->producto && $this->producto->stock >= $this->attributes['unidades'];
        }
        
        public function updateDatosArticulo () {
            if ($this->producto) {
                $tasaIva = (100 + $this->producto->iva) / 100;
                $precio_sin_iva = $this->producto->precio_rebajado / $tasaIva;

                $this->precio = $this->producto->precio_rebajado;
                $this->iva = $this->producto->iva;
                $this->precio_iva = $this->precio - $precio_sin_iva;
            }
            return $this;
        }

    }