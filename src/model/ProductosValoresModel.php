<?php

    namespace App\Model\Productos;

    class Valores extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'productos__valores';

        protected $guarded = ['id','id_producto'];

        protected $visible = [
            'clave' ,
            'valor' ,
        ];

    }