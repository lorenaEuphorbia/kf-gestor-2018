<?php

    namespace App\Model\Preferencias;

    use App\Files\Types as FileTypes;

    class Files extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'preferences__files';

        const CREATED_AT = 'creacion';
        const UPDATED_AT = 'modificacion';

        protected $guarded = [];

        protected $visible = [
            'owner_type' ,
            'title_1' ,
            'name' ,
            'mime_type' ,
            'size' ,
            'height' ,
            'width' ,
            'path' ,
        ];

        public function getRutaDirectorio () {
            return (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]" . $this->getRutaDirectorioRelativa();
        }

        public function getRutaDirectorioServidor () {
            return $_SERVER['DOCUMENT_ROOT'] . $this->getRutaDirectorioRelativa();
        }

        public function getRutaDirectorioRelativa () {
            return '/files/preferences/' . $this->owner_type . '/' . $this->type . '/';
        }
        public function getRutaImagen () {
        	return $this->getRutaDirectorio () . $this->id . FileTypes::getExtension($this->mime_type);
        }

        public function getPathAttribute () {
        	return $this->getRutaImagen();
        }


    }
