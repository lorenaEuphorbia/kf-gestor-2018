<?php

    namespace App\Model\Orders;

    class Request extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'orders__request';

        protected $guarded = [];

        protected $visible = [
            'modification',
            'creacion',
            'id_order',
            'historico',
            'tipo',
            'articulos',
            'pvp_articulos',
            'pvp_total',
            'pvp_envio',
            'pvp_iva',
            'pvp_descuento',
            'pvp_pago',
            'pvp_tarjeta',
            'pvp_envolver',
            'pvp_puntos',
            'errors',
        ];

        const CREATED_AT = 'creation';
        const UPDATED_AT = 'modification';

        public function order () {
            return $this->belongsTo('App\Model\Orders', 'id_order');
        }


    }