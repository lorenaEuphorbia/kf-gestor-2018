<?php

    namespace App\Model\Portes;

    class Provincias extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'portes__provincias';
        protected $hidden = ['created_at','updated_at'];
        protected $visible = [];

        # RELATIONS
        
        public function pais () {
            return $this->belongsTo('App\Model\Portes\Paises', 'id_pais');
        }

        # STATICS
    }