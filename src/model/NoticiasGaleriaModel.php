<?php

    namespace App\Model\Noticias;

    class Galeria extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'noticias__galeria';
        protected $visible = [
            'id',
            'nombre',
            'alt',
            'link',
            'owner_id',
            'imagenes'
        ];

	    public function post ()
	    {
	        return $this->belongsTo('App\Model\Noticias', 'post_id');
	    }

	    public function user ()
	    {
	        return $this->belongsTo('App\Model\Users', 'owner_id');
	    }

        public function _imagenes () {
            return $this->hasMany('App\Model\Noticias\Imagenes','owner_id','id');
        }

        public function imagenes () {
          //return $this->_imagenes()->where('owner_type','=', "galeria");
            return $this->_imagenes();
        }
    }
