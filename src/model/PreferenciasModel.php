<?php

    namespace App\Model;

    class Preferencias extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'preferences';

	    protected $primaryKey = 'id'; // or null

	    public $incrementing = false;

        protected $guarded = [
        ];

        protected $visible = [
            'id',
            'text_1' ,
            'imagenes'
        ];

        public function imagenes () {
            return $this->hasMany('App\Model\Preferencias\Files','owner_id','id');
        }


    }
