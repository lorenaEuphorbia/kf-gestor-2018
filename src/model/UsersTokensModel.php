<?php

    namespace App\Model\Users;

    use \Datetime;
    use \DateInterval;
    use \Tuupola\Base62;

    class Tokens extends \Illuminate\Database\Eloquent\Model
    {        
        protected $table = 'users__tokens';
        protected $hidden = ['id','valido'];
        protected $attributes = [
            'valido' => '1'
        ];

        const CREATED_AT = 'creacion';
        const UPDATED_AT = 'modificacion';

        public function __construct (array $attributes = array()) {
            parent::__construct($attributes);

            $this->newCodigo ();
        }

        # RELATIONS

        public function usuario () {
            return $this->belongsTo('App\Model\Users', 'id_usuario');
        }

        public function orders () {
            return $this->hasMany('App\Model\Orders', 'id_token');
        }
        

        # STATICS

        public static function getValid ($token) {
            $token = self::query()->where('codigo', $token)->first();

            if ($token && $token->isValido()) {
                return $token;
            }
            return false;
        }

        public static function setExpirados ($minutes) {
            $expires = date('Y-m-d H:i:s', strtotime('-' . $minutes . ' minutes'));

            $builder = self::query();
            $builder->where('modificacion', '<', $expires)->where('valido', '=', '1')->whereNull('expiracion')->update(['valido' => '0', 'expiracion' => date('Y-m-d H:i:s')]);
        }

        # METHODS
        
        public function createOrder () {
            $objOrder = new \App\Model\Orders;
            $objOrder->codigo_cliente = $this->attributes['id_usuario'];

            $this->orders()->save($objOrder);

            $objOrder->fillDatosUsuarioGlobales ();
            $objOrder->fillDatosUsuarioEnvio ();
            $objOrder->fillDatosUsuarioFacturacion ();
            $objOrder->save();

            return $objOrder->fresh();
        }

        /**
         * https://stackoverflow.com/questions/40745307/generating-tokens-with-slim-jwt-auth
         * @param  int $minutes Los minutos en los que caduca el token
         * @return array          pareja clave con el payload
         */
        public function getJWTPayload ($minutes) {
            return [
                "aud" => $this->attributes['codigo'],
                "iss" => $_SERVER['HTTP_HOST'] . '/api/',
                "jti" => $this->attributes['codigo'],
                "iat" => $this->getCreationTime (),
                "exp" => $this->getExpiresTime ($minutes),
                "sub" => $this->usuario? $this->usuario->id: false,
                "name" => $this->usuario && $this->usuario->datos? $this->usuario->datos->nombre . ' ' . $this->usuario->datos->apellidos: false,
                "roles" => $this->usuario? $this->usuario->roles: false,
            ];
        }

        public function getCurrentOrder () {
            $objOrder = $this->orders()->where(['historico'=>'0'])->whereNull('request_id')->orderBy('modification_date','desc')->first(); 

            if ($objOrder) return $objOrder;
            return $this->createOrder();
        }

        public function getCreationTime () {
            $now = new DateTime($this->attributes['creacion']);

            return $now->getTimeStamp();
        }

        public function getExpiresTime ($minutes) {
            $future = new DateTime($this->attributes['modificacion']);
            $future->add(new DateInterval('PT' . $minutes . 'M'));

            return $future->getTimeStamp();
        }

        public function newCodigo () {
            $base62 = new Base62;

            $this->attributes['codigo'] = $base62->encode(random_bytes(16));
        }

        public function expirar() {
            $this->attributes['valido'] = '0';            
            $this->attributes['expiracion'] = date('Y-m-d H:i:s');  

            $this->save();        
        }

        public function isValido() {
            return $this->attributes['valido'] && empty($this->attributes['expiracion']);
        }

    }