<?php

    namespace App\Model\Noticias;

    class Comentarios extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'noticias__comments';
        
	    public function post ()
	    {
	        return $this->belongsTo('App\Model\Noticias', 'post_id');
	    }

        public function parent ()
        {
            return $this->belongsTo('App\Model\Noticias\Comentarios', 'parent_id');
        }

        public function child ()
        {
            return $this->hasOne('App\Model\Noticias\Comentarios', 'parent_id');
        }

    }