<?php

    namespace App\Model\Users;

    class RelRoles extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'users__rel_roles';
        
	    public function user ()
	    {
	        return $this->belongsTo('App\Model\Users', 'user_id');
	    }
        
	    public function role ()
	    {
	        return $this->belongsTo('App\Model\Users\Roles', 'role_id');
	    }

    }