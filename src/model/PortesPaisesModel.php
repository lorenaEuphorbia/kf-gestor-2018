<?php

    namespace App\Model\Portes;

    class Paises extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'portes__paises';
        protected $hidden = ['created_at','updated_at'];
        protected $visible = [];

        # RELATIONS
        
        public function provincias () {
            return $this->hasMany('App\Model\Portes\Provincias','id_pais');
        }

        # STATICS
    }