<?php

    namespace App\Model;

    class Noticias extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'noticias__posts';

        public function tags() {
            return $this->hasMany('App\Model\Noticias\RelTags','post_id');
        }

        public function _imagenes () {
            return $this->hasMany('App\Model\Noticias\Imagenes','owner_id','id');
        }

        public function imagenes () {
            return $this->_imagenes()->where('owner_type','=', "posts");
        }

        public function comentarios() {
        	return $this->hasMany('App\Model\Noticias\Comentarios','post_id');
        }

        /*
        public function galeria() {
        	return $this->hasMany('App\Model\Noticias\Galeria','id_noticia');
        }
        */

        public function galeria() {
            return $this->hasMany('App\Model\Noticias\Galeria','owner_id');
            //return $this->hasMany('App\Model\Productos\Galeria','owner_id','id');
        }

        public function archivos() {
        	return $this->hasMany('App\Model\Noticias\Archivos','id_noticia');
        }

        public function categoria() {
        	return $this->hasMany('App\Model\Noticias\Categorias','category_id');
        }
    }
