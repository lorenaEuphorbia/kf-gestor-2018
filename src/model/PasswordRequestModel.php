<?php

    namespace App\Model;

    class PasswordRequests extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'password_requests';
        protected $hidden = [];

        const CREATED_AT = 'date';
        const UPDATED_AT = 'updated_at';

        public function __construct (array $attributes = array()) {
            parent::__construct($attributes);
        }

        # RELATIONS

        # STATICS
        
        public static function purgeRequests() {
            $deadline = Date("Y-m-d H:i:s", strtotime("- 30 min"));
            return self::where('date', ['<', $deadline])->delete();
        }

        public static function searchRequest ($email, $token) {
            self::purgeRequests();
            return self::where('email', $email)->where('token', $token)->first();
        }


        # METHODS
        
        public function usuario () {
            return Users::getByUsername ($this->attributes['email']);
        }

    }