<?php

    namespace App\Model\Users;

    class Data extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'users__data';
        protected $hidden = ['id','id_user','usuario'];
        protected $guarded = ['id','id_user'];

        public function usuario () {
            return $this->belongsTo('App\Model\Users', 'id_user');
        }

        public function getEmailAttribute () {
            return $this->usuario->username;
        }

    }