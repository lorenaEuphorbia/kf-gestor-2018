<?php

    namespace App\Model;

    class Productos extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'productos';

        protected $guarded = [];

        protected $visible = [
            'id',
            //'id_tipo',
            //'codigo' ,
            'nombre' ,
            'url' ,
            'lugar',
            'fecha_realizacion',
            'descripcion' ,
            'caracteristicas' ,
            'meta_titulo' ,
            'meta_descripcion' ,
            //'formato' ,
            //'ingredientes' ,
            'id_categoria' ,
            //'precio' ,
            //'precio_rebajado' ,
            //'iva' ,
            //'stock' ,
            //'peso' ,
            //'etiqueta' ,
            'contenido' ,
            'publicado' ,
            'es_novedad' ,
            'es_privado',
            'nombre_carpeta',
            'numero_fotos',
            'video',
            'password',
            'position' ,
            'galeria' ,
            'galeria.imagenes' ,
            'created_at' ,
            'updated_at' ,
            //'valores_nutricionales' ,
            'imagenes',
            'comentarios',
            'relacionados'
        ];

//        protected $fillable = ['username', 'email', 'password'];

//        public function tags() {
//        	return $this->hasMany('App\Model\Productos\RelTags','post_id')->select(['tag_id as id']);
//        }

//        public function comentarios() {
//        	return $this->hasMany('App\Model\Productos\Comentarios','post_id');
//        }

          public function galeria() {
              return $this->hasMany('App\Model\Productos\Galeria','owner_id');
              //return $this->hasMany('App\Model\Productos\Galeria','owner_id','id');
          }

//        public function archivos() {
//        	return $this->hasMany('App\Model\Productos\Archivos','id_noticia');
//        }

        public function categoria () {
            return $this->hasOne('App\Model\Productos\Categorias','id_categoria');
        }

        public function comentarios () {
            return $this->hasMany('App\Model\Productos\Comentarios','id_parent','id');
        }

        public function valores_nutricionales () {
            return $this->hasMany('App\Model\Productos\Valores','id_producto');
        }

        public function imagenes () {
            return $this->hasMany('App\Model\Productos\Files','owner_id','id');
        }

        public function relacionados () {
            return $this->hasMany('App\Model\Productos\Relacionados','id_producto_relaccionado','id');
        }

        public function productos () {
            return $this->hasMany('App\Model\Orders\Articulos','articulo','id');
        }


        public function borrarImagenes () {
            $objImagen = $this->imagenes->first();

            if ($objImagen) {
                $directorio_imagenes = $_SERVER['DOCUMENT_ROOT'] . $objImagen->getRutaDirectorio();
                if (file_exists($directorio_imagenes)) {
                    return $this->deleteDirectorio($directorio_imagenes);
                }

                $this->imagenes()->getQuery()->delete();
                return 0;
            }

            return false;
        }

        public static function deleteDirectorio ($directorio) {
            $count = 0;
            $files = glob($directorio . '*'); // get all file names
            foreach($files as $file){ // iterate files
                if(is_file($file) && unlink($file)) $count++; // delete file
            }
            rmdir($directorio);
            return $count;
        }


        public static function moveUploadedFile ($filename, $directory, $uploadedFile) {
            if (!file_exists($directory)) {
                mkdir($directory, 0777, true);
            }

            return $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
        }
    }
