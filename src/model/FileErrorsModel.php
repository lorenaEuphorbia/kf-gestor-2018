<?php

    namespace App\Files;

	class Errors {

		public static function getMensaje ($code) { 
	        switch ($code) { 
	            case UPLOAD_ERR_INI_SIZE: 
	                $message = "El tamaño del archivo subido excede el tamaño máximo permitido por el servidor."; 
	                break; 
	            case UPLOAD_ERR_FORM_SIZE: 
	                $message = "El tamaño del archivo subido excede el tamaño máximo permitido por el formulario."; 
	                break; 
	            case UPLOAD_ERR_PARTIAL: 
	                $message = "La subida del archivo no se ha completado correctamente."; 
	                break; 
	            case UPLOAD_ERR_NO_FILE: 
	                $message = "No se subió ningún archivo."; 
	                break; 
	            case UPLOAD_ERR_NO_TMP_DIR: 
	                $message = "No se ha encontrado el directorio temporal del servidor."; 
	                break; 
	            case UPLOAD_ERR_CANT_WRITE: 
	                $message = "Error al escribir en el disco."; 
	                break; 
	            case UPLOAD_ERR_EXTENSION: 
	                $message = "Subida de archivos cancelada por una extensión-"; 
	                break; 

	            default: 
	                $message = "Error de subida desconocido."; 
	                break; 
	        } 
	        return $message; 
	    } 
	}
