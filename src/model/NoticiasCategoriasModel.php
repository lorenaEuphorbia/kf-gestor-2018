<?php

    namespace App\Model\Noticias;

    class Categorias extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'noticias__categories';
    }