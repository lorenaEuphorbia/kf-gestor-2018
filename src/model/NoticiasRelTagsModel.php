<?php

    namespace App\Model\Noticias;

    class RelTags extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'noticias__rel_tags';
        
	    public function post ()
	    {
	        return $this->belongsTo('App\Model\Noticias', 'post_id');
	    }
        
	    public function tag ()
	    {
	        return $this->belongsTo('App\Model\Noticias\Tags', 'tag_id');
	    }

    }