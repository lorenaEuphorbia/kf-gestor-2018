<?php

    namespace App\Model;

    class Users extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'users';
        protected $hidden = ['id','username','password', 'salt', 'creador', 'fecha_creacion', 'baja', 'oculto'];

        const CREATED_AT = 'fecha_creacion';
        const UPDATED_AT = 'fecha_modificacion';

        public function __construct (array $attributes = array()) {
            parent::__construct($attributes);
        }

        # RELATIONS
        
        public function datos () {
            return $this->hasOne('App\Model\Users\Data','id_user');
        }

        public function orders () {
            return $this->hasMany('App\Model\Orders','codigo_cliente');            
        }

        public function roles () {
            return $this->hasMany('App\Model\Users\RelRoles','user_id');
        }
        
        public function tokens () {
            return $this->hasMany('App\Model\Users\Tokens','id_usuario');
        }
        
        public function comentarios () {
            return $this->hasMany('App\Model\Productos\Comentarios','id_usuario');
        }

        # STATICS

        public static function getByAutentificacion ($username, $password) {
            $user = self::getByUsername ($username);
            return  $user && $user->isPasswordCorrect ($password)? $user: false;
        }

        public static function getByUsername ($username) {
            return self::whereNull('baja')->where('username', $username)->first();
        }

        public function newPassword ($password) {            
            require_once $_SERVER['DOCUMENT_ROOT'] . '/gestor/_inc/config.php';
            require_once $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-utilities.php';

            $new_salt = hash('sha512', \Utilities::getRandomString(20));
            $password = hash('sha512', $password . $new_salt);
            $new_password = hash('sha512', $password . $new_salt);

            $this->password = $new_password;
            $this->salt = $new_salt;

            return $this;
        }


        # METHODS
         
        public function isPasswordCorrect ($password) {
            $passwordCheck = hash('sha512', $password . $this->salt);
            $passwordCheck = hash('sha512', $passwordCheck . $this->salt);

            return $this->password === $passwordCheck;
        }

        public function newToken () {
            $token = new Users\Tokens;
            $this->tokens()->save($token);

            return $token;      
        }

        public function token_valido () {
            return $this->tokens()->where('valido','=', 1)->whereNull('expiracion')->first();
        }
        
        public function passwordRequests () {
            App\Model\PasswordRequests::purgeRequests();
            return App\Model\PasswordRequests::where ('email', $this->attributes['username'])->find();
        }

    }