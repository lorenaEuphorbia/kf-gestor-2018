<?php

	use Illuminate\Database\Capsule\Manager as Capsule;
	use App\Model as Model;

	$app->group('/productos', function () {
		
	   	$this->get('/','App\Controller\Productos:getAll');
	   	$this->get('/{id:[0-9]+}','App\Controller\Productos:get');
	   	$this->get('/{url:[a-zA-Z0-9\-\_]+}','App\Controller\Productos:get');

		$this->group('/categorias', function () {			
	    	$this->get('/','App\Controller\Productos\Categorias:getAll');
	   		$this->get('/{url:[a-zA-Z0-9\-\_]+}','App\Controller\Productos\Categorias:get');
	    	$this->get('/{id}','App\Controller\Productos\Categorias:get');
		});

		$this->group('/comentarios', function () {
	    	$this->put('/{id:[0-9]+}','App\Controller\Productos\Comentarios:guardarComentario');
		});


	});