<?php

	use Illuminate\Database\Capsule\Manager as Capsule;
	use App\Model as Model;

	$app->group('/noticias', function () {
		
	   	$this->get('/','App\Controller\Noticias:getAll');
	   	$this->get('/{id:[0-9]+}','App\Controller\Noticias:get');
	   	$this->get('/{url:[a-zA-Z0-9\-\_]+}','App\Controller\Noticias:get');
	   	$this->get('/historico/','App\Controller\Noticias:getHistorico');

		$this->group('/categorias', function () {			
	    	$this->get('/','App\Controller\Noticias\Categorias:getAll');
		   	$this->get('/{id:[0-9]+}','App\Controller\Noticias\Categorias:get');
		   	$this->get('/{url:[a-zA-Z0-9\-\_]+}','App\Controller\Noticias\Categorias:get');
		});

		$this->group('/tags', function () {			
	    	$this->get('/','App\Controller\Noticias\Tags:getAll');
		   	$this->get('/{id:[0-9]+}','App\Controller\Noticias\Tags:get');
		   	$this->get('/{url:[a-zA-Z0-9\-\_]+}','App\Controller\Noticias\Tags:get');
		});


	});