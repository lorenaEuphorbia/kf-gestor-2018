<?php

	use Illuminate\Database\Capsule\Manager as Capsule;
	use App\Model as Model;

	$app->group('/preferencias', function () {

	    $this->get('/','App\Controller\Preferencias:getAllAsoc');
	    $this->get('/{id}','App\Controller\Preferencias:get');
	    
	});