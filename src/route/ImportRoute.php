<?php

	use Illuminate\Database\Capsule\Manager as Capsule;
	use App\Model as Model;

	$app->group('/import', function () {

		$this->group('/productos', function () {

	   		$this->put('/','App\Controller\Productos:import');

			$this->group('/categorias', function () {
	   			$this->put('/','App\Controller\Productos\Categorias:import');
			});

			$this->group('/{id}', function () {
				$this->group('/imagenes', function () {
		   			//$this->post('/','App\Controller\Productos\Imagenes:import');
		   			$this->post('/','App\Controller\Productos\Imagenes:import');
		   			$this->delete('/','App\Controller\Productos\Imagenes:delete');
				});
			});
		});
	    

	});