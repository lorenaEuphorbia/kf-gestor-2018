<?php

	use Illuminate\Database\Capsule\Manager as Capsule;
    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as ResponseInterface;
    use App\Response\Base as Response;
    use App\Model as Model;

	$app->group('/usuario', function () {

		
		$this->get('/','App\Controller\Usuarios:getDatos');
		$this->post('/','App\Controller\Usuarios:setDatos');
	    
	});