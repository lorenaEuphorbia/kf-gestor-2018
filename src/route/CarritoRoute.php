<?php

	use Illuminate\Database\Capsule\Manager as Capsule;
	use App\Model as Model;

	$app->group('/carrito', function () {
		
	   	$this->get('/','App\Controller\Carrito:get');
	   	$this->post('/','App\Controller\Carrito:putCarrito');

	   	# ARTICULOS

		$this->group('/articulo', function () {		
	   		$this->post('/','App\Controller\Carrito:setArticulo');
	   		$this->put('/','App\Controller\Carrito::putArticulo');
	   		$this->delete('/{id:[0-9]+}','App\Controller\Carrito:removeArticulo');
	   		$this->post('/unidades','App\Controller\Carrito:addArticuloUnidades');
		});

	   	# COMPROBACIONES
	   	
		$this->group('/articulos', function () {
	   		$this->post('/check','App\Controller\Carrito:checkArticulos');
		});

	   	$this->post('/check','App\Controller\Carrito:check');


	});