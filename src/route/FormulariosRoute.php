<?php

	use Illuminate\Database\Capsule\Manager as Capsule;
	use App\Model as Model;

	$app->group('/formularios', function () {

		$this->group('/contacto', function () {			
	    	$this->post('/','App\Controller\Formularios:enviarContacto');
		});
	    
	});