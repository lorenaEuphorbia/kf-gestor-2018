<?php

	use Illuminate\Database\Capsule\Manager as Capsule;
	use App\Model as Model;

	$app->group('/orders', function () {
		
	   	$this->get('/','App\Controller\Orders:getAll');
	    $this->get('/{id}','App\Controller\Orders:get');

	   	$this->group('/articulos', function () {
	   		$this->get('/', 'App\Controller\Orders:getArticulos');
	   	});



	});