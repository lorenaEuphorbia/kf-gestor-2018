<?php

	use Illuminate\Database\Capsule\Manager as Capsule;
	use App\Model as Model;

	$app->group('/autentificacion', function () {

		$this->get('/','App\Controller\Autentificacion:getTokenAuth');
		$this->get('/token','App\Controller\Autentificacion:createToken');
		
		# https://www.uno-de-piera.com/autenticacion-con-jwt-php-y-angularjs/
		$this->post('/login/','App\Controller\Autentificacion:loginUser');
		$this->post('/logout/','App\Controller\Autentificacion:deleteToken');
		$this->post('/registro/','App\Controller\Autentificacion:registerUser');
		$this->post('/actualizar/','App\Controller\Autentificacion:refreshToken');
		$this->post('/recuperar-password/','App\Controller\Autentificacion:recoverPassword');
		$this->post('/restablecer-contrasena/','App\Controller\Autentificacion:restablecerContrasena');
		$this->post('/cambiar-password/','App\Controller\Autentificacion:cambiarPassword');
	    
	});