<?php

	namespace App\Controller;

    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as ResponseInterface;
    use App\Response\Base as Response;
    use App\Model as Model;

	class Noticias extends \App\Controller\Base {

		protected $model = 'App\Model\Noticias';

        public function getAll(Request $request, ResponseInterface $response, $args, $extras = array()) {
        	$extras = [
				'tags',
				'imagenes',
				//'comentarios',
				//'galeria',
				//'archivos',
        	];
			
            return parent::getAll($request, $response, $args, $extras );
        }

        public function get(Request $request, ResponseInterface $response, $args, $extras = array()) {
        	$extras = [
				'tags',
				'imagenes',
				'comentarios',
				'galeria',
				'galeria.imagenes',
				'archivos',
        	];
			
			
            $objSearch = $this->getData($request, $response, $args, $extras);
            return $response->withJson($objSearch);                  
        }

        public function getHistorico(Request $request, ResponseInterface $response, $args, $extras = array()) {

            $objSearch = Model\Noticias::all(['publishing_date']);


            $arrHistorico = [];
            foreach ($objSearch as $data) {
				$date = new \DateTime($data->publishing_date);
				$anno = $date->format('Y');
				$mes = $date->format('m');
            	$arrHistorico[(int)$anno][(int)$mes] = "$anno-$mes-01";
            }

            return $response->withJson($arrHistorico);                  
        }
	}