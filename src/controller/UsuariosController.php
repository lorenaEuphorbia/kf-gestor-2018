<?php

	namespace App\Controller;

    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as ResponseInterface;
    use App\Response\Base as Response;
    use App\Model as Model;

	class Usuarios extends \App\Controller\Base {

		protected $model = 'App\Model\Users';

		/**
		 * Devuelve los datos del usuairo
		 * @param  [type] $request  Debe contener la cabecera con el token del usuario
		 * @param  [type] $response [description]
		 * @param  [type] $args     [description]
		 * @return [type]           [description]
		 */
		public function getDatos ($request, $response, $args) {

	    	# recogemos el token
	    	$token = $request->getAttribute('token');
     		$objToken = Model\Users\Tokens::getValid ($token);

     		if ($objToken && $objToken->usuario) {     			
              return Response::create()->statusSuccess('Estos son los datos del usuario.')->setDatos($objToken->usuario->datos)->putJSON($response);
     		}

            return Response::create()->statusUnauthorized('Los datos del usuario son incorrectos.')->putJSON($response);
	    }

	    /**
	     * Actualiza o crea los datos del usuario
		 * @param [type] $request  Debe contener la cabecera con el token del usuario
	     * @param [type] $response [description]
	     * @param [type] $args     [description]
	     */
		public function setDatos ($request, $response, $args) {

	    	# recogemos el token
	    	$token = $request->getAttribute('token');
     		$objToken = Model\Users\Tokens::getValid ($token);

     		if ($objToken && $objToken->usuario) {

     			$data = $request->getParsedBody();
     			$datos = $objToken->usuario->datos? $objToken->usuario->datos->fill($data): new Model\Users\Data($data);

     			if ($objToken->usuario->datos()->save($datos)) {

              		return Response::create()->statusSuccess('Datos actualizados.')->setDatos($datos)->putJSON($response);
     			}

              	return Response::create()->statusError('Error al guardar los datos.')->putJSON($response);
     		}

            return Response::create()->statusUnauthorized('Los datos del usuario son incorrectos.')->putJSON($response);
	    }

	}