<?php

	namespace App\Controller;

    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as ResponseInterface;
    use App\Response\Base as Response;
    use App\Model as Model;

	class Productos extends \App\Controller\Base {

		protected $model = 'App\Model\Productos';

        public function getAll(Request $request, ResponseInterface $response, $args, $extras = array()) {
        	$extras = [
				'imagenes',
				'comentarios',
				'relacionados',

				//'archivos',
        	];

            return parent::getAll($request, $response, $args, $extras );
        }

        public function get(Request $request, ResponseInterface $response, $args, $extras = array()) {
        	$extras = [
				//'valores_nutricionales',
				'galeria',
				'galeria.imagenes',
				'imagenes',
				'comentarios',
				'relacionados'
        	];

            $objSearch = $this->getData($request, $response, $args, $extras);
            return $response->withJson($objSearch);
        }

		public function import (Request $request, ResponseInterface $response, $args, $extras = array()) {

            $builder = call_user_func(array($this->model, 'query'));

            $arrData = $request->getParsedBody();

            if (!empty($arrData['productos'])) {
            	$builder->delete();

	            foreach ($arrData['productos'] as $index => $objData) {

					$arrValores = $objData['valores_nutricionales'];
					unset($objData['valores_nutricionales']);

	           		$objDataModel = call_user_func(array($this->model, 'create'), $objData);
					if ($arrValores) {
						$objValores = $objDataModel->valores_nutricionales();
						foreach ($arrValores as $clave => $valor) {
							$objValor = $objValores->create([
								'clave' => $clave,
								'valor' => $valor,
							]);
						}
					}

					if (empty($objDataModel->codigo)) {

						$letra = substr(strrev ("TRWAGMYFPDXBNJZSQVHLCKE"), $objDataModel->id%23, 1);
						$codCat = str_pad($objDataModel->id, 4, "0", STR_PAD_RIGHT) . str_pad($objDataModel->id, 4, "0", STR_PAD_RIGHT);
						$codCat = substr(md5($codCat), strlen($objDataModel->id . ''), 4);

						$objDataModel->codigo = strtoupper("$codCat-$letra") . str_pad($objDataModel->id, 4, "0", STR_PAD_LEFT);
						$objDataModel->save ();
					}
	            }

	    		// else el usuario o la contraseña son incorrectos
				return Response::create()->statusSuccess('Productos importados')->putJSON($response);
	        }

    		// else el usuario o la contraseña son incorrectos
			return Response::create()->statusError('Los datos de entrada son incorrectos')->putJSON($response);
		}


	}
