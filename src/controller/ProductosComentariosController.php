<?php

	namespace App\Controller\Productos;

    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as ResponseInterface;
    use App\Response\Base as Response;
    use App\Model as Model;

	class Comentarios extends \App\Controller\Base {

		protected $model = 'App\Model\Productos\Comentarios';

        public function guardarComentario(Request $request, ResponseInterface $response, $args, $extras = array()) {

            # recogemos parámetros de entrada
            $data = $request->getParsedBody();

            # recogemos el token
            $token = $request->getAttribute('token');
            $objToken = Model\Users\Tokens::getValid ($token);

            // var_dump($objToken->usuario); exit;

            if ($objToken && $objToken->usuario) {
            	$objComentario = $objToken->usuario->comentarios()->firstOrNew(['id_parent'=>$args['id']]);
            	$objComentario->setNombreUsuario($objToken->usuario->datos->nombre, $objToken->usuario->datos->apellidos);
            	$objComentario->email_usuario = $objToken->usuario->username;
            	if ($objComentario->fill($data) && $objComentario->save()) {
                	return Response::create()->statusSuccess('Comentario Guardado.')->setDatos($objComentario)->putJSON($response);            		
            	}

                return Response::create()->statusError('Error al Guardar el Comentarios.')->putJSON($response);    
            }

            return Response::create()->statusError('Error en los parámetros de entrada, pruebe pasados unos minutos, por favor.')->putJSON($response);    
        }

            
	}