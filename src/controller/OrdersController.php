<?php

	namespace App\Controller;

    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as ResponseInterface;
    use App\Response\Base as Response;
    use App\Model as Model;

	class Orders extends \App\Controller\Base {
		
		protected $model = 'App\Model\Orders';

        public function getAll(Request $request, ResponseInterface $response, $args, $extras = array()) {
            $extras = [
                // 'articulos'
            ];

            # recogemos el token
            $token = $request->getAttribute('token');
            $objToken = Model\Users\Tokens::getValid ($token);
            $args['codigo_cliente'] = $objToken->usuario? $objToken->usuario->id: false;
            $args['historico'] = '1';

            return parent::getAll($request, $response, $args, $extras );            
        }

        public function get(Request $request, ResponseInterface $response, $args, $extras = array()) {
            $extras = [
                'articulos',
                'request'
            ];
            
            # recogemos el token
            $token = $request->getAttribute('token');
            $objToken = Model\Users\Tokens::getValid ($token);
            $args['codigo_cliente'] = $objToken->usuario? $objToken->usuario->id: false;

            return parent::get($request, $response, $args, $extras );    
        }

        public function getArticulos(Request $request, ResponseInterface $response, $args, $extras = array()) {
            $extras = [
            ];

            # recogemos el token
            $token = $request->getAttribute('token');
            $objToken = Model\Users\Tokens::getValid ($token);            
            $args['codigo_cliente'] = $objToken->usuario? $objToken->usuario->id: false;

            $objSearch = $this->getAllData($request, $response, $args, $extras);

            $arrArticulos = [];
            foreach ($objSearch as $order) {
                foreach ($order->articulos as $articulo) {
                    if (!isset($arrArticulos[$articulo->articulo])) {
                        $arrArticulos[$articulo->articulo] = [
                            'id' => $articulo->articulo,
                            'unidades' => 0
                        ];                    
                    }
                    $arrArticulos[$articulo->articulo]['unidades'] += $articulo->unidades;
                }
            }

            return Response::create()->setDatos ($arrArticulos)->putJSONObject($response);             
        }

	}