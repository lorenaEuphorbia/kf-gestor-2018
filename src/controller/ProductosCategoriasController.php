<?php

	namespace App\Controller\Productos;

    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as ResponseInterface;
    use App\Response\Base as Response;
    use App\Model as Model;

	class Categorias extends \App\Controller\Base {

		protected $model = 'App\Model\Productos\Categorias';

		public function import (Request $request, ResponseInterface $response, $args, $extras = array()) {

            $builder = call_user_func(array($this->model, 'query'));

            $arrData = $request->getParsedBody();

            if (!empty($arrData['categorias'])) {
            	$builder->delete();

	            foreach ($arrData['categorias'] as $index => $objData) {
	           		$objDataModel = call_user_func(array($this->model, 'create'), $objData);
	            }

	    		// else el usuario o la contraseña son incorrectos
				return Response::create()->statusSuccess('Categorias importadas')->putJSON($response);
	        }


    		// else el usuario o la contraseña son incorrectos
			return Response::create()->statusError('Los datos de entrada son incorrectos')->putJSON($response);
		}

	}