<?php

	namespace App\Controller;

    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as ResponseInterface;
    use App\Response\Base as Response;
    use App\Model as Model;

	class Formularios extends \App\Controller\Base {

		protected $model = 'App\Model\Formularios';

		public function enviarContacto (Request $request, ResponseInterface $response, $args, $extras = array()) {
	    	$data = $request->getParsedBody();

	    	if (!empty($data['nombre']) &&
	    		!empty($data['email']) &&
	    		!empty($data['mensaje']) &&
	    		!empty($data['is_aviso'])) {

	    		$formulario = \App\Model\Formularios::create($data);
	    		if ($formulario) {
	    			
					/// Cargamos otras clases necesarias (estáticas)
					require_once $_SERVER['DOCUMENT_ROOT'] . '/gestor/_inc/config.php';
					require_once $_SERVER['DOCUMENT_ROOT'] . '/gestor/_inc/routes.php';
					require_once $_SERVER['DOCUMENT_ROOT'] . '/gestor/_inc/functions.php';
					require_once $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-utilities.php';
					require_once $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-domElements.php';
					require_once $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-controller.php';
					require_once $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-adminPreferences.php';

					setAutoloader();

					/// Arrancamos el panel de control
					$b = new \Base();
					$correos = new \Correos($b);

					$formulario->is_correo = $correos->sendContacto($data);
					$formulario->save();

				  	return Response::create()->statusSuccess('El formulario se ha enviado correctamente')->setDatos($formulario)->putJSON($response);
	    		}

	    		// else el usuario o la contraseña son incorrectos
				return Response::create()->statusError('Es posible que el correo tenga algún dato no permitido, reviselo o pruebe pasados unos minutos, por favor')->putJSON($response);
	    	}
	    	else {
				return Response::create()->statusWarning('Rellena todos los datos, por favor')->putJSON($response);
	    	}


		} 
	}