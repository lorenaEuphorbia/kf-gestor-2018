<?php

	namespace App\Controller\Noticias;

    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as ResponseInterface;
    use App\Response\Base as Response;
    use App\Model as Model;
	 
	class Tags extends \App\Controller\Base {

		protected $model = 'App\Model\Noticias\Tags';
		
	}