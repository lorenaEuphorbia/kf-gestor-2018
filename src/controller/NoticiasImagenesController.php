<?php

	namespace App\Controller\Noticias;

    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as ResponseInterface;
    use App\Response\Base as Response;
    use App\Model as Model;
    use App\Files\Types as FileTypes;
    use App\Files\Errors as FileErrors;

	class Imagenes extends \App\Controller\Base {

		protected $model = 'App\Model\Noticias\Files';

		public function append (Request $request, ResponseInterface $response, $args, $extras = array()) {
			$objResponse = Response::create();
			$objProducto = Model\Noticias::where('id', $args['id'])->first();

			# comprobamos que el producto exista
			if ($objProducto) {
				$this->guardarImagenes ($objProducto, $request, $objResponse);
			}
			else {
				$objResponse->statusError('No se ha encontrado el producto con el código ' . $args['id']);
			}

			return $objResponse->putJSON ($response);
		}

		public function import (Request $request, ResponseInterface $response, $args, $extras = array()) {
			$objResponse = Response::create();
			$objProducto = Model\Noticias::where('id', $args['id'])->first();

			# comprobamos que el producto exista
			if ($objProducto) {
				# borramos imagenes antiguas
				$objResponse->imagenes_borradas = $objProducto->borrarImagenes ();
				$this->guardarImagenes ($objProducto, $request, $objResponse);

			}
			else {
				$objResponse->statusError('No se ha encontrado el producto con el código ' . $args['id']);
			}

			return $objResponse->putJSON ($response);
		}

		private function guardarImagenes ($objProducto, $request, $objResponse) {
				$uploadedFiles = $request->getUploadedFiles();
				$directorio_imagenes = $_SERVER['DOCUMENT_ROOT']  . $this->app['settings']['images_dir'] . $objProducto->codigo . '/';
				$arrErrores = array();

				if ($uploadedFiles) {
					foreach ($uploadedFiles as $clave => $uploadedFile) {
						// var_dump($_FILES[$clave]);
						// var_dump($uploadedFile);
						// exit;

						if (!$uploadedFile->getError()) {
							$strRuta = $_FILES[$clave]['tmp_name'];

							if (FileTypes::isTipo($strRuta, FileTypes::TIPO_IMAGEN)) {

								$image_size = getimagesize($strRuta);
								$image_size = $image_size? $image_size: [0,0];
								$mime_type = FileTypes::getMimeType($strRuta);
								$extension = FileTypes::getExtension($mime_type);

					            $arrParams = [
						            'owner_type' => 'Galeria',
						            'type' => $objProducto->codigo,
						            'title_1' => $uploadedFile->getClientFilename(),
						            'name' => $clave,
						            'mime_type' => $mime_type,
						            'size' => $uploadedFile->getSize(),
									'height' => $image_size[1],
									'width' => $image_size[0],
								];

					            $objImagen = Model\Noticias\Files::firstOrCreate($arrParams);
					            $objProducto->imagenes()->save($objImagen);

								$filename = $objImagen->id  . $extension;
							    Model\Noticias::moveUploadedFile ($filename, $objImagen->getRutaDirectorioServidor(), $uploadedFile);
							}
							else {
								$arrErrores[$clave]= 'El archivo ´' . $uploadedFile->getClientFilename() . '´ no es una imagen válida';
							}
						}
						else {
							$arrErrores[$clave]= FileErrors::getMensaje($_FILES[$clave]['error']);

						}
					}

					if (empty($arrErrores)) {
						$objResponse->statusSuccess('La subida de archivos se ha completado correctamente.');
					}
					else {
						$objResponse->statusWarning('La subida de archivos se ha completado con errores.');
						$objResponse->errores = $arrErrores;
					}
				}
				else {
					$objResponse->statusError('No se han encontrado fotos en la subida.');
				}
		}

		public function delete (Request $request, ResponseInterface $response, $args, $extras = array()) {
			$objResponse = Response::create();

			$objProducto = Model\Noticias::where('id', $args['id'])->first();
			$directorio_imagenes = $_SERVER['DOCUMENT_ROOT']  . $this->app['settings']['images_dir'] . $args['id'] . '/';

			if ($objProducto) {
				$objResponse->archivos_borrados = $objProducto->borrarImagenes ();

				if ($objResponse->archivos_borrados)
					$objResponse->statusSuccess('El borrado de archivos ha terminado correctamente.');
				else
                    $objResponse->statusWarning('No se han encontrado imagenes para ese producto.');
			}
			else {
				$objResponse->statusError('La noticia no existe.');
			}

			return $objResponse->putJSON ($response);
		}





	}
