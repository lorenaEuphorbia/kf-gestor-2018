<?php

	namespace App\Controller\Orders;

    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as ResponseInterface;
    use App\Response\Base as Response;
    use App\Model as Model;

	class Articulos extends \App\Controller\Base {
		
		protected $model = 'App\Model\Orders\Articulos';

        public function getAll(Request $request, ResponseInterface $response, $args, $extras = array()) {
            $extras = [
                // 'articulos'
            ];

            # recogemos el token
            $token = $request->getAttribute('token');
            $objToken = Model\Users\Tokens::getValid ($token);
            $args['codigo_cliente'] = $objToken->usuario? $objToken->usuario->id: false;

            return parent::getAll($request, $response, $args, $extras );            
        }           

        public function get(Request $request, ResponseInterface $response, $args, $extras = array()) {
            $extras = [
                'articulos',
                'request'
            ];
            
            # recogemos el token
            $token = $request->getAttribute('token');
            $objToken = Model\Users\Tokens::getValid ($token);
            $args['codigo_cliente'] = $objToken->usuario? $objToken->usuario->id: false;

            return parent::get($request, $response, $args, $extras );    
        }



	}