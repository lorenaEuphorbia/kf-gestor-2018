<?php

    namespace App\Controller;

    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as ResponseInterface;
    use App\Response\Base as Response;
    use App\Model as Model;
     
    abstract class Base {

        protected $app;
        protected $model;

        public function __construct($app) {
            $this->app = $app;
        }

        public function getAllData(Request $request, ResponseInterface $response, $args, $extras = array()) {
            return call_user_func(array($this->model, 'with'), $extras)->where($args)->get(); 
        }

        public function getData(Request $request, ResponseInterface $response, $args, $extras = array()) {
            return call_user_func(array($this->model, 'with'), $extras)->where($args)->first(); 
        }

        public function getAll(Request $request, ResponseInterface $response, $args, $extras = array()) {
            $objSearch = $this->getAllData($request, $response, $args, $extras);
            return Response::create()->setDatos ($objSearch)->putJSONObject($response);         
        }

        public function get(Request $request, ResponseInterface $response, $args, $extras = array()) {
            $objSearch = $this->getData($request, $response, $args, $extras);
            return Response::create()->setDatos ($objSearch)->putJSONObject($response);
        }
    }