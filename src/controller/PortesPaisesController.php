<?php

	namespace App\Controller\Portes;

    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as ResponseInterface;
    use App\Response\Base as Response;
    use App\Model as Model;

	class Paises extends \App\Controller\Base {
		
		protected $model = 'App\Model\Portes\Paises';

        public function getAll(Request $request, ResponseInterface $response, $args, $extras = array()) {
        	$extras = [
				'provincias',
        	];
			
            return parent::getAll($request, $response, $args, $extras );
        }
	}