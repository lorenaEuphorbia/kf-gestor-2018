<?php

	namespace App\Controller;

    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as ResponseInterface;
    use App\Response\Base as Response;
    use App\Model as Model;

	class Carrito extends \App\Controller\Base {
		
		protected $model = 'App\Model\Orders';

        public function addArticuloUnidades(Request $request, ResponseInterface $response, $args, $extras = array()) {

            # recogemos parámetros de entrada
            $data = $request->getParsedBody();

            $objOrder = $this->getCurrentOrder($request);
            $objArticulo = $objOrder->articulos()->firstOrNew(['articulo'=>$data['articulo']]);

            $unidadesActuales = !empty($objArticulo->unidades)? $objArticulo->unidades: 0;
            $unidadesEnviadas = !empty($data['unidades'])? $data['unidades']: 1;
            $objArticulo->unidades = $unidadesActuales + $unidadesEnviadas;  

            $objArticulo->unidades = $objArticulo->unidades > 0? $objArticulo->unidades: 0;

            if ($objArticulo->unidades > $objArticulo->producto->stock) {
                return Response::create()->statusWarning('No disponemos del stock solicitado.')->setDatos($objArticulo)->putJSON($response); 
            }
          

            if ($objOrder && $objArticulo->updateDatosArticulo()->save()) {
                $objOrder->updateDatosPedido()->save();
                return Response::create()->statusSuccess('Artículo añadido.')->setDatos($objOrder)->putJSON($response);
            }

            return Response::create()->statusError('Error al añadir el artículo.')->setDatos($objArticulo)->putJSON($response); 
        }


        public function check(Request $request, ResponseInterface $response, $args, $extras = array()) {

            # recogemos parámetros de entrada
            $data = $request->getParsedBody();
            $objOrder = $this->getCurrentOrder($request);

            $objOrderDestino = $this->getData($request, $response, ['id'=>$objOrder['id']], $extras);

            if ($objOrderDestino->id == $objOrder->id) {
                $arrErrors = [];

                if (!$objOrder->hasDatosEnvio ()) { $arrErrors['datos_envio'] = 'La datos de envío está incompletos o son incorrectos.';  }
                if (!$objOrder->hasDatosFacturacion ()) { $arrErrors['datos_facturacion'] = 'La datos de facturación está incompletos o son incorrectos.';  }
                if (!$objOrder->hasDatosGlobales ()) { $arrErrors['datos_globales'] = 'Tus datos de contacto están incompletos o son incorrectos.';  }

                if ($arrErrors) {
                    return Response::create()->statusWarning('No se puede continuar, hay datos del pedido incompletos o incorrectos.')->setDatos($arrErrors)->putJSON($response); 
                }
                return Response::create()->statusSuccess('Los datos del pedido están correctos.')->putJSON($response); 
            }
            return Response::create()->statusError('Los parámetros enviados son incorrectos.')->setDatos($objOrderDestino)->putJSON($response); 
        }

        public function checkArticulos(Request $request, ResponseInterface $response, $args, $extras = array()) {

            # recogemos parámetros de entrada
            $data = $request->getParsedBody();
            $objOrder = $this->getCurrentOrder($request);

            $objOrderDestino = $this->getData($request, $response, ['id'=>$objOrder['id']], $extras);

            if ($objOrderDestino->id == $objOrder->id) {
                $arrErrors = [];

                foreach ($objOrder->articulos as $index => $objArticulo) {
                    if (!$objArticulo->producto) {
                        $arrErrors[$objArticulo->id]['error'] = 'El artículo ' . $objArticulo->producto->nombre . ' no está disponible.';
                    }
                    else {
                        if (!$objArticulo->isVenta()) $arrErrors[$objArticulo->id]['venta'] = 'El artículo ' . $objArticulo->producto->nombre . ' no está a la venta.';
                        if (!$objArticulo->isStock()) $arrErrors[$objArticulo->id]['stock'] = 'No disponemos del stock solicitado para el articulo ' . $objArticulo->producto->nombre . '.';
                    }
                }

                if (!empty($arrErrors)) {
                    return Response::create()->statusWarning('No se puede continuar, debes modificar tu carrito:')->setDatos($arrErrors)->putJSON($response); 
                }
                return Response::create()->statusSuccess('Los datos de los artículos están correctos.')->setDatos($arrErrors)->putJSON($response); 
            }
            return Response::create()->statusError('Los parámetros enviados son incorrectos.')->setDatos($objOrderDestino)->putJSON($response); 
        }

        public function getAll(Request $request, ResponseInterface $response, $args, $extras = array()) {
        	$extras = [
				'imagenes',
				//'comentarios',
				//'galeria',
				//'archivos',
        	];
			
            return parent::getAll($request, $response, $args, $extras );
        }

        public function get (Request $request, ResponseInterface $response, $args, $extras = array()) {	
            $objOrder = $this->getCurrentOrder($request);
            if ($objOrder) $objOrder->articulos;

            return Response::create()->statusSuccess('Este es tu carrito.')->setDatos($objOrder)->putJSONObject($response);
        }

        private function getCurrentOrder($request) {
            # recogemos el token
            $token = $request->getAttribute('token');
            $objToken = Model\Users\Tokens::getValid ($token);
            
            return $objToken? $objToken->getCurrentOrder (): false;
        }

        public function putCarrito(Request $request, ResponseInterface $response, $args, $extras = array()) {

            # recogemos parámetros de entrada
            $data = $request->getParsedBody();
            $objOrder = $this->getCurrentOrder($request);

            $objOrderDestino = $this->getData($request, $response, ['id'=>$objOrder['id']], $extras);

            //var_dump($data);
            if ($objOrderDestino->id == $objOrder->id) {
                if ($objOrder->fill($data)->updateDatosPedido()->save()) {
                    $objOrderReturn = $this->getData($request, $response, ['id'=>$objOrder['id']], $extras);
                    $objOrderReturn->articulos;

                    return Response::create()->statusSuccess('Carrito actualizado.')->setDatos($objOrderReturn)->putJSON($response);                    
                }
            }

            return Response::create()->statusError('Los parámetros enviados son incorrectos.')->setDatos($objOrderDestino)->putJSON($response); 
        }

        public function putArticulo(Request $request, ResponseInterface $response, $args, $extras = array()) {

            # recogemos parámetros de entrada
            $data = $request->getParsedBody();
            $objOrder = $this->getCurrentOrder($request);

            $objArticulo = $objOrder->articulos()->find($data->id);

            $unidadesActuales = !empty($objArticulo->unidades)? $objArticulo->unidades: 0;
            $unidadesEnviadas = !empty($data['unidades'])? $data['unidades']: 1;
            $objArticulo->unidades = $unidadesActuales + $unidadesEnviadas;

            $objArticulo->unidades = $objArticulo->unidades > 0? $objArticulo->unidades: 0;

            if (!$objArticulo->isVenta()) {
                return Response::create()->statusWarning('Ese artículo ya no se encuentra a la venta.')->setDatos($objArticulo)->putJSON($response);
            }

            if (!$objArticulo->isStock()) {
                $objArticulo->unidades = $objArticulo->producto->stock;
                return Response::create()->statusWarning('No disponemos del stock solicitado.')->setDatos($objArticulo)->putJSON($response);
            }

            if ($objOrder && $objOrder->articulos()->save($objArticulo)) {
                $objOrder->updateDatosPedido()->save();
                return Response::create()->statusSuccess('Artículo añadido.')->putJSON($response);
            }

            return Response::create()->statusError('Error al añadir el artículo.')->putJSON($response); 
        }

        public function removeArticulo(Request $request, ResponseInterface $response, $args, $extras = array()) {
            $id = $args['id'];

            $objOrder = $this->getCurrentOrder($request);
            $objArticulo = $objOrder->articulos()->find($id);

            if ($objArticulo && $objArticulo->delete()) {
                $objOrder->updateDatosPedido()->save();
                return Response::create()->statusSuccess('Artículo borrado.')->putJSON($response);
            }
            else {
                return Response::create()->statusError('Error al borrar el artículo.')->putJSON($response);              
            }
        }

        public function setArticulo(Request $request, ResponseInterface $response, $args, $extras = array()) {

            # recogemos parámetros de entrada
            $data = $request->getParsedBody();

            $id = !empty($data['id'])? $data['id']: false;
            $articulo = !empty($data['articulo'])? $data['articulo']: false;
            $talla = !empty($data['talla'])? $data['talla']: null;
            $unidades = !empty($data['unidades']) && $data['unidades'] > 0? $data['unidades']: 0;

            $objOrder = $this->getCurrentOrder($request);
            $objArticulo = $objOrder->articulos()->find($id);
            $objArticulo->unidades = $unidades;
            $objArticulo->talla = $talla;

            if (!$objArticulo->isVenta()) {
                return Response::create()->statusWarning('Ese artículo ya no se encuentra a la venta.')->setDatos($objArticulo)->putJSON($response);
            }

            if (!$objArticulo->isStock()) {
                $objArticulo->unidades = $objArticulo->producto->stock;
                return Response::create()->statusWarning('No disponemos del stock solicitado.')->setDatos($objArticulo)->putJSON($response);
            }

            if ($objArticulo && $objArticulo->save()) {
                $objOrder->updateDatosPedido()->save();
                return Response::create()->statusSuccess('Artículo guardado.')->setDatos($objArticulo)->putJSON($response);
            }
            else {
                return Response::create()->statusError('Error al guardar el artículo.')->setDatos($objArticulo)->putJSON($response);              
            }
        }

            



	}