<?php

	namespace App\Controller;

    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as ResponseInterface;
    use App\Response\Base as Response;
    use App\Model as Model;

	class Preferencias extends \App\Controller\Base {

		protected $model = 'App\Model\Preferencias';

        public function getAll(Request $request, ResponseInterface $response, $args, $extras = array()) {
        	$args = [
        		'guarded' => 0
        	];

            return parent::getAll($request, $response, $args, $extras );
        }

        public function getAllAsoc(Request $request, ResponseInterface $response, $args, $extras = array()) {
        	$args = [
        		'guarded' => 0
        	];


        	$objArray = [];
            $objSearch = $this->getAllData($request, $response, $args, $extras);
            foreach ($objSearch as $index => $row) {
            	$objArray[$row['id']] = $row['text_1'];
            }

            return Response::create()->setDatos ($objArray)->putJSONObject($response);
        }

        public function get(Request $request, ResponseInterface $response, $args, $extras = array()) {
        	$args = ['guarded' => 0];
					$extras = [
							'imagenes'
					];
            return parent::get($request, $response, $args, $extras );
        }
	}
