<?php

	namespace App\Controller;

    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as ResponseInterface;
    use App\Response\Base as Response;
    use App\Model as Model;

    use \Firebase\JWT\JWT;
    JWT::$leeway = 60 * $container->settings['token_time'];

	class Autentificacion {

		private $app;

		public function __construct( $app ) {
			$this->app = $app;
		}

    public function cambiarPassword (Request $request, ResponseInterface $response, $args) {

      # recogemos el token
      $token = $request->getAttribute('token');
      $objToken = Model\Users\Tokens::getValid ($token);

      # recogemos parámetros de entrada
      $data = $request->getParsedBody();

      if (!empty($data['password_old']) &&
        !empty($data['password']) &&
        !empty($data['password_repeat'])) {

        $password_old = $data['password_old'];
        $password = $data['password'];
        $password_repeat = $data['password_repeat'];

        if ($objToken && $objToken->usuario) {   
          if ($objToken->usuario->isPasswordCorrect($password_old)) {
            if ($password == $password_repeat) {
              if (\Utilities::checkPassword($password)) {
                $objToken->usuario->newPassword ($password)->save();
                return Response::create()->statusSuccess('La contraseña se ha modificado.')->putJSON($response);
              }
              return Response::create()->statusError('La contraseña debe tener al menos 6 caracteres y contener como mínimo una letra y un número.')->putJSON($response);
            }
            return Response::create()->statusError('Las contraseñas no coinciden.')->putJSON($response);
          }
          return Response::create()->statusError('La contraseña actual no es correcta.')->putJSON($response);
        }
        return Response::create()->statusUnauthorized('Usuario no encontrado.')->putJSON($response);
      }
      return Response::create()->statusError('Debes rellenar todos los datos.')->putJSON($response);
    }

    /**
     * Crea un token con el usuario y contraseña pasados como parámetros
     * @param  Request           $request  [description]
     * @param  ResponseInterface $response [description]
     * @param  [type]            $args     [description]
     * @return [type]                      [description]
     */
    public function createToken (Request $request, ResponseInterface $response, $args) {
      global $container;

      # recogemos el token
      $token = $request->getAttribute('token');
      $objToken = Model\Users\Tokens::getValid ($token);

      # creamos uno nuevo
      if (!$objToken || !$objToken->isValido()) {
        $objToken = new Model\Users\Tokens;
        $objToken->save();

        $objResponseHandler = Response::create()->statusCreated('Token creado correctamente');
      }
      else {        
        $objResponseHandler = Response::create()->statusCreated('Token actualizado correctamente');
      }

      # preparamos llamada JWT
      $expires = intval($container['settings']['token_time']);
      $secret = $container['settings']['api_key'];
      $algorithms = $container['settings']['api_algorithms'];
      $payload = $objToken->getJWTPayload($expires);

      $objResponseHandler->token = JWT::encode($payload, $secret, $algorithms);
      //$objResponseHandler->token_entrada = $token;
      //$objResponseHandler->token_entrada_obj = $objToken;
      //$objResponseHandler->is_valido = $objToken->isValido();
      $objResponseHandler->expires = $objToken->getExpiresTime ($expires);

      return $objResponseHandler->putJSON($response);
    }



    /**
     * Crea un token con el usuario y contraseña pasados como parámetros
     * @param  Request           $request  [description]
     * @param  ResponseInterface $response [description]
     * @param  [type]            $args     [description]
     * @return [type]                      [description]
     */
    public function loginUser (Request $request, ResponseInterface $response, $args) {
      global $container;

      # recogemos parámetros de entrada
      $data = $request->getParsedBody();
      $username = $data['username'];
      $password = $data['password'];

      # validamos el usuario
      $user = Model\Users::getByAutentificacion ($username, $password);
      if ($user) {

        # recogemos el token
        $token = $request->getAttribute('token');
        $objToken = Model\Users\Tokens::getValid ($token);
        $objTokenUsuarioOld = $user->token_valido();

        if ($objToken) {

          # expiramos token antiguo
          if ($objTokenUsuarioOld &&
              $objToken->id !== $objTokenUsuarioOld->id) {
            $objTokenUsuarioOld->expirar();
          }

          # comprobamos que no tenga cliente o sea el cliente destino
          if ($objToken->id_usuario == null || $objToken->id_usuario === $user->id) {
            $user->tokens()->save($objToken);
          }
        }

        # creamos token para el usuario si no tiene
        $objTokenUsuario = $user->token_valido();
        if (!$objTokenUsuario) $objTokenUsuario = $user->newToken();

        # asignamos pedido actual
        $objOrder = $objTokenUsuario->getCurrentOrder ();
        $objOrder->codigo_cliente = $user->id;
        $objOrder->fillDatosUsuarioGlobales ();
        $objOrder->fillDatosUsuarioFacturacion ();
        $objOrder->fillDatosUsuarioEnvio ();
        $objOrder->save();

        # preparamos llamada JWT
        $expires = intval($container['settings']['token_time']);
        $secret = $container['settings']['api_key'];
        $algorithms = $container['settings']['api_algorithms'];
        $payload = $objTokenUsuario->getJWTPayload($expires);

        $objResponseHandler = Response::create()->statusCreated('Token creado correctamente');
        $objResponseHandler->token = JWT::encode($payload, $secret, $algorithms);
        $objResponseHandler->expires = $objTokenUsuario->getExpiresTime ($expires);

        return $objResponseHandler->putJSON($response);
      }

      // else el usuario o la contraseña son incorrectos 
      return Response::create()->statusUnauthorized('Usuario o contraseña incorrectos')->putJSON($response);
    }
    
    /**
     * Invalida el token de la base de datos
     * @param  Request           $request  [description]
     * @param  ResponseInterface $response [description]
     * @param  [type]            $args     [description]
     * @return [type]                      [description]
     */
    public function deleteToken (Request $request, ResponseInterface $response, $args) {

      # recogemos el token
      $token = $request->getAttribute('token');
      $objToken = Model\Users\Tokens::getValid ($token);

      if ($objToken) {
        $objToken->expirar();
        $objResponseHandler = Response::create()->statusSuccess('Token borrado correctamente');
      }
      else {
        $objResponseHandler = Response::create()->statusInfo('El token ya no es válido');
      }
      return $objResponseHandler->putJSON($response);
    }
    
    public function recoverPassword (Request $request, ResponseInterface $response, $args) {

      # recogemos parámetros de entrada
      $data = $request->getParsedBody();
      $username = $data['username'];

      # validamos el usuario
      $user = Model\Users::getByUsername ($username);
      if ($user) {        
            
          /// Cargamos otras clases necesarias (estáticas)
          require_once $_SERVER['DOCUMENT_ROOT'] . '/gestor/_inc/config.php';
          require_once $_SERVER['DOCUMENT_ROOT'] . '/gestor/_inc/routes.php';
          require_once $_SERVER['DOCUMENT_ROOT'] . '/gestor/_inc/functions.php';
          require_once $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-utilities.php';
          require_once $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-domElements.php';
          require_once $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-controller.php';
          require_once $_SERVER['DOCUMENT_ROOT'] . '/' . ADMIN_FOLDER . '/_classes/class-adminPreferences.php';

          setAutoloader();

          /// Arrancamos el panel de control
          $b = new \Base();
          $correos = new \Correos($b);

          if ($user->datos && $correos->sendRecoverPassword($user->datos->toArray())) {
            return Response::create()->statusSuccess('Se ha enviado un correo a ' . $username . ' con los pasos a seguir.')->putJSON($response);
          }
          else {
            return Response::create()->statusError('Lo sentimos, ha habido un error al enviar el correo, pruebe pasados unos minutos.')->putJSON($response);            
          }

      }

      // else el usuario o la contraseña son incorrectos 
      return Response::create()->statusUnauthorized('Lo sentimos, no hemos encontrado ningún usuario con ese email.')->putJSON($response);
    }

    public function restablecerContrasena (Request $request, ResponseInterface $response, $args) {

      # recogemos parámetros de entrada
      $data = $request->getParsedBody();
      $email = @$data['email'];
      $token = @$data['token'];
      $password = @$data['password'];
      $password_repeat = @$data['password_repeat'];

      # validamos el token
      $request = Model\PasswordRequests::searchRequest ($email, $token);

      if ($request && $usuario = $request->usuario()) {
          if (isset($data['password']) && isset($data['password_repeat'])) {
            if ($password && $password_repeat) {
              if ($password == $password_repeat) {
                if (\Utilities::checkPassword($password)) {
                  $usuario->newPassword ($password)->save();
                  $request->delete();
                  return Response::create()->statusSuccess('Los datos se han modificado, logueate con tu nueva contraseña.')->putJSON($response);
                }
                return Response::create()->statusError('La contraseña debe tener al menos 6 caracteres y contener como mínimo una letra y un número.')->putJSON($response);
              }
              return Response::create()->statusError('Las contraseñas no coinciden.')->putJSON($response);
            }
            return Response::create()->statusError('Debes rellenar todos los campos.')->putJSON($response);
          }
          else {
            /* ACTUALIZAMOS LA FECHA */
            $request->date = date('Y-m-d H:i:s');
            $request->save();
            return Response::create()->statusSuccess('Las credenciales son correctas.')->putJSON($response);            
          }
      }
      // else el usuario o la contraseña son incorrectos 
      return Response::create()->setDatos($request)->statusError('Lo sentimos, los datos enviados han caducado o son erroneos, vuelva a restablecer su contraseña')->putJSON($response);     
    }

    /**
     * Registra el usuario. Comprueba si no existe y la seguridad de la contraseña
     * @param  Request           $request  [description]
     * @param  ResponseInterface $response [description]
     * @param  [type]            $args     [description]
     * @return [type]                      [description]
     */
    public function registerUser (Request $request, ResponseInterface $response, $args) {
      global $container;

      # recogemos parámetros de entrada
      $data = $request->getParsedBody();
      $username = $data['username'];
      $password = $data['password'];
      $password_repeat = $data['password_repeat'];

      $users = Model\Users::query();
      # buscamos usuario
      if (!$users->where(['username'=>$username,'baja'=>null])->first()) {
        if ($password === $password_repeat) {
          if (\Utilities::checkPassword($password)) {            
            $usuario = new Model\Users();
            $usuario->username = $username;
            $usuario->newPassword ($password);
            $datos = new Model\Users\Data();

            if ($usuario->save() && $usuario->datos()->save($datos)) {
              return Response::create()->statusSuccess('Usuario creado. Logueate con tus datos.')->putJSON($response);
            }
            return Response::create()->statusError('Error al crear el usuario.')->putJSON($response);
          }
          return Response::create()->statusError($password_repeat . 'La contraseña debe tener al menos 6 caracteres y contener como mínimo una letra y un número.')->putJSON($response);
        }
        return Response::create()->statusError('Las contraseñas no coinciden.')->putJSON($response);
      }
      return Response::create()->statusError('Ya existe un usuario con ese nombre.')->putJSON($response);
    }

    /**
     * Actualiza el token
     * @param  Request           $request  [description]
     * @param  ResponseInterface $response [description]
     * @param  [type]            $args     [description]
     * @return [type]                      [description]
     */
    public function refreshToken (Request $request, ResponseInterface $response, $args) {
      global $container;

      # recogemos parámetros de entrada
      $data = $request->getParsedBody();
      $username = $data['username'];
      $token = $data['token'];

      # recogemos el token
      $userToken = Model\Users\Tokens::getValid ($token);

      # comprobamos el token
      if ($userToken && $userToken->usuario && $userToken->usuario->username && $userToken->usuario->username == $username) {
        $userToken->touch();

        # preparamos llamada JWT
        $expires = intval($container['settings']['token_time']);
        $secret = $container['settings']['api_key'];
        $algorithms = $container['settings']['api_algorithms'];
        $payload = $userToken->getJWTPayload($expires);

        $objResponseHandler = Response::create()->statusCreated('Token actualizado correctamente');
        $objResponseHandler->token = JWT::encode($payload, $secret, $algorithms);
        $objResponseHandler->expires = $userToken->getExpiresTime ($expires);

        return $objResponseHandler->putJSON($response);
      }

      return Response::create()->statusUnauthorized('El token es inválido')->putJSON($response);
    }

    /**
     * Devuelve los datos de usuario y sus roles
     * @param  Request           $request  [description]
     * @param  ResponseInterface $response [description]
     * @param  [type]            $args     [description]
     * @return [type]                      [description]
     */
    public function getTokenAuth (Request $request, ResponseInterface $response, $args) {

      # recogemos el token
      $token = $request->getAttribute('token');
      $objToken = Model\Users\Tokens::getValid ($token);

      if ($objToken) {
        $objResponseHandler = Response::create()->statusSuccess('Estos son los datos del usuario');
        $objResponseHandler->usuario = $objToken->usuario;
        $objResponseHandler->roles = $objToken->usuario ? $objToken->usuario->roles: null;        
      }
      else {
        $objResponseHandler = Response::create()->statusError('El token de entrada es incorrecto o ha caducado');
        $objResponseHandler->usuario = null;
        $objResponseHandler->roles = null;     

      }

      return $objResponseHandler->putJSON($response);

    }




	}