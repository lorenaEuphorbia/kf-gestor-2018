<?php

    namespace App\Response;

    class Base {

        private $status;
        private $status_code;
        private $result_code;
        private $mensaje;
        private $datos;

        # ESTADOS
        const STATUS_INFO           = 0x1;
        const STATUS_SUCCESS        = 0x2;
        const STATUS_WARNING        = 0x4;
        const STATUS_ERROR          = 0x8;
        const STATUS_DEBUG          = 0x16;
        const STATUS_UNAUTHORIZED   = 0x32;
        const STATUS_NOTALLOWED     = 0x64;
        const STATUS_CREATED        = 0x128;
        const STATUS_FORBIDDEN      = 0x256;

        public function __construct($statusCode = 0x1, $mensaje = '', $datos = false, $result_code = 0) {
            $this->setStatusCode ($statusCode);
            $this->setResultCode ($result_code);
            $this->setMensaje ($mensaje);
            $this->setDatos ($datos);
        }

        public static function create ($statusCode = 0x1, $mensaje = '', $datos = false, $result_code = 0) {
            return new self ($statusCode, $mensaje, $datos, $result_code);
        }

        public function isError () {
            return $this->status_code > self::STATUS_SUCCESS;
        }

        public function putJSON ($response) {
            return $this->putHeaders($response)->withJson($this->getData());
        }

        public function putJSONObject ($response) {
            return $this->putHeaders($response)->withJson($this->datos);
        }

        public function putHeaders ($response) {
            return $response
                ->withHeader('Access-Control-Allow-Origin', '*')
                ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
                ->withStatus(self::getStatusCodeHTML($this->status_code));
        }

        public function getData () {
            return [
                'status' => $this->status,
                'status_code' => $this->status_code,
                'result_code' => $this->result_code,
                'mensaje' => $this->mensaje,
                'datos' => $this->datos,
            ];
        }

        public function __set($name, $value) {
            $this->datos[$name] = $value;
        }

        public function __get($name) {
            if (array_key_exists($name, $this->datos)) {
                return $this->datos[$name];
            }

            $trace = debug_backtrace();
            trigger_error(
                'Propiedad indefinida mediante __get(): ' . $name .
                ' en ' . $trace[0]['file'] .
                ' en la línea ' . $trace[0]['line'],
                E_USER_NOTICE);
            return null;
        }

        public function __isset($name) {
            return isset($this->data[$name]);
        }

        public function __unset($name) {
            unset($this->data[$name]);
        }

        public function setDatos ($datos) {
            $this->datos = $datos;
            return $this;
        }

        public function setResultCode ($result_code) {
            $this->result_code = $result_code;
            return $this;
        }

        public function setMensaje ($mensaje) {
            $this->mensaje = $mensaje;
            return $this;
        }

        public function setStatusCode ($statusCode) {
            $this->status_code = $statusCode;
            return $this->setStatus($statusCode);
        }

        public function setStatus ($statusCode) {
            $this->status = self::getStatusString ($statusCode);
            return $this;
        }        

        public function statusInfo ($mensaje = '', $result_code = 0) {
            return $this->setStatusCode (self::STATUS_INFO)->setMensaje ($mensaje)->setResultCode ($result_code);
        }
        
        public function statusSuccess ($mensaje = '', $result_code = 0) {
            return $this->setStatusCode (self::STATUS_SUCCESS)->setMensaje ($mensaje)->setResultCode ($result_code);
        }
        
        public function statusWarning ($mensaje = '', $result_code = 0) {
            return $this->setStatusCode (self::STATUS_WARNING)->setMensaje ($mensaje)->setResultCode ($result_code);
        }
        
        public function statusError ($mensaje = '', $result_code = 0) {
            return $this->setStatusCode (self::STATUS_ERROR)->setMensaje ($mensaje)->setResultCode ($result_code);
        }
        
        public function statusDebug ($mensaje = '', $result_code = 0) {
            return $this->setStatusCode (self::STATUS_DEBUG)->setMensaje ($mensaje)->setResultCode ($result_code);
        }
        
        public function statusUnauthorized  ($mensaje = '', $result_code = 0) {
            return $this->setStatusCode (self::STATUS_UNAUTHORIZED)->setMensaje ($mensaje)->setResultCode ($result_code);
        }

        public function statusNotallowed  ($mensaje = '', $result_code = 0) {
            return $this->setStatusCode (self::STATUS_NOTALLOWED)->setMensaje ($mensaje)->setResultCode ($result_code);
        }

        public function statusCreated  ($mensaje = '', $result_code = 0) {
            return $this->setStatusCode (self::STATUS_CREATED)->setMensaje ($mensaje)->setResultCode ($result_code);
        }

        public function statusForbidden  ($mensaje = '', $result_code = 0) {
            return $this->setStatusCode (self::STATUS_FORBIDDEN)->setMensaje ($mensaje)->setResultCode ($result_code);
        }

        public static function getStatusString ($status) {
            switch ($status) {
                case self::STATUS_INFO: return 'info';
                case self::STATUS_SUCCESS: return 'success';
                case self::STATUS_WARNING: return 'warning';
                case self::STATUS_ERROR: return 'error';
                case self::STATUS_DEBUG: return 'debug';
                case self::STATUS_UNAUTHORIZED: return 'error';
                case self::STATUS_NOTALLOWED: return 'error';
                case self::STATUS_CREATED: return 'success';
                case self::STATUS_FORBIDDEN: return 'forbidden';
            }
            return '';
        }

        public static function getStatusCodeHTML ($status) {
            switch ($status) {
                case self::STATUS_INFO: return 200;
                case self::STATUS_SUCCESS: return 200;
                case self::STATUS_WARNING: return 200;
                case self::STATUS_ERROR: return 410;
                case self::STATUS_DEBUG: return 200;
                case self::STATUS_UNAUTHORIZED: return 401;
                case self::STATUS_NOTALLOWED: return 405;
                case self::STATUS_CREATED: return 201;
                case self::STATUS_FORBIDDEN: return 403;
            }
            return 404;
        }
    }