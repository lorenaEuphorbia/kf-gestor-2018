<?php

        namespace App\Response;

        class Status{

                const CONTINUE_ = 100;
                const SWITCHING_PROTOCOLS = 101;
                const OK = 200;
                const CREATED = 201;
                const ACCEPTED = 202;
                const NON_AUTHORITATIVE_INFORMATION = 203;
                const NO_CONTENT = 204;
                const RESET_CONTENT = 205;
                const PARTIAL_CONTENT = 206;
                const MULTIPLE_CHOICES = 300;
                const MOVED_PERMANENTLY = 301;
                const FOUND = 302;
                const SEE_OTHER = 303;
                const NOT_MODIFIED = 304;
                const USE_PROXY = 305;
                const TEMPORARY_REDIRECT = 307;
                const BAD_REQUEST = 400;
                const UNAUTHORIZED = 401;
                const PAYMENT_REQUIRED = 402;
                const FORBIDDEN = 403;
                const NOT_FOUND = 404;
                const METHOD_NOT_ALLOWED = 405;
                const NOT_ACCEPTABLE = 406;
                const PROXY_AUTHENTICATION_REQUIRED = 407;
                const REQUEST_TIME_OUT = 408;
                const CONFLICT = 409;
                const GONE = 410;
                const LENGTH_REQUIRED = 411;
                const PRECONDITION_FAILED = 412;
                const REQUEST_ENTITY_TOO_LARGE = 413;
                const REQUEST_URI_TOO_LARGE = 414;
                const UNSUPPORTED_MEDIA_TYPE = 415;
                const REQUESTED_RANGE_NOT_SATISFIABLE = 416;
                const EXPECTATION_FAILED = 417;
                const INTERNAL_SERVER_ERROR = 500;
                const NOT_IMPLEMENTED = 501;
                const BAD_GATEWAY = 502;
                const SERVICE_UNAVAILABLE = 503;
                const GATEWAY_TIME_OUT = 504;
                const HTTP_VERSION_NOT_SUPPORTED = 505;


                public static function toString ($status) {
                        switch ($status) {
                                case self::CONTINUE_: return 'Continue';
                                case self::SWITCHING_PROTOCOLS: return 'Switching Protocols';
                                case self::OK: return 'OK';
                                case self::CREATED: return 'Created';
                                case self::ACCEPTED: return 'Accepted';
                                case self::NON_AUTHORITATIVE_INFORMATION: return 'Non-Authoritative Information';
                                case self::NO_CONTENT: return 'No Content';
                                case self::RESET_CONTENT: return 'Reset Content';
                                case self::PARTIAL_CONTENT: return 'Partial Content';
                                case self::MULTIPLE_CHOICES: return 'Multiple Choices';
                                case self::MOVED_PERMANENTLY: return 'Moved Permanently';
                                case self::FOUND: return 'Found';
                                case self::SEE_OTHER: return 'See Other';
                                case self::NOT_MODIFIED: return 'Not Modified';
                                case self::USE_PROXY: return 'Use Proxy';
                                case self::TEMPORARY_REDIRECT: return 'Temporary Redirect';
                                case self::BAD_REQUEST: return 'Bad Request';
                                case self::UNAUTHORIZED: return 'Unauthorized';
                                case self::PAYMENT_REQUIRED: return 'Payment Required';
                                case self::FORBIDDEN: return 'Forbidden';
                                case self::NOT_FOUND: return 'Not Found';
                                case self::METHOD_NOT_ALLOWED: return 'Method Not Allowed';
                                case self::NOT_ACCEPTABLE: return 'Not Acceptable';
                                case self::PROXY_AUTHENTICATION_REQUIRED: return 'Proxy Authentication Required';
                                case self::REQUEST_TIME_OUT: return 'Request Time-out';
                                case self::CONFLICT: return 'Conflict';
                                case self::GONE: return 'Gone';
                                case self::LENGTH_REQUIRED: return 'Length Required';
                                case self::PRECONDITION_FAILED: return 'Precondition Failed';
                                case self::REQUEST_ENTITY_TOO_LARGE: return 'Request Entity Too Large';
                                case self::REQUEST_URI_TOO_LARGE: return 'Request-URI Too Large';
                                case self::UNSUPPORTED_MEDIA_TYPE: return 'Unsupported Media Type';
                                case self::REQUESTED_RANGE_NOT_SATISFIABLE: return 'Requested range not satisfiable';
                                case self::EXPECTATION_FAILED: return 'Expectation Failed';
                                case self::INTERNAL_SERVER_ERROR: return 'Internal Server Error';
                                case self::NOT_IMPLEMENTED: return 'Not Implemented';
                                case self::BAD_GATEWAY: return 'Bad Gateway';
                                case self::SERVICE_UNAVAILABLE: return 'Service Unavailable';
                                case self::GATEWAY_TIME_OUT: return 'Gateway Time-out';
                                case self::HTTP_VERSION_NOT_SUPPORTED: return 'HTTP Version not supported';
                        }
                        return 'Undefined status';
                }

        }