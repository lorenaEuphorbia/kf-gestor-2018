<?php
return [
    'settings' => [

        # Allow the web server to send the content-length header 
        'addContentLengthHeader' => false,    

        'api_key' => '2ee43360-7090-4901-8f9b-3f499d769db4',
        'api_key_public' => '2ee43360-7090-4901-8f9b-3f499d769db4',
        'api_algorithms' => 'HS256',

        # Database Config
        'db' => [
            'driver' => DB_DRIVER,
            'host' => DB_HOST,
            'database' => DB_NAME,
            'username' => DB_USER,
            'password' => DB_PASS,
            'charset'   => 'utf8',
            'collation' => 'utf8_spanish2_ci',
            'prefix'    => '',
            'strict' => false
        ],

        'determineRouteBeforeAppMiddleware' => false,

        # set to false in production
        'displayErrorDetails' => true, 

        # Folders Scripts PHP
        'includes' => [
            'config',
            'handler',
            'model',
            'route',
            'controller',
            'observer',
        ],

        # Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            //'level' => \Monolog\Logger::DEBUG,
        ],

        'outputBuffering' => false,

        # Setting private routes
        'private_routes' => [
            [
                'path' => '/autentificacion',
                'public_methods' => ['OPTIONS','GET','POST'],
                'roles' => false,
                'log_path' => false,
            ],
            [
                'path' => '/usuario',
                'public_methods' => ['OPTIONS'],
                'roles' => [],
                'log_path' => false,
            ],
            [
                'path' => '/carrito',
                'public_methods' => ['OPTIONS'],
                'roles' => [],
                'log_path' => false,
            ],
            [
                'path' => '/orders',
                'public_methods' => ['OPTIONS'],
                'roles' => [],
                'log_path' => false,
            ],
            [
                'path' => '/import',
                'public_methods' => false,
                'roles' => ['publisher'],
                'log_path' => 'logs/',
            ],
            [
                'path' => '/productos/comentarios',
                'public_methods' => ['OPTIONS'],
                'roles' => false,
                'log_path' => 'logs/',
            ],
        ],

        # Setting public routes
        'public_routes' => [
            [
                'path' => '/autentificacion/token',
                'public_methods' => ['OPTIONS','GET'],
                'roles' => [],
                'log_path' => false,
            ],
        ],

        # Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        # Tiempo de login (minutos)
        'token_time' => 30,

        # Tiempo de login (minutos)
        'images_dir' => '/files/productos/',
    ],
];
