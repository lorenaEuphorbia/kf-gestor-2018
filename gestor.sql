-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 02-11-2017 a las 09:08:49
-- Versión del servidor: 5.7.19
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gestor`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formularios`
--

DROP TABLE IF EXISTS `formularios`;
CREATE TABLE IF NOT EXISTS `formularios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) DEFAULT NULL,
  `email` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `mensaje` text COLLATE utf8_spanish2_ci,
  `is_aviso` tinyint(1) NOT NULL DEFAULT '0',
  `is_correo` tinyint(1) DEFAULT '0',
  `creacion` timestamp NULL DEFAULT NULL,
  `modificacion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `formularios`
--

INSERT INTO `formularios` (`id`, `users_id`, `email`, `telefono`, `nombre`, `mensaje`, `is_aviso`, `is_correo`, `creacion`, `modificacion`) VALUES
(1, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:23:29', '2017-07-04 10:23:29'),
(2, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:23:53', '2017-07-04 10:23:53'),
(3, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:42:44', '2017-07-04 10:42:44'),
(4, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:43:19', '2017-07-04 10:43:19'),
(5, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:43:26', '2017-07-04 10:43:26'),
(6, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:45:44', '2017-07-04 10:45:44'),
(7, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:48:59', '2017-07-04 10:48:59'),
(8, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:49:25', '2017-07-04 10:49:25'),
(9, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:49:49', '2017-07-04 10:49:49'),
(10, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:50:30', '2017-07-04 10:50:30'),
(11, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-04 10:54:09', '2017-07-04 10:54:09'),
(12, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 1, '2017-07-04 10:55:32', '2017-07-04 12:55:33'),
(13, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 0, '2017-07-05 06:12:48', '2017-07-05 06:12:48'),
(14, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 1, '2017-07-05 06:14:02', '2017-07-05 08:14:02'),
(15, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 0, 1, '2017-07-05 06:15:21', '2017-07-05 08:15:21'),
(16, NULL, 'adsas@asdas.com', NULL, 'Cesar Vega Martin', 'asdasda das d s', 1, 1, '2017-07-05 07:11:51', '2017-07-05 09:11:52'),
(17, NULL, 'adsas@asdas.com', NULL, 'Cesar Vega Martin', 'asdasda das d s', 1, 1, '2017-07-05 07:12:04', '2017-07-05 09:12:04'),
(18, NULL, 'adsas@asdas.com', NULL, 'Cesar Vega Martin', 'asdasda das d s', 1, 1, '2017-07-05 07:19:01', '2017-07-05 09:19:02'),
(19, NULL, 'asdad@asdas.com', NULL, 'Cesar Vega Martin', 'asdas', 1, 1, '2017-07-05 07:25:04', '2017-07-05 09:25:05'),
(20, NULL, 'asdad@asdas.com', NULL, 'Cesar Vega Martin', 'asdas', 1, 1, '2017-07-05 07:27:01', '2017-07-05 09:27:05'),
(21, NULL, 'asdad@asdas.com', NULL, 'Cesar Vega Martin', 'asdas', 1, 1, '2017-07-05 09:08:14', '2017-07-05 11:08:15'),
(22, NULL, 'asdad@asdas.com', NULL, 'Cesar Vega Martin', 'asdas', 1, 1, '2017-07-05 09:08:27', '2017-07-05 11:08:34'),
(23, NULL, NULL, NULL, NULL, NULL, 0, 1, '2017-07-05 09:10:17', '2017-07-05 11:10:24'),
(24, NULL, 'cevm88@gmail.com', NULL, 'Cesar Vega Martin', 'asdas', 1, 1, '2017-07-05 09:10:40', '2017-07-05 11:10:41'),
(25, NULL, 'asdasda', NULL, 'Cesar Vega Martin', 'dsadasd', 1, 1, '2017-07-05 09:16:23', '2017-07-05 11:16:24'),
(26, NULL, 'asdasda', NULL, 'Cesar Vega Martin', 'dsadasd', 1, 1, '2017-07-05 09:20:53', '2017-07-05 11:20:53'),
(27, NULL, 'asdasda', NULL, 'Cesar Vega Martin', 'dsadasd', 1, 1, '2017-07-05 09:20:58', '2017-07-05 11:20:58'),
(28, NULL, 'adsas@asdas.com', NULL, 'Cesar Vega Martin', 'asdasd', 1, 1, '2017-07-05 09:22:27', '2017-07-05 11:22:28'),
(29, NULL, 'adsas@asdas.com', NULL, 'Cesar Vega Martin', 'asdasd', 1, 1, '2017-07-05 09:22:49', '2017-07-05 11:22:50'),
(30, NULL, 'cevm88@gmail.com', NULL, 'Cesar Vega Martin', 'asdasdad', 1, 1, '2017-07-05 09:23:01', '2017-07-05 11:23:02'),
(31, NULL, 'cevm88@gmail.com', NULL, 'Cesar Vega Martin', 'asdsadas', 1, 1, '2017-07-05 09:23:54', '2017-07-05 11:23:54'),
(32, NULL, 'cevm88@gmail.com', NULL, 'Cesar Vega Martin', 'asdsadas', 1, 1, '2017-07-05 09:25:33', '2017-07-05 11:25:34'),
(33, NULL, 'asdads', NULL, 'Cesar Vega Martin', 'asdas', 1, 1, '2017-07-05 09:31:27', '2017-07-05 11:31:28'),
(34, NULL, 'adsas@asdas.com', NULL, 'Cesar Vega Martin', 'asdasd', 1, 1, '2017-07-05 09:32:24', '2017-07-05 11:32:25'),
(35, NULL, 'asdad@asdas.com', NULL, 'Cesar Vega Martin', 'asd', 1, 1, '2017-07-05 09:33:17', '2017-07-05 11:33:18'),
(36, NULL, 'cevm88@gmail.com', NULL, 'Cesar Vega Martin', 'asdas', 1, 1, '2017-07-05 09:34:22', '2017-07-05 11:34:23'),
(37, NULL, 'cevm88@gmail.com', NULL, 'Cesar Vega Martin', 'ascd', 1, 1, '2017-07-05 09:34:59', '2017-07-05 11:35:00'),
(38, NULL, 'cevm88@gmail.com', NULL, 'Cesar Vega Martin', 'ceasdad', 1, 1, '2017-07-05 09:36:42', '2017-07-05 11:36:42'),
(39, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 1, 1, '2017-07-11 04:28:58', '2017-07-11 06:28:59'),
(40, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 1, 1, '2017-07-20 05:21:09', '2017-07-20 07:21:12'),
(41, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 1, 1, '2017-08-09 09:16:23', '2017-08-09 11:16:24'),
(42, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 1, 1, '2017-08-09 09:19:19', '2017-08-09 11:19:20'),
(43, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 1, 1, '2017-08-09 09:21:01', '2017-08-09 11:21:02'),
(44, NULL, 'cevm88@gmail.com', NULL, 'test', 'teasd', 1, 1, '2017-08-09 10:44:01', '2017-08-09 12:44:01'),
(45, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 1, 1, '2017-08-22 09:20:39', '2017-08-22 11:20:40'),
(46, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 1, 1, '2017-09-14 06:41:34', '2017-09-14 08:41:35'),
(47, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 1, 1, '2017-10-16 06:45:02', '2017-10-16 08:45:04'),
(48, NULL, 'cesar@euphorbia.es', NULL, 'cesar', 'HOLA!', 1, 1, '2017-10-16 06:48:49', '2017-10-16 08:48:49'),
(49, NULL, 'cevm88@gmail.com', '601181340', 'César Vega', 'asd', 1, 1, '2017-10-24 05:15:22', '2017-10-24 07:15:23'),
(50, NULL, 'cevm88@gmail.com', '601181340', 'César Vega', 'asdasdasda', 1, 1, '2017-10-24 05:17:06', '2017-10-24 07:17:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idiomas`
--

DROP TABLE IF EXISTS `idiomas`;
CREATE TABLE IF NOT EXISTS `idiomas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `region` varchar(5) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre_es` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `oculto` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `region` (`region`)
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `idiomas`
--

INSERT INTO `idiomas` (`id`, `codigo`, `region`, `nombre_es`, `nombre`, `activo`, `oculto`) VALUES
(1, 'en', 'en_US', 'Inglés', 'English - United States', 1, 0),
(2, 'es', 'es_ES', 'Español', 'Spanish - Spain (Traditional)', 1, 0),
(3, 'zh', 'zh_SG', 'Chino', 'Chinese - Singapore', 0, 1),
(4, 'fr', 'fr_FR', 'Francés', 'French - France', 0, 1),
(5, 'ar', 'ar', 'árabe', 'árabe', 0, 0),
(6, 'ar', 'ar_AE', 'árabe [Emiratos Árabes Unidos]', 'árabe [Emiratos Árabes Unidos]', 0, 0),
(7, 'ar', 'ar_BH', 'árabe [Bahráin]', 'árabe [Bahráin]', 0, 0),
(8, 'ar', 'ar_DZ', 'árabe [Argelia]', 'árabe [Argelia]', 0, 0),
(9, 'ar', 'ar_EG', 'árabe [Egipto]', 'árabe [Egipto]', 0, 0),
(10, 'ar', 'ar_IQ', 'árabe [Iraq]', 'árabe [Iraq]', 0, 0),
(11, 'ar', 'ar_JO', 'árabe [Jordania]', 'árabe [Jordania]', 0, 0),
(12, 'ar', 'ar_KW', 'árabe [Kuwait]', 'árabe [Kuwait]', 0, 0),
(13, 'ar', 'ar_LB', 'árabe [Líbano]', 'árabe [Líbano]', 0, 0),
(14, 'ar', 'ar_LY', 'árabe [Libia]', 'árabe [Libia]', 0, 0),
(15, 'ar', 'ar_MA', 'árabe [Marruecos]', 'árabe [Marruecos]', 0, 0),
(16, 'ar', 'ar_OM', 'árabe [Omán]', 'árabe [Omán]', 0, 0),
(17, 'ar', 'ar_QA', 'árabe [Qatar]', 'árabe [Qatar]', 0, 0),
(18, 'ar', 'ar_SA', 'árabe [Arabia Saudita]', 'árabe [Arabia Saudita]', 0, 0),
(19, 'ar', 'ar_SD', 'árabe [Sudán]', 'árabe [Sudán]', 0, 0),
(20, 'ar', 'ar_SY', 'árabe [Siria]', 'árabe [Siria]', 0, 0),
(21, 'ar', 'ar_TN', 'árabe [Túnez]', 'árabe [Túnez]', 0, 0),
(22, 'ar', 'ar_YE', 'árabe [Yemen]', 'árabe [Yemen]', 0, 0),
(23, 'be', 'be', 'bielorruso', 'bielorruso', 0, 0),
(24, 'be', 'be_BY', 'bielorruso [Bielorrusia]', 'bielorruso [Bielorrusia]', 0, 0),
(25, 'bg', 'bg', 'búlgaro', 'búlgaro', 0, 0),
(26, 'bg', 'bg_BG', 'búlgaro [Bulgaria]', 'búlgaro [Bulgaria]', 0, 0),
(27, 'ca', 'ca', 'catalán', 'catalán', 0, 0),
(28, 'ca', 'ca_ES', 'catalán [España]', 'catalán [España]', 0, 0),
(29, 'cs', 'cs', 'checo', 'checo', 0, 0),
(30, 'cs', 'cs_CZ', 'checo [Chequia]', 'checo [Chequia]', 0, 0),
(31, 'da', 'da', 'danés', 'danés', 0, 0),
(32, 'da', 'da_DK', 'danés [Dinamarca]', 'danés [Dinamarca]', 0, 0),
(33, 'de', 'de', 'alemán', 'alemán', 0, 0),
(34, 'de', 'de_AT', 'alemán [Austria]', 'alemán [Austria]', 0, 0),
(35, 'de', 'de_CH', 'alemán [Suiza]', 'alemán [Suiza]', 0, 0),
(36, 'de', 'de_DE', 'alemán [Alemania]', 'alemán [Alemania]', 0, 0),
(37, 'de', 'de_LU', 'alemán [Luxemburgo]', 'alemán [Luxemburgo]', 0, 0),
(38, 'el', 'el', 'griego', 'griego', 0, 0),
(39, 'el', 'el_CY', 'griego [Chipre]', 'griego [Chipre]', 0, 0),
(40, 'el', 'el_GR', 'griego [Grecia]', 'griego [Grecia]', 0, 0),
(41, 'en', 'en', 'inglés', 'inglés', 0, 0),
(42, 'en', 'en_AU', 'inglés [Australia]', 'inglés [Australia]', 0, 0),
(43, 'en', 'en_CA', 'inglés [Canadá]', 'inglés [Canadá]', 0, 0),
(44, 'en', 'en_GB', 'inglés [Reino Unido]', 'inglés [Reino Unido]', 0, 0),
(45, 'en', 'en_IE', 'inglés [Irlanda]', 'inglés [Irlanda]', 0, 0),
(46, 'en', 'en_IN', 'inglés [India]', 'inglés [India]', 0, 0),
(47, 'en', 'en_MT', 'inglés [Malta]', 'inglés [Malta]', 0, 0),
(48, 'en', 'en_NZ', 'inglés [Nueva Zelanda]', 'inglés [Nueva Zelanda]', 0, 0),
(49, 'en', 'en_PH', 'inglés [Filipinas]', 'inglés [Filipinas]', 0, 0),
(50, 'en', 'en_SG', 'inglés [Singapur]', 'inglés [Singapur]', 0, 0),
(51, 'en', 'en_ZA', 'inglés [Sudáfrica]', 'inglés [Sudáfrica]', 0, 0),
(52, 'es', 'es', 'español', 'español', 0, 0),
(53, 'es', 'es_AR', 'español [Argentina]', 'español [Argentina]', 0, 0),
(54, 'es', 'es_BO', 'español [Bolivia]', 'español [Bolivia]', 0, 0),
(55, 'es', 'es_CL', 'español [Chile]', 'español [Chile]', 0, 0),
(56, 'es', 'es_CO', 'español [Colombia]', 'español [Colombia]', 0, 0),
(57, 'es', 'es_CR', 'español [Costa Rica]', 'español [Costa Rica]', 0, 0),
(58, 'es', 'es_DO', 'español [República Dominicana]', 'español [República Dominicana]', 0, 0),
(59, 'es', 'es_EC', 'español [Ecuador]', 'español [Ecuador]', 0, 0),
(60, 'es', 'es_GT', 'español [Guatemala]', 'español [Guatemala]', 0, 0),
(61, 'es', 'es_HN', 'español [Honduras]', 'español [Honduras]', 0, 0),
(62, 'es', 'es_MX', 'español [México]', 'español [México]', 0, 0),
(63, 'es', 'es_NI', 'español [Nicaragua]', 'español [Nicaragua]', 0, 0),
(64, 'es', 'es_PA', 'español [Panamá]', 'español [Panamá]', 0, 0),
(65, 'es', 'es_PE', 'español [Perú]', 'español [Perú]', 0, 0),
(66, 'es', 'es_PR', 'español [Puerto Rico]', 'español [Puerto Rico]', 0, 0),
(67, 'es', 'es_PY', 'español [Paraguay]', 'español [Paraguay]', 0, 0),
(68, 'es', 'es_SV', 'español [El Salvador]', 'español [El Salvador]', 0, 0),
(69, 'es', 'es_US', 'español [Estados Unidos]', 'español [Estados Unidos]', 0, 0),
(70, 'es', 'es_UY', 'español [Uruguay]', 'español [Uruguay]', 0, 0),
(71, 'es', 'es_VE', 'español [Venezuela]', 'español [Venezuela]', 0, 0),
(72, 'et', 'et', 'estonio', 'estonio', 0, 0),
(73, 'et', 'et_EE', 'estonio [Estonia]', 'estonio [Estonia]', 0, 0),
(74, 'fi', 'fi', 'finés', 'finés', 0, 0),
(75, 'fi', 'fi_FI', 'finés [Finlandia]', 'finés [Finlandia]', 0, 0),
(76, 'fr', 'fr', 'francés', 'francés', 0, 0),
(77, 'fr', 'fr_BE', 'francés [Bélgica]', 'francés [Bélgica]', 0, 0),
(78, 'fr', 'fr_CA', 'francés [Canadá]', 'francés [Canadá]', 0, 0),
(79, 'fr', 'fr_CH', 'francés [Suiza]', 'francés [Suiza]', 0, 0),
(80, 'fr', 'fr_LU', 'francés [Luxemburgo]', 'francés [Luxemburgo]', 0, 0),
(81, 'ga', 'ga', 'irlandés', 'irlandés', 0, 0),
(82, 'ga', 'ga_IE', 'irlandés [Irlanda]', 'irlandés [Irlanda]', 0, 0),
(83, 'hi', 'hi_IN', 'hindú [India]', 'hindú [India]', 0, 0),
(84, 'hr', 'hr', 'croata', 'croata', 0, 0),
(85, 'hr', 'hr_HR', 'croata [Croacia]', 'croata [Croacia]', 0, 0),
(86, 'hu', 'hu', 'húngaro', 'húngaro', 0, 0),
(87, 'hu', 'hu_HU', 'húngaro [Hungría]', 'húngaro [Hungría]', 0, 0),
(88, 'in', 'in', 'indonesio', 'indonesio', 0, 0),
(89, 'in', 'in_ID', 'indonesio [Indonesia]', 'indonesio [Indonesia]', 0, 0),
(90, 'is', 'is', 'islandés', 'islandés', 0, 0),
(91, 'is', 'is_IS', 'islandés [Islandia]', 'islandés [Islandia]', 0, 0),
(92, 'it', 'it', 'italiano', 'italiano', 0, 0),
(93, 'it', 'it_CH', 'italiano [Suiza]', 'italiano [Suiza]', 0, 0),
(94, 'it', 'it_IT', 'italiano [Italia]', 'italiano [Italia]', 0, 0),
(95, 'iw', 'iw', 'hebreo', 'hebreo', 0, 0),
(96, 'iw', 'iw_IL', 'hebreo [Israel]', 'hebreo [Israel]', 0, 0),
(97, 'ja', 'ja', 'japonés', 'japonés', 0, 0),
(98, 'ja', 'ja_JP', 'japonés [Japón]', 'japonés [Japón]', 0, 0),
(99, 'ko', 'ko', 'coreano', 'coreano', 0, 0),
(100, 'ko', 'ko_KR', 'coreano [Corea del Sur]', 'coreano [Corea del Sur]', 0, 0),
(101, 'lt', 'lt', 'lituano', 'lituano', 0, 0),
(102, 'lt', 'lt_LT', 'lituano [Lituania]', 'lituano [Lituania]', 0, 0),
(103, 'lv', 'lv', 'letón', 'letón', 0, 0),
(104, 'lv', 'lv_LV', 'letón [Letonia]', 'letón [Letonia]', 0, 0),
(105, 'mk', 'mk', 'macedonio', 'macedonio', 0, 0),
(106, 'mk', 'mk_MK', 'macedonio [Macedonia]', 'macedonio [Macedonia]', 0, 0),
(107, 'ms', 'ms', 'malayo', 'malayo', 0, 0),
(108, 'ms', 'ms_MY', 'malayo [Malasia]', 'malayo [Malasia]', 0, 0),
(109, 'mt', 'mt', 'maltés', 'maltés', 0, 0),
(110, 'mt', 'mt_MT', 'maltés [Malta]', 'maltés [Malta]', 0, 0),
(111, 'nl', 'nl', 'neerlandés', 'neerlandés', 0, 0),
(112, 'nl', 'nl_BE', 'neerlandés [Bélgica]', 'neerlandés [Bélgica]', 0, 0),
(113, 'nl', 'nl_NL', 'neerlandés [Holanda]', 'neerlandés [Holanda]', 0, 0),
(114, 'no', 'no', 'noruego', 'noruego', 0, 0),
(115, 'no', 'no_NO', 'noruego [Noruega]', 'noruego [Noruega]', 0, 0),
(116, 'pl', 'pl', 'polaco', 'polaco', 0, 0),
(117, 'pl', 'pl_PL', 'polaco [Polonia]', 'polaco [Polonia]', 0, 0),
(118, 'pt', 'pt', 'portugués', 'portugués', 0, 0),
(119, 'pt', 'pt_BR', 'portugués [Brasil]', 'portugués [Brasil]', 0, 0),
(120, 'pt', 'pt_PT', 'portugués [Portugal]', 'portugués [Portugal]', 0, 0),
(121, 'ro', 'ro', 'rumano', 'rumano', 0, 0),
(122, 'ro', 'ro_RO', 'rumano [Rumania]', 'rumano [Rumania]', 0, 0),
(123, 'ru', 'ru', 'ruso', 'ruso', 0, 0),
(124, 'ru', 'ru_RU', 'ruso [Rusia]', 'ruso [Rusia]', 0, 0),
(125, 'sk', 'sk', 'eslovaco', 'eslovaco', 0, 0),
(126, 'sk', 'sk_SK', 'eslovaco [Eslovaquia]', 'eslovaco [Eslovaquia]', 0, 0),
(127, 'sl', 'sl', 'eslovenio', 'eslovenio', 0, 0),
(128, 'sl', 'sl_SI', 'eslovenio [Eslovenia]', 'eslovenio [Eslovenia]', 0, 0),
(129, 'sq', 'sq', 'albanés', 'albanés', 0, 0),
(130, 'sq', 'sq_AL', 'albanés [Albania]', 'albanés [Albania]', 0, 0),
(131, 'sr', 'sr', 'serbio', 'serbio', 0, 0),
(132, 'sr', 'sr_BA', 'serbio [Bosnia y Hercegovina]', 'serbio [Bosnia y Hercegovina]', 0, 0),
(133, 'sr', 'sr_CS', 'serbio [Serbia y Montenegro]', 'serbio [Serbia y Montenegro]', 0, 0),
(134, 'sr', 'sr_ME', 'serbio [Montenegro]', 'serbio [Montenegro]', 0, 0),
(135, 'sr', 'sr_RS', 'serbio [Serbia]', 'serbio [Serbia]', 0, 0),
(136, 'sv', 'sv', 'sueco', 'sueco', 0, 0),
(137, 'sv', 'sv_SE', 'sueco [Suecia]', 'sueco [Suecia]', 0, 0),
(138, 'th', 'th', 'tailandés', 'tailandés', 0, 0),
(139, 'th', 'th_TH', 'tailandés [Tailandia]', 'tailandés [Tailandia]', 0, 0),
(140, 'tr', 'tr', 'turco', 'turco', 0, 0),
(141, 'tr', 'tr_TR', 'turco [Turquía]', 'turco [Turquía]', 0, 0),
(142, 'uk', 'uk', 'ucranio', 'ucranio', 0, 0),
(143, 'uk', 'uk_UA', 'ucranio [Ucrania]', 'ucranio [Ucrania]', 0, 0),
(144, 'vi', 'vi', 'vietnamita', 'vietnamita', 0, 0),
(145, 'vi', 'vi_VN', 'vietnamita [Vietnam]', 'vietnamita [Vietnam]', 0, 0),
(146, 'zh', 'zh', 'chino', 'chino', 0, 0),
(147, 'zh', 'zh_CN', 'chino [China]', 'chino [China]', 0, 0),
(148, 'zh', 'zh_HK', 'chino [Hong Kong]', 'chino [Hong Kong]', 0, 0),
(149, 'zh', 'zh_TW', 'chino [Taiwán]', 'chino [Taiwán]', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idiomas__claves`
--

DROP TABLE IF EXISTS `idiomas__claves`;
CREATE TABLE IF NOT EXISTS `idiomas__claves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_seccion` int(11) NOT NULL COMMENT 'sección',
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre identificativo según la sección',
  `type` varchar(20) COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'text',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`id_seccion`,`nombre`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=214 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `idiomas__claves`
--

INSERT INTO `idiomas__claves` (`id`, `id_seccion`, `nombre`, `type`) VALUES
(1, 1, 'texto', 'ckeditor'),
(2, 1, 'nombre', 'text'),
(3, 1, 'titulo', 'text'),
(4, 1, 'subtitulo', 'text'),
(5, 1, 'meta_titulo', 'text'),
(6, 1, 'meta_descripcion', 'text'),
(7, 2, 'texto', 'ckeditor'),
(8, 2, 'nombre', 'text'),
(9, 2, 'titulo', 'text'),
(11, 2, 'meta_titulo', 'text'),
(12, 2, 'meta_descripcion', 'text'),
(13, 5, '', 'ckeditor'),
(14, 5, 'nombre', 'text'),
(15, 5, 'titulo', 'text'),
(16, 5, 'subtitulo', 'text'),
(17, 5, 'meta_titulo', 'text'),
(18, 5, 'meta_descripcion', 'text'),
(19, 6, 'url', 'text'),
(20, 6, 'nombre', 'text'),
(21, 6, 'titulo', 'text'),
(22, 6, 'subtitulo', 'text'),
(23, 6, 'meta_titulo', 'text'),
(24, 6, 'meta_descripcion', 'text'),
(25, 9, 'url', 'text'),
(26, 9, 'nombre', 'text'),
(27, 9, 'titulo', 'text'),
(28, 9, 'subtitulo', 'text'),
(29, 9, 'meta_titulo', 'text'),
(30, 9, 'meta_descripcion', 'text'),
(32, 10, 'nombre', 'text'),
(33, 10, 'titulo', 'text'),
(34, 10, 'subtitulo', 'text'),
(35, 10, 'meta_titulo', 'text'),
(36, 10, 'meta_descripcion', 'text'),
(45, 9, 'bt-home', 'text'),
(46, 9, 'bt-objetivos', 'text'),
(47, 9, 'bt-organizacion', 'text'),
(48, 9, 'bt-historia', 'text'),
(49, 9, 'bt-contacto', 'text'),
(50, 9, 'bt-miembros', 'text'),
(51, 9, 'bt-actividades', 'text'),
(52, 9, 'bt-donacion', 'text'),
(53, 9, 'bt-noticias', 'text'),
(54, 9, 'bt-enlaces', 'text'),
(55, 9, 'bt-aportaciones', 'text'),
(56, 9, 'bt-aviso', 'text'),
(57, 9, 'bt-politica', 'text'),
(58, 9, 'bt-cookies', 'text'),
(59, 9, 'bt-asociacion', 'text'),
(60, 9, 'bt-asociacion-objetivos', 'text'),
(61, 9, 'bt-asociacion-organizacion', 'text'),
(62, 9, 'bt-asociacion-localizacion', 'text'),
(63, 9, 'bt-asociacion-estatutos', 'text'),
(64, 9, 'bt-asociarse', 'text'),
(66, 11, 'nombre-bt', 'text'),
(67, 11, 'objetivo', 'text'),
(68, 11, 'texto-intro', 'text'),
(69, 11, 'meta_titulo', 'text'),
(70, 11, 'meta_descripcion', 'text'),
(71, 9, 'objetivo-actual', 'text'),
(72, 9, 'quiero-donar-bt', 'text'),
(73, 1, 'numero-amigos', 'number'),
(74, 1, 'leyenda-ventajas', 'ckeditor'),
(75, 10, 'texto', 'ckeditor'),
(77, 9, 'noticiasmarcador', 'text'),
(78, 9, 'actividadesmarcador', 'text'),
(79, 2, 'subtitulo', 'text'),
(80, 12, 'texto', 'ckeditor'),
(81, 12, 'nombre', 'text'),
(82, 12, 'titulo', 'text'),
(83, 12, 'subtitulo', 'text'),
(84, 12, 'meta_titulo', 'text'),
(85, 12, 'meta_descripcion', 'text'),
(86, 9, 'ver_mas', 'text'),
(87, 13, 'texto', 'ckeditor'),
(88, 13, 'nombre', 'text'),
(89, 13, 'titulo', 'text'),
(90, 13, 'subtitulo', 'text'),
(91, 13, 'meta_titulo', 'text'),
(92, 13, 'meta_descripcion', 'text'),
(93, 14, 'url', 'text'),
(94, 14, 'nombre', 'text'),
(95, 14, 'titulo', 'text'),
(96, 14, 'subtitulo', 'text'),
(97, 14, 'meta_titulo', 'text'),
(98, 14, 'meta_descripcion', 'text'),
(100, 14, 'texto', 'ckeditor'),
(101, 15, 'url', 'text'),
(102, 15, 'nombre', 'text'),
(103, 15, 'titulo', 'text'),
(104, 15, 'subtitulo', 'text'),
(105, 15, 'meta_titulo', 'text'),
(106, 15, 'meta_descripcion', 'text'),
(107, 15, 'texto', 'ckeditor'),
(108, 16, 'url', 'text'),
(109, 16, 'nombre', 'text'),
(110, 16, 'titulo', 'text'),
(111, 16, 'subtitulo', 'text'),
(112, 16, 'meta_titulo', 'text'),
(113, 16, 'meta_descripcion', 'text'),
(114, 16, 'texto', 'ckeditor'),
(115, 17, 'url', 'text'),
(116, 17, 'nombre', 'text'),
(117, 17, 'titulo', 'text'),
(118, 17, 'subtitulo', 'text'),
(119, 17, 'meta_titulo', 'text'),
(120, 17, 'meta_descripcion', 'text'),
(121, 17, 'texto', 'ckeditor'),
(122, 5, 'texto', 'ckeditor'),
(123, 18, 'url', 'text'),
(124, 18, 'nombre', 'text'),
(125, 18, 'titulo', 'text'),
(126, 18, 'subtitulo', 'text'),
(127, 18, 'meta_titulo', 'text'),
(128, 18, 'meta_descripcion', 'text'),
(129, 18, 'texto', 'ckeditor'),
(142, 21, 'url', 'text'),
(143, 21, 'nombre', 'text'),
(144, 21, 'titulo', 'text'),
(145, 21, 'subtitulo', 'text'),
(146, 21, 'meta_titulo', 'text'),
(147, 21, 'meta_descripcion', 'text'),
(178, 21, 'texto', 'ckeditor'),
(179, 27, 'url', 'text'),
(180, 27, 'nombre', 'text'),
(181, 27, 'titulo', 'text'),
(182, 27, 'subtitulo', 'text'),
(183, 27, 'meta_titulo', 'text'),
(184, 27, 'meta_descripcion', 'text'),
(185, 27, 'texto', 'ckeditor'),
(186, 28, 'url', 'text'),
(187, 28, 'nombre', 'text'),
(188, 28, 'titulo', 'text'),
(189, 28, 'subtitulo', 'text'),
(190, 28, 'meta_titulo', 'text'),
(191, 28, 'meta_descripcion', 'text'),
(192, 28, 'texto', 'ckeditor'),
(193, 29, 'url', 'text'),
(194, 29, 'nombre', 'text'),
(195, 29, 'titulo', 'text'),
(196, 29, 'subtitulo', 'text'),
(197, 29, 'meta_titulo', 'text'),
(198, 29, 'meta_descripcion', 'text'),
(199, 29, 'texto', 'ckeditor'),
(200, 30, 'url', 'text'),
(201, 30, 'nombre', 'text'),
(202, 30, 'titulo', 'text'),
(203, 30, 'subtitulo', 'text'),
(204, 30, 'meta_titulo', 'text'),
(205, 30, 'meta_descripcion', 'text'),
(206, 30, 'texto', 'ckeditor'),
(207, 11, 'titulo', 'text'),
(208, 11, 'subtitulo', 'text'),
(209, 11, 'texto', 'ckeditor'),
(210, 11, 'titulo-formulario-donacion', 'text'),
(211, 11, 'texto-formulario-donacion', 'ckeditor'),
(212, 11, 'info-donacion', 'ckeditor'),
(213, 16, 'texto-aviso', 'ckeditor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idiomas__secciones`
--

DROP TABLE IF EXISTS `idiomas__secciones`;
CREATE TABLE IF NOT EXISTS `idiomas__secciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(200) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `logo` varchar(200) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `position` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `oculto` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `idiomas__secciones`
--

INSERT INTO `idiomas__secciones` (`id`, `slug`, `nombre`, `logo`, `position`, `parent_id`, `oculto`) VALUES
(1, 'home', 'Home', '', -1, 0, 0),
(2, 'asociacion', 'Asociacion', '', -2, 0, 1),
(5, 'noticias', 'Noticias', '', -4, 0, 1),
(6, 'contacto', 'Contacto', '', -5, 0, 0),
(9, 'comunes', 'Comunes', '', -6, 0, 1),
(10, 'historia', 'Historia', 'star', -3, 0, 1),
(11, 'mecenazgo', 'Mecenazgo', '', 0, 0, 0),
(12, 'objetivos', 'Objetivos', '', 0, 0, 1),
(13, 'miembros', 'Miembros', '', 0, 0, 1),
(14, 'organizacion', 'Organizacion', '', 0, 0, 0),
(15, 'localizacion', 'Localizacion', '', 0, 0, 0),
(16, 'asociarse', 'Asociarse', '', 0, 0, 1),
(17, 'estatutos', 'Estatutos', '', 0, 0, 0),
(18, 'actividades', 'Actividades', '', 0, 0, 1),
(21, 'donacion-y-restauracion', 'DonacionYRestauracion', '', 0, 0, 0),
(27, 'enlaces', 'Enlaces', '', 0, 0, 0),
(28, 'aviso-legal', 'AvisoLegal', '', 0, 0, 1),
(29, 'politica-de-privacidad', 'PoliticaDePrivacidad', '', 0, 0, 1),
(30, 'cookies', 'Cookies', '', 0, 0, 1);

--
-- Disparadores `idiomas__secciones`
--
DROP TRIGGER IF EXISTS `insertar_claves_comunes`;
DELIMITER $$
CREATE TRIGGER `insertar_claves_comunes` AFTER INSERT ON `idiomas__secciones` FOR EACH ROW INSERT INTO idiomas__claves (`id_seccion`, `nombre`)
VALUES
		(NEW.id, 'url'),
		(NEW.id, 'nombre'),
		(NEW.id, 'titulo'),
		(NEW.id, 'subtitulo'),
		(NEW.id, 'meta_titulo'),
		(NEW.id, 'meta_descripcion')
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idiomas__textos`
--

DROP TABLE IF EXISTS `idiomas__textos`;
CREATE TABLE IF NOT EXISTS `idiomas__textos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_clave` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `valor` text COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_clave` (`id_clave`,`id_idioma`),
  KEY `FK_IDIOMAS` (`id_idioma`)
) ENGINE=InnoDB AUTO_INCREMENT=189 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `idiomas__textos`
--

INSERT INTO `idiomas__textos` (`id`, `id_clave`, `id_idioma`, `valor`) VALUES
(1, 1, 2, '<p>Desde la Asociación de Amigos del Museo Nacional de Escultura queremos darte la bienvenida a este espacio de intercambio cultural.</p>\r\n<h3><strong>Considerate en tu casa</strong></h3>\r\n<p>Esta web, junto a nuestra asociación, busca acercar el <a href=\"http://www.mecd.gob.es/mnescultura/inicio.html\" target=\"_blank\">Museo Nacional de Escultura</a> al visitante no especializado, hacérle partícipe de nuestras actividades, exposiciones y eventos y contribuir a ampliar la oferta cultural de la ciudad de Valladolid.</p>'),
(2, 2, 2, 'Incio'),
(3, 18, 1, 'Here meta tag'),
(4, 18, 2, 'Esta es la meta'),
(5, 18, 3, 'CHINO'),
(6, 18, 4, 'Çe est le meta'),
(7, 3, 1, 'Hey!'),
(8, 3, 2, '¡Bienvenido!'),
(9, 3, 3, 'CHINO'),
(10, 3, 4, 'Salut!'),
(11, 2, 1, 'Home'),
(12, 2, 3, 'CHINO'),
(13, 2, 4, 'SALUT'),
(14, 36, 1, 'Meta in inglés'),
(15, 36, 2, 'Meta en español'),
(16, 36, 3, 'CHINO'),
(17, 36, 4, 'Le Meta en Français'),
(18, 5, 1, 'iuhnuh'),
(19, 5, 2, 'trct'),
(28, 45, 1, 'Home'),
(29, 45, 2, 'Inicio'),
(30, 46, 1, 'Objetivos en'),
(31, 46, 2, 'Objetivos'),
(32, 47, 1, 'organización en'),
(33, 47, 2, 'Organización'),
(34, 48, 1, 'History'),
(35, 48, 2, 'Historia'),
(36, 49, 1, 'Contact'),
(37, 49, 2, 'Contacto'),
(38, 26, 1, ''),
(39, 26, 2, 'Hola Amigos del museo'),
(40, 50, 1, 'Members'),
(41, 50, 2, 'Miembros'),
(42, 51, 1, 'Activities'),
(43, 51, 2, 'Actividades'),
(44, 52, 1, 'Donacion en'),
(45, 52, 2, 'Donación y restauración'),
(46, 53, 1, 'News'),
(47, 53, 2, 'Noticias'),
(48, 54, 1, 'Links'),
(49, 54, 2, 'Enlaces'),
(50, 55, 1, 'Aportaciones en'),
(51, 55, 2, 'Aportaciones'),
(52, 56, 1, 'Legal advice'),
(53, 56, 2, 'Aviso legal'),
(54, 57, 1, 'Privace policy'),
(55, 57, 2, 'Política de privacidad'),
(56, 58, 1, 'Cookies'),
(57, 58, 2, 'Política de cookies'),
(58, 59, 1, 'Asociacion en'),
(59, 59, 2, 'Asociación'),
(60, 60, 1, 'Objetivos en'),
(61, 60, 2, 'Objetivos'),
(62, 61, 1, 'organización en'),
(63, 61, 2, 'Organización'),
(64, 62, 1, 'Location'),
(65, 62, 2, 'Localización'),
(66, 63, 1, 'Estatutos en'),
(67, 63, 2, 'Estatutos'),
(68, 64, 1, 'Asociarse en'),
(69, 64, 2, 'Asociarse'),
(70, 66, 1, 'Micromecenazgo en'),
(71, 66, 2, 'Micromecenazgo'),
(72, 68, 1, 'dsfsdfsdfsdfsdfegrdfgesfdged'),
(73, 68, 2, 'La Asociación Amigos del Museo de Escultura contempla un plan de micromecenazgo para que cualquier particular o empresa participe'),
(74, 67, 1, 'Restauration'),
(75, 67, 2, 'Restauración del Retablo de la Virgen lorem ipsum (1612)'),
(76, 71, 1, 'current target'),
(77, 71, 2, 'objetivo actual'),
(78, 72, 1, 'I want to donate'),
(79, 72, 2, 'quiero donar'),
(80, 73, 1, '200'),
(81, 73, 2, '348'),
(82, 74, 1, '<p>dfasdfqwdsfc</p>'),
(83, 74, 2, '<p>Ser Amigo del Museo Nacional de Escultura está lleno de <a href=\"#/miembros\" target=\"_self\">ventajas</a></p>'),
(84, 33, 1, 'History of the museum'),
(85, 33, 2, 'Historia del Museo'),
(86, 34, 1, 'Breve... en'),
(87, 34, 2, 'Breve historia de nuestro museo'),
(88, 75, 1, '<p>ds</p>'),
(89, 75, 2, '<h3>El mueso nacional colegio nacional de escultura</h3>\r\n<p>La creación del <a href=\"http://www.mecd.gob.es/mnescultura/inicio.html\" target=\"_blank\"><strong>Museo Nacional de Escultura</strong></a> se remonta al año 1933. En la primera sede elegida para su ubicación, el Colegio San Gregorio y la capilla funeraria de su fundador, Fray Alonso de Burgos, se logró reunir e instalar un importante conjunto de esculturas y pinturas que, procedentes del antiguo <strong>Museo Provincial de Bellas Artes</strong>, constituyeron su núcleo inicial.</p>\r\n<p>En las siete decadas transcurridas desde entonces, han sido mucho los cambios que la institución ha debido asumir. El importante crecimiento de sus colecciones, unas mayores exigencias en el cumplimiento de las funciones de conservación e investigación, un fuerte incremento de su actividad cultural, la necesidad de nuevos servicios de atención al público... han obligado a ampliar su superficie con edificios adyacentes (Palacio de Villena, Casa del Sol...) y a abordar un programa de renovación de infraestructuras y mejora de instalaciones museográficas que actualmente está siendo aplicado a su sede principal, el Colegio San Gregorio.</p>\r\n<p><img alt=\"\" src=\"http://imac.local:5757/files/ckeditor/3cfda62.jpg\" style=\"height:753px; width:500px\" /></p>\r\n<blockquote>\r\n<p>Imagen de la fachada</p></blockquote>\r\n<h3>La colección</h3>\r\n<p>La colección del Museo Nacional de Escultura está integrada por <strong>un extraordinario conjunto de escultura y pintura de los siglos XIII al XIX</strong> que permite apreciar la <strong>evolución formal de la plástica hispana a lo largo de los siglos Gótico, Renacimiento, Manierismo, Barroco y Rococó</strong>.</p>\r\n<p>Los ricos fondos que la componen son una muestra elocuente de la calidad que alcanzaron las formas artísticas en nuestro país, expresión viva de un complejo entramado de relaciones internacionales, de circulación de influencias y artistas geniales que dieron como resultado un magnífico conjunto patrimonial.</p>\r\n<p>El núcleo primitivo de esta colección procede de los conventos desamortizados de Valladolid y su entorno. La transcendencia histórica de la ciudad en épocas pasadas, está en el origen de la importancia de las creaciones artísticas atesoradas por esta vía, producto de una larga tradición de mecenazgo y protección de las artes. A estas aportaciones hay que añadir <strong>donaciones, depósitos </strong>y, sobre todo, <strong>adquisiciones del Estado</strong> orientadas a completar una visión global de la escultura española con todos sus matices, dentro de unos límites geográficos que exceden los peninsulares.</p>\r\n<p><img alt=\"\" src=\"http://imac.local:5757/files/ckeditor/4511583.jpg\" style=\"height:333px; width:500px\" /></p>\r\n<p>Retablos, sillerías, monumentos funerarios, pasos procesionales... son algunas de las tipologías a las que pertenecían en origen este importante conjunto de obras. Los variados materiales en los que fueron realizadas (barro, piedra, bronce y, sobre todo, madera policromada) se nos presentan hoy como privilegiados vehículos de las ideas, sentimientos y emociones de la sociedad en la que se gestaron.</p>\r\n<p>Las peculiaridades de la escultura en cuanto a tipologías (retablos, sillerías corales o escultura funeraria) y también en lo que se refiere a materiales (madera, bronce, piedra, marmol o marfil), alcanzan en España uno de sus puntos álgidos con el uso de la madera policromada, una auténtica síntesis de las artes, de volumen y color, de esfuerzo por lograr mayor verismo y transmitir auténticas sensaciones.</p>\r\n<p>La secuencia cronológica permite conocer lo que supuso en cada periodo, la evolución de los géneros artísticos como reflejo de la sociedad en la que se creó. Importación, comercio, talleres locales y corrientes estílisticas dieron lugar a formas singulares llenas de interés.</p>'),
(92, 77, 1, 'news'),
(93, 77, 2, 'noticias'),
(94, 78, 1, 'actividadaes'),
(95, 78, 2, 'actividades'),
(96, 9, 1, ''),
(97, 9, 2, 'Asociación de Amigos del Museo Nacional de Escultura'),
(98, 79, 1, ''),
(99, 79, 2, 'Acercando el museo a la gente'),
(100, 7, 1, ''),
(101, 7, 2, '<p><strong>El MUSEO NACIONAL DE ESCULTURA</strong> representa una de las principales instituciones culturales de nuestro país y, atendiendo a la importancia de sus colecciones, una de las más relevantes a nivel mundial.</p>\r\n<p>La Asociación tiene como finalidad principal el apoyo al Museo bien entendido que la palabra “apoyo” no presenta, en este caso, ningún signo de pasividad ya que lo que <strong>logramos</strong> con nuestra actividad es <strong>llevar la voz de la persona no especializada</strong>, de la sociedad <strong>y hacerla oír en los museos</strong>.</p>\r\n<p>A lo largo de los años, el trabajo esforzado y las ideas de todos ellos han producido en los propios museos una transformación, acercándolos a la gente y acercando la gente a los museos.</p>\r\n<p>La Asociación no persigue otra finalidad que <strong>favorecer a sus socios el conocimiento preferente de todas las muestras culturales</strong> que pueda desarrollar o albergar el Museo, así como <strong>llevar a cabo su propio programa cultural </strong>que estará en sintonía con el del propio Museo, ya que es lo que constituye nuestra razón de ser.</p>\r\n<h3><strong>Te necesitamos</strong></h3>\r\n<p>Un proyecto de estas características necesita, evidentemente, del apoyo de la Sociedad a la que sirve. Por eso, al igual que otras instituciones de este género en todo el mundo, nos parece necesario invitar a particulares y empresas a participar en su desarrollo.</p>\r\n<p>A través de los Amigos del Museo nos gustaría que, todos los amantes del arte y la cultura, pudieran vincularse a nuestra tarea, lanzando con esta línea, un llamamiento a la sociedad para seguir en este empeño que sólo será posible con la colaboración y participación entusiasta de todos.</p>'),
(102, 80, 1, ''),
(103, 80, 2, '<p>Desde su inicio, la Asociación ha venido desarrollando, gracias a la colaboración y esfuerzo de todos los Amigos, una importante labor de consolidación y afianzamiento en la sociedad consiguiendo hacerse un hueco en la actividad cultural de nuestra ciudad.</p>\r\n<p>Así, afianzados, la Asociación tiene previsto potenciar su actividad con la finalidad de contribuir a la difusión de las colecciones del <a href=\"http://www.mecd.gob.es/mnescultura\" target=\"_blank\">Museo Nacional de Escultura</a>.&nbsp;</p>\r\n<h3><strong>Nuestros objetivos se centran</strong></h3>\r\n<p>Sus objetivos se centran, por un lado, en consolidar las líneas de trabajo que se han desarrollado hasta la fecha y, por otro, en introducir nuevas propuestas que contribuyan a la consecución de los fines de la misma.</p>\r\n<p>En la actualidad, el número de socios ha aumentado considerablemente, lo cual nos llena de satisfacción ya que siempre es muy agradable encontrar una buena respuesta de implicación de la sociedad en la conservación y promoción de nuestro patrimonio del cual es una muestra incomparable el Museo Nacional de Escultura.</p>\r\n<p>La Asociación no persigue otra finalidad que acercar a sus socios el conocimiento preferente de todas las muestras culturales que pueda generar o albergar el Museo, así como desarrollar su propio programa cultural que siempre estará en sintonía con el del Museo ya que ello es lo que constituye nuestra razón de ser.</p>\r\n<h3><strong>¿Quieres formar parte de la asociación?</strong></h3>\r\n<p>Ser parte de Amigos del Museo Nacional de Escultura está lleno de <a href=\"/#miembros/\" target=\"_self\">ventajas</a>.</p>\r\n<p>&nbsp;</p>'),
(104, 82, 1, 'Objetivos de la Asociación EN'),
(105, 82, 2, 'Objetivos de la Asociación'),
(106, 83, 1, 'Acercando el museo a la gente'),
(107, 83, 2, 'Acercando el museo a la gente'),
(108, 86, 1, ''),
(109, 86, 2, 'ver más'),
(110, 1, 1, ''),
(111, 89, 1, ''),
(112, 89, 2, 'Ventajas de ser socio'),
(113, 90, 1, 'fdter'),
(114, 90, 2, 'Ventajas exclusivas para nuestros miembros'),
(115, 87, 1, ''),
(116, 87, 2, '<p>Existen diferentes membresías para nuestra asociación.</p>\r\n<h3>Amigos</h3>\r\n<p><strong>Aportaciones</strong>:</p>\r\n<ul>\r\n	<li>A partir de 40€/año</li>\r\n	<li>A partir de 18€/año para menores de 25 años</li>\r\n</ul>\r\n<p><strong>Beneficios:</strong></p>\r\n<ol>\r\n	<li>Entrada gratuita al Museo con un acompañante</li>\r\n	<li>Entrada gratuita a otros museos estatales</li>\r\n	<li>Invitación y visitas guuiadas exclusivas para los Amigos</li>\r\n	<li>Invitación a las conferencias del Museo</li>\r\n	<li>Información sobre actividades del Museo y de la asociación</li>\r\n	<li>Descuento del 25% en publicaciones editadas por la Asociación</li>\r\n	<li>Descuento del 10% en artículos de la tienda y otras publicaciones.</li>\r\n	<li>Descuento en actividades que requieran pago</li>\r\n	<li>Desgravación fiscal</li>\r\n</ol>\r\n<h3><strong>Amigos protectores</strong></h3>\r\n<p><strong>Aportaciones</strong></p>\r\n<p>A partir de 300€/año</p>\r\n<p><strong>Beneficios (extra sobre los amigos)</strong></p>\r\n<ol>\r\n	<li>Catálogos de la Asociación gratuitos</li>\r\n	<li>Seis carnés de iguales beneficios</li>\r\n</ol>\r\n<h3><strong>Protectores empresariales o institucionales</strong></h3>\r\n<p><strong>Aportaciones</strong></p>\r\n<p>A partir de 900€/año</p>\r\n<p><strong>Beneficios</strong>&nbsp;</p>\r\n<ol>\r\n	<li>Carné personalizado a la persona designada</li>\r\n	<li>Diez carnés de amigo protector con iguales beneficios</li>\r\n	<li>Reconocimiento de esta colaboración en la memoria y difusión publicitarioa anual de la Asociación.</li>\r\n	<li>Prioridad de uso de espacios del Museo para celebrar recepciones especiales o visitas privadas, que deberán contar con la oportuna autorización del Ministerio de Cultura</li>\r\n</ol>\r\n<p>&nbsp;</p>'),
(117, 94, 1, 'Organization'),
(118, 94, 2, 'Organización'),
(119, 95, 1, 'Organization'),
(120, 95, 2, 'Organización'),
(121, 96, 1, 'Organización cultural sin ánimo de lucro'),
(122, 96, 2, 'Organización cultural sin ánimo de lucro'),
(123, 100, 1, ''),
(124, 100, 2, '<p>La Asociación se constituye como una organización cultural sin ánimo de lucro, de ámbito nacional en torno al <a href=\"http://www.mecd.gob.es/mnescultura/inicio.html\" target=\"_blank\">Museo Nacional de Escultura</a> de Valladolid.</p>\r\n<h3><strong>Estatutos y órganos de gobierno</strong></h3>\r\n<p><strong>Asamblea general</strong>: Órgano supremo de la Asociación que está integrada por todas las personas asociadas.</p>\r\n<p><strong>Junta directiva</strong>, actualmente compuesta por:</p>\r\n<ol>\r\n	<li><strong>Presidentea</strong>: D. Eduardo de Mata Trapote</li>\r\n	<li><strong>Vicepresidenta</strong>: Dña Mar Álvarez Rueda</li>\r\n	<li><strong>Secretario</strong>: Dña Ruth Domínguez Fernández</li>\r\n	<li><strong>Tesorero</strong>: D. Juan Carlos de Margarida Sanz</li>\r\n	<li><strong>Vocales</strong>: Dña Dolores Benavides Agundez, Dña Leonor Rodríguez Rodríguez, D. José Manuel San Miguel Monteira</li>\r\n</ol>'),
(125, 103, 1, 'Location'),
(126, 103, 2, 'Localización'),
(127, 104, 1, ''),
(128, 104, 2, ''),
(129, 107, 1, ''),
(130, 107, 2, '<p>La Asociación de Amigos del Museo Nacional de Escultura tiene su sede en el propio <a href=\"http://www.mecd.gob.es/mnescultura/inicio.html\" target=\"_blank\">Museo Nacional de Escultura</a>, sito en <strong>Calle Cadenas de San Gregorio, 1.2</strong></p>\r\n<p><strong><img alt=\"\" src=\"http://imac.local:5757/files/ckeditor/5745c86.jpg\" /></strong></p>\r\n<h3><strong>Horarios de atención al público</strong></h3>\r\n<p><strong>De martes a sábado</strong>: de 10h a 14h y de 16h a 19:30h</p>\r\n<p><strong>Domingos y festivos</strong>: de 10h a 14h</p>\r\n<p>Cerrado todos los lunes del año y los festivos:</p>\r\n<ul>\r\n	<li>1 y 6 de enero</li>\r\n	<li>1 de mayo</li>\r\n	<li>8 de septiembre</li>\r\n	<li>24, 25 y 31 de diciembre</li>\r\n</ul>'),
(131, 110, 1, 'Asociarse'),
(132, 110, 2, 'Asociarse'),
(133, 114, 1, ''),
(134, 114, 2, '<p>Si después de haber leído <a href=\"/#estatutos/\">nuestros estatutos</a> desea asociarse, le proponemos dos posibles formas:</p>\r\n<h3><strong>Presencial</strong></h3>\r\n<p>En el propio Museo, en el horario indicado puede hacer efectiva su inscripción.</p>\r\n<h3><strong>Online</strong></h3>\r\n<p>Si lo desea, puede rellenar el siguiente formulario de inscripción con el que procederemos a domiciliar el pago de su cuota. &nbsp;En ambos casos, en el menor tiempo posible, recibirá su carnet de Amigo con el que podrá disfrutar de <a href=\"/#miembros/\">todas las ventanjas</a> durante un año.</p>'),
(135, 117, 1, ''),
(136, 117, 2, 'Nuestros estatutos'),
(137, 121, 1, ''),
(138, 121, 2, '<h3><strong>OBJETO DE LA ASOCIACIÓN </strong></h3>\r\n<p><strong>Artículo 1.- </strong>Con la denominación de ASOCIACIÓN AMIGOS DEL MUSEO NACIONAL DE ESCULTURA, se constituye una entidad sin ánimo de lucro, con capacidad jurídica y plena capacidad de obrar, que se propone agrupar a la sociedad civil a través de todos los amantes y simpatizantes de nuestro Museo y de toda clase de manifestaciones artísticas.</p>\r\n<p>El Régimen de la entidad está constituido por los presentes estatutos y los acuerdos válidamente adoptados por su Asamblea General y órganos directivos, dentro de la esfera de su respectiva competencia. En lo no previsto se regirá por la Ley Orgánica 1/2002, de 22 de marzo, reguladora del derecho de asociación, y normas complementarias,</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 2.- </strong>Los fines de esta Asociación son los siguientes:</p>\r\n<p>a) Apoyar la gestión y labor del Museo Nacional de Escultura y de las Secciones y Colecciones a él vinculadas.</p>\r\n<p>b) Ofrecer su concurso moral y material para el mejor desenvolvimiento de la actividad del Museo y su proyección en la sociedad.</p>\r\n<p>c) Ofrecer el concurso de sus socios para que, gratuitamente, puedan prestar su colaboración en cuantas actividades programe el Museo.</p>\r\n<p>d) Apoyar toda iniciativa que tienda a velar por la conservación de la riqueza artística de España y procurar la relación constante con todos aquellos organismos similares, nacionales y extranjeros.</p>\r\n<p>&nbsp;</p>\r\n<p>Para el cumplimiento de estos fines la Asociación podrá realizar cuantas actividades, incluidas, en su caso, las económicas, conduzcan directa o indirectamente a su consecución, sin que, en ningún caso, la realización de actividades mercantiles pueda constituir su actividad principal.</p>\r\n<p>A titulo enunciativo y no limitativo, pueden señalarse las siguientes actividades:</p>\r\n<p>a) Educativas, para dar a conocer la historia, los edificios y las colecciones del Museo.</p>\r\n<p>b) Expositivas, para dar mayor difusión a los fondos del Museo, en sí mismos, o en relación con otras obras cuya exposición conjunta permita conocer y valorar mejor las del Museo.</p>\r\n<p>c) Editoriales, mediante la publicación, en cualquier soporte, de obras que tengan por objetos el estudio, la divulgación o el apoyo al Museo, previa la oportuna autorización, si se requiere, por parte del Ministerio de Cultura.</p>\r\n<p>Si se obtuvieren beneficios económicos derivados de las actividades, deberán ser destinados exclusivamente al cumplimiento de los fines de la Asociación, sin que quepa el reparto de los mismos entre los asociados, familiares de los mismos o relacionados, como tampoco la cesión gratuita a personas físicas o jurídicas que tengan ánimo de lucro.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 3.- </strong>Mientras la Asamblea General convocada específicamente con tal objeto, no acuerde lo contrario, el domicilio de la Asociación radicará en el Museo Nacional de Escultura, calle Cadenas de San Gregorio n.º 1-2, en Valladolid, en locales cedidos para este solo fin por la dirección del centro.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 4.- </strong>El ámbito territorial de acción de la Asociación se extiende a todo el territorio nacional y, en su caso, al extranjero.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 5.- </strong>La duración de la Asociación será indefinida y sólo se disolverá por acuerdo de la Asamblea General Extraordinaria o por cualquiera de las causas previstas en las leyes.</p>\r\n<p>&nbsp;</p>\r\n<h3><strong>GOBIERNO DE LA ASOCIACIÓN: ÓRGANOS DIRECTIVOS Y FORMAS DE ADMINISTRACIÓN</strong></h3>\r\n<p><strong>Artículo 6.- </strong>La dirección y administración de la Asociación será ejercida por el Presidente, la Junta Directiva y la Asamblea General de Socios.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 7.- </strong>La Junta Directiva estará formada por un Presidente, un Vicepresidente, un Secretario, un Vicesecretario, un Tesorero, un Vicetesorero y dos vocales, cargos que deberán recaer en socios activos y elegidos por votación.</p>\r\n<p>La Junta Directiva celebrará sesiones a iniciativa del presidente o por petición de la mitad de sus miembros, quedando constituida cuando asista la mitad de los integrantes de la misma.</p>\r\n<p>A criterio de la Junta Directiva, se establece la posibilidad de proponer a la Asamblea General el nombramiento de una Presidencia honorífica.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 8.- </strong>Los cargos que componen la Junta Directiva, serán gratuitos y se elegirán por la Asamblea General. La composición de dicha Junta Directiva se renovará al menos cada cuatro años. Los componentes de la Junta saliente podrán presentarse a la reelección si así lo desean.</p>\r\n<p>Los miembros de la Junta Directiva que hubieran agotado el plazo para el cual fueron elegidos, continuarán ostentando sus cargos hasta el momento en que se produzca la aceptación de los que les sustituyan.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 9.- </strong>Facultades de la Junta Directiva:</p>\r\n<p>1.- Las facultades de la Junta Directiva se extenderán, con carácter general, a todos los actos propios de las finalidades de la Asociación, siempre que no requieran, según estos Estatutos, autorización expresa de la Asamblea General.</p>\r\n<p>&nbsp;</p>\r\n<p>2.- Son facultades particulares de la Junta Directiva:</p>\r\n<p>a) Dirigir las actividades sociales y culturales, así como llevar la gestión económica y administrativa de la Asociación.</p>\r\n<p>b) Ejecutar los acuerdos de la Asamblea General.</p>\r\n<p>c) Formular y someter a la aprobación de la Asamblea General los Balances y las Cuentas anuales.</p>\r\n<p>d) Resolver sobre la admisión de nuevos asociados.</p>\r\n<p>e) Nombrar delegados para alguna determinada actividad de la Asociación.</p>\r\n<p>f) Cualquier otra facultad que no sea de exclusiva competencia de la Asamblea General de socios.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 10.- </strong>El Presidente de la Asociación tendrá las siguientes facultades:</p>\r\n<p>a) Representar legalmente a la Asociación ante toda clase de organismos públicos o privados.</p>\r\n<p>b) Convocar, presidir y levantar las sesiones que celebre la Asamblea General y la Junta Directiva.</p>\r\n<p>c) Dirigir las deliberaciones de una y otra.</p>\r\n<p>d) Ordenar pagos y autorizar con su firma los documentos, actas y correspondencia.</p>\r\n<p>e) Adoptar cualquier medida urgente que la buena marcha de la Asociación aconseje o en el desarrollo de sus actividades resulte necesaria o conveniente, sin perjuicio de dar cuenta posteriormente a la Junta Directiva.</p>\r\n<p>El Vicepresidente sustituirá al Presidente en ausencia de éste, motivada por enfermedad, vacante o cualquier otra causa, y tendrá las mismas atribuciones que él; teniendo las facultades que el presidente le delegue o establezca para él la Asamblea General.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 11.- </strong>El Secretario tendrá a su cargo:</p>\r\n<p>a) La dirección de los trabajos puramente administrativos de la Asociación.</p>\r\n<p>b) Expedir certificaciones.</p>\r\n<p>c) Levantar actas de las Asambleas y de las reuniones de la Junta Directiva.</p>\r\n<p>d) Llevar los libros de la Asociación que sean legalmente establecidos y el fichero de asociados.</p>\r\n<p>e) Custodiar la documentación de la entidad.</p>\r\n<p>f) Hacer que se cursen a los Registros correspondientes las comunicaciones sobre designación de Juntas Directivas y demás acuerdos sociales inscribibles, así como que se presenten las cuentas anuales y que se cumplan las obligaciones documentales en los términos que legalmente correspondan.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 12.- </strong>Son funciones del Tesorero:</p>\r\n<p>a) Recaudar y custodiar los fondos pertenecientes a la Asociación.</p>\r\n<p>b) Dar cumplimiento a las órdenes de pago que expida el Presidente.</p>\r\n<p>c) Elaborar y presentar las cuentas anuales de la Asociación y cumplir con las obligaciones fiscales de la misma.</p>\r\n<p>d) Llevar inventario de los bienes, si los hubiera.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 13.- </strong>Los Vocales tendrán las obligaciones propias de su cargo como miembros de la Junta Directiva, así como las que nazcan de las delegaciones o comisiones de trabajo que la propia Junta las encomiende.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 14.- </strong>Las vacantes que se pudieran producir durante el mandato de cualquiera de los miembros de la Junta Directiva serán cubiertas provisionalmente entre dichos miembros hasta la elección definitiva por la Asamblea General Extraordinaria.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 15.- </strong>Para que los acuerdos de la Junta Directiva sean válidos deberán adoptarse por mayoría de votos de los asistentes, siendo el Secretario el encargado de levantar acta, que se transcribirá en el libro de Actas o Acuerdos, de conformidad con el art. 11.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 16.- </strong>La Asamblea General es el órgano supremo de gobierno de la Asociación y estará integrada por todos los asociados.</p>\r\n<p>Las reuniones de la Asamblea General serán ordinarias y extraordinarias. La ordinaria se celebrará una vez al año dentro de los cuatro meses siguientes al cierre del ejercicio; las extraordinarias se celebrarán cuando las circunstancias lo aconsejen, a juicio del Presidente, cuando la Directiva lo acuerde o cuando lo proponga por escrito una décima parte de los asociados.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 17.- </strong>Las convocatorias de las Asambleas Generales se realizarán por escrito que señale el lugar, día y hora de la reunión, así como el orden del día con expresión concreta de los asuntos a tratar. Entre la convocatoria y el día señalado para la celebración de la Asamblea en primera convocatoria habrán de mediar al menos quince días, pudiendo así mismo hacerse constar, si procediere, la fecha y hora en que se reunirá la Asamblea en segunda convocatoria, sin que entre una y otra pueda mediar un plazo inferior a una hora.</p>\r\n<p>Las Asambleas Generales, tanto ordinarias como extraordinarias, quedarán válidamente constituidas en primera convocatoria cuando concurran a ella un tercio de los asociados con derecho a voto, y en segunda convocatoria cualquiera que sea el número de asociados con derecho a voto.</p>\r\n<p>Los acuerdos se tomarán por mayoría simple de las personas presentes, o representadas mediante autorización escrita; es decir, cuando los votos afirmativos superen a los negativos, no siendo computables a estos efectos los votos en blanco ni las abstenciones.</p>\r\n<p>&nbsp;</p>\r\n<p>Será necesaria mayoría cualificada de las personas presentes o representadas, que resultará cuando los votos afirmativos superen la mitad de éstas, para:</p>\r\n<p>a) Nombramiento de las Juntas directivas y administradores.</p>\r\n<p>b) Acuerdo para constituir una Federación de Asociaciones o integrarse en ella.</p>\r\n<p>c) Disposición o enajenación de bienes integrantes del inmovilizado.</p>\r\n<p>d) Modificación de los estatutos.</p>\r\n<p>e) Disolución de la entidad.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 18.- </strong>Son facultades de la Asamblea General Ordinaria:</p>\r\n<p>a) Aprobar, en su caso, la gestión de la Junta Directiva.</p>\r\n<p>b) Examinar y aprobar las Cuentas anuales.</p>\r\n<p>c) Aprobar o rechazar las propuestas de la Junta Directiva en orden a las actividades de la Asociación.</p>\r\n<p>d) Fijar las cuotas ordinarias o extraordinarias y la cuantía y periodicidad de las mismas, así como acordar los gastos que deben atenderse con las cuotas extraordinarias.</p>\r\n<p>e) Cualquiera otra que no sea de la competencia exclusiva de la Asamblea Extraordinaria.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Corresponde a la Asamblea General Extraordinaria: </strong></p>\r\n<p>a) Nombramiento de los miembros de la Junta Directiva.</p>\r\n<p>b) Modificación de los Estatutos.</p>\r\n<p>c) Disolución de la Asociación.</p>\r\n<p>d) Expulsión de socios, a propuesta de la Junta Directiva.</p>\r\n<p>e) Constitución de Federaciones o integración en ellas.</p>\r\n<p>f) Solicitud de declaración de utilidad pública.</p>\r\n<p>&nbsp;</p>\r\n<h3><strong>SOCIOS.- DERECHOS Y DEBERES</strong></h3>\r\n<p><strong>Artículo 19.- </strong>Podrán pertenecer a la Asociación aquellas personas con capacidad de obrar que tengan interés en el desarrollo de los fines de la Asociación. Las solicitudes deberán realizarse por escrito en que conste la manifestación de voluntad de asociarse y de acatar los estatutos y las disposiciones que rijan en cada momento.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 20.- </strong>Las categorías de socios serán: Amigos, Protectores y Protectores Empresariales o Institucionales.</p>\r\n<p>Tendrá categoría de “Amigo” toda persona que solicite su ingreso y contribuya a los fines de la Asociación con la aportación que se establezca como mínima.</p>\r\n<p>Con el fin de fomentar el conocimiento del Museo por parte de los más jóvenes, se crea un apartado para “Amigos menores de 25 años” cuya aportación será inferior a la mínima establecida, aunque gozarán de los mismos derechos.</p>\r\n<p>Serán “Socios Protectores” quienes contribuyan al sostenimiento de la Asociación con una aportación superior a la que se estipule como mínima.</p>\r\n<p>“Protectores empresariales o Institucionales” son aquellas empresas o entidades (también puede existir la figura a nivel individual) que aporten a la Asociación una cuota más elevada o contribuyan con su apoyo en ciertas ayudas concretas que, para el beneficio del Museo o de la Asociación, puedan ofrecer.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 21.- </strong>Los socios causarán baja por alguna de las causas siguientes:</p>\r\n<p>a) Por renuncia voluntaria, comunicada por escrito a la Junta Directiva.</p>\r\n<p>b) Por incumplimiento de las obligaciones económicas y de socio.</p>\r\n<p>c) Por conducta contraria a la buena convivencia y a los fines de la Asociación.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 22.- </strong>Todos los socios tendrán derecho a:</p>\r\n<p>a) Participar en las actividades de la Asociación y de sus órganos de gobierno, ejerciendo el derecho de voto.</p>\r\n<p>b) Audiencia previa respecto de la adopción de medidas disciplinarias contra los mismos.</p>\r\n<p>c) Impugnar los acuerdos de los órganos de la Asociación.</p>\r\n<p>d) Acceso a toda la documentación de la Asociación, a través de los órganos de representación, en los términos previstos en la ley orgánica 15/1999, de protección de datos de carácter personal.</p>\r\n<p>e) Entrada gratuita al Museo para el titular del carné y un acompañante.</p>\r\n<p>f) Invitación a las inauguraciones o visitas de preinauguración a las exposiciones del Museo.</p>\r\n<p>g) Invitación a las conferencias del Museo.</p>\r\n<p>h) Información sobre las actividades organizadas por la Asociación.</p>\r\n<p>i) Descuento en el pago de las actividades que no sean gratuitas (cursos, concier-tos, viajes…).</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Los amigos Protectores gozarán, además de:</strong></p>\r\n<p>a) Catálogos de la Asociación, gratuitos.</p>\r\n<p>b) Seis invitaciones al año para que visiten el museo o las exposiciones las personas que designen.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Los Protectores empresariales o institucionales tendrán también las siguientes ventajas: </strong></p>\r\n<p>a) Diez carnés de amigo de la Asociación a nombre de la empresa o entidad.</p>\r\n<p>b) Reconocimiento de esta colaboración en las publicaciones, así como en la memoria y en la difusión publicitaria anual de los actos de la Asociación.</p>\r\n<p>c) Prioridad en la utilización de los espacios del Museo para celebrar recepciones especiales o visitas privadas, que deberán contar con la oportuna autorización del Ministerio de Cultura.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 23.- </strong>Serán obligaciones de todos los socios:</p>\r\n<p>a) Compartir las finalidades de la Asociación y colaborar en la consecución de las mismas.</p>\r\n<p>b) Pagar las cuotas, derramas y aportaciones que pudieren corresponderles.</p>\r\n<p>c) Acatar las prescripciones contenidas en estos Estatutos y los acuerdos válidos adoptados por la Asamblea General y por la Junta Directiva.</p>\r\n<p>d) Cumplir las obligaciones inherentes a su condición.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 24.- </strong>Régimen disciplinario:</p>\r\n<p>El asociado que incumpliere sus obligaciones para con la Asociación, o cuya conducta menoscabe los fines o prestigio de la misma, será objeto del correspondiente expediente disciplinario incoado por la Junta Directiva, quien, previa audiencia al interesado, resolverá lo que proceda.</p>\r\n<p>Las sanciones pueden comprender desde la suspensión temporal de derechos, hasta la expulsión.</p>\r\n<p>Si la Junta acordare la expulsión, propondrá la misma a la Asamblea General para su aprobación.</p>\r\n<p>&nbsp;</p>\r\n<h3><strong>RÉGIMEN ECONÓMICO </strong></h3>\r\n<p><strong>Artículo 25.- </strong>El patrimonio fundacional es inexistente y el presupuestos inicial anual no podrá exceder de ciento cincuenta mil euros.</p>\r\n<p>El ejercicio asociativo y económico será anual, y su cierre tendrá lugar el 31 de diciembre de cada año.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 26.- </strong>Los recursos económicos previstos para el desarrollo de las actividades de la Asociación serán los siguientes:</p>\r\n<p>a) Cuotas de socios.</p>\r\n<p>b) Donativos de entidades o particulares.</p>\r\n<p>c) Subvenciones del Estado, Comunidad Autónoma, Ayuntamiento o Diputación.</p>\r\n<p>d) Herencias o legados que pudiere recibir legalmente por asociados o terceras personas.</p>\r\n<p>e) Ingresos que se obtengan mediante actividades lícitas que acuerde la Junta Directiva.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 27.- </strong>Las cuotas ordinarias o extraordinarias se establecerán por la Asamblea General a propuesta de la Junta Directiva y no serán reintegrables.</p>\r\n<p>La Asamblea General podrá fijar una cuota de admisión de socios, como aportación inicial no reintegrable.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Artículo 28.- </strong>La Asociación dispondrá de una relación actualizada de sus asociados y llevará una contabilidad que permita obtener una imagen fiel del patrimonio, resultado y situación financiera de la misma, así como de de las actividades realizadas. Efectuará un inventario de bienes y recogerá en un libro de actas las reuniones de los órganos de gobierno y representación.</p>\r\n<p>La contabilidad será llevada conforme a las normas específicas de aplicación.</p>\r\n<p>&nbsp;</p>\r\n<h3><strong>DISOLUCIÓN DE LA ASOCIACIÓN </strong></h3>\r\n<p><strong>Artículo 29.- </strong>Se disolverá la Asociación:</p>\r\n<p>a) Voluntariamente cuando así lo acuerde la Asamblea General Extraordinaria, convocada al efecto, por mayoría absoluta de los asociados presentes. Esta Asamblea debe celebrarse con un quórum mínimo de 2/3 de los asociados.</p>\r\n<p>b) Por las causas que especifica el art. 39 del Código Civil.</p>\r\n<p>c) Por sentencia judicial firme.</p>\r\n<p>En caso de disolución, se nombrará una comisión liquidadora la cual si, una vez extinguidas las deudas, existiese sobrante líquido, lo destinará para fines que no desvirtúen su naturaleza no lucrativa (concretamente al Museo Nacional de Escultura).</p>\r\n<p>&nbsp;</p>\r\n<h3><strong>REFORMA DE LOS ESTATUTOS </strong></h3>\r\n<p><strong>Artículo 30.- </strong>Las modificaciones de los presentes estatutos serán competencia de la Asamblea General Extraordinaria, debiéndose adoptar el acuerdo por mayoría cualificada de los presentes y representados. Se deberán comunicar al registro las modificaciones que se realicen.</p>\r\n<p>&nbsp;</p>\r\n<h3><strong>DISPOSICIÓN ADICIONAL </strong></h3>\r\n<p>En todo cuanto no esté previsto en los presentes Estatutos se aplicará la vigente Ley Orgánica 1/2002, de 22 de marzo, reguladora del Derecho de Asociación, y las disposiciones complementarias.</p>'),
(139, 15, 1, 'Amigos del Museo de Escultura Asociation News'),
(140, 15, 2, 'Noticias de la asociación Amigos del Museo de Escultura'),
(141, 122, 1, ''),
(142, 122, 2, '<p>Desde la Asociación Amigos del Museo de Escultura, en nuestra labor de acercar el Museo a la gente, destinamos este espacio para dar a conocer todas las novedades, eventos y noticias de interés para asociados y amigos.</p>'),
(143, 125, 1, ''),
(144, 125, 2, 'Actividades Asociación Amigos del Museo de Escultura'),
(145, 129, 1, '<p>texto</p>'),
(146, 129, 2, '<p>La Asociación Amigos del Museo Nacional de Escultura organiza una serie de actividades con la finalidad de contribuir a la difusión de nuestro rico patrimonio.</p>\r\n<p><strong>Conferencias</strong>, <strong>publicaciones</strong>, <strong>adqusiciones</strong> y <strong>restauraciones</strong>, <strong>intercambios culturales</strong> y otras actividades como <strong>conciertos</strong>, <strong>visitas guiadas</strong> son algunas de las muchas actividades que proponemos desde nuestra asociación.</p>\r\n<h3>¡Conócelas!</h3>'),
(147, 144, 1, ''),
(148, 144, 2, 'Donaciones y Restauraciones'),
(149, 178, 1, ''),
(150, 178, 2, '<p>La Asociación de Amigos del Mueseo Nacional de Escultura, desde sus incios, ha apostado por contribuir en mejorar nuestro patrimonio cultural. Gracias a nuestros socios y <strong><a href=\"#/aportaciones/\" target=\"_self\">donantes</a></strong> hemos adquirido diferentes piezas así como colaborado en la restauración de muchas de ellas.</p>\r\n<h3><strong>1997</strong></h3>\r\n<p>La Asociación de Amigos donó al Museo las siguientes obras:</p>\r\n<p>De J. PÉREZ VILLAAMIL: “Patio del Colegio de San Gregorio” Tinta y acuarela sobre papel (40x 27,5)</p>\r\n<p>“Patio del Colegio de San Gregorio” Grabado(40x 27,5)</p>\r\n<p>Anónimo francés, 1868: “Patio de estudios del Colegio de San Gregorio” Lápiz sobre papel(18x 11, 5)</p>\r\n<p>&nbsp;</p>\r\n<h3><em><strong>1998</strong></em></h3>\r\n<p>La Junta Directiva de la Asociación, a propuesta de la Dirección del Museo, ha adquirido en el comercio del anticuario madrileño una intereseantísima escultura del siglo XVII. Se trata de una representación del <strong>Niño Jesús </strong>de plomo y policromada, que se puede adscribir al escultor ALONSO CANO (1601-1667)</p>\r\n<p>&nbsp;</p>\r\n<h3><em><strong>1999</strong></em></h3>\r\n<p>La Asociación ha financiado la restauración del <strong>órgano realejo</strong>, actualmente expuesto en la capilla del Museo.</p>\r\n<p>&nbsp;</p>\r\n<h3><em><strong>2</strong></em><em><strong>000</strong></em></h3>\r\n<p>A propuesta de la Dirección del Museo, la Asociación financió la restauración de la obra del pintor ANTONIO MORO “Calvario”, firmado y fechado en 1573.</p>\r\n<p>Gestionó con El Corte Inglés la restauración de un óleo sobre tabla titulado “La Virgen de la Rosa”, magnifica copia española del siglo XVI del original RAFAEL DE URBINO.</p>\r\n<p>&nbsp;</p>\r\n<h3><em><strong>2004</strong></em></h3>\r\n<p>Restauración en colaboración con Caja España, de las obras pictóricas de BOCANEGRA.</p>\r\n<p>&nbsp;</p>\r\n<h3><em><strong>2006</strong></em></h3>\r\n<p>Compra y donación al Museo de una pequeña escultura de un Cristo atado en la columna de principios del S. XVI del Círculo de Gregorio Fernández en cera.</p>\r\n<p>&nbsp;</p>\r\n<h3><em><strong>2014</strong></em></h3>\r\n<p>A finales del 2014, la Asociación adquirió para el Museo, unas litografías sobre</p>\r\n<p>papel de Bartolomé de las Casas, Bartolomé de Carranza, Luís de Granada y</p>\r\n<p>Melchor Cano, que ahora mismo están expuestas en las nuevas salas temáticas del Museo.</p>'),
(151, 145, 1, ''),
(152, 145, 2, 'Aumentando nuestro patrimonio cultural'),
(153, 181, 1, 'Links de interés'),
(154, 181, 2, 'Enlaces de interés'),
(155, 185, 1, ''),
(156, 185, 2, '<h3>Museos</h3>\r\n<table style=\"border:none; width:609px\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><a href=\"http://icom.museum/\" target=\"_blank\">The Internacional Council of Museums (ICOM)</a></p></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><a href=\"http://www.europeanmuseumforum.org/\" target=\"_blank\">European Museum Forum (Foro Europeo de los Museos)</a></p></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><a href=\"http://www.icom-ce.org/\" target=\"_blank\">Comité Español del ICO</a></p></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<h3>&nbsp;</h3>\r\n<h3>Cultura</h3>\r\n<table style=\"border:none; width:609px\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><a href=\"http://www.patrimonio-mundial.com/\" target=\"_blank\">Patrimonio Mundial</a></p></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><a href=\"http://www.amigosdemuseos.com/\" target=\"_blank\">Federación Española de Amigos de los Museos</a></p></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<h3>&nbsp;</h3>\r\n<h3>Instituciones</h3>\r\n<table style=\"border:none; width:609px\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><a href=\"https://www.valladolid.es\" target=\"_blank\">Ayuntamiento de Valladolid</a></p></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><a href=\"http://www.jcyl.es/\" target=\"_blank\">Junta de Castilla y León</a></p></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><a href=\"http://www.uva.es/export/sites/uva/\" target=\"_blank\">Universidad de Valladolid</a></p></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><a href=\"http://www.mcu.es/\" target=\"_blank\">Ministerio de Cultura</a></p></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<h3>&nbsp;</h3>\r\n<h3>Valladolid</h3>\r\n<table style=\"border:none; width:609px\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><a href=\"http://diariosalamanca.blogspot.com/\" target=\"_blank\">blogValladolid, diario de una ciudad</a></p></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>&nbsp;</p>'),
(157, 4, 1, ''),
(158, 4, 2, 'Asociación de Amigos del Museo Nacional de Escultura'),
(159, 188, 1, 'Legal advised'),
(160, 188, 2, 'Aviso Legal'),
(161, 192, 1, ''),
(162, 192, 2, '<p>La Web <a href=\"http://www.amigosmuseoescultura.es\">www.amigosmuseoescultura.es</a> tiene por objeto facilitar el conocimiento por el público en general de las actividades que realiza y de los servicios profesionales que presta.</p>\r\n<h3>1.Información general para dar cumplimiento a la Ley 34/200</h3>\r\n<p>Titular: Amigos Museo Nacional de Escultura</p>\r\n<p>Dirección: Colegio San Gregorio C/Cadenas de San Gregorio, s/n, 47011, Valladolid (España)</p>\r\n<p>Contacto: <a href=\"mailto:amigos.museoescultura@mcu.es\">amigos.museoescultura@mcu.es</a> o +34 983 250 375</p>\r\n<p>Datos relativos a la empresa: La asociación de Amigos del Museo Nacional Colegio de San Gregorio está inscrita en el registro Nacional de Asociaciones, de acuerdo con lo dispuesto por el artículo 28.1 apartado e) de la Ley Orgánica 1/2002, de 22 de marzo, reguladora del Derecho de Asociación, en el Grupo: 1/Número Nacional: 56479.</p>\r\n<h3>2.Tabla de precios de los productos y servicios</h3>\r\n<p>Consulta nuestras cuotas de <a href=\"#/miembros\" target=\"_self\">asociados</a></p>\r\n<h3>3.Propiedad intelectual</h3>\r\n<p>Los contenidos suministrados por <a href=\"http://www.amigosmuseoescultura.es\">www.amigosmuseoescultura.es</a> están sujetos a los derechos de propiedad intelectual e industrial y son titularidad exclusiva de Amigos Museo Nacional de Escultura. Mediante la adquisición de un producto o servicio, Amigos Museo Nacional de Escultura no confiere al adquirente ningún derecho de alteración, explotación, reproducción o distribución del mismo fuera de lo estrictamente contratado, reservándose amigos Museo Nacional de Escultura todos estos derechos. La cesión de los citados derechos precisará el previo consentimiento por escrito por parte de Amigos Museo Nacional de Escultura.</p>\r\n<p>La propiedad intelectual se extiende, además del contenido incluido en <a href=\"http://www.amigosmuseoescultura.es\">www.amigosmuseoescultura.es</a>, a sus gráficos, logotipos, diseño, imágenes y código fuente utilizado para su programación.</p>'),
(163, 195, 1, ''),
(164, 195, 2, 'Política de privacidad'),
(165, 196, 1, ''),
(166, 196, 2, 'Amigos del Museo Nacional de Escultura'),
(167, 199, 1, ''),
(168, 199, 2, '<p>A través de la página <a href=\"http://www.amigosmuseoescultura.es\">www.amigosmuseoescultura.es</a> no se recoge ningún dato personal sin su conocimiento, ni se ceden a terceros.</p>\r\n<p>Los datos personales proporcionados a <a href=\"http://www.amigosmuseoescultura.es\">www.amigosmuseoescultura.es</a>, a través de sus formularios de contacto o por los clientes que adquieran productos o servicios de <a href=\"http://www.amigosmuseoescultra.es\">www.amigosmuseoescultra.es</a>, van a ser almacenados en un fichero automatizado de datos, propiedad de Amigos Museo Nacional de Escultura, para la finalidad única de la gestión de la asociación.</p>\r\n<p>El destinatario de la información es en exclusiva Amigos Museo Nacional de Escultura, que se comprometa a <strong>NO ENVIAR COMUNICACIONES PUBLICITARIAS O COMERCIALES SOBRE SUS PRODUCTOS O SERVICIOS.</strong></p>\r\n<p>Amigos Museo Nacional de Escultura se compromete a tratar los datos de manera lícita y responsable, de acuerdo con la Ley Orgánica 15/1999, de Protección de Datos de Carácter Personal, garantizando el Derecho a la intimidad personal y familiar, el honor y el Derecho Fundamental a la protección de los Datos Personales de todas las personas que se encuentren en sus ficheros.</p>\r\n<p>Todo aquel que proporcione sus datos personales a Amigos Museo Nacional de Escultura a través de las web <a href=\"http://www.amigosmuseoescultura.es\">www.amigosmuseoescultura.es</a> tendrá la posibilidad de revocar su consentimiento en cualquier momento, o de ejercitar sus derechos de Acceso, Rectificación, Cancelación y Oposición por cualquier medio admitido en derecho que permita acreditar la personalidad. Para ello deberá dirigirse a Amigos Museo Nacional de Escultura, C/ Cadenas de San Gregorio s/n, 47011, Valladolid (España) o a través de correo electrónico a la dirección <a href=\"mailto:amigos.museoescultura@mcu.es\">amigos.museoescultura@mcu.es</a> bajo el asunto “Datos personales” o también a través del teléfono +34 983 250 375.</p>'),
(169, 202, 1, ''),
(170, 202, 2, 'Política de Cookies'),
(171, 203, 1, ''),
(172, 203, 2, 'Amigos del Museo Nacional de Escultura'),
(173, 206, 1, '<p>texto</p>');
INSERT INTO `idiomas__textos` (`id`, `id_clave`, `id_idioma`, `valor`) VALUES
(174, 206, 2, '<p>El prestador por su propia cuenta o la de un tercero contratado para la prestación de servicios de medición, pueden utilizar cookies cuando un usuario navega por el sitio web. Las cookies son ficheros enviados al navegador por medio de un servidor web con la finalidad de registrar las actividades del usuario durante su tiempo de navegación.</p>\r\n<p>Las cookies utilizadas por el sitio web se asocian únicamente con un usuario anónimo y su ordenador, y no proporcionan por sí mismas los datos personales del usuario.</p>\r\n<p>Mediante el uso de las cookies resulta posible que el servidor donde se encuentra la web, reconozca el navegador web utilizado por el usuario con la finalidad de que la navegación sea más sencilla, permitiendo, por ejemplo, el acceso a los usuarios que se hayan registrado previamente, acceder a las áreas, servicios, promociones o concursos reservados exclusivamente a ellos sin tener que registrarse en cada visita. Se utilizan también para medir la audiencia y parámetros del tráfico, controlar el progreso y número de entradas.</p>\r\n<p>El usuario tiene la posibilidad de configurar su navegador para ser avisado de la recepción de cookies y para impedir su instalación en su equipo. Por favor, consulte las instrucciones y manuales de su navegador para ampliar esta información.</p>\r\n<p>Para utilizar el sitio web, no resulta necesario que el usuario permita la instalación de las cookies enviadas por el sitio web, o el tercero que actúe en su nombre, sin perjuicio de que sea necesario que el usuario inicie una sesión como tal en cada uno de los servicios cuya prestación requiera el previo registro o \"login\".</p>\r\n<p>Las cookies utilizadas en este sitio web tienen, en todo caso, carácter temporal con la única finalidad de hacer más eficaz su transmisión ulterior. En ningún caso se utilizarán las cookies para recoger información de carácter personal.</p>\r\n<h3><strong>Tabla de cookies utilizadas:</strong></h3>\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th>Proveedor</th>\r\n			<th>Nombre</th>\r\n			<th>Propósito</th>\r\n			<th>Más información</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Amigos del Museo Nacional de Escultura</td>\r\n			<td>PHPSESSID</td>\r\n			<td>Permite al usuario visualizar la página e interactuar con ella. Es esencial para el funcionamiento correcto de la página.</td>\r\n			<td>&nbsp;\r\n			<p><a href=\"#/aviso-legal\">Aviso legal</a></p>\r\n			<p><a href=\"#/politica-de-privacidad\">Política de privacidad</a></p></td>\r\n		</tr>\r\n		<tr>\r\n			<td>Google Analytics</td>\r\n			<td>\r\n			<p>__utma<br />\r\n			__utmb</p>\r\n			<p>__utmc<br />\r\n			__utmz</p></td>\r\n			<td>Recopilan información anónima sobre la navegación de los usuarios por el sitio web con el fin de conocer el origen de las visitas y otros datos estadísticos similares.</td>\r\n			<td>&nbsp;\r\n			<p><a href=\"https://www.google.com/intl/es/policies/\" target=\"_blank\">Centro de privacidad de Google</a></p>\r\n			<p><a href=\"http://tools.google.com/dlpage/gaoptout?hl=es\" target=\"_blank\">Complemento de inhabilitación de Google Analytics</a></p></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>Para desactivar las cookies, el usuario podrá, en cualquier momento, elegir cuáles quiere que funcionen en este sitio web mediante:</p>\r\n<p><strong>Configuración del navegador</strong>; por ejemplo:</p>\r\n<ol>\r\n	<li>Firefox, desde&nbsp;<a href=\"http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we\" target=\"_blank\">http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we</a></li>\r\n	<li>Chrome, desde&nbsp;<a href=\"http://support.google.com/chrome/bin/answer.py?hl=es&amp;answer=95647\" target=\"_blank\">http://support.google.com/chrome/bin/answer.py?hl=es&amp;answer=95647</a></li>\r\n	<li>Explorer, desde&nbsp;<a href=\"http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9\" target=\"_blank\">http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9</a></li>\r\n	<li>Safari, desde&nbsp;<a href=\"http://support.apple.com/kb/ph5042\" target=\"_blank\">http://support.apple.com/kb/ph5042</a></li>\r\n</ol>\r\n<p><strong>Sistemas de opt-out </strong>específicos indicados en la tabla anterior respecto de la cookie de que se trate (estos sistemas pueden conllevar que se instale en su equipo una cookie \"de rechazo\" para que funcione su elección de desactivación)</p>\r\n<p><strong>Otras herramientas de terceros</strong>, disponibles on line, que permiten a los usuarios detectar las cookies en cada sitio web que visita y gestionar su desactivación (por ejemplo, Ghostery:&nbsp;</p>\r\n<ol>\r\n	<li><a href=\"http://www.ghostery.com/privacy-statement\" target=\"_blank\">http://www.ghostery.com/privacy-statement</a>,&nbsp;</li>\r\n	<li><a href=\"http://www.ghostery.com/faq\" target=\"_blank\">http://www.ghostery.com/faq</a>).</li>\r\n</ol>'),
(175, 207, 1, ''),
(176, 207, 2, 'Aportaciones - Micromecenazgo'),
(177, 208, 1, ''),
(178, 208, 2, 'Colaborar con el Museo Nacional de Escultura'),
(179, 209, 1, ''),
(180, 209, 2, '<p>Desde de hace décadas, la Asociación de Amigos del Museo Nacional de Escultura promueve una serie de donaciones y restaruaciones con el fín de mejorar nuestro patrimonio cultural. Gracias a tu apoyo y al de cientos de donantes hemos llevado a cabo <a href=\"#/donacion-y-restauracion\" target=\"_self\">diferentes acciones</a>.&nbsp;</p>'),
(181, 210, 1, ''),
(182, 210, 2, 'Quiero hacer una donación'),
(183, 211, 1, ''),
(184, 211, 2, '<p>Para hacer una aportación rellena el siguiente formulario indicando tus datos y el importe que quieres donar. Recibirás un comprobante con el que podrás desgravarte.</p>'),
(185, 212, 1, ''),
(186, 212, 2, '<h3><strong>El objeto de tu donativo</strong></h3>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse dictum orci nec quam blandit tempor. Sed commodo ut nibh consectetur viverra. Duis tincidunt mauris eget magna iaculis, ac scelerisque nunc aliquet. Integer vestibulum porta porttitor. Sed accumsan erat ex, ut congue justo mattis in. Pellentesque fermentum quam suscipit, eleifend metus sed, placerat erat. Vestibulum gravida, nulla et iaculis ultrices, tellus dolor euismod turpis, sed convallis sapien metus quis risus. Praesent quam orci, venenatis et rutrum sed, fringilla vel tortor. Vestibulum facilisis sagittis ipsum non vehicula. Donec porttitor est mi, non tincidunt est vehicula a. Quisque et leo eros. Morbi dignissim dictum vestibulum. Praesent at laoreet augue. Etiam rhoncus urna neque, sed dignissim odio euismod quis.</p>'),
(187, 213, 1, '<p>Los datos recogidos serán incorporados a una base de datos propiedad de Amigos del Museo Nacional de Escultura con el fin único de la gestión de nuestra asociación. Podrá en todo momento acceder para modificar o eliminar sus datos o para darse de baja, para ello, deberá dirigirse de manera presencial a nuestras oficinas, sitas en el propio Museo en horario de atención al público.</p>'),
(188, 213, 2, '<h5>Los datos recogidos serán incorporados a una base de datos propiedad de Amigos del Museo Nacional de Escultura con el fin único de la gestión de nuestra asociación. Podrá en todo momento acceder para modificar o eliminar sus datos o para darse de baja, para ello, deberá dirigirse de manera presencial a nuestras oficinas, sitas en el propio Museo en horario de atención al público.</h5>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__archivos`
--

DROP TABLE IF EXISTS `noticias__archivos`;
CREATE TABLE IF NOT EXISTS `noticias__archivos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_noticia` int(11) UNSIGNED NOT NULL,
  `creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `descripcion` varchar(1000) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_noticia` (`id_noticia`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `noticias__archivos`
--

INSERT INTO `noticias__archivos` (`id`, `id_noticia`, `creacion`, `descripcion`) VALUES
(37, 1, '2016-07-11 07:59:47', 'Adjunto 1'),
(38, 1, '2016-07-11 08:00:14', 'Adjunto 2'),
(39, 2, '2016-07-11 10:18:01', 'Cuaderno de campo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__categorias`
--

DROP TABLE IF EXISTS `noticias__categorias`;
CREATE TABLE IF NOT EXISTS `noticias__categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `url` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `titulo` varchar(120) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `meta_titulo` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `meta_descripcion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `owner_id` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `noticias__categorias`
--

INSERT INTO `noticias__categorias` (`id`, `nombre`, `url`, `titulo`, `meta_titulo`, `meta_descripcion`, `position`, `owner_id`, `created_at`, `updated_at`) VALUES
(1, 'Categoría Primera', 'categoria-primera', NULL, 'Categoría Primera', 'Categoría Primera', -1, 0, '2017-08-01 03:53:00', '2017-08-01 03:53:00'),
(2, 'Categoría Segunda', 'categoria-segunda', NULL, 'Categoría Segunda', 'Categoría Segunda', -3, 0, '2017-07-31 21:15:00', '2017-07-31 21:15:00'),
(3, 'Categoría Tercera', 'categoria-tercera', NULL, 'Categoría Tercera', 'Categoría Tercera', -4, 0, '2017-10-06 07:44:00', '2017-10-06 07:44:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__categories`
--

DROP TABLE IF EXISTS `noticias__categories`;
CREATE TABLE IF NOT EXISTS `noticias__categories` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'url en español',
  `nombre` varchar(255) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre en español',
  `position` int(11) NOT NULL DEFAULT '0',
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `noticias__categories`
--

INSERT INTO `noticias__categories` (`id`, `url`, `nombre`, `position`, `owner_id`, `created_at`, `updated_at`) VALUES
(1, 'expo', 'Exposiciones', -2, 0, NULL, NULL),
(2, 'itinerante-en', 'Itinerante', -1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__comments`
--

DROP TABLE IF EXISTS `noticias__comments`;
CREATE TABLE IF NOT EXISTS `noticias__comments` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` int(11) UNSIGNED NOT NULL,
  `parent_id` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `number` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `author` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `content` text COLLATE utf8_spanish2_ci NOT NULL,
  `date` datetime DEFAULT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `moderated` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__files`
--

DROP TABLE IF EXISTS `noticias__files`;
CREATE TABLE IF NOT EXISTS `noticias__files` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `owner_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'propietario (id)',
  `owner_type` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'propietario (mod)',
  `type` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'tipo de archivo',
  `title_1` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'título',
  `name` text COLLATE utf8_spanish2_ci COMMENT 'nombre original',
  `mime_type` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'tipo recogido',
  `size` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'tamaño',
  `height` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'alto (si imagen)',
  `width` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ancho (si imagen)',
  `creacion` timestamp NULL DEFAULT NULL,
  `modificacion` timestamp NULL DEFAULT NULL,
  `path` varchar(200) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `owner_id` (`owner_id`,`owner_type`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `noticias__files`
--

INSERT INTO `noticias__files` (`id`, `owner_id`, `owner_type`, `type`, `title_1`, `name`, `mime_type`, `size`, `height`, `width`, `creacion`, `modificacion`, `path`) VALUES
(63, 2, 'posts', 'img', 'BLACK_600x400.png', 'BLACK_600x400.png', 'image/png', 1780, 400, 600, '2017-10-06 08:10:55', NULL, NULL),
(64, 12, 'galeria', 'galery-img', 'BLACK_100x100.png', 'BLACK_100x100.png', 'image/png', 271, 100, 100, '2017-10-06 08:10:55', NULL, NULL),
(65, 11, 'galeria', 'galery-img', 'BLACK_100x100.png', 'BLACK_100x100.png', 'image/png', 271, 100, 100, '2017-10-06 08:10:55', NULL, NULL),
(66, 3, 'galeria', 'galery-img', 'BLACK_100x100.png', 'BLACK_100x100.png', 'image/png', 271, 100, 100, '2017-10-06 08:10:55', NULL, NULL),
(67, 1, 'posts', 'img', 'WHITE_600x400.png', 'WHITE_600x400.png', 'image/png', 1780, 400, 600, '2017-10-06 09:43:00', NULL, NULL),
(68, 1, 'galeria', 'galery-img', 'WHITE_100x100.png', 'WHITE_100x100.png', 'image/png', 271, 100, 100, '2017-10-06 09:43:00', NULL, NULL),
(69, 3, 'posts', 'img', 'GREY_600x400.png', 'GREY_600x400.png', 'image/png', 1780, 400, 600, '2017-10-06 09:51:26', NULL, NULL),
(70, 4, 'posts', 'img', 'GREY_600x400.png', 'GREY_600x400.png', 'image/png', 1780, 400, 600, '2017-10-06 10:39:51', NULL, NULL),
(71, 5, 'posts', 'img', 'BLACK_600x400.png', 'BLACK_600x400.png', 'image/png', 1780, 400, 600, '2017-10-06 10:40:02', NULL, NULL),
(72, 6, 'posts', 'img', 'WHITE_600x400.png', 'WHITE_600x400.png', 'image/png', 1780, 400, 600, '2017-10-06 10:40:15', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__galeria`
--

DROP TABLE IF EXISTS `noticias__galeria`;
CREATE TABLE IF NOT EXISTS `noticias__galeria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_noticia` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `alt` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_noticia` (`id_noticia`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `noticias__galeria`
--

INSERT INTO `noticias__galeria` (`id`, `id_noticia`, `nombre`, `alt`, `position`, `created_at`, `updated_at`) VALUES
(1, 1, '1', '1', -1, '2017-02-09 13:31:42', NULL),
(3, 2, '3', '3', -3, NULL, NULL),
(11, 2, '2', '2', -2, NULL, NULL),
(12, 2, '1', '1', -1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__posts`
--

DROP TABLE IF EXISTS `noticias__posts`;
CREATE TABLE IF NOT EXISTS `noticias__posts` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'url en español',
  `title` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'título en español',
  `meta_titulo` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `meta_descripcion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `intro` text COLLATE utf8_spanish2_ci COMMENT 'entradilla en español',
  `content` text COLLATE utf8_spanish2_ci COMMENT 'contenido en español',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'fecha de creación',
  `leido` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'veces visto',
  `publicado` tinyint(1) NOT NULL DEFAULT '1',
  `category_id` int(11) UNSIGNED DEFAULT NULL COMMENT 'id de categoría',
  `publishing_date` timestamp NULL DEFAULT NULL COMMENT 'fecha de publicación',
  `modification_date` timestamp NULL DEFAULT NULL COMMENT 'ultima modificación',
  PRIMARY KEY (`id`),
  UNIQUE KEY `POST_URL` (`url`) USING BTREE,
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `noticias__posts`
--

INSERT INTO `noticias__posts` (`id`, `url`, `title`, `meta_titulo`, `meta_descripcion`, `intro`, `content`, `date`, `leido`, `publicado`, `category_id`, `publishing_date`, `modification_date`) VALUES
(1, 'lorem-ipsum-dolor-sit-amet', 'Lorem ipsum dolor sit amet', '', '', 'consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>\r\n<p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.</p>\r\n<p>Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus.</p>', '2017-10-06 09:43:00', 69, 1, 1, '2017-02-08 08:27:00', '2017-10-06 07:40:00'),
(2, 'donec-pede-justo', 'Donec pede justo', 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, ', 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, ', 'Fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut', '<p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.</p>\r\n<p>Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus.</p>', '2017-10-06 09:50:13', 45, 1, 1, '2017-02-09 11:30:00', '2017-10-06 04:10:00'),
(3, 'aenean-leo-ligula-porttitor-eu-1-1', 'Aenean leo ligula, porttitor eu', 'Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. ', 'Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. ', 'Consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. ', '<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.</p>\r\n<p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus.</p>', '2017-10-06 10:36:15', 0, 0, 2, '2017-10-06 05:50:00', '2017-10-06 05:50:00'),
(4, 'aenean-leo-ligula-porttitor-eu-1-1-1', 'Aenean leo ligula, porttitor eu-1', 'Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. ', 'Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. ', 'Consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. ', '<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.</p>\r\n<p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus.</p>', '2017-10-06 10:39:51', 0, 0, 2, '2017-10-06 03:50:00', '2017-10-06 03:50:00'),
(5, 'donec-pede-justo-1', 'Donec pede justo-1', 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, ', 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, ', 'Fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut', '<p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.</p>\r\n<p>Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus.</p>', '2017-10-06 10:40:02', 45, 0, 1, '2017-02-09 10:30:00', '2017-10-06 02:10:00'),
(6, 'lorem-ipsum-dolor-sit-amet-1', 'Lorem ipsum dolor sit amet-1', '', '', 'consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>\r\n<p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.</p>\r\n<p>Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus.</p>', '2017-10-06 10:40:15', 69, 0, 1, '2017-02-08 07:27:00', '2017-10-06 05:40:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__rel_tags`
--

DROP TABLE IF EXISTS `noticias__rel_tags`;
CREATE TABLE IF NOT EXISTS `noticias__rel_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) UNSIGNED NOT NULL COMMENT 'id de post',
  `tag_id` int(11) UNSIGNED NOT NULL COMMENT 'id de tag',
  PRIMARY KEY (`id`),
  UNIQUE KEY `post_id` (`post_id`,`tag_id`),
  KEY `FK_NOTICIAS_REL_TAGS_TAG` (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `noticias__rel_tags`
--

INSERT INTO `noticias__rel_tags` (`id`, `post_id`, `tag_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(4, 2, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias__tags`
--

DROP TABLE IF EXISTS `noticias__tags`;
CREATE TABLE IF NOT EXISTS `noticias__tags` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_tipo` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'url en español',
  `text` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'nombre en español',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `noticias__tags`
--

INSERT INTO `noticias__tags` (`id`, `id_tipo`, `url`, `text`) VALUES
(1, NULL, 'primero', 'primero'),
(2, NULL, 'segundo', 'segundo'),
(3, NULL, 'novedad', 'novedad'),
(4, NULL, 'eventos', 'eventos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(128) COLLATE utf8_spanish2_ci NOT NULL,
  `codigo_cliente` int(11) UNSIGNED DEFAULT NULL,
  `id_token` int(11) UNSIGNED DEFAULT NULL,
  `nombre` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `apellidos` varchar(160) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nif` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `nif_dir_facturacion` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre_dir_facturacion` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `codigo_dir_facturacion` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `direccion_dir_facturacion` varchar(120) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `cp_dir_facturacion` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `localidad_dir_facturacion` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `provincia_dir_facturacion` int(11) DEFAULT NULL,
  `pais_dir_facturacion` int(11) DEFAULT NULL,
  `codigo_dir_envio` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre_dir_envio` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `direccion_dir_envio` varchar(120) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `cp_dir_envio` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `localidad_dir_envio` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `provincia_dir_envio` int(11) DEFAULT NULL,
  `pais_dir_envio` int(11) DEFAULT NULL,
  `envioEmpresa` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `envioImporte` float NOT NULL DEFAULT '0',
  `envioObservaciones` varchar(1000) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `formaDePago` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `is_factura` tinyint(1) NOT NULL DEFAULT '0',
  `is_envolver_regalo` tinyint(1) NOT NULL DEFAULT '0',
  `is_tarjeta_regalo` tinyint(1) NOT NULL DEFAULT '0',
  `str_tarjeta_regalo` varchar(1000) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `vale` int(11) DEFAULT '0',
  `pvp_total` float NOT NULL DEFAULT '0',
  `peso_total` int(11) NOT NULL DEFAULT '0',
  `pagando` tinyint(1) NOT NULL DEFAULT '0',
  `pagandoFecha` datetime DEFAULT NULL,
  `bancoRespPre` mediumtext COLLATE utf8_spanish2_ci,
  `bancoRespNot` mediumtext COLLATE utf8_spanish2_ci,
  `bancoRespConfirm` mediumtext COLLATE utf8_spanish2_ci,
  `bancoRespCancel` mediumtext COLLATE utf8_spanish2_ci,
  `validacion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `carrito` mediumtext COLLATE utf8_spanish2_ci,
  `request_id` int(11) DEFAULT NULL,
  `historico` tinyint(1) NOT NULL DEFAULT '0',
  `correo_resumen` tinyint(1) NOT NULL DEFAULT '0',
  `correo_envio` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `codigo_cliente` (`codigo_cliente`),
  KEY `codigo_token` (`id_token`),
  KEY `provincia_dir_facturacion` (`provincia_dir_facturacion`),
  KEY `pais_dir_facturacion` (`pais_dir_facturacion`),
  KEY `codigo_dir_envio` (`codigo_dir_envio`)
) ENGINE=InnoDB AUTO_INCREMENT=461 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `orders`
--

INSERT INTO `orders` (`id`, `code`, `codigo_cliente`, `id_token`, `nombre`, `apellidos`, `email`, `nif`, `telefono`, `fecha_nacimiento`, `nif_dir_facturacion`, `nombre_dir_facturacion`, `codigo_dir_facturacion`, `direccion_dir_facturacion`, `cp_dir_facturacion`, `localidad_dir_facturacion`, `provincia_dir_facturacion`, `pais_dir_facturacion`, `codigo_dir_envio`, `nombre_dir_envio`, `direccion_dir_envio`, `cp_dir_envio`, `localidad_dir_envio`, `provincia_dir_envio`, `pais_dir_envio`, `envioEmpresa`, `envioImporte`, `envioObservaciones`, `formaDePago`, `is_factura`, `is_envolver_regalo`, `is_tarjeta_regalo`, `str_tarjeta_regalo`, `vale`, `pvp_total`, `peso_total`, `pagando`, `pagandoFecha`, `bancoRespPre`, `bancoRespNot`, `bancoRespConfirm`, `bancoRespCancel`, `validacion`, `creation_date`, `modification_date`, `carrito`, `request_id`, `historico`, `correo_resumen`, `correo_envio`) VALUES
(304, 'H2017009KNX07AO08T70U487L1ZC19', NULL, 979, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-07 08:48:19', '2017-09-07 08:48:19', NULL, NULL, 0, 0, 0),
(305, 'D2017R09BP0070K0962AA35TH96Z17', NULL, 980, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-07 09:35:17', '2017-09-07 09:35:17', NULL, NULL, 0, 0, 0),
(306, '72017A09A4U118B06LVI415EZI8N27', NULL, 981, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 06:15:27', '2017-09-11 06:15:27', NULL, NULL, 0, 0, 0),
(307, '72017O09VNP11VQ07U3NJ05PCK0L01', 5, 983, 'adsasd', 'asdasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 07:05:01', '2017-09-11 07:05:01', NULL, NULL, 0, 0, 0),
(308, 'S2017X09SXL11C507BRJT11TM0YN42', NULL, 984, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 07:11:42', '2017-09-11 07:11:42', NULL, NULL, 0, 0, 0),
(309, 'T2017V09CN911KD07L7DJ143924M04', NULL, 985, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 07:14:04', '2017-09-11 07:14:04', NULL, NULL, 0, 0, 0),
(310, 'O2017O09980114X079Y3R15EG4F029', NULL, 986, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 07:15:29', '2017-09-11 07:15:29', NULL, NULL, 0, 0, 0),
(311, '62017H09IEF11QU07E73A16Y2C7K50', NULL, 987, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 07:16:50', '2017-09-11 07:16:50', NULL, NULL, 0, 0, 0),
(312, 'S2017009MLX11F507JG8H18V3P3M13', NULL, 988, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 07:18:13', '2017-09-11 07:18:13', NULL, NULL, 0, 0, 0),
(313, '82017A096OL114M07ENT818TQV3423', NULL, 989, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 07:18:23', '2017-09-11 07:18:23', NULL, NULL, 0, 0, 0),
(314, 'T20179099J211CE07XY7T21J3JSS13', NULL, 990, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 07:21:13', '2017-09-11 07:21:13', NULL, NULL, 0, 0, 0),
(315, 'D2017D093Z311JH078YZL416KFTA14', NULL, 995, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 07:41:14', '2017-09-11 07:41:14', NULL, NULL, 0, 0, 0),
(316, '32017U09RO611WI08SF8019MEJLL20', NULL, 996, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 08:19:20', '2017-09-11 08:19:20', NULL, NULL, 0, 0, 0),
(317, '32017E09E0W11T9088BHW27EK6A346', NULL, 997, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 08:27:46', '2017-09-11 08:27:46', NULL, NULL, 0, 0, 0),
(318, 'M2017S09U1011RC09BENT36U4YDS05', NULL, 999, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 09:36:05', '2017-09-11 09:36:05', NULL, NULL, 0, 0, 0),
(319, 'N2017109CFR11VW10BMRF31V4QKR33', NULL, 1004, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 10:31:33', '2017-09-11 10:31:33', NULL, NULL, 0, 0, 0),
(320, 'T2017009MDC11Z5105OBR33N92QP59', NULL, 1007, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 10:33:59', '2017-09-11 10:33:59', NULL, NULL, 0, 0, 0),
(321, 'R2017T09KHJ113D10AR9634LGWPP26', NULL, 1009, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 10:34:26', '2017-09-11 10:34:26', NULL, NULL, 0, 0, 0),
(322, 'N2017X09EJR123707BIBL21G72O640', NULL, 1011, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-12 07:21:40', '2017-09-12 07:21:40', NULL, NULL, 0, 0, 0),
(323, 'T2017K09L3I1360086K1B084XR8904', 5, 1013, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-13 08:08:04', '2017-09-13 08:09:14', NULL, NULL, 0, 0, 0),
(324, '92017U09ICL13QQ09459P255V06R13', NULL, 1014, 'daaadsaddsasdsa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-13 09:25:13', '2017-09-13 10:01:24', NULL, NULL, 0, 0, 0),
(325, '62017R0948E13X210KMWS46KAQJ102', NULL, 1016, 'daaadsaddsasdsa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 78, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-13 10:46:02', '2017-09-13 12:24:56', NULL, NULL, 0, 0, 0),
(326, 'Y2017Y09H2213H612QMHL26832YF17', NULL, 1018, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-13 12:26:17', '2017-09-13 12:26:17', NULL, NULL, 0, 0, 0),
(327, '92017E09LP413CS148JS331VJC9Z12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-13 14:31:12', '2017-09-13 03:09:35', NULL, NULL, 0, 0, 0),
(328, 'Q2017709VOF13M712GGPP48QDJLE11', NULL, 1023, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-13 12:48:11', '2017-09-13 12:48:11', NULL, NULL, 0, 0, 0),
(329, 'F2017U0903814BE075WFZ07ZHCMJ55', NULL, 1025, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 17, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-14 07:07:55', '2017-09-14 07:08:14', NULL, NULL, 0, 0, 0),
(330, 'M2017U09KHP147O08SPLA11I5OQ631', NULL, 1027, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-14 08:11:31', '2017-09-14 08:11:31', NULL, NULL, 0, 0, 0),
(331, 'F2017S09A2814NJ080PJ51762A2839', 5, 1028, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-14 08:17:39', '2017-09-14 08:18:02', NULL, 210000379, 0, 0, 0),
(332, 'K2017P09QGW14ER08SQ4Q332T53F13', 5, 1029, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-14 08:33:13', '2017-09-14 08:43:12', NULL, NULL, 0, 0, 0),
(333, '32017E09PKG14FV10QN3H41RT9J934', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-14 10:41:34', '2017-09-14 10:09:35', NULL, NULL, 0, 0, 0),
(334, 'X2017Q09W7D14BV09IT8447O4H8G01', 5, 1030, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-14 09:47:01', '2017-09-14 09:47:12', NULL, NULL, 0, 0, 0),
(335, 'S2017309LEZ15LE07VCR705OYSWS41', NULL, 1031, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 07:05:41', '2017-09-15 07:05:41', NULL, NULL, 0, 0, 0),
(336, 'Y2017D093DL15QJ07JO9B31DXAZY40', 5, 1037, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 07:31:40', '2017-09-15 07:35:05', NULL, NULL, 0, 0, 0),
(337, 'A2017Q091AL15D107URH335L0V4419', NULL, 1038, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 07:35:19', '2017-09-15 07:35:19', NULL, NULL, 0, 0, 0),
(338, 'M2017O09WYF15JN07XHAA408EM4B48', NULL, 1041, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 07:40:48', '2017-09-15 07:40:48', NULL, NULL, 0, 0, 0),
(339, 'X2017T09CDJ15DL07097L44CQ0OT24', 5, 1044, 'adsasd', 'asdasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 07:44:24', '2017-09-15 07:44:24', NULL, NULL, 0, 0, 0),
(340, 'J2017E09TMQ157Z094NQW50MOELB24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 09:50:24', '2017-09-15 09:09:37', NULL, NULL, 0, 0, 0),
(341, '42017P09N0X15U5082MGO42BDC7C59', 5, 1045, 'adsasd', 'asdasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 08:42:59', '2017-09-15 08:42:59', NULL, NULL, 0, 0, 0),
(342, 'D2017309SI315HU0962WZ56QRGD206', 5, 1047, 'César', 'Vega Martín', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 09:56:06', '2017-09-15 09:56:06', NULL, NULL, 0, 0, 0),
(343, 'A2017Q09QQ115SG09DGUH568RSUY30', 5, 1048, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 09:56:30', '2017-09-15 09:56:34', NULL, NULL, 0, 0, 0),
(344, 'Q2017L09K27155T0960KN58ILWCW00', 5, 1049, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 09:58:00', '2017-09-15 09:58:04', NULL, NULL, 0, 0, 0),
(345, '02017Y099QO15EP09K9TF58P9BK159', 5, 1050, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 09:58:59', '2017-09-15 10:00:05', NULL, NULL, 0, 0, 0),
(346, '62017M0923S15IP10R3OR00SX3VU30', 5, 1051, 'César', 'Vega Martín', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 10:00:30', '2017-09-15 10:00:30', NULL, NULL, 0, 0, 0),
(347, 'O2017209GGS152K10P9MW000R1ZF59', 5, 1052, 'César', 'Vega Martín', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 10:00:59', '2017-09-15 10:00:59', NULL, NULL, 0, 0, 0),
(348, '12017A09HQP15FL10EKS601M2HHA28', 5, 1053, 'César', 'Vega Martín', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 10:01:28', '2017-09-15 10:01:28', NULL, NULL, 0, 0, 0),
(349, 'A2017P09C5F15Z610HIEF04QUE7338', 5, 1054, 'César', 'Vega Martín', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 10:04:38', '2017-09-15 10:04:38', NULL, NULL, 0, 0, 0),
(350, '12017W09WT515ZO10T8M505F3QVB05', 5, 1055, 'César', 'Vega Martín', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 10:05:05', '2017-09-15 10:05:05', NULL, NULL, 0, 0, 0),
(351, 'T2017H09RTV15UH108YEB57Y1QXB02', 5, 1056, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 10:57:02', '2017-09-15 10:57:12', NULL, NULL, 0, 0, 0),
(352, '32017E090IQ15SY10I2RP590O0MH54', 5, 1057, 'César', 'Vega Martín', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 10:59:54', '2017-09-15 10:59:54', NULL, NULL, 0, 0, 0),
(353, 'E2017J09VCU15Y611307E00F45HX54', 5, 1059, 'César', 'Vega Martín', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 11:00:54', '2017-09-15 11:00:54', NULL, NULL, 0, 0, 0),
(354, '32017L09JTJ159111T2X701FN7Q046', 5, 1060, 'César', 'Vega Martín', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 11:01:46', '2017-09-15 11:01:46', NULL, NULL, 0, 0, 0),
(355, 'I2017C09BZ2158111U2FC03Y4OJS12', 5, 1061, 'César', 'Vega Martín', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 11:03:12', '2017-09-15 11:03:12', NULL, NULL, 0, 0, 0),
(356, '22017N095LV15A7116T2U06UMAHQ11', 5, 1062, 'César', 'Vega Martín', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 11:06:11', '2017-09-15 11:06:11', NULL, NULL, 0, 0, 0),
(357, 'Z2017K09VQC155811EQH406MI0ML33', 5, 1063, 'César', 'Vega Martín', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 11:06:33', '2017-09-15 11:06:33', NULL, NULL, 0, 0, 0),
(358, '22017709U9M15FY1167YB06ZNTJD49', 5, 1064, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 11:06:49', '2017-09-15 11:06:58', NULL, NULL, 0, 0, 0),
(359, 'F2017H090VL156N11SMNO072GGS212', 5, 1065, 'César', 'Vega Martín', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 11:07:12', '2017-09-15 11:07:12', NULL, NULL, 0, 0, 0),
(360, 'E2017G09JQ215P611ZU4N07FJNJS27', 5, 1066, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 11:07:27', '2017-09-15 11:07:36', NULL, NULL, 0, 0, 0),
(361, 'Y2017Z09SW6155811AZYZ33E7ENX07', 5, 1067, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 11:33:07', '2017-09-15 11:33:20', NULL, NULL, 0, 0, 0),
(362, 'Q201760955J15GT12S3LE04ZLEVC03', 5, 1069, 'César', 'Vega Martín', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 'Pepe Perez', 'c/ 1234', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 12:04:03', '2017-09-15 12:04:03', NULL, NULL, 0, 0, 0),
(363, 'U2017N0988C15W312BKYV41YRPIR31', 5, 1072, NULL, NULL, NULL, NULL, '601181340', NULL, '12312', 'César Vega', NULL, 'Amadeo Aráis', '4014', 'Valladolid', 13, 1, NULL, 'César Vega', 'Amadeo Aráis', '47014', 'Valladolid', 49, 1, NULL, 0, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 32, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 12:41:31', '2017-09-15 12:55:41', NULL, NULL, 0, 0, 0),
(364, '12017709Y1M15OO14YARR59UGJFI36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-15 14:59:36', '2017-09-15 03:09:27', NULL, NULL, 0, 0, 0),
(365, 'E2017H10CF502R9109X9943FPJ2806', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-02 10:43:06', '2017-10-02 10:10:33', NULL, NULL, 0, 0, 0),
(366, 'C2017W10ZCG02DN128CS4047ZQ5J15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-02 12:04:15', '2017-10-02 12:10:38', NULL, NULL, 0, 0, 0),
(367, 'J2017A10R5C02NK12Q8EB22KQN7I35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-02 12:22:35', '2017-10-02 12:10:35', NULL, NULL, 0, 0, 0),
(368, '72017610JB602YJ12WUZZ22S3AGP35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-02 12:22:35', '2017-10-02 12:10:08', NULL, NULL, 0, 0, 0),
(369, 'V2017P10XYB02DY12W96334EBVPX53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-02 12:34:53', '2017-10-02 02:10:32', NULL, NULL, 0, 0, 0),
(370, 'R2017A10TKE02DW12GL5J58597IZ21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-02 12:58:21', '2017-10-02 01:10:49', NULL, NULL, 0, 0, 0),
(371, '92017X10RXB02S214VOIZ340T6P754', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-02 14:34:54', '2017-10-02 02:10:59', NULL, NULL, 0, 0, 0),
(372, '32017K10L3W03VS08OV5C23RTDW230', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-03 08:23:30', '2017-10-03 08:10:55', NULL, NULL, 0, 0, 0),
(373, 'Y2017M10FK203QP08JKH5520W0ZU02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-03 08:52:02', '2017-10-03 08:10:16', NULL, NULL, 0, 0, 0),
(374, 'Y2017R107G604RG14D9R623QUHQ052', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-04 14:23:52', '2017-10-04 02:10:49', NULL, NULL, 0, 0, 0),
(375, '42017N10QHX06WW10A7KJ09UC5QC53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-06 10:09:53', '2017-10-06 10:10:16', NULL, NULL, 0, 0, 0),
(376, 'Q2017010ZRA06ES1175M738USRFK32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-06 11:38:32', '2017-10-06 01:10:19', NULL, NULL, 0, 0, 0),
(377, '62017C10J0B09FO09X9VX19JIRIP13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-09 09:19:13', '2017-10-09 11:10:12', NULL, NULL, 0, 0, 0),
(378, 'C2017410SLI118U09CRMV55VTOCA27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-11 09:55:27', '2017-10-11 11:10:32', NULL, NULL, 0, 0, 0),
(379, 'X2017N109J611ZR12D5AC20CXTUF55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-11 12:20:55', '2017-10-11 12:10:19', NULL, NULL, 0, 0, 0),
(380, 'B2017F10ZVJ138109CXFQ30YUF7Z15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-13 09:30:15', '2017-10-13 09:10:51', NULL, NULL, 0, 0, 0),
(381, 'H2017610BQ413NX12L8DT04MBI7M46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-13 12:04:46', '2017-10-13 12:10:46', NULL, NULL, 0, 0, 0),
(382, 'K2017C10RTK16HJ103DAR4596LGW02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-16 10:45:02', '2017-10-16 10:10:49', NULL, NULL, 0, 0, 0),
(383, '72017J10E4K23ZL093X3D224VJ5O40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-23 09:22:40', '2017-10-23 09:10:59', NULL, NULL, 0, 0, 0),
(384, '22017A102MQ230913IYAV08MSCPW39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-23 13:08:39', '2017-10-23 02:10:55', NULL, NULL, 0, 0, 0),
(385, 'V2017910WY123MA11Z6SG10NSTMT13', NULL, 1135, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-23 11:10:13', '2017-10-23 11:10:13', NULL, NULL, 0, 0, 0),
(386, 'C2017K1007923ZW111SQP50FZ3CA29', NULL, 1137, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-23 11:50:29', '2017-10-23 11:50:29', NULL, NULL, 0, 0, 0),
(387, '32017U10JGF23OM12BMKF12PBUHQ05', 5, 1139, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-23 12:12:05', '2017-10-23 12:22:31', NULL, NULL, 0, 0, 0),
(388, 'V2017C105IQ23OM12PP1P28IQ06939', 5, 1140, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', NULL, NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 276, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-23 12:28:39', '2017-10-23 12:56:02', NULL, NULL, 0, 0, 0),
(389, '42017J109FX23J7120USW569H1NO57', 5, 1141, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, 'TEST', 'contrarembolso', 0, 0, 0, NULL, 0, 26, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-23 12:56:57', '2017-10-23 12:57:40', NULL, NULL, 0, 0, 0),
(390, 'P2017P10G0K23GT12Y7WW59WWIT135', 5, 1142, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 68, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-23 12:59:35', '2017-10-23 13:01:56', NULL, NULL, 0, 0, 0),
(391, 'A2017H10IWD245Z07D3KP0710G0D41', 5, 1145, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-24 07:07:41', '2017-10-24 07:07:41', NULL, NULL, 0, 0, 0),
(392, '12017K101EG24Q409X9KD15ZW50E23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-24 09:15:23', '2017-10-24 09:10:23', NULL, NULL, 0, 0, 0),
(393, 'Q2017I10XSQ24L609CFNI172YXRX06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-24 09:17:06', '2017-10-24 09:10:07', NULL, NULL, 0, 0, 0),
(394, 'B2017Z1020G24E2081K1E10GQ4X949', 5, 1147, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 50, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-24 08:10:49', '2017-10-24 08:11:11', NULL, NULL, 0, 0, 0),
(395, 'O2017610SOV24J211JBO0254QV3I55', 5, 1150, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-24 11:25:55', '2017-10-24 11:40:39', NULL, NULL, 0, 0, 0),
(396, 'T2017O10NMU24DV1297AZ20RV3PH12', 5, 1152, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-24 12:20:12', '2017-10-24 12:20:12', NULL, NULL, 0, 0, 0),
(397, '72017E10UIN24YP13VQ8S00F10GL40', NULL, 1154, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 50, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-24 13:00:40', '2017-10-24 13:00:41', NULL, NULL, 0, 0, 0),
(398, 'Y2017I106B425TE06FWCF57GIX6109', 5, 1155, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 06:57:09', '2017-10-25 06:57:09', NULL, NULL, 0, 0, 0),
(399, 'L2017T10D7125CC09LNZM3401EJV58', NULL, 1157, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 26, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 09:34:58', '2017-10-25 09:35:13', NULL, NULL, 0, 0, 0),
(400, 'V2017610233252910DO6318EJG5X27', 5, 1160, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 25, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 10:18:27', '2017-10-25 10:20:38', NULL, NULL, 0, 0, 0),
(401, 'R2017S10815257S101JUF23BY8U802', 5, 1161, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 10:23:02', '2017-10-25 10:23:18', NULL, NULL, 0, 0, 0),
(402, '72017J1000325PS10OREU27ML1A302', 5, 1162, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 10:27:02', '2017-10-25 10:27:19', NULL, NULL, 0, 0, 0),
(403, 'I2017K10IQB254J10ORZ9270TSE548', 5, 1163, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 10:27:48', '2017-10-25 10:27:48', NULL, NULL, 0, 0, 0),
(404, 'E2017J10RPH25J0100HFK2862IV911', 5, 1164, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 10:28:11', '2017-10-25 10:28:11', NULL, NULL, 0, 0, 0),
(405, 'J2017Y10S04259T10IOCW38SR0D949', 5, 1165, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 10:38:49', '2017-10-25 10:49:16', NULL, NULL, 0, 0, 0),
(406, 'U2017W10NSM259Z10AY5P490WTNX39', 5, 1166, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 10:49:39', '2017-10-25 10:49:39', NULL, NULL, 0, 0, 0),
(407, 'C2017410EAG25I010JU1M496QJ4R57', 5, 1167, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 10:49:57', '2017-10-25 10:49:57', NULL, NULL, 0, 0, 0),
(408, '42017I10KIC25LC111DNY22M473L13', NULL, 1170, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 11:22:13', '2017-10-25 11:22:13', NULL, NULL, 0, 0, 0),
(409, 'S2017H109DL257Z11TBRR371KUWF49', 5, 1171, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 11:37:49', '2017-10-25 11:37:49', NULL, NULL, 0, 0, 0),
(410, 'G2017U10DET25XO11HC8G385JUX907', 5, 1173, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 11:38:07', '2017-10-25 11:38:07', NULL, NULL, 0, 0, 0),
(411, 'L2017L10HE225TY11LE2X38782RQ26', 5, 1176, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 11:38:26', '2017-10-25 11:38:26', NULL, NULL, 0, 0, 0),
(412, 'Y2017O10RA025OQ11VTSP4138XAO36', 14, 1177, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 11:41:36', '2017-10-25 11:41:36', NULL, NULL, 0, 0, 0),
(413, 'W2017310EBH25Z5112BHE42Y9KRO58', 15, 1178, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 11:42:58', '2017-10-25 11:42:58', NULL, NULL, 0, 0, 0),
(414, 'E2017K10PUT25PR11HS8243QI0J857', 15, 1179, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 11:43:57', '2017-10-25 11:43:57', NULL, NULL, 0, 0, 0),
(415, 'C20172105B125O711I7QP44F7OSA18', 5, 1180, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 11:44:18', '2017-10-25 11:44:18', NULL, NULL, 0, 0, 0),
(416, 'S2017010TAJ255K11KG4I44RIID842', 15, 1181, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 11:44:42', '2017-10-25 11:44:42', NULL, NULL, 0, 0, 0),
(417, '52017310FCC25TG1213E81246SCB18', 19, 1182, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 12:12:18', '2017-10-25 12:12:18', NULL, NULL, 0, 0, 0),
(418, 'X2017K107AD25VC12CIWH15HSQSK09', 20, 1183, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 12:15:09', '2017-10-25 12:15:09', NULL, NULL, 0, 0, 0),
(419, 'K2017610OLI25B61454CV31DRPSH04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 14:31:04', '2017-10-25 02:10:35', NULL, NULL, 0, 0, 0),
(420, 'M2017X10FCN25SP12E67N38RE18204', 5, 1184, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 12:38:04', '2017-10-25 12:38:04', NULL, NULL, 0, 0, 0),
(421, 'X2017P103JA259T147Q3J38OYLP147', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 14:38:47', '2017-10-25 02:10:47', NULL, NULL, 0, 0, 0),
(422, 'P2017E10H5X255I14UNPA39XQ3F459', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 14:39:59', '2017-10-25 02:10:59', NULL, NULL, 0, 0, 0),
(423, 'E20175106KC25N2142DBS41RB4XD17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 14:41:17', '2017-10-25 02:10:18', NULL, NULL, 0, 0, 0),
(424, 'D2017R10B3P25R214H6CX50P3XZ129', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 14:50:29', '2017-10-25 02:10:30', NULL, NULL, 0, 0, 0),
(425, 'L2017F10AXC260X06URDU42YD7BP17', 5, 1192, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-26 06:42:17', '2017-10-26 06:42:17', NULL, NULL, 0, 0, 0),
(426, '02017K105VP26UT083H8A42J6S5X26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-26 08:42:26', '2017-10-26 08:10:29', NULL, NULL, 0, 0, 0),
(427, '32017N10NY0268P10DDIK10PCR4X20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-26 10:10:20', '2017-10-26 10:10:22', NULL, NULL, 0, 0, 0),
(428, 'D2017W10G7G26RN11Y4GC3812AZH39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-26 11:38:39', '2017-10-26 11:10:41', NULL, NULL, 0, 0, 0),
(429, 'B2017P100LV26EK11Z6SU44DKBYX20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-26 11:44:20', '2017-10-26 11:10:21', NULL, NULL, 0, 0, 0),
(430, 'L2017E10K0S262211V97W47AZDSL08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-26 11:47:08', '2017-10-26 11:10:09', NULL, NULL, 0, 0, 0),
(431, '12017O10H9V26NS11UUFB49CXMQ449', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-26 11:49:49', '2017-10-26 11:10:50', NULL, NULL, 0, 0, 0),
(432, 'N2017010FP426GM116FZK5069WQT36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-26 11:50:36', '2017-10-26 11:10:36', NULL, NULL, 0, 0, 0),
(433, 'I2017Q10EV126SY10BV0H35477XW01', 5, 1207, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-26 10:35:01', '2017-10-26 10:35:01', NULL, NULL, 0, 0, 0),
(434, 'C2017C10XTU26F4108M4T35SIDM808', 5, 1208, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-26 10:35:08', '2017-10-26 10:35:08', NULL, NULL, 0, 0, 0),
(435, '120171106RT26VF10SHG3397XEUB51', 5, 1209, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, 'TEST', 'tarjeta', 0, 0, 0, NULL, 0, 50, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-26 10:39:51', '2017-10-26 10:46:05', NULL, NULL, 0, 0, 0),
(436, 'A2017910IVR26YX11KFWU28VQ5FL05', 5, 1210, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 50, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-26 11:28:05', '2017-10-26 11:28:57', NULL, NULL, 0, 0, 0),
(437, 'R2017L10XQI26IL13TKES28L83GN23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.98, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-26 13:28:23', '2017-10-26 02:10:17', NULL, NULL, 0, 0, 0),
(438, '22017Z10TMP260E12ONC342PY6SN58', 5, 1213, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 4.98, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-26 12:42:58', '2017-10-26 02:10:38', NULL, NULL, 0, 0, 0),
(439, '32017J1035H26QT12SEQ947SPK3X57', 5, 1211, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 83, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-26 12:47:57', '2017-10-26 02:10:47', NULL, NULL, 0, 0, 0),
(440, '52017W10B0V27W406X5PF133QOPO24', 5, 1214, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 75, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-27 06:13:24', '2017-10-27 06:15:01', NULL, NULL, 0, 0, 0),
(441, '92017L103QD27RE06GJ4S15HOAY532', 5, 1215, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 50, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-27 06:15:32', '2017-10-27 06:15:48', NULL, NULL, 0, 0, 0),
(442, 'U20170100VI271F06VX1Y55158EB13', NULL, 1217, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 26, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-27 06:55:13', '2017-10-27 07:25:44', NULL, NULL, 0, 0, 0),
(443, 'J2017410V8627F507HWBS26O5W0O36', 5, 1219, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 250, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-27 07:26:36', '2017-10-27 07:45:51', NULL, NULL, 0, 0, 0),
(444, 'C20171102OO278008FOXV206517N43', NULL, 1220, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-27 08:20:43', '2017-10-27 08:20:43', NULL, NULL, 0, 0, 0);
INSERT INTO `orders` (`id`, `code`, `codigo_cliente`, `id_token`, `nombre`, `apellidos`, `email`, `nif`, `telefono`, `fecha_nacimiento`, `nif_dir_facturacion`, `nombre_dir_facturacion`, `codigo_dir_facturacion`, `direccion_dir_facturacion`, `cp_dir_facturacion`, `localidad_dir_facturacion`, `provincia_dir_facturacion`, `pais_dir_facturacion`, `codigo_dir_envio`, `nombre_dir_envio`, `direccion_dir_envio`, `cp_dir_envio`, `localidad_dir_envio`, `provincia_dir_envio`, `pais_dir_envio`, `envioEmpresa`, `envioImporte`, `envioObservaciones`, `formaDePago`, `is_factura`, `is_envolver_regalo`, `is_tarjeta_regalo`, `str_tarjeta_regalo`, `vale`, `pvp_total`, `peso_total`, `pagando`, `pagandoFecha`, `bancoRespPre`, `bancoRespNot`, `bancoRespConfirm`, `bancoRespCancel`, `validacion`, `creation_date`, `modification_date`, `carrito`, `request_id`, `historico`, `correo_resumen`, `correo_envio`) VALUES
(445, '12017L109AG27TZ08T74E21LEREN02', 5, 1221, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 280, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-27 08:21:02', '2017-10-27 08:47:07', NULL, NULL, 0, 0, 0),
(446, '62017910LZ527YY08O289511TN8F17', 5, 1223, 'César', 'Vega Martín', NULL, '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 56, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-27 08:51:17', '2017-10-27 08:52:08', NULL, NULL, 0, 0, 0),
(447, 'Y2017K10AUA27JJ09ACYF14MS5EZ58', 5, 1224, 'César', 'Vega Martín', 'cesar@euphorbia.es', '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 26, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-27 09:14:58', '2017-10-27 09:15:05', NULL, NULL, 0, 0, 0),
(448, '72017U10YAG27TK093FM346HD4H105', 5, 1226, 'César', 'Vega Martín', 'cesar@euphorbia.es', '12345645', '601181340', '1988-06-20', NULL, '', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, 'TEST', 'tarjeta', 1, 0, 0, NULL, 0, 26, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-27 09:46:05', '2017-10-27 12:10:53', NULL, NULL, 0, 0, 0),
(449, 'G2017Q10Q8H272212VVI403SO9QY36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-27 12:03:36', '2017-10-27 12:10:50', NULL, NULL, 0, 0, 0),
(450, 'R2017G10W7627BQ10QX5316XARZF31', 5, 1228, 'César', 'Vega Martín', 'cesar@euphorbia.es', '12345645', '42', '1988-06-20', '12345645', 'César Vega', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'César Vega', 'test', '4014', 'Valladolid', 49, 1, NULL, 0, 'Ràpido', 'tarjeta', 1, 0, 0, NULL, 0, 26, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-27 10:16:31', '2017-10-27 01:10:59', NULL, NULL, 0, 0, 0),
(451, 'S2017G10R5M27ZJ11VUIU299MTNY48', 5, 1230, 'César', 'Vega Martín', 'cesar@euphorbia.es', '12345645', '601181340', '1988-06-20', '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, 'NINGUNA', 'tarjeta', 1, 0, 0, NULL, 0, 70, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-27 11:29:48', '2017-10-27 01:10:30', NULL, NULL, 0, 0, 0),
(452, 'M2017N1060V27MS12Y04C07K1SK734', 5, 1232, 'César', 'Vega Martín', 'cesar@euphorbia.es', '12345645', '601181340', '1988-06-20', '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, 'test', 'tarjeta', 1, 0, 0, NULL, 0, 52, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-27 12:07:34', '2017-10-27 02:10:26', NULL, NULL, 0, 0, 0),
(453, '52017O10MQX27N514I4S308ZIGU602', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'tarjeta', 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-27 14:08:02', '2017-10-27 02:10:02', NULL, NULL, 0, 0, 0),
(454, 'V2017T10SBE27GG12ZKKZ48O84Z300', 5, 1234, 'César', 'Vega Martín', 'cesar@euphorbia.es', '12345645', '601181340', '1988-06-20', '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, 'TEST', 'paypal', 1, 0, 0, NULL, 0, 800, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-27 12:48:00', '2017-10-27 02:10:10', NULL, NULL, 0, 0, 0),
(455, 'J2017A10RCX303L1131B137RJBKF01', 5, 1246, 'César', 'Vega Martín', 'cesar@euphorbia.es', '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-30 11:37:01', '2017-10-30 11:37:01', NULL, NULL, 0, 0, 0),
(456, 'F2017T10LB1303312V517179RALS40', 5, 1248, 'César', 'Vega Martín', 'cesar@euphorbia.es', '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-30 12:17:40', '2017-10-30 12:17:40', NULL, NULL, 0, 0, 0),
(457, 'P201751065131LC088KE629IL1S513', 5, 1256, 'César', 'Vega Martín', 'cesar@euphorbia.es', '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-31 08:29:13', '2017-10-31 08:29:13', NULL, NULL, 0, 0, 0),
(458, 'E2017E10WYQ31O008M2Z0590KKR239', 5, 1258, 'César', 'Vega Martín', 'cesar@euphorbia.es', '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-31 08:59:39', '2017-10-31 08:59:39', NULL, NULL, 0, 0, 0),
(459, 'M2017P10P1P31IQ11069A04N7TDL50', 5, 1262, 'César', 'Vega Martín', 'cesar@euphorbia.es', '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 161, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-31 11:04:50', '2017-10-31 11:28:21', NULL, NULL, 0, 0, 0),
(460, 'L2017Z10V8A31RY11TMBB52M6C8227', 5, 1265, 'César', 'Vega Martín', 'cesar@euphorbia.es', '12345645', '601181340', NULL, '12345645', 'Pepe Perez', NULL, 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 'Pepe Perez', 'c/ 1234', '4014', 'Valladolid', 49, 1, NULL, 0, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-31 11:52:27', '2017-10-31 11:52:27', NULL, NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders__articulos`
--

DROP TABLE IF EXISTS `orders__articulos`;
CREATE TABLE IF NOT EXISTS `orders__articulos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(10) UNSIGNED DEFAULT NULL,
  `articulo` int(11) DEFAULT NULL,
  `talla` varchar(3) CHARACTER SET utf8 DEFAULT NULL,
  `precio` float NOT NULL DEFAULT '0',
  `iva` float NOT NULL DEFAULT '0',
  `precio_iva` float NOT NULL DEFAULT '0',
  `unidades` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_order_2` (`id_order`,`articulo`,`talla`),
  KEY `id_order` (`id_order`),
  KEY `articulo` (`articulo`)
) ENGINE=InnoDB AUTO_INCREMENT=1035 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `orders__articulos`
--

INSERT INTO `orders__articulos` (`id`, `id_order`, `articulo`, `talla`, `precio`, `iva`, `precio_iva`, `unidades`, `created_at`, `updated_at`) VALUES
(846, 325, NULL, NULL, 39, 10, 3.54545, 2, '2017-09-13 10:22:01', '2017-09-13 10:24:56'),
(848, 326, NULL, NULL, 7.5, 10, 0.681818, 3, '2017-09-13 10:26:48', '2017-09-13 10:26:58'),
(849, 329, NULL, NULL, 9.5, 10, 0.863636, 1, '2017-09-14 05:07:59', '2017-09-14 05:18:45'),
(850, 329, NULL, NULL, 7.5, 10, 0.681818, 2, '2017-09-14 05:08:01', '2017-09-14 05:23:45'),
(851, 330, NULL, NULL, 9.5, 10, 0.863636, 1, '2017-09-14 06:11:31', '2017-09-14 06:11:31'),
(852, 330, NULL, NULL, 7.5, 10, 0.681818, 3, '2017-09-14 06:11:33', '2017-09-14 06:13:05'),
(853, 330, NULL, NULL, 7.5, 10, 0.681818, 1, '2017-09-14 06:11:49', '2017-09-14 06:11:49'),
(854, 331, NULL, NULL, 9.5, 10, 0.863636, 1, '2017-09-14 06:17:45', '2017-09-14 06:17:45'),
(855, 334, NULL, NULL, 4.5, 10, 0.409091, 1, '2017-09-14 07:47:53', '2017-09-14 07:47:53'),
(856, 339, NULL, NULL, 7.5, 10, 0.681818, 1, '2017-09-15 05:45:24', '2017-09-15 06:00:04'),
(857, 339, NULL, NULL, 9.5, 10, 0.863636, 1, '2017-09-15 05:56:18', '2017-09-15 05:56:18'),
(858, 360, NULL, NULL, 9.5, 10, 0.863636, 2, '2017-09-15 09:07:27', '2017-09-15 09:08:19'),
(859, 361, NULL, NULL, 9.5, 10, 0.863636, 1, '2017-09-15 09:33:07', '2017-09-15 09:33:07'),
(860, 363, NULL, NULL, 9.5, 10, 0.863636, 1, '2017-09-15 10:41:36', '2017-09-15 11:05:26'),
(861, 363, NULL, NULL, 7.5, 10, 0.681818, 3, '2017-09-15 10:41:47', '2017-09-15 11:05:26'),
(862, 385, 23, NULL, 0, 4, 0, 1, '2017-10-23 09:10:17', '2017-10-23 09:10:17'),
(863, 386, 25, NULL, 25, 4, 0.961538, 2, '2017-10-23 09:51:53', '2017-10-23 10:02:18'),
(864, 386, 26, NULL, 26, 4, 1, 1, '2017-10-23 09:51:54', '2017-10-23 09:51:54'),
(865, 387, 25, NULL, 25, 4, 0.961538, 5, '2017-10-23 10:17:51', '2017-10-23 10:21:54'),
(866, 387, 26, NULL, 26, 4, 1, 4, '2017-10-23 10:18:33', '2017-10-23 10:21:44'),
(867, 387, 30, NULL, 30, 4, 1.15385, 0, '2017-10-23 10:19:40', '2017-10-23 10:20:20'),
(868, 387, 31, NULL, 31, 4, 1.19231, 1, '2017-10-23 10:19:41', '2017-10-23 10:19:41'),
(869, 387, 24, NULL, 24, 4, 0.923077, 1, '2017-10-23 10:21:18', '2017-10-23 10:21:18'),
(876, 388, 25, NULL, 25, 4, 0.961538, 10, '2017-10-23 10:55:35', '2017-10-23 10:56:02'),
(877, 388, 26, NULL, 26, 4, 1, 1, '2017-10-23 10:56:02', '2017-10-23 10:56:02'),
(878, 389, 26, NULL, 26, 4, 1, 1, '2017-10-23 10:56:57', '2017-10-23 10:56:57'),
(879, 390, 34, NULL, 34, 4, 1.30769, 2, '2017-10-23 11:01:52', '2017-10-23 11:01:56'),
(880, 394, 25, NULL, 25, 4, 0.961538, 2, '2017-10-24 06:10:55', '2017-10-24 06:11:00'),
(881, 397, 25, NULL, 25, 4, 0.961538, 2, '2017-10-24 11:00:41', '2017-10-24 11:00:41'),
(882, 399, 26, NULL, 26, 4, 1, 1, '2017-10-25 07:35:13', '2017-10-25 07:35:13'),
(883, 400, 25, NULL, 25, 4, 0.961538, 1, '2017-10-25 08:20:23', '2017-10-25 08:20:23'),
(884, 435, 25, NULL, 25, 4, 0.961538, 2, '2017-10-26 08:39:55', '2017-10-26 09:05:16'),
(885, 436, 25, NULL, 25, 4, 0.961538, 2, '2017-10-26 09:28:54', '2017-10-26 09:36:39'),
(905, 439, 8, '', 8, 4, 0.32, 1, NULL, NULL),
(906, 439, 25, '', 25, 4, 1, 3, NULL, NULL),
(907, 440, 25, NULL, 25, 4, 0.961538, 3, '2017-10-27 04:13:24', '2017-10-27 04:14:29'),
(908, 441, 25, NULL, 25, 4, 0.961538, 2, '2017-10-27 04:15:37', '2017-10-27 04:15:41'),
(909, 442, 26, NULL, 26, 4, 1, 1, '2017-10-27 04:55:13', '2017-10-27 05:25:44'),
(910, 443, 25, NULL, 25, 4, 0.961538, 9999, '2017-10-27 05:26:59', '2017-10-27 05:44:19'),
(912, 445, 28, NULL, 28, 4, 1.07692, 10, '2017-10-27 06:40:00', '2017-10-27 06:47:16'),
(913, 446, 28, NULL, 28, 4, 1.07692, 2, '2017-10-27 06:51:58', '2017-10-27 06:52:03'),
(914, 447, 26, NULL, 26, 4, 1, 1, '2017-10-27 07:14:58', '2017-10-27 07:14:58'),
(919, 448, 26, '', 26, 4, 1.04, 1, NULL, NULL),
(955, 450, 26, '', 26, 4, 1.04, 1, NULL, NULL),
(1002, 451, 35, '', 35, 4, 1.4, 2, NULL, NULL),
(1018, 452, 26, '', 26, 4, 1.04, 2, NULL, NULL),
(1031, 454, 26, '', 26, 4, 1.04, 20, NULL, NULL),
(1032, 454, 28, '', 28, 4, 1.12, 10, NULL, NULL),
(1033, 459, 26, NULL, 26, 4, 1, 5, '2017-10-31 10:04:53', '2017-10-31 10:24:26'),
(1034, 459, 31, NULL, 31, 4, 1.19231, 1, '2017-10-31 10:08:48', '2017-10-31 10:28:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders__refunds`
--

DROP TABLE IF EXISTS `orders__refunds`;
CREATE TABLE IF NOT EXISTS `orders__refunds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `NPedidoWeb` int(11) NOT NULL,
  `RefNum` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `Cliente` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `direccion` varchar(40) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `localidad` varchar(40) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `codigo_postal` varchar(20) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `pais` int(4) NOT NULL DEFAULT '0',
  `zona` int(4) NOT NULL DEFAULT '0',
  `metodo_pago` varchar(20) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `transportista` varchar(5) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `comentario` text COLLATE utf8_spanish2_ci,
  `mail` tinyint(1) NOT NULL DEFAULT '0',
  `creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders__request`
--

DROP TABLE IF EXISTS `orders__request`;
CREATE TABLE IF NOT EXISTS `orders__request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `response` text CHARACTER SET utf8,
  `modification` datetime DEFAULT NULL,
  `creacion` datetime NOT NULL,
  `id_order` int(10) UNSIGNED DEFAULT NULL,
  `historico` tinyint(1) NOT NULL DEFAULT '0',
  `tipo` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `articulos` int(11) NOT NULL DEFAULT '0',
  `pvp_articulos` float NOT NULL DEFAULT '0',
  `pvp_total` float NOT NULL DEFAULT '0',
  `pvp_envio` float NOT NULL DEFAULT '0',
  `pvp_iva` float NOT NULL DEFAULT '0',
  `pvp_descuento` float NOT NULL DEFAULT '0',
  `pvp_pago` float NOT NULL DEFAULT '0',
  `pvp_tarjeta` float NOT NULL DEFAULT '0',
  `pvp_envolver` float NOT NULL DEFAULT '0',
  `pvp_puntos` int(11) NOT NULL DEFAULT '0',
  `errors` text COLLATE utf8_spanish2_ci,
  PRIMARY KEY (`id`),
  KEY `id_order` (`id_order`)
) ENGINE=InnoDB AUTO_INCREMENT=210000421 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `orders__request`
--

INSERT INTO `orders__request` (`id`, `response`, `modification`, `creacion`, `id_order`, `historico`, `tipo`, `articulos`, `pvp_articulos`, `pvp_total`, `pvp_envio`, `pvp_iva`, `pvp_descuento`, `pvp_pago`, `pvp_tarjeta`, `pvp_envolver`, `pvp_puntos`, `errors`) VALUES
(210000379, '{\"Ds_Date\":\"17\\/04\\/2017\",\"Ds_Hour\":\"12:47\",\"Ds_SecurePayment\":\"1\",\"Ds_Card_Country\":\"724\",\"Ds_Amount\":\"3000\",\"Ds_Currency\":\"978\",\"Ds_Order\":\"210000379\",\"Ds_MerchantCode\":\"336054614\",\"Ds_Terminal\":\"002\",\"Ds_Response\":\"0000\",\"Ds_MerchantData\":\"\",\"Ds_TransactionType\":\"0\",\"Ds_ConsumerLanguage\":\"1\",\"Ds_AuthorisationCode\":\"057678\"}', NULL, '2017-04-17 12:47:41', 331, 1, 'tarjeta', 1, 30, 30, 0, 6.3, 0, 0, 0, 0, 0, NULL),
(210000380, '{\"Ds_Date\":\"17\\/04\\/2017\",\"Ds_Hour\":\"12:51\",\"Ds_SecurePayment\":\"1\",\"Ds_Card_Country\":\"724\",\"Ds_Amount\":\"4480\",\"Ds_Currency\":\"978\",\"Ds_Order\":\"210000380\",\"Ds_MerchantCode\":\"336054614\",\"Ds_Terminal\":\"002\",\"Ds_Response\":\"0000\",\"Ds_MerchantData\":\"\",\"Ds_TransactionType\":\"0\",\"Ds_ConsumerLanguage\":\"1\",\"Ds_AuthorisationCode\":\"057683\"}', NULL, '2017-04-17 12:51:40', 331, 1, 'tarjeta', 1, 44.8, 44.8, 0, 9.408, 0, 0, 0, 0, 0, NULL),
(210000381, NULL, NULL, '0000-00-00 00:00:00', 439, 0, 'tarjeta', 1, 50, 54.98, 4.98, 1, 0, 0, 0, 0, 0, NULL),
(210000382, NULL, NULL, '0000-00-00 00:00:00', 439, 0, 'tarjeta', 1, 50, 54.98, 4.98, 1, 0, 0, 0, 0, 0, NULL),
(210000383, NULL, NULL, '0000-00-00 00:00:00', 439, 0, 'tarjeta', 2, 83, 83, 0, 1, 0, 0, 0, 0, 0, NULL),
(210000384, NULL, NULL, '0000-00-00 00:00:00', 439, 0, 'tarjeta', 2, 83, 83, 0, 1, 0, 0, 0, 0, 0, NULL),
(210000385, NULL, NULL, '0000-00-00 00:00:00', 439, 0, 'tarjeta', 2, 83, 83, 0, 1, 0, 0, 0, 0, 0, NULL),
(210000386, NULL, NULL, '0000-00-00 00:00:00', 439, 0, 'tarjeta', 2, 83, 83, 0, 1, 0, 0, 0, 0, 0, NULL),
(210000387, NULL, NULL, '0000-00-00 00:00:00', 448, 0, 'tarjeta', 1, 26, 26, 0, 1.04, 0, 0, 0, 0, 0, NULL),
(210000388, NULL, NULL, '0000-00-00 00:00:00', 450, 0, 'tarjeta', 1, 26, 26, 0, 1.04, 0, 0, 0, 0, 0, NULL),
(210000389, NULL, NULL, '0000-00-00 00:00:00', 450, 0, 'tarjeta', 1, 0, 0, 0, 1.04, 0, 0, 0, 0, 0, NULL),
(210000390, NULL, NULL, '0000-00-00 00:00:00', 450, 0, 'tarjeta', 1, 0, 0, 0, 1.04, 0, 0, 0, 0, 0, NULL),
(210000391, NULL, NULL, '0000-00-00 00:00:00', 450, 0, 'tarjeta', 1, 0, 0, 0, 1.04, 0, 0, 0, 0, 0, NULL),
(210000392, NULL, NULL, '0000-00-00 00:00:00', 450, 0, 'tarjeta', 1, 0, 0, 0, 1.04, 0, 0, 0, 0, 0, NULL),
(210000393, NULL, NULL, '0000-00-00 00:00:00', 450, 0, 'tarjeta', 1, 0, 0, 0, 1.04, 0, 0, 0, 0, 0, NULL),
(210000394, NULL, NULL, '0000-00-00 00:00:00', 450, 0, 'tarjeta', 1, 26, 26, 0, 1.04, 0, 0, 0, 0, 0, NULL),
(210000395, NULL, NULL, '0000-00-00 00:00:00', 451, 0, 'tarjeta', 1, 70, 70, 0, 1.4, 0, 0, 0, 0, 0, NULL),
(210000396, NULL, NULL, '0000-00-00 00:00:00', 451, 0, 'tarjeta', 1, 70, 70, 0, 1.4, 0, 0, 0, 0, 0, NULL),
(210000397, NULL, NULL, '0000-00-00 00:00:00', 451, 0, 'tarjeta', 1, 70, 70, 0, 1.4, 0, 0, 0, 0, 0, NULL),
(210000398, NULL, NULL, '0000-00-00 00:00:00', 451, 0, 'tarjeta', 1, 70, 70, 0, 1.4, 0, 0, 0, 0, 0, NULL),
(210000399, NULL, NULL, '0000-00-00 00:00:00', 451, 0, 'tarjeta', 1, 70, 70, 0, 1.4, 0, 0, 0, 0, 0, NULL),
(210000400, NULL, NULL, '0000-00-00 00:00:00', 451, 0, 'tarjeta', 1, 70, 70, 0, 1.4, 0, 0, 0, 0, 0, NULL),
(210000401, NULL, NULL, '0000-00-00 00:00:00', 451, 0, 'tarjeta', 1, 70, 70, 0, 1.4, 0, 0, 0, 0, 0, NULL),
(210000402, NULL, NULL, '0000-00-00 00:00:00', 451, 0, 'tarjeta', 1, 70, 70, 0, 1.4, 0, 0, 0, 0, 0, NULL),
(210000403, NULL, NULL, '0000-00-00 00:00:00', 451, 0, 'tarjeta', 1, 70, 70, 0, 1.4, 0, 0, 0, 0, 0, NULL),
(210000404, NULL, NULL, '0000-00-00 00:00:00', 451, 0, 'tarjeta', 1, 70, 70, 0, 1.4, 0, 0, 0, 0, 0, NULL),
(210000405, NULL, NULL, '0000-00-00 00:00:00', 451, 0, 'tarjeta', 1, 70, 70, 0, 1.4, 0, 0, 0, 0, 0, NULL),
(210000406, NULL, NULL, '0000-00-00 00:00:00', 451, 0, 'tarjeta', 1, 70, 70, 0, 1.4, 0, 0, 0, 0, 0, NULL),
(210000407, NULL, NULL, '0000-00-00 00:00:00', 451, 0, 'tarjeta', 1, 70, 70, 0, 1.4, 0, 0, 0, 0, 0, NULL),
(210000408, NULL, NULL, '0000-00-00 00:00:00', 451, 0, 'tarjeta', 1, 70, 70, 0, 1.4, 0, 0, 0, 0, 0, NULL),
(210000409, NULL, NULL, '0000-00-00 00:00:00', 451, 0, 'tarjeta', 1, 70, 70, 0, 1.4, 0, 0, 0, 0, 0, NULL),
(210000410, NULL, NULL, '0000-00-00 00:00:00', 451, 0, 'tarjeta', 1, 70, 70, 0, 1.4, 0, 0, 0, 0, 0, NULL),
(210000411, NULL, NULL, '0000-00-00 00:00:00', 451, 0, 'tarjeta', 1, 70, 70, 0, 1.4, 0, 0, 0, 0, 0, NULL),
(210000412, NULL, NULL, '0000-00-00 00:00:00', 451, 0, 'tarjeta', 1, 70, 70, 0, 1.4, 0, 0, 0, 0, 0, NULL),
(210000413, NULL, NULL, '0000-00-00 00:00:00', 451, 0, 'tarjeta', 1, 70, 70, 0, 1.4, 0, 0, 0, 0, 0, NULL),
(210000414, NULL, NULL, '0000-00-00 00:00:00', 451, 0, 'tarjeta', 1, 70, 70, 0, 1.4, 0, 0, 0, 0, 0, NULL),
(210000415, NULL, NULL, '0000-00-00 00:00:00', 451, 0, 'tarjeta', 1, 70, 70, 0, 1.4, 0, 0, 0, 0, 0, NULL),
(210000416, NULL, NULL, '0000-00-00 00:00:00', 452, 0, 'tarjeta', 1, 52, 52, 0, 1.04, 0, 0, 0, 0, 0, NULL),
(210000417, NULL, NULL, '0000-00-00 00:00:00', 454, 0, 'tarjeta', 1, 26, 26, 0, 1.04, 0, 0, 0, 0, 0, NULL),
(210000418, NULL, NULL, '0000-00-00 00:00:00', 454, 0, 'paypal', 1, 26, 26, 0, 1.04, 0, 0, 0, 0, 0, NULL),
(210000419, NULL, NULL, '0000-00-00 00:00:00', 454, 0, 'paypal', 1, 26, 26, 0, 1.04, 0, 0, 0, 0, 0, NULL),
(210000420, NULL, NULL, '0000-00-00 00:00:00', 454, 0, 'paypal', 2, 800, 800, 0, 1.12, 0, 0, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_requests`
--

DROP TABLE IF EXISTS `password_requests`;
CREATE TABLE IF NOT EXISTS `password_requests` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `token` varchar(128) COLLATE utf8_spanish2_ci NOT NULL,
  `date` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `password_requests`
--

INSERT INTO `password_requests` (`id`, `email`, `token`, `date`, `updated_at`) VALUES
(1, '', 'A20176109A125WL14UWMK351SYW615', '2017-10-25 14:35:15', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portes__paises`
--

DROP TABLE IF EXISTS `portes__paises`;
CREATE TABLE IF NOT EXISTS `portes__paises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(5) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `code` varchar(2) CHARACTER SET latin1 DEFAULT NULL,
  `nombre` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `nombre_idioma` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '0',
  `envio` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `locale` (`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `portes__paises`
--

INSERT INTO `portes__paises` (`id`, `locale`, `code`, `nombre`, `nombre_idioma`, `activo`, `envio`, `created_at`, `updated_at`) VALUES
(1, 'es_ES', 'es', 'Español', 'Español', 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portes__provincias`
--

DROP TABLE IF EXISTS `portes__provincias`;
CREATE TABLE IF NOT EXISTS `portes__provincias` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_pais` int(11) NOT NULL,
  `nombre` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre_idioma` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '0',
  `envio` tinyint(1) NOT NULL DEFAULT '0',
  `code` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `portes__provincias`
--

INSERT INTO `portes__provincias` (`id`, `id_pais`, `nombre`, `nombre_idioma`, `activo`, `envio`, `code`, `created_at`, `updated_at`) VALUES
(1, 1, 'Álava', 'Álava', 1, 1, 'alava', NULL, NULL),
(2, 1, 'Albacete', 'Albacete', 1, 1, 'albacete', NULL, NULL),
(3, 1, 'Alicante', 'Alicante', 1, 1, 'alicante', NULL, NULL),
(4, 1, 'Almería', 'Almería', 1, 1, 'almeria', NULL, NULL),
(5, 1, 'Asturias', 'Asturias', 1, 1, 'asturias', NULL, NULL),
(6, 1, 'Ávila', 'Ávila', 1, 1, 'avila', NULL, NULL),
(7, 1, 'Badajoz', 'Badajoz', 1, 1, 'badajoz', NULL, NULL),
(8, 1, 'Barcelona', 'Barcelona', 1, 1, 'barcelona', NULL, NULL),
(9, 1, 'Burgos', 'Burgos', 1, 1, 'burgos', NULL, NULL),
(10, 1, 'Cáceres', 'Cáceres', 1, 1, 'caceres', NULL, NULL),
(11, 1, 'Cádiz', 'Cádiz', 1, 1, 'cadiz', NULL, NULL),
(12, 1, 'Cantabria', 'Cantabria', 1, 1, 'cantabria', NULL, NULL),
(13, 1, 'Castellón', 'Castellón', 1, 1, 'castellon', NULL, NULL),
(14, 1, 'Ceuta', 'Ceuta', 1, 1, 'ceuta', NULL, NULL),
(15, 1, 'Ciudad Real', 'Ciudad Real', 1, 1, 'ciudad_real', NULL, NULL),
(16, 1, 'Córdoba', 'Córdoba', 1, 1, 'cordoba', NULL, NULL),
(17, 1, 'Cuenca', 'Cuenca', 1, 1, 'cuenca', NULL, NULL),
(18, 1, 'Gerona', 'Gerona', 1, 1, 'gerona', NULL, NULL),
(19, 1, 'Granada', 'Granada', 1, 1, 'granada', NULL, NULL),
(20, 1, 'Guadalajara', 'Guadalajara', 1, 1, 'guadalajara', NULL, NULL),
(21, 1, 'Guipúzcoa', 'Guipúzcoa', 1, 1, 'guipuzcoa', NULL, NULL),
(22, 1, 'Huelva', 'Huelva', 1, 1, 'huelva', NULL, NULL),
(23, 1, 'Huesca', 'Huesca', 1, 1, 'huesca', NULL, NULL),
(24, 1, 'Islas Baleares', 'Islas Baleares', 1, 1, 'islas_baleares', NULL, NULL),
(25, 1, 'Jaén', 'Jaén', 1, 1, 'jaen', NULL, NULL),
(26, 1, 'La Coruña', 'La Coruña', 1, 1, 'la_coruña', NULL, NULL),
(27, 1, 'La Rioja', 'La Rioja', 1, 1, 'la_rioja', NULL, NULL),
(28, 1, 'Las Palmas', 'Las Palmas', 1, 1, 'las_palmas', NULL, NULL),
(29, 1, 'León', 'León', 1, 1, 'leon', NULL, NULL),
(30, 1, 'Lérida', 'Lérida', 1, 1, 'lleida', NULL, NULL),
(31, 1, 'Lugo', 'Lugo', 1, 1, 'lugo', NULL, NULL),
(32, 1, 'Madrid', 'Madrid', 1, 1, 'madrid', NULL, NULL),
(33, 1, 'Málaga', 'Málaga', 1, 1, 'malaga', NULL, NULL),
(34, 1, 'Melilla', 'Melilla', 1, 1, 'melilla', NULL, NULL),
(35, 1, 'Murcia', 'Murcia', 1, 1, 'murcia', NULL, NULL),
(36, 1, 'Navarra', 'Navarra', 1, 1, 'navarra', NULL, NULL),
(37, 1, 'Orense', 'Orense', 1, 1, 'orense', NULL, NULL),
(38, 1, 'Palencia', 'Palencia', 1, 1, 'palencia', NULL, NULL),
(39, 1, 'Pontevedra', 'Pontevedra', 1, 1, 'pontevedra', NULL, NULL),
(40, 1, 'Salamanca', 'Salamanca', 1, 1, 'salamanca', NULL, NULL),
(41, 1, 'Segovia', 'Segovia', 1, 1, 'segovia', NULL, NULL),
(42, 1, 'Sevilla', 'Sevilla', 1, 1, 'sevilla', NULL, NULL),
(43, 1, 'Soria', 'Soria', 1, 1, 'soria', NULL, NULL),
(44, 1, 'Tarragona', 'Tarragona', 1, 1, 'tarragona', NULL, NULL),
(45, 1, 'Tenerife', 'Tenerife', 1, 1, 'tenerife', NULL, NULL),
(46, 1, 'Teruel', 'Teruel', 1, 1, 'teruel', NULL, NULL),
(47, 1, 'Toledo', 'Toledo', 1, 1, 'toledo', NULL, NULL),
(48, 1, 'Valencia', 'Valencia', 1, 1, 'valencia', NULL, NULL),
(49, 1, 'Valladolid', 'Valladolid', 1, 1, 'valladolid', NULL, NULL),
(50, 1, 'Vizcaya', 'Vizcaya', 1, 1, 'vizcaya', NULL, NULL),
(51, 1, 'Zamora', 'Zamora', 1, 1, 'zamora', NULL, NULL),
(52, 1, 'Zaragoza', 'Zaragoza', 1, 1, 'zaragoza', NULL, NULL),
(53, 2, 'Aveiro', 'Aveiro', 0, 0, 'aveiro', NULL, NULL),
(54, 2, 'Beja', 'Beja', 0, 0, 'beja', NULL, NULL),
(55, 2, 'Braga', 'Braga', 0, 0, 'braga', NULL, NULL),
(56, 2, 'Bragança', 'Bragança', 0, 0, 'braganca', NULL, NULL),
(57, 2, 'Castelo Branco', 'Castelo Branco', 0, 0, 'castelo_branco', NULL, NULL),
(58, 2, 'Coimbra', 'Coimbra', 0, 0, 'coimbra', NULL, NULL),
(59, 2, 'Évora', 'Évora', 0, 0, 'evora', NULL, NULL),
(60, 2, 'Faro', 'Faro', 0, 0, 'faro', NULL, NULL),
(61, 2, 'Guarda', 'Guarda', 0, 0, 'guarda', NULL, NULL),
(62, 2, 'Leiria', 'Leiria', 0, 0, 'leiria', NULL, NULL),
(63, 2, 'Lisboa', 'Lisboa', 0, 0, 'lisboa', NULL, NULL),
(64, 2, 'Portalegre', 'Portalegre', 0, 0, 'portalegre', NULL, NULL),
(65, 2, 'Porto', 'Porto', 0, 0, 'porto', NULL, NULL),
(66, 2, 'Santarém', 'Santarém', 0, 0, 'santarem', NULL, NULL),
(67, 2, 'Setúbal', 'Setúbal', 0, 0, 'setubal', NULL, NULL),
(68, 2, 'Viana do Castelo', 'Viana do Castelo', 0, 0, 'viana_do_castelo', NULL, NULL),
(69, 2, 'Vila Real', 'Vila Real', 0, 0, 'vila_real', NULL, NULL),
(70, 2, 'Viseu', 'Viseu', 0, 0, 'viseu', NULL, NULL),
(71, 2, 'Açores', 'Açores', 0, 0, 'azores', NULL, NULL),
(72, 2, 'Madeira', 'Madeira', 0, 0, 'madeira', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portes__zonas`
--

DROP TABLE IF EXISTS `portes__zonas`;
CREATE TABLE IF NOT EXISTS `portes__zonas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pais` varchar(4) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `id_provincia` varchar(4) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `id_transportista` varchar(5) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `pvp` float DEFAULT NULL,
  `pvp_rebaja` float DEFAULT NULL,
  `pvp_devolver` float DEFAULT NULL,
  `limite_rebaja` float DEFAULT NULL,
  `is_rebaja` tinyint(1) NOT NULL DEFAULT '0',
  `dias_entrega` int(2) UNSIGNED NOT NULL DEFAULT '2',
  `activo` tinyint(1) NOT NULL DEFAULT '0',
  `envio` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` mediumint(9) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cod_pais` (`id_pais`,`id_provincia`,`id_transportista`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `portes__zonas`
--

INSERT INTO `portes__zonas` (`id`, `id_pais`, `id_provincia`, `id_transportista`, `pvp`, `pvp_rebaja`, `pvp_devolver`, `limite_rebaja`, `is_rebaja`, `dias_entrega`, `activo`, `envio`, `created_at`, `updated_at`) VALUES
(1, '', NULL, '', 0, 0, 0, 0, 0, 2, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preferences`
--

DROP TABLE IF EXISTS `preferences`;
CREATE TABLE IF NOT EXISTS `preferences` (
  `id` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `text_1` text COLLATE utf8_spanish2_ci NOT NULL,
  `guarded` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `preferences`
--

INSERT INTO `preferences` (`id`, `text_1`, `guarded`, `created_at`, `updated_at`) VALUES
('analytics_google_analytics', '', 0, NULL, NULL),
('contacto_direccion', '<p>asdasd</p>', 0, NULL, NULL),
('contacto_email', 'cesar@euphorbia.es', 0, NULL, NULL),
('contacto_facebook', '', 0, NULL, NULL),
('contacto_instagram', 'asd', 0, NULL, NULL),
('contacto_nombre', 'Lorena Euphorbia', 0, NULL, NULL),
('contacto_twitter', 'asd', 0, NULL, NULL),
('contacto_youtube', 'asd', 0, NULL, NULL),
('forms_email', 'cesar@euphorbia.es', 1, NULL, NULL),
('forms_smtp_auth', '1', 1, NULL, NULL),
('forms_smtp_password', 'Cfrw2#39', 1, NULL, NULL),
('forms_smtp_port', '25', 1, NULL, NULL),
('forms_smtp_security', '', 1, NULL, NULL),
('forms_smtp_server', 'mail.en-pruebas.com', 1, NULL, NULL),
('forms_smtp_user', 'formularios@en-pruebas.com', 1, NULL, NULL),
('general_meta_description', 'Más de 20 años en distribución de todo tipo de productos alimenticios en Castilla y León y Cantabria.', 0, NULL, NULL),
('general_meta_title', 'Distribuidor de alimentos en Castilla y León', 0, NULL, NULL),
('general_titulo_web', 'Alfonso Peña :: Comercial de productos alimenticios', 0, NULL, NULL),
('idiomas_contacto_texto', '<p>Oficinas centrales:<br />\r\nAv. de la Técnica, 33,<br />\r\n28522 Rivas-Vaciamadrid, Madrid</p>\r\n<p>&nbsp;</p>\r\n<p>Teléfono:<br />\r\n801 525 661<br />\r\ninfo@tsrwind.com</p>', 0, NULL, NULL),
('idiomas_contacto_texto_en', '<p>Oficinas centrales:<br />\r\nAv. de la Técnica, 33,<br />\r\n28522 Rivas-Vaciamadrid, Madrid</p>\r\n<p>&nbsp;</p>\r\n<p>Teléfono:<br />\r\n801 525 661<br />\r\ninfo@tsrwind.com</p>', 0, NULL, NULL),
('idiomas_contacto_titulo', 'Contacto', 0, NULL, NULL),
('idiomas_contacto_titulo_en', 'Contacto', 0, NULL, NULL),
('idiomas_equipo_titulo', 'Equipo', 0, NULL, NULL),
('idiomas_footer_texto1', 'Nuestra empresa cuenta con el respaldo y la financiación de la Empresa Nacional de Innovación dependiente del Ministerio de Industria, Energía y Turismo del Gobierno de España.', 0, NULL, NULL),
('idiomas_footer_texto1_en', 'Nuestra empresa cuenta con el respaldo y la financiación de la Empresa Nacional de Innovación dependiente del Ministerio de Industria, Energía y Turismo del Gobierno de España.', 0, NULL, NULL),
('idiomas_footer_texto2', 'Our company has been elected by the Horizon 2020, the biggest EU Research and Innovation programme ever.', 0, NULL, NULL),
('idiomas_footer_texto2_en', 'Our company has been elected by the Horizon 2020, the biggest EU Research and Innovation programme ever.', 0, NULL, NULL),
('idiomas_home_boton_entradilla2', 'ACCESO A CLIENTES', 0, NULL, NULL),
('idiomas_home_boton_entradilla2_en', 'ACCESO A CLIENTES', 0, NULL, NULL),
('idiomas_home_boton_noticias', 'VER TODAS', 0, NULL, NULL),
('idiomas_home_boton_noticias_en', 'VER TODAS', 0, NULL, NULL),
('idiomas_home_boton_servicios1', 'Conocelos', 0, NULL, NULL),
('idiomas_home_boton_servicios1_en', 'Conocelos', 0, NULL, NULL),
('idiomas_home_subtitulo_banner1', 'El sistema de inspección de palas más avanzado del mundo.', 0, NULL, NULL),
('idiomas_home_subtitulo_banner1_en', 'El sistema de inspección de palas más avanzado del mundo.', 0, NULL, NULL),
('idiomas_home_texto_colaboradores', 'COLABORADORES', 0, NULL, NULL),
('idiomas_home_texto_colaboradores_en', 'COLABORADORES', 0, NULL, NULL),
('idiomas_home_texto_entradilla1', 'Formada por ingenieros de los sectores de las renovables y la robótica, TSR Wind es una start-up especializada en el tratamiento de palas de aerogeneradores que ha revolucionado el mercado con su innovador sistema de robots magnéticos.', 0, NULL, NULL),
('idiomas_home_texto_entradilla1_en', 'Formada por ingenieros de los sectores de las renovables y la robótica, TSR Wind es una start-up especializada en el tratamiento de palas de aerogeneradores que ha revolucionado el mercado con su innovador sistema de robots magnéticos.', 0, NULL, NULL),
('idiomas_home_texto_entradilla2', 'Una plataforma web en la que el cliente puede acceder a toda la información sobre el estado de sus palas: fotografías, informes, etc.', 0, NULL, NULL),
('idiomas_home_texto_entradilla2_en', 'Una plataforma web en la que el cliente puede acceder a toda la información sobre el estado de sus palas: fotografías, informes, etc.', 0, NULL, NULL),
('idiomas_home_texto_productos1', 'Inspección fotográfica de palas de alta definición', 0, NULL, NULL),
('idiomas_home_texto_productos1_en', 'Inspección fotográfica de palas de alta definición', 0, NULL, NULL),
('idiomas_home_texto_productos2', 'Limpieza integral de aerogeneradores', 0, NULL, NULL),
('idiomas_home_texto_productos2_en', 'Limpieza integral de aerogeneradores', 0, NULL, NULL),
('idiomas_home_texto_servicios1', 'El objetivo de TSR es optimizar y rentabilzar al máximo el servicio de mantenimiento de los aerogeneradores a través de la tecnología y la robótica.\r\n\r\n', 0, NULL, NULL),
('idiomas_home_texto_servicios1_en', 'El objetivo de TSR es optimizar y rentabilzar al máximo el servicio de mantenimiento de los aerogeneradores a través de la tecnología y la robótica.\r\n\r\n', 0, NULL, NULL),
('idiomas_home_titulo_banner1', 'Siempre a favor del viento', 0, NULL, NULL),
('idiomas_home_titulo_banner1_en', 'Siempre a favor del viento', 0, NULL, NULL),
('idiomas_home_titulo_entradilla1', 'INGENIERÍA Y ROBÓTICA\r\nAL SERVICIO DEL SECTOR EÓLICO.', 0, NULL, NULL),
('idiomas_home_titulo_entradilla1_en', 'INGENIERÍA Y ROBÓTICA\r\nAL SERVICIO DEL SECTOR EÓLICO.', 0, NULL, NULL),
('idiomas_home_titulo_entradilla2', 'TSR INSPECTOR.', 0, NULL, NULL),
('idiomas_home_titulo_entradilla2_en', 'TSR INSPECTOR.', 0, NULL, NULL),
('idiomas_home_titulo_noticias', 'NOTICIAS TRS WIND', 0, NULL, NULL),
('idiomas_home_titulo_noticias_en', 'NOTICIAS TRS WIND', 0, NULL, NULL),
('idiomas_home_titulo_productos1', 'EOLOS', 0, NULL, NULL),
('idiomas_home_titulo_productos1_en', 'EOLOS', 0, NULL, NULL),
('idiomas_home_titulo_productos2', 'ARGOS', 0, NULL, NULL),
('idiomas_home_titulo_productos2_en', 'ARGOS', 0, NULL, NULL),
('idiomas_home_titulo_servicios1', 'SERVICIOS', 0, NULL, NULL),
('idiomas_home_titulo_servicios1_en', 'SERVICIOS', 0, NULL, NULL),
('idiomas_noticias_leermas', 'Leer', 0, NULL, NULL),
('idiomas_noticias_leermas_en', 'Read', 0, NULL, NULL),
('idiomas_noticias_titulo', 'Noticias', 0, NULL, NULL),
('idiomas_noticias_titulo_en', 'News', 0, NULL, NULL),
('idiomas_noticias_titulo1', 'NOTICIAS TSR WIND', 0, NULL, NULL),
('idiomas_noticias_titulo1_en', 'NOTICIAS TSR WIND', 0, NULL, NULL),
('idiomas_politicadecalidad_texto1', '<p>es</p>', 0, NULL, NULL),
('idiomas_politicadecalidad_texto1_en', '<p>en</p>', 0, NULL, NULL),
('idiomas_politicadecalidad_titulo', 'Política de calidad', 0, NULL, NULL),
('idiomas_politicadecalidad_titulo_en', 'Política de calidad', 0, NULL, NULL),
('idiomas_productos_subtitulo_producto1', 'Inspección fotográfica de palas en alta definición', 0, NULL, NULL),
('idiomas_productos_subtitulo_producto1_en', 'Inspección fotográfica de palas en alta definición', 0, NULL, NULL),
('idiomas_productos_subtitulo_producto2', 'Limpieza integral de aerogeneradores', 0, NULL, NULL),
('idiomas_productos_subtitulo_producto2_en', 'Limpieza integral de aerogeneradores', 0, NULL, NULL),
('idiomas_productos_texto_gama', 'GAMA', 0, NULL, NULL),
('idiomas_productos_texto_gama_en', 'GAMA', 0, NULL, NULL),
('idiomas_productos_texto_producto1', 'La gama de robots EOLOS está compuesta por una serie de robots magnéticos que se adhieren a la superficie metálica de las torres mediante imanes premanentes de neodimio, lo que les permite alcanzar la altura óptima para tomar fotos.\r\nLos robots EOLOS incorporan una cámara de alta resolución y un objetivo sobre una muñeca pan&tilt con dos grados de libertad que permite realizar fotografías y videos de alta resolución de cualquier punto de las palas.', 0, NULL, NULL),
('idiomas_productos_texto_producto1_en', 'La gama de robots EOLOS está compuesta por una serie de robots magnéticos que se adhieren a la superficie metálica de las torres mediante imanes premanentes de neodimio, lo que les permite alcanzar la altura óptima para tomar fotos.\r\nLos robots EOLOS incorporan una cámara de alta resolución y un objetivo sobre una muñeca pan&tilt con dos grados de libertad que permite realizar fotografías y videos de alta resolución de cualquier punto de las palas.', 0, NULL, NULL),
('idiomas_productos_texto_producto2', 'Concebida para realizar trabajos pesados, la gama de robots Argos está basada en una plataforma de acople magnético de gran movilidad. Ideal para los trabajos de limpieza, tanto de torre como de plas, su principal característica es su versatilidad y capacidad de adaptación, lo que le permite incorporar distintos dispositivos según el servicio a realizar. Por ejemplo, para trabajos de limpieza puede incorporar desde cañones de agua a cepillos rotativos.', 0, NULL, NULL),
('idiomas_productos_texto_producto2_en', 'Concebida para realizar trabajos pesados, la gama de robots Argos está basada en una plataforma de acople magnético de gran movilidad. Ideal para los trabajos de limpieza, tanto de torre como de plas, su principal característica es su versatilidad y capacidad de adaptación, lo que le permite incorporar distintos dispositivos según el servicio a realizar. Por ejemplo, para trabajos de limpieza puede incorporar desde cañones de agua a cepillos rotativos.', 0, NULL, NULL),
('idiomas_productos_titulo_producto1', 'Eolos', 0, NULL, NULL),
('idiomas_productos_titulo_producto1_en', 'Eolos', 0, NULL, NULL),
('idiomas_productos_titulo_producto2', 'Argos', 0, NULL, NULL),
('idiomas_productos_titulo_producto2_en', 'Argos', 0, NULL, NULL),
('idiomas_servicios_grupo1_texto1', 'EOLOS 200 incorpora una cámara de alta resolución con la que lleva a cabo el servicio de inspección, obteniendo fotografías de gran calidad y máximo detalle que nos ofrecen el estado completo de la pala así como imágenes de detalle de los daños detectados.', 0, NULL, NULL),
('idiomas_servicios_grupo1_texto1_en', 'EOLOS 200 incorpora una cámara de alta resolución con la que lleva a cabo el servicio de inspección, obteniendo fotografías de gran calidad y máximo detalle que nos ofrecen el estado completo de la pala así como imágenes de detalle de los daños detectados.', 0, NULL, NULL),
('idiomas_servicios_grupo1_texto2', 'La información completa de cada pala queda a disposición del cliente en nuestro servidor para su consulta. Esto permite conocer la evolución del estado de las palas dándole un enorme valor añadido al servicio prestado por TSR Wind.', 0, NULL, NULL),
('idiomas_servicios_grupo1_texto2_en', 'La información completa de cada pala queda a disposición del cliente en nuestro servidor para su consulta. Esto permite conocer la evolución del estado de las palas dándole un enorme valor añadido al servicio prestado por TSR Wind.', 0, NULL, NULL),
('idiomas_servicios_grupo1_texto3', 'Un solo operario desde tierra monitoriza y dirige la actividad del robot dirigiendo con precisión las operaciones. Esto permite substituir el uso de plataformas elevadoras o trabajos verticales con lo que ello supone en reducción de riesgos laborales.', 0, NULL, NULL),
('idiomas_servicios_grupo1_texto3_en', 'Un solo operario desde tierra monitoriza y dirige la actividad del robot dirigiendo con precisión las operaciones. Esto permite substituir el uso de plataformas elevadoras o trabajos verticales con lo que ello supone en reducción de riesgos laborales.', 0, NULL, NULL),
('idiomas_servicios_grupo1_titulo', 'INSPECCIÓN DE PALAS', 0, NULL, NULL),
('idiomas_servicios_grupo1_titulo_en', 'INSPECCIÓN DE PALAS', 0, NULL, NULL),
('idiomas_servicios_grupo1_titulo1', 'CALIDAD FOTOGRÁFICA', 0, NULL, NULL),
('idiomas_servicios_grupo1_titulo1_en', 'CALIDAD FOTOGRÁFICA', 0, NULL, NULL),
('idiomas_servicios_grupo1_titulo2', 'INFORMACIÓN', 0, NULL, NULL),
('idiomas_servicios_grupo1_titulo2_en', 'INFORMACIÓN', 0, NULL, NULL),
('idiomas_servicios_grupo1_titulo3', 'SEGURIDAD', 0, NULL, NULL),
('idiomas_servicios_grupo1_titulo3_en', 'SEGURIDAD', 0, NULL, NULL),
('idiomas_servicios_grupo2_texto1', 'Argos, el robot especializado en labores de limpieza (tanto de torre como de palas) opera mediante un módulo especialmente diseñado con una con guración de boquillas de alta presión. Su capacidad de adaptación le permite incorporar distintos dispositivos según el servicio a realizar desde cañones de agua a cepillos rotativos.', 0, NULL, NULL),
('idiomas_servicios_grupo2_texto1_en', 'Argos, el robot especializado en labores de limpieza (tanto de torre como de palas) opera mediante un módulo especialmente diseñado con una con guración de boquillas de alta presión. Su capacidad de adaptación le permite incorporar distintos dispositivos según el servicio a realizar desde cañones de agua a cepillos rotativos.', 0, NULL, NULL),
('idiomas_servicios_grupo2_texto2', 'La versatilidad y agilidad de los robots de TSR Wind con eren una mayor e cacia al proceso, puesto que no se producen prácticamente pérdidas de tiempo en el funcionamiento de los aerogeneradores. Mantenerlos limpios evita la merma en la capacidad de producir energía que provoca la suciedad (1% anual)', 0, NULL, NULL),
('idiomas_servicios_grupo2_texto2_en', 'La versatilidad y agilidad de los robots de TSR Wind con eren una mayor e cacia al proceso, puesto que no se producen prácticamente pérdidas de tiempo en el funcionamiento de los aerogeneradores. Mantenerlos limpios evita la merma en la capacidad de producir energía que provoca la suciedad (1% anual)', 0, NULL, NULL),
('idiomas_servicios_grupo2_texto3', 'El sistema de plataforma magnética de TSR Wind permite substituir el uso de plataformas elevadoras o trabajos verticales con lo que ello supone en reducción de riesgos laborales. Un solo operario desde tierra monitoriza y dirige la actividad dirigiendo con precisión las operaciones.', 0, NULL, NULL),
('idiomas_servicios_grupo2_texto3_en', 'El sistema de plataforma magnética de TSR Wind permite substituir el uso de plataformas elevadoras o trabajos verticales con lo que ello supone en reducción de riesgos laborales. Un solo operario desde tierra monitoriza y dirige la actividad dirigiendo con precisión las operaciones.', 0, NULL, NULL),
('idiomas_servicios_grupo2_titulo', 'LIMPIEZA', 0, NULL, NULL),
('idiomas_servicios_grupo2_titulo_en', 'LIMPIEZA', 0, NULL, NULL),
('idiomas_servicios_grupo2_titulo1', 'ADAPTABILIDAD', 0, NULL, NULL),
('idiomas_servicios_grupo2_titulo1_en', 'ADAPTABILIDAD', 0, NULL, NULL),
('idiomas_servicios_grupo2_titulo2', 'EFICIENCIA', 0, NULL, NULL),
('idiomas_servicios_grupo2_titulo2_en', 'EFICIENCIA', 0, NULL, NULL),
('idiomas_servicios_grupo2_titulo3', 'SEGURIDAD', 0, NULL, NULL),
('idiomas_servicios_grupo2_titulo3_en', 'SEGURIDAD', 0, NULL, NULL),
('idiomas_servicios_titulo', 'Servicios', 0, NULL, NULL),
('idiomas_servicios_titulo_en', 'Servicios', 0, NULL, NULL),
('idiomas_trsinspector_boton_clientes', 'ACCESO A CLIENTES', 0, NULL, NULL),
('idiomas_trsinspector_boton_clientes_en', 'ACCESO A CLIENTES', 0, NULL, NULL),
('idiomas_trsinspector_entradilla1_texto', 'Por pequeño que sea lo encontraremos. Con TSR Inspector, un software de reconstrucción de palas desarrollado por nuestros ingenieros, se pueden realizar un análisis exhaustivo de las palas completas y determinar de forma milimétrica y detallada de daños.\r\nExclusivo para clientes, TSR Inspector funciona a su vez como plataforma web donde almacenar, consultar o descargar fotografías originales e informes.', 0, NULL, NULL),
('idiomas_trsinspector_entradilla1_texto_en', 'Por pequeño que sea lo encontraremos. Con TSR Inspector, un software de reconstrucción de palas desarrollado por nuestros ingenieros, se pueden realizar un análisis exhaustivo de las palas completas y determinar de forma milimétrica y detallada de daños.\r\nExclusivo para clientes, TSR Inspector funciona a su vez como plataforma web donde almacenar, consultar o descargar fotografías originales e informes.', 0, NULL, NULL),
('idiomas_trsinspector_entradilla1_titulo', 'SOFTWARE EXCLUSIVO', 0, NULL, NULL),
('idiomas_trsinspector_entradilla1_titulo_en', 'SOFTWARE EXCLUSIVO', 0, NULL, NULL),
('idiomas_trsinspector_titulo', 'TRS Inspector', 0, NULL, NULL),
('idiomas_trsinspector_titulo_en', 'TRS Inspector', 0, NULL, NULL),
('idiomas_tsrenelmundo_texto_entradilla1', 'Somos una empresa con vocación internacional que prestamos servicios en paquetes eólicos de todo el mundo. La exclusividad de nuestra propuesta, que combina tecnología punta y un servicio ágil, seguro y versátil, nos permite ofrecer soluciones ad hoc para cada cliente, esté donde esté.', 0, NULL, NULL),
('idiomas_tsrenelmundo_texto_entradilla1_en', 'Somos una empresa con vocación internacional que prestamos servicios en paquetes eólicos de todo el mundo. La exclusividad de nuestra propuesta, que combina tecnología punta y un servicio ágil, seguro y versátil, nos permite ofrecer soluciones ad hoc para cada cliente, esté donde esté.', 0, NULL, NULL),
('idiomas_tsrenelmundo_titulo', 'Proyectos', 0, NULL, NULL),
('idiomas_tsrenelmundo_titulo_en', 'Proyectos', 0, NULL, NULL),
('idiomas_tsrenelmundo_titulo_entradilla1', 'TSR EN EL MUNDO', 0, NULL, NULL),
('idiomas_tsrenelmundo_titulo_entradilla1_en', 'TSR EN EL MUNDO', 0, NULL, NULL),
('legal_aviso_legal', '<p>La web www.comercialap.com tiene por objeto facilitar el conocimiento por el público en general de las actividades que realiza y de los servicios profesionales que presta.</p>\r\n<p><br />\r\n<strong>1. Información general para dar cumplimiento a la Ley 34/2002</strong><br />\r\n<br />\r\nTitular: Alfonso Peña S.L.<br />\r\nDirección: Calle Málaga, 29, 34004 Palencia (España)<br />\r\nContacto: info@comercialap.com o +34 979-165-827</p>\r\n<p><br />\r\nDatos relativos a la empresa: inscrita en el registro mercantil de Palencia, Folio 111, Tomo 72 General, Libro14, Sección 2ª de Sociedades, Hoja nº28 Inscripción 1ª<br />\r\nR.S.I. 4009851/P</p>\r\n<p>CIF de empresa: B34030072</p>\r\n<p><br />\r\n<strong>2. Propiedad intelectual</strong><br />\r\n<br />\r\nLos contenidos suministrados por www.comercialap.com están sujetos a los derechos de propiedad intelectual e industrial y son titularidad exclusiva de Alfonso Peña S.L.<br />\r\n<br />\r\nMediante la adquisición de un producto o servicio, Alfonso Peña S.L no confiere al adquirente ningún derecho de alteración, explotación, reproducción o distribución del mismo fuera de lo estrictamente contratado, reservándose Alfonso Peña S.L todos estos derechos.<br />\r\n<br />\r\nLa cesión de los citados derechos precisará el previo consentimiento por escrito por parte de Alfonso Peña S.L.<br />\r\n<br />\r\nLa propiedad intelectual se extiende, además del contenido incluido en www.comercialap.com, a sus gráficos, logotipos, diseño, imágenes y código fuente utilizado para su programación. Cualquier acto de transmisión, distribución, cesión, reproducción, almacenaje o comunicación pública total o parcial, debe contar con el consentimiento expreso de Alfonso Peña S.L.<br />\r\n<br />\r\n<strong>3. Cláusula de exención de responsabilidad</strong><br />\r\n<br />\r\nEsta página web puede contener enlaces a otras páginas que Alfonso Peña S.L no puede controlar, por lo tanto, Alfonso Peña S.L declina cualquier responsabilidad respecto a la información que pueda aparecer en páginas de terceros.</p>', 0, NULL, NULL),
('legal_aviso_legal_en', '<p>La web www.comercialap.com tiene por objeto facilitar el conocimiento por el público en general de las actividades que realiza y de los servicios profesionales que presta.</p>\r\n<p><br />\r\n<strong>1. Información general para dar cumplimiento a la Ley 34/2002</strong><br />\r\n<br />\r\nTitular: Alfonso Peña S.L.<br />\r\nDirección: Calle Málaga, 29, 34004 Palencia (España)<br />\r\nContacto: info@comercialap.com o +34 979-165-827</p>\r\n<p><br />\r\nDatos relativos a la empresa: inscrita en el registro mercantil de Palencia, Folio 111, Tomo 72 General, Libro14, Sección 2ª de Sociedades, Hoja nº28 Inscripción 1ª<br />\r\nR.S.I. 4009851/P</p>\r\n<p>CIF de empresa: B34030072</p>\r\n<p><br />\r\n<strong>2. Propiedad intelectual</strong><br />\r\n<br />\r\nLos contenidos suministrados por www.comercialap.com están sujetos a los derechos de propiedad intelectual e industrial y son titularidad exclusiva de Alfonso Peña S.L.<br />\r\n<br />\r\nMediante la adquisición de un producto o servicio, Alfonso Peña S.L no confiere al adquirente ningún derecho de alteración, explotación, reproducción o distribución del mismo fuera de lo estrictamente contratado, reservándose Alfonso Peña S.L todos estos derechos.<br />\r\n<br />\r\nLa cesión de los citados derechos precisará el previo consentimiento por escrito por parte de Alfonso Peña S.L.<br />\r\n<br />\r\nLa propiedad intelectual se extiende, además del contenido incluido en www.comercialap.com, a sus gráficos, logotipos, diseño, imágenes y código fuente utilizado para su programación. Cualquier acto de transmisión, distribución, cesión, reproducción, almacenaje o comunicación pública total o parcial, debe contar con el consentimiento expreso de Alfonso Peña S.L.<br />\r\n<br />\r\n<strong>3. Cláusula de exención de responsabilidad</strong><br />\r\n<br />\r\nEsta página web puede contener enlaces a otras páginas que Alfonso Peña S.L no puede controlar, por lo tanto, Alfonso Peña S.L declina cualquier responsabilidad respecto a la información que pueda aparecer en páginas de terceros.</p>', 0, NULL, NULL),
('legal_politica_de_cookies', '<p>El prestador por su propia cuenta o la de un tercero contratado para la prestación de servicios de medición, pueden utilizar cookies cuando un usuario navega por el sitio web. Las cookies son ficheros enviados al navegador por medio de un servidor web con la finalidad de registrar las actividades del usuario durante su tiempo de navegación.</p>\r\n<p>Las cookies utilizadas por el sitio web se asocian únicamente con un usuario anónimo y su ordenador, y no proporcionan por sí mismas los datos personales del usuario.</p>\r\n<p>Mediante el uso de las cookies resulta posible que el servidor donde se encuentra la web, reconozca el navegador web utilizado por el usuario con la finalidad de que la navegación sea más sencilla, permitiendo, por ejemplo, el acceso a los usuarios que se hayan registrado previamente, acceder a las áreas, servicios, promociones o concursos reservados exclusivamente a ellos sin tener que registrarse en cada visita. Se utilizan también para medir la audiencia y parámetros del tráfico, controlar el progreso y número de entradas.</p>\r\n<p>El usuario tiene la posibilidad de configurar su navegador para ser avisado de la recepción de cookies y para impedir su instalación en su equipo. Por favor, consulte las instrucciones y manuales de su navegador para ampliar esta información.</p>\r\n<p>Para utilizar el sitio web, no resulta necesario que el usuario permita la instalación de las cookies enviadas por el sitio web, o el tercero que actúe en su nombre, sin perjuicio de que sea necesario que el usuario inicie una sesión como tal en cada uno de los servicios cuya prestación requiera el previo registro o \"login\".</p>\r\n<p>Las cookies utilizadas en este sitio web tienen, en todo caso, carácter temporal con la única finalidad de hacer más eficaz su transmisión ulterior. En ningún caso se utilizarán las cookies para recoger información de carácter personal.</p>\r\n<p>Tabla de cookies utilizadas:</p>\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th>Proveedor</th>\r\n			<th>Nombre</th>\r\n			<th>Propósito</th>\r\n			<th>Más información</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Comercial AP</td>\r\n			<td>PHPSESSID</td>\r\n			<td>Permite al usuario visualizar la página e interactuar con ella. Es esencial para el funcionamiento correcto de la página.</td>\r\n			<td>\r\n			<p><a href=\"/aviso-legal/\">Aviso legal</a></p>\r\n			<p><a href=\"/politica-de-privacidad/\">Política de privacidad</a></p></td>\r\n		</tr>\r\n		<tr>\r\n			<td>Google Analytics</td>\r\n			<td>__utma<br />\r\n			__utmb<br />\r\n			__utmc<br />\r\n			__utmz</td>\r\n			<td>Recopilan información anónima sobre la navegación de los usuarios por el sitio web con el fin de conocer el origen de las visitas y otros datos estadísticos similares.</td>\r\n			<td>\r\n			<p><a href=\"https://www.google.com/intl/es/policies/\" target=\"_blank\">Centro de privacidad de Google</a></p>\r\n			<p><a href=\"http://tools.google.com/dlpage/gaoptout?hl=es\" target=\"_blank\">Complemento de inhabilitación de Google Analytics</a></p></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>Para desactivar las cookies, el usuario podrá, en cualquier momento, elegir cuáles quiere que funcionen en este sitio web mediante:</p>\r\n<ul>\r\n	<li>la configuración del navegador; por ejemplo:\r\n	<ul>\r\n		<li>Firefox, desde <a href=\"http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we\" target=\"_blank\">http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we</a></li>\r\n		<li>Chrome, desde <a href=\"http://support.google.com/chrome/bin/answer.py?hl=es&amp;answer=95647\" target=\"_blank\">http://support.google.com/chrome/bin/answer.py?hl=es&amp;answer=95647</a></li>\r\n		<li>Explorer, desde <a href=\"http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9\" target=\"_blank\">http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9</a></li>\r\n		<li>Safari, desde <a href=\"http://support.apple.com/kb/ph5042\" target=\"_blank\">http://support.apple.com/kb/ph5042</a></li>\r\n	</ul></li>\r\n	<li>los sistemas de opt-out específicos indicados en la tabla anterior respecto de la cookie de que se trate (estos sistemas pueden conllevar que se instale en su equipo una cookie \"de rechazo\" para que funcione su elección de desactivación)</li>\r\n	<li>otras herramientas de terceros, disponibles on line, que permiten a los usuarios detectar las cookies en cada sitio web que visita y gestionar su desactivación (por ejemplo, Ghostery: <a href=\"http://www.ghostery.com/privacy-statement\" target=\"_blank\">http://www.ghostery.com/privacy-statement</a>, <a href=\"http://www.ghostery.com/faq\" target=\"_blank\">http://www.ghostery.com/faq</a>).</li>\r\n</ul>', 0, NULL, NULL),
('legal_politica_de_cookies_en', '<p>El prestador por su propia cuenta o la de un tercero contratado para la prestación de servicios de medición, pueden utilizar cookies cuando un usuario navega por el sitio web. Las cookies son ficheros enviados al navegador por medio de un servidor web con la finalidad de registrar las actividades del usuario durante su tiempo de navegación.</p>\r\n<p>Las cookies utilizadas por el sitio web se asocian únicamente con un usuario anónimo y su ordenador, y no proporcionan por sí mismas los datos personales del usuario.</p>\r\n<p>Mediante el uso de las cookies resulta posible que el servidor donde se encuentra la web, reconozca el navegador web utilizado por el usuario con la finalidad de que la navegación sea más sencilla, permitiendo, por ejemplo, el acceso a los usuarios que se hayan registrado previamente, acceder a las áreas, servicios, promociones o concursos reservados exclusivamente a ellos sin tener que registrarse en cada visita. Se utilizan también para medir la audiencia y parámetros del tráfico, controlar el progreso y número de entradas.</p>\r\n<p>El usuario tiene la posibilidad de configurar su navegador para ser avisado de la recepción de cookies y para impedir su instalación en su equipo. Por favor, consulte las instrucciones y manuales de su navegador para ampliar esta información.</p>\r\n<p>Para utilizar el sitio web, no resulta necesario que el usuario permita la instalación de las cookies enviadas por el sitio web, o el tercero que actúe en su nombre, sin perjuicio de que sea necesario que el usuario inicie una sesión como tal en cada uno de los servicios cuya prestación requiera el previo registro o \"login\".</p>\r\n<p>Las cookies utilizadas en este sitio web tienen, en todo caso, carácter temporal con la única finalidad de hacer más eficaz su transmisión ulterior. En ningún caso se utilizarán las cookies para recoger información de carácter personal.</p>\r\n<p>Tabla de cookies utilizadas:</p>\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th>Proveedor</th>\r\n			<th>Nombre</th>\r\n			<th>Propósito</th>\r\n			<th>Más información</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Comercial AP</td>\r\n			<td>PHPSESSID</td>\r\n			<td>Permite al usuario visualizar la página e interactuar con ella. Es esencial para el funcionamiento correcto de la página.</td>\r\n			<td>\r\n			<p><a href=\"/aviso-legal/\">Aviso legal</a></p>\r\n			<p><a href=\"/politica-de-privacidad/\">Política de privacidad</a></p></td>\r\n		</tr>\r\n		<tr>\r\n			<td>Google Analytics</td>\r\n			<td>__utma<br />\r\n			__utmb<br />\r\n			__utmc<br />\r\n			__utmz</td>\r\n			<td>Recopilan información anónima sobre la navegación de los usuarios por el sitio web con el fin de conocer el origen de las visitas y otros datos estadísticos similares.</td>\r\n			<td>\r\n			<p><a href=\"https://www.google.com/intl/es/policies/\" target=\"_blank\">Centro de privacidad de Google</a></p>\r\n			<p><a href=\"http://tools.google.com/dlpage/gaoptout?hl=es\" target=\"_blank\">Complemento de inhabilitación de Google Analytics</a></p></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>Para desactivar las cookies, el usuario podrá, en cualquier momento, elegir cuáles quiere que funcionen en este sitio web mediante:</p>\r\n<ul>\r\n	<li>la configuración del navegador; por ejemplo:\r\n	<ul>\r\n		<li>Firefox, desde <a href=\"http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we\" target=\"_blank\">http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we</a></li>\r\n		<li>Chrome, desde <a href=\"http://support.google.com/chrome/bin/answer.py?hl=es&amp;answer=95647\" target=\"_blank\">http://support.google.com/chrome/bin/answer.py?hl=es&amp;answer=95647</a></li>\r\n		<li>Explorer, desde <a href=\"http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9\" target=\"_blank\">http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9</a></li>\r\n		<li>Safari, desde <a href=\"http://support.apple.com/kb/ph5042\" target=\"_blank\">http://support.apple.com/kb/ph5042</a></li>\r\n	</ul></li>\r\n	<li>los sistemas de opt-out específicos indicados en la tabla anterior respecto de la cookie de que se trate (estos sistemas pueden conllevar que se instale en su equipo una cookie \"de rechazo\" para que funcione su elección de desactivación)</li>\r\n	<li>otras herramientas de terceros, disponibles on line, que permiten a los usuarios detectar las cookies en cada sitio web que visita y gestionar su desactivación (por ejemplo, Ghostery: <a href=\"http://www.ghostery.com/privacy-statement\" target=\"_blank\">http://www.ghostery.com/privacy-statement</a>, <a href=\"http://www.ghostery.com/faq\" target=\"_blank\">http://www.ghostery.com/faq</a>).</li>\r\n</ul>', 0, NULL, NULL),
('legal_politica_de_privacidad', '<p>A través de la página www.comerciaap.com no se recoge ningún dato personal sin su conocimiento, ni se ceden a terceros.<br />\r\n<br />\r\nLos datos personales proporcionados a www.comercialap.com, a través de sus formularios de contacto o por los clientes que adquieran productos o servicios de www.comercialap.com, van a ser almacenados en un fichero automatizado de datos, propiedad de Alfonso Peña S.L., para la finalidad única de la gestión de la empresa.<br />\r\n<br />\r\nAlfonso Peña S.L. se compromete a tratar los datos de manera lícita y responsable, de acuerdo con la Ley Orgánica 15/1999, de Protección de Datos de Carácter Personal, garantizando el Derecho a la Intimidad personal y familiar, el honor y el Derecho Fundamental a la protección de los Datos Personales de todas las personas que se encuentren en sus ficheros.<br />\r\n<br />\r\nTodo aquel que proporcione sus datos personales a Alfonso Peña S.L. a través de la web www.comercialap.com tendrá la posibilidad de revocar su consentimiento en cualquier momento, o de ejercitar sus derechos de Acceso, Rectificación, Cancelación y Oposición, por cualquier medio admitido en derecho que permita acreditar la personalidad. Para ello deberá dirigirse a Alfonso Peña S.L, Calle Málaga, 29, 34004 Palencia (España)o a través de correo electrónico a la dirección info@comercialap.com bajo el asunto \"Datos personales\" o también a través del teléfono +34 979-165-827.</p>', 0, NULL, NULL),
('legal_politica_de_privacidad_en', '<p>A través de la página www.comerciaap.com no se recoge ningún dato personal sin su conocimiento, ni se ceden a terceros.<br />\r\n<br />\r\nLos datos personales proporcionados a www.comercialap.com, a través de sus formularios de contacto o por los clientes que adquieran productos o servicios de www.comercialap.com, van a ser almacenados en un fichero automatizado de datos, propiedad de Alfonso Peña S.L., para la finalidad única de la gestión de la empresa.<br />\r\n<br />\r\nAlfonso Peña S.L. se compromete a tratar los datos de manera lícita y responsable, de acuerdo con la Ley Orgánica 15/1999, de Protección de Datos de Carácter Personal, garantizando el Derecho a la Intimidad personal y familiar, el honor y el Derecho Fundamental a la protección de los Datos Personales de todas las personas que se encuentren en sus ficheros.<br />\r\n<br />\r\nTodo aquel que proporcione sus datos personales a Alfonso Peña S.L. a través de la web www.comercialap.com tendrá la posibilidad de revocar su consentimiento en cualquier momento, o de ejercitar sus derechos de Acceso, Rectificación, Cancelación y Oposición, por cualquier medio admitido en derecho que permita acreditar la personalidad. Para ello deberá dirigirse a Alfonso Peña S.L, Calle Málaga, 29, 34004 Palencia (España)o a través de correo electrónico a la dirección info@comercialap.com bajo el asunto \"Datos personales\" o también a través del teléfono +34 979-165-827.</p>', 0, NULL, NULL),
('pasarela_clave_secreta', '', 0, NULL, NULL),
('pasarela_codigo_terminal', '', 0, NULL, NULL),
('pasarela_codigo_titular', '', 0, NULL, NULL),
('pasarela_is_pruebas', '0', 0, NULL, NULL),
('pasarela_is_real', '0', 0, NULL, NULL),
('pasarela_moneda', '978', 0, NULL, NULL),
('pasarela_nombre_titular', '', 0, NULL, NULL),
('pasarela_url_formulario', '', 0, NULL, NULL),
('pasarela_url_ko', 'banco-no-respuesta', 0, NULL, NULL),
('pasarela_url_ok', 'banco-respuesta', 0, NULL, NULL),
('pasarela_url_operaciones', '', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preferences__files`
--

DROP TABLE IF EXISTS `preferences__files`;
CREATE TABLE IF NOT EXISTS `preferences__files` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `owner_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'propietario (id)',
  `owner_type` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'propietario (mod)',
  `type` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'tipo de archivo',
  `title_1` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'título',
  `name` text COLLATE utf8_spanish2_ci COMMENT 'nombre original',
  `mime_type` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'tipo recogido',
  `size` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'tamaño',
  `height` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'alto (si imagen)',
  `width` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ancho (si imagen)',
  `creacion` timestamp NULL DEFAULT NULL,
  `modificacion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `owner_id` (`owner_id`,`owner_type`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `preferences__files`
--

INSERT INTO `preferences__files` (`id`, `owner_id`, `owner_type`, `type`, `title_1`, `name`, `mime_type`, `size`, `height`, `width`, `creacion`, `modificacion`) VALUES
(3, 3, 'Galeria', 'galeria', 'Tanco.jpg', 'Tanco.jpg', 'image/jpeg', 230921, 500, 1600, '2017-01-29 20:18:31', NULL),
(4, 4, 'Galeria', 'galeria', 'Krone.jpg', 'Krone.jpg', 'image/jpeg', 280810, 500, 1600, '2017-01-29 20:19:00', NULL),
(5, 1, 'Johndeere', 'logo-john-deere', 'John_Deere.png', 'John_Deere.png', 'image/png', 4853, 60, 250, '2017-01-29 18:56:54', NULL),
(7, 5, 'Galeria', 'galeria', 'amazone.jpg', 'amazone.jpg', 'image/jpeg', 318592, 500, 1600, '2017-01-29 20:16:32', NULL),
(8, 6, 'Galeria', 'galeria', 'BvL.jpg', 'BvL.jpg', 'image/jpeg', 230945, 500, 1600, '2017-01-29 20:17:02', NULL),
(9, 7, 'Galeria', 'galeria', 'Kramer.jpg', 'Kramer.jpg', 'image/jpeg', 255744, 500, 1600, '2017-01-29 20:17:27', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preferences__galeria`
--

DROP TABLE IF EXISTS `preferences__galeria`;
CREATE TABLE IF NOT EXISTS `preferences__galeria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `alt` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `link` varchar(250) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '/',
  `link_1` varchar(250) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '/',
  `nombre_1` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `alt_1` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `id_tipo` int(11) DEFAULT NULL,
  `id_preferencia` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `creacion` timestamp NULL DEFAULT NULL,
  `modificacion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `preferences__galeria`
--

INSERT INTO `preferences__galeria` (`id`, `nombre`, `alt`, `link`, `link_1`, `nombre_1`, `alt_1`, `id_tipo`, `id_preferencia`, `owner_id`, `position`, `creacion`, `modificacion`) VALUES
(3, '', 'Tanco', '/', '/', '', 'Tanco', 1, '1', 0, 0, '2017-01-27 12:57:43', NULL),
(4, '', 'Krone', '/', '/', '', 'Krone', 1, '1', 0, 0, '2017-01-27 12:58:08', NULL),
(5, '', 'amazone', '/', '/', '', 'amazone', 1, '1', 0, 0, '2017-01-29 20:16:31', NULL),
(6, '', 'BvL', '/', '/', '', 'BvL', 1, '1', 0, 0, '2017-01-29 20:17:02', NULL),
(7, '', 'Kramer', '/', '/', '', 'Kramer', 1, '1', 0, 0, '2017-01-29 20:17:27', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

DROP TABLE IF EXISTS `productos`;
CREATE TABLE IF NOT EXISTS `productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipo` int(11) NOT NULL,
  `codigo` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `meta_titulo` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `meta_descripcion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8_spanish2_ci,
  `caracteristicas` text COLLATE utf8_spanish2_ci,
  `formato` text COLLATE utf8_spanish2_ci,
  `ingredientes` text COLLATE utf8_spanish2_ci,
  `id_categoria` int(11) DEFAULT '0',
  `precio` float NOT NULL DEFAULT '0',
  `precio_rebajado` float NOT NULL DEFAULT '0',
  `iva` float NOT NULL DEFAULT '4',
  `stock` int(11) NOT NULL DEFAULT '0',
  `peso` int(11) NOT NULL DEFAULT '0',
  `etiqueta` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `contenido` int(4) NOT NULL DEFAULT '0',
  `publicado` tinyint(1) NOT NULL DEFAULT '0',
  `es_novedad` tinyint(1) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `id_tipo`, `codigo`, `nombre`, `meta_titulo`, `meta_descripcion`, `url`, `descripcion`, `caracteristicas`, `formato`, `ingredientes`, `id_categoria`, `precio`, `precio_rebajado`, `iva`, `stock`, `peso`, `etiqueta`, `contenido`, `publicado`, `es_novedad`, `position`, `locked`, `created_at`, `updated_at`) VALUES
(8, 1, 'LOREM', 'Lorem ipsum 1', 'Lorem ipsum ', 'Lorem ipsum ', 'lorem-ipsum', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>\r\n<p>Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '', '', 1, 8, 8, 4, 20, 0, '', 0, 1, 0, -2, 0, '2017-10-10 19:55:00', '2017-10-10 19:55:00'),
(23, 1, 'LOREM-1', 'Lorem ipsum A', 'Lorem ipsum ', 'Lorem ipsum ', 'lorem-ipsum-1', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>\r\n<p>Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '', '', 5, 23, 23, 4, 20, 0, '', 0, 1, 0, -1, 0, '2017-10-10 19:55:00', '2017-10-10 19:55:00'),
(24, 1, 'LOREM-2', 'Lorem ipsum ', 'Lorem ipsum ', 'Lorem ipsum ', 'lorem-ipsum-2', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>\r\n<p>Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '', '', 1, 24, 24, 4, 0, 0, '', 0, 1, 0, -3, 0, '2017-10-10 23:55:00', '2017-10-10 23:55:00'),
(25, 1, 'LOREM-3', 'Lorem ipsum ', 'Lorem ipsum ', 'Lorem ipsum ', 'lorem-ipsum-3', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>\r\n<p>Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '', '', 1, 25, 25, 4, 10, 0, '', 0, 0, 0, -4, 0, '2017-10-10 23:55:00', '2017-10-10 23:55:00'),
(26, 1, 'LOREM-4', 'Lorem ipsum ', 'Lorem ipsum ', 'Lorem ipsum ', 'lorem-ipsum-4', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>\r\n<p>Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '', '', 1, 26, 26, 4, 20, 0, '', 0, 1, 0, -5, 0, '2017-10-10 23:55:00', '2017-10-10 23:55:00'),
(27, 1, 'LOREM-5', 'Lorem ipsum ', 'Lorem ipsum ', 'Lorem ipsum ', 'lorem-ipsum-5', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>\r\n<p>Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '', '', 1, 27, 27, 4, 0, 0, '', 0, 1, 0, -6, 0, '2017-10-10 23:55:00', '2017-10-10 23:55:00'),
(28, 1, 'LOREM-6', 'Lorem ipsum ', 'Lorem ipsum ', 'Lorem ipsum ', 'lorem-ipsum-6', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>\r\n<p>Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '', '', 1, 28, 28, 4, 10, 0, '', 0, 1, 0, -7, 0, '2017-10-11 01:55:00', '2017-10-11 01:55:00'),
(29, 1, 'LOREM-7', 'Lorem ipsum ', 'Lorem ipsum ', 'Lorem ipsum ', 'lorem-ipsum-7', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>\r\n<p>Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '', '', 1, 29, 29, 4, 20, 0, '', 0, 1, 0, -8, 0, '2017-10-10 23:55:00', '2017-10-10 23:55:00'),
(30, 1, 'LOREM-8', 'Lorem ipsum ', 'Lorem ipsum ', 'Lorem ipsum ', 'lorem-ipsum-8', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>\r\n<p>Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '', '', 1, 30, 30, 4, 0, 0, '', 0, 1, 0, -9, 0, '2017-10-10 23:55:00', '2017-10-10 23:55:00'),
(31, 1, 'LOREM-9', 'Lorem ipsum ', 'Lorem ipsum ', 'Lorem ipsum ', 'lorem-ipsum-9', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>\r\n<p>Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '', '', 1, 31, 31, 4, 10, 0, '', 0, 1, 0, -10, 0, '2017-10-10 23:55:00', '2017-10-10 23:55:00'),
(32, 1, 'LOREM-10', 'Lorem ipsum ', 'Lorem ipsum ', 'Lorem ipsum ', 'lorem-ipsum-10', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>\r\n<p>Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '', '', 1, 32, 32, 4, 20, 0, '', 0, 1, 0, -11, 0, '2017-10-10 23:55:00', '2017-10-10 23:55:00'),
(33, 1, 'LOREM-11', 'Lorem ipsum ', 'Lorem ipsum ', 'Lorem ipsum ', 'lorem-ipsum-11', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>\r\n<p>Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '', '', 1, 33, 33, 4, 0, 0, '', 0, 1, 0, -12, 0, '2017-10-10 23:55:00', '2017-10-10 23:55:00'),
(34, 1, 'LOREM-12', 'Lorem ipsum ', 'Lorem ipsum ', 'Lorem ipsum ', 'lorem-ipsum-12', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>\r\n<p>Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '', '', 1, 34, 34, 4, 10, 0, '', 0, 1, 0, -13, 0, '2017-10-10 23:55:00', '2017-10-10 23:55:00'),
(35, 2, 'LOREM-13', 'Lorem ipsum ', 'Lorem ipsum ', 'Lorem ipsum ', 'lorem-ipsum-13', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>\r\n<p>Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '', '', 2, 35, 35, 4, 20, 0, '', 0, 1, 0, -14, 0, '2017-10-10 23:55:00', '2017-10-10 23:55:00'),
(36, 2, 'LOREM-14', 'Lorem ipsum ', 'Lorem ipsum ', 'Lorem ipsum ', 'lorem-ipsum-14', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>\r\n<p>Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '', '', 2, 36, 36, 4, 0, 0, '', 0, 1, 0, -15, 0, '2017-10-10 23:55:00', '2017-10-10 23:55:00'),
(37, 2, 'LOREM-15', 'Lorem ipsum ', 'Lorem ipsum ', 'Lorem ipsum ', 'lorem-ipsum-15', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>\r\n<p>Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '', '', 2, 37, 37, 4, 10, 0, '', 0, 1, 0, -16, 0, '2017-10-10 23:55:00', '2017-10-10 23:55:00'),
(38, 2, 'LOREM-16', 'Lorem ipsum ', 'Lorem ipsum ', 'Lorem ipsum ', 'lorem-ipsum-16', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>\r\n<p>Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '', '', 2, 38, 38, 4, 20, 0, '', 0, 1, 0, -17, 0, '2017-10-10 23:55:00', '2017-10-10 23:55:00'),
(39, 2, 'LOREM-17', 'Lorem ipsum ', 'Lorem ipsum ', 'Lorem ipsum ', 'lorem-ipsum-17', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>\r\n<p>Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '', '', 2, 39, 39, 4, 0, 0, '', 0, 1, 0, -18, 0, '2017-10-10 23:55:00', '2017-10-10 23:55:00'),
(40, 2, 'LOREM-18', 'Lorem ipsum ', 'Lorem ipsum ', 'Lorem ipsum ', 'lorem-ipsum-18', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>\r\n<p>Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '', '', 2, 40, 40, 4, 10, 0, '', 0, 1, 0, -19, 0, '2017-10-10 23:55:00', '2017-10-10 23:55:00'),
(41, 2, 'LOREM-19', 'Lorem ipsum ', 'Lorem ipsum ', 'Lorem ipsum ', 'lorem-ipsum-19', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>\r\n<p>Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '', '', 2, 41, 41, 4, 20, 0, '', 0, 1, 0, -20, 0, '2017-10-11 01:55:00', '2017-10-11 01:55:00'),
(42, 2, 'LOREM-20', 'Lorem ipsum ', 'Lorem ipsum ', 'Lorem ipsum ', 'lorem-ipsum-20', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nDonec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>\r\n<p>Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '', '', 2, 42, 42, 4, 0, 0, '', 0, 1, 0, -21, 0, '2017-10-10 23:55:00', '2017-10-10 23:55:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__categorias`
--

DROP TABLE IF EXISTS `productos__categorias`;
CREATE TABLE IF NOT EXISTS `productos__categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipo` int(11) NOT NULL DEFAULT '0',
  `nombre` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `url` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `titulo` varchar(120) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `meta_titulo` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `meta_descripcion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `owner_id` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos__categorias`
--

INSERT INTO `productos__categorias` (`id`, `id_tipo`, `nombre`, `url`, `titulo`, `meta_titulo`, `meta_descripcion`, `position`, `owner_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'Fase 1', 'fase-1', 'Aceites con denominación de origen :: Selectisimo', 'Fase 1', 'Fase 1', -1, 0, '2017-09-01 06:44:38', '2017-09-01 06:44:38'),
(2, 2, 'Retrato', 'retrato', 'Selección Gourmet de Loncheados :: Selectisimo', 'Retrato', 'Retrato', -3, 0, '2017-09-01 06:44:38', '2017-09-01 06:44:38'),
(3, 2, 'Paisajes', 'paisajes', NULL, 'Paisajes', 'Paisajes', -4, 0, '2017-10-11 08:07:46', '2017-10-11 08:07:46'),
(4, 2, 'Personal', 'personal', NULL, 'Personal', 'Personal', -5, 0, '2017-10-11 08:07:56', '2017-10-11 08:07:56'),
(5, 1, 'Fase 2', 'fase-2', NULL, 'Fase 2', 'Fase 2', -2, 1, '2017-10-11 08:08:36', '2017-10-11 08:08:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__comments`
--

DROP TABLE IF EXISTS `productos__comments`;
CREATE TABLE IF NOT EXISTS `productos__comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) NOT NULL DEFAULT '0',
  `id_usuario` int(11) NOT NULL DEFAULT '0',
  `nombre_usuario` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email_usuario` varchar(120) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `titulo` varchar(200) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `comentario` text COLLATE utf8_spanish2_ci,
  `puntuacion` int(11) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos__comments`
--

INSERT INTO `productos__comments` (`id`, `id_parent`, `id_usuario`, `nombre_usuario`, `email_usuario`, `titulo`, `comentario`, `puntuacion`, `position`, `owner_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 122, 5, 'test', 'test', '', '', 0, 0, 0, NULL, NULL, NULL),
(2, 56, 5, 'César', 'cevm88@gmail.com', 'Buenísimo', '<p>Lorem Ipsum Dolor</p>', 3, 0, 0, NULL, NULL, NULL),
(3, 56, 5, 'César Vega', 'cesar.euphorbia@gmail.com', 'Me encataron', '<p>test</p>', 5, 0, 0, NULL, NULL, NULL),
(5, 1, 5, 'César V.M.', 'cesar@euphorbia.es', 'Perfecto para todo', 'Un artículo que se puede tomar solo o utilizar en cualquier elaboración.', 0, 0, 0, '2017-09-15 06:55:51', '2017-09-15 06:55:51', NULL),
(7, 26, 5, 'César V.M.', 'cesar@euphorbia.es', NULL, NULL, 0, 0, 0, '2017-10-31 08:10:41', '2017-10-31 08:10:41', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__files`
--

DROP TABLE IF EXISTS `productos__files`;
CREATE TABLE IF NOT EXISTS `productos__files` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `owner_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'propietario (id)',
  `owner_type` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'propietario (mod)',
  `type` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'tipo de archivo',
  `title_1` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'título',
  `name` text COLLATE utf8_spanish2_ci COMMENT 'nombre original',
  `mime_type` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'tipo recogido',
  `size` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'tamaño',
  `height` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'alto (si imagen)',
  `width` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ancho (si imagen)',
  `path` varchar(200) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `creacion` timestamp NULL DEFAULT NULL,
  `modificacion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos__files`
--

INSERT INTO `productos__files` (`id`, `owner_id`, `owner_type`, `type`, `title_1`, `name`, `mime_type`, `size`, `height`, `width`, `path`, `creacion`, `modificacion`) VALUES
(35, 52, 'Galeria', '0000000001', 'campanal.jpg', 'hola', 'image/jpeg', 24243, 414, 508, NULL, '2017-07-13 10:48:40', '2017-07-13 10:48:40'),
(36, 52, 'Galeria', '0000000001', 'distribuidor_leon.png', 'f1231', 'image/png', 33431, 320, 320, NULL, '2017-07-13 11:03:03', '2017-07-13 11:03:03'),
(48, 55, 'Galeria', '0000000001', 'aceitunas_sarasa1.jpg', 'qewqeq', 'image/jpeg', 231148, 243, 556, NULL, '2017-07-26 05:36:36', '2017-07-26 05:36:36'),
(49, 55, 'Productos', 'img', 'aceites.jpg', 'aceites.jpg', 'image/jpeg', 33709, 414, 508, NULL, '2017-07-27 09:11:33', NULL),
(50, 552, 'Productos', 'img', 'aceites.jpg', 'aceites.jpg', 'image/jpeg', 33709, 414, 508, NULL, '2017-07-27 10:45:04', NULL),
(53, 58, 'Productos', 'img', 'distribuidor_burgos.png', 'distribuidor_burgos.png', 'image/png', 36416, 320, 320, NULL, '2017-07-27 11:08:44', NULL),
(55, 56, 'Productos', 'img', 'aceites.jpg', 'aceites.jpg', 'image/jpeg', 33709, 414, 508, NULL, '2017-07-28 06:53:34', NULL),
(76, 126, 'productos', 'img', 'descarga.jpg', 'descarga.jpg', 'image/jpeg', 7121, 225, 225, NULL, '2017-08-08 11:53:46', NULL),
(94, 8, 'productos', 'img', 'img-6530_201706031857165933068cd2eec.sized.jpg', 'img-6530_201706031857165933068cd2eec.sized.jpg', 'image/jpeg', 650998, 960, 1440, NULL, '2017-10-13 07:36:32', NULL),
(95, 9, 'productos', 'img', 'jamonibericobellota-400x300.jpg', 'jamonibericobellota-400x300.jpg', 'image/jpeg', 33664, 300, 400, NULL, '2017-08-16 11:58:40', NULL),
(96, 10, 'productos', 'img', 'images.jpg', 'images.jpg', 'image/jpeg', 8675, 173, 292, NULL, '2017-08-16 12:03:16', NULL),
(99, 12, 'productos', 'img', 'salchichon-iberico-de-bellota-pata-negra-4.jpg', 'salchichon-iberico-de-bellota-pata-negra-4.jpg', 'image/jpeg', 90293, 300, 400, NULL, '2017-08-16 12:19:54', NULL),
(100, 12, 'productos', 'img', 'salchichon-iberico-de-bellota-pata-negra-4.jpg', 'salchichon-iberico-de-bellota-pata-negra-4.jpg', 'image/jpeg', 90293, 300, 400, NULL, '2017-08-16 12:20:03', NULL),
(101, 12, 'productos', 'img', 'salchichon-iberico-de-bellota-pata-negra-4.jpg', 'salchichon-iberico-de-bellota-pata-negra-4.jpg', 'image/jpeg', 90293, 300, 400, NULL, '2017-08-16 12:20:20', NULL),
(102, 12, 'productos', 'img', 'salchichon-iberico-de-bellota-pata-negra-4.jpg', 'salchichon-iberico-de-bellota-pata-negra-4.jpg', 'image/jpeg', 90293, 300, 400, NULL, '2017-08-16 12:20:32', NULL),
(103, 11, 'productos', 'img', 'foto llangonissa talls.jpg', 'foto llangonissa talls.jpg', 'image/jpeg', 32313, 300, 400, NULL, '2017-08-16 12:21:02', NULL),
(104, 13, 'productos', 'img', 'salchichon-iberico-dehesa-cordobesa-1.jpg', 'salchichon-iberico-dehesa-cordobesa-1.jpg', 'image/jpeg', 57813, 300, 400, NULL, '2017-08-16 12:44:04', NULL),
(105, 4, 'Galeria', 'F2A7-H0004', 'macarons.jpg', 'foto_frontal', 'image/jpeg', 276185, 745, 745, NULL, '2017-09-01 06:47:56', '2017-09-01 06:47:56'),
(106, 5, 'Galeria', '191A-V0005', 'crema-de-queso-con-miel.jpg', 'foto_lateral', 'image/jpeg', 47102, 500, 500, NULL, '2017-09-01 06:49:14', '2017-09-01 06:49:14'),
(107, 6, 'Galeria', '2790-Q0006', 'garrapinadas.jpg', 'foto_lateral', 'image/jpeg', 148052, 414, 650, NULL, '2017-09-01 06:49:38', '2017-09-01 06:49:38'),
(108, 7, 'Galeria', '7988-S0007', '00113301500826____5__640x640.jpg', 'foto_inicial', 'image/jpeg', 135926, 640, 640, NULL, '2017-09-01 06:50:24', '2017-09-01 06:50:24'),
(109, 7, 'Galeria', '7988-S0007', 'akelarre2.png', 'foto_1', 'image/png', 216089, 650, 540, NULL, '2017-09-01 06:50:24', '2017-09-01 06:50:24'),
(110, 7, 'Galeria', '7988-S0007', 'akelarre3.png', 'foto_2', 'image/png', 348859, 439, 644, NULL, '2017-09-01 06:50:24', '2017-09-01 06:50:24'),
(111, 1, 'Galeria', '5099-K0001', 'aceite-oliva-magnasur-picual-500-ml_m.jpg', 'foto_inicial', 'image/jpeg', 23795, 500, 500, NULL, '2017-09-01 06:52:05', '2017-09-01 06:52:05'),
(112, 2, 'Galeria', 'D5EA-C0002', 'CASAS-DE-HUALDO-ARBEQUINA1.png', 'foto_inicial', 'image/png', 32012, 400, 400, NULL, '2017-09-01 06:52:50', '2017-09-01 06:52:50'),
(113, 3, 'Galeria', '5B85-L0003', 'jamon-joselito-gran-reserva-bellota.jpg', 'foto_inicial', 'image/jpeg', 24768, 458, 458, NULL, '2017-09-01 06:53:08', '2017-09-01 06:53:08'),
(114, 23, 'productos', 'img', 'img-6504_201706031857165933068cf2112.sized.jpg', 'img-6504_201706031857165933068cf2112.sized.jpg', 'image/jpeg', 380898, 1152, 768, NULL, '2017-10-13 07:39:51', NULL),
(115, 24, 'productos', 'img', 'BLACK_600x800.png', 'BLACK_600x800.png', 'image/png', 2058, 800, 600, NULL, '2017-10-11 09:15:43', NULL),
(116, 25, 'productos', 'img', 'GREY_100x100.png', 'GREY_100x100.png', 'image/png', 271, 100, 100, NULL, '2017-10-11 09:15:54', NULL),
(117, 26, 'productos', 'img', 'WHITE_100x100.png', 'WHITE_100x100.png', 'image/png', 271, 100, 100, NULL, '2017-10-11 09:16:17', NULL),
(118, 27, 'productos', 'img', 'WHITE_600x400.png', 'WHITE_600x400.png', 'image/png', 1780, 400, 600, NULL, '2017-10-11 09:16:32', NULL),
(119, 29, 'productos', 'img', 'BLACK_600x400.png', 'BLACK_600x400.png', 'image/png', 1780, 400, 600, NULL, '2017-10-11 10:25:42', NULL),
(120, 30, 'productos', 'img', 'GREY_600x400.png', 'GREY_600x400.png', 'image/png', 1780, 400, 600, NULL, '2017-10-11 10:25:52', NULL),
(121, 31, 'productos', 'img', 'BLACK_100x100.png', 'BLACK_100x100.png', 'image/png', 271, 100, 100, NULL, '2017-10-11 10:26:06', NULL),
(122, 32, 'productos', 'img', 'WHITE_100x100.png', 'WHITE_100x100.png', 'image/png', 271, 100, 100, NULL, '2017-10-11 10:26:19', NULL),
(123, 33, 'productos', 'img', 'WHITE_600x400.png', 'WHITE_600x400.png', 'image/png', 1780, 400, 600, NULL, '2017-10-11 10:26:28', NULL),
(124, 34, 'productos', 'img', 'BLACK_600x800.png', 'BLACK_600x800.png', 'image/png', 2058, 800, 600, NULL, '2017-10-11 10:26:38', NULL),
(125, 35, 'productos', 'img', 'GREY_600x800.png', 'GREY_600x800.png', 'image/png', 2058, 800, 600, NULL, '2017-10-11 10:26:47', NULL),
(126, 37, 'productos', 'img', 'WHITE_600x400.png', 'WHITE_600x400.png', 'image/png', 1780, 400, 600, NULL, '2017-10-11 10:27:00', NULL),
(127, 38, 'productos', 'img', 'BLACK_800x600.png', 'BLACK_800x600.png', 'image/png', 2803, 600, 800, NULL, '2017-10-11 10:27:09', NULL),
(128, 40, 'productos', 'img', 'GREY_600x800.png', 'GREY_600x800.png', 'image/png', 2058, 800, 600, NULL, '2017-10-11 10:27:17', NULL),
(129, 39, 'productos', 'img', 'WHITE_1200x900.png', 'WHITE_1200x900.png', 'image/png', 4228, 900, 1200, NULL, '2017-10-11 10:27:29', NULL),
(130, 36, 'productos', 'img', 'GREY_600x600.png', 'GREY_600x600.png', 'image/png', 1970, 600, 600, NULL, '2017-10-11 10:27:55', NULL),
(131, 42, 'productos', 'img', 'BLACK_1200x900.png', 'BLACK_1200x900.png', 'image/png', 4228, 900, 1200, NULL, '2017-10-11 10:28:19', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__galeria`
--

DROP TABLE IF EXISTS `productos__galeria`;
CREATE TABLE IF NOT EXISTS `productos__galeria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `alt` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `link` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__relaccionados`
--

DROP TABLE IF EXISTS `productos__relaccionados`;
CREATE TABLE IF NOT EXISTS `productos__relaccionados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) DEFAULT NULL,
  `id_producto_relaccionado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos__relaccionados`
--

INSERT INTO `productos__relaccionados` (`id`, `id_producto`, `id_producto_relaccionado`) VALUES
(1, 56, 56),
(2, 56, 58),
(3, 6, 4),
(4, 4, 4),
(5, 4, 6),
(6, 4, 7),
(7, 4, 8),
(8, 7, 4),
(9, 7, 6),
(10, 7, 7),
(11, 7, 8),
(15, 9, 4),
(16, 9, 6),
(17, 9, 7),
(18, 9, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__rel_tags`
--

DROP TABLE IF EXISTS `productos__rel_tags`;
CREATE TABLE IF NOT EXISTS `productos__rel_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) UNSIGNED NOT NULL COMMENT 'id de post',
  `tag_id` int(11) UNSIGNED NOT NULL COMMENT 'id de tag',
  PRIMARY KEY (`id`),
  UNIQUE KEY `post_id` (`post_id`,`tag_id`),
  KEY `FK_NOTICIAS_REL_TAGS_TAG` (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos__rel_tags`
--

INSERT INTO `productos__rel_tags` (`id`, `post_id`, `tag_id`) VALUES
(5, 2, 1),
(7, 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__tags`
--

DROP TABLE IF EXISTS `productos__tags`;
CREATE TABLE IF NOT EXISTS `productos__tags` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_tipo` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'url en español',
  `url_2` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'url en inglés',
  `text` varchar(255) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre en español',
  `text_2` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'nombre en inglés',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos__tags`
--

INSERT INTO `productos__tags` (`id`, `id_tipo`, `url`, `url_2`, `text`, `text_2`) VALUES
(1, NULL, 'art', 'art', 'arte', 'art'),
(2, NULL, 'barrocoadsds-121-1', 'baroque', 'barrocoadsds 121 1 ', 'baroque');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos__valores`
--

DROP TABLE IF EXISTS `productos__valores`;
CREATE TABLE IF NOT EXISTS `productos__valores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) NOT NULL,
  `clave` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `valor` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `position` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_producto` (`id_producto`,`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `salt` varchar(128) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `creador` int(11) DEFAULT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_modificacion` timestamp NULL DEFAULT NULL,
  `baja` timestamp NULL DEFAULT NULL,
  `oculto` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `username_2` (`username`,`baja`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `salt`, `creador`, `fecha_creacion`, `fecha_modificacion`, `baja`, `oculto`) VALUES
(2, 'lorena@euphorbia.es', '8d8774060e2f1046758f8ad72f0c3b1f2fd7f2260405d3526b28f20a71f0208fc262440820b42fb80b7f0126a88a8645a2e72130e64e024901a1900afecd724d', '364f105ee0b6b9f82954ca7e3b5740bad160bab2c777628dc0fb43b59d27fb9dbedbb3950b4d83aea2b4a816d058a69a093bf0efd4dcbbec682161f75b7b6c83', 0, '2017-01-19 12:05:51', NULL, NULL, 1),
(4, 'admin', '8d8774060e2f1046758f8ad72f0c3b1f2fd7f2260405d3526b28f20a71f0208fc262440820b42fb80b7f0126a88a8645a2e72130e64e024901a1900afecd724d', '364f105ee0b6b9f82954ca7e3b5740bad160bab2c777628dc0fb43b59d27fb9dbedbb3950b4d83aea2b4a816d058a69a093bf0efd4dcbbec682161f75b7b6c83', 0, '2017-05-04 07:31:01', NULL, NULL, 1),
(5, 'cesar@euphorbia.es', '7670bbdef85ec2a3483965f40cd44d422915cc866ff46962c0ae8476b3dce766ffa92dc722cef0747679a18274dfb6da7191db6bf491e3c093b5dceca7577363', '9019d2aadf0b3c0f4fc1b41809a8eb51f3f30f3e131df5852a24a9ab2ac3c68bb76a99b947c814404b465bd24bc3a621ee725b0d7b39db94ed8626d5fd6d818b', 0, '2017-10-26 09:50:53', '2017-10-26 07:50:53', NULL, 1),
(6, 'servicios@selectisimo.com', '2bd017b58df1866bd22495a1f661f2af4fab65156e0cfa5788c861f4277a716ebe8d136a1bd85ad3a4db84db5eb5d113f7b4a83d744e95617363a7bf74fb819c', 'd3301ce25f5ef0617c1bee565ab5346a93779b113fbd6837c396a2e89f6abfeb0a59f2cd642bb49164f63f27bd4bdf33e599f84a17ebc53415c222893a5b0b4c', NULL, '2017-07-14 11:52:47', NULL, NULL, 0),
(7, 'cevm88@gmail.com', 'b172cecb30459b7eddea99df97444fe8e5faf4eb4c03e3a140e35f651e47b06a5536fbbb0259572bcc066af9675fd91bf8022bbae8b8532c0452b972343100c9', 'cf68b8eab2187837890d3c144780df83f684ec1f454cd9fd221da3c4c599421ea535fe54a8887d5dd7afa3c557ab1d4c586bdb17e5d5c3f9368bb203c77f7009', 5, '2017-08-01 19:15:00', '2017-08-01 17:14:00', NULL, 0),
(9, 'gestor@euphorbia.es', NULL, NULL, 5, '2017-08-02 05:26:00', '2017-08-02 05:26:00', NULL, 0),
(10, 'cesar@selectisimo.com', '10b4c99beaf6e0c7a25d3a757a9d0482f4182db880f099f5b5f517ed448144ac3fd62ed60411f02d66ca435f84b9aa7a276f38ba44f3b9d586e5958793a76d32', 'a79b7f6fb9092a551928c99ec0eac18949de87857b3c1c94baa2afc20aed978bd71b45f3d40af4fd17920c361a1ac4fe414afe13fb2b6860e837cc3cbefe85eb', NULL, '2017-08-21 07:28:27', '2017-08-21 07:28:27', NULL, 0),
(11, 'usuario@test.com', '9dc24da62f22a027c40c22ae0be80566f1ae98596b64d2e58fdb1192544f752efeb4301d4bbdb1a4e071c008f1e13e0935cf1a89d837bce11091637e2b9311b4', 'd84fda215c163c79e3a75de68b5c6df141ef276fdbfce95357e7200741686aa6eeddbf48e0366fc1d91588f8e203e6f0450cedec27a6c90170f5b63a072873af', NULL, '2017-08-21 07:30:23', '2017-08-21 07:30:23', NULL, 0),
(12, 'test@test.com', '88b9aa1e33735db712b6534ee4c3caac138a68e1415ede2288cdca04b6246c9403e41b638609377bf46f28f1cdcac4627c1fecd500564595f52ad5d0251f89c8', '79067be1de38c055e7c3308dd9480d1c412f58339fdfa52cf3950ce1edbcedb2ae80cb2e76a1e0ffcaaf3a2d7b2bf35602ae3f693def383aa5d479b129457beb', NULL, '2017-08-21 07:40:43', '2017-08-21 07:40:43', NULL, 0),
(13, 'usuario1@test.com', '01feff1641aeb46fe3205b2e2a014d438a4303ea1eec0219b2dbf78c83bae190f03383eeae02117e3fa499356d66d10b86452216846b98a7faf7c588feaab97f', '7835fd2ac924693fccbfd8ad0306a607bfc9cf27f44e23d824ebc3b2c4b8cf03fd792c9394b609f40766246f39969317f6f4753e8153b21c8626902b1b80c50e', NULL, '2017-10-25 09:04:51', '2017-10-25 09:04:51', NULL, 0),
(14, 'cevm888@gmail.com', '5056711856bf3ce1e0aace5bc580dc4432c4ebc0a4721435ac4417a8ea61e887e52b4b0571dc59e755f8aaab42704156780c84a585afb86b2c70abb1bff7cee7', 'e1901acafa0789ae9491a82666dff4c08b52507d6396bec1a0e538fd11023f2d97b3027c163e240dee5695e3343b691e275617154cfd3fa05b4886a68e693d41', NULL, '2017-10-25 09:41:20', '2017-10-25 09:41:20', NULL, 0),
(15, 'test@asda.com', '8ce2824074dbfc4eab97c6344ea4d1c6f8ff6cb5986495a9b703f994c9297fdcf57f468b427814d287ad33bf2673c28f6b9f839b2231dbf24db7755400e2cac0', '2b9cbadefcc3207388e75b74e5f710e8353252b74e2f0629fbfffe4f6698867c3aab14ec41e99d61954310dda4b74dd212fbf28802f3302619c44b6e3cd72f7e', NULL, '2017-10-25 09:42:52', '2017-10-25 09:42:52', NULL, 0),
(16, 'usuario2@test.com', '509d523c57d73fb81354ed7aa3d7354cdec8706d994bc2874ea59488ad8b3a52bc02eeef145c12d243259d285dcc7fa38d1ce2627b6aba7d157a7a949ded8853', '58cc93d98391fcbb47228e84d263bc15f2558ac531da9a312e86e4fbf857f1f12e8626037b165435cea58c933caa0785820fd7afb0172deb7b80307739513fb2', NULL, '2017-10-25 09:48:41', '2017-10-25 09:48:41', NULL, 0),
(17, 'usuario4@test.com', '23faac14bbc6c7183bea558d71e2028383e2ab44ef474c4d53d6a71a6ecc2671bdca1af3beb6a2e13ef5fc44b27e9b6b49e315adb8db40e1d717b21936213a73', '1fcc57f521c289ae5708645caccf2a5f18ca2459d09609e917456c4a37e800ce2c3a4c09ad73fb0e344f8c803718e7bc796b5c3210a62631a105c09aa787a7b6', NULL, '2017-10-25 10:09:46', '2017-10-25 10:09:46', NULL, 0),
(18, 'usuario5@test.com', '408f024be7ffcb54aa4ef9c236e045aaeb1a0113847d6c5ac431d209c6e16864b3dd94be05b74b0288318a73ce852549fd1a15529a07213baaa92a1cd502d017', 'cc09384bd04d59cbcecc3cbc3f98c834e41c4704b2288fd0eeda252c683c55741d1f28aabdec6ee0f41d3ef64d0880f829f8213649bacebe520996abd09c450b', NULL, '2017-10-25 10:10:51', '2017-10-25 10:10:51', NULL, 0),
(19, 'test1@euphorbia.es', '6407d234e9864f171c9ec07f69c0a7f114eb1f9200e67abce5e73651772091690b416d126e72c97f064e16479e97a77c9bb9d1b012f270f7e138225a6a1d4eab', '6a3c5fd496db1a1d134d7b28c3e147a0b4df4ee37e68b44844393952fd6d3199764438daa99864bab058a52e9cbeeae8997940652cb1038249bd05b445e1bd17', NULL, '2017-10-25 10:12:12', '2017-10-25 10:12:12', NULL, 0),
(20, 'test2@euphorbia.es', '4529fffcaffc12a30c77274502d4381c652123ec7fde8f0b66d1f85a5a28b753f9281650434b15ac9d8551653955da8e9be32319b762857496fa7cff8b87fba2', 'c50ec2c118bc28ce9e62d404ca72b925c05ed144462af83691d9a09464404cd9c4c9e9c04d9d512b10088bcdb84b29be24b1486e885691501f97e0eb49c0106c', NULL, '2017-10-25 10:15:05', '2017-10-25 10:15:05', NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users__data`
--

DROP TABLE IF EXISTS `users__data`;
CREATE TABLE IF NOT EXISTS `users__data` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_user` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `apellidos` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `dni` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `razon` varchar(180) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `direccion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `poblacion` varchar(120) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `id_pais` int(11) DEFAULT NULL,
  `id_provincia` int(11) DEFAULT NULL,
  `codigo_postal` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_USERS_DATA_USER_ID` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `users__data`
--

INSERT INTO `users__data` (`id`, `id_user`, `nombre`, `apellidos`, `dni`, `razon`, `direccion`, `poblacion`, `id_pais`, `id_provincia`, `codigo_postal`, `email`, `telefono`, `fecha_nacimiento`, `created_at`, `updated_at`) VALUES
(3, 2, 'Lorena', 'Fernandez', '12345678P', 'Lorena Fernandez', 'Santuario 3 ', 'Valladolid', 1, 49, '47014', 'lorena@euphorbia.es', '987987987', NULL, NULL, NULL),
(11, 5, 'César', 'Vega Martín', '12345645', 'Pepe Perez', 'c/ 1234', 'Valladolid', 1, 49, '4014', 'cesar@euphorbia.es', '601181340', '1988-06-20', '2017-08-21 08:57:53', '2017-10-27 09:30:19'),
(14, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 10:10:51', '2017-10-25 10:10:51'),
(15, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 10:12:12', '2017-10-25 10:12:12'),
(16, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 10:15:05', '2017-10-25 10:15:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users__files`
--

DROP TABLE IF EXISTS `users__files`;
CREATE TABLE IF NOT EXISTS `users__files` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `owner_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'propietario (id)',
  `owner_type` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'propietario (mod)',
  `type` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'tipo de archivo',
  `title_1` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'título',
  `name` text COLLATE utf8_spanish2_ci COMMENT 'nombre original',
  `mime_type` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'tipo recogido',
  `size` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'tamaño',
  `height` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'alto (si imagen)',
  `width` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ancho (si imagen)',
  `creacion` timestamp NULL DEFAULT NULL,
  `modificacion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `owner_id` (`owner_id`,`owner_type`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users__login_attempts`
--

DROP TABLE IF EXISTS `users__login_attempts`;
CREATE TABLE IF NOT EXISTS `users__login_attempts` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `time` datetime DEFAULT NULL,
  `ip` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users__rel_roles`
--

DROP TABLE IF EXISTS `users__rel_roles`;
CREATE TABLE IF NOT EXISTS `users__rel_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED NOT NULL,
  `role_id` varchar(20) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_2` (`user_id`,`role_id`),
  UNIQUE KEY `user_id_3` (`user_id`,`role_id`),
  UNIQUE KEY `user_id_4` (`user_id`,`role_id`),
  KEY `user_id` (`user_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `users__rel_roles`
--

INSERT INTO `users__rel_roles` (`id`, `user_id`, `role_id`) VALUES
(2, 2, 'administrator'),
(70, 5, 'administrator'),
(3, 5, 'master'),
(76, 5, 'publisher'),
(73, 5, 'root'),
(75, 6, 'publisher'),
(77, 7, 'administrator');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users__roles`
--

DROP TABLE IF EXISTS `users__roles`;
CREATE TABLE IF NOT EXISTS `users__roles` (
  `id` varchar(20) CHARACTER SET utf8 NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `users__roles`
--

INSERT INTO `users__roles` (`id`, `name`, `level`) VALUES
('administrator', 'Administrador', 1),
('master', 'Master', 2),
('publisher', 'Publisher', 3),
('root', 'Root', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users__tokens`
--

DROP TABLE IF EXISTS `users__tokens`;
CREATE TABLE IF NOT EXISTS `users__tokens` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) UNSIGNED DEFAULT NULL,
  `codigo` varchar(32) COLLATE utf8_spanish2_ci NOT NULL,
  `creacion` timestamp NULL DEFAULT NULL,
  `modificacion` timestamp NULL DEFAULT NULL,
  `expiracion` timestamp NULL DEFAULT NULL,
  `valido` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=1266 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `users__tokens`
--

INSERT INTO `users__tokens` (`id`, `id_usuario`, `codigo`, `creacion`, `modificacion`, `expiracion`, `valido`) VALUES
(979, NULL, '7hIXEnyyFkAdwgmgHstvE1', '2017-09-07 06:48:15', '2017-09-07 07:35:08', '2017-09-07 07:35:08', 0),
(980, NULL, 'mG3oBXRlh98XNAdB9k5iK', '2017-09-07 07:35:08', '2017-09-11 04:15:27', '2017-09-11 04:15:27', 0),
(981, NULL, '2oE9pFw316plHgRZQ8SaHV', '2017-09-11 04:15:19', '2017-09-11 04:57:25', '2017-09-11 04:57:25', 0),
(982, 5, 'w3FPKizrNDWNQvuRmh4Ah', '2017-09-11 04:57:26', '2017-09-11 05:04:05', '2017-09-11 05:04:05', 0),
(983, 5, '3Q7rKOkJo7RYu4i0zTNpKB', '2017-09-11 05:04:10', '2017-09-11 05:11:31', '2017-09-11 05:11:31', 0),
(984, 5, 'yVdYVRaYQ3BDKrtZjuqKy', '2017-09-11 05:11:38', '2017-09-11 05:13:55', '2017-09-11 05:13:55', 0),
(985, 5, '32PgvyItOAecq8jVQSBjbU', '2017-09-11 05:14:00', '2017-09-11 05:15:23', '2017-09-11 05:15:23', 0),
(986, 5, '1AZTecggK05cl5Q67hSNqH', '2017-09-11 05:15:27', '2017-09-11 05:16:47', '2017-09-11 05:16:47', 0),
(987, 5, '2FhKf2JmpiWSlmMVL27ZJb', '2017-09-11 05:16:49', '2017-09-11 05:18:08', '2017-09-11 05:18:08', 0),
(988, NULL, '5wfjWIMOXT6pxa90DctkG0', '2017-09-11 05:18:11', '2017-09-11 05:59:00', '2017-09-11 05:59:00', 0),
(989, 5, 'HG43bcpV2w5LqzRviYD4u', '2017-09-11 05:18:22', '2017-09-11 05:21:08', '2017-09-11 05:21:08', 0),
(990, 5, '2GsFrhvVIWrzuOE0Fr8Ev3', '2017-09-11 05:21:11', '2017-09-11 05:21:59', '2017-09-11 05:21:59', 0),
(991, NULL, '2fvFlPhvMA7AJt47gmICLP', '2017-09-11 05:22:21', '2017-09-11 05:59:00', '2017-09-11 05:59:00', 0),
(992, NULL, '45vSuTdoGLOmFso7y0OdWC', '2017-09-11 05:22:21', '2017-09-11 05:22:29', '2017-09-11 05:22:29', 0),
(993, NULL, '3r2TVODysPoEy9ieEyRIQm', '2017-09-11 05:22:32', '2017-09-11 05:59:00', '2017-09-11 05:59:00', 0),
(994, 5, '3oZt5ZdVnSsUoFDdLk8PWW', '2017-09-11 05:22:35', '2017-09-11 05:41:03', '2017-09-11 05:41:03', 0),
(995, NULL, 'h1cT4UIaHfHwrmT513LH6', '2017-09-11 05:41:08', '2017-09-11 05:59:00', '2017-09-11 05:59:00', 0),
(996, 5, '7l8pz4ZPwqdSiRGevE5jLL', '2017-09-11 05:59:04', '2017-09-11 06:27:29', '2017-09-11 06:27:29', 0),
(997, 5, '1gYQeTz8U0TCUisijJBnlh', '2017-09-11 06:27:38', '2017-09-11 06:46:29', '2017-09-11 06:46:29', 0),
(998, NULL, '70VouI029D14G76HzXo9qg', '2017-09-11 06:46:33', '2017-09-11 07:23:36', '2017-09-11 07:23:36', 0),
(999, NULL, '2axXCoNxBTSuGozs4pMBor', '2017-09-11 07:36:02', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1000, NULL, '3SXOiNsF39MHXj4c92bu27', '2017-09-11 08:05:58', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1001, NULL, '7imUP4PsunDixJEO9oSxJE', '2017-09-11 08:06:20', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1002, NULL, '1BVC6Ce8uyVImNJSF7AWUv', '2017-09-11 08:06:25', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1003, NULL, '2HkWTky6KPhNmTtcKjMTKm', '2017-09-11 08:07:15', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1004, NULL, '5tO7o30tZbaxyPSidkMhYN', '2017-09-11 08:07:15', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1005, NULL, '2l3Dd7Ii77mk2XWJDxFeLa', '2017-09-11 08:32:29', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1006, NULL, '256fvYSFrcqiaAgZt3DhZf', '2017-09-11 08:32:30', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1007, NULL, '6pw26Nmc8EsORRFLMlJuco', '2017-09-11 08:33:49', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1008, NULL, '2VOvnPqAXRYWMTm2R4Mdve', '2017-09-11 08:34:20', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1009, NULL, '5t9ToWCn4yPwsnhPwDO0EI', '2017-09-11 08:34:21', '2017-09-11 10:29:31', '2017-09-11 10:29:31', 0),
(1010, NULL, '4ZAXN1eFZtbak28kJDbOo8', '2017-09-11 10:29:31', '2017-09-12 05:21:35', '2017-09-12 05:21:35', 0),
(1011, NULL, '1yq9T2hOMAqE6MoF4N3i2B', '2017-09-12 05:21:35', '2017-09-13 04:12:34', '2017-09-13 04:12:34', 0),
(1012, NULL, '7dXxYq0uaGCAHMyaqm0Izt', '2017-09-13 04:12:34', '2017-09-13 06:07:02', '2017-09-13 06:07:02', 0),
(1013, 5, '5shli58YBVFu4jTXoBix1j', '2017-09-13 06:07:17', '2017-09-13 07:24:47', '2017-09-13 07:24:47', 0),
(1014, NULL, '2ZryaC7TCbUze2Ta1Eu1Cg', '2017-09-13 07:25:01', '2017-09-13 08:47:46', '2017-09-13 08:47:46', 0),
(1015, NULL, '5pkrMtcQb3Fn3US2vhpF8o', '2017-09-13 08:45:56', '2017-09-13 09:17:22', '2017-09-13 09:17:22', 0),
(1016, NULL, 'zTJjopVbZ74AAKZ9tQOHV', '2017-09-13 08:45:59', '2017-09-14 05:07:53', '2017-09-14 05:07:53', 0),
(1017, NULL, '1sITYbuU62lKKcbblVcUId', '2017-09-13 10:26:13', '2017-09-14 05:07:53', '2017-09-14 05:07:53', 0),
(1018, 5, '79o9yJlXddOHScJgh8EPNm', '2017-09-13 10:26:13', '2017-09-13 10:42:16', '2017-09-13 10:42:16', 0),
(1019, 5, '19mweB4Y1vD6n97JXqYwxI', '2017-09-13 10:42:19', '2017-09-13 10:44:44', '2017-09-13 10:44:44', 0),
(1020, NULL, '1kGK2PYmT7yDNEL3TlcEx5', '2017-09-13 10:44:50', '2017-09-14 05:07:53', '2017-09-14 05:07:53', 0),
(1021, 5, '4czlnI63nwaLbIip701QXI', '2017-09-13 10:44:58', '2017-09-13 10:46:14', '2017-09-13 10:46:14', 0),
(1022, 5, '3uPzz9bdnSLKN1GIoIZSkO', '2017-09-13 10:46:19', '2017-09-13 10:47:45', '2017-09-13 10:47:45', 0),
(1023, NULL, '5njj1VFfqxkzzadzppjf4D', '2017-09-13 10:48:00', '2017-09-14 05:07:53', '2017-09-14 05:07:53', 0),
(1024, NULL, '7JgTENGfomjKdUnt39EPoA', '2017-09-14 05:07:53', '2017-09-14 06:08:24', '2017-09-14 06:08:24', 0),
(1025, NULL, 'GnGmrtlAmxiwF6CE5jjRB', '2017-09-14 05:07:54', '2017-09-14 06:08:24', '2017-09-14 06:08:24', 0),
(1026, 5, '1qn9zd51sotRBVi6AWhw0h', '2017-09-14 06:08:59', '2017-09-14 06:11:41', '2017-09-14 06:11:41', 0),
(1027, 5, '37mbwHJuciNp2tFdLhEQpb', '2017-09-14 06:11:29', '2017-09-14 06:17:29', '2017-09-14 06:17:29', 0),
(1028, 5, '5Kgx4qN3oM1DBXDe5XhI3E', '2017-09-14 06:17:35', '2017-09-14 06:33:03', '2017-09-14 06:33:03', 0),
(1029, 5, '6ftAzKW9nZIthzSOMhQ4l6', '2017-09-14 06:33:10', '2017-09-14 07:40:49', '2017-09-14 07:40:49', 0),
(1030, 5, '5XZBsVOrarSz9Xyb9v9cOO', '2017-09-14 07:46:59', '2017-09-14 09:01:27', NULL, 0),
(1031, NULL, '7Ymp5j7Y3aEJAhYSoI81N6', '2017-09-15 05:05:40', '2017-09-15 05:35:45', '2017-09-15 05:35:45', 0),
(1032, NULL, '1ZhGUzNyODbj3OrfrwdPOo', '2017-09-15 05:14:29', '2017-09-15 05:35:45', '2017-09-15 05:35:45', 0),
(1033, NULL, '75AS2UNnwmoU2TPKn38RR1', '2017-09-15 05:23:17', '2017-09-15 05:35:45', '2017-09-15 05:35:45', 0),
(1034, NULL, '77EMBtMdXf1Nh4CVy8TVji', '2017-09-15 05:23:23', '2017-09-15 05:35:45', '2017-09-15 05:35:45', 0),
(1035, NULL, '30uKGeuiYzTJSZd2k1oSUx', '2017-09-15 05:23:42', '2017-09-15 05:35:45', '2017-09-15 05:35:45', 0),
(1036, NULL, '4MEigBpF8OzUV1HviFZ34K', '2017-09-15 05:29:31', '2017-09-15 05:35:45', '2017-09-15 05:35:45', 0),
(1037, 5, '1AGQCmvHR0lrb71e5IA6fM', '2017-09-15 05:31:39', '2017-09-15 05:35:11', '2017-09-15 05:35:11', 0),
(1038, NULL, '13oJaKfQPYA8aue1OR8MIj', '2017-09-15 05:35:18', '2017-09-15 05:35:54', '2017-09-15 05:35:54', 0),
(1039, NULL, '2nD7KVMirkgSNg3yfG6oOy', '2017-09-15 05:35:59', '2017-09-15 05:40:44', '2017-09-15 05:40:44', 0),
(1040, NULL, '4jQbYk5R4Hi3melpXVlHFn', '2017-09-15 05:40:44', '2017-09-15 05:42:24', '2017-09-15 05:42:24', 0),
(1041, NULL, '47VIeja3mhyk6J6WcsuOk9', '2017-09-15 05:40:44', '2017-09-15 05:42:24', '2017-09-15 05:42:24', 0),
(1042, NULL, '49PfoMA9zppinFYLf4EUDV', '2017-09-15 05:42:24', '2017-09-15 05:43:37', '2017-09-15 05:43:37', 0),
(1043, NULL, '6vV0TRBlDfhtyUjftURdxY', '2017-09-15 05:43:37', '2017-09-15 06:22:10', '2017-09-15 06:22:10', 0),
(1044, 5, '2rG7p5HRuCarC1emAevrDO', '2017-09-15 05:44:07', '2017-09-15 06:40:51', '2017-09-15 06:40:51', 0),
(1045, 5, '1tkl9kIFTvMTnT8ZuCQ4xr', '2017-09-15 06:22:10', '2017-09-15 07:45:12', '2017-09-15 07:45:12', 0),
(1046, NULL, '6k5L6CvahxuGBdMo8RwqOv', '2017-09-15 06:38:32', '2017-09-15 07:45:12', '2017-09-15 07:45:12', 0),
(1047, 5, '5Xywy2yrBaeUxyxBNpWiuv', '2017-09-15 07:45:12', '2017-09-15 07:56:27', '2017-09-15 07:56:27', 0),
(1048, 5, 'vuVAeoWEnybYRg4uS1cbh', '2017-09-15 07:56:29', '2017-09-15 07:57:56', '2017-09-15 07:57:56', 0),
(1049, 5, '4FHDwEn5T6p9uOqg9Q0HUd', '2017-09-15 07:58:00', '2017-09-15 07:58:36', '2017-09-15 07:58:36', 0),
(1050, 5, 'GBovSjepg3j1ua2nZ6xp3', '2017-09-15 07:58:59', '2017-09-15 08:00:08', '2017-09-15 08:00:08', 0),
(1051, 5, '7EUYFmDB94Q08Xg6Ntd7zy', '2017-09-15 08:00:25', '2017-09-15 08:00:32', '2017-09-15 08:00:32', 0),
(1052, 5, '6vs1AuaSrakcXxFOeeWHLn', '2017-09-15 08:00:54', '2017-09-15 08:01:18', '2017-09-15 08:01:18', 0),
(1053, 5, '6KbYCnQtucr23Qxsdw7L84', '2017-09-15 08:01:22', '2017-09-15 08:04:22', '2017-09-15 08:04:22', 0),
(1054, 5, '6ha1LWTeKQh16s4DWBjtu9', '2017-09-15 08:04:32', '2017-09-15 08:04:46', '2017-09-15 08:04:46', 0),
(1055, 5, '6UPCBMxqYyq5vj4OgbYpcw', '2017-09-15 08:04:50', '2017-09-15 08:05:22', '2017-09-15 08:05:22', 0),
(1056, 5, '2V5FGzq8MJed3GAeCJNM98', '2017-09-15 08:56:22', '2017-09-15 08:57:19', '2017-09-15 08:57:19', 0),
(1057, 5, '1kCP4egNe0V2FKsa6vs79o', '2017-09-15 08:59:48', '2017-09-15 09:00:54', '2017-09-15 09:00:54', 0),
(1058, NULL, '59OblX77v0BaECD9RT2VGY', '2017-09-15 09:00:22', '2017-09-15 09:33:00', '2017-09-15 09:33:00', 0),
(1059, 5, '3nwq9eNbAe9ZbaBfZgbUEQ', '2017-09-15 09:00:47', '2017-09-15 09:01:05', '2017-09-15 09:01:05', 0),
(1060, 5, '4m3M3PzYxSYqg7oQsBDvRX', '2017-09-15 09:01:41', '2017-09-15 09:01:53', '2017-09-15 09:01:53', 0),
(1061, 5, '63D9N35d6C4sqq89Jh71yD', '2017-09-15 09:03:01', '2017-09-15 09:03:18', '2017-09-15 09:03:18', 0),
(1062, 5, '6AvBYvqan2P7VXYeykgcnp', '2017-09-15 09:03:31', '2017-09-15 09:06:28', '2017-09-15 09:06:28', 0),
(1063, 5, '7JNT9NOBtpuUqflBaHV1as', '2017-09-15 09:06:33', '2017-09-15 09:06:58', '2017-09-15 09:06:58', 0),
(1064, 5, 'lODxLk6tpjJPcMmsFzO0C', '2017-09-15 09:06:49', '2017-09-15 09:07:05', '2017-09-15 09:07:05', 0),
(1065, 5, 'W9tvQTLB2C0JY9Y2TdrpP', '2017-09-15 09:07:12', '2017-09-15 09:07:22', '2017-09-15 09:07:22', 0),
(1066, 5, '3NkQkt9vMOVnIwX7Uidaig', '2017-09-15 09:07:25', '2017-09-15 09:08:30', '2017-09-15 09:08:30', 0),
(1067, 5, '3LeTTDPN42SZYFto3fkwCC', '2017-09-15 09:09:23', '2017-09-15 10:04:03', '2017-09-15 10:04:03', 0),
(1068, NULL, '6RsHx3PDHSoaVIMq1kZdwd', '2017-09-15 10:03:29', '2017-09-15 10:40:55', '2017-09-15 10:40:55', 0),
(1069, 5, '7RFQ2ee5u8efMFpVLVYjTp', '2017-09-15 10:03:30', '2017-09-15 10:40:55', '2017-09-15 10:40:55', 0),
(1070, NULL, '1yw4cT9KCBE5PCHFVSrRhT', '2017-09-15 10:40:55', '2017-10-02 10:47:16', '2017-10-02 10:47:16', 0),
(1071, NULL, '1V3tBHLod3lFShrE3npZQU', '2017-09-15 10:40:56', '2017-10-02 10:47:16', '2017-10-02 10:47:16', 0),
(1072, 5, 'JcRz5HFgfppjZlAFDRMxI', '2017-09-15 10:40:56', '2017-10-02 10:47:16', '2017-10-02 10:47:16', 0),
(1073, NULL, '5RknLQRKjcoHaz4P1EKWZT', '2017-10-02 10:47:16', '2017-10-06 06:08:41', '2017-10-06 06:08:41', 0),
(1074, NULL, '52Wd2KWNFw0VRxNtF1cbBh', '2017-10-06 06:08:49', '2017-10-06 07:31:54', '2017-10-06 07:31:54', 0),
(1075, NULL, '23TObcglkSHDzTAx337gnY', '2017-10-06 06:09:12', '2017-10-06 07:31:54', '2017-10-06 07:31:54', 0),
(1076, NULL, '5mRBiLtGYBvO67Hk7HnGl9', '2017-10-06 07:31:54', '2017-10-06 08:39:22', '2017-10-06 08:39:22', 0),
(1077, NULL, '7AqCcPbcN0I5gbGKQtZEjI', '2017-10-06 07:31:54', '2017-10-06 08:39:22', '2017-10-06 08:39:22', 0),
(1078, NULL, 'NifxmEp3Q6h3WgQUV5QBt', '2017-10-06 08:39:21', '2017-10-06 09:11:15', '2017-10-06 09:11:15', 0),
(1079, NULL, '1UmADX0bzU9dldVH1JxBQ5', '2017-10-06 08:39:22', '2017-10-06 09:11:15', '2017-10-06 09:11:15', 0),
(1080, NULL, '4mudDuMWyt4xy5mX0C3Djd', '2017-10-06 09:11:15', '2017-10-09 05:28:34', '2017-10-09 05:28:34', 0),
(1081, NULL, 'SgOVnTpXNOMoSwXePMvji', '2017-10-06 09:11:15', '2017-10-09 05:28:34', '2017-10-09 05:28:34', 0),
(1082, NULL, '6QeyICsNNsEaAChGNh3P9c', '2017-10-09 05:43:23', '2017-10-09 06:14:08', '2017-10-09 06:14:08', 0),
(1083, NULL, 'ufEF0zJC42FId1wizp5vt', '2017-10-09 05:43:24', '2017-10-09 06:14:08', '2017-10-09 06:14:08', 0),
(1084, NULL, '5Cpy2Lw6y0aKvDAIjXEqPi', '2017-10-09 06:14:08', '2017-10-09 06:44:22', '2017-10-09 06:44:22', 0),
(1085, NULL, '1TmcwcyB7rWwNgckKOpQWv', '2017-10-09 06:14:08', '2017-10-09 06:44:22', '2017-10-09 06:44:22', 0),
(1086, NULL, '6CS1VcNT3KGsdxcydPNzxG', '2017-10-09 06:44:22', '2017-10-09 07:14:39', '2017-10-09 07:14:39', 0),
(1087, NULL, '33aAhE6bbYsV33SGtdtyTz', '2017-10-09 06:44:22', '2017-10-09 07:14:39', '2017-10-09 07:14:39', 0),
(1088, NULL, '1Eh9jB4JDHwaIPufeHLAAr', '2017-10-09 07:14:39', '2017-10-09 07:56:32', '2017-10-09 07:56:32', 0),
(1089, NULL, '4xF5SzGTohD2FkJHBoaTsN', '2017-10-09 07:14:39', '2017-10-09 07:56:32', '2017-10-09 07:56:32', 0),
(1090, NULL, '2cwPQcuT5i0JmtZOwO0GdI', '2017-10-09 07:14:42', '2017-10-09 07:56:32', '2017-10-09 07:56:32', 0),
(1091, NULL, '1RZzLRBbXZlyLAJCkfo2Cf', '2017-10-09 07:14:42', '2017-10-09 07:56:32', '2017-10-09 07:56:32', 0),
(1092, NULL, '6YCZblS86ItoFnpUzQ5wto', '2017-10-09 07:56:31', '2017-10-09 08:31:34', '2017-10-09 08:31:34', 0),
(1093, NULL, '5T4BqT9AgujTaXiLt8LzdO', '2017-10-09 07:56:32', '2017-10-09 08:31:34', '2017-10-09 08:31:34', 0),
(1094, NULL, '1RGTrvzxKuIxdnC806E0Ae', '2017-10-09 08:31:34', '2017-10-09 09:02:47', '2017-10-09 09:02:47', 0),
(1095, NULL, '6C5kQEAxt4FVKxrM9qeapM', '2017-10-09 08:31:34', '2017-10-09 09:02:47', '2017-10-09 09:02:47', 0),
(1096, NULL, 'ofdF0QiHIK680Fli3Yjbe', '2017-10-09 09:02:46', '2017-10-09 09:34:08', '2017-10-09 09:34:08', 0),
(1097, NULL, 'FRA65gs1tDTTmY3raEIN', '2017-10-09 09:02:46', '2017-10-09 09:34:08', '2017-10-09 09:34:08', 0),
(1098, NULL, '4BqloM8uVJ9pEo23FzYjrA', '2017-10-09 09:34:07', '2017-10-09 10:10:48', '2017-10-09 10:10:48', 0),
(1099, NULL, '53e7WXbMr6iy1O5ACLCvdG', '2017-10-09 10:10:48', '2017-10-09 10:42:07', '2017-10-09 10:42:07', 0),
(1100, NULL, '1T6vikQzEoz598YjSjijRj', '2017-10-09 10:42:07', '2017-10-10 04:07:08', '2017-10-10 04:07:08', 0),
(1101, NULL, '4YeFNk9nib1F7RnbxOPuhz', '2017-10-10 04:07:08', '2017-10-10 04:37:49', '2017-10-10 04:37:49', 0),
(1102, NULL, '44RjxIOEjpzJlaywjAMxKI', '2017-10-10 04:37:48', '2017-10-10 05:08:36', '2017-10-10 05:08:36', 0),
(1103, NULL, '3mNc8oBfhBbvePh3wMfFrq', '2017-10-10 05:08:36', '2017-10-10 05:38:57', '2017-10-10 05:38:57', 0),
(1104, NULL, '7H5maHRQlQH3Xza2cNaME0', '2017-10-10 05:38:57', '2017-10-10 06:09:39', '2017-10-10 06:09:39', 0),
(1105, NULL, '41tUNvZQonrHe1U0KCCfa', '2017-10-10 06:09:39', '2017-10-10 06:40:47', '2017-10-10 06:40:47', 0),
(1106, NULL, 'QiOm3IxUO6Wv5I9c43H3J', '2017-10-10 06:40:47', '2017-10-10 07:16:32', '2017-10-10 07:16:32', 0),
(1107, NULL, '3yYoyOGLiCw95Cu8DkUDzW', '2017-10-10 07:16:32', '2017-10-10 07:50:10', '2017-10-10 07:50:10', 0),
(1108, NULL, '5QMN8fzlXzXMQRBIB0lW5h', '2017-10-10 07:50:10', '2017-10-10 08:21:09', '2017-10-10 08:21:09', 0),
(1109, NULL, '1Ksl5RkUTM7yqQKPKa878c', '2017-10-10 08:21:09', '2017-10-10 08:51:36', '2017-10-10 08:51:36', 0),
(1110, NULL, 'Q9mknMdRFWI4ktwcKfjM8', '2017-10-10 08:51:36', '2017-10-10 09:22:40', '2017-10-10 09:22:40', 0),
(1111, NULL, 'dN2CGiuF3jfuKuP1POr4w', '2017-10-10 09:22:39', '2017-10-10 09:52:55', '2017-10-10 09:52:55', 0),
(1112, NULL, '5yiujEpvEqcalXxKX3ZXBT', '2017-10-10 09:52:54', '2017-10-10 10:44:14', '2017-10-10 10:44:14', 0),
(1113, NULL, '5RMC3LCCQ9zWNrthPy9PFJ', '2017-10-10 10:44:14', '2017-10-11 04:18:19', '2017-10-11 04:18:19', 0),
(1114, NULL, '4JXh7IiriDtyxsqyHEFiZ1', '2017-10-11 04:18:18', '2017-10-11 04:57:08', '2017-10-11 04:57:08', 0),
(1115, NULL, 'mTjyMWZe453gXKq3rJOrJ', '2017-10-11 04:57:07', '2017-10-11 06:28:27', '2017-10-11 06:28:27', 0),
(1116, NULL, '2XCRdaRoJtxYeS8lLUHxUz', '2017-10-11 06:56:01', '2017-10-11 07:40:22', '2017-10-11 07:40:22', 0),
(1117, NULL, '3T7BFpSdOaeCMDF3Qb3vUa', '2017-10-11 07:40:21', '2017-10-11 08:21:41', '2017-10-11 08:21:41', 0),
(1118, NULL, '208jUhFoY8TZV3RhcDMQti', '2017-10-11 08:21:41', '2017-10-11 08:52:20', '2017-10-11 08:52:20', 0),
(1119, NULL, '4c4b2SQoJQZ8BPqC8KpJHB', '2017-10-11 08:52:20', '2017-10-11 09:53:15', '2017-10-11 09:53:15', 0),
(1120, NULL, '6Yj6dyjE8LHHORiX3SIPOC', '2017-10-11 09:53:14', '2017-10-11 11:05:51', '2017-10-11 11:05:51', 0),
(1121, NULL, '5fn64n5VEPKI3VS5ULwycX', '2017-10-11 11:05:51', '2017-10-13 04:16:07', '2017-10-13 04:16:07', 0),
(1122, NULL, '219DoyB8iBgqOi6Yasckl7', '2017-10-13 04:16:06', '2017-10-13 05:01:20', '2017-10-13 05:01:20', 0),
(1123, NULL, '16TbU0j2nhsqsA3mwdC3R7', '2017-10-13 05:01:20', '2017-10-13 05:36:37', '2017-10-13 05:36:37', 0),
(1124, NULL, '6E8D3qnK9q72cgWWJ11sKX', '2017-10-13 05:36:37', '2017-10-13 06:06:48', '2017-10-13 06:06:48', 0),
(1125, NULL, '46jwwQTCQbVqNzI0j7FALo', '2017-10-13 06:06:48', '2017-10-13 07:09:05', '2017-10-13 07:09:05', 0),
(1126, NULL, '3T4ljuEZSBo1rzF2ebGFb0', '2017-10-13 07:09:04', '2017-10-13 07:39:07', '2017-10-13 07:39:07', 0),
(1127, NULL, '4PK4hSHRvrCBh4aUEGloDk', '2017-10-13 07:39:06', '2017-10-13 10:01:48', '2017-10-13 10:01:48', 0),
(1128, NULL, '6pOXCShVlhdgEIEw0R9vra', '2017-10-13 10:01:48', '2017-10-13 10:36:26', '2017-10-13 10:36:26', 0),
(1129, NULL, '4fuqTeVI50WkCfuhyLtNyP', '2017-10-13 10:36:23', '2017-10-16 04:06:50', '2017-10-16 04:06:50', 0),
(1130, NULL, '2yiKeqz9GKO7vTAL1VoPMP', '2017-10-16 04:06:49', '2017-10-16 04:36:54', '2017-10-16 04:36:54', 0),
(1131, NULL, '2OT2VjVWK5Ie1uqhlc8cs3', '2017-10-16 04:36:54', '2017-10-18 07:56:22', '2017-10-18 07:56:21', 0),
(1132, NULL, '3esCxYxLai67C27Bvpwrf3', '2017-10-18 07:56:49', '2017-10-23 04:25:13', '2017-10-23 04:25:13', 0),
(1133, NULL, '4rBXbzbUJgNa2HiZOX9UDh', '2017-10-23 04:25:13', '2017-10-23 05:02:17', '2017-10-23 05:02:17', 0),
(1134, NULL, '3Ikdmns4rTknRjlYREV3cx', '2017-10-23 05:02:44', '2017-10-23 09:03:56', '2017-10-23 09:03:56', 0),
(1135, NULL, '2QIW0xesEBW3FjLorQKaCy', '2017-10-23 09:03:55', '2017-10-23 09:41:36', '2017-10-23 09:41:36', 0),
(1136, NULL, '7MQXJ8xa3qx6vhFn6qzmIt', '2017-10-23 09:41:36', '2017-10-23 10:12:04', '2017-10-23 10:12:04', 0),
(1137, NULL, '7N5NhJu3QIb0rcmAY3aQXm', '2017-10-23 09:41:36', '2017-10-23 10:32:28', '2017-10-23 10:32:28', 0),
(1138, NULL, 'HE0vdI5hsXe35xNjdxuFI', '2017-10-23 10:12:04', '2017-10-23 10:44:02', '2017-10-23 10:44:02', 0),
(1139, 5, '6agVYDyLsYnWTXrbaS1iWp', '2017-10-23 10:12:04', '2017-10-23 10:28:37', '2017-10-23 10:28:37', 0),
(1140, 5, '2uqFsUldN5vB3MMCvc8eWN', '2017-10-23 10:28:39', '2017-10-23 10:56:50', '2017-10-23 10:56:50', 0),
(1141, 5, '1I3eKMvuMVDOPyYh9b05di', '2017-10-23 10:56:55', '2017-10-23 10:59:33', '2017-10-23 10:59:33', 0),
(1142, 5, '1XiAnnI4TcW8ycWXmBs8Dw', '2017-10-23 10:59:35', '2017-10-24 04:35:09', '2017-10-24 04:35:09', 0),
(1143, NULL, '6MI1VsDZOeRCkJ5azDA5Gh', '2017-10-24 04:35:09', '2017-10-24 05:05:13', '2017-10-24 05:05:13', 0),
(1144, NULL, '3MBWfLfvUKjapB2qCXIPRj', '2017-10-24 05:05:13', '2017-10-24 06:03:38', '2017-10-24 06:03:38', 0),
(1145, 5, '4J1w1emhSzmzoTh83xuQca', '2017-10-24 05:05:13', '2017-10-24 06:03:38', '2017-10-24 06:03:38', 0),
(1146, NULL, '3b5JVi1XRoH3uQiYBoyNjn', '2017-10-24 06:03:35', '2017-10-24 09:25:55', '2017-10-24 09:25:55', 0),
(1147, 5, 'RidFdCLFFbhQJfDxJF9Dg', '2017-10-24 06:03:38', '2017-10-24 09:25:55', '2017-10-24 09:25:55', 0),
(1148, NULL, '7jTAh9e4EzxbKHjHj9QnYs', '2017-10-24 09:25:55', '2017-10-24 10:07:51', '2017-10-24 10:07:51', 0),
(1149, NULL, '1vTELti4HCffMGhR6AJi3U', '2017-10-24 09:25:55', '2017-10-24 10:07:51', '2017-10-24 10:07:51', 0),
(1150, 5, '4Ut33iJqiAoXKjYUvsM7sJ', '2017-10-24 09:25:55', '2017-10-24 10:20:12', '2017-10-24 10:20:12', 0),
(1151, NULL, '4eKq6xTCc6AESQX0HWTNGC', '2017-10-24 10:17:07', '2017-10-24 10:47:47', '2017-10-24 10:47:47', 0),
(1152, 5, '3q8iqxw2PpFJc7o8t7w8Mp', '2017-10-24 10:17:07', '2017-10-24 10:51:36', '2017-10-24 10:51:36', 0),
(1153, NULL, '7ZdR2qzZAklyZG10rDrCTB', '2017-10-24 10:51:36', '2017-10-25 04:38:06', '2017-10-25 04:38:06', 0),
(1154, NULL, '2MKm7S1VFx38ykp0X8nziK', '2017-10-24 10:51:36', '2017-10-25 04:38:06', '2017-10-25 04:38:06', 0),
(1155, 5, '7T0Az8Zu2DV65pD5AoanFf', '2017-10-25 04:38:06', '2017-10-25 04:57:18', '2017-10-25 04:57:18', 0),
(1156, NULL, 'PNlBkRhCWtYl7m0hqiK3L', '2017-10-25 04:59:25', '2017-10-25 07:14:31', '2017-10-25 07:14:31', 0),
(1157, NULL, '7Y2Qy1LCRQdRsyg7InmKZt', '2017-10-25 07:14:30', '2017-10-25 08:18:18', '2017-10-25 08:18:18', 0),
(1158, NULL, '3eHqDtcFGaI6wYHVgQMCU', '2017-10-25 08:18:19', '2017-10-25 08:48:20', '2017-10-25 08:48:20', 0),
(1159, NULL, '6gV1yjiNcPCWcFXfG5yZLV', '2017-10-25 08:18:19', '2017-10-25 08:48:20', '2017-10-25 08:48:20', 0),
(1160, 5, '6V34ZewW7FDw78LRLxtIQs', '2017-10-25 08:18:19', '2017-10-25 08:22:13', '2017-10-25 08:22:13', 0),
(1161, 5, '18mrqgaV83mUC2RGjWkOM5', '2017-10-25 08:22:51', '2017-10-25 08:25:54', '2017-10-25 08:25:54', 0),
(1162, 5, '2hDiHFpCOxI8Eb5AU9Ta5M', '2017-10-25 08:25:54', '2017-10-25 08:27:37', '2017-10-25 08:27:37', 0),
(1163, 5, '1Yt4zKgtIm7EUDCS8dRRUY', '2017-10-25 08:27:37', '2017-10-25 08:27:53', '2017-10-25 08:27:53', 0),
(1164, 5, '51ibmQOFvNhGQ6ukHryYqL', '2017-10-25 08:27:53', '2017-10-25 08:38:44', '2017-10-25 08:38:44', 0),
(1165, 5, '1QeNhu0TgLglU8AuTLxNre', '2017-10-25 08:38:44', '2017-10-25 08:49:31', '2017-10-25 08:49:31', 0),
(1166, 5, '74H0Eecx8vhrM49LNm2EZJ', '2017-10-25 08:49:31', '2017-10-25 08:49:51', '2017-10-25 08:49:51', 0),
(1167, 5, '2QJi8NBcFrV07rkxgZtxam', '2017-10-25 08:49:51', '2017-10-25 08:50:05', '2017-10-25 08:50:05', 0),
(1168, NULL, 'XId7vP39bG1bGUL7IJvmy', '2017-10-25 08:50:05', '2017-10-25 09:20:07', '2017-10-25 09:20:07', 0),
(1169, NULL, '22bpBJwHnHKg9k3fYh8LBC', '2017-10-25 09:20:07', '2017-10-25 09:50:36', '2017-10-25 09:50:36', 0),
(1170, NULL, '3QBLMveX18NnqXwnXIc3Sb', '2017-10-25 09:20:07', '2017-10-25 10:04:54', '2017-10-25 10:04:54', 0),
(1171, 5, '5FN4jTvCPI9d5LVYRkIdLv', '2017-10-25 09:37:44', '2017-10-25 09:38:07', '2017-10-25 09:38:07', 0),
(1172, NULL, '7ZAQhiD2XnwOaAxDJV9Vwr', '2017-10-25 09:38:01', '2017-10-25 10:08:22', '2017-10-25 10:08:22', 0),
(1173, 5, '1qQol8DlEBasB5eAAEqyg5', '2017-10-25 09:38:03', '2017-10-25 09:38:26', '2017-10-25 09:38:26', 0),
(1174, NULL, 'UuVuNRgMICa3ue7KyuXK', '2017-10-25 09:38:16', '2017-10-25 10:08:22', '2017-10-25 10:08:22', 0),
(1175, NULL, '3D4CQN5hT9sr16S51pNXVA', '2017-10-25 09:38:16', '2017-10-25 10:08:22', '2017-10-25 10:08:22', 0),
(1176, 5, '7fOKLfJbtH0kacDvZAsn3t', '2017-10-25 09:38:16', '2017-10-25 09:38:47', '2017-10-25 09:38:47', 0),
(1177, 14, '9lqZQ5eOfZJTfEcLRTMP5', '2017-10-25 09:38:47', '2017-10-25 09:42:36', '2017-10-25 09:42:36', 0),
(1178, 15, 'ZaqWdhELW0BC3QDrPk6Ac', '2017-10-25 09:42:36', '2017-10-25 09:43:49', '2017-10-25 09:43:49', 0),
(1179, 15, 'kblfW6DEAWmrmXJpLyxsz', '2017-10-25 09:43:49', '2017-10-25 09:44:10', '2017-10-25 09:44:10', 0),
(1180, 5, '6IsB1ZqWgskAH7vl9K6pp1', '2017-10-25 09:44:10', '2017-10-25 09:44:34', '2017-10-25 09:44:34', 0),
(1181, 15, '2zZ9KUeDnUZSVytx9dg2qw', '2017-10-25 09:44:34', '2017-10-25 10:11:46', '2017-10-25 10:11:46', 0),
(1182, 19, '6n9TzGPW1c7iO98XcWjkVq', '2017-10-25 10:11:46', '2017-10-25 10:14:49', '2017-10-25 10:14:49', 0),
(1183, 20, 'dI92YaYIPHXlpeVz95D2I', '2017-10-25 10:14:49', '2017-10-25 10:18:14', '2017-10-25 10:18:14', 0),
(1184, 5, '7M0Dtbz8EoMPqBvfKb2GeP', '2017-10-25 10:18:14', '2017-10-25 10:38:42', '2017-10-25 10:38:42', 0),
(1185, NULL, '1M9ztBIF9tzDicmWFxUHA4', '2017-10-25 10:38:42', '2017-10-26 04:41:00', '2017-10-26 04:41:00', 0),
(1186, NULL, '7IGlsx3aEdNXBbJ8hlz1t8', '2017-10-25 10:38:54', '2017-10-26 04:41:00', '2017-10-26 04:41:00', 0),
(1187, NULL, '2PcHdZbXT8DEf2bAibsC2n', '2017-10-25 10:51:39', '2017-10-26 04:41:00', '2017-10-26 04:41:00', 0),
(1188, NULL, '5IGO1pfjUF4DxGeFqHtsYO', '2017-10-25 10:51:50', '2017-10-26 04:41:00', '2017-10-26 04:41:00', 0),
(1189, NULL, '29eFyjHZAnyyonW2Uin5On', '2017-10-25 10:52:52', '2017-10-26 04:41:00', '2017-10-26 04:41:00', 0),
(1190, NULL, 'iNZYQeRxePqKdE01Oeo5w', '2017-10-25 10:54:59', '2017-10-26 04:41:00', '2017-10-26 04:41:00', 0),
(1191, NULL, 'fDjbYsxND2LY1gdmy49u', '2017-10-25 10:57:26', '2017-10-26 04:41:00', '2017-10-26 04:41:00', 0),
(1192, 5, '5cUrEnFSx2VBjbfUVIhaED', '2017-10-26 04:41:00', '2017-10-26 04:42:21', '2017-10-26 04:42:21', 0),
(1193, NULL, '3WMhwDVtouEy2BXbuEP9WY', '2017-10-26 04:42:21', '2017-10-26 05:13:01', '2017-10-26 05:13:01', 0),
(1194, NULL, '3P5xpsr7iVbAPtxm1aESeD', '2017-10-26 05:13:01', '2017-10-26 06:07:19', '2017-10-26 06:07:19', 0),
(1195, NULL, '1DndCiLLhfrerIrYXhfPGd', '2017-10-26 05:13:01', '2017-10-26 06:07:19', '2017-10-26 06:07:19', 0),
(1196, NULL, '7IIoAUFNySB5hFbikjLeki', '2017-10-26 06:10:15', '2017-10-26 07:11:36', '2017-10-26 07:11:36', 0),
(1197, NULL, '13OwkmpthaCXlwSxDoVXFS', '2017-10-26 06:12:35', '2017-10-26 07:11:36', '2017-10-26 07:11:36', 0),
(1198, NULL, '7M1HuKRqlerVNMQdIrxQey', '2017-10-26 06:12:43', '2017-10-26 07:11:36', '2017-10-26 07:11:36', 0),
(1199, NULL, '3jH3ysx8l2FYrseqa6b27i', '2017-10-26 06:12:55', '2017-10-26 07:11:36', '2017-10-26 07:11:36', 0),
(1200, NULL, '3zwy6UbWGMjbyXwftM18cc', '2017-10-26 07:11:36', '2017-10-26 07:43:24', '2017-10-26 07:43:24', 0),
(1201, NULL, 'jTAyqfqqzqDn3fiV0dx6T', '2017-10-26 07:11:37', '2017-10-26 07:43:24', '2017-10-26 07:43:24', 0),
(1202, NULL, '65eYXntsbeiF9ysJlkakME', '2017-10-26 07:43:24', '2017-10-26 08:15:33', '2017-10-26 08:15:33', 0),
(1203, NULL, '3EBpRSxrf9jxX4LMxlJN7g', '2017-10-26 07:43:25', '2017-10-26 08:15:33', '2017-10-26 08:15:33', 0),
(1204, NULL, '1LtPk3mPeAfnI0Sqa5oldF', '2017-10-26 08:15:33', '2017-10-26 08:46:05', '2017-10-26 08:46:05', 0),
(1205, NULL, 'ev8sqK6KUJ0ffXlNSk0WD', '2017-10-26 08:15:33', '2017-10-26 08:46:05', '2017-10-26 08:46:05', 0),
(1206, NULL, '6tcRYXq0S43DljVwtdSxPA', '2017-10-26 08:15:35', '2017-10-26 08:46:05', '2017-10-26 08:46:05', 0),
(1207, 5, '1NBxBvXDp9yVkuE1lGsEQM', '2017-10-26 08:15:35', '2017-10-26 08:35:05', '2017-10-26 08:35:05', 0),
(1208, 5, '1FyKTmqRb7CXataPpWAeqF', '2017-10-26 08:35:05', '2017-10-26 08:39:37', '2017-10-26 08:39:37', 0),
(1209, 5, '3t1jPNaGf9Ldv3akpGgqZN', '2017-10-26 08:39:38', '2017-10-26 09:28:18', '2017-10-26 09:28:18', 0),
(1210, 5, '7Q0fD3OBgAAwcUZ5BfG3oF', '2017-10-26 09:28:04', '2017-10-26 10:42:22', '2017-10-26 10:42:22', 0),
(1211, 5, '4PQRwhTYg8aJrSMF6n54E5', '2017-10-26 10:42:22', '2017-10-27 04:13:16', '2017-10-27 04:13:16', 0),
(1212, NULL, '4AUmOpAAcmwGhmqOkBRPl1', '2017-10-26 10:42:22', '2017-10-27 04:13:16', '2017-10-27 04:13:16', 0),
(1213, 5, '2hb5tB8TnOdidkDNFeZf8n', '2017-10-26 10:42:58', '2017-10-26 10:48:07', '2017-10-26 10:48:07', 0),
(1214, 5, '3hETJAQTzGTUz31vzdqgl', '2017-10-27 04:13:15', '2017-10-27 04:15:30', '2017-10-27 04:15:30', 0),
(1215, 5, '6MHNVhQpvy7lJGFlkc0Ijo', '2017-10-27 04:15:30', '2017-10-27 04:54:50', '2017-10-27 04:54:50', 0),
(1216, NULL, '6ii8YrkoC0rCTGlQ9hylOE', '2017-10-27 04:54:50', '2017-10-27 05:24:53', '2017-10-27 05:24:53', 0),
(1217, NULL, '3oGqoJOppD8KKq7bAUcvbv', '2017-10-27 04:54:50', '2017-10-27 06:01:59', '2017-10-27 06:01:59', 0),
(1218, NULL, '1MPPi8l5d7dsyPaZR6oDJK', '2017-10-27 05:26:33', '2017-10-27 06:01:59', '2017-10-27 06:01:59', 0),
(1219, 5, 'iAlbX63NNGIHAc1msdA9k', '2017-10-27 05:26:33', '2017-10-27 06:21:02', '2017-10-27 06:21:02', 0),
(1220, NULL, '4jDEdtkknmnlsvBwadMDWR', '2017-10-27 06:20:42', '2017-10-27 06:51:16', '2017-10-27 06:51:16', 0),
(1221, 5, '7gUOVfi6Q6hdzvjNseqvjy', '2017-10-27 06:20:43', '2017-10-27 06:52:08', '2017-10-27 06:52:08', 0),
(1222, NULL, 'xccYe73kGWZfh4ztNMBo0', '2017-10-27 06:51:16', '2017-10-27 07:21:18', '2017-10-27 07:21:18', 0),
(1223, 5, '4NdckoyRnX7rxaBkLgxr7V', '2017-10-27 06:51:16', '2017-10-27 07:14:47', '2017-10-27 07:14:47', 0),
(1224, 5, '2YXsowuTa55KX1m8wVjUQV', '2017-10-27 07:14:47', '2017-10-27 07:46:05', '2017-10-27 07:46:05', 0),
(1225, NULL, '7fg1ufklrJ9rx1y5VZNvIb', '2017-10-27 07:45:55', '2017-10-27 08:15:56', '2017-10-27 08:15:56', 0),
(1226, 5, '7YAKYYo5ntI7QTnMZCry1A', '2017-10-27 07:45:55', '2017-10-27 08:17:04', '2017-10-27 08:17:04', 0),
(1227, NULL, '1sdS6Jael0imoRg38NwUNX', '2017-10-27 08:16:30', '2017-10-27 09:29:39', '2017-10-27 09:29:39', 0),
(1228, 5, '4kXX6IyaXey3jCQ6eH5HlH', '2017-10-27 08:16:30', '2017-10-27 09:29:39', '2017-10-27 09:29:39', 0),
(1229, NULL, '2zO9ORjP1Baxxl8E9dU1Gq', '2017-10-27 09:29:39', '2017-10-27 10:07:29', '2017-10-27 10:07:29', 0),
(1230, 5, '7Ac7MnpvXOKr3V1mvmo6oa', '2017-10-27 09:29:39', '2017-10-27 10:07:29', '2017-10-27 10:07:29', 0),
(1231, NULL, '5z4f1MyEGEvBQ2DnOwtJE8', '2017-10-27 10:07:28', '2017-10-27 10:47:55', '2017-10-27 10:47:55', 0),
(1232, 5, '1yICH5PYg65577jCLjU5iG', '2017-10-27 10:07:29', '2017-10-27 10:47:55', '2017-10-27 10:47:55', 0),
(1233, NULL, '3PkPRcSLEey6pmOTBcy21E', '2017-10-27 10:47:57', '2017-10-30 06:03:27', '2017-10-30 06:03:27', 0),
(1234, 5, '2NXM7tdzc90A0HHHAYntXd', '2017-10-27 10:47:57', '2017-10-30 06:03:27', '2017-10-30 06:03:27', 0),
(1235, NULL, '64nrAKJbOSVvFVtyFbcLOc', '2017-10-30 06:03:27', '2017-10-30 06:34:34', '2017-10-30 06:34:34', 0),
(1236, NULL, '3QxO1eJkKN6udl6YqIk1g3', '2017-10-30 06:34:34', '2017-10-30 07:13:16', '2017-10-30 07:13:16', 0),
(1237, NULL, '2yoewlQDro3Vtc1dAY3HJN', '2017-10-30 06:34:34', '2017-10-30 07:13:16', '2017-10-30 07:13:16', 0),
(1238, NULL, '5eCDho8M6mB6I3Jpsij5nP', '2017-10-30 07:13:15', '2017-10-30 07:44:50', '2017-10-30 07:44:50', 0),
(1239, NULL, '3MCSdyGbnxhnkH9brhaxeY', '2017-10-30 07:13:16', '2017-10-30 07:44:50', '2017-10-30 07:44:50', 0),
(1240, NULL, 'TH2W5HiKvITkyoQf8CJJd', '2017-10-30 07:44:49', '2017-10-30 08:17:47', '2017-10-30 08:17:47', 0),
(1241, NULL, '2gkO3jXD2ZiAKSoKYIK82W', '2017-10-30 07:44:50', '2017-10-30 08:17:47', '2017-10-30 08:17:47', 0),
(1242, NULL, '38MXPkkbCvx4WQH5TvrLqn', '2017-10-30 08:17:46', '2017-10-30 09:51:10', '2017-10-30 09:51:10', 0),
(1243, NULL, '69PdZdzknXTnjDIQk9Rn9I', '2017-10-30 08:17:46', '2017-10-30 09:51:10', '2017-10-30 09:51:10', 0),
(1244, NULL, 'uO0I41WFJNvbl1tphgVrN', '2017-10-30 09:51:09', '2017-10-30 10:21:22', '2017-10-30 10:21:22', 0),
(1245, NULL, '17elafAE8h9ZLGZsSBgfO7', '2017-10-30 10:21:21', '2017-10-30 10:55:08', '2017-10-30 10:55:08', 0),
(1246, 5, '19nQe2JfSejIJH8kOniduL', '2017-10-30 10:21:21', '2017-10-30 11:17:40', '2017-10-30 11:17:40', 0),
(1247, NULL, 'ObniaO8O3Xb8bSD0fHPCI', '2017-10-30 11:17:06', '2017-10-30 11:50:44', '2017-10-30 11:50:44', 0),
(1248, 5, '4TtBF9eMGeWoFYyH47sVIF', '2017-10-30 11:17:07', '2017-10-30 12:30:10', '2017-10-30 12:30:10', 0),
(1249, NULL, '2FwqkoeZzYFreLIonx0GSQ', '2017-10-30 11:50:44', '2017-10-30 12:30:10', '2017-10-30 12:30:10', 0),
(1250, NULL, '4BXy1TqsHKP68WD6UYgXbh', '2017-10-30 11:50:44', '2017-10-30 12:30:10', '2017-10-30 12:30:10', 0),
(1251, NULL, '5DWVRI9shvoO57lGgTGICe', '2017-10-30 12:30:10', '2017-10-30 13:01:38', '2017-10-30 13:01:38', 0),
(1252, NULL, 'q1Utzn6v4z7yLtgAhl1H1', '2017-10-30 12:30:10', '2017-10-30 13:01:38', '2017-10-30 13:01:38', 0),
(1253, NULL, 'UycQf6K3UOV0DVhJSTMkY', '2017-10-30 13:01:36', '2017-10-31 06:23:09', '2017-10-31 06:23:09', 0),
(1254, NULL, '6saobm1X8zxoQMk8Ocx9bW', '2017-10-30 13:01:38', '2017-10-31 06:23:09', '2017-10-31 06:23:09', 0),
(1255, NULL, '7gsiFnLfudgRSGkCvsK6k8', '2017-10-31 06:23:09', '2017-10-31 07:05:02', '2017-10-31 07:05:02', 0),
(1256, 5, '48l35JyXCrISDl7fYZaNvG', '2017-10-31 07:05:02', '2017-10-31 07:59:39', '2017-10-31 07:59:39', 0),
(1257, NULL, '5TxJ41ihpQQftQYEWZWlsi', '2017-10-31 07:59:30', '2017-10-31 08:31:15', '2017-10-31 08:31:15', 0),
(1258, 5, '36c0pG5wNaEu1WNSSZ6dr5', '2017-10-31 07:59:30', '2017-10-31 10:02:01', '2017-10-31 10:02:01', 0),
(1259, NULL, '5PxhH5zwoVZUU8m6kkEoCI', '2017-10-31 08:31:15', '2017-10-31 10:02:01', '2017-10-31 10:02:01', 0),
(1260, NULL, '23fIvTF7J6Tfx45NgzCw0L', '2017-10-31 08:31:16', '2017-10-31 10:02:01', '2017-10-31 10:02:01', 0),
(1261, NULL, '4ZMtaZ01gRbzasysAhb0Mm', '2017-10-31 10:02:00', '2017-10-31 10:46:36', '2017-10-31 10:46:36', 0),
(1262, 5, '68ItE69SxtgXN3zSHSF376', '2017-10-31 10:02:01', '2017-10-31 10:52:27', '2017-10-31 10:52:27', 0),
(1263, NULL, '7BO0V8oUsOkqs6YxMJ5ruy', '2017-10-31 10:46:36', '2017-11-02 06:12:05', '2017-11-02 06:12:05', 0),
(1264, NULL, 'Ja56Am5WL9IZ7sW7tUDbO', '2017-10-31 10:46:36', '2017-11-02 06:12:05', '2017-11-02 06:12:05', 0),
(1265, 5, '5fy1Im8Tjxx3bobcQBTgQg', '2017-10-31 10:49:02', '2017-11-02 06:12:05', '2017-11-02 06:12:05', 0);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `idiomas__claves`
--
ALTER TABLE `idiomas__claves`
  ADD CONSTRAINT `FK_SECCIONES` FOREIGN KEY (`id_seccion`) REFERENCES `idiomas__secciones` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `idiomas__textos`
--
ALTER TABLE `idiomas__textos`
  ADD CONSTRAINT `FK_CLAVES` FOREIGN KEY (`id_clave`) REFERENCES `idiomas__claves` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_IDIOMAS` FOREIGN KEY (`id_idioma`) REFERENCES `idiomas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `noticias__archivos`
--
ALTER TABLE `noticias__archivos`
  ADD CONSTRAINT `FK_NOTICIAS_ARCHIVOS_POST` FOREIGN KEY (`id_noticia`) REFERENCES `noticias__posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `noticias__comments`
--
ALTER TABLE `noticias__comments`
  ADD CONSTRAINT `FK_NOTICIAS_COMMENTS_POST` FOREIGN KEY (`post_id`) REFERENCES `noticias__posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `noticias__galeria`
--
ALTER TABLE `noticias__galeria`
  ADD CONSTRAINT `FK_NOTICIAS_GALERIA_POST` FOREIGN KEY (`id_noticia`) REFERENCES `noticias__posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `noticias__posts`
--
ALTER TABLE `noticias__posts`
  ADD CONSTRAINT `FK_NOTICIAS_POSTS_CATEGORIA` FOREIGN KEY (`category_id`) REFERENCES `noticias__categories` (`id`);

--
-- Filtros para la tabla `noticias__rel_tags`
--
ALTER TABLE `noticias__rel_tags`
  ADD CONSTRAINT `FK_NOTICIAS_REL_TAGS_POST` FOREIGN KEY (`post_id`) REFERENCES `noticias__posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_NOTICIAS_REL_TAGS_TAG` FOREIGN KEY (`tag_id`) REFERENCES `noticias__tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FK_ORDERS_TOKEN_ID` FOREIGN KEY (`id_token`) REFERENCES `users__tokens` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ORDERS_USER_ID` FOREIGN KEY (`codigo_cliente`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `orders__articulos`
--
ALTER TABLE `orders__articulos`
  ADD CONSTRAINT `FK_ORDERS_ARTICULOS_ID` FOREIGN KEY (`id_order`) REFERENCES `orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_ORDERS_ARTICULOS_PRODUCTO_ID` FOREIGN KEY (`articulo`) REFERENCES `productos` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Filtros para la tabla `orders__request`
--
ALTER TABLE `orders__request`
  ADD CONSTRAINT `FK_ORDERS_REQUEST_ORDER_ID` FOREIGN KEY (`id_order`) REFERENCES `orders` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `productos__valores`
--
ALTER TABLE `productos__valores`
  ADD CONSTRAINT `FK_PRODUCTOS_VALORES_PRODUCTO` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `users__data`
--
ALTER TABLE `users__data`
  ADD CONSTRAINT `FK_USERS_DATA_ID` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `users__rel_roles`
--
ALTER TABLE `users__rel_roles`
  ADD CONSTRAINT `FK_USERS_REL_ROLES_ROLE_ID` FOREIGN KEY (`role_id`) REFERENCES `users__roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `USERS_REL_ROLES_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `users__tokens`
--
ALTER TABLE `users__tokens`
  ADD CONSTRAINT `FK_USERS_TOKENS_USER_ID` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
